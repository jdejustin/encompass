var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var ModelName= "Rep";

var Reps = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

  this.index = function (req, resp, params) {
    var self = this;
    var reps = [];

    var currentClient = self.session.get('currentClient');
    var roleId = self.session.get('userRoleId');
    
    self.session.set('currentSection', constants.sections.CLIENT_REPS);

    var searchRepProperties = constants.SEARCH_REP_PROPERTIES();

     //process any reps that the client has stored
    if (currentClient.reps && currentClient.reps.length > 0)
    {
        var totalIdsProcessed = 0;

        var i;
        for (i = 0; i < currentClient.reps.length; i++) {
          //get rep
          geddy.model.Rep.first( currentClient.reps[i], function(err, rep) {
            if (err) {
              totalIdsProcessed++;
            } else {
              //store result
              reps.push(rep);

              totalIdsProcessed++;
              
              //check if we have processed all the client's reps
              if (totalIdsProcessed == currentClient.reps.length)
              {
                  //we're done!
                  self.respond( {
                        reps:reps, 
                        currentClient: currentClient,
                        searchRepProperties:searchRepProperties
                        },
                        {type:'Rep'});
              }
            }
            });
        }
    } else {
      self.respond( {
        reps:reps, 
        currentClient: currentClient,
        searchRepProperties:searchRepProperties
        },
        {type:'Rep'});
    }
  };


  this.show = function (req, resp, params) {
    var self = this;
    var roleId = self.session.get('userRoleId');
    
    geddy.model.Rep.all({role: {ne: null}},function (err, reps) {
      if (err)
      {
        throw err;
      }

      var resultObject = new Object();
      resultObject ['allReps'] = reps;

      resultObject ['displayEdit'] = true;

      DBUtils.showObject(self,ModelName,params,constants.sections.CLIENT_REPS, resultObject);
    });
  };


  this.clearCurrentRep = function (req, resp, params) {
    var self = this;
    DBUtils.clearCurrentObject(self,ModelName,params);
   };



  this.save = function (req, resp, params) {
    var self = this;

    DBUtils.saveObject(self,ModelName,params);

    var currentClient = self.session.get("currentClient");
    var currentRep = self.session.get("currentRep");

    DBUtils.saveDataTo(self, 'Client', currentClient.id, 'reps', currentRep.id);
  }


  this.saveEmbeddedModel = function (req, resp, params) {
    var self = this;
    DBUtils.saveObjectEmbeddedModel(self,ModelName,params);

    var currentClient = self.session.get("currentClient");
    var currentRep = self.session.get("currentRep");

    DBUtils.saveDataTo(self, 'Client', currentClient.id, 'reps', currentRep.id);
  }

  this.saveDefaultItem = function (req, resp, params){
    var self = this;
    var currentRep = self.session.get("currentRep");

    currentRep[params.targetProperty] = params.value;

    currentRep.save(function(err, data){
      if (params.targetProperty == 'defaultEmail' && currentRep.level)
      {
        //send rep data to be saved as a user... if needed
        //but first, let's check the level for the user so we can set their role correctly

        var role;
        switch(currentRep.level)
        {
          case "1":
          case "2":
            role = constants.roles.MANAGER.label;
            break;

          case "3": 
          case "4":
          case "5":
            role = constants.roles.REP.label;
            break;
        }

        DBUtils.saveAsUser(self, currentRep.firstName, currentRep.lastName, params.value, role, currentRep.id);
      }

      self.respond(true, {
        format: 'json'
      });
    });
  }


  this.deleteReps = function (req, resp, params) {
    var self = this;
    DBUtils.deleteObject(self,ModelName,params);
  };


  this.deleteRepFromClient = function (req, resp, params) {
    var self = this;
    DBUtils.deleteFromArrayByValue(self, params.ids, self.session.get('currentClient'), 'reps');
  }


  // Contact Step pages
  this.repStep1 = function (req, resp, params) {
    var self = this;
    var suffixes = constants.SUFFIXES();
    var countries = constants.COUNTRIES();
    var states = constants.STATES();
    var selectPrompt = constants.SELECT_PROMPT();
    var addressTypes = constants.ADDRESS_TYPES();
    var phoneTypes = constants.PHONE_NUMBER_TYPES();

    var currentRep = self.session.get("currentRep");

    var currentClient = self.session.get('currentClient');

    //let's put together the info for roles and levels
    var roles = [];
    var level1 = constants.SALES_FORCE_LABELS_LEVEL1();
    var level2 = constants.SALES_FORCE_LABELS_LEVEL2();
    var level3 = constants.SALES_FORCE_LABELS_LEVEL3();
    var level4 = constants.MEDICAL_AFFAIRS_LABELS();
    var level5 = constants.PAYOR_LABELS();

    var i;

    if (currentClient.salesForceLabelsLevel1)
    {
      for (i = 0; i < level1.length; i++) {
        if (currentClient.salesForceLabelsLevel1.indexOf(level1[i].label) != -1)
        {
          roles.push(level1[i]);
        }
      };
    }

    if (currentClient.salesForceLabelsLevel2)
    {
      for (i = 0; i < level2.length; i++) {
        if (currentClient.salesForceLabelsLevel2.indexOf(level2[i].label) != -1)
        {
          roles.push(level2[i]);
        }
      };
    }

    if (currentClient.salesForceLabelsLevel3)
    {
      for (i = 0; i < level3.length; i++) {
        if (currentClient.salesForceLabelsLevel3.indexOf(level3[i].label) != -1)
        {
          roles.push(level3[i]);
        }
      };
    }

    if (currentClient.medicalAffairs)
    {
      for (i = 0; i < level4.length; i++) {
        if (currentClient.medicalAffairs.indexOf(level4[i].label) != -1)
        {
          roles.push(level4[i]);
        }
      };
    }

    if (currentClient.payors)
    {
      for (i = 0; i < level5.length; i++) {
        if (currentClient.payors.indexOf(level5[i].label) != -1)
        {
          roles.push(level5[i]);
        }
      };
    }


    //get all the reps for current client... we'll need them for when a role is changed and we display next level up (for manager info)
    var allReps = [];
    geddy.model.Rep.all({role: {ne: null}},function (err, reps) {
      if (err)
      {
        throw err;
      }

      allReps = reps;

      self.respond( {
          rep: currentRep,
          roles: roles,
          allReps: allReps,
          suffixes:suffixes,
          selectPrompt:selectPrompt,
          currentClient:self.session.get('currentClient'),
          countries:countries,
          states:states,
          addressTypes:addressTypes,
          phoneTypes:phoneTypes,
          stepTitle:createTitle(currentRep)},
          {format: 'html',template: 'app/views/reps/repStep1'});
    });
  }

   // Contact Step pages
  this.repStep2 = function (req, resp, params) {
    console.log('?>>>>>>>>>>>>>>REPO STEP2 ');
    self = this;
    //var countries = constants.COUNTRIES();
    var selectPrompt = constants.SELECT_PROMPT();
    var currentRep = self.session.get("currentRep");

    self.respond( {
      rep: currentRep,
      selectPrompt:selectPrompt,
      currentClient:self.session.get('currentClient'),
      stepTitle:createTitle(currentRep)},
      {format: 'html',template: 'app/views/reps/repStep2'});
  };

  this.getRepsByLevel = function (req, resp, params) {
    var self = this;
    geddy.model.Rep.all({level: {lt: params.level}}, function (err, reps){
      if (err)
      {
        throw err;
      }

      self.respond( {
         reps: reps},
         {format: 'json'}
      );
    });

  }

  this.search = function (req, resp, params) {
    var $thisRequest = this;
    var search = require('../helpers/search/search.js');
    search.AdvanceSearch($thisRequest, 'reps', params);
  }


  function createTitle (currentRep) {

     var headerTitle = "";

     if(currentRep) {
        if((currentRep.firstName) || (currentRep.lastName)){
           headerTitle = currentRep.firstName + ' ' + currentRep.lastName + ' - ';
        }
     } else {   
        headerTitle = 'New Field Force - '
     }

     return headerTitle;
  }
};

exports.Reps = Reps;
