var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var ModelName= "Client";

var Portalclients = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];
   
   this.before(function () { 
      var self = this;
      if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
      {
       geddy.globalDb.close();
        self.respond('false',{format: 'txt'});
      }
      else
      {
        self.session.set('activeController', self.name);
      }
    });


  this.index = function (req, resp, params) {
    var self = this;

    self.session.set('currentSection', constants.sections.CLIENT_PORTAL_PROGRAM_DETAILS);

    self.respond( true,
    {format: 'html',template: 'app/views/portals/clients/clientProgramDetails'});
  };

  this.getClientEvents = function(req, resp, param){
    var self = this;
    var currentClientId = self.session.get('userClientId');

    geddy.model.Client.first(currentClientId, function (error, client){
      //set the current client 
      self.session.set('currentClient', client);
    });

    DBUtils.GetClientEvents(currentClientId,this.responseEvents,self);
  }


  this.responseEvents = function(response,events){
      var EVENT_APPROVAL_STATUS = constants.EVENT_APPROVAL_STATUS();
      var EVENT_STATUS = constants.EVENT_STATUS();

      events.sort(function(a,b){
        var result = DBUtils.numericCompare(b.approvalStatus, a.approvalStatus);
        return result;
      });

      var myEvents = [];

      for(var i=0;i<events.length;i++){
        var tmpEvent = {};

        if (events[i].potentialSpeakers && events[i].potentialSpeakers.length > 0)
        {
          for (var j = 0; j < events[i].potentialSpeakers.length; j++) {
            if (events[i].potentialSpeakers[j].crgStatus == '1') //confirmed crg status
            {
               events[i].confirmedSpeaker = events[i].potentialSpeakers[j].speakerName;
            } 
          };
        }
        tmpEvent.eventInfo = events[i];
        tmpEvent.statusDisplay = EVENT_STATUS[events[i].status].type;          
        tmpEvent.statusApprovalDisplay = EVENT_APPROVAL_STATUS[events[i].approvalStatus].type;          
        myEvents.push(tmpEvent);
      }

      response.respond({dataset:myEvents}, { format: 'json' });
  }



  this.clientProgramDetails = function (req, resp, params){
    var self = this;

    self.session.set('currentSection', constants.sections.CLIENT_PORTAL_PROGRAM_DETAILS);

    self.respond( true,
    {format: 'html',template: 'app/views/portals/clients/clientProgramDetails'});
  }



  this.clientDetails = function (req, resp, params){
    var self = this;

    params.id = self.session.get('userClientId'); 

    var resultObject = new Object();
    resultObject.editing = false;
    resultObject.editor = null;

    var callBack = function(){
      self.respond( resultObject,
        {format: 'html',template: 'app/views/portals/clients/clientDetails'});
    };

    DBUtils.showObject(self,ModelName,params,constants.sections.CLIENT_PORTAL_DETAILS, resultObject, callBack);
  }



  this.clientResources = function (req, resp, params) {
    var self = this;

    var clientId = self.session.get('userClientId'); 

    self.session.set('currentSection', constants.sections.CLIENT_PORTAL_RESOURCES);

    geddy.model.Client.first(clientId, function (err, client){
      if (err)
      {
        throw err;
      }

      self.respond({

        kolResources: client.kolResources,
        repResources: client.repResources,
        rsmResources: client.rsmResources
        },
        {format: 'html',template: 'app/views/portals/clients/clientResources'}
      );
    });

  }
};

exports.Portalclients = Portalclients;