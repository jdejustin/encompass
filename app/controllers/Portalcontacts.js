var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var ModelName= "Contact";

var Portalcontacts = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

   this.before(function () { 
    var self = this;
    if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
    {
     geddy.globalDb.close();
      self.redirect('/login');
    }
    self.session.set('activeController', self.name);
    });


  
  
  this.index = function (req, resp, params) {
    var self = this;

    self.session.set('currentSection', constants.sections.CONTACT_PORTAL_PROGRAM_DETAILS);

    self.respond( true,
    {format: 'html',template: 'app/views/portals/contacts/contactProgramDetails'});
  };

  this.getMyEvents = function(req, resp, param){
      var self = this;
      var currentContactId = self.session.get('userLinkedId');

      DBUtils.GetContactEvents(currentContactId, this.responseEvents, self);
  }


  this.responseEvents = function(response, events){
    var EVENT_STATUS = constants.EVENT_STATUS();

    events.sort(function(a,b){
      var result = DBUtils.numericCompare(b.approvalStatus, a.approvalStatus);
      return result;
    });

    var myEvents = [];

    for(var i=0;i<events.length;i++){
      var tmpEvent = {};
      tmpEvent.eventInfo = events[i];
      //console.log(">>>> status: " + events[i].status);
      tmpEvent.statusDisplay = EVENT_STATUS[events[i].status].type;          
      myEvents.push(tmpEvent);
    }

    response.respond({dataset:myEvents}, { format: 'json' });
  }



  this.contactProgramDetails = function (req, resp, params){
    var self = this;

    self.session.set('currentSection', constants.sections.CONTACT_PORTAL_PROGRAM_DETAILS);

    self.respond( true,
    {format: 'html',template: 'app/views/portals/contacts/contactProgramDetails'});
  }


  this.contactDetails = function (req, resp, params){
    var self = this;

    params.id = self.session.get('userLinkedId'); 

    //get all clients
    geddy.model.Client.all({createdAt: {ne: null}}, {sort: {name: 'asc'}}, function (err, clients) {
      if (err) {
        throw err;
      }
      
      var resultObject = new Object();
      resultObject['clients'] = clients;
      resultObject.editing = false;
      resultObject.editor = null;

      //get contact... we need to get its reps first
      geddy.model.Contact.first(params.id, function (err, contact) {
        if (err)
        {
          throw err;
        }

        //now get the reps for this contact
        if (contact.reps)
        {
          var totalRepsProcessed = 0;

          var i;
          for (i = 0; i < contact.reps.length; i++) {
            geddy.model.Rep.first(contact.reps[i], function (err, rep) {
              if (err)
              {
                totalRepsProcessed++;
              } 

              totalRepsProcessed++;

              if (!resultObject.reps){
                resultObject.reps = [];
              }
              resultObject.reps.push(rep);

              if (totalRepsProcessed == contact.reps.length)
              {
                if (contact.contracts && contact.contracts.length > 0)
                {
                  //now we need all the contact's contracts
                  var totalContractsProcessed = 0;

                  var i;
                  for (i = 0; i < contact.contracts.length; i++) {
                    geddy.model.Contract.first(contact.contracts[i], function (err, contract) {
                      if (err)
                      {
                        totalContractsProcessed++;
                      } 

                      totalContractsProcessed++;

                      if (!resultObject.contracts){
                        resultObject.contracts = [];
                      }

                      resultObject.contracts.push(contract);

                      if (totalContractsProcessed == contact.contracts.length)
                      {
                        var callBack = function(){
                          self.respond( resultObject,
                            {format: 'html',template: 'app/views/portals/contacts/contactDetails'});
                        };

                        DBUtils.showObject(self,ModelName,params,constants.sections.CONTACT_PORTAL_DETAILS,resultObject, callBack);
                      }
                    });
                  }
                } else {

                  var callBack = function(){
                    self.respond( resultObject,
                      {format: 'html',template: 'app/views/portals/contacts/contactDetails'});
                  };

                  DBUtils.showObject(self,ModelName,params,constants.sections.CONTACT_PORTAL_DETAILS,resultObject, callBack);
                }
              }
            });
          }
        } else {
          resultObject['reps'] = [];

          //now we need all the contact's contracts
          if (contact.contracts && contact.contracts.length > 0)
          {
            var totalContractsProcessed = 0;

            var i;
            for (i = 0; i < contact.contracts.length; i++) {
              geddy.model.Contract.first(contact.contracts[i], function (err, contract) {
                if (err)
                {
                  totalContractsProcessed++;
                } 

                totalContractsProcessed++;

                if (!resultObject.contracts){
                  resultObject.contracts = [];
                }

                resultObject.contracts.push(contract);

                if (totalContractsProcessed == contact.contracts.length)
                {
                  var callBack = function(){
                    self.respond( resultObject,
                      {format: 'html',template: 'app/views/portals/contacts/contactDetails'});
                  };

                  DBUtils.showObject(self,ModelName,params,constants.sections.CONTACT_PORTAL_DETAILS,resultObject, callBack);
                }
              });
            }
          } else {
            resultObject['contracts'] = [];

            var callBack = function(){
              self.respond( resultObject,
                {format: 'html',template: 'app/views/portals/contacts/contactDetails'});
            };

            DBUtils.showObject(self,ModelName,params,constants.sections.CONTACT_PORTAL_DETAILS,resultObject, callBack);
          }
        }
      });
    });
  }


  this.contactResources = function (req, resp, params) {
    var self = this;

    self.session.set('currentSection', constants.sections.CONTACT_PORTAL_RESOURCES);

    //get all contact resources from clients
    geddy.model.Contact.first(self.session.get('userLinkedId'), function(err, contact){
      if (err)
      {
        throw err;
      }

      var resources = [];
      var totalClientsProcessed = 0;
      var i;
      for (i = 0; i < contact.clients.length; i++) {
        var clientId = contact.clients[i].id;

        geddy.model.Client.first(clientId, function (err, client){
          totalClientsProcessed++;

          resources.push({clientName: client.name , resources: client.kolResources});

          if (totalClientsProcessed == contact.clients.length)
          {
            //done!
            self.respond({
              contactResources: resources
              },
              {format: 'html',template: 'app/views/portals/contacts/contactResources'}
            );
          }
        });
      }
    });
  }
};

exports.Portalcontacts = Portalcontacts;