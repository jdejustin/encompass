var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var ModelName= "Rep";

var Portalforcefields = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

  this.index = function (req, resp, params) {
    var self = this;

    self.session.set('currentSection', constants.sections.REP_PORTAL_PROGRAM_DETAILS);
    //console.log("portal reps index: Current Section: "+self.session.get('currentSection'));
    self.respond( { selectPrompt: constants.SELECT_PROMPT()},
      {format: 'html',template: 'app/views/portals/reps/repProgramDetails'});
  };



  this.repProgramDetails = function (req, resp, params){
    var self = this;
    self.session.set('currentSection', constants.sections.REP_PORTAL_PROGRAM_DETAILS);
    //console.log("portal reps repProgramDetails: Current Section: "+self.session.get('currentSection'));
    self.respond( { selectPrompt: constants.SELECT_PROMPT()},
      {format: 'html',template: 'app/views/portals/reps/repProgramDetails'});
  }


  this.responseEvents = function(response,events){
    var EVENT_APPROVAL_STATUS = constants.EVENT_APPROVAL_STATUS();
    var EVENT_STATUS = constants.EVENT_STATUS();

    // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>RESPONSE events:"+JSON.stringify(events));

    events.sort(function(a,b){
      var result = DBUtils.numericCompare(b.approvalStatus, a.approvalStatus);
      // console.log(">>>>>>>>>>>>>>>>>>>>>>>>> SORT Result:"+result);
      return result;
    });

    var myEvents = [];

    for(var i=0;i<events.length;i++){
      var tmpEvent = {};
      tmpEvent.eventInfo = events[i];
      tmpEvent.statusApprovalDisplay = EVENT_APPROVAL_STATUS[events[i].approvalStatus].type;          
      tmpEvent.statusDisplay = EVENT_STATUS[events[i].status].type;          
      myEvents.push(tmpEvent);
    }

      // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>> LUEGO DEL FOR");
      // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> myEvents on the Callback "+JSON.stringify(myEvents));
      response.respond({dataset:myEvents}, { format: 'json' });
  }



  this.repDetails = function (req, resp, params){
    var self = this;

    params.id = self.session.get('userLinkedId');

    geddy.model.Rep.all({role: {ne: null}},function (err, reps) {
      if (err)
      {
        throw err;
      }

      var resultObject = new Object();
      resultObject ['allReps'] = reps;

      var callBack = function(){
        self.respond( resultObject,
          {format: 'html',template: 'app/views/portals/reps/repDetails'});
      };
    
      DBUtils.showObject(self,ModelName,params,constants.sections.REP_PORTAL_DETAILS, resultObject, callBack);
    });
  }


  this.getMyProgramRequest = function(req, resp, param){
    var self = this;
    var currentRepId = self.session.get('userLinkedId');

    console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> session rep "+currentRepId);
    DBUtils.GetMyProgramRequestEvents(currentRepId,this.responseEvents,self);
  }


  this.repResources = function (req, resp, params) {
    var self = this;

    self.session.set('currentSection', constants.sections.REP_PORTAL_RESOURCES);

    //get all rep resources from client
    var clientCollection  = self.session.get('clientCollection');

    //searching object 
    //use reps id to find out their client and from that, we get the resources.
    var searchObject = {"reps":{ $all: [self.session.get('userLinkedId')]}};

    if (typeof clientCollection == "undefined" || !clientCollection)
    {
      self.session.set("clientCollection", geddy.globalDb.collection('clients'));
      clientCollection  = self.session.get('clientCollection');
    }

    clientCollection.find(searchObject).toArray(function(err, client){
      self.respond({
        repResources: client[0].repResources
        },
        {format: 'html',template: 'app/views/portals/reps/repResources'}
      );
    });
  }
};

exports.Portalforcefields = Portalforcefields;

