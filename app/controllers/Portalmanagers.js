var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var ModelName= "Rep";

var Portalmanagers = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

   this.before(function () { 
    var self = this;
    if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
    {
     geddy.globalDb.close();
      self.respond('false',{format: 'txt'});
    }
    else
    {
      self.session.set('activeController', self.name);
    }
    });
  
  this.index = function (req, resp, params) {
    var self = this;

    self.session.set('currentSection', constants.sections.MANAGER_PORTAL_PROGRAM_DETAILS);

    self.respond( true,
    {format: 'html',template: 'app/views/portals/managers/managerProgramDetails'});
  };


  this.getMyProgramRequest = function(req, resp, param){
    var self = this;
    var currentRepId = self.session.get('userLinkedId');

    //set the current client also
    geddy.model.Client.first({reps: {$in : [currentRepId]}}, function (err, client){
      self.session.set('currentClient', client);
    })
        
    DBUtils.GetRepManagerEvents(currentRepId,this.responseEvents,self);
  }

  this.responseEvents = function(response,events){
      //response.respond({dataset:events}, { format: 'json' });
      var EVENT_STATUS = constants.EVENT_STATUS();
      var EVENT_APPROVAL_STATUS = constants.EVENT_APPROVAL_STATUS();

      // events.sort(function(a,b){
      //   var result = DBUtils.numericCompare(b.approvalStatus, a.approvalStatus);
      //   return result;
      // });

      var s = DBUtils.firstBy(function (v1, v2) { return v1.approvalStatus < v2.approvalStatus ? -1 : (v1.approvalStatus > v2.approvalStatus ? 1 : 0); })
                 .thenBy(function (v1, v2) { return v1.eventDatePref1 - v2.eventDatePref1; });

      events.sort(s);

      var myEvents = [];

      for(var i=0;i<events.length;i++){
        var tmpEvent = {};

      if (events[i].potentialSpeakers && events[i].potentialSpeakers.length > 0)
      {
        for (var j = 0; j < events[i].potentialSpeakers.length; j++) {
          if (events[i].potentialSpeakers[j].crgStatus == '1') //confirmed crg status
          {
             events[i].confirmedSpeaker = events[i].potentialSpeakers[j].speakerName;
          } 
        };
      }
        
        tmpEvent.eventInfo = events[i];
        tmpEvent.statusDisplay = EVENT_STATUS[events[i].status].type;     
        tmpEvent.statusApprovalDisplay = EVENT_APPROVAL_STATUS[events[i].approvalStatus].type;          
        myEvents.push(tmpEvent);
      }

      response.respond({dataset:myEvents}, { format: 'json' });
  }

  this.managerProgramDetails = function (req, resp, params){
    var self = this;

    self.session.set('currentSection', constants.sections.MANAGER_PORTAL_PROGRAM_DETAILS);

    self.respond( true,
    {format: 'html',template: 'app/views/portals/managers/managerProgramDetails'});
  }


  this.managerDetails = function (req, resp, params){
    var self = this;

    params.id = self.session.get('userLinkedId'); 

    geddy.model.Rep.all({role: {ne: null}},function (err, reps) {
      if (err)
      {
        throw err;
      }

      var resultObject = new Object();
      resultObject ['allReps'] = reps;
      resultObject.editing = false;
      resultObject.editor = null;

      var callBack = function(){
        self.respond( resultObject,
          {format: 'html',template: 'app/views/portals/managers/managerDetails'});
      };
    

      DBUtils.showObject(self,ModelName,params,constants.sections.MANAGER_PORTAL_DETAILS, resultObject, callBack);
    });
  }


  this.managerResources = function (req, resp, params) {
    var self = this;

    self.session.set('currentSection', constants.sections.MANAGER_PORTAL_RESOURCES);

    //get all rep resources from client
    var clientCollection  = self.session.get('clientCollection');

    //searching object 
    //use reps id to find out their client and from that, we get the resources.
    var searchObject = {"reps":{ $all: [self.session.get('userLinkedId')]}};

    if (typeof clientCollection == "undefined" || !clientCollection)
    {
      self.session.set("clientCollection", geddy.globalDb.collection('clients'));
      clientCollection  = self.session.get('clientCollection');
    }

    clientCollection.find(searchObject).toArray(function(err, client){
      self.respond({
        managerResources: client[0].rsmResources
        },
        {format: 'html',template: 'app/views/portals/managers/managerResources'}
      );
    });
  }
};

exports.Portalmanagers = Portalmanagers;

