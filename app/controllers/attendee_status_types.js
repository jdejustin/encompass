var AttendeeStatusTypes = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

  this.index = function (req, resp, params) {
    var self = this;

    geddy.model.AttendeeStatusType.all(function(err, attendeeStatusTypes) {
      if (err) {
        throw err;
      }
      self.respondWith(attendeeStatusTypes, {type:'AttendeeStatusType'});
    });
  };

  this.add = function (req, resp, params) {
    this.respond({params: params});
  };

  this.create = function (req, resp, params) {
    var self = this
      , attendeeStatusType = geddy.model.AttendeeStatusType.create(params);

    if (!attendeeStatusType.isValid()) {
      this.respondWith(attendeeStatusType);
    }
    else {
      attendeeStatusType.save(function(err, data) {
        if (err) {
          throw err;
        }
        self.respondWith(attendeeStatusType, {status: err});
      });
    }
  };

  this.show = function (req, resp, params) {
    var self = this;

    geddy.model.AttendeeStatusType.first(params.id, function(err, attendeeStatusType) {
      if (err) {
        throw err;
      }
      if (!attendeeStatusType) {
        throw new geddy.errors.NotFoundError();
      }
      else {
        self.respondWith(attendeeStatusType);
      }
    });
  };

  this.edit = function (req, resp, params) {
    var self = this;

    geddy.model.AttendeeStatusType.first(params.id, function(err, attendeeStatusType) {
      if (err) {
        throw err;
      }
      if (!attendeeStatusType) {
        throw new geddy.errors.BadRequestError();
      }
      else {
        self.respondWith(attendeeStatusType);
      }
    });
  };

  this.update = function (req, resp, params) {
    var self = this;

    geddy.model.AttendeeStatusType.first(params.id, function(err, attendeeStatusType) {
      if (err) {
        throw err;
      }
      attendeeStatusType.updateProperties(params);

      if (!attendeeStatusType.isValid()) {
        self.respondWith(attendeeStatusType);
      }
      else {
        attendeeStatusType.save(function(err, data) {
          if (err) {
            throw err;
          }
          self.respondWith(attendeeStatusType, {status: err});
        });
      }
    });
  };

  this.remove = function (req, resp, params) {
    var self = this;

    geddy.model.AttendeeStatusType.first(params.id, function(err, attendeeStatusType) {
      if (err) {
        throw err;
      }
      if (!attendeeStatusType) {
        throw new geddy.errors.BadRequestError();
      }
      else {
        geddy.model.AttendeeStatusType.remove(params.id, function(err) {
          if (err) {
            throw err;
          }
          self.respondWith(attendeeStatusType);
        });
      }
    });
  };

};

exports.AttendeeStatusTypes = AttendeeStatusTypes;
