var AttendeeTypes = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

  this.index = function (req, resp, params) {
    var self = this;

    geddy.model.AttendeeType.all(function(err, attendeeTypes) {
      if (err) {
        throw err;
      }
      self.respondWith(attendeeTypes, {type:'AttendeeType'});
    });
  };

  this.add = function (req, resp, params) {
    this.respond({params: params});
  };

  this.create = function (req, resp, params) {
    var self = this
      , attendeeType = geddy.model.AttendeeType.create(params);

    if (!attendeeType.isValid()) {
      this.respondWith(attendeeType);
    }
    else {
      attendeeType.save(function(err, data) {
        if (err) {
          throw err;
        }
        self.respondWith(attendeeType, {status: err});
      });
    }
  };

  this.show = function (req, resp, params) {
    var self = this;

    geddy.model.AttendeeType.first(params.id, function(err, attendeeType) {
      if (err) {
        throw err;
      }
      if (!attendeeType) {
        throw new geddy.errors.NotFoundError();
      }
      else {
        self.respondWith(attendeeType);
      }
    });
  };

  this.edit = function (req, resp, params) {
    var self = this;

    geddy.model.AttendeeType.first(params.id, function(err, attendeeType) {
      if (err) {
        throw err;
      }
      if (!attendeeType) {
        throw new geddy.errors.BadRequestError();
      }
      else {
        self.respondWith(attendeeType);
      }
    });
  };

  this.update = function (req, resp, params) {
    var self = this;

    geddy.model.AttendeeType.first(params.id, function(err, attendeeType) {
      if (err) {
        throw err;
      }
      attendeeType.updateProperties(params);

      if (!attendeeType.isValid()) {
        self.respondWith(attendeeType);
      }
      else {
        attendeeType.save(function(err, data) {
          if (err) {
            throw err;
          }
          self.respondWith(attendeeType, {status: err});
        });
      }
    });
  };

  this.remove = function (req, resp, params) {
    var self = this;

    geddy.model.AttendeeType.first(params.id, function(err, attendeeType) {
      if (err) {
        throw err;
      }
      if (!attendeeType) {
        throw new geddy.errors.BadRequestError();
      }
      else {
        geddy.model.AttendeeType.remove(params.id, function(err) {
          if (err) {
            throw err;
          }
          self.respondWith(attendeeType);
        });
      }
    });
  };

};

exports.AttendeeTypes = AttendeeTypes;
