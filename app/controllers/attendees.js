var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var search = require('../helpers/search/search.js');
var ModelName= "Attendee";

var Attendees = function () {
   this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

   this.before(function () { 
    var self = this;
    if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
    {
      geddy.globalDb.close();
      self.respond('false',{format: 'txt'});
    }
    });

   this.index = function (req, resp, params) {
      var self = this;

      DBUtils.unlockObject(self);

      self.session.set('currentSection', constants.sections.EVENT_ATTENDEES);

      var result={};
      result.searchAttendeesProperties = constants.SEARCH_ATTENDEE_PROPERTIES();
      result.attendees = null;
      result.contacts = [];

      var start = +new Date();  // log start timestamp

      var currentEvent = self.session.get('currentEvent');

      //clear current Contact
      self.session.set('currentContact', null);

      //pagination vars 
      var attendeeCollection = geddy.globalDb.collection('attendees');
      var contactCollection = geddy.globalDb.collection('contacts');
      var pageLimit = geddy.config.pageLimit;
      var currentPage = 1;
      var recordCount=0;
      var totalPages=0;
      var searchObject={};

      result.currentPage = currentPage;
      result.totalPages = totalPages;
      result.statusTypes = constants.ATTENDEE_STATUS_TYPE();
      result.currentEvent = currentEvent;
      
      if (!currentEvent.attendees)
      {
        currentEvent.attendees = [];
      }
         
      DBUtils.GetEventAttendees(currentEvent.attendees, self.onGetAllAttendees, self, result);
   }


  this.onGetAllAttendees = function (self, result)
  {

     self.respond(result);
  }


this.show = function (req, resp, params) {
    var self = this;

    var continueProcess = true;

    if (params.id == '1234')
    {
      if (self.session.get('currentAttendee'))
      {
        params.id = self.session.get('currentAttendee').id;
      } else {
        continueProcess = false;
        self.respond('null',{format: 'txt'});
      }
    }

    if (continueProcess)
    {

      DBUtils.unlockObject(self);

      //Get attendee info first, then get the rest of contact info to display
      var callBack = function() {
        var currentAttendee = self.session.get('currentAttendee');
        var result = {};
        result.reps = [];
        result.contracts = [];
        result.statusTypes = constants.ATTENDEE_STATUS_TYPE();

        geddy.model.Contact.first(currentAttendee.contactId,function (err, contact) {
           if (err)
           {
              throw err;
           }

           result.currentAttendee = currentAttendee;
           result.currentContact = contact;

           self.session.set('currentContact', contact);

           if (contact.reps && contact.reps.length > 0)
           {
              var repCollection = geddy.globalDb.collection('reps');

              var searchObject = {id: {$in: contact.reps}};// mongo search object

              var sort = {
                   lastName: 'asc',
                   firstName : 'asc'
              }  

              repCollection.find(searchObject).toArray(function(err, allReps){
                 if (allReps != null)
                 {
                    result.reps = allReps;
                 }

                 if (contact.contracts && contact.contracts.length > 0)
                 {
                    var contractsCollection = geddy.globalDb.collection('contracts');

                    var searchObject = {id: {$in: contact.contracts}};// mongo search object

                    contractsCollection.find(searchObject).toArray(function(err, allContracts){
                       if (allContracts != null)
                       {
                          result.contracts = allContracts;
                       }

                       self.respond(result);
                    });
                 } else {
                    self.respond(result);
                 }
              });
           } else {
              if (contact.contracts && contact.contracts.length > 0)
              {
                 var contractsCollection = geddy.globalDb.collection('contracts');

                 var searchObject = {id: {$in: contact.contracts}};// mongo search object

                 contractsCollection.find(searchObject).toArray(function(err, allContracts){
                    if (allContracts != null)
                    {
                       result.contracts = allContracts;
                    }

                    self.respond(result);
                 });
              } else {
                 self.respond(result);
              }
           }
        });

      }

      DBUtils.showObject(self, ModelName, params, constants.sections.EVENT_ATTENDEES, null, callBack);
    }
  };


this.save = function (req, resp, params) {
    var self = this;

    var onAttendeeSaved = function()
    {
      var currentEvent = self.session.get("currentEvent");
      var currentAttendee = self.session.get("currentAttendee");
      var currentProgram = self.session.get("currentProgram");

      //if there's no registration date yet, add it
      if (!currentAttendee.registrationDate)
      {
        currentAttendee.registrationDate = new Date();
        currentAttendee.eventId = currentEvent.id;
        currentAttendee.save(function(err, data) {
          if (err) {
            throw err;
          }

          DBUtils.saveDataTo(self, 'Event', currentEvent.id, 'attendees', currentAttendee.id);
        });
      } else {
        DBUtils.saveDataTo(self, 'Event', currentEvent.id, 'attendees', currentAttendee.id);
      }

      var notificationData;

      if (currentEvent.sendRegistration && currentEvent.sendRegistration == true && params.status == 6) //Registered status
      {
         notificationData = new Object();
         notificationData.programId = currentProgram.id;
         notificationData.eventId = currentEvent.id;
         notificationData.deliveryType = constants.EVENT_REGISTRATION;

         geddy.model.Contact.first(currentAttendee.contactId, function(err, contact){
            if (err)
            {
               throw err;
            }


            notificationData.registeredAttendeeEmail = contact.defaultEmail;
            
            self.respond({notificationData: notificationData}, {format: 'json'});
         });

      } else {
         self.respond(true, {format: 'json'});
      }


    }

    DBUtils.saveObject(self,ModelName,params, null, onAttendeeSaved);
  }


  this.saveEmbeddedModel = function (req, resp, params) {
    var self = this;
    DBUtils.saveObjectEmbeddedModel(self,ModelName,params);

    var currentEvent = self.session.get("currentEvent");
    var currentAttendee = self.session.get("currentAttendee");

    DBUtils.saveDataTo(self, 'Event', currentEvent.id, 'attendees', currentAttendee.id);
  }

  this.deleteAttendees = function (req, resp, params) {
    var self = this;
    DBUtils.deleteObject(self,ModelName,params);
  };


  this.deleteAttendeeFromEvent = function (req, resp, params) {
    var self = this;
    DBUtils.deleteFromArrayByValue(self, params.ids, self.session.get('currentEvent'), 'attendees');
  }

  this.deleteTravelDocuments = function (req, resp, params) {
    var self = this;
    DBUtils.deleteUploadedFiles(self, params.ids, self.session.get('currentAttendee'), 'travelDocuments');
    DBUtils.deleteFromArrayByIndex(self, params.ids, self.session.get('currentAttendee'), 'travelDocuments');
  }


  this.clearCurrentAttendee = function (req, resp, params) {
    var self = this;
    DBUtils.clearCurrentObject(self,ModelName,params);
   };


   this.searchByNPI = function (req, resp, params){
      var self = this;

      //console.log('npi: ' + params.npiNumber);

      geddy.model.Contact.first({npi: params.npiNumber}, function (err, contact){
         if (err)
         {
            throw err;
         }

         self.session.set('currentContact', contact);

         var foundContact = (contact)? true : false;
         var attendee;

         if (foundContact)
         {
           attendee = null;
            attendee = linkAttendeeToContact(self, foundContact, contact);
         } else {
            self.respond({foundContact: foundContact, contact: contact, attendee: attendee}, {format: 'json'});
         }

     });
   }

   this.searchById = function (req, resp, params){
      var self = this;

      //console.log('npi: ' + params.npiNumber);

      geddy.model.Contact.first({id: params.id}, function (err, contact){
         if (err)
         {
            throw err;
         }

         self.session.set('currentContact', contact);

         var foundContact = (contact)? true : false;
         var attendee;

         if (foundContact)
         {
            attendee = linkAttendeeToContact(self, foundContact, contact);
         } else {
            self.respond({foundContact: foundContact, contact: contact, attendee: attendee}, {format: 'json'});
         }

     });
   }

   this.addById = function (req, resp, params){
      var self = this;

for ( var i = 0; i < params.ids.length; ++i)
		{
      console.log ("-----> i"+i);
      geddy.model.Contact.first({id: params.ids[i]}, function (err, contact){
         if (err)
         {
            throw err;
         }

         self.session.set('currentContact', contact);

         var foundContact = (contact)? true : false;
         var attendee;

         if (foundContact)
         {
            attendee = linkAttendeeToContact(self, foundContact, contact);
         } else {
            self.respond({foundContact: foundContact, contact: contact, attendee: attendee}, {format: 'json'});
         }

     });
    }


   }

   function linkAttendeeToContact(self, foundContact, contact)
   {
      // check that this contact id is not linked to an attendee already
      // if it's not, then create new attendee and save to currentevent also
      // if it is, don't do anything

      //now save attendee to event 
      var currentEvent = self.session.get('currentEvent');

      geddy.model.Attendee.first({contactId: contact.id, eventId: currentEvent.id}, function (err, attendee){
         if (err)
         {
            throw err
         }

         if (!attendee)
         {
            var newAttendee = geddy.model.Attendee.create();
            newAttendee.contactId = contact.id;

            newAttendee.save(function(err, data) {
               if (err) {
                  throw err;
               }

               if (!currentEvent.attendees)
               {
                  currentEvent.attendees = [];
               }

               currentEvent.attendees.push(data.id);

               currentEvent.save(function(err, eventData){
                  if (err) {
                  throw err;
                  }

                  //set new attendee as the currentAttendee
                  self.session.set('currentAttendee', data);
                  self.respond({foundContact: foundContact, contact: contact, attendee: attendee}, {format: 'json'});
               });
            });
         } else {
            console.log ("attendee exists already!");
            self.session.set('currentAttendee', attendee);
            self.respond({foundContact: foundContact, contact: contact, attendee: attendee}, {format: 'json'});
         }

      });
      //var currentAttendee = self.session.get('currentAttendee');
      // var currentEvent = self.session.get('currentEvent');

      // //check to make sure attendee is not in event already
      // if (currentEvent.attendees.indexOf(contactId) == -1)
      // {

      //    // if (!currentAttendee)
      //    // {
      //       var newAttendee = geddy.model.Attendee.create();
      //       newAttendee.contactId = contactId

      //       newAttendee.save(function(err, data) {
      //          if (err) {
      //             throw err;
      //          }

      //          self.session.set('currentAttendee', data);
      //       });
      //    // }
      // }
   }


   this.createNewKOL = function (req, resp, params) {
      var self = this;

      var currentClient = self.session.get('currentClient');

      var callback = function()
      {
         var contactId = self.session.get('savedId');

         self.respond({
            contactId: contactId
         }, {format: 'json'});
      }

      DBUtils.saveObjectWithoutRespond(self, 'Contact', params,callback);
   }


  // Contact Step pages
  this.attendeeStep1 = function (req, resp, params) {
   var self = this;
   
   var result = {};
   result.selectPrompt = constants.SELECT_PROMPT();
   result.currentAttendee = self.session.get('currentAttendee');
   result.currentContact = self.session.get('currentContact');
   result.statusTypes = constants.ATTENDEE_STATUS_TYPE();
   result.attendeeTypes = constants.ATTENDEE_TYPE();
   result.roomTypes = constants.ROOM_TYPES();
   result.searchAttendeesProperties = constants.SEARCH_ATTENDEE_PROPERTIES();
   result.salutations = constants.SALUTATIONS();
   result.suffixes = constants.SUFFIXES();
   result.states = constants.STATES();
   result.stepTitle = createTitle(self.session.get('currentAttendee'));
   result.currentEvent = self.session.get('currentEvent');
   result.addNewAttendee = params.addNewAttendee;

   if (result.currentAttendee && result.currentAttendee.id)
   {
      DBUtils.lockObject(self, result.currentAttendee.id);
   }

    //set all clients list since it's needed when data for a contact is saved...
    geddy.model.Client.all({createdAt: {ne: null}}, {sort: {name: 'asc'}}, function (err, clients) {
         if (err) {
            throw err;
         }
         result.clients = clients
         self.session.set('allClients',clients);
                  
         self.respond( result,
               {format: 'html',template: 'app/views/attendees/attendeeStep1'}
         );
      });
  };

  this.attendeeStep2 = function (req, resp, params) {
    var self = this;
   
    var selectPrompt = constants.SELECT_PROMPT();
    var currentAttendee = self.session.get('currentAttendee');
    var statusTypes = [];
    var attendeeTypes = [];
    var currentClient = self.session.get('currentClient');
    var currentContact = self.session.get('currentContact');
    var phoneTypes = constants.PHONE_NUMBER_TYPES();
    var states = constants.STATES();
    var countries = constants.COUNTRIES();
    var addressTypes = constants.ADDRESS_TYPES();

    self.respond( {
            stepTitle: createTitle(currentAttendee),
            selectPrompt:selectPrompt,
            currentAttendee : currentAttendee,
            statusTypes: statusTypes,
            attendeeTypes: attendeeTypes,
            currentContact: currentContact,
            currentClient: currentClient,
            phoneTypes: phoneTypes,
            states: states,
            countries: countries,
            addressTypes: addressTypes

         },
         {format: 'html',template: 'app/views/attendees/attendeeStep2'}
      );
  };

  this.attendeeStep3 = function (req, resp, params) {
      var self = this;
      var phoneTypes = constants.PHONE_NUMBER_TYPES();
      var states = constants.STATES();
      var selectPrompt = constants.SELECT_PROMPT();
      var currentClient = self.session.get('currentClient');
      var currentContact = self.session.get('currentContact');;
      var currentAttendee = self.session.get('currentAttendee');

      this.respond({
            phoneTypes: phoneTypes,
            selectPrompt:selectPrompt,
            states: states,
            currentAttendee : currentAttendee,
            currentContact: currentContact,
            currentClient: currentClient,         
            stepTitle:createTitle(currentAttendee)
         }, 
         {format: 'html',
         template: 'app/views/attendees/attendeeStep3'}
      );
   };  

   this.attendeeStep4 = function (req, resp, params) {
      var self = this;
      var countries = constants.COUNTRIES();
      var selectPrompt = constants.SELECT_PROMPT();
      var reps = [];
      var currentAttendee = self.session.get('currentAttendee');
      var currentClient = self.session.get('currentClient');
      var currentContact = self.session.get('currentContact');;

      //Get client first, then get each rep from its reps array
      geddy.model.Client.first(currentClient.id, function (err, client) {
         if (err)
         {
            throw err;
         }

         if (currentClient.reps && currentClient.reps.length > 0)
            {
            var totalRepsProcessed = 0;
            var i;
            for (i = 0; i < client.reps.length; i++) {
               geddy.model.Rep.first(client.reps[i], function (err, rep) {
                  if (err)
                  {
                     totalRepsProcessed++;
                  } else {
                     totalRepsProcessed++;

                     reps.push(rep);

                     if (totalRepsProcessed == client.reps.length)
                     {
                        self.respond(
                           {
                              currentAttendee : currentAttendee,
                              currentContact:currentContact,
                              currentClient:currentClient, 
                              selectPrompt: selectPrompt,
                              countries: countries,
                              reps:reps,
                              stepTitle:createTitle(currentAttendee)
                           },
                           {format: 'html',
                           template: 'app/views/attendees/attendeeStep4'}
                        );
                     }
                  }
               });
            };
         } else {
            self.respond(
               {
                  currentAttendee : currentAttendee,
                  currentContact:currentContact,
                  currentClient:currentClient, 
                  selectPrompt: selectPrompt,
                  countries: countries,
                  reps:reps,
                  stepTitle:createTitle(currentAttendee)
               },
               {format: 'html',
               template: 'app/views/attendees/attendeeStep4'}
            );
         }
      });
   };  

   this.attendeeStep5 = function (req, resp, params) {
      var self = this;
      var states = constants.STATES();
      var countries = constants.COUNTRIES();
      var selectPrompt = constants.SELECT_PROMPT();
      var currentAttendee = self.session.get('currentAttendee');
      var currentClient = self.session.get('currentClient');
      var currentContact = self.session.get('currentContact');;

      this.respond({
         selectPrompt:selectPrompt,
         states: states,
         countries: countries,
         currentAttendee : currentAttendee,
         currentContact:currentContact,
         currentClient:currentClient,  
         allClients:self.session.get("allClients"),      
         mode:self.session.get("sessionContactMode"),
         stepTitle:createTitle(currentAttendee)},
          {format: 'html',
         template: 'app/views/attendees/attendeeStep5'}
      );
   };  

  this.search = function (req, resp, params) {
    var $thisRequest = this;
    //var search = require('../helpers/search/search.js');
    var currentEvent = $thisRequest.session.get('currentEvent');
    var searchObject = {id: {$in: currentEvent.attendees}};

    var sort = {
              lastName: 'asc',
              firstName : 'asc'
    }

    search.AdvanceSearch($thisRequest, searchObject, sort, 'attendees', params);
  }

  function createTitle (currentAttendee) {

    var headerTitle = "";

    if(currentAttendee) {
      if(currentAttendee.name){
         headerTitle = currentAttendee.name + ' - ';
      }
    } else {   
      headerTitle = 'New Attendee - '
    }

    return headerTitle;
  }
  
  
};

exports.Attendees = Attendees;
