var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var ModelName= "Client";

var Clients = function () {
	this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

	 this.before(function () { 
	    var self = this;
	    if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
	    {
	    	geddy.globalDb.close();

	        self.respond('false', {format: 'txt'});
	    }
	    else
	    {
	    	self.session.set('activeController', self.name);
	    }
    });


	this.index = function (req, resp, params) {
		console.log(">>>>>>>>> CLIENTS INDEX ");
		var self = this;

		DBUtils.unlockObject(self);

		var result={};

		self.session.set('currentSection', constants.sections.CLIENT_INDEX);

		result.searchClientProperties = constants.SEARCH_CLIENT_PROPERTIES();
		result.selectPrompt = constants.SELECT_PROMPT();

		var clientCollection = geddy.globalDb.collection('clients');
		var pageLimit = geddy.config.pageLimit;
	    var currentPage = 1;
	    var totalPages=0;

	    var sort = {
          name: 'asc'
      	} 

		self.session.set('currentClient', null);

		var searchObject = new Object;

		//geddy.model.Client.all(function(err, clients) {
		clientCollection.find(searchObject).count(function(err, recordCount){	
			if (err) {
				throw err;
			}
      		totalPages = Math.ceil(recordCount/pageLimit);

      		clientCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,clients){
	      		result.currentPage = currentPage;
	        	result.totalPages = totalPages;
	        	result.clients = clients;
				
				self.respond( result,{ type:'Client'});
			});
		});
	};


	this.cleanUp = function(req, resp, params)
	{
		geddy.model.Venue.all(function(err, venues){
			var state = '';
			var allStates =  constants.STATES();
			for (var i = 0; i < venues.length; i++) {
				if (venues[i].addresses && venues[i].addresses.length > 0 && venues[i].addresses[0].state)
				{
					state = venues[i].addresses[0].state.toLowerCase();

					for (var j = 0; j < allStates.length; j++) {
						var allStateName = allStates[j].name.toLowerCase();

						if (allStateName == state)
						{
							venues[i].addresses[0].state = allStates[j].name;
							break;
						}
					};
					//venues[i].addresses[0].state = state.charAt(0).toUpperCase() + state.slice(1);
					venues[i].save(function(err, data) {
						if (err) {
							throw err;
						}
					});
				}
			};
		});
	}

	 this.show = function (req, resp, params) {
	  	var self = this;

	  	var continueProcess = true;

		if (params.id == '1234')
	    {
	      if (self.session.get('currentClient'))
	      {
	        params.id = self.session.get('currentClient').id;
	      } else {
	        continueProcess = false;
	        self.respond('null',{format: 'txt'});
	      }
	    }

	    if (continueProcess)
		{

		  DBUtils.unlockObject(self);

		  var otherParams = new Object();
	      otherParams.displayEdit = true;

		  DBUtils.showObject(self,ModelName,params,constants.sections.CLIENT_DETAILS,otherParams);
		}
	 };


	 this.save = function (req, resp, params) {
	  var self = this;
	  DBUtils.saveObject(self,ModelName,params);
	 };

	
	 this.saveEmbeddedModel = function (req, resp, params) {
	  var self = this;
	  DBUtils.saveObjectEmbeddedModel(self,ModelName,params);
	 };

	
	
	this.deleteEmbeddedModel = function (req, resp, params) {
	  var self = this;
	  DBUtils.deleteEmbeddedModel(self,ModelName,params);
	 };


	this.deleteClients = function (req, resp, params) {
	  var self = this;
	  DBUtils.deleteObject(self,ModelName,params);
	 };

	 this.deleteContactClients = function (req, resp, params) {
	  var self = this;
	  DBUtils.deleteFromArrayByIndex(self,params.ids,self.session.get('currentClient'),"clientContacts");
	 };


	 this.clearCurrentClientId = function (req, resp, params) {
	  var self = this;
	  DBUtils.clearCurrentObject(self,ModelName,params);
	 };

	
	// Client Step pages
	this.clientStep1 = function (req, resp, params) {
		self = this;
		var selectPrompt = constants.SELECT_PROMPT();
		var deliveries = constants.DELIVERIES();
		var compliances = constants.COMPLIANCES();
		var countries = constants.COUNTRIES();
		var states = constants.STATES();
		var addressTypes = constants.CLIENT_ADDRESS_TYPES();
		var currentClient = self.session.get('currentClient');

		if (currentClient && currentClient.id)
		{
			DBUtils.lockObject(self, currentClient.id);
		}

		self.respond( {
			stepTitle:createTitleForClient(currentClient),
			selectPrompt:selectPrompt,
			deliveries:deliveries,
			compliances:compliances,
			countries:countries,
			states:states,
			addressTypes:addressTypes,
			currentClient: currentClient},
			{format: 'html',template: 'app/views/clients/clientStep1'}
		);
	};
	
	this.clientStep2 = function (req, resp, params) {
		self = this;
		var phoneTypes = constants.PHONE_NUMBER_TYPES();
		//var states = constants.STATES();
		//var countries = constants.COUNTRIES();
		//var addressTypes = constants.ADDRESS_TYPES();
		var selectPrompt = constants.SELECT_PROMPT();
		self.respond( {
			stepTitle:createTitleForClient(self.session.get('currentClient')),
			selectPrompt:selectPrompt,
			phoneTypes:phoneTypes,
			currentClient: self.session.get('currentClient')},
			{format: 'html',template: 'app/views/clients/clientStep2'});
	};


	this.contactClientStep = function (req, resp, params) {
		self = this;
		var result ={};
		result.phoneTypes = constants.PHONE_NUMBER_TYPES();
		result.states = constants.STATES();
		result.countries = constants.COUNTRIES();
		result.addressTypes = constants.ADDRESS_TYPES();
		result.selectPrompt = constants.SELECT_PROMPT();
		result.stepTitle = createTitleForClient(self.session.get('currentClient')),
		result.currentContact = null;

		if (params.index)
		{
			result.currentContact = self.session.get('currentClient').clientContacts[params.index];
		}

		self.respond( result,{format: 'html',template: 'app/views/clients/contactClientStep'});
	};

	this.clientStep3 = function (req, resp, params) {
		self = this;
		var selectPrompt = constants.SELECT_PROMPT();
		var salesForceLabelsL1 = constants.SALES_FORCE_LABELS_LEVEL1();
		var salesForceLabelsL2 = constants.SALES_FORCE_LABELS_LEVEL2();
		var salesForceLabelsL3 = constants.SALES_FORCE_LABELS_LEVEL3();
		var medicalAffairsLabels = constants.MEDICAL_AFFAIRS_LABELS();
		var payorLabels = constants.PAYOR_LABELS();

		self.respond( {
			stepTitle:createTitleForClient(self.session.get('currentClient')),
			selectPrompt:selectPrompt, 
			currentClient: self.session.get('currentClient'),
			salesForceLabelsL1: salesForceLabelsL1,
			salesForceLabelsL2: salesForceLabelsL2,
			salesForceLabelsL3: salesForceLabelsL3,
			medicalAffairsLabels: medicalAffairsLabels,
			payorLabels: payorLabels,
			},
			{format: 'html',template: 'app/views/clients/clientStep3'});
	};


	this.search = function (req, resp, params) {
      	var $thisRequest = this;
      	var search = require('../helpers/search/search.js');

    	var sort = {
              name: 'asc'
    	}

      search.AdvanceSearch($thisRequest, null, sort, 'clients',params);
	}


	//////////// Guidelines ///////////////
	this.guidelines = function (req, resp, params) {
		var self = this;

		DBUtils.unlockObject(self);

		self.session.set('currentSection', constants.sections.CLIENT_GUIDELINES);

		self.respond( {
			currentClient: self.session.get('currentClient'),
			},
			{format: 'html',template: 'app/views/clients/guidelines'});
	}

	this.deleteGuidelines = function(req, resp, params)
	{
		var self = this;

		DBUtils.deleteUploadedFiles(self, params.ids, self.session.get('currentClient'), 'guidelines');
		DBUtils.deleteFromArrayByIndex(self, params.ids, self.session.get('currentClient'), 'guidelines');
	}

	//////////// Resources ///////////////
	this.resources = function (req, resp, params) {
		var self = this;

		DBUtils.unlockObject(self);

		var selectPrompt = constants.SELECT_PROMPT();

		self.session.set('currentSection', constants.sections.CLIENT_RESOURCES);

		self.respond( {
			selectPrompt:selectPrompt,
			currentClient: self.session.get('currentClient'),
			},
			{format: 'html',template: 'app/views/resources/clientResources'});
	}

	this.deleteKolResources = function(req, resp, params)
	{
		var self = this;

		DBUtils.deleteUploadedFiles(self, params.ids, self.session.get('currentClient'), 'kolResources');
		DBUtils.deleteFromArrayByIndex(self, params.ids, self.session.get('currentClient'), 'kolResources');
	}

	this.deleteRepResources = function(req, resp, params)
	{
		var self = this;
		DBUtils.deleteUploadedFiles(self, params.ids, self.session.get('currentClient'), 'repResources');
		DBUtils.deleteFromArrayByIndex(self, params.ids, self.session.get('currentClient'), 'repResources');
	}


	this.deleteRSMResources = function(req, resp, params)
	{
		var self = this;
		DBUtils.deleteUploadedFiles(self, params.ids, self.session.get('currentClient'), 'rsmResources');
		DBUtils.deleteFromArrayByIndex(self, params.ids, self.session.get('currentClient'), 'rsmResources');
	}


	function createTitleForClient (currentClient) {
      var headerTitle = "";
      if (currentClient) {
         if (currentClient.name) {
            headerTitle = currentClient.name + ' - ';
         } else {
          headerTitle = 'Event - '
         }
      } else {
         headerTitle = 'New Client - '
      }
      return headerTitle;
   }
};

exports.Clients = Clients;
