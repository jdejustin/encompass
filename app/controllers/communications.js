var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var ModelName;
var fs = require('fs');
var api_key = 'key-0ec2fda357887c7aec58d91bc6f0f239';
var domain = 'encompassmanager.com';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

var Communications = function () {
	this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

   this.before(function () { 
    var self = this;
    if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
    {
   	geddy.globalDb.close();
      self.respond({session:false},{format: 'json'});
    }
    });

	 this.communicationDelivery = function (req, resp, params) {
			var $thisRequest = this;
			$thisRequest.session.set('currentSection', constants.sections.EVENT_COMMUNICATIONS);

			var currentClient = $thisRequest.session.get('currentClient');
			var currentProgram = $thisRequest.session.get('currentProgram');
			var currentEvent = $thisRequest.session.get('currentEvent');

			result = {};
			result.deliveries = [];
			result.currentClient = currentClient;

			 //pagination vars 
			var deliveryCollection = geddy.globalDb.collection('communication_deliveries');
			//var pageLimit = geddy.config.pageLimit;
			var searchObject = {eventId: currentEvent.id};
			//var currentPage = 1;
			//var recordCount=0;
			//var totalPages=0;
			var sort = {
						lastDateSent: 'asc',
			}
			//result.currentPage = currentPage;

			result.currentEvent = currentEvent;
			result.communicationTitle =  currentClient.name +" - " +currentEvent.brand +" - " +currentEvent.presentationTitle;

			deliveryCollection.find(searchObject).toArray(function(err,allDeliveries){
				 if (allDeliveries != null)
				 {
					//recordCount = allDeliveries.length;
					//totalPages = Math.ceil(recordCount/pageLimit);
					result.deliveries = allDeliveries;
				 }

				 //deliveryCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,deliveries){

					//result.totalPages = totalPages;
					//result.deliveries = deliveries;

					$thisRequest.respond( result,{format: 'html',template: 'app/views/communications/communicationDeliveries'}); 
				//});
			});
	 };


	 this.clearCurrentTemplate = function (req, resp, params) {
			var self = this;
			ModelName = 'CommunicationTemplate';
			DBUtils.clearCurrentObject(self,ModelName,params);
	 };

    this.clearCurrentProgramDelivery = function (req, resp, params) {
         var self = this;
         ModelName = 'CommunicationProgramDelivery';
         DBUtils.clearCurrentObject(self,ModelName,params);
    };    

	 this.communicationTemplate = function (req, resp, params) {
      var $thisRequest = this;
      $thisRequest.session.set('currentSection', constants.sections.PROGRAM_COMMUNICATIONS);

      DBUtils.unlockObject($thisRequest);

      var currentProgram = $thisRequest.session.get('currentProgram');
      var currentClient = $thisRequest.session.get('currentClient');


      result = {};
      result.currentProgram = currentProgram;
      result.currentClient = currentClient;
      result.templates = [];
      result.programDeliveries = [];

      //pagination vars 
      var templateCollection = geddy.globalDb.collection('communication_templates');
      var programDeliveriesCollection = geddy.globalDb.collection('communication_program_deliveries');
      //var pageLimit = geddy.config.pageLimit;
      var searchObject = {programId: currentProgram.id};
      /*var currentPage = 1;
      var currentDeliveryPage = 1;
      var recordCount=0;
      var recordDeliveryCount=0;
      var totalPages=0;
      var totalDeliveryPages=0;*/

      var sort = {
      			templateTitle: 'asc',
      }
      /*
      var sortDelivery = {
      			id: 'asc',
      }*/

      result.communicationTitle =  currentClient.name +" - " +currentProgram.programName;
      //result.currentPage = currentPage;

      templateCollection.find(searchObject).toArray(function(err,allTemplates){
         if (allTemplates != null)
         {
        /* recordCount = allTemplates.length;
         totalPages = Math.ceil(recordCount/pageLimit);*/
         }

         //templateCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,templates){

            //result.totalPages = totalPages;
            result.currentClient = currentClient;
            result.templates = allTemplates;

            programDeliveriesCollection.find(searchObject).toArray(function(err,allProgramDeliveries){
            	if (allProgramDeliveries != null)
            	{
            		//recordDeliveryCount = allProgramDeliveries.length;
            		//totalDeliveryPages = Math.ceil(recordDeliveryCount/pageLimit);
            	}

            	//result.totalDeliveryPages = totalDeliveryPages;
            	result.programDeliveries = allProgramDeliveries;
            	$thisRequest.respond( result,{format: 'html',template: 'app/views/communications/communicationTemplates'});
            });
         //});
      });
	};

	this.saveTemplate = function (req, resp, params) {
		var self = this;
		ModelName = "CommunicationTemplate";
		var currentProgram = self.session.get('currentProgram');
		params.programId = currentProgram.id;
		
		DBUtils.saveObjectWithoutRespond(self,ModelName,params,creatingTemplateFile);
	}


	 function creatingTemplateFile(data, self, params){
			//data should be access like data.communicationTemplate 
			var currentProgram =  self.session.get('currentProgram');

			var fse = require('fs-extra');

			var filePath = getTemplateFilePath(currentProgram.id);
			var fileName = data.communicationtemplateId+".html";
			console.log(">>>>>>>>>>>> filePath:" + filePath);
			fse.ensureDirSync(filePath);


			var content = params.content.replace(/.*<body>/im,"");
			content = content.replace(/<\/body>.*/im,"");
 			content  = 	"<html>"+
 						"<head>"+
 						"<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>"+
 						"</head>"+
 						"<body>" +
 							content +
 						"</body>"+
						"</html>";


			fs.writeFile(filePath+'/'+fileName, content, function(err) {
				 if(err) {
						 console.log("Error writing file: "+err);
				 } else {
						 console.log("The file was saved!");
						 //response
						 self.respond(data,{format:'json'});
				 }
			});
	}


	 function openTemplateFile(self,template){
			var currentProgram =  self.session.get('currentProgram');
			var data = null;

			var filePath = getTemplateFilePath(currentProgram.id);
			var fileName = template.id+".html";
			try{
				data = fs.readFileSync(filePath+'/'+fileName, { encoding: 'utf-8' });
		 }catch(ex){

		 }
			//console.log('received data: ' + data);
			return data;
	 }


	 function getDeliveryFileContent(filePath,fileName){
			var data = null;
			
			try{
				data = fs.readFileSync(filePath+'/'+fileName, { encoding: 'utf-8' });
		 }catch(ex){

		 }
			//console.log('received data: ' + data);
			return data;
	 }

	function getTemplateFilePath(programId)
	 {
			var util = require('util');
			var path = require('path');
			var filePath = path.join(geddy.config.CommunicationsFolder + 
			util.format("templates/%s",
				programId
				)
			);
			return filePath;
	 }



	this.editDeliveryCommunication = function (req, resp, params) {
			var $thisRequest = this;

			DBUtils.unlockObject($thisRequest);

			var currentProgram = $thisRequest.session.get('currentProgram');
			var currentClient = $thisRequest.session.get('currentClient');
			var currentEvent = $thisRequest.session.get('currentEvent');

			var currentDelivery = null;
			var templateArray = [];


			result = {};
			result.currentDelivery = currentDelivery;
			result.templatesArray = [];
			result.selectPrompt = constants.SELECT_PROMPT();
			//result.deliverableTypes = constants.DELIVERABLE_TYPES();
			result.toRecepients = constants.DELIVERABLE_TO_RECEPIENTS();
			result.ccRecepients = constants.DELIVERABLE_CC_RECEPIENTS();
			result.attendeeStatus = constants.ATTENDEE_STATUS_TYPE();
			result.speakerStatus = constants.SPEAKER_STATUS_CRG();
			result.speakers = currentEvent.potentialSpeakers;
			result.externalAttachments = [];

			var templateCollection = geddy.globalDb.collection('communication_templates');
			var searchObject = {};
			var searchObject = {programId: currentProgram.id};

			var sort = {
						templateTitle: 'asc',
			}

			templateCollection.find(searchObject).sort(sort).toArray(function(err,allTemplates){
				 if (allTemplates != null &&  typeof allTemplates != "undefined" )
				 {
						result.templatesArray = allTemplates;
				 }
				
				 result.editCommunicationTitle = currentClient.name+ ' - ' + currentProgram.programName;
				 

				 if (params.deliveryId == null || typeof params.deliveryId == "undefined"){
					 //new 
					 result.selectPrompt = constants.SELECT_PROMPT();
					 result.editing = false;
					 result.editor = null;

					 //clear the current delivery
					 $thisRequest.session.set('currentCommunicationDelivery',null); 
					 //$thisRequest.respond( result,{format: 'html',template: 'app/views/communications/editDeliveryCommunication'}); 
					 DBUtils.GetEventAttendees(currentEvent.attendees, $thisRequest.onGetAllAttendees, $thisRequest, result);
				 }else{

					//existing 
					 geddy.model.CommunicationDelivery.first(params.deliveryId,function (err, delivery) {
							if (err)
							{
								throw err;
							}
							///console.log (" >>>>>>>> DELIVERY "+ JSON.stringify(delivery));

							currentDelivery = delivery;
							$thisRequest.session.set('currentCommunicationDelivery',currentDelivery); 
							result.currentDelivery = currentDelivery; 

							//get list of 'clean' attachment names, if they exist
							if (currentDelivery.externalAttachments)
							{
								var i;
								for (i = 0; i < currentDelivery.externalAttachments.length; i++) {
									var attachment = {};
									attachment.filePath = currentDelivery.externalAttachments[i];
									attachment.fileName = attachment.filePath.slice(attachment.filePath.indexOf('_attachment_') + 12);

									result.externalAttachments.push(attachment);
								};
							}

							//find object in collection, if there, then no, it's not editable
							geddy.model.Lock.first({objectId:result.currentDelivery.id}, function(err, lock){
								if (err)
								{
									throw err;
								}

								if (lock)
								{
									result.editing = true;
									result.editor = lock.editorFirstName + ' ' + lock.editorLastName;
								} else {
									result.editing = false;
									result.editor = null;

									DBUtils.lockObject($thisRequest, result.currentDelivery.id);
								}

								DBUtils.GetEventAttendees(currentEvent.attendees, $thisRequest.onGetAllAttendees, $thisRequest, result);
							});
						});
				 }
			});
	}

	this.onGetAllAttendees = function($thisRequest, result)
	{
		//console.log('>>>>>>>>>>>>>>>>>> attendees '+JSON.stringify(result.attendees));
		// this is the response of the edit Delivery communication 
		$thisRequest.respond( result,{format: 'html',template: 'app/views/communications/editDeliveryCommunication'}); 
	}


	this.deleteExternalAttachments = function(req, resp, params)
	{
		var $thisRequest = this;

		// remove the physical file
		var j;
		for (j = 0; j < params.ids.length; j++) {
			var filePath = params.ids[j];

			//remove file
			if (fs.existsSync(filePath))
			{
				fs.unlink(filePath, function (err) {
		  			if (err) throw err;
				});
			}
		}


		//if we're editing a delivery, we need to remove the data from DB as well
		var currentDelivery = $thisRequest.session.get('currentCommunicationDelivery');

		if (currentDelivery)
		{
			var i;
			for (i = currentDelivery.externalAttachments.length - 1; i >=0; i--) {
				for (j = 0; j < params.ids.length; j++) {
					if (params.ids[j] == currentDelivery.externalAttachments[i])
					{
						currentDelivery.externalAttachments.splice(i, 1);

						currentDelivery.save(function (err, data){
							if (err)
							{
								throw err;
							}
						})
						
						break;
					}
				};

			};
		} 

		$thisRequest.respond(params.ids, {format: 'json'});
	}


	this.editProgramDeliveryCommunication = function (req, resp, params) {
			var $thisRequest = this;

			var currentProgram = $thisRequest.session.get('currentProgram');
			var currentClient = $thisRequest.session.get('currentClient');

			var currentDelivery = null;
			var templateArray = [];

			result = {};
			result.currentDelivery = currentDelivery;
			result.templatesArray = [];
			result.selectPrompt = constants.SELECT_PROMPT();
			result.deliverableTypes = constants.DELIVERABLE_TYPES();


			var templateCollection = geddy.globalDb.collection('communication_templates');
			var searchObject = {};
			var searchObject = {programId: currentProgram.id};

			var sort = {
				templateTitle: 'asc',
			}

			templateCollection.find(searchObject).sort(sort).toArray(function(err,allTemplates){
				 if (allTemplates != null)
				 {
						result.templatesArray = allTemplates;
				 }

				 if (params.programDeliveryId == null || typeof params.programDeliveryId == "undefined"){
					 //new 
					 result.selectPrompt = constants.SELECT_PROMPT();
					 result.editing = false;
					 result.editor = null;

					 //clear the current program delivery
					 $thisRequest.session.set('currentCommunicationProgramDelivery',null); 
					 $thisRequest.respond( result,{format: 'html',template: 'app/views/communications/editProgramDeliveryCommunication'}); 
				 }else{

					//existing 
					 geddy.model.CommunicationProgramDelivery.first(params.programDeliveryId,function (err, delivery) {
							if (err)
							{
								throw err;
							}
							//console.log (" >>>>>>>> PRogram DELIVERY "+ JSON.stringify(delivery));

							currentDelivery = delivery;
							$thisRequest.session.set('currentCommunicationProgramDelivery',currentDelivery); 
							result.currentDelivery = currentDelivery; 

							//find object in collection, if there, then no, it's not editable
							geddy.model.Lock.first({objectId:result.currentDelivery.id}, function(err, lock){
								if (err)
								{
									throw err;
								}

								if (lock)
								{
									result.editing = true;
									result.editor = lock.editorFirstName + ' ' + lock.editorLastName;
								} else {
									result.editing = false;
									result.editor = null;

									DBUtils.lockObject($thisRequest, result.currentDelivery.id);
								}

								$thisRequest.respond( result,{format: 'html',template: 'app/views/communications/editProgramDeliveryCommunication'}); 
							});
						});
				 }
			});
	}


	 this.editTemplateCommunication = function (req, resp, params) {
			var $thisRequest = this;
			var currentTemplate = null;
			var currentFile = null;
			//console.log(" >>>>>>>>>>>>>> params "+ JSON.stringify(params));

			var currentProgram = $thisRequest.session.get('currentProgram');
			var currentClient = $thisRequest.session.get('currentClient');

			var result = {};

			result.editCommunicationTitle = currentClient.name+ ' - ' + currentProgram.programName;
			if (params.templateId == null || typeof params.templateId == 'undefined')
			{
				 //new 
						$thisRequest.session.set('currentCommunicationTemplate',currentTemplate); 
						result.currentTemplate = currentTemplate;
						result.currentFile = currentFile;
						result.editing = false;
						result.editor = null;

						$thisRequest.respond( result,{format: 'html',template: 'app/views/communications/editTemplateCommunication'}); 
			}
			else
			{
				 //existing 
				 geddy.model.CommunicationTemplate.first(params.templateId,function (err, template) {
						if (err)
						{
							throw err;
						}
						currentTemplate = template
						var content = openTemplateFile($thisRequest,template);
						content = content.replace(/.*<body>/im,"");
						content = content.replace(/<\/body>.*/im,"");
						result.currentFile = content;


						$thisRequest.session.set('currentCommunicationTemplate',currentTemplate); 
						result.currentTemplate = currentTemplate;

						//find object in collection, if there, then no, it's not editable
						geddy.model.Lock.first({objectId:result.currentTemplate.id}, function(err, lock){
							if (err)
							{
								throw err;
							}

							if (lock)
							{
								result.editing = true;
								result.editor = lock.editorFirstName + ' ' + lock.editorLastName;
							} else {
								
								result.editing = false;
								result.editor = null;

								DBUtils.lockObject($thisRequest, result.currentTemplate.id);
							}
						
							$thisRequest.respond( result,{format: 'html',template: 'app/views/communications/editTemplateCommunication'}); 
						});
				 }); 
			}
	 }


	this.deleteTemplates = function (req, resp, params) {
         var $thisRequest = this;
         var currentProgram =  $thisRequest.session.get('currentProgram');
         var filePath = getTemplateFilePath(currentProgram.id);
         ModelName = "CommunicationTemplate";
         
         var i;
         for (i= 0; i < params.ids.length; i++) {
             var fileName = params.ids[i]+".html";
             console.log('erase '+filePath+'/'+fileName);
             try{
                  fs.unlinkSync(filePath+'/'+fileName);
             }
             catch(err){}
         }
         DBUtils.deleteObject($thisRequest,ModelName,params);
    }


    this.deleteProgramDeliveries = function (req, resp, params) {
			var $thisRequest = this;
			var currentProgram =  $thisRequest.session.get('currentProgram');
			var filePath = getTemplateFilePath(currentProgram.id);
			ModelName = "CommunicationProgramDelivery";
			
			
			var i;
			for (i= 0; i < params.ids.length; i++) {
				 var fileName = params.ids[i]+".html";
				 console.log('erase '+filePath+'/'+fileName);
				 try{
						fs.unlinkSync(filePath+'/'+fileName);
				 }
				 catch(err){}
			}
			DBUtils.deleteObject($thisRequest,ModelName,params);
	 }


	this.deleteDeliveries = function (req, resp, params) {
		var $thisRequest = this;
		var currentProgram =  $thisRequest.session.get('currentProgram');
		var filePath = getTemplateFilePath(currentProgram.id);
		ModelName = "CommunicationDelivery";

		var i;
		for (i= 0; i < params.ids.length; i++) {
			var dirName = params.ids[i];
			console.log('erase '+dirName);

			var filePath = getRenderFilePath(dirName);
			$thisRequest.deleteDir($thisRequest,filePath);
		}

		DBUtils.deleteObject($thisRequest,ModelName,params);
	}

	this.saveDelivery = function (req, resp, params) {
		var $thisRequest = this;
		ModelName = "CommunicationDelivery";
		var currentEvent = $thisRequest.session.get('currentEvent');
		params.eventId = currentEvent.id;

		//get mail template so we can store the tile for display purposes
		geddy.model.CommunicationTemplate.first(params.mailTemplate, function(err, mailTemplate){
			if (err)
			{
				throw err;
			}

			params.templateTitle = mailTemplate.templateTitle;
			
			//console.log(">>>>>>>>>>>>>>>>> save Delivery PARAMS "+ JSON.stringify(params));
			DBUtils.saveObjectWithoutRespond($thisRequest,ModelName,params,saveDeliveryCallback);
		});
	}


	function saveDeliveryCallback(result, $thisRequest, params)
	{
		var communicationDeliveryID = result.communicationdeliveryId;

		console.log("RESULT SAVECALLBACK CommunicationDeliveryID>>>>>" + JSON.stringify(result));

		geddy.model.CommunicationDelivery.first({ id: communicationDeliveryID },
			function (err, communicationDelivery) {
				if (err){
					console.log("<<<<<<<<<<<<<<err>>>>>" + JSON.stringify(err));
				}
				if (communicationDelivery) {
					if (communicationDelivery.individualEmails)
					{
						$thisRequest.ProcessCommunicationObject = $thisRequest.createIndividualCommunicationFiles;
					}
					else
					{
						$thisRequest.ProcessCommunicationObject = $thisRequest.createCommunicationFiles;
					}
					$thisRequest.BuildCommunicationsObject($thisRequest,communicationDelivery);
				}else{
					console.log("<<<<<<<<<<<<<<CommunicationDelivery NOT FOUND>>>>>");
					$thisRequest.respond({}, { format: 'json' });
				}
		}); 
	}


	this.saveProgramDelivery = function (req, resp, params) {
		var self = this;
		ModelName = "CommunicationProgramDelivery";
		var currentProgram = self.session.get('currentProgram');
		params.programId = currentProgram.id;
		
		DBUtils.saveObjectWithoutRespond(self,ModelName,params,savedDelivery);
	}


	this.saveProgramDeliveryTemplates = function (req, resp, params)
	{
		var self = this;

		var currentCommunicationProgramDelivery = self.session.get('currentCommunicationProgramDelivery');

		currentCommunicationProgramDelivery.attachmentTemplates = params.attachmentTemplates;

		currentCommunicationProgramDelivery.save(function(err, data){
			if (err)
			{
				throw err;
			}

			self.respond(true,{format:'json'});
		});

	}


	function savedDelivery(data, self, params){
		 self.respond(data,{format:'json'});
	}


	 this.sendTestSample = function (req, resp, params) {
		var $thisRequest = this;

		//get the delivery 
		geddy.model.CommunicationDelivery.first(params.deliveryId,function (err, delivery) {

			var bodyId = delivery.mailTemplate;
			var templatesIds = delivery.attachmentTemplates;

			if (delivery.individualEmails)
			{

				delivery.targetEmail = params.email;

				$thisRequest.ProcessCommunicationObject = $thisRequest.finalizeSendTestSample;
			
				$thisRequest.BuildCommunicationsObject($thisRequest,delivery);

				$thisRequest.respond(true ,{format:'json'});

			}
			else
			{
				//get the attachment files
				var attachmentsFiles = [];
    			var currentAttachment;
				var filePath = getRenderFilePath(params.deliveryId);
				if (templatesIds){
					for(var i=0; i < templatesIds.length;i++){
						var attachTemplateId = templatesIds[i];
						var fileName = attachTemplateId+".pdf";
						currentAttachment = {};
		        		currentAttachment.filePath = filePath+'/'+fileName;
		        		currentAttachment.subject = fileName;
		        		attachmentsFiles.push(currentAttachment);
					}
				}

				if (delivery.externalAttachments)
				{
					for(var i=0; i < delivery.externalAttachments.length;i++){
						currentAttachment = {};
		        		currentAttachment.filePath = delivery.externalAttachments[i];
		        		attachmentsFiles.push(currentAttachment);
					}
				}

				//general email 
		    	$thisRequest.sendCommunication({
					bodyFilename:filePath+"/"+bodyId+".html",
					subject:'Sample Communication',//communicationObject.mailTemplateInfo.subject,
					attachmentsFiles: attachmentsFiles,
					to: params.email,
					from: $thisRequest.session.get('currentClient').email
				});

				//console.log(">>>>>>>>>>>>>>>>> save Delivery PARAMS "+ JSON.stringify(params));
				$thisRequest.respond(result,{format:'json'});
				
			}
		});
	}

	this.finalizeSendTestSample = function(response, communicationObject)
	{
		//for individual emails 
		var sift = require('sift');

	   	var mail = [];
	   	var bodyFileName;
  		var filePath = getRenderFilePath(communicationObject.communicationDelivery.id);
  		var mailTemplateId = communicationObject.communicationDelivery.mailTemplate;
  		var targetEmail = communicationObject.communicationDelivery.targetEmail;

  		var plainCommunicationObject = response.getPlainCommunicationObject(communicationObject);

		switch(communicationObject.communicationDelivery.toRecepients[0])
		{
			case "Client":
				var currentClient = response.session.get('currentClient');
	   			bodyFileName = filePath+"/client/"+currentClient.id+"/"+mailTemplateId+".html"

	   			//get the attachment files 
				var attachmentsFiles = [];
		    	var currentAttachment;
				if (communicationObject.attachmentTemplates){
					for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
						var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.id;
						var fileName = attachTemplateId+".pdf";
						currentAttachment = {};
		        		currentAttachment.filePath = filePath+"/client/"+currentClient.id+"/"+fileName;
		        		currentAttachment.subject = communicationObject.attachmentTemplates[i].templateInfo.subject;
		        		attachmentsFiles.push(currentAttachment);
					}
				}

				if (communicationObject.externalAttachments)
				{
					for(var i=0; i < communicationObject.externalAttachments.length;i++){
						currentAttachment = {};
		        		currentAttachment.filePath = communicationObject.externalAttachments[i];
		        		attachmentsFiles.push(currentAttachment);
					}
				}

				//send mail
				response.sendCommunication({
			      bodyFilename:bodyFileName,
			      subject:communicationObject.mailTemplateInfo.subject,
			      attachmentsFiles: attachmentsFiles,
			      to: targetEmail,
			      from: 'test@encompasmanager.com'
				});
				break;
     		case "Attendees":
     			 var attendeesRecepients = communicationObject.communicationDelivery.attendeesRecepients;

   				bodyFileName = filePath+"/attendees/"+attendeesRecepients[0]+"/"+mailTemplateId+".html"
   				
   				//get the attachment files 
   				var attachmentsFiles = [];
		    	var currentAttachment;
				if (communicationObject.attachmentTemplates){
					for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
						var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.id;
						var fileName = attachTemplateId+".pdf";
						currentAttachment = {};
		        		currentAttachment.filePath = filePath+"/attendees/"+attendeesRecepients[0]+"/"+fileName;
		        		currentAttachment.subject = communicationObject.attachmentTemplates[i].templateInfo.subject;
		        		attachmentsFiles.push(currentAttachment);
					}
				}

				if (communicationObject.externalAttachments)
				{
					for(var i=0; i < communicationObject.externalAttachments.length;i++){
						currentAttachment = {};
		        		currentAttachment.filePath = communicationObject.externalAttachments[i];
		        		attachmentsFiles.push(currentAttachment);
					}
				}

  				//send mail
  				response.sendCommunication({
				      bodyFilename:bodyFileName,
				      subject:communicationObject.mailTemplateInfo.subject,
				      attachmentsFiles: attachmentsFiles,
				      to: targetEmail,
				      from: 'test@encompasmanager.com'
				});
				break;
     		case "Speakers":
     			var speakersRecepients = communicationObject.communicationDelivery.speakersRecepients;
   				bodyFileName = filePath+"/speaker/"+ speakersRecepients[0] +"/"+mailTemplateId+".html"
   				//get the attachment files 
   				var attachmentsFiles = [];
		    	var currentAttachment;
				if (communicationObject.attachmentTemplates){
					for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
						var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.id;
						var fileName = attachTemplateId+".pdf";
						currentAttachment = {};
		        		currentAttachment.filePath = filePath+"/speaker/"+speakersRecepients[0]+"/"+fileName;
		        		currentAttachment.subject = communicationObject.attachmentTemplates[i].templateInfo.subject;
		        		attachmentsFiles.push(currentAttachment);
					}
				}

				if (communicationObject.externalAttachments)
				{
					for(var i=0; i < communicationObject.externalAttachments.length;i++){
						currentAttachment = {};
		        		currentAttachment.filePath = communicationObject.externalAttachments[i];
		        		attachmentsFiles.push(currentAttachment);
					}
				}
		    	
  				//send mail
  				response.sendCommunication({
				      bodyFilename:bodyFileName,
				      subject:communicationObject.mailTemplateInfo.subject,
				      attachmentsFiles: attachmentsFiles,
				      to: targetEmail,
				      from: 'test@encompasmanager.com'
				});
				break;
     		case "Field Force":
   				bodyFileName = filePath+"/fieldforce/"+ communicationObject.RepRequestingInfo.id +"/"+mailTemplateId+".html"
					//get the attachment files 
   				var attachmentsFiles = [];
		    	var currentAttachment;
				if (communicationObject.attachmentTemplates){
					for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
						var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.id;
						var fileName = attachTemplateId+".pdf";
						currentAttachment = {};
		        		currentAttachment.filePath = filePath+"/fieldforce/"+communicationObject.RepRequestingInfo.id+"/"+fileName;
		        		currentAttachment.subject = communicationObject.attachmentTemplates[i].templateInfo.subject;
		        		attachmentsFiles.push(currentAttachment);
					}
				}

				if (communicationObject.externalAttachments)
				{
					for(var i=0; i < communicationObject.externalAttachments.length;i++){
						currentAttachment = {};
		        		currentAttachment.filePath = communicationObject.externalAttachments[i];
		        		attachmentsFiles.push(currentAttachment);
					}
				}
		    	
   				//send mail
   				response.sendCommunication({
				      bodyFilename:bodyFileName,
				      subject:communicationObject.mailTemplateInfo.subject,
				      attachmentsFiles: attachmentsFiles,
				      to: targetEmail,
				      from: 'test@encompasmanager.com'
				});
				break;
     		case "RSM":
   				bodyFileName = filePath+"/rsm/"+ communicationObject.RSMInfo.id +"/"+mailTemplateId+".html"
   				//get the attachment files 
   				var attachmentsFiles = [];
		    	var currentAttachment;
				if (communicationObject.attachmentTemplates){
					for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
						var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.id;
						var fileName = attachTemplateId+".pdf";
						currentAttachment = {};
		        		currentAttachment.filePath = filePath+"/rsm/"+communicationObject.RSMInfo.id+"/"+fileName;
		        		currentAttachment.subject = communicationObject.attachmentTemplates[i].templateInfo.subject;
		        		attachmentsFiles.push(currentAttachment);
					}
				}

				if (communicationObject.externalAttachments)
				{
					for(var i=0; i < communicationObject.externalAttachments.length;i++){
						currentAttachment = {};
		        		currentAttachment.filePath = communicationObject.externalAttachments[i];
		        		attachmentsFiles.push(currentAttachment);
					}
				}

   				//send mail
   				response.sendCommunication({
				      bodyFilename:bodyFileName,
				      subject:communicationObject.mailTemplateInfo.subject,
				      attachmentsFiles: attachmentsFiles,
				      to: targetEmail,
				      from: 'test@encompasmanager.com'
				});
			break;
		}
	}

	this.downloadDelivery = function (req, resp, params) {
		var $thisRequest = this;
		var nodeZip = require('node-zip');
		var result = {};
		var fs = require("fs");
		var fse = require('fs-extra');

		var zip = new nodeZip();


		var files=[];
		var rendererFilePath = getRenderFilePath(params.deliveryId);
		if( fs.existsSync(rendererFilePath) ) {
    		files = $thisRequest.getFilesRecursiveByExt('pdf',$thisRequest,rendererFilePath);
    	}
    	//console.log ("RENDERER FILES OUTSIDE");
      files.forEach(function(file,index){
	   	//console.log (JSON.stringify(file));
	   	var fileContent = fs.readFileSync(rendererFilePath+'/'+file);
			zip.file(file,fileContent);
	   });


		//zip.file("nested/hello.txt", "Hello World\n");
		//console.log(" >>>>>>>>> download delivery  ... params "+JSON.stringify(params));    
		
		var data = zip.generate({base64:false,compression:'DEFLATE'});
		//console.log(" >>>>>>>>> Data  ...  "+data);    
		var filePath ="private/temp/"
		fse.ensureDirSync(filePath);
		var deliveryName = params.deliveryId+".zip";
		fs.writeFileSync(filePath+deliveryName, data, 'binary');


		resp.resp.setHeader('Content-Disposition', 'attachment; filename="communicationFiles.zip"');
		resp.sendFile(filePath+deliveryName); 

	}


	 this.editDeliveryFiles = function (req, resp, params) {
		var $thisRequest = this;
		var result = {};
		result.currentFile = null;

		var filePath = getRenderFilePath(params.deliveryId);

    	var files = [];
    	if( fs.existsSync(filePath) ) {
    		files = $thisRequest.getFilesRecursiveByExt('html',$thisRequest,filePath);
      /*  var filesInFolder = fs.readdirSync(filePath);

        for(var i in filesInFolder) {
				if(filesInFolder[i].substr(-5) === '.html') { //do something 
					//files.splice(i, 1);
					files.push(filesInFolder[i]);
	            ;
				}
		  	}
		*/
			/*console.log ("FILES OUTSIDE");
	      files.forEach(function(file,index){
		   	console.log (JSON.stringify(file));
		   });*/
     	}

		//existing 
		geddy.model.CommunicationDelivery.first(params.deliveryId,function (err, delivery) {
			if (err)
			{
				throw err;
			}
			console.log (" >>>>>>>> DELIVERY "+ JSON.stringify(delivery));


			//existing 
			geddy.model.CommunicationTemplate.first(delivery.mailTemplate,function (err, template) {
				if (err)
				{
					throw err;
				}
				console.log (" >>>>>>>> TEMPLATE "+ JSON.stringify(template));


				//var currentDelivery = self.session.get("CommunicationDelivery")
				$thisRequest.session.set("currentDeliveryId",params.deliveryId);
				$thisRequest.session.set("currentDeliveryFiles",files);
				//read the first file 
				result.currentFile = getDeliveryFileContent(filePath,files[0]);
				result.totalFiles = files.length;
				result.currentIndex = 1;
				result.subject = template.subject;

				$thisRequest.respond( result,{format: 'html',template: 'app/views/communications/editDeliveryFiles'});
			});
		});
	}

	this.saveDeliveryFile = function (req, resp, params) {
		var HTML2PDF = require('../helpers/pdf/html2pdf.js');   
		var $thisRequest = this;
		console.log(" >>>>>>>>> send delivery  ... params "+JSON.stringify(params));    
		//console.log
		var currentDeliveryId = $thisRequest.session.get('currentDeliveryId');
		var files = $thisRequest.session.get('currentDeliveryFiles');
		var filePath = getRenderFilePath(currentDeliveryId);
		$thisRequest.saveToFile(filePath,files[parseInt(params.fileIndex)-1],params.fileContent);
		HTML2PDF.convert(filePath+'/'+files[parseInt(params.fileIndex)-1],{});

		$thisRequest.respond(result,{format:'json'});
	}  


	this.getFileContent = function (req, resp, params) {
		var $thisRequest = this;
		//console.log(" >>>>>>>>> GetFile COntent  ... params "+JSON.stringify(params));    
		var currentDeliveryId = $thisRequest.session.get('currentDeliveryId');
		var files = $thisRequest.session.get('currentDeliveryFiles');
		var filePath = getRenderFilePath(currentDeliveryId);
		console.log("index " + params.contentIndex + " File Path  "+ filePath + " file "+files[params.contentIndex]);

		result = getDeliveryFileContent(filePath,files[parseInt(params.contentIndex)-1]);
		console.log(" RESULT "+ JSON.stringify(result))
		$thisRequest.respond(result,{format:'json'});
	}

	this.getFilesRecursiveByExt = function (ext,self,folder,path) {
		var fileContents = fs.readdirSync(folder);
		var fileTree = [];
		var stats;
		 
		fileContents.forEach(function (fileName) {
			stats = fs.lstatSync(folder + '/' + fileName);
			 
			if (stats.isDirectory()) {
				/*fileTree.push({
					name: stats+"/"+fileName,
					children: self.getFilesRecursiveByExt(ext, self, folder + '/' + fileName)
				});*/
					//fileTree.push(fileName+"/"+self.getFilesRecursiveByExt(ext, self, folder + '/' + fileName));
					console.log("was folder "+ fileName);
					var allFileName = fileName;
					if(path != "" && typeof path !="undefined")
					{
						allFileName = path+"/"+fileName;
					}
					fileTree = fileTree.concat(self.getFilesRecursiveByExt(ext, self,folder + '/' + fileName,allFileName) );
			} else {
				
				if(fileName.substr(fileName.indexOf(".")+1,fileName.length) === ext)
				{
					var allFileName = fileName;
					if(path != "" && typeof path !="undefined")
					{
						allFileName = path+"/"+fileName;
					}
					fileTree.push(allFileName);
				}
			}
		});
		return fileTree;
	};

	this.sendAutomaticEventRequestNotification = function (req, resp, params) {
		var $thisRequest = this;
		var result = {};
		var eventId = params.eventId;
		//var currentProgram = params.currentProgram; //$thisRequest.session.get('currentProgram');
		var deliveryType =  params.deliveryType; //"Program Request Submission";
		var programId;

		///search 
		//if deliveryType is not submission i have to find the programid 
		if (params.deliveryType != constants.PROGRAM_REQUEST_SUBMISSION && params.deliveryType != constants.EVENT_REGISTRATION)
		{
			//find the program ID from the event ....
			geddy.model.Event.first({id: eventId},
				function (err, event) {
					if (err){
						console.log("<<<<<<<<<<<<<<err>>>>>" + JSON.stringify(err));
					}
					if (event) {
						sendEventRequestNotification($thisRequest, eventId, event.programId, deliveryType);
					}
				}
			);
		}
		else
		{
			programId = params.programId;

			var registeredAttendeeEmail;
			if (params.registeredAttendeeEmail)
			{
				registeredAttendeeEmail = params.registeredAttendeeEmail;
			}

			sendEventRequestNotification($thisRequest, eventId, programId, deliveryType, registeredAttendeeEmail);
		}
	}

	function sendEventRequestNotification($thisRequest, eventId, programId, deliveryType, registeredAttendeeEmail){
		console.log("eventId>>>>>" + JSON.stringify(eventId));
		geddy.model.CommunicationProgramDelivery.first({ programId: programId, deliveryType: deliveryType },
			function (err, communicationProgramDelivery) {
				if (err){
					console.log("<<<<<<<<<<<<<<err>>>>>" + JSON.stringify(err));
				}
				if (communicationProgramDelivery) {
					//$thisRequest.ProcessCommunicationObject = $thisRequest.createCommunicationFiles;
       				$thisRequest.ProcessCommunicationObject = $thisRequest.createAutomaticCommunicationFiles;
					$thisRequest.BuildAutomaticCommunicationsObject($thisRequest, eventId, communicationProgramDelivery, registeredAttendeeEmail);
				}else{
					console.log("<<<<<<<<<<<<<<communicationProgramDelivery NOT FOUND>>>>>");
					$thisRequest.respond({}, { format: 'json' });
				}
		});
	}

	this.clearCurrentDeliveryFiles = function(req,resp,params)
	{
		var $thisRequest = this;
		$thisRequest.session.set("currentDeliveryId",null);
		$thisRequest.session.set("currentDeliveryFiles",null);
		$thisRequest.respond(true, {
			format: 'json'
		});
	}

	this.sendDelivery = function (req, resp, params) {
		var $thisRequest = this;
		console.log(" >>>>>>>>> send delivery  ... params "+JSON.stringify(params));    
		//console.log(">>>>>>>>>>>>>>>>> save Delivery PARAMS "+ JSON.stringify(params));

		//get the delivery 
		geddy.model.CommunicationDelivery.first(params.deliveryId,function (err, communicationDelivery) {
			if (communicationDelivery) {
	 			$thisRequest.ProcessCommunicationObject = $thisRequest.proccessSendingEventCommunication;
				$thisRequest.BuildCommunicationsObject($thisRequest,communicationDelivery);

				communicationDelivery.lastDateSent = new Date();

				communicationDelivery.save(function(err, data){
					if (err)
					{
						throw err;
					}
				});

				$thisRequest.respond(true ,{format:'json'});
			}else{
				console.log("<<<<<<<<<<<<<<CommunicationDelivery NO FOUND>>>>>");
				$thisRequest.respond({}, { format: 'json' });
			}
		});
	}


/*******************************/
	//Pointer to ProcessFunction
	var ProcessCommunicationObject = null;


  function GetAttendeesEmailList(response,communicationObject){
    var result = [];
    var attendeesInfo = communicationObject.attendeesInfo;
    if (attendeesInfo){
      for (var i=0;i < attendeesInfo.length;i++){
        var contactInfo = attendeesInfo[i].contactInfo;
        if (contactInfo && contactInfo.defaultEmail){
          result.push(contactInfo.defaultEmail);
        }
      }
    }
    return result;
  }


  function GetSpeakersEmailList(response,communicationObject){
    var result = [];
    var speakersInfo = communicationObject.SpeakersInfo
    if (speakersInfo){
      for (var i=0;i < speakersInfo.length;i++){
        var contactInfo = speakersInfo[i].contactInfo;
        if (contactInfo && contactInfo.defaultEmail){
          result.push(contactInfo.defaultEmail);
        }
      }
    }
    return result;
  }


  function GetSpeakersOfficeEmailList(response,communicationObject){
    var result = [];
    var speakersInfo = communicationObject.SpeakersInfo;
    if (speakersInfo){
      for (var i=0;i < speakersInfo.length;i++){
	if (communicationObject.communicationDelivery.speakersRecepients && communicationObject.communicationDelivery.speakersRecepients.length > 0)
      	{      
	   if (communicationObject.communicationDelivery.speakersRecepients.indexOf(speakersInfo[i].contactId) != -1)
      	   {
	      	if (speakersInfo[i].contactInfo && speakersInfo[i].contactInfo.officeStaffContact && speakersInfo[i].contactInfo.officeStaffContact.length > 0)
	      	{
		        var contactInfo = speakersInfo[i].contactInfo.officeStaffContact[0];
		        if (contactInfo && contactInfo.email){
		          result.push(contactInfo.email);
		        }
		    }
		}
           }
	}
    }
    return result;

    
  }



  function GetClientContactEmailList(response){
    var currentClient = response.session.get('currentClient');
    var result = [];

    if (currentClient && currentClient.clientContacts){
      for(var i=0; i < currentClient.clientContacts.length; i++){
        if (currentClient.clientContacts[i].emailAddresses){
          for(var j=0; j< currentClient.clientContacts[i].emailAddresses.length;j++){
            var currentClientEmail = currentClient.clientContacts[i].emailAddresses[j].email;
            if (currentClientEmail){
              result.push(currentClientEmail);
            }
          }
        }
      }
    }
    return result;    
  }

this.proccessSendingEventCommunication = function(response,communicationObject){

   communicationObject.AttendeesEmails = GetAttendeesEmailList(response,communicationObject);
   communicationObject.RepEmail = communicationObject.RepRequestingInfo.defaultEmail;

   if (communicationObject.RSMInfo)
   	communicationObject.RSMEmail = communicationObject.RSMInfo.defaultEmail;

   communicationObject.ClientEmails = GetClientContactEmailList(response);
   communicationObject.SpeakersEmails = GetSpeakersEmailList(response,communicationObject);
   communicationObject.SpeakersOfficeEmails = GetSpeakersOfficeEmailList(response,communicationObject);

	var currentClient = response.session.get('currentClient');

	var includeOfficeContact = false;

   var mailTemplateId = communicationObject.communicationDelivery.mailTemplate;
   if (communicationObject.communicationDelivery.individualEmails)
	{
		var sift = require('sift');


   		var mail = [];
   		var bodyFileName;
  		var filePath = getRenderFilePath(communicationObject.communicationDelivery.id);

  		var ccMailList =[];
  		if (communicationObject.communicationDelivery.ccRecepients)
  		{
			for(var i=0; i < communicationObject.communicationDelivery.ccRecepients.length;i++){
				switch(communicationObject.communicationDelivery.ccRecepients[i])
				{
					case "Field Force":
						ccMailList.push(communicationObject.RepEmail);
					break;
					case "RSM":
						ccMailList.push(communicationObject.RSMEmail);
					break;
					case "Client":
						ccMailList = ccMailList.concat(communicationObject.ClientEmails);
					break;
					case "CRG Program Manager":
						ccMailList.push(communicationObject.CRGManagerInfo.email);
						ccMailList.push(communicationObject.CRGEventManagerInfo.email);
					break;
					case "Speaker's Office Contact":
						includeOfficeContact = true;
					break;
	        	}
	       }
	   }

	    //process any additional emails
	    if (communicationObject.communicationDelivery.additionalEmails && communicationObject.communicationDelivery.additionalEmails.length > 0 && communicationObject.communicationDelivery.additionalEmails[0])
	    {
	    	if (communicationObject.communicationDelivery.additionalEmails[0].indexOf(','))
	    	{
	    		var emailsAddresses = communicationObject.communicationDelivery.additionalEmails[0].split(',');
       		
	       		for (var i = 0; i < emailsAddresses.length; i++) {
	       			ccMailList.push(emailsAddresses[i]);
	       		}
	       	} else {
	       		ccMailList.push(communicationObject.communicationDelivery.additionalEmails[0]);
	       	}
	    }


		switch(communicationObject.communicationDelivery.toRecepients[0])
		{
			case "Client":
   			bodyFileName = filePath+"/client/"+currentClient.id+"/"+mailTemplateId+".html"

   			//get the attachment files 
				var attachmentsFiles = [];
		    	var currentAttachment;
				if (communicationObject.attachmentTemplates){
					for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
						var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
						var fileName = attachTemplateId+".pdf";
						currentAttachment = {};
		        		currentAttachment.filePath = filePath+"/client/"+currentClient.id+"/"+fileName;
		        		currentAttachment.subject = communicationObject.attachmentTemplates[i].templateInfo.subject;
		        		attachmentsFiles.push(currentAttachment);
					}
				}

				if (communicationObject.externalAttachments)
				{
					for(var i=0; i < communicationObject.externalAttachments.length;i++){
						currentAttachment = {};
		        		currentAttachment.filePath = communicationObject.externalAttachments[i];
		        		attachmentsFiles.push(currentAttachment);
					}
				}

				for (var i=0;i < currentClient.clientContacts.length;i++){
     				if (currentClient.clientContacts[i] && currentClient.clientContacts[i].length > 0 && currentClient.clientContacts[i].emailAddresses
     					&& currentClient.clientContacts[i].emailAddresses.length > 0 && currentClient.clientContacts[i].emailAddresses[0].email){
       				//send mail
       				response.sendCommunication({
					      bodyFilename:bodyFileName,
					      subject:communicationObject.mailTemplateInfo.subject,
					      attachmentsFiles: attachmentsFiles,
					      to: currentClient.clientContacts[i].emailAddresses[0].email,
					      cc: ccMailList,
					      from: currentClient.email
						});
       			}
       		}
			break;
     		case "Attendees":
     			var attendeesRecepients = communicationObject.communicationDelivery.attendeesRecepients;

     			for (var j=0;j < attendeesRecepients.length;j++){
	   				bodyFileName = filePath+"/attendees/"+attendeesRecepients[j]+"/"+mailTemplateId+".html"
	   				
	   				//get the attachment files 
	   				var attachmentsFiles = [];
				    	var currentAttachment;
						if (communicationObject.attachmentTemplates){
							for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
								var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
								var fileName = attachTemplateId+".pdf";
								currentAttachment = {};
				        		currentAttachment.filePath = filePath+"/attendees/"+attendeesRecepients[j]+"/"+fileName;
				        		currentAttachment.subject = communicationObject.attachmentTemplates[i].templateInfo.subject;
				        		attachmentsFiles.push(currentAttachment);
							}
						}

						if (communicationObject.externalAttachments)
						{
							for(var i=0; i < communicationObject.externalAttachments.length;i++){
								currentAttachment = {};
				        		currentAttachment.filePath = communicationObject.externalAttachments[i];
				        		attachmentsFiles.push(currentAttachment);
							}
						}
				    	
				    	var attendeesInfo = sift({'id': attendeesRecepients[j]}, communicationObject.attendeesInfo);
						if (attendeesInfo){
		      			for (var i=0;i < attendeesInfo.length;i++){
		        				var contactInfo = attendeesInfo[i].contactInfo;
		        				if (contactInfo && contactInfo.defaultEmail){
		          				//send mail
		          				response.sendCommunication({
								      bodyFilename:bodyFileName,
								      subject:communicationObject.mailTemplateInfo.subject,
								      attachmentsFiles: attachmentsFiles,
								      to: contactInfo.defaultEmail,
								      cc: ccMailList,
								      from: currentClient.email
									});
		          			}
		          		}
		          	}
				}
			break;
     		case "Speakers":
     			var speakersRecepients = communicationObject.communicationDelivery.speakersRecepients;

     			for (var j=0;j < speakersRecepients.length;j++){
   					bodyFileName = filePath+"/speaker/"+ speakersRecepients[j] +"/"+mailTemplateId+".html"
	   				//get the attachment files 
	   				var attachmentsFiles = [];
				    	var currentAttachment;
						if (communicationObject.attachmentTemplates){
							for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
								var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
								var fileName = attachTemplateId+".pdf";
								currentAttachment = {};
				        		currentAttachment.filePath = filePath+"/speaker/"+speakersRecepients[j]+"/"+fileName;
				        		currentAttachment.subject = communicationObject.attachmentTemplates[i].templateInfo.subject;
				        		attachmentsFiles.push(currentAttachment);
							}
						}

						if (communicationObject.externalAttachments)
						{
							for(var i=0; i < communicationObject.externalAttachments.length;i++){
								currentAttachment = {};
				        		currentAttachment.filePath = communicationObject.externalAttachments[i];
				        		attachmentsFiles.push(currentAttachment);
							}
						}

					//send emails
					var speakersInfo = sift({'contactId': speakersRecepients[j]}, communicationObject.SpeakersInfo);
					if (speakersInfo)
					{
						var speakerContactInfo = speakersInfo[0].contactInfo;
        				if (speakerContactInfo && speakerContactInfo.defaultEmail){

        					if (includeOfficeContact)
        						ccMailList.push(speakerContactInfo.officeStaffContact[0].email);

		      				response.sendCommunication({
							    bodyFilename:bodyFileName,
							    subject:communicationObject.mailTemplateInfo.subject,
							    attachmentsFiles: attachmentsFiles,
							    to: speakerContactInfo.defaultEmail,
							    cc: ccMailList,
							    from: currentClient.email
							});
		      			}
					}
				}
				break;
     		case "Field Force":
   				bodyFileName = filePath+"/fieldforce/"+ communicationObject.RepRequestingInfo.id +"/"+mailTemplateId+".html"
					//get the attachment files 
   				var attachmentsFiles = [];
			    	var currentAttachment;
					if (communicationObject.attachmentTemplates){
						for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
							var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
							var fileName = attachTemplateId+".pdf";
							currentAttachment = {};
			        		currentAttachment.filePath = filePath+"/fieldforce/"+communicationObject.RepRequestingInfo.id+"/"+fileName;
			        		currentAttachment.subject = communicationObject.attachmentTemplates[i].templateInfo.subject;
			        		attachmentsFiles.push(currentAttachment);
						}
					}

					if (communicationObject.externalAttachments)
					{
						for(var i=0; i < communicationObject.externalAttachments.length;i++){
							currentAttachment = {};
			        		currentAttachment.filePath = communicationObject.externalAttachments[i];
			        		attachmentsFiles.push(currentAttachment);
						}
					}
			    	
			    	var repRequestingInfo = communicationObject.RepRequestingInfo;
     				if (repRequestingInfo && repRequestingInfo.defaultEmail){
       					//send mail
       					response.sendCommunication({
							bodyFilename:bodyFileName,
							subject:communicationObject.mailTemplateInfo.subject,
							attachmentsFiles: attachmentsFiles,
							to: repRequestingInfo.defaultEmail,
							cc: ccMailList,
							from: currentClient.email
						});
       				}

			break;
     		case "RSM":
   				bodyFileName = filePath+"/rsm/"+ communicationObject.RSMInfo.id +"/"+mailTemplateId+".html"
   				//get the attachment files 
   				var attachmentsFiles = [];
			    	var currentAttachment;
					if (communicationObject.attachmentTemplates){
						for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
							var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
							var fileName = attachTemplateId+".pdf";
							currentAttachment = {};
			        		currentAttachment.filePath = filePath+"/rsm/"+communicationObject.RSMInfo.id+"/"+fileName;
			        		currentAttachment.subject = communicationObject.attachmentTemplates[i].templateInfo.subject;
			        		attachmentsFiles.push(currentAttachment);
						}
					}

					if (communicationObject.externalAttachments)
					{
						for(var i=0; i < communicationObject.externalAttachments.length;i++){
							currentAttachment = {};
			        		currentAttachment.filePath = communicationObject.externalAttachments[i];
			        		attachmentsFiles.push(currentAttachment);
						}
					}
			    	
			    	var rsmInfo = communicationObject.RepRequestingInfo;
     				if (rsmInfo && rsmInfo.defaultEmail){
       				//send mail
       				response.sendCommunication({
					      bodyFilename:bodyFileName,
					      subject:communicationObject.mailTemplateInfo.subject,
					      attachmentsFiles: attachmentsFiles,
					      to: rsmInfo.defaultEmail,
					      cc: ccMailList,
					      from: currentClient.email
						});
       			}

			break;
		}
	}
	else
	{
		//general mail
		var filePath = getRenderFilePath(communicationObject.communicationDelivery.id);
		
		//get the attachment files
		var attachmentsFiles = [];
    	var currentAttachment;
		if (communicationObject.attachmentTemplates){
			for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
				var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
				var fileName = attachTemplateId+".pdf";
				currentAttachment = {};
        		currentAttachment.filePath = filePath+'/'+fileName;
        		currentAttachment.subject = communicationObject.attachmentTemplates[i].templateInfo.subject;
        		attachmentsFiles.push(currentAttachment);
			}
		}

		if (communicationObject.externalAttachments)
		{
			for(var i=0; i < communicationObject.externalAttachments.length;i++){
				currentAttachment = {};
        		currentAttachment.filePath = communicationObject.externalAttachments[i];
        		attachmentsFiles.push(currentAttachment);
			}
		}

		var ccMailList =[];
		if (communicationObject.communicationDelivery.ccRecepients)
  		{
			for(var i=0; i < communicationObject.communicationDelivery.ccRecepients.length;i++){
				switch(communicationObject.communicationDelivery.ccRecepients[i])
				{
					case "Field Force":
						ccMailList.push(communicationObject.RepEmail);
					break;
					case "RSM":
						ccMailList.push(communicationObject.RSMEmail);
					break;
					case "Client":
						ccMailList = ccMailList.concat(communicationObject.ClientEmails);
					break;
					case "CRG Program Manager":
						ccMailList.push(communicationObject.CRGManagerInfo.email);
						ccMailList.push(communicationObject.CRGEventManagerInfo.email);
					break;
					case "Speaker's Office Contact":
						ccMailList = ccMailList.concat(communicationObject.SpeakersOfficeEmails);
					break;
	        	}
	       }
	   }

	    //process any additional emails
	   if (communicationObject.communicationDelivery.additionalEmails && communicationObject.communicationDelivery.additionalEmails.length > 0 && communicationObject.communicationDelivery.additionalEmails[0])
	    {
	    	if (communicationObject.communicationDelivery.additionalEmails[0].indexOf(','))
	    	{
	    		var emailsAddresses = communicationObject.communicationDelivery.additionalEmails[0].split(',');
       		
	       		for (var i = 0; i < emailsAddresses.length; i++) {
	       			ccMailList.push(emailsAddresses[i]);
	       		}
	       	} else {
	       		ccMailList.push(communicationObject.communicationDelivery.additionalEmails[0]);
	       	}
	    }

		//for each sender send the notification
		for(var i=0; i < communicationObject.communicationDelivery.toRecepients.length;i++){
			var mailList =[];
			switch(communicationObject.communicationDelivery.toRecepients[i])
			{
				case "Client":
    				mailList = mailList.concat(communicationObject.ClientEmails);
				break;
        		case "Attendees":
					mailList = mailList.concat(communicationObject.AttendeesEmails);
				break;
        		case "Speakers":
        			mailList = mailList.concat(communicationObject.SpeakersEmails);


				break;
        		case "Field Force":
    				mailList.push(communicationObject.RepEmail);
				break;
        		case "RSM":
    				mailList.push(communicationObject.RSMEmail);
				break;
        	}
			for(var j=0; j < mailList.length;j++){
			 	response.sendCommunication({
			      bodyFilename:filePath+"/"+mailTemplateId+".html",
			      subject:communicationObject.mailTemplateInfo.subject,
			      attachmentsFiles: attachmentsFiles,
			      to: mailList[j],
			      cc: ccMailList,
			      from: currentClient.email
				});
			}
		}

		response.respond(result,{format:'json'});
	}
}

	this.createCommunicationFiles = function(response,communicationObject){
	    var HTML2PDF = require('../helpers/pdf/html2pdf.js');   
		var plainCommunicationObject = response.getPlainCommunicationObject(communicationObject);
		//response.respond(plainCommunicationObject, { format: 'json' });

		var MailContent = communicationObject.mailTemplateContent;
    	MailContent = ReplaceTagsTags(plainCommunicationObject,MailContent);
	    MailContent = ClearTags(MailContent);

		var mailTemplateId = communicationObject.communicationDelivery.mailTemplate;
		var communicationDeliveryId = communicationObject.communicationDelivery.id;
		var filePath = getRenderFilePath(communicationDeliveryId);
		var fileName = mailTemplateId+".html";

		//empty dir
		response.deleteDir(response,filePath);

		response.saveToFile(filePath,fileName,MailContent);

    	HTML2PDF.convert(filePath+'/'+fileName,{});

		if (communicationObject.attachmentTemplates){
			console.log(">>>>>>>>>>>if (communicationObject.attachmentTemplates){");

			for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
				console.log(">>>>>>>>>>>for(var i=0; i < communicationObject.attachmentTemplates;i++){");
				var AttachContent = communicationObject.attachmentTemplates[i].templateContent;
		        AttachContent = ReplaceTagsTags(plainCommunicationObject,AttachContent);
          		AttachContent = ClearTags(AttachContent);
				var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
				var communicationDeliveryId = communicationObject.communicationDelivery.id;
				var filePath = getRenderFilePath(communicationDeliveryId);
				var fileName = attachTemplateId+".html";
				console.log(">>>filePath"+filePath);
				console.log(">>>fileName"+fileName);
				response.saveToFile(filePath,fileName,AttachContent);
        		HTML2PDF.convert(filePath+'/'+fileName,{});
			}
		}

		response.respond(communicationObject
			, { format: 'json' });
	}



function ReplaceTagsTags(args,content){
      if (content){
	      var keys = [];
	      for (var k in args){
	        keys.push(k);
	      }
	     // console.log("total " + keys.length + " keys: " + keys);


	      for (var j = 0; j < keys.length; j++) {
	          var param = keys[j];
	          var regexParams = new RegExp("\\{\\$"+param+"\\}","g");
	          
	          if (typeof (args[param]) == 'undefined'){
	            // content = content.replace(regexParams, " ");
	          }
	          else if (args[param]){
	            content = content.replace(regexParams, args[param]);
	          }else{
	            // content = content.replace(regexParams, " ");
	          }
	      }
	  }
      return content
}

function ClearTags(content){
    if (content){
    	var regexParams = new RegExp("\\{\\$[a-zA-Z0-9_]+\\}","g");
    	return content.replace(regexParams, " ");
	}
}


  this.createIndividualCommunicationFiles = function(response,communicationObject){
    var HTML2PDF = require('../helpers/pdf/html2pdf.js');   
    var plainCommunicationObject = response.getPlainCommunicationObject(communicationObject);

    response.respond(plainCommunicationObject.Event_ConfirmedAttendeeList
      , { format: 'json' });

    var MailContent = communicationObject.mailTemplateContent;

    switch(communicationObject.communicationDelivery.toRecepients[0])
	{
	    //Attendees
		case "Attendees":
		    for (var attendeeIndex=0; attendeeIndex < communicationObject.communicationDelivery.attendeesRecepients.length;attendeeIndex++){
		      //Replace General Tags
		      var MailContentAttendees = communicationObject.mailTemplateContent;
		      MailContentAttendees = ReplaceTagsTags(plainCommunicationObject,MailContentAttendees);

		      for (var i = 0; i < plainCommunicationObject.Event_ConfirmedAttendeeList.length; i++) {
		      		if (plainCommunicationObject.Event_ConfirmedAttendeeList[i].id == communicationObject.communicationDelivery.attendeesRecepients[attendeeIndex])
		      		{
		      			MailContentAttendees = ReplaceTagsTags(plainCommunicationObject.Event_ConfirmedAttendeeList[i],MailContentAttendees);
		      			break;
		      		}
		      };

		      //Replace Individual Tags
		      MailContentAttendees = ClearTags(MailContentAttendees);
		      var mailTemplateId = communicationObject.communicationDelivery.mailTemplate;
		      var communicationDeliveryId = communicationObject.communicationDelivery.id;
		      var filePath = getRenderFilePath(communicationDeliveryId);

		      filePath += "/attendees/" + communicationObject.communicationDelivery.attendeesRecepients[attendeeIndex];
		      var fileName = mailTemplateId+".html";
		      
		      //empty dir
		      response.deleteDir(response,filePath);

		      response.saveToFile(filePath,fileName,MailContentAttendees);
		      HTML2PDF.convert(filePath+'/'+fileName,{});
		    
		      console.log(">>>>>>>>>filePath" + filePath);
		      console.log(">>>>>>>>>fileName" + fileName);

		      if (communicationObject.attachmentTemplates){
		        for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
		          var AttachContent = communicationObject.attachmentTemplates[i].templateContent;
		          //Replace General Tags
		          AttachContent = ReplaceTagsTags(plainCommunicationObject,AttachContent);
		          
		          //Replace Individual Tags
		          for (var i = 0; i < plainCommunicationObject.Event_ConfirmedAttendeeList.length; i++) {
		      		if (plainCommunicationObject.Event_ConfirmedAttendeeList[i].id == communicationObject.communicationDelivery.attendeesRecepients[attendeeIndex])
		      		{
		      			AttachContent = ReplaceTagsTags(plainCommunicationObject.Event_ConfirmedAttendeeList[i],AttachContent);
		      			break;
		      		}
		      	  };
		          
		          AttachContent = ClearTags(AttachContent);
		          var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
		          var communicationDeliveryId = communicationObject.communicationDelivery.id;
		          var filePath = getRenderFilePath(communicationDeliveryId);
		          filePath += "/attendees/" + communicationObject.communicationDelivery.attendeesRecepients[attendeeIndex];
		          var fileName = attachTemplateId+".html";
		          response.saveToFile(filePath,fileName,AttachContent);
		          HTML2PDF.convert(filePath+'/'+fileName,{});
		        }
		      }
		    }
		    break;

		case "Speakers":
			for (var speakerIndex=0; speakerIndex < communicationObject.communicationDelivery.speakersRecepients.length;speakerIndex++){
				var MailContentSpeakers = communicationObject.mailTemplateContent;
				MailContentSpeakers = ReplaceTagsTags(plainCommunicationObject,MailContentSpeakers);

				for (var i = 0; i < plainCommunicationObject.Event_Speakers.length; i++) {
		      		if (plainCommunicationObject.Event_Speakers[i].id == communicationObject.communicationDelivery.speakersRecepients[speakerIndex])
		      		{
		      			MailContentSpeakers = ReplaceTagsTags(plainCommunicationObject.Event_Speakers[i], MailContentSpeakers);
		      			break;
		      		}
		      	};

				MailContentSpeakers = ClearTags(MailContentSpeakers);


				var mailTemplateId = communicationObject.communicationDelivery.mailTemplate;
				var communicationDeliveryId = communicationObject.communicationDelivery.id;
				var filePath = getRenderFilePath(communicationDeliveryId);
				filePath += "/speaker/" + communicationObject.communicationDelivery.speakersRecepients[speakerIndex];
				var fileName = mailTemplateId+".html";
				
				//empty dir
				response.deleteDir(response,filePath);
				
				response.saveToFile(filePath,fileName,MailContentSpeakers);
				HTML2PDF.convert(filePath+'/'+fileName,{});


				if (communicationObject.attachmentTemplates){
					for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
					  var AttachContent = communicationObject.attachmentTemplates[i].templateContent;
					  
					  //Replace General Tags
					  AttachContent = ReplaceTagsTags(plainCommunicationObject,AttachContent);

					  for (var j = 0; j < plainCommunicationObject.Event_Speakers.length; j++) {
			      		if (plainCommunicationObject.Event_Speakers[j].id == communicationObject.communicationDelivery.speakersRecepients[speakerIndex])
			      		{
			      			AttachContent = ReplaceTagsTags(plainCommunicationObject.Event_Speakers[j], AttachContent);
			      			break;
			      		}
			      	  };
					  
					  AttachContent = ClearTags(AttachContent);

					  var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
					  var communicationDeliveryId = communicationObject.communicationDelivery.id;
					  var filePath = getRenderFilePath(communicationDeliveryId);
					  filePath += "/speaker/" + communicationObject.communicationDelivery.speakersRecepients[speakerIndex];
					  var fileName = attachTemplateId+".html";
					  response.saveToFile(filePath,fileName,AttachContent);
					  HTML2PDF.convert(filePath+'/'+fileName,{});
					}
				}
			}
			break;

		case "Field Force":
			//fieldForce
			MailContent = ReplaceTagsTags(plainCommunicationObject,MailContent);
			MailContent = ClearTags(MailContent);
			var mailTemplateId = communicationObject.communicationDelivery.mailTemplate;
			var communicationDeliveryId = communicationObject.communicationDelivery.id;
			var filePath = getRenderFilePath(communicationDeliveryId);
			filePath += "/fieldforce/" + communicationObject.RepRequestingInfo.id;
			var fileName = mailTemplateId+".html";
			
			//empty dir
			response.deleteDir(response,filePath);
			
			response.saveToFile(filePath,fileName,MailContent);
			HTML2PDF.convert(filePath+'/'+fileName,{});

			console.log(">>>>>>>>>filePath" + filePath);
			console.log(">>>>>>>>>fileName" + fileName);


			if (communicationObject.attachmentTemplates){
				for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
				  var AttachContent = communicationObject.attachmentTemplates[i].templateContent;
				  //Replace General Tags
				  AttachContent = ReplaceTagsTags(plainCommunicationObject,AttachContent);
				  AttachContent = ClearTags(AttachContent);
				  var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
				  var communicationDeliveryId = communicationObject.communicationDelivery.id;
				  var filePath = getRenderFilePath(communicationDeliveryId);
				  filePath += "/fieldforce/" + communicationObject.RepRequestingInfo.id;
				  var fileName = attachTemplateId+".html";
				  response.saveToFile(filePath,fileName,AttachContent);
				  HTML2PDF.convert(filePath+'/'+fileName,{});
				}
			}

			break;

		case "RSM":
			//RSM
			MailContent = ReplaceTagsTags(plainCommunicationObject,MailContent);
			MailContent = ClearTags(MailContent);
			var mailTemplateId = communicationObject.communicationDelivery.mailTemplate;
			var communicationDeliveryId = communicationObject.communicationDelivery.id;
			var filePath = getRenderFilePath(communicationDeliveryId);
			filePath += "/rsm/" + communicationObject.RSMInfo.id;
			var fileName = mailTemplateId+".html";
			
			//empty dir
			response.deleteDir(response,filePath);
			
			response.saveToFile(filePath,fileName,MailContent);
			HTML2PDF.convert(filePath+'/'+fileName,{});

			console.log(">>>>>>>>>filePath" + filePath);
			console.log(">>>>>>>>>fileName" + fileName);


			if (communicationObject.attachmentTemplates){
				for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
				  var AttachContent = communicationObject.attachmentTemplates[i].templateContent;
				  //Replace General Tags
				  AttachContent = ReplaceTagsTags(plainCommunicationObject,AttachContent);
				  AttachContent = ClearTags(AttachContent);
				  var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
				  var communicationDeliveryId = communicationObject.communicationDelivery.id;
				  var filePath = getRenderFilePath(communicationDeliveryId);
				  filePath += "/rsm/" + communicationObject.RSMInfo.id;
				  var fileName = attachTemplateId+".html";
				  response.saveToFile(filePath,fileName,AttachContent);
				  HTML2PDF.convert(filePath+'/'+fileName,{});
				}
			}
			break;

		case "Client":
			//Client
		    var currentClient = response.session.get('currentClient');
			MailContent = ReplaceTagsTags(plainCommunicationObject,MailContent);
			MailContent = ClearTags(MailContent);
			var mailTemplateId = communicationObject.communicationDelivery.mailTemplate;
			var communicationDeliveryId = communicationObject.communicationDelivery.id;
			var filePath = getRenderFilePath(communicationDeliveryId);
			filePath += "/client/" + currentClient.id;
			var fileName = mailTemplateId+".html";
			
			//empty dir
			response.deleteDir(response,filePath);
			
			response.saveToFile(filePath,fileName,MailContent);
			HTML2PDF.convert(filePath+'/'+fileName,{});

			console.log(">>>>>>>>>filePath" + filePath);
			console.log(">>>>>>>>>fileName" + fileName);


			if (communicationObject.attachmentTemplates){
				for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
				  var AttachContent = communicationObject.attachmentTemplates[i].templateContent;
				  //Replace General Tags
				  AttachContent = ReplaceTagsTags(plainCommunicationObject,AttachContent);
				  AttachContent = ClearTags(AttachContent);
				  var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
				  var communicationDeliveryId = communicationObject.communicationDelivery.id;
				  var filePath = getRenderFilePath(communicationDeliveryId);
				  filePath += "/client/" + currentClient.id;
				  var fileName = attachTemplateId+".html";
				  response.saveToFile(filePath,fileName,AttachContent);
				  HTML2PDF.convert(filePath+'/'+fileName,{});
				}
			}

			break;
		}
  }


	function getRenderFilePath(communicationDeliveryId, isAutomatic)
	 {
			var util = require('util');
			var path = require('path');
			var filePath = path.join(geddy.config.CommunicationsFolder + 
			util.format("renderCommunication/%s%s",
				isAutomatic?"/automatic/":"",
				communicationDeliveryId
				)
			);
			return filePath;
	 }

	this.saveToFile = function(filePath,fileName,content){
			var fse = require('fs-extra');
			console.log(">>>>>>>>>>>> filePath" + filePath);
			console.log(">>>>>>>>>>>> filePath" + fileName);
			fse.ensureDirSync(filePath);
			var result = filePath+'/'+fileName;
     		fs.writeFileSync(result, content); 
     		return result;
	}

	this.deleteDir = function($thisRequest,filePath)
	{

    	var files = [];
    	if( fs.existsSync(filePath) ) {
        files = fs.readdirSync(filePath);
        files.forEach(function(file,index){
            var curPath = filePath + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                $thisRequest.deleteDir($thisRequest,curPath);
            } else { // delete file*/
             fs.unlinkSync(curPath);
            }
        });
        
        fs.rmdirSync(filePath);
     	}
	}



	this.getPlainCommunicationObject = function(communicationObject){
		var result = {};
		if (communicationObject.RSMInfo)
			result.RSM_FullName = communicationObject.RSMInfo.firstName + ' ' + communicationObject.RSMInfo.lastName;

		result.Rep_FullName= communicationObject.RepRequestingInfo.firstName + ' ' + communicationObject.RepRequestingInfo.lastName;
		result.Rep_PhoneNumber= communicationObject.RepRequestingInfo.defaultPhoneNumber;
		result.Rep_Email= communicationObject.RepRequestingInfo.defaultEmail;
		result.Portal_URL= 'https://' + geddy.config.baseURL;
		result.CRG_ProgramManager= communicationObject.CRGManagerInfo.firstName + ' ' + communicationObject.CRGManagerInfo.lastName;
		result.CRG_EventManager= communicationObject.CRGEventManagerInfo.firstName + ' ' + communicationObject.CRGEventManagerInfo.lastName;
		result.Event_Confirmed_Date= communicationObject.confirmedDate;
		result.Event_Brand= communicationObject.brand;
		result.Event_PresentationTitle = communicationObject.presentationTitle;
		result.Event_ProgramType= communicationObject.eventType;
		result.Event_City= communicationObject.city;
		result.Event_State= communicationObject.state;
		result.Event_DatePref1= communicationObject.eventDatePref1;

		result.Event_Fee_Amount = '';
		result.Event_Check_Number = '';
		result.Event_Date_Paid = '';
		result.Event_Expenses_Amount = '';
		result.Event_Expenses_Check_Number = '';
		result.Event_Expenses_Date_Paid = '';

		if (communicationObject.invoices && communicationObject.invoices.length > 0)
		{
			result.Event_Fee_Amount = communicationObject.invoices[0].feeService;
			result.Event_Check_Number = communicationObject.invoices[0].feeCheck;
			result.Event_Date_Paid = communicationObject.invoices[0].feeDatePaid;
			result.Event_Expenses_Amount = communicationObject.invoices[0].expenseAmount;
			result.Event_Expenses_Check_Number = communicationObject.invoices[0].expenseCheck;
			result.Event_Expenses_Date_Paid = communicationObject.invoices[0].expenseDatePaid;
		}

		if (!communicationObject.eventDatePref2)
		{
			communicationObject.eventDatePref2 = '';
		}
		
		result.Event_DatePref2= communicationObject.eventDatePref2;

		if (!communicationObject.eventDatePref3)
		{
			communicationObject.eventDatePref3 = '';
		} 
		
		result.Event_DatePref3= communicationObject.eventDatePref3;
		result.Event_JobNumber = communicationObject.jobNumber;
		result.Event_StartTime= communicationObject.eventStartTime;
		result.Event_TimeZone = communicationObject.timezone;

		result.Event_Speakers = [];

		var j;
		for (var j = 0; j < communicationObject.SpeakersInfo.length; j++) {
			var speaker = new Object();

			speaker.Speaker_FullName= communicationObject.SpeakersInfo[j].speakerName;

			if (communicationObject.SpeakersInfo[j].contactInfo)
			{
				speaker.Speaker_FirstName= communicationObject.SpeakersInfo[j].contactInfo.firstName;
				speaker.Speaker_LastName= communicationObject.SpeakersInfo[j].contactInfo.lastName;
				if (communicationObject.SpeakersInfo[j].contactInfo.officeStaffContact && communicationObject.SpeakersInfo[j].contactInfo.officeStaffContact.length > 0)
				{
					speaker.Speaker_OfficeStaffContact = communicationObject.SpeakersInfo[j].contactInfo.officeStaffContact[0].email;
				} else {
					speaker.Speaker_OfficeStaffContact = '';
				}
				speaker.Speaker_Email= communicationObject.SpeakersInfo[j].contactInfo.defaultEmail;
			} else {
				speaker.Speaker_FirstName = '';
				speaker.Speaker_LastName= '';
				speaker.Speaker_OfficeStaffContact = '';
				speaker.Speaker_Email = '';
			}

			speaker.Speaker_Status= communicationObject.SpeakersInfo[j].crgStatus;
			speaker.Speaker_City_State= communicationObject.SpeakersInfo[j].city + ', ' + communicationObject.SpeakersInfo[j].state;
			speaker.Speaker_City= communicationObject.SpeakersInfo[j].city;
			speaker.Speaker_State= communicationObject.SpeakersInfo[j].state;     
			speaker.Speaker_TravelType= communicationObject.SpeakersInfo[j].travelType;
			speaker.Speaker_GroundTransportation= communicationObject.SpeakersInfo[j].groundTransportation;	
			speaker.id = communicationObject.SpeakersInfo[j].contactId;

			result.Event_Speakers.push(speaker);
		}

		result.Event_Pref1_Speaker_FullName= communicationObject.SpeakersInfo[0].speakerName;

		if (communicationObject.SpeakersInfo[0].contactInfo)
		{
			result.Event_Pref1_Speaker_FirstName= communicationObject.SpeakersInfo[0].contactInfo.firstName;
			result.Event_Pref1_Speaker_LastName= communicationObject.SpeakersInfo[0].contactInfo.lastName;
			if (communicationObject.SpeakersInfo[0].contactInfo.officeStaffContact && communicationObject.SpeakersInfo[0].contactInfo.officeStaffContact.length > 0)
			{
				speaker.Event_Pref1_Speaker_OfficeStaffContact = communicationObject.SpeakersInfo[0].contactInfo.officeStaffContact[0].email;
			} else {
				speaker.Event_Pref1_Speaker_OfficeStaffContact = '';
			}
		} else {
			speaker.Event_Pref1_Speaker_FirstName = '';
			speaker.Event_Pref1_Speaker_LastName= '';
			speaker.Event_Pref1_Speaker_OfficeStaffContact = '';
		}

		result.Event_Pref1_Speaker_Status= communicationObject.SpeakersInfo[0].crgStatus;
		result.Event_Pref1_Speaker_City_State= communicationObject.SpeakersInfo[0].city  + ', ' + communicationObject.SpeakersInfo[0].state;
		result.Event_Pref1_Speaker_City= communicationObject.SpeakersInfo[0].city;
		result.Event_Pref1_Speaker_State= communicationObject.SpeakersInfo[0].state;     
		result.Event_Pref1_Speaker_TravelType= communicationObject.SpeakersInfo[0].travelType;
		result.Event_Pref1_Speaker_GroundTransportation= communicationObject.SpeakersInfo[0].groundTransportation;
		
		if (!communicationObject.SpeakersInfo[1])
		{
			result.Event_Pref2_Speaker_FullName= '';
			result.Event_Pref2_Speaker_FirstName= '';
			result.Event_Pref2_Speaker_LastName= '';
			result.Event_Pref2_Speaker_Status= '';
			result.Event_Pref2_Speaker_City= '';
			result.Event_Pref2_Speaker_State= '';
			result.Event_Pref2_Speaker_TravelType= '';
			result.Event_Pref2_Speaker_GroundTransportation= '';
		} else {
			result.Event_Pref2_Speaker_FullName= communicationObject.SpeakersInfo[1].speakerName;

			if (communicationObject.SpeakersInfo[1].contactInfo)
			{
				result.Event_Pref2_Speaker_FirstName= communicationObject.SpeakersInfo[1].contactInfo.firstName;
				result.Event_Pref2_Speaker_LastName= communicationObject.SpeakersInfo[1].contactInfo.lastName;
				if (communicationObject.SpeakersInfo[1].contactInfo.officeStaffContact && communicationObject.SpeakersInfo[1].contactInfo.officeStaffContact.length > 0)
				{
					speaker.Event_Pref2_Speaker_OfficeStaffContact = communicationObject.SpeakersInfo[1].contactInfo.officeStaffContact[0].email;
				} else {
					speaker.Event_Pref2_Speaker_OfficeStaffContact = '';
				}
			} else {
				speaker.Event_Pref2_Speaker_FirstName = '';
				speaker.Event_Pref2_Speaker_LastName= '';
				speaker.Event_Pref2_Speaker_OfficeStaffContact = '';
			}
			
			result.Event_Pref2_Speaker_Status= communicationObject.SpeakersInfo[1].crgStatus;
			result.Event_Pref2_Speaker_City_State= communicationObject.SpeakersInfo[1].city + ', ' + communicationObject.SpeakersInfo[1].state;
			result.Event_Pref2_Speaker_City= communicationObject.SpeakersInfo[1].city;
			result.Event_Pref2_Speaker_State= communicationObject.SpeakersInfo[1].state;
			result.Event_Pref2_Speaker_TravelType= communicationObject.SpeakersInfo[1].travelType;
			result.Event_Pref2_Speaker_GroundTransportation= communicationObject.SpeakersInfo[1].groundTransportation;
		}

		if (!communicationObject.SpeakersInfo[2])
		{
			result.Event_Pref3_Speaker_FullName= '';
			result.Event_Pref3_Speaker_FirstName= '';
			result.Event_Pref3_Speaker_LastName= '';
			result.Event_Pref3_Speaker_Status= '';
			result.Event_Pref3_Speaker_City_State= '';
			result.Event_Pref3_Speaker_City= '';
			result.Event_Pref3_Speaker_State= '';
			result.Event_Pref3_Speaker_TravelType= '';
			result.Event_Pref3_Speaker_GroundTransportation= ''; 
		} else {
			result.Event_Pref3_Speaker_FullName= communicationObject.SpeakersInfo[2].speakerName;

			if (communicationObject.SpeakersInfo[2].contactInfo)
			{
				result.Event_Pref3_Speaker_FirstName= communicationObject.SpeakersInfo[2].contactInfo.firstName;
				result.Event_Pref3_Speaker_LastName= communicationObject.SpeakersInfo[2].contactInfo.lastName;
				if (communicationObject.SpeakersInfo[2].contactInfo.officeStaffContact && communicationObject.SpeakersInfo[2].contactInfo.officeStaffContact.length > 0)
				{
					speaker.Event_Pref3_Speaker_OfficeStaffContact = communicationObject.SpeakersInfo[2].contactInfo.officeStaffContact[0].email;
				} else {
					speaker.Event_Pref3_Speaker_OfficeStaffContact = '';
				}
			}else {
				speaker.Event_Pref3_Speaker_FirstName = '';
				speaker.Event_Pref3_Speaker_LastName= '';
				speaker.Event_Pref3_Speaker_OfficeStaffContact = '';
			}

			result.Event_Pref3_Speaker_Status= communicationObject.SpeakersInfo[2].crgStatus;
			result.Event_Pref3_Speaker_City_State= communicationObject.SpeakersInfo[2].city + ', ' + communicationObject.SpeakersInfo[2].state;
			result.Event_Pref3_Speaker_City= communicationObject.SpeakersInfo[2].city;
			result.Event_Pref3_Speaker_State= communicationObject.SpeakersInfo[2].state;
			result.Event_Pref3_Speaker_TravelType= communicationObject.SpeakersInfo[2].travelType;
			result.Event_Pref3_Speaker_GroundTransportation= communicationObject.SpeakersInfo[2].groundTransportation;
		}

		
		if (!communicationObject.VenueInfo)
		{
			result.Event_Venue_Type= '';
			result.Event_Venue_Name= '';
			result.Event_Venue_Address1= '';
			result.Event_Venue_Address2= '';
			result.Event_Venue_City= '';
			result.Event_Venue_State= '';
			result.Event_Venue_Zip= '';
			result.Event_Venue_Contact_Name= '';
			result.Event_Venue_Contact_Phone= '';
			result.Event_Venue_Contact_Email= '';
			result.Event_Venue_PhoneNumber= '';
		} else {
			result.Event_Venue_Type= communicationObject.VenueInfo.venueType;
			result.Event_Venue_Name= communicationObject.VenueInfo.venueName;

			if (communicationObject.VenueInfo.addresses && communicationObject.VenueInfo.addresses.length > 0){
				result.Event_Venue_Address1 = communicationObject.VenueInfo.addresses[0].address1;
				result.Event_Venue_Address2 = communicationObject.VenueInfo.addresses[0].address2;
				result.Event_Venue_City = communicationObject.VenueInfo.addresses[0].city;
				result.Event_Venue_State = communicationObject.VenueInfo.addresses[0].state;
				result.Event_Venue_Zip = communicationObject.VenueInfo.addresses[0].postalCode;
			} else {
				result.Event_Venue_Address1= '';
				result.Event_Venue_Address2= '';
				result.Event_Venue_City= '';
				result.Event_Venue_State= '';
				result.Event_Venue_Zip= '';
			}

			result.Event_Venue_Contact_Name= '';
			result.Event_Venue_Contact_Phone= '';
			result.Event_Venue_Contact_Email= '';
				
			if (communicationObject.VenueInfo.contact && communicationObject.VenueInfo.contact.length > 0){
				if (communicationObject.VenueInfo.contact[0].firstName && communicationObject.VenueInfo.contact[0].lastName)
				{
					result.Event_Venue_Contact_Name = communicationObject.VenueInfo.contact[0].firstName + ' ' + communicationObject.VenueInfo.contact[0].lastName;
				}

				if (communicationObject.VenueInfo.contact[0].phoneNumbers && communicationObject.VenueInfo.contact[0].phoneNumbers.length > 0 && communicationObject.VenueInfo.contact[0].phoneNumbers[0].number)
				{
					result.Event_Venue_Contact_Phone = communicationObject.VenueInfo.contact[0].phoneNumbers[0].number;
				}

				if (communicationObject.VenueInfo.contact[0].email)
				{
					result.Event_Venue_Contact_Email = communicationObject.VenueInfo.contact[0].email;
				}
			} 

			if (communicationObject.VenueInfo.phoneNumbers && communicationObject.VenueInfo.phoneNumbers.length > 0)
			{
				result.Event_Venue_PhoneNumber= communicationObject.VenueInfo.phoneNumbers[0].number;
			} else {
				result.Event_Venue_PhoneNumber= '';
			}

		}
		
		if (!communicationObject.CatererInfo)
		{
			result.Event_Caterer_Name= '';
		} else {
			result.Event_Caterer_Name= communicationObject.CatererInfo.venueName;
		}
		
		result.Event_Estimated_Meal_Cost= communicationObject.estimatedMealCost;
		result.Event_Estimated_Attendance= communicationObject.expectedNumberAttendees;

		result.Event_Invitations_Distribution_Selections= '';
		if (!communicationObject.invitationsPDF)
		{
			communicationObject.invitationsPDF = 'false';
		} 

		if (!communicationObject.invitationsHardcopyDirect)
		{
			communicationObject.invitationsHardcopyDirect = 'false';
		} 

		if (!communicationObject.invitationsHardcopyRep)
		{
			communicationObject.invitationsHardcopyRep = 'false';
		}  

		result.Event_Invitations_Distribution_Selections= communicationObject.invitationsPDF + ',' + communicationObject.invitationsHardcopyDirect + ',' + communicationObject.invitationsHardcopyRep ;
		result.Event_Audience_Recruitment_Email_Count =  communicationObject.emailInvitations ? communicationObject.emailInvitations.length: 0 ;
		result.Event_Audience_Recruitment_HardCopy_Count= communicationObject.hardcopyInvitations ? communicationObject.hardcopyInvitations.length : 0;

		
		if (communicationObject.ConfirmedSpeakerInfo && (communicationObject.ConfirmedSpeakerInfo.length > 0)){
			result.Event_ConfirmedSpeakerId = communicationObject.ConfirmedSpeakerInfo[0].contactId;
			result.Event_Confirmed_Speaker_FirstName= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.firstName;
			result.Event_Confirmed_Speaker_LastName= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.lastName;
			result.Event_Confirmed_Speaker_FullName= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.firstName + '  ' + communicationObject.ConfirmedSpeakerInfo[0].contactInfo.lastName;
			if (communicationObject.ConfirmedSpeakerInfo[0].contactInfo.degrees && communicationObject.ConfirmedSpeakerInfo[0].contactInfo.degrees.length > 0)
			{
				result.Event_Confirmed_Speaker_Degrees= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.degrees.toString();
			} else {
				result.Event_Confirmed_Speaker_Degrees= '';
			}
			result.Event_Confirmed_Speaker_CellNumber= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.defaultPhoneNumber;
			result.Event_Confirmed_Speaker_DefaultEmail= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.defaultEmail;
			
			if (communicationObject.ConfirmedSpeakerInfo[0].contactInfo.emailAddresses && communicationObject.ConfirmedSpeakerInfo[0].contactInfo.emailAddresses.length > 0)
			{
				result.Event_Confirmed_Speaker_OtherEmails = communicationObject.ConfirmedSpeakerInfo[0].contactInfo.emailAddresses.toString();
			}

			if (communicationObject.ConfirmedSpeakerInfo[0].contactInfo.addresses && communicationObject.ConfirmedSpeakerInfo[0].contactInfo.addresses.length > 0)
			{
				result.Event_Confirmed_Speaker_Address= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.addresses[0].address1;
				result.Event_Confirmed_Speaker_Address2= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.addresses[0].address2;
				result.Event_Confirmed_Speaker_City= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.addresses[0].city;
				result.Event_Confirmed_Speaker_State= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.addresses[0].state;
				result.Event_Confirmed_Speaker_PostalCode= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.addresses[0].postalCode;
			}

			if (communicationObject.ConfirmedSpeakerInfo[0].contactInfo.taxForms && communicationObject.ConfirmedSpeakerInfo[0].contactInfo.taxForms.length > 0)
			{
				result.Event_Confirmed_Speaker_W9Address= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.taxForms[0].address1;
				result.Event_Confirmed_Speaker_W9Address2= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.taxForms[0].address2;
				result.Event_Confirmed_Speaker_W9City= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.taxForms[0].city;
				result.Event_Confirmed_Speaker_W9State= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.taxForms[0].state;
				result.Event_Confirmed_Speaker_W9PostalCode= communicationObject.ConfirmedSpeakerInfo[0].contactInfo.taxForms[0].postalCode;
			} else {
				result.Event_Confirmed_Speaker_W9Address= '';
				result.Event_Confirmed_Speaker_W9City= '';
				result.Event_Confirmed_Speaker_W9State= '';
				result.Event_Confirmed_Speaker_W9PostalCode= '';
			}
		}
		
		result.Event_Attendee_List = '<table width="100%" border="1"><tr style="background-color:#E8F1F3"><th>Last Name</th><th>First Name</th><th>Degrees</th><th>City</th><th>State</th></tr>';
		result.Event_Attendee_List_Registered = '<table width="100%" border="1"><tr style="background-color:#E8F1F3"><th>Last Name</th><th>First Name</th><th>Degrees</th><th>City</th><th>State</th></tr>';

		result.Event_ConfirmedAttendeeList= [];

		if (communicationObject.attendeesInfo){
		var i;
		for (i = 0; i < communicationObject.attendeesInfo.length; i++) {
			var registered = false;
			if (communicationObject.attendeesInfo[i].status == 6)
			{
				registered = true;
			}

			result.Event_Attendee_List += '<tr>';
			if (registered)
			{
				result.Event_Attendee_List_Registered += '<tr>';
			}

			var attendee = new Object();
			attendee.Attendee_LastName = communicationObject.attendeesInfo[i].contactInfo.lastName;
			result.Event_Attendee_List += '<td>' + attendee.Attendee_LastName + '</td>';
			if (registered)
			{
				result.Event_Attendee_List_Registered += '<td>' + attendee.Attendee_LastName + '</td>';
			}

			attendee.Attendee_FirstName = communicationObject.attendeesInfo[i].contactInfo.firstName;
			result.Event_Attendee_List += '<td>' + attendee.Attendee_FirstName + '</td>';
			if (registered)
			{
				result.Event_Attendee_List_Registered += '<td>' + attendee.Attendee_FirstName + '</td>';
			}

			attendee.Attendee_FullName = communicationObject.attendeesInfo[i].contactInfo.firstName + ' ' + communicationObject.attendeesInfo[i].contactInfo.lastName;
			attendee.Attendee_Suffix = communicationObject.attendeesInfo[i].contactInfo.suffix;
			attendee.Attendee_Salutation = communicationObject.attendeesInfo[i].contactInfo.salutation;


			if (communicationObject.attendeesInfo[i].contactInfo.degrees && communicationObject.attendeesInfo[i].contactInfo.degrees.length > 0)
			{
				attendee.Attendee_Degrees = communicationObject.attendeesInfo[i].contactInfo.degrees.toString();
			} else {
				attendee.Attendee_Degrees = '';
			}

			result.Event_Attendee_List += '<td>' + attendee.Attendee_Degrees + '</td>';
			if (registered)
			{
				result.Event_Attendee_List_Registered += '<td>' + attendee.Attendee_Degrees + '</td>';
			}
			
	      	attendee.id = communicationObject.attendeesInfo[i].id;

	      	
	      	if (communicationObject.attendeesInfo[i].contactInfo.affiliations && communicationObject.attendeesInfo[i].contactInfo.affiliations.length > 0)
	      	{
		      	attendee.Attendee_Affiliation_Name = communicationObject.attendeesInfo[i].contactInfo.affiliations[0].affiliationName;
				attendee.Attendee_Affiliation_City = communicationObject.attendeesInfo[i].contactInfo.affiliations[0].city;
				attendee.Attendee_Affiliation_State = communicationObject.attendeesInfo[i].contactInfo.affiliations[0].state;

				result.Event_Attendee_List += '<td>' + attendee.Attendee_Affiliation_City + '</td>';
				result.Event_Attendee_List += '<td>' + attendee.Attendee_Affiliation_State + '</td>';
				if (registered)
				{
					result.Event_Attendee_List_Registered += '<td>' + attendee.Attendee_Affiliation_City + '</td>';
					result.Event_Attendee_List_Registered += '<td>' + attendee.Attendee_Affiliation_State + '</td>';
				}
			} else {
				result.Event_Attendee_List += '<td></td>';
				result.Event_Attendee_List += '<td></td>';
				if (registered)
				{
					result.Event_Attendee_List_Registered += '<td></td>';
					result.Event_Attendee_List_Registered += '<td></td>';
				}
			}

			attendee.Attendee_Registration_Date = communicationObject.attendeesInfo[i].registrationDate;

			if (communicationObject.attendeesInfo[i].status){
				 attendee.Attendee_Status = constants.ATTENDEE_STATUS_TYPE()[communicationObject.attendeesInfo[i].status];  //this is the status as set by CRG
			}

			if (communicationObject.attendeesInfo[i].pin){
				 attendee.Attendee_Pin = communicationObject.attendeesInfo[i].pin;  //this is the pin as set by CRG
			}
		
			result.Event_ConfirmedAttendeeList.push(attendee);

			result.Event_Attendee_List += '</tr>';
			if (registered)
			{
				result.Event_Attendee_List_Registered += '</tr>';
			}
	 	}; 

	 	result.Event_Attendee_List += '</table>';
	 	result.Event_Attendee_List_Registered += '</table>';
	}

	return result;
}

/******************************/

			this.BuildCommunicationsObject = function(response,communicationDelivery){
					var MJ = require("mongo-fast-join");
					var MongoClient = require('mongodb').MongoClient;
					var mongoJoin = new MJ();
					var eventId = communicationDelivery.eventId;
					var connectionString = 'mongodb://'+geddy.config.db.mongo.host+':'+geddy.config.db.mongo.port+'/'+geddy.config.db.mongo.dbname;
					if (typeof(geddy.config.db.mongo.serverOptions)){
						if(geddy.config.db.mongo.serverOptions.ssl){
							connectionString += "?ssl=true";
						}
					}

					MongoClient.connect(connectionString, function(err, db) {
						if(err) throw err;
						var EventsCollection = db.collection('events');
						var ProgramCollection = db.collection('programs');
						mongoJoin.query(EventsCollection,
									{id:eventId}, //query statement
									{}, //fields
									{//limit: 10000//options
									}
							).join({
									joinCollection: ProgramCollection,
									//respects the dot notation, multiple keys can be specified in this array
									leftKeys: ["programId"],
									//This is the key of the document in the right hand document
									rightKeys: ["id"],
									//This is the new subdocument that will be added to the result document
									newKey: "program"
							}).exec(function (err, items) {    //Call exec to run the compiled query and catch any errors and results, in the callback
								//callback(response,items);
											var DBUtils = require('../helpers/utils/DBUtils.js');   
											var CommunicationsObject = DBUtils.cloneObject(items);
											if (CommunicationsObject.length > 0){
												CommunicationsObject = CommunicationsObject[0];
											}
											CommunicationsObject.communicationDelivery = communicationDelivery;
											db.close();

											var attendees = CommunicationsObject.attendees.slice(0);

											response.AddAttendiesInfo(response,CommunicationsObject,attendees,[]);
							});
						});
			}

			// this.AddAttendiesInfo = function(response,communicationObject,arrayAttendies,result){
			this.AddAttendiesInfo = function(response,communicationObject,arrayAttendies,result){

				// console.log(">>>>arrayAttendies:"+JSON.stringify(arrayAttendies));
				// console.log(">>>>arrayAttendies.length:"+JSON.stringify(arrayAttendies.length));

				if (!arrayAttendies || arrayAttendies.length == 0){
					//console.log(">>>>result:"+JSON.stringify(result));
					communicationObject.attendeesInfo = result;
					response.AddRepRequestingInfo(response,communicationObject);
				}else{
					var currentAttendee = arrayAttendies.shift();
					var MJ = require("mongo-fast-join");
					var MongoClient = require('mongodb').MongoClient;
					var mongoJoin = new MJ();
					var connectionString = 'mongodb://'+geddy.config.db.mongo.host+':'+geddy.config.db.mongo.port+'/'+geddy.config.db.mongo.dbname;

					if (typeof(geddy.config.db.mongo.serverOptions)){
						if(geddy.config.db.mongo.serverOptions.ssl){
							connectionString += "?ssl=true";
						}
					}
					//Get attendeeInfo with contactInfo at the same time
					MongoClient.connect(connectionString, function(err, db) {
						if(err) throw err;
					var AttendeeCollection = db.collection('attendees');
					var ContactCollection = db.collection('contacts');
					mongoJoin.query(AttendeeCollection,
								{id:currentAttendee}, //query statement
								{}, //fields
								{
										//limit: 10000//options
								}
						).join({
								joinCollection: ContactCollection,
								//respects the dot notation, multiple keys can be specified in this array
								leftKeys: ["contactId"],
								//This is the key of the document in the right hand document
								rightKeys: ["id"],
								//This is the new subdocument that will be added to the result document
								newKey: "contactInfo"
						}).exec(function (err, items) {    //Call exec to run the compiled query and catch any errors and results, in the callback
								if (items && items.length > 0) {
									result.push(items[0]);
								}            

								db.close();
								return response.AddAttendiesInfo(response,communicationObject,arrayAttendies,result);
						});
					});
				}
			}

			this.AddRepRequestingInfo = function(response,communicationObject){
					geddy.model.Rep.first({ id: communicationObject.requestingRepId }, function (err, data) {
							if (data) {
								communicationObject.RepRequestingInfo = data;
							}
							response.AddRSMInfo(response,communicationObject);
						});
			}

			this.AddRSMInfo = function(response,communicationObject){
				if (communicationObject.RepRequestingInfo.managerId && communicationObject.RepRequestingInfo.managerId != 'N/A')
				{
					geddy.model.Rep.first({ id: communicationObject.RepRequestingInfo.managerId }, function (err, data) {
						if (data) {
							communicationObject.RSMInfo = data;
						}
						
						response.AddVenueInfo(response,communicationObject);
					});
				} else {
					communicationObject.RSMInfo = null;
					response.AddVenueInfo(response,communicationObject);
				}
			}

			this.AddVenueInfo = function(response,communicationObject){
					geddy.model.Venue.first({ id: communicationObject.venueId }, function (err, data) {
							if (data) {
								communicationObject.VenueInfo = data;
							}
							response.AddCatererInfo(response,communicationObject);
						});
			}

			this.AddCatererInfo = function(response,communicationObject){
					geddy.model.Venue.first({ id: communicationObject.catererId }, function (err, data) {
							if (data) {
								communicationObject.CatererInfo = data;
							}

							// var potentialSpeakers;
							// if (communicationObject.potentialSpeakers)
							// {
							// 	potentialSpeakers = communicationObject.potentialSpeakers.slice(0);
							// } else {
							// 	potentialSpeakers = [];
							// }

							// var selectedSpeakers;
							// if (communicationObject.communicationDelivery.speakersRecepients)
							// {
							// 	selectedSpeakers = communicationObject.communicationDelivery.speakersRecepients.slice(0);
							// } else {
							// 	selectedSpeakers = [];
							// }


							// response.AddSpeakersInfo(response,communicationObject, potentialSpeakers, selectedSpeakers,[]);
							response.AddSpeakersInfo(response,communicationObject,communicationObject.potentialSpeakers,[]);

						});
			}

			this.AddSpeakersInfo = function(response,communicationObject,arraySpeakers,result){
				if ((!arraySpeakers) || arraySpeakers.length == 0){
					console.log(">>>>result:"+JSON.stringify(result));
					communicationObject.SpeakersInfo = result;
					communicationObject.ConfirmedSpeakerInfo = [];
					for(var i=0; result.length > i ;i++ ){
						if (result[i].crgStatus == 1){
							communicationObject.ConfirmedSpeakerInfo.push(result[i]);
						}        
					}
					response.AddCRGManagerInfo(response,communicationObject);
					//response.AddRepRequestingInfo(response,communicationObject);
				}else{
					console.log(">>>>arraySpeakers:"+JSON.stringify(arraySpeakers));
					console.log(">>>>arraySpeakers.length:"+JSON.stringify(arraySpeakers.length));
					var currentSpeaker = arraySpeakers.shift();
					console.log(">>>>>>currentSpeaker"+JSON.stringify(currentSpeaker));
					geddy.model.Contact.first({ id: currentSpeaker.contactId }, function (err, data) {
							var DBUtils = require('../helpers/utils/DBUtils.js');   
							var newSpeakerInfo = DBUtils.cloneObject(currentSpeaker);
							if (data) {
								newSpeakerInfo.contactInfo = data
							}
							result.push(newSpeakerInfo);
							return response.AddSpeakersInfo(response,communicationObject,arraySpeakers,result);
						});
				}
			}

			this.AddCRGManagerInfo = function(response,communicationObject){
					geddy.model.User.first({ id: communicationObject.program.crgProgramManager }, function (err, data) {
							if (data) {
								communicationObject.CRGManagerInfo = data;
							}

							if (communicationObject.crgProgramManager)
							{
								geddy.model.User.first({ email: communicationObject.crgProgramManager }, function (err, eventManager) {
									if (eventManager) {
										communicationObject.CRGEventManagerInfo = eventManager;
									}

									response.AddProgramClientInfo(response,communicationObject);
								});
							} else {
								communicationObject.CRGEventManagerInfo = {firstName: '', lastName:'', email:''};
								response.AddProgramClientInfo(response,communicationObject);
							}
						});
			}


			this.AddProgramClientInfo = function(response,communicationObject){
				communicationObject.programClientEmail = "";
				
				if (communicationObject.program.programClientEmail)
				{
					communicationObject.programClientEmail = communicationObject.program.programClientEmail;
				} 
				
				response.AddMailTemplateInfo(response,communicationObject);
			}


			this.AddMailTemplateInfo = function(response,communicationObject){
				var DBUtils = require('../helpers/utils/DBUtils.js');   
				var mailTemplateId = communicationObject.communicationDelivery.mailTemplate;
				var programId = communicationObject.programId;
				var filePath = getTemplateFilePath(programId);
				var fileName = mailTemplateId+".html";
				geddy.model.CommunicationTemplate.first({ id: mailTemplateId }, function (err, data) {
						if (data) {
							communicationObject.mailTemplateInfo = DBUtils.cloneObject(data) ;
							try{
								communicationObject.mailTemplateContent = fs.readFileSync(filePath+'/'+fileName, { encoding: 'utf-8' });
							}catch(ex){
								console.log(ex);
							} 
						}
						response.AddAttachmentTemplatesInfo(response,communicationObject,communicationObject.communicationDelivery.attachmentTemplates,[]);
					});
			}


			this.AddAttachmentTemplatesInfo = function(response,communicationObject,arrayAttachmentTemplates,result){
				var DBUtils = require('../helpers/utils/DBUtils.js');   
				if ((!arrayAttachmentTemplates) || arrayAttachmentTemplates.length == 0){
					// console.log(">>>>result:>>>>"+JSON.stringify(result));
					communicationObject.attachmentTemplates = result;

					//add external attachments info if any
					communicationObject.externalAttachments = communicationObject.communicationDelivery.externalAttachments;

					//make final callback
					response.ProcessCommunicationObject(response,communicationObject);
				}else{
					// console.log(">>>>arrayAttachmentTemplates:"+JSON.stringify(arrayAttachmentTemplates));
					// console.log(">>>>arrayAttachmentTemplates.length:"+JSON.stringify(arrayAttachmentTemplates.length));
					var currentTemplate = arrayAttachmentTemplates.shift();
					// console.log(">>>>>>currentTemplate"+JSON.stringify(currentTemplate));
					geddy.model.CommunicationTemplate.first({ id: currentTemplate }, function (err, data) {
							if (data) {
								var newTemplate = {};
								newTemplate.templateInfo = DBUtils.cloneObject(data);
								var programId = communicationObject.programId;
								var filePath = getTemplateFilePath(programId);
								var fileName = currentTemplate+".html";
								try{
									newTemplate.templateContent = fs.readFileSync(filePath+'/'+fileName, { encoding: 'utf-8' });
								}catch(ex){
									console.log(ex);
								} 

								// console.log(">>>>>>>>newTemplate:" + JSON.stringify(newTemplate));

								result.push(newTemplate);
							}
							return response.AddAttachmentTemplatesInfo(response,communicationObject,arrayAttachmentTemplates,result);
						});
				}
			}


			this.TestCommunicationObject = function (req, resp, params) {
					var $thisRequest = this;
					var result = {};
					var CommunicationDeliveryID = params.id;

					console.log("CommunicationDeliveryID>>>>>" + JSON.stringify(CommunicationDeliveryID));

					geddy.model.CommunicationDelivery.first({ id: CommunicationDeliveryID },
						function (err, communicationDelivery) {
							if (err){
								console.log("<<<<<<<<<<<<<<err>>>>>" + JSON.stringify(err));
							}
							if (communicationDelivery) {
								//$thisRequest.ProcessCommunicationObject = $thisRequest.createCommunicationFiles;
                			$thisRequest.ProcessCommunicationObject = $thisRequest.createIndividualCommunicationFiles;
								$thisRequest.BuildCommunicationsObject($thisRequest,communicationDelivery);
							}else{
								console.log("<<<<<<<<<<<<<<CommunicationDelivery NO ENCONTRADO>>>>>");
								$thisRequest.respond({}, { format: 'json' });
							}
					});          
			}


/***********************************/


	this.createAutomaticCommunicationFiles = function(response,communicationObject){
	    var HTML2PDF = require('../helpers/pdf/html2pdf.js');   
		var plainCommunicationObject = response.getPlainCommunicationObject(communicationObject);
		var pdfList = [];

		var MailContent = communicationObject.mailTemplateContent;
    	MailContent = ReplaceTagsTags(plainCommunicationObject,MailContent);
	    MailContent = ClearTags(MailContent);

		var mailTemplateId = communicationObject.communicationDelivery.mailTemplate;
		var communicationDeliveryId = communicationObject.communicationDelivery.id;
		var filePath = getRenderFilePath(communicationDeliveryId,true);
		var fileName = mailTemplateId+".html";

		//empty dir
		response.deleteDir(response,filePath);

		var bodyFilename = response.saveToFile(filePath,fileName,MailContent);

		var filesToConvert = [];
    	var currentAttachment;
		if (communicationObject.attachmentTemplates){

			for(var i=0; i < communicationObject.attachmentTemplates.length;i++){
				var AttachContent = communicationObject.attachmentTemplates[i].templateContent;
		        AttachContent = ReplaceTagsTags(plainCommunicationObject,AttachContent);
          		AttachContent = ClearTags(AttachContent);
				var attachTemplateId = communicationObject.attachmentTemplates[i].templateInfo.templateTitle;
				var communicationDeliveryId = communicationObject.communicationDelivery.id;
				var filePath = getRenderFilePath(communicationDeliveryId,true);
				var fileName = attachTemplateId+".html";
				console.log(">>>filePath"+filePath);
				console.log(">>>fileName"+fileName);
				response.saveToFile(filePath,fileName,AttachContent);
				filesToConvert.push(filePath+'/'+fileName); 
			}
		}

		HTML2PDF.convertMultipleFiles(
			filesToConvert,
			response.sendAutomaticCommunicationCallback,//the callback to send
			{
				bodyFilename:bodyFilename,
				response:response,
				communicationObject:communicationObject
			}
			);

		//this response is for test 
		response.respond(communicationObject
			, { format: 'json' });
	}


	this.sendAutomaticCommunicationCallback = function(attachmentsFiles,args){
		var communicationObject = args.communicationObject;
		var response = args.response;
		var bodyFilename = args.bodyFilename;

		console.log("READY TO SEND EMAILS");
		communicationObject.RepEmail = communicationObject.RepRequestingInfo.defaultEmail;

		if (communicationObject.RSMInfo)
   			communicationObject.RSMEmail = communicationObject.RSMInfo.defaultEmail;
   		else {
   			communicationObject.RSMEmail = '';
   		}

   		var to = [];
   		var cc = [];
   		switch(communicationObject.communicationDelivery.deliveryType){
   			case constants.PROGRAM_REQUEST_SUBMISSION:
   				//if manager is sending the submission, only email crg program manager
   				if (communicationObject.RSMInfo && communicationObject.RSMInfo.id == communicationObject.RepRequestingInfo.id)
   				{
   					to.push(communicationObject.CRGManagerInfo.email);
   				} else {
   					to.push(communicationObject.RepEmail);
   					cc.push(communicationObject.CRGManagerInfo.email);
   					cc.push(communicationObject.programClientEmail);

   				}
   				break;

   			case constants.PROGRAM_REQUEST_APPROVAL_DENIAL:
   				to.push(communicationObject.RSMEmail);
   				cc.push(communicationObject.CRGManagerInfo.email);
   				cc.push(communicationObject.programClientEmail);
   				break;

   			case constants.PROGRAM_REQUEST_APPROVAL:
   			case constants.PROGRAM_REQUEST_DENIAL:
   				to.push(communicationObject.RepEmail);
   				cc.push(communicationObject.CRGManagerInfo.email);
   				cc.push(communicationObject.programClientEmail);
   				break;

   			case constants.EVENT_REGISTRATION:
   				to.push(communicationObject.registeredAttendeeEmail);
   		}

   		console.log('>>>>>>>>>>>> Sending emails to: ' + JSON.stringify(to));
   		console.log('>>>>>>>>>>>> Sending emails CC: ' + JSON.stringify(cc));
   		
		response.sendCommunication({
			from: communicationObject.clientEmail,
			bodyFilename:bodyFilename,
			subject:communicationObject.mailTemplateInfo.subject,
			attachmentsFiles: attachmentsFiles,
			to: to,
			cc: cc
		});   		
	}


			this.TestAutimaticCommunicationObject = function (req, resp, params) {
					var $thisRequest = this;
					var result = {};
					var eventId = params.id;
					var currentProgram = $thisRequest.session.get('currentProgram');
					var deliveryType = "Program Request Submission";


					console.log("eventId>>>>>" + JSON.stringify(eventId));
					console.log("currentProgram>>>>>" + JSON.stringify(currentProgram));
					geddy.model.CommunicationProgramDelivery.first({ programId: currentProgram.id, deliveryType: deliveryType },
						function (err, communicationProgramDelivery) {
							if (err){
								console.log("<<<<<<<<<<<<<<err>>>>>" + JSON.stringify(err));
							}
							if (communicationProgramDelivery) {
								//$thisRequest.ProcessCommunicationObject = $thisRequest.createCommunicationFiles;
                				$thisRequest.ProcessCommunicationObject = $thisRequest.createAutomaticCommunicationFiles;
								$thisRequest.BuildAutomaticCommunicationsObject($thisRequest, eventId, communicationProgramDelivery);
							}else{
								console.log("<<<<<<<<<<<<<<communicationProgramDelivery NO ENCONTRADO>>>>>");
								$thisRequest.respond({}, { format: 'json' });
							}
					});          
			}


			this.BuildAutomaticCommunicationsObject = function(response,eventId,communicationProgramDelivery, registeredAttendeeEmail){
					var MJ = require("mongo-fast-join");
					var MongoClient = require('mongodb').MongoClient;
					var mongoJoin = new MJ();
					var connectionString = 'mongodb://'+geddy.config.db.mongo.host+':'+geddy.config.db.mongo.port+'/'+geddy.config.db.mongo.dbname;
					if (typeof(geddy.config.db.mongo.serverOptions)){
						if(geddy.config.db.mongo.serverOptions.ssl){
							connectionString += "?ssl=true";
						}
					}

					MongoClient.connect(connectionString, function(err, db) {
						if(err) throw err;
						var EventsCollection = db.collection('events');
						var ProgramCollection = db.collection('programs');
						mongoJoin.query(EventsCollection,
									{id:eventId}, //query statement
									{}, //fields
									{//limit: 10000//options
									}
							).join({
									joinCollection: ProgramCollection,
									//respects the dot notation, multiple keys can be specified in this array
									leftKeys: ["programId"],
									//This is the key of the document in the right hand document
									rightKeys: ["id"],
									//This is the new subdocument that will be added to the result document
									newKey: "program"
							}).exec(function (err, items) {    //Call exec to run the compiled query and catch any errors and results, in the callback
								//callback(response,items);
											var DBUtils = require('../helpers/utils/DBUtils.js');   
											var CommunicationsObject = DBUtils.cloneObject(items);
											if (CommunicationsObject.length > 0){
												CommunicationsObject = CommunicationsObject[0];
											}

											CommunicationsObject.clientEmail = response.session.get('currentClient').email;

											CommunicationsObject.communicationDelivery = communicationProgramDelivery;

											if (registeredAttendeeEmail)
											{
												CommunicationsObject.registeredAttendeeEmail = registeredAttendeeEmail;
											}

											db.close();
											
											response.AddAttendiesInfo(response,CommunicationsObject,CommunicationsObject.attendees,[]);
							});
						});
			}




/************************************/
		this.sendCommunication = function (args) {
				var body =  this.getCommunicationBody(args.bodyFilename);

				if (body) {
						var mailOptions = {
								from: args.from,
								to: args.to,
								subject: geddy.config.EmailSubjectPrefix + args.subject,
								html: body,

						};

						var today = new Date();
			            var month = today.getMonth();
			            var logData = '<p>Message sent on ' + month + '/' + today.getDate() + '/' + today.getFullYear();
			            logData += ' at ' + today.getHours() + ':' + today.getMinutes() + '</p>';

			            logData += '<p>Subject: ' + mailOptions.subject + '</p>';
			            logData += '<p>To: ' + mailOptions.to + '</p>';
						
						if (args.cc && args.cc != '')
						{
							mailOptions.cc =  args.cc;

							logData += '<p> Copy to: ' + args.cc + '</p>';

						}

						if (args.attachmentsFiles && args.attachmentsFiles.length > 0)
						{
							mailOptions.attachment = this.getAttachments(args.attachmentsFiles);
							
							logData += '<p> Attachments: </p>';

							var i;
							for (i = 0; i < mailOptions.attachment.length; i++) {
								logData += i + 1 + '. ' + mailOptions.attachment[i].filename + '<br>';
							};

							logData += '</p>';
						}

						// console.log('Sending email to MailGun: ' + JSON.stringify(mailOptions));

			             mailgun.messages().send(mailOptions, function (error, body) {
			                console.log(body);
			                console.log(error);
			                if (body)
			                {
								logData += '<p>Mailgun response: ' + body.message + '</p>';
							} else {
								logData += '<p>Mailgun response: ' + error.toString() + ': ' + JSON.stringify(error)+ '</p>';
							}
							logData += '<p>------------------------</p>';
							// console.log('Saving to file:   ' + logData);

							//save info to file
							var path = require('path');
							var filePath = path.join(geddy.config.CommunicationsFolder + 'log')

				            var fse = require('fs-extra');
							fse.ensureDirSync(filePath);
							
							var fullPath = filePath +'/mailgun_log.html';

				            fs.appendFile(fullPath, logData);
			            });
				} else {
						return false;
				}
		};

/*
		this.sendTestCommunicationDelivery = function sendTestCommunicationDelivery(args) {
				//console.log("sendTestCommunicationDelivery with ARGS >>>>>>>>>>>>>>>>"+JSON.stringify(args));
				var nodemailer = require("nodemailer");
				var transport = nodemailer.createTransport("SMTP", geddy.config.SMTP);
				var body =  this.getCommunicationBody(args.bodyFilename);

				//console.log('body data: ' + body);
				if (body) {
						//console.log("to :" + args.to);

						var mailOptions = {
								from: geddy.config.EmailSentFrom,
								to: args.to,
								subject: geddy.config.EmailSubjectPrefix + args.subject,
								html: body,
						};

						//console.log("mailOptions "+mailOptions);

						transport.sendMail(mailOptions, function () {
								//$thisRequest.respond(params, { format: 'json' });
								//Email Sent;
								return true;
						});
				} else {
						return false;
				}
		};
*/

		this.getCommunicationBody = function getCommunicationBody(filename) {
				var data = fs.readFileSync(filename, { encoding: 'utf-8' });
				console.log('received data: ' + data);
				return data;
		};


		this.getAttachments = function getAttachments(files){
				var path = require('path');
				var attachments = [];
				//var files = ["attach1.txt","attach2.txt","attach3.txt"];
				for (var i=0;i < files.length;i++){
						var filePath = files[i].filePath;
						var filename = filePath.slice(filePath.lastIndexOf('/') + 1);

						//check for external files attached
						var attachment = filename.indexOf('_attachment_')
						if ( attachment != -1)
						{
							filename = filename.slice(attachment + 12);
						}

						var attach = new mailgun.Attachment({
								filename:filename,
								data: filePath
						});
						attachments.push(attach);

				}
				console.log("attachments");
				console.log(JSON.stringify(attachments));
				return attachments;
		};





};

exports.Communications = Communications;
