var ContactTypes = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

  this.index = function (req, resp, params) {
    var self = this;

    geddy.model.ContactType.all(function(err, contactTypes) {
      if (err) {
        throw err;
      }
      self.respondWith(contactTypes, {type:'ContactType'});
    });
  };

  this.add = function (req, resp, params) {
    this.respond({params: params});
  };

  this.create = function (req, resp, params) {
    var self = this
      , contactType = geddy.model.ContactType.create(params);

    if (!contactType.isValid()) {
      this.respondWith(contactType);
    }
    else {
      contactType.save(function(err, data) {
        if (err) {
          throw err;
        }
        self.respondWith(contactType, {status: err});
      });
    }
  };

  this.show = function (req, resp, params) {
    var self = this;

    geddy.model.ContactType.first(params.id, function(err, contactType) {
      if (err) {
        throw err;
      }
      if (!contactType) {
        throw new geddy.errors.NotFoundError();
      }
      else {
        self.respondWith(contactType);
      }
    });
  };

  this.edit = function (req, resp, params) {
    var self = this;

    geddy.model.ContactType.first(params.id, function(err, contactType) {
      if (err) {
        throw err;
      }
      if (!contactType) {
        throw new geddy.errors.BadRequestError();
      }
      else {
        self.respondWith(contactType);
      }
    });
  };

  this.update = function (req, resp, params) {
    var self = this;

    geddy.model.ContactType.first(params.id, function(err, contactType) {
      if (err) {
        throw err;
      }
      contactType.updateProperties(params);

      if (!contactType.isValid()) {
        self.respondWith(contactType);
      }
      else {
        contactType.save(function(err, data) {
          if (err) {
            throw err;
          }
          self.respondWith(contactType, {status: err});
        });
      }
    });
  };

  this.remove = function (req, resp, params) {
    var self = this;

    geddy.model.ContactType.first(params.id, function(err, contactType) {
      if (err) {
        throw err;
      }
      if (!contactType) {
        throw new geddy.errors.BadRequestError();
      }
      else {
        geddy.model.ContactType.remove(params.id, function(err) {
          if (err) {
            throw err;
          }
          self.respondWith(contactType);
        });
      }
    });
  };

};

exports.ContactTypes = ContactTypes;
