var constants = require('../helpers/utils/constants.js');
var formidable = require('formidable');
var fs = require('fs');
var DBUtils = require('../helpers/utils/DBUtils.js');
var Utils = require('../helpers/utils/utils.js');
var ModelName= "Contact";



var util = require('util');

var Contacts = function () {
	this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

	this.before(function () { 
		var self = this;

		if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
		{
			geddy.globalDb.close();
		 	self.respond('false',{format: 'txt'});
		}
	});

	this.index = function (req, resp, params) {
		var self = this;

		self.session.set('activeController', self.name);
		
		DBUtils.unlockObject(self);

		var result={};
		self.session.set('currentContact',null);
		self.session.set('currentClient',null);
		self.session.set("clientIds", null);
		self.session.set('linkedtoClient',true);
		

		result.searchKolProperties = constants.SEARCH_KOL_PROPERTIES();
		result.selectPrompt = constants.SELECT_PROMPT();

		var contactCollection = geddy.globalDb.collection('contacts');
		var pageLimit = geddy.config.pageLimit;
		var searchObject = new Object({contactType: 'kol'});
		var currentPage = 1;
		var recordCount=0;
		var totalPages=0;

		var sort = {
			lastName: 'asc',
			firstName : 'asc'
    	}

		geddy.model.Client.all({createdAt: {ne: null}}, {sort: {name: 'asc'}}, function (err, clients) {
			if (err) {
				throw err;
			}
			result.clients = clients;
			self.session.set('allClients',clients);
			
			contactCollection.find(searchObject).toArray(function(err,allContacts){
				if (allContacts != null)
				{
					recordCount = allContacts.length;
					totalPages = Math.ceil(recordCount/pageLimit);
				}	
				
				contactCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,contacts){
					result.totalPages = totalPages;
					result.currentPage = currentPage;
					result.contacts = contacts;

					self.respond(result,{type:'Contact'});
				});
			});
		});
	};
	
	
	this.search = function (req, resp, params) {
        var $thisRequest = this;
        var search = require('../helpers/search/search.js');
        var sort = {
              lastName: 'asc',
              firstName : 'asc'
    	}	

    	var searchObject = new Object({contactType: 'kol'});

        search.AdvanceSearch($thisRequest, searchObject, sort, 'contacts', params);
	}


	this.searchAll = function (req, resp, params) {
        var $thisRequest = this;
        var search = require('../helpers/search/search.js');
        var sort = {
              lastName: 'asc',
              firstName : 'asc'
    	}	
    	var searchObject = new Object(); //var searchObject = new Object({contactType: 'attendee'});
        search.AdvanceSearch($thisRequest, searchObject, sort, 'contacts', params);
	}



	this.searchContactsByClientId = function (req, resp, params) 
	{
		var self = this;
		//console.log("<><<<<<<<<<<<<<<<<<< Params searchByClientId "+JSON.stringify(params.clientId));
		//var contactCollection  = self.session.get('contactCollection');
		var contactCollection = geddy.globalDb.collection('contacts');
		var pageLimit = geddy.config.pageLimit;
		var currentPage = 1;
		var recordCount=0;
		var totalPages=0;
		var result = {};

		var sort = {
			lastName: 'asc',
			firstName : 'asc'
    	}

		//searching object 
		var searchObject = {};

		if (params.clientId != '' && params.clientId != 'All Clients')
		{
			searchObject = {clients: {$elemMatch: {id:params.clientId}}, contactType:'kol'};//{"clients.id": params.clientId, contactType: 'kol'};
			//searchObject = new Object({contactType: 'kol'});
		}

		if (typeof contactCollection == "undefined" || !contactCollection)
		{
			self.session.set("contactCollection",geddy.globalDb.collection('contacts'));
			contactCollection  = self.session.get('contactCollection');
		}


		/*contactCollection.find(searchObject).sort(sort).toArray(function(err,contactList){
			self.respond(
					{dataset: contactList},
					{
						format:'json'
					}
			);
		});*/


		contactCollection.find(searchObject).toArray(function(err,allContacts){
			if (allContacts != null)
			{
				recordCount = allContacts.length;
				totalPages = Math.ceil(recordCount/pageLimit);
			}	
			contactCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,contacts){
				result.totalPages = totalPages;
				result.currentPage = currentPage;
				result.dataset = contacts;
				self.respond(result, {format:'json'});
			});
		});


		// var search = require('../helpers/search/search.js');
		//       var sort = {
		//             lastName: 'asc',
		//             firstName : 'asc'
		//   	}	

		// search.AdvanceSearch(self, searchObject, sort, 'contacts', params);

	};

	this.save = function (req, resp, params) {
		var self = this;

		//make sure we set ourselves into edit mode now..
		self.session.set('sessionContactMode', 'editingContact');
		
		//saving the current element without response 
		var callback = function() {
			//updateClientId(self,self.session.get('currentContact'));
			updateAllIds(self);

			//make sure the type is set correctly
			var currentSection = self.session.get('currentSection');
			var currentContact = self.session.get('currentContact');

			if (currentSection == constants.sections.EVENT_ATTENDEES)
			{
				console.log('>>>>>>>>>>>>>>>>>>> type:'+ currentContact.contactType);

				if (!currentContact.contactType)
				{
					console.log('setting type to attendee');
					//we're saving from the attendee section
					currentContact.contactType = 'attendee';
				}
			} else {
				console.log('setting type to KOL');
				currentContact.contactType = 'kol';
			}

			DBUtils.saveObject(self,ModelName);
		}

		console.log(" >>>>>>>>>contact save before calling  PARAMS<<<<<<<<<<<<"+JSON.stringify(params));
		DBUtils.saveObjectWithoutRespond(self,ModelName,params,callback);
	};


	this.edit = function (req, resp, params){
		var self = this;
		self.session.set('sessionContactMode', 'editingContact');

		this.respond(true,
			{format: 'json'
		});
	};
	
	
	this.setCurrentClient = function(req, resp, params) {
		var self = this;

		geddy.model.Client.first(params.id, function(err, client) {
			self.session.set('currentClient',client);
			self.session.set('linkedtoClient',true);

			self.respond({currentStep: self.session.get('currentStep')}, {
				format: 'json'
			});
		});
	};


	function updateAllIds(self)
	{
		var currentContact = self.session.get("currentContact");
		//update contact's properties that have clients list and add (if necessary), the new client Id
		updateClientId(self,currentContact);

		var i;
		if (currentContact.emailAddresses)
		{
			for (var i = 0; i < currentContact.emailAddresses.length; i++) {
				updateClientId(self,currentContact.emailAddresses[i]);
			};
		}

		if (currentContact.phoneNumbers)
		{
			for (var i = 0; i < currentContact.phoneNumbers.length; i++) {
				updateClientId(self,currentContact.phoneNumbers[i]);
			};
		}

		if (currentContact.addresses)
		{
			for (var i = 0; i < currentContact.addresses.length; i++) {
				updateClientId(self,currentContact.addresses[i]);
			};
		}

		if (currentContact.cvs)
		{
			for (var i = 0; i < currentContact.cvs.length; i++) {
				updateClientId(self,currentContact.cvs[i]);
			};
		}

		if (currentContact.bios)
		{
			for (var i = 0; i < currentContact.bios.length; i++) {
				updateClientId(self,currentContact.bios[i]);
			};
		}
		
		if (currentContact.factSheets)
		{
			for (var i = 0; i < currentContact.factSheets.length; i++) {
				updateClientId(self,currentContact.factSheets[i]);
			};
		}

		if (currentContact.contracts)
		{
			for (var i = 0; i < currentContact.contracts.length; i++) {
				updateClientId(self,currentContact.contracts[i]);
			};
		}

		if (currentContact.taxForms)
		{
			for (var i = 0; i < currentContact.taxForms.length; i++) {
				updateClientId(self,currentContact.taxForms[i]);
			};
		};
	}

	//adds (if needed), a clientId to the list of ids for specified model
	function updateClientId (self,modelInstance){
		var allClients = self.session.get('allClients');
		var currentClientId = self.session.get('currentClient').id;
		var modelsList = constants.MODELS_WITH_CLIENTS_LIST();

		var i;
		for (i = 0; i < modelsList.length; i++)
		{
			if (modelInstance && modelInstance.type == modelsList[i].name)
			{
				if (!modelInstance.clients)
				{
					modelInstance.clients = [];
				}

				//find client in allClients list first
				var j;
				var targetClient;
				for (var j = 0; j < allClients.length; j++) {
					if (currentClientId == allClients[j].id)
					{
						targetClient = {id:allClients[j].id, name: allClients[j].name};
						break;
					};
				};

				var addToList = true;
				var z;
				for (z = 0; z < modelInstance.clients.length; z++) {
					//check if id is already stored, if not, add it to the array
					if (modelInstance.clients[z].id == targetClient.id)
					{
						addToList = false;
						break;
					};
				}	

				if (addToList)
				{
					modelInstance.clients.push(targetClient);
				}

				break;
			};
		};
	}


	this.linkToNewClient = function (req, resp, params) {
		var self = this;
		
		//set currentClientId first
		geddy.model.Client.first(params.id, function(err, client) {
			self.session.set('currentClient', client);
			self.session.set('linkedtoClient',true);
			var currentContact = self.session.get('currentContact');

			//first make sure clientID is not already linked to contact
			var alreadyLinked = false;

			var i;
			for (i = 0; i < currentContact.clients.length; i++) {
				if (currentContact.clients[i].id == params.id)
				{
					alreadyLinked = true;
					break;
				}
			}
			
			if (!alreadyLinked)
			{
				updateAllIds(self);


				//save contact
				currentContact.save(function(err, data) {
					if (err) {
						throw err;
					}
					
					self.respond(true, {
						format: 'json'
					});
				});
			} else {
				self.flash.info('This KOL had been linked to the selected client previously.');

				self.respond(true, {
					format: 'json'
				});
			}
		});
		
	};


	this.saveEmbeddedModel = function (req, resp, params) {
		var self = this;
	  	var callback = function() {
			//updateClientId(self,self.session.get('currentContact'));
			updateAllIds(self);
			DBUtils.saveObject(self,ModelName);
		}

		if (params.targetProperty == "photos")
		{
			//save photo to user account also
			var currentContact = self.session.get('currentContact');

			DBUtils.savePhotoToUser(currentContact.id, params.value);
		}

	  	DBUtils.saveObjectEmbeddedModel(self,ModelName,params,callback);
		
	};

	this.saveDefaultItem = function (req, resp, params){
		var self = this;
		var currentContact = self.session.get("currentContact");

		currentContact[params.targetProperty] = params.value;

	    currentContact.save(function(err, data){
			if (params.targetProperty == 'defaultEmail' && data.contactType == 'kol')
			{
			  //send contact data to be saved as a user... if needed
			  var photo;
			  if (currentContact.photos && currentContact.photos.length > 0)
			  {
			  	photo = currentContact.photos[0].pathToFile;
			  }

			  DBUtils.saveAsUser(self, currentContact.firstName, currentContact.lastName, params.value, constants.roles.KOL.label, currentContact.id, photo);
			}

			self.respond(true, {
			  format: 'json'
			});
		});
	}


	this.deleteEmbeddedModel = function (req, resp, params) {
		var self = this;
	  	DBUtils.deleteEmbeddedModel(self,ModelName,params);
	};

	this.removeClientId = function(req, resp, params) {
		var self = this;
		var currentContact = self.session.get("currentContact");
		if (currentContact[params.targetProperty]) {
			//find element and remove client id from it
			var i;
			for (var i = 0; i < currentContact[params.targetProperty].length; i++) {
				var j;
				for (var j = 0; j < currentContact[params.targetProperty][i].clients.length; j++) {
					if (currentContact[params.targetProperty][i].clients[j].id == params.id)
					{
						//got it!  Le'ts remove it from the list
						currentContact[params.targetProperty][i].clients.splice(j, 1);

						currentContact.save(function(err, data) {
							if (err) {
								throw err;
							}
							self.respond({currentStep: self.session.get('currentStep')}, {
								format: 'json'
							});
						});
					} else {
						self.respond({currentStep: self.session.get('currentStep')}, {
							format: 'json'
						});
					}
				};
			}
		}
	};

	this.show = function (req, resp, params) {
		var self = this;

		var continueProcess = true;

		if (params.id == '1234')
	    {
	      if (self.session.get('currentContact'))
	      {
	        params.id = self.session.get('currentContact').id;
	      } else {
	        continueProcess = false;
	        self.respond('null',{format: 'txt'});
	      }
	    }

	    if (continueProcess)
	    {


			DBUtils.unlockObject(self);
			
			self.session.set("linkedtoClient", true);

			//get all clients
			geddy.model.Client.all({createdAt: {ne: null}}, {sort: {name: 'asc'}}, function (err, clients) {
				if (err) {
					throw err;
				}
				
				var otherParams = new Object();
				otherParams['clients'] = clients;
				otherParams['displayEdit'] = true;
				otherParams['displayLinks'] = true;
				otherParams.reps = [];
				otherParams.contracts = [];

				//get contact... we need to get its reps first
				geddy.model.Contact.first(params.id, function (err, contact) {
					if (err)
					{
						throw err;
					}

					//now get the reps for this contact
					if (contact.reps)
					{
						var repCollection = geddy.globalDb.collection('reps');

						var searchObject = {id: {$in: contact.reps}};// mongo search object

						var sort = {
						     lastName: 'asc',
						     firstName : 'asc'
						}  

						repCollection.find(searchObject).toArray(function(err, allReps){
			            if (allReps != null)
			            {
			            	otherParams.reps = allReps;
			            }

			            if (contact.contracts && contact.contracts.length > 0)
							{
								var contractsCollection = geddy.globalDb.collection('contracts');

								var searchObject = {id: {$in: contact.contracts}};// mongo search object

								contractsCollection.find(searchObject).toArray(function(err, allContracts){
					            if (allContracts != null)
					            {
					            	otherParams.contracts = allContracts;
									}

					            DBUtils.showObject(self,ModelName,params,constants.sections.CONTACT_DETAILS,otherParams);
					         });
							} else {
								DBUtils.showObject(self,ModelName,params,constants.sections.CONTACT_DETAILS,otherParams);
							}
				      });
					} else {
						if (contact.contracts && contact.contracts.length > 0)
						{
							var contractsCollection = geddy.globalDb.collection('contracts');

							var searchObject = {id: {$in: contact.contracts}};// mongo search object

							contractsCollection.find(searchObject).toArray(function(err, allContracts){
				            if (allContracts != null)
				            {
				            	otherParams.contracts = allContracts;
								}

				            DBUtils.showObject(self,ModelName,params,constants.sections.CONTACT_DETAILS,otherParams);
				         });
						} else {
							DBUtils.showObject(self,ModelName,params,constants.sections.CONTACT_DETAILS,otherParams);
						}
					}
				});
			});
		}
	};


	this.deleteContacts = function (req, resp, params) {
		var self = this;
	  	DBUtils.deleteObject(self,ModelName,params);
	};


	this.deleteCVs = function (req, resp, params) {
	    var self = this;
	    DBUtils.deleteUploadedFiles(self, params.ids, self.session.get('currentContact'), 'cvs');
		DBUtils.deleteFromArrayByIndex(self, params.ids, self.session.get('currentContact'), 'cvs');
	};

	this.deleteBios = function (req, resp, params) {
	    var self = this;
	    DBUtils.deleteUploadedFiles(self, params.ids, self.session.get('currentContact'), 'bios');
		DBUtils.deleteFromArrayByIndex(self, params.ids, self.session.get('currentContact'), 'bios');
	};

	this.deleteFactSheets = function (req, resp, params) {
	    var self = this;
	    DBUtils.deleteUploadedFiles(self, params.ids, self.session.get('currentContact'), 'factSheets');
		DBUtils.deleteFromArrayByIndex(self, params.ids, self.session.get('currentContact'), 'factSheets');
	};

	this.deleteW8W9s = function (req, resp, params) {
	    var self = this;
	    DBUtils.deleteUploadedFiles(self, params.ids, self.session.get('currentContact'), 'taxForms');
		DBUtils.deleteFromArrayByIndex(self, params.ids, self.session.get('currentContact'), 'taxForms');
	};

	this.deletePhotos = function (req, resp, params) {
	    var self = this;
	    DBUtils.deleteUploadedFiles(self, params.ids, self.session.get('currentContact'), 'photos');
		DBUtils.deleteFromArrayByIndex(self, params.ids, self.session.get('currentContact'), 'photos');
		DBUtils.deletePhotoFromUser(self.session.get('currentContact').id);
	};


	this.clearCurrentContactId = function (req, resp, params) {
		console.log(">>>>>>>>>>>>>>>>>>>>>>>> clearCurrentContactId");
		var self = this;
		self.session.set('currentClient', null);
		self.session.set("clientIds", null);
		self.session.set('linkedtoClient',false);
		self.session.set('sessionContactMode', 'newContact');
	  	DBUtils.clearCurrentObject(self,ModelName,params);
	};


	// Contact Step pages
	this.contactStep1 = function (req, resp, params) {
		var self = this;
		var salutations = constants.SALUTATIONS();
		var suffixes = constants.SUFFIXES();
		var states = constants.STATES();
		var selectPrompt = constants.SELECT_PROMPT();
		var clientIds = [];

		var sessionContactMode = self.session.get('sessionContactMode');
		if (sessionContactMode && sessionContactMode == "editingContact")
		{
			var currentContact = self.session.get('currentContact');

			if (currentContact && currentContact.id)
			{
				DBUtils.lockObject(self, currentContact.id);
			}

			self.session.set('linkedtoClient', true);

			var i;
			for (i = 0; i < currentContact.clients.length; i++)
			{
				clientIds.push({id: currentContact.clients[i].id, name: currentContact.clients[i].name});
			}

			self.session.set("clientIds",clientIds);

			if (!self.session.get('currentClient'))
			{
				self.session.set("currentClient",currentContact.clients[0]);  //default to first client Id in list
			}
		}

		self.session.set('currentStep', 1);

		self.respond( {
			salutations:salutations,
			suffixes:suffixes,
			states:states,
			selectPrompt:selectPrompt,
			mode:sessionContactMode, 
			stepTitle:createTitle(self),
			currentContact:currentContact,
			linkedtoClient: self.session.get("linkedtoClient"), 
			clientIds: clientIds, 
			allClients: self.session.get('allClients'),
			currentClient:self.session.get("currentClient"),
			allClients:self.session.get("allClients")},
			{format: 'html',template: 'app/views/contacts/contactStep1'});
	};

	this.contactStep2 = function (req, resp, params) {
		var self = this;
		var phoneTypes = constants.PHONE_NUMBER_TYPES();
		var states = constants.STATES();
		var countries = constants.COUNTRIES();
		var addressTypes = constants.ADDRESS_TYPES();
		var selectPrompt = constants.SELECT_PROMPT();

		self.session.set('currentStep', 2);

		this.respond({
			phoneTypes: phoneTypes,
		    states: states,
		    countries: countries,
		    selectPrompt:selectPrompt,
		    addressTypes: addressTypes,
		    linkedtoClient:self.session.get('linkedtoClient'),
		    clientIds: self.session.get('clientIds'),
		    currentContact:self.session.get('currentContact'),
		    allClients:self.session.get("allClients"),
			currentClient:self.session.get("currentClient"),		    
		    mode:self.session.get("sessionContactMode"),
          	stepTitle:createTitle(self)
          },
			{format: 'html',
		    template: 'app/views/contacts/contactStep2'}
		);
	};
	  
	  
	this.contactStep3 = function (req, resp, params) {
		var self = this;
	  	var phoneTypes = constants.PHONE_NUMBER_TYPES();
	  	var states = constants.STATES();
	  	var selectPrompt = constants.SELECT_PROMPT();

	  	self.session.set('currentStep', 3);

		this.respond({
			phoneTypes: phoneTypes,
			selectPrompt:selectPrompt,
			states: states,
		    clientIds: self.session.get('clientIds'),
		    linkedtoClient:self.session.get('linkedtoClient'),
		    clientIds: self.session.get('clientIds'),
		    currentContact:self.session.get('currentContact'),
		    allClients:self.session.get("allClients"),
		    currentClient:self.session.get("currentClient"),		    
		    mode:self.session.get("sessionContactMode"),
         	stepTitle:createTitle(self)
         }, 
			{format: 'html',
			template: 'app/views/contacts/contactStep3'}
	  	);
	};  

	this.contactStep4 = function (req, resp, params) {
	   	var self = this;
	   	var countries = constants.COUNTRIES();
	   	var selectPrompt = constants.SELECT_PROMPT();
	   	var currentClient = self.session.get("currentClient");
	   	var reps = [];

	   	self.session.set('currentStep', 4);

		//Get client first, then get each rep from its reps array
		geddy.model.Client.first(currentClient.id, function (err, client) {
			if (err)
			{
				throw err;
			}

			if (client.reps && client.reps.length > 0)
				{
				var totalRepsProcessed = 0;
				var i;
				for (i = 0; i < client.reps.length; i++) {
					geddy.model.Rep.first(client.reps[i], function (err, rep) {
						if (err)
						{
							totalRepsProcessed++;
						} else {
							totalRepsProcessed++;

							reps.push(rep);

							if (totalRepsProcessed == client.reps.length)
							{
								self.respond(
									{
										clientIds: self.session.get('clientIds'),
									    linkedtoClient:self.session.get('linkedtoClient'),
									    clientIds: self.session.get('clientIds'),
									    currentContact:self.session.get('currentContact'),
									    currentClient:currentClient,	
									    allClients:self.session.get("allClients"),	    
									    mode:self.session.get("sessionContactMode"),
										selectPrompt: selectPrompt,
										countries: countries,
										reps:reps,
					               stepTitle:createTitle(self)
									},
									{format: 'html',
									template: 'app/views/contacts/contactStep4'}
								);
							}
						}
					});
				};
			} else {
				self.respond(
					{
						clientIds: self.session.get('clientIds'),
					    linkedtoClient:self.session.get('linkedtoClient'),
					    clientIds: self.session.get('clientIds'),
					    currentContact:self.session.get('currentContact'),
					    currentClient:currentClient,	
					    allClients:self.session.get("allClients"),	    
					    mode:self.session.get("sessionContactMode"),
						selectPrompt: selectPrompt,
						countries: countries,
						reps:reps,
	               stepTitle:createTitle(self)
					},
					{format: 'html',
					template: 'app/views/contacts/contactStep4'}
				);
			}
		});
	};  

	this.contactstep5 = function (req, resp, params) {
		var self = this;
		var states = constants.STATES();
		var countries = constants.COUNTRIES();
		var selectPrompt = constants.SELECT_PROMPT();

		self.session.set('currentStep', 5);

		this.respond({
			selectPrompt:selectPrompt,
			states: states,
		    countries: countries,
		    clientIds: self.session.get('clientIds'),
		    linkedtoClient:self.session.get('linkedtoClient'),
		    clientIds: self.session.get('clientIds'),
		    currentContact:self.session.get('currentContact'),
		    currentClient:self.session.get("currentClient"),	
		    allClients:self.session.get("allClients"),	    
		    mode:self.session.get("sessionContactMode"),
			stepTitle:createTitle(self)},
		    {format: 'html',
			template: 'app/views/contacts/contactStep5'}
	  	);
	};  


	this.exportContacts = function(req, resp, params)
	{
       	var self = this;

      	self.respond( true,{format: 'html',template: 'app/views/export/export'});
	}

	this.getAllContactsForExport = function(req, resp, params){
		var self  = this;

		geddy.model.Contact.all(function(err, contacts){
       		if (err)
       		{
       			throw err;
       		}

       		self.respond({contacts: contacts}, {format: 'json'});
       	});
	}


	this.showImport = function (req, resp, params) {
		var self = this;

		self.session.set('importResult', { diff: [], newContacts: [], failedContacts: [], total: [] });

		self.session.set('currentKOLImportType', 'kol');

		var selectPrompt = constants.SELECT_PROMPT(); 
		self.respond({
			selectPrompt:selectPrompt,
			clients: self.session.get("allClients")},
		    {format: 'html',
			template: 'app/views/contacts/import'}
	  	);
	};  


	this.setCurrentKOLTypeForImport = function (req, resp, params) {
		var self = this;

		self.session.set('currentKOLImportType', params.type);

		self.respond(true, {format:'json'});
	}


	this.import = function (req, resp, params) {
	  var self = this;

	  self.session.set('importResult', { diff: [], newContacts: [], failedContacts: [], total: [],totalCnt:0,processed:[] });
	  var importResult = self.session.get('importResult');
	  
	  

	  var form = new formidable.IncomingForm();
	  form.uploadDir = geddy.config.UploadTempFile;
	  //console.log(" SALIDA ->" + "FILE");

	  form.on('end', function () {
	      //console.log('FILE DONE');
	  });

	  form.parse(req, function (err, fields, files) {
	      //console.log(" SALIDA ->" + JSON.stringify(files["ExcelFile"]));
	      var path = files.ExcelFile.path;
	      //console.log(" Filename ->" + path);

	      var XLSX = require('xlsx')
	      var xlsx = XLSX.readFile(path);
	      var sheet_name_list = xlsx.SheetNames;

	      var sheet = 0;

	      xlsx.SheetNames.forEach(function (y) {
	          sheet++;
	          var array1 = XLSX.utils.sheet_to_row_object_array(xlsx.Sheets[y]);

	          array1.forEach(function (data) {
				  console.log(data);
	              importResult.total.push(data);
	          });
	      });

	      //remove first item, since this is the sample row in template
	      importResult.total.shift();
		  importResult.totalCnt=importResult.total.length;
	      console.log("New contact db->" + JSON.stringify(importResult.total));
	      ProcessImport(importResult.total.shift(), self, self.session.get('currentClient'));

	  });

	};


      // Final task (same in all the examples)
      var FinalImport = function (self) {
          //console.log('Done');

          var importResult = self.session.get('importResult');

          self.respond(importResult, { format: 'json' });
      }

      var ProcessImport = function (currentContact, self, client) {
          if (currentContact) {
          	var importResult = self.session.get('importResult');

          	var kolImportType = self.session.get('currentKOLImportType');

              geddy.model.Contact.all({ firstName: currentContact["First Name"], lastName: currentContact["Last Name"] , middleName: currentContact["Middle"], contactType: kolImportType},
                   function (err, contacts) {
                   		
                       if (err) {
                           console.log(err);
                       }
                       else if (contacts.length > 0) {
                       		var i;
                        	for (i = 0; i < contacts.length; i++) {
                       	  	   handleImportedContact(contacts[i], currentContact, kolImportType, client.id, client.name, importResult)
                           	}
                       } else {
						   console.log(currentContact);
                           handleImportedContact(null, currentContact, kolImportType, client.id, client.name, importResult)
                       }

                       return ProcessImport(importResult.total.shift(), self, client);
                   });

          }
          else {
              return FinalImport(self);
          }
      };


      function handleImportedContact(newContact, currentContact, kolImportType, clientId, clientName, importResult)
      {
      	if (!newContact)
      		newContact = geddy.model.Contact.create();

      	newContact.clients = [{id: clientId, name: clientName}];
		newContact.contactType = kolImportType;


		if (!currentContact["First Name"] || !currentContact["Last Name"] || !currentContact["Middle"]) {
			importResult.failedContacts.push(currentContact);
			return false;
		}
		importResult.processed.push(currentContact);

		newContact.firstName = currentContact["First Name"].trim();
		newContact.lastName = currentContact["Last Name"].trim();
		newContact.middleName = currentContact["Middle"].trim();
		newContact.suffix = currentContact["Suffix"];

		console.log('Handling: ' + newContact.firstName + ' ' + newContact.lastName);

		var degrees = currentContact["Degrees"];

		if (degrees && degrees != '')
		{
		   newContact.degrees = [];

		   //clear any spaces first
		   degrees = degrees.replace(/, /g, ',');
		   newContact.degrees = degrees.split(',');
		}

		newContact.salutation = currentContact["Salutation"];

		newContact.therapeuticInterests = new Array();

		var areas = currentContact["Therapeutic Area/ Area(s) of Interest"];

		if (areas && areas != '')
		{
		    //clear any spaces first
		   areas = areas.replace(/, /g, ',');
		   newContact.therapeuticInterests = areas.split(',');
		}

		newContact.tsaName = currentContact["TSA: Name on Photo"];
		newContact.tsaGender = currentContact["TSA: Gender"];

		newContact.tsaDOB = currentContact["TSA: Date of Birth (DOB)"];

		newContact.dietaryRestrictions = currentContact["Dietary Restrictions"];
		newContact.specialNeeds = currentContact["Special Needs"];


		newContact.npi = currentContact["NPI # (10-digits)"];
		newContact.dea = currentContact["DEA #"];
		newContact.meNumber = currentContact["ME #"];

		newContact.notes = currentContact["Notes"];


		/*Address*/
		var newAddress = geddy.model.Address.create();
		newAddress.addressType = currentContact["Address Type"];
		newAddress.preferred = 'true';
		newAddress.address1 = currentContact["Address 1"];
		newAddress.address2 = currentContact["Address 2"];
		newAddress.city = currentContact["City"];
		newAddress.state = currentContact["State"];
		newAddress.postalCode = currentContact["Zip"];
		newAddress.country = currentContact["Country"];
		newContact.addresses = new Array();
		newContact.addresses.push(newAddress);
		newContact.defaultAddress = currentContact["Address Type"];

		/*Phones*/
		newContact.phoneNumbers = new Array();
		var newPhone;

		if (currentContact["Office Phone"]) {
		   newPhone = geddy.model.ContactPhoneNumber.create();
		   newPhone.phoneType = "Office";
		   newPhone.number = Utils.formatPhoneNumber(currentContact["Office Phone"]);
		   newPhone.preferred = 'true';
		   newContact.phoneNumbers.push(newPhone);
		   newContact.defaultPhoneNumber = newPhone.number;
		}


		if (currentContact["Fax"]) {
		   newPhone = geddy.model.ContactPhoneNumber.create();
		   newPhone.phoneType = "Fax";
		   newPhone.number = Utils.formatPhoneNumber(currentContact["Fax"]);
		   newContact.phoneNumbers.push(newPhone);
		}

		if (currentContact["Home"]) {
		   newPhone = geddy.model.ContactPhoneNumber.create();
		   newPhone.phoneType = "Home";
		   newPhone.number = Utils.formatPhoneNumber(currentContact["Home"]);
		   newContact.phoneNumbers.push(newPhone);
		}


		if (currentContact["Cell"]) {
		   newPhone = geddy.model.ContactPhoneNumber.create();
		   newPhone.phoneType = "Cell";
		   newPhone.number = Utils.formatPhoneNumber(currentContact["Cell"]);
		   newContact.phoneNumbers.push(newPhone);
		}

		newContact.specialties = new Array();
		if (currentContact["Specialty 1"]) {
		   newContact.specialties.push(currentContact["Specialty 1"]);
		}
		if (currentContact["Specialty 2"]) {
		   newContact.specialties.push(currentContact["Specialty 2"]);
		}
		if (currentContact["Specialty 3"]) {
		   newContact.specialties.push(currentContact["Specialty 3"]);
		}

		newContact.medicalLicenses = new Array();
		var medicalLicense;
		if (currentContact["Medical License #1"]) {
		   medicalLicense = geddy.model.MedicalLicense.create();
		   medicalLicense.state = currentContact["Licensing State #1"];
		   medicalLicense.number = currentContact["Medical License #1"];
		   newContact.medicalLicenses.push(medicalLicense);
		}

		if (currentContact["Medical License #2"]) {
		   medicalLicense = geddy.model.MedicalLicense.create();
		   medicalLicense.state = currentContact["Licensing State #2"];
		   medicalLicense.number = currentContact["Medical License #2"];
		   newContact.medicalLicenses.push(medicalLicense);
		}


		newContact.emailAddresses = new Array();
		var eMailAddress;
		if (currentContact["Preferred Email Address"]) {
		   eMailAddress = geddy.model.ContactEmailAddress.create();
		   eMailAddress.email = currentContact["Preferred Email Address"];
		   eMailAddress.prefered = 'true';
		   newContact.emailAddresses.push(eMailAddress);
		   newContact.defaultEmail = eMailAddress.email;
		}

		if (currentContact["Seconday Email Address"]) {
		   eMailAddress = geddy.model.ContactEmailAddress.create();
		   eMailAddress.email = currentContact["Seconday Email Address"];
		   eMailAddress.prefered = 'false';
		   newContact.emailAddresses.push(eMailAddress);
		}


		if (currentContact["Office Staff Contact"]) {
		   newContact.officeStaffContact = new Array();

		   var officeStaffContact = geddy.model.ContactInfo.create();
		   officeStaffContact.fullName = currentContact["Office Staff Contact"];
		   officeStaffContact.phoneNumbers = new Array();

				if (currentContact["Office Staff Email"]) {
					officeStaffContact.email = currentContact["Office Staff Email"];
				}

				if (currentContact["Office Staff Tel #"]) {
		   		newPhone = geddy.model.ContactPhoneNumber.create();
		   		newPhone.number = Utils.formatPhoneNumber(currentContact["Office Staff Tel #"]);
		   		newPhone.phoneType = currentContact["Office Staff Tel # Type"];
		   		officeStaffContact.phoneNumbers.push(newPhone);
		   	}

				newContact.officeStaffContact.push(officeStaffContact);
			}

			if (currentContact["Emergency Contact Name"]) {

		   newContact.emergencyContacts = new Array();
		   var emergencyContact = geddy.model.ContactInfo.create();
		   emergencyContact.fullName = currentContact["Emergency Contact Name"];
		   emergencyContact.phoneNumbers = new Array();

				if (currentContact["Emergency Contact Name"]) {

		       newPhone = geddy.model.ContactPhoneNumber.create();
		       newPhone.number = Utils.formatPhoneNumber(currentContact["Emergency Contact #"]);
		       newPhone.phoneType = currentContact["Emergency Contact Phone Type"];
		       emergencyContact.phoneNumbers.push(newPhone);
		   }
		   
		   newContact.emergencyContacts.push(emergencyContact);
		}


		if (currentContact["Affiliation"]) {
		   newContact.affiliations = new Array();
		   var affiliation = geddy.model.Affiliation.create();
		   affiliation.affiliationName = currentContact["Affiliation"];
		   affiliation.title = currentContact["Title"];
		   affiliation.city = currentContact["City"];
		   affiliation.state = currentContact["State"];

		   newContact.affiliations.push(affiliation);
		}


		if (currentContact["Additional Affiliation"]) {
		   affiliation = geddy.model.Affiliation.create();
		   affiliation.affiliationName = currentContact["Additional Affiliation"];
		   affiliation.title = currentContact["Additional Title"];
		   affiliation.city = currentContact["Additional City"];
		   affiliation.state = currentContact["Additional State"];
		   newContact.affiliations.push(affiliation);
		}


		if (currentContact["Frequent Flyer Airline"]) {
		   newContact.airlines = new Array();
		   var travelAir = geddy.model.TravelAir.create();
		   travelAir.airlineName = currentContact["Frequent Flyer Airline"];
		   travelAir.ffNumber = currentContact["Frequent Flyer #"];
		   newContact.airlines.push(travelAir);
		}

		importResult.newContacts.push(newContact);

   	   	newContact.save(function (err, data) {
		   if (err) {
		       console.log(JSON.stringify(err));
		       throw err;
		   }
		});
      }

      this.saveDiff = function (req, resp, params) {
          var $thisRequest = this;
          //console.log('Done');

          var currentClient = $thisRequest.session.get('currentClient');

          geddy.model.Contact.first(params.id, function (err, contact) {
              if (err) {
                  //throw err;
                  console.log(err);
              }
              if (!contact) {
                  throw new geddy.errors.NotFoundError();
              }
              else {


              	//figure out if we need to add a client to this contact
              	if (contact.clients)
              	{
              		var i;
              		var found = false;
              		for (i = 0; i < contact.clients.length; i++) {
              			if (contact.clients[i].id == currentClient.id)
              			{
              				found = true;
              				break;
              			}
              		};
              	} else {
              		contact.clients = new Array();
              	}

          		if (!found)
          		{
          			//add the client to the list
          			contact.clients.push ({id: currentClient.id, name: currentClient.name});
          		}

	          		contact.firstName = params.firstName;
	          		contact.middleName = params.middleName;
	          		contact.lastName = params.lastName;

                  contact.suffix = params.suffix;

                  if (contact.degrees && contact.degrees[0]) {
                      contact.degrees = params.degrees.split(',');
                  }
                  contact.salutation = params.salutation;

                  contact.npi = params.npi;
                  contact.dea = params.dea;
                  contact.meNumber = params.meNumber;
                  contact.tsaName = params.tsaName;
                  contact.tsaGender = params.tsaGender;
                  contact.tsaDOB = params.tsaDOB;
                  contact.dietaryRestrictions = params.dietaryRestrictions;
                  contact.specialNeeds = params.specialNeeds;
                  contact.notes = params.notes;


                  if (contact.affiliations && contact.affiliations[0]) {
                      contact.affiliations[0].affiliationName = params.affiliation[0];
                      contact.affiliations[0].title = params.affiliationTitle[0];
                      contact.affiliations[0].city = params.affiliationCity[0];
                  }

                  if (contact.affiliations && contact.affiliations[1]) {
                      contact.affiliations[1].affiliationName = params.affiliation[1];
                      contact.affiliations[1].title = params.affiliationTitle[1];
                      contact.affiliations[1].city = params.affiliationCity[1];
                  }

                  if (contact.emailAddresses && contact.emailAddresses[0]) {
                      contact.emailAddresses[0].email = params.email[0];
                  }


                  if (contact.phoneNumbers) {
                      for (var i = 0; i < contact.phoneNumbers.length; i++) {
                          contact.phoneNumbers[i].number = params.phoneNumber[i];
                      }
                  }

                  if (contact.addresses) {
                      for (var i = 0; i < contact.addresses.length; i++) {
                          contact.addresses[i].addressType = params.addressType[i];
                          contact.addresses[i].address1 = params.address1[i];
                          contact.addresses[i].address2 = params.address2[i];
                          contact.addresses[i].city = params.city[i];
                          contact.addresses[i].state = params.state[i];
                          contact.addresses[i].postalCode = params.zip[i];
                          contact.addresses[i].country = params.country[i];
                      }
                  }

                  if (contact.officeStaffContact) {
                      contact.officeStaffContact.fullName = params.officeStaffContact;
                      contact.officeStaffContact.email = params.officeStaffContactEmail;
                      if (contact.officeStaffContact.phoneNumbers) {
                          contact.officeStaffContact.phoneNumbers[0].number = params.officeStaffContactPhone;
                      }
                  }


                  if (contact.emergencyContacts && contact.emergencyContacts[0]) {
                      contact.emergencyContacts[0].fullName = params.emergencyContactName;
                      if (contact.emergencyContacts[0].phoneNumbers) {
                          contact.emergencyContacts[0].phoneNumbers[0].number = params.emergencyContactPhones;
                      }
                  }


                  if (contact.specialties) {
                      if (contact.specialties[0]) {
                          params.specialty[0];
                      } else contact.specialties.push(params.specialty[0]);
                      if (contact.specialties[1]) {
                          params.specialty[1];
                      } else contact.specialties.push(params.specialty[1]);
                      if (contact.specialties[2]) {
                          params.specialty[2];
                      } else contact.specialties.push(params.specialty[2]);
                  }

                  if (contact.therapeuticInterests && contact.therapeuticInterests[0]) {
                      contact.therapeuticInterests = params.therapeuticInterests.split(',');
                  }

                  if (contact.medicalLicenses && contact.medicalLicenses[0]) {
                      contact.medicalLicenses[0].number = params.medicalLicenses[0];
                      contact.medicalLicenses[0].state = params.medicalLicenseState[0];
                  }

                  if (contact.medicalLicenses && contact.medicalLicenses[1]) {
                      contact.medicalLicenses[1].number = params.medicalLicenses[1];
                      contact.medicalLicenses[1].state = params.medicalLicenseState[1];
                  }


                  contact.save(function (err, data) {
                      if (err) {
                          console.log(JSON.stringify(err));
                          throw err;
                      } else {
                          $thisRequest.respond(params, { format: 'json' });
                      }
                  });

              }
          });
      };






};

function createTitle (self) {
   var headerTitle = "";
   var currentContact = self.session.get("currentContact");
   if(currentContact) {
      if((currentContact.firstName) || (currentContact.lastName)){
         headerTitle = currentContact.firstName + ' ' + currentContact.lastName + ' - ';
      }
   } else {   
      headerTitle = 'New KOL - '
   }

   return headerTitle;
}




function mapAction(actionToMap)
{
	//index,search should be the default 
	returnValue = 'list';
	switch (actionToMap)
	{
		//index
		case 'save':
		case 'contactStep1':
		case 'contactStep2':
		case 'contactStep3':
		case 'contactStep4':
		case 'contactStep5':
		case 'saveEmbeddedModel':
		case 'saveSubModel':
		case 'clearCurrentContactId':
		case 'edit':
		case 'deleteEmbeddedModel'://edit contact
		case 'setCurrentClient':
		case 'updateAllClientsIds':
		case 'updateAllIds':
		case 'updateClientId':
		case 'linkToNewClient':
			returnValue = 'edit'; // should be actions located in the constants file
			break;
		case 'show':
			returnValue = 'view'; // should be actions located in the constants file
			break;
		case 'deleteContacts':
			returnValue = 'delete'; // should be actions located in the constants file
			break;
	}
	return returnValue;
}




exports.Contacts = Contacts;
