var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var ModelName= "Contract";

var Contracts = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

   this.before(function () { 
    var self = this;
    if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
    {
     geddy.globalDb.close();
      self.respond('false',{format: 'txt'});
    }
    else
    {
      self.session.set('activeController', self.name);
    }
    });
   
  this.index = function (req, resp, params) {
    var self = this;

    DBUtils.unlockObject(self);

    self.session.set('currentContract', null);

    var result = {};
    // self.session.set('allPrograms', []);
    // var allPrograms = self.session.get('allPrograms');

    self.session.set('contractsArray', []);
    var contractsArray = self.session.get('contractsArray');

    var clientContacts;

    self.session.set('currentSection', constants.sections.CLIENT_CONTRACTS);

    var currentClient = self.session.get('currentClient');
    var currentProgram = self.session.get('currentProgram');

    //Get all the client's contacts
    var contactCollection = geddy.globalDb.collection('contacts');
    
    var searchObject = {"clients.id": currentClient.id, contactType: 'kol'};
    contactCollection.find(searchObject).toArray(function(err, contacts){
      if (err) {
        throw err;
      }

      clientContacts = contacts;
      self.session.set('clientContacts', contacts);

      //Get all the correct contact's contracts
      if (self.session.get('currentSection') == constants.sections.CLIENT_CONTRACTS)
      {
        totalIdsProcessed = 0;

        if (currentClient.contracts && currentClient.contracts.length > 0)
        {
          for (i = 0; i < currentClient.contracts.length; i++) 
          {
            //get program
            geddy.model.Contract.first( currentClient.contracts[i], function(err, contract) {
              if (err) {
                totalIdsProcessed++;
              } else {

                //store the result
                contractsArray.push(contract);

                totalIdsProcessed++;

                //check if all have been processed
                if (totalIdsProcessed == currentClient.contracts.length)
                {
                  result.contracts = contractsArray;
                  result.contacts = clientContacts;
                  result.currentClient = currentClient;

                  self.respond(result,{type:'Contract'});
                }
              }
            });
          }
        } else {
          result.contracts = contractsArray;
          result.contacts = clientContacts;
          result.currentClient = currentClient;
          self.respond(result,{type:'Contract'});
        }
      } else {
        //get contracts from currentProgram
        //Get program's contracts
        totalIdsProcessed = 0;

        for (i = 0; i < currentProgram.contracts.length; i++) 
        {
          //get contract
          geddy.model.Contract.first( currentProgram.contracts[i], function(err, contract) {
            if (err) {
              totalIdsProcessed++;
            } else {

              //store the result
              contractsArray.push(contract);

              totalIdsProcessed++;

              //check if all have been processed
              if (totalIdsProcessed == currentProgram.contracts.length)
              {
                 result.contracts = contractsArray;
                  result.contacts = clientContacts;
                  result.currentProgram = currentProgram;
                  self.respond(result,{type:'Contract'});
              }
            }
          });
        }
      }
    });
  };


  this.show = function (req, resp, params) {
    var self = this;

    DBUtils.unlockObject(self);

    var resultObject = new Object();
    resultObject ['contacts'] = self.session.get('clientContacts');
    resultObject ['currentClient'] = self.session.get('currentClient');

    DBUtils.showObject(self,ModelName,params,constants.sections.CLIENT_CONTRACTS, resultObject);
  };
  

  this.save = function (req, resp, params) {
    var self = this;
    var resultObject;
    var contactName = '';
    var clientContacts = self.session.get("clientContacts");
    var currentClient = self.session.get("currentClient");
    var currentProgram = self.session.get("currentProgram");

    //if saving contactId, then let's get the contact's name so we can return it to the display
    if (params.contactId != null)
    {
      for (i = 0; i < clientContacts.length; i++) {
        if (clientContacts[i].id == params.contactId)
        {
          contactName =  clientContacts[i].firstName + ' ' + clientContacts[i].lastName;
          self.session.set('currentContact', clientContacts[i]);
          break;
        }
      };

      resultObject = new Object();
      resultObject ['contactName'] = contactName;
    } 

    var currentContact = self.session.get("currentContact");

    DBUtils.saveObject(self,ModelName,params, resultObject);

    //get currentContract after we save it
    var currentContract = self.session.get("currentContract");

    if (self.session.get('currentSection') == constants.sections.CLIENT_CONTRACTS)
    {
      DBUtils.saveDataTo(self, 'Client', currentClient.id, 'contracts', currentContract.id);
    } else {
      DBUtils.saveDataTo(self, 'Program', currentProgram.id, 'contracts', currentContract.id);
    }

    //finally save contract to contact
    DBUtils.saveDataTo(self, 'Contact', currentContact.id, 'contracts', currentContract.id);
  }


  this.saveEmbeddedModel = function (req, resp, params) {
    var self = this;

    DBUtils.saveObjectEmbeddedModel(self,ModelName,params);

    var currentClient = self.session.get("currentClient");
    var currentProgram = self.session.get("currentProgram");
    var currentContract = self.session.get("currentContract");

    if (self.session.get('currentSection') == constants.sections.PROGRAM_SPEAKERS)
    {
      //saving a contract at program level, we need to "link" contract to program
      currentContract.linkedTo = currentProgram.id;
      currentContract.contactId = self.session.get('currentContact').id;
    } 

    //store contract id in client or program also
    if (self.session.get('currentSection') == constants.sections.CLIENT_CONTRACTS)
    {
      DBUtils.saveDataTo(self, 'Client', currentClient.id, 'contracts', self.session.get("currentContract").id);
    } else {
      DBUtils.saveDataTo(self, 'Program', currentProgram.id, 'contracts', self.session.get("currentContract").id);
    }
  }


  this.deleteContracts = function (req, resp, params) {
    var self = this;
    DBUtils.deleteObject(self,ModelName,params);
  };


  this.deleteContractFromClientOrProgram = function (req, resp, params) {
    var self = this;

    var totalIdsProcessed = 0;

    //delete from Contact
    var i;
    for (i = 0; i < params.ids.length; i++) {
      geddy.model.Contract.first( params.ids[i], function(err, contract) {
        if (err) {
          throw err;
        } else {
          //get the contact
          if (contract.contactId)
          {
            geddy.model.Contact.first( contract.contactId, function(err, contact) {
              if (err) {
                throw err;
              } else {
                DBUtils.deleteFromArrayByValue(self, params.ids, contact, 'contracts');
              }

              totalIdsProcessed++;

              if (totalIdsProcessed == params.ids.length)
              {
                if (self.session.get('currentSection') == constants.sections.CLIENT_CONTRACTS)
                {
                  if (contract.contractFile && contract.contractFile.length > 0) {
                    DBUtils.deleteUploadedFile(self, contract.contractFile[0].pathToFile);
                  }
                  DBUtils.deleteFromArrayByValue(self, params.ids, self.session.get('currentClient'), 'contracts');
                } else {
                  if (contract.contractFile && contract.contractFile.length > 0) {
                    DBUtils.deleteUploadedFile(self, contract.contractFile[0].pathToFile);
                  }
                  DBUtils.deleteFromArrayByValue(self, params.ids, self.session.get('currentProgram'), 'contracts');
                }
              }
            });
          } else {
            if (self.session.get('currentSection') == constants.sections.CLIENT_CONTRACTS)
            {
              if (contract.contractFile && contract.contractFile.length > 0) {
                DBUtils.deleteUploadedFile(self, contract.contractFile[0].pathToFile);
              }
              DBUtils.deleteFromArrayByValue(self, params.ids, self.session.get('currentClient'), 'contracts');
            } else {
              if (contract.contractFile && contract.contractFile.length > 0) {
                DBUtils.deleteUploadedFile(self, contract.contractFile[0].pathToFile);
              }
              DBUtils.deleteFromArrayByValue(self, params.ids, self.session.get('currentProgram'), 'contracts');
            }
          }  
        }
      });
    };
  }

  this.clearCurrentContract = function (req, resp, params) {
    var self = this;
    DBUtils.clearCurrentObject(self,ModelName,params);
   };


  // Contract Step pages
  this.contractStep1 = function (req, resp, params) {
    var self = this;

    var currentContract = self.session.get('currentContract');

    if (currentContract && currentContract.id)
    {
      DBUtils.lockObject(self, currentContract.id);
    }

    self.respond( {
      stepTitle: createTitle(self.session.get('currentContract')),
      contacts: self.session.get('clientContacts'),
      currentClient: self.session.get('currentClient'),
      currentContract: currentContract},
      {format: 'html',template: 'app/views/contracts/contractStep1'}
    );
  };

  this.contractStep2 = function (req, resp, params) {
    var self = this;

    var selectPrompt = constants.SELECT_PROMPT();
    var contractStatusTypes = constants.CONTRACT_STATUS_TYPES();

    self.respond( {
      stepTitle: createTitle(self.session.get('currentContract')),
      selectPrompt:selectPrompt,
      contractStatusTypes: contractStatusTypes,
      currentClient: self.session.get('currentClient'),
      contracts: self.session.get('contractsArray'),
      currentContract: self.session.get('currentContract')}, 
      {format: 'html', template: 'app/views/contracts/contractStep2'}
    );
  };


  //////////////  Program Contracts  ////////////////
  this.programContractStep1 = function (req, resp, params) {
    var self = this;

    DBUtils.unlockObject(self);

    //get contact's name to display on steps's header
    geddy.model.Contact.first(params.contactId, function (err, contact) {
      if (err) {
        throw err;
      } 

      //set current Contact
      self.session.set('currentContact', contact);

      var selectPrompt = constants.SELECT_PROMPT();
      var contractStatusTypes = constants.CONTRACT_STATUS_TYPES();

      var title = createTitle(self.session.get('currentContract'));
      title += contact.firstName + " " + contact.lastName;

      // //let's see if we have a contract in place or not
      // geddy.model.Contract.first({contactId: params.contractId}, function (err, contract) {
      //     if (err)
      //     {
      //       throw err;
      //     }




      if (params.contractId)
      {
        geddy.model.Contract.first(params.contractId, function (err, contract) {
          if (err)
          {
            throw err;
          }

          self.session.set('currentContract', contract);

          self.respond( {
            stepTitle: title,
            selectPrompt:selectPrompt,
            contractStatusTypes: contractStatusTypes,
            currentClient: self.session.get('currentClient'),
            currentProgram: self.session.get('currentProgram'),
            currentContract: self.session.get('currentContract')},
            {format: 'html',template: 'app/views/contracts/programContractStep1'}
          );
        })
      } else {
        self.respond( {
          stepTitle: title,
          selectPrompt:selectPrompt,
          contractStatusTypes: contractStatusTypes,
          currentClient: self.session.get('currentClient'),
          currentProgram: self.session.get('currentProgram'),
          currentContract: self.session.get('currentContract')},
          {format: 'html',template: 'app/views/contracts/programContractStep1'}
        );
      }
    });
  };

 function createTitle (currentContract) {

   var headerTitle = "";

   if(currentContract) {
      if(currentContract.contractName){
         headerTitle = currentContract.contractName + ' - ';
      }
   } else {   
      headerTitle = 'New Contract - '
   }

   return headerTitle;
  }

  this.search = function (req, resp, params) {
    var $thisRequest = this;
    var search = require('../helpers/search/search.js');


    search.AdvanceSearch($thisRequest, null, null,'contracts',params);
  }
};

exports.Contracts = Contracts;
