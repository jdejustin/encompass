var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var search = require('../helpers/search/search.js');
var ModelName= "Event";


var Events = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

   this.before(function () { 
    var self = this;
    if(typeof self.session.get('userRoleId') =='undefined' || !self.session.get('userRoleId'))
    {
        geddy.globalDb.close();
        self.respond('false',{format: 'txt'});
    }
    else
    {
      self.session.set('activeController', self.name);
    }
    });
   
  this.index = function (req, resp, params) {
    var self = this;

    DBUtils.unlockObject(self);

    self.session.set('currentSection', constants.sections.PROGRAM_EVENTS);

    var currentProgram = self.session.get('currentProgram');
    var currentClient = self.session.get('currentClient');


    var events = [];
    var services = [];
    var venues = [];

    var result = {};
    result.eventStatusArray = constants.EVENT_STATUS();
    result.eventApprovalStatusArray = constants.EVENT_APPROVAL_STATUS();
    result.searchEventProperties = constants.SEARCH_EVENT_PROPERTIES();

    var sort = {
      jobNumber: 'asc'
    };

    //pagination vars 
    var eventCollection = geddy.globalDb.collection('events');
    var pageLimit = geddy.config.pageLimit;
    var notSubmittedConstant = constants.NOT_SUBMITTED;
    var currentPage = 1;
    var recordCount=0;
    var totalPages=0;

    //check currentClient for reps
    if (!currentClient.reps){
      currentClient.reps = [];
    }
    
    result.currentClient = currentClient;
    result.currentProgram = currentProgram;

    //process any events that the program has stored
    if (currentProgram.events && currentProgram.events.length > 0)
    {
      var searchObject = {id: {$in: currentProgram.events},approvalStatus:{$ne:notSubmittedConstant}};// mongo search object
      var totalIdsProcessed = 0;
 
     //retrieve all possible services
     geddy.model.Service.all(function(err, servicesData) {
       if (err) {
         throw err;
       }
       result.services = servicesData;

       //retrieve all possible venues
       geddy.model.Venue.all(function(err, venuesData) {
         if (err) {
           throw err;
         }
         result.venues = venuesData;

         eventCollection.find(searchObject).toArray(function(err,allEvents){
           if (allEvents != null)
           {
             recordCount = allEvents.length;
             totalPages = Math.ceil(recordCount/pageLimit);
           }
           eventCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,events){
             result.events = events;

             //get all the CRG users
              geddy.model.User.all({or: [{roleId: constants.roles.SUPER_ADMIN.label}, {roleId: constants.roles.ADMIN.label}]},function (err, users){
                if (err) {
                  throw err;
                }

                result.crgUsers = users;

                result.currentPage = currentPage;
                result.totalPages = totalPages;
                
                self.respond( result,{type:'Event'});
              });
           });
         });
       });
     });
    } else {
      result.events = events;
      result.services = services;
      result.venues = venues;
      result.currentPage = currentPage;
      result.totalPages = totalPages;
      
      self.respond( result,{type:'Event'}
      );
    }
  };


  this.show = function (req, resp, params) {
    var self = this;

    var continueProcess = true;

    if (params.id == '1234')
    {
      if (self.session.get('currentEvent'))
      {
        params.id = self.session.get('currentEvent').id;
      } else {
        continueProcess = false;
        self.respond('null',{format: 'txt'});
      }
    }

    if (continueProcess)
    {
      DBUtils.unlockObject(self);

      var result={};

      var currentObjectTag = "current"+ModelName;

      var displayPRF = self.session.get('displayingPRF');

      result.displayPRF = displayPRF;

      if ( self.session.get('currentSection') == constants.sections.REP_PORTAL_PROGRAM_DETAILS) 
      {
        self.session.set('currentSection',constants.sections.REP_PORTAL_EVENT_DETAILS);
      }
      else if (self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_PROGRAM_DETAILS)
      {
        self.session.set('currentSection',constants.sections.MANAGER_PORTAL_EVENT_DETAILS);
      } else if (self.session.get('currentSection') == constants.sections.CONTACT_PORTAL_PROGRAM_DETAILS)
      {
        self.session.set('currentSection',constants.sections.CONTACT_PORTAL_EVENT_DETAILS);
      } else if (self.session.get('currentSection') == constants.sections.CLIENT_PORTAL_PROGRAM_DETAILS)
      {
        self.session.set('currentSection',constants.sections.CLIENT_PORTAL_EVENT_DETAILS);
      } else if (!displayPRF || displayPRF == false)
      {
        self.session.set('currentSection',constants.sections.EVENT_DETAILS);
      } else {
        //reset displayingPRF, so we can load event details if they click on that tab next
        self.session.set('displayingPRF', false);
      }



      result.currentSection = self.session.get('currentSection');
      result.speakerStatusArray = constants.SPEAKER_STATUS();
      result.speakerCRGStatusArray = constants.SPEAKER_STATUS_CRG();
      result.statusTypes = constants.ATTENDEE_STATUS_TYPE();

      result.attendees = [];
      result.contacts = [];

      geddy.model[ModelName].first(params.id, function(err, object) {
        if (err) {
          throw err;
        }
        if (!object) {
          throw new geddy.errors.NotFoundError();
        }
        else {
          console.log(' session setting '+currentObjectTag);
          self.session.set(currentObjectTag,object);

          //put the event in the respond
          if (self.session.get('currentSection') == constants.sections.EVENT_PRF)
          {
            // //make sure we have an approved event
            if (object.approvalStatus == constants.APPROVED)
            {
              if (object.historical)
              {
                result[currentObjectTag] = DBUtils.DeserializeObject(object.historical[object.historical.length - 1].objectSnapshot);
              } else {
                result[currentObjectTag] = object;  
              }
            } else {
              result[currentObjectTag] = object;

               //end it here
               // self.respond(result);
            }
          } else {
            result[currentObjectTag] = object;
          }

          geddy.model.Lock.first({objectId:object.id}, function(err, lock){
            if (err)
            {
              throw err;
            }

            if (lock)
            {
              // //check lock, in case owner left it locked and their session has timed out
              // var timeDiff = new Date().getTime() - lock.editTime.getTime();
             //     timeDiff = (timeDiff/1000/60) << 0;

              // if (timeDiff >  0.5)//constants.LOCK_MAX_TIME)
              // {
              //  dbutils.unlockObject(self, lock.id);
              //  result.editing = false;
              //  result.editor = null;
              // } else {
              if (lock.editorId == self.session.get('userId'))
              {
                result.editing = false;
                result.editor = null;
              } else {
                result.editing = true;
                result.editor = lock.editorFirstName + ' ' + lock.editorLastName;
              }
              // }
            } else {
              result.editing = false;
              result.editor = null;
            }

            var eventStatusApprovalArray = constants.EVENT_APPROVAL_STATUS();
            result.statusApprovalLabel = eventStatusApprovalArray[object.approvalStatus].type;

            var eventStatusArray = constants.EVENT_STATUS();
            result.statusLabel = eventStatusArray[object.status].type;

            if (!result[currentObjectTag].attendees)
            {
              result[currentObjectTag].attendees = [];
            }   

            if (result[currentObjectTag].crgProgramManager)
            {
              geddy.model.User.first({email: result[currentObjectTag].crgProgramManager}, function(err, user){
                if (err)
                {
                  throw err;
                }

                result[currentObjectTag].crgProgramManagerName = user.firstName + ' ' + user.lastName

                self.session.set('currentEvent', result[currentObjectTag]);

                DBUtils.GetEventAttendees(result[currentObjectTag].attendees, self.onGetAllAttendees, self, result);
              });
            } else {
                self.session.set('currentEvent', result[currentObjectTag]);
                 DBUtils.GetEventAttendees(result[currentObjectTag].attendees, self.onGetAllAttendees, self, result);
               }
           });
          
        }
     });
  }
}


this.onGetAllAttendees = function(self, result){
    var currentEvent = self.session.get('currentEvent');

     // console.log("events show: event approval status: "+ result.currentEvent.approvalStatus + result.statusApprovalLabel);
     result.venue = null;
     result.caterer = null;
     result.rep = null;

    //get the venueid 
    if(currentEvent.venueId)
    {
      geddy.model.Venue.first(currentEvent.venueId, function(err, venueObject) {
        //console.log("getting venue "+ currentEvent.venueId);
        result.venue = venueObject;

        //get the catererid
        if(currentEvent.catererId)
        {
          geddy.model.Venue.first(currentEvent.catererId, function(err, catererObject) {
            //console.log("getting caterer "+ currentEvent.catererId);
            result.caterer = catererObject
            
            //get the requesting repId
            if(currentEvent.requestingRepId)
            {
              geddy.model.Rep.first(currentEvent.requestingRepId, function(err, repObject) {
                //console.log("getting rep "+ currentEvent.requestingRepId);
                result.rep = repObject;

                //console.log(">>>>>>>> repObject "+ JSON.stringify(repObject));
                //console.log(">>>>>>>> result "+ JSON.stringify(result));
                self.respond(result);
              });
            }
            else
            {
              // no requesting repId
              //console.log("result "+ JSON.stringify(result));
              self.respond(result);
            }
          });
        }
        else
        {
          //no caterer
          //get the requesting repId
          if(currentEvent.requestingRepId)
          {
            geddy.model.Rep.first(currentEvent.requestingRepId, function(err, repObject) {
             // console.log("getting rep "+ currentEvent.requestingRepId);
              result.rep = repObject;
              //console.log("result "+ JSON.stringify(result));
              self.respond(result);
            });
          }
          else
          {
            // no requesting repId
            //console.log("result "+ JSON.stringify(result));
            self.respond(result);
          }
        }
      });  
    }
    else
    {
      //no venue, use potential venue info
      if (currentEvent.potentialVenue)
      {
        result.venue = currentEvent.potentialVenue[0];
      }

      //get the catererid
      if(currentEvent.catererId)
      {
        geddy.model.Venue.first(currentEvent.catererId, function(err, catererObject) {
         // console.log("getting caterer "+ currentEvent.catererId);
          result.caterer = catererObject
          
          //get the requesting repId
          if(currentEvent.requestingRepId)
          {
            geddy.model.Rep.first(currentEvent.requestingRepId, function(err, repObject) {
             // console.log("getting rep "+ currentEvent.requestingRepId);
              result.rep = repObject;
              //console.log("result "+ JSON.stringify(result));
              self.respond(result);
            });
          }
          else
          {
            // no requesting repId
            //console.log("result"+ JSON.stringify(result));
            self.respond(result);
          }
        });
      }
      else
      {
        //no caterer, use potential Caterer
        if (currentEvent.potentialCaterer)
        {
          result.caterer = currentEvent.potentialCaterer[0];
        }

        //get the requesting repId
        if(currentEvent.requestingRepId)
        {
          geddy.model.Rep.first(currentEvent.requestingRepId, function(err, repObject) {
            //console.log("getting rep "+ currentEvent.requestingRepId);
            result.rep = repObject;
            //console.log("result "+ JSON.stringify(result));
            self.respond(result);
          });
        }
        else
        {
          // no requesting repId
          //console.log("result "+ JSON.stringify(result));
          self.respond(result);
        }
      }
    }
  }



  this.save = function (req, resp, params) {
    var self = this;
    console.log (self.session.get("currentSection") +">>>>>>>>>>> SAVING EVENT  currentVenue"+ self.session.get("currentVenue") );
    //check if the section is event request put the status on pending of approval
    if (self.session.get('currentSection') == constants.sections.REP_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.REP_PORTAL_PROGRAM_DETAILS ||
      self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_PROGRAM_DETAILS)
    {//saving the event request
      
      //console.log (">>>>>>>>>>> SAVING Event request  currentVenue"+ self.session.get("currentVenue") );
      params.requestingRepId = self.session.get('userLinkedId');
      params.requestingRepName = self.session.get('userFirstName') + ' ' + self.session.get('userLastName');
      params.approvalStatus = constants.NOT_SUBMITTED; //saving by default not submited
      params.status = constants.NOT_CONFIRMED;

      if(params.programId)
      {//set the current program based on the programId selected
        geddy.model.Program.first(params.programId, function(err, program) {
          if (err)
          {}
          else
          {
            self.session.set("currentProgram",program);
            DBUtils.saveObject(self,ModelName,params);

            var currentEvent = self.session.get('currentEvent');
            DBUtils.saveDataTo(self, 'Program', program.id, 'events', currentEvent.id);
          }
        });
      }
      else
      {
        DBUtils.saveObject(self,ModelName,params);

         var currentProgram = self.session.get('currentProgram');       
        var currentEvent = self.session.get('currentEvent');
        DBUtils.saveDataTo(self, 'Program', currentProgram.id, 'events', currentEvent.id);   
      }  
     // console.log (">>>>>>>>>>> SAVING Event request" + currentEvent.approvalStatus);
    }
    else
    {//saving the event
      //check if the current event exist
      var currentEvent = self.session.get('currentEvent');
      if (!currentEvent || !currentEvent.approvalStatus)
      {
        params.approvalStatus = constants.APPROVED;
        params.status = 0;
      }

      //TODO:  check if all required data in here now.. if yes, then set event.status to "Confirmed"
      //Also check that if we're saving the event status, not to set it to 'Confirmed', but just take the provided value

      var currentProgram = self.session.get('currentProgram');
      params.programId = currentProgram.id;

      DBUtils.saveObject(self,ModelName,params);

      currentEvent = self.session.get('currentEvent');
      //console.log (">>>>>>>>>>> SAVING Event" + currentEvent.approvalStatus);
      DBUtils.saveDataTo(self, 'Program', currentProgram.id, 'events', currentEvent.id);
    }
  }


  this.submitStatus = function (req, resp, params) {
    
    var self = this;
   // console.log (">>>>>>>>>>> SAVING EVENT  currentVenue"+ self.session.get("currentVenue") );
    
   //if a manager is requesting an event, go ahead and set the approval status to 'approved'
   if (self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_PROGRAM_DETAILS)
   {
      params.approvalStatus = constants.APPROVED;
   } else {
      params.approvalStatus = constants.PENDING_APPROVAL;
   }

    var currentObject;

    if (!self.session.get("currentEvent")) 
    {
      //console.log('submitting new event this should not happend');
      //create object
      currentObject = geddy.model[Event].create(params);
      self.session.set("currentEvent",currentObject);
    }
    else
    {
      //console.log('submitting existing event');
      currentObject = self.session.get("currentEvent");

      if (typeof currentObject.submitingTimes == 'undefined' || currentObject.submitingTimes == null)
      {
        
        currentObject.submitingTimes = 0;
      }
      else
      {
        currentObject.submitingTimes = currentObject.submitingTimes + 1;  
      }
      //console.log("editing times "+currentObject.submitingTimes);
      if (params)
      {
        currentObject.updateProperties(params);
        self.session.set("currentEvent",currentObject);
      }
      else
      {
         // save the editing Times
        currentObject.save();
        self.session.set("currentEvent",currentObject);
      }  
    }

    var notificationData ={};
    var currentProgram = self.session.get('currentProgram');
    var currentEvent = self.session.get('currentEvent');
    notificationData.programId = currentProgram.id;
    notificationData.eventId = currentEvent.id;
    notificationData.deliveryType = constants.PROGRAM_REQUEST_SUBMISSION;

    var callBack = function() 
    {
      if (self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_PROGRAM_DETAILS)
      {
        notificationData.managerRequesting= true;
         self.respond({notificationData: notificationData}, {format: 'json'});
      }else {
        notificationData.managerRequesting= false;

         self.respond({notificationData: notificationData, approval_denial_type: constants.PROGRAM_REQUEST_APPROVAL_DENIAL}, {format: 'json'});
      }
    }

    DBUtils.unlockObject(self);
    DBUtils.saveObject(self,ModelName,params, null, callBack);
    DBUtils.saveDataTo(self, 'Program', currentProgram.id, 'events', currentEvent.id);
  }


  this.search = function (req, resp, params)
  {
    var $thisRequest = this;
    var notSubmittedConstant = constants.NOT_SUBMITTED;
    var currentProgram = $thisRequest.session.get('currentProgram');
    var searchObject = {id: {$in: currentProgram.events},approvalStatus:{$ne:notSubmittedConstant}};// mongo search object
     var sort = {
     jobNumber: 'asc', 
    };

    search.AdvanceSearch($thisRequest, searchObject, sort, 'events', params, processEvents);
  }


   function processEvents (self, result)
   {
      //get all the CRG users
      geddy.model.User.all({or: [{roleId: constants.roles.SUPER_ADMIN.label}, {roleId: constants.roles.ADMIN.label}]},function (err, crgUsers){
         if (err) {
            throw err;
         }

         geddy.model.Venue.all(function(err, venuesData) {
            if (err) {
             throw err;
            }
            result.venues = venuesData;

            var i;
            var j;
            var z;
            var statusArray = constants.EVENT_STATUS();
            var approvalStatusArray = constants.EVENT_APPROVAL_STATUS();

            for (i= 0 ; i < result.dataset.length; i++)
            {
               //for each result, check the status id and set it to string for display
               result.dataset[i].status = statusArray[result.dataset[i].status].type;
               result.dataset[i].approvalStatus = approvalStatusArray[result.dataset[i].approvalStatus].type;

               //Need to add missing columns
               if (result.dataset[i].potentialSpeakers && result.dataset[i].potentialSpeakers.length > 0)
               {
                  for (j = 0; j < result.dataset[i].potentialSpeakers.length; j++) {
                    if (result.dataset[i].potentialSpeakers[j].crgStatus == constants.SPEAKER_CONFIRMED)
                    {
                      result.dataset[i].speakerName = result.dataset[i].potentialSpeakers[j].speakerName;
                      break;
                    }
                  };
               }

               if (result.dataset[i].crgProgramManager)
               {
                  for (var index in crgUsers) { 
                     if (crgUsers[index].email == result.dataset[i].crgProgramManager) {
                        result.dataset[i].crgProgramManager = crgUsers[index].firstName + ' ' + crgUsers[index].lastName;
                        break;
                     } 
                  }
               }

               if (result.dataset[i].venueId)
               {
                  for (z = 0; z < venuesData.length; z++) {
                     if (venuesData[z].id == result.dataset[i].venueId)
                     {
                        result.dataset[i].venueName = venuesData[z].venueName;
                        break;
                     }
                  }
               }
            }
            
            //send results to view
            self.respond(result, { format: 'json' });
         });
      });
   }



  this.saveStatusEvent = function (req, resp, params) {
    var self = this;
    var currentEvent;
    
    if (!self.session.get("currentEvent")) 
    {
      //console.log('submitting new event this should not happend');
      //create object
      currentEvent = geddy.model[Event].create(params);
      self.session.set("currentEvent",currentEvent);
    }
    else
    {
      //console.log('submitting existing event');
      currentEvent = self.session.get("currentEvent");
    }

    if (typeof(params.status) == 'undefined')
    {
      params.status = null;
    }

    var status = Number(params.approvalStatus);

    DBUtils.changeStatus(ModelName,currentEvent.id, status, params.status, params.comment, self.session.get('userLinkedId'), true);

    var currentProgram = self.session.get("currentProgram");
    var notificationData = {};
    notificationData.eventId = currentEvent.id;

    switch (status){
        case constants.APPROVED://aproval status approved
          notificationData.deliveryType = constants.PROGRAM_REQUEST_APPROVAL;
          break;
        case constants.DENIED://aproval status denied
          notificationData.deliveryType = constants.PROGRAM_REQUEST_DENIAL;
          break;
      }

    self.respond(notificationData, {
        format: 'json'
      });
  }


  this.saveEmbeddedModel = function (req, resp, params) {
    var self = this;

    DBUtils.saveObjectEmbeddedModel(self,ModelName,params);

    var currentProgram = self.session.get('currentProgram');
    var currentEvent = self.session.get('currentEvent');
    
    DBUtils.saveDataTo(self, 'Program', currentProgram.id, 'events', currentEvent.id);
  }

  this.saveInvitationListContact = function(req, resp, params) {
    var self = this;
    var currentEvent = self.session.get('currentEvent');

    if (!currentEvent[params.targetList])
    {
      currentEvent[params.targetList] = [];
    }

    //clean up values first
    params.contact.firstName = params.contact.firstName.trim();
    params.contact.lastName = params.contact.lastName.trim();
    currentEvent[params.targetList].push(params.contact);

    //find attendee
    geddy.model.Attendee.first({contactId: params.contact.id, eventId: currentEvent.id}, function (err, attendee){
      if (err)
      {
         throw err;
      }

      if (!attendee)
      {
         var newAttendee = geddy.model.Attendee.create();
         newAttendee.contactId = params.contact.id;
         newAttendee.status = 1;
         newAttendee.eventId = currentEvent.id;
         newAttendee.attendeeType = 'Attendee';

         newAttendee.save(function(err, data){
            if (err)
               throw err;

             if (!currentEvent.attendees)
             {
                currentEvent.attendees = [];
             }

            currentEvent.attendees.push(data.id);

            currentEvent.save(function(err, data) {
               if (err) {
                 throw err;
               }

               self.respond(true, {
                 format: 'json'
               });

            });
         });
      } else {
        //make sure attendee is in list
        var i;
        var found = false
        for (i = currentEvent.attendees.length - 1; i >= 0; i--) {
          if (currentEvent.attendees[i] == attendee.id)
          {
            found = true;
            break;
          }
        };

        if (!found)
        {
          currentEvent.attendees.push(attendee.id);
        }
        
        currentEvent.save(function(err, data) {
            if (err) {
              throw err;
            }

            self.respond(true, {
              format: 'json'
            });

         });
      }
    });
  }


  this.deleteEvents = function (req, resp, params) {
    var self = this;
    DBUtils.deleteObject(self,ModelName,params);
  };


  this.deleteEmbeddedModel = function (req, resp, params) {
    var self = this;
    DBUtils.deleteEmbeddedModel(self,ModelName,params);
   };

  this.deleteEventFromProgram = function (req, resp, params) {
    var self = this;

    DBUtils.deleteFromArrayByValue(self, params.ids, self.session.get('currentProgram'), 'events');
  };

   this.removeInvitationListContact = function (req, resp, params){
      var self = this;

      var currentEvent = self.session.get('currentEvent');

      //find id in list and remove
      var i;
      var inMultipleList = false;

      for (i = 0; i < currentEvent[params.targetList].length; i++) {
         if (currentEvent[params.targetList][i].id == params.contact.id)
         {
            currentEvent[params.targetList].splice(i, 1);
            break;
         }
      };

      //see if attendee is in any of the lists, if they are, then don't remove attendee from attendees collection
       if (currentEvent.emailInvitations && currentEvent.emailInvitations.length > 0)
      {
       	    for (i = 0; i < currentEvent.emailInvitations.length; i++) {
         	if (currentEvent.emailInvitations[i].id == params.contact.id)
        	 {
            	inMultipleList = true;
            	break;
        	 }
	     };
	}

      if (!inMultipleList)
      {
	if (currentEvent.hardcopyInvitations && currentEvent.hardcopyInvitations.length > 0)
         {
        	for (i = 0; i < currentEvent.hardcopyInvitations.length; i++) {
          		if (currentEvent.hardcopyInvitations[i].id == params.contact.id)
          		{
           		 inMultipleList = true;
            		break;
          		}
       		 };
	}
      }

      if (!inMultipleList)
      {
        //find attendee and remove them from collection
        geddy.model.Attendee.first({contactId: params.contact.id, eventId: currentEvent.id}, function(err, attendee) {
           if (err)
           {
              throw err;
           }

           //remove from event's list
           for (i = 0; i < currentEvent.attendees.length; i++) {
              if (currentEvent.attendees[i] == attendee.id)
              {
                 currentEvent.attendees.splice(i, 1);
              }
           }

           //remove attendee now
           geddy.model.Attendee.remove(attendee.id, function(err) {
              if (err)
              {
                 throw err;
              }

              currentEvent.save(function(err, data) {
                 if (err) {
                   throw err;
                 }

                 self.respond(true, {
                   format: 'json'
                 });
              });
           });   
        });
      } else {
        currentEvent.save(function(err, data) {
           if (err) {
             throw err;
           }

           self.respond(true, {
             format: 'json'
           });
        });
      }
   }

   this.removeAllInvitations = function (req, resp, params)
   {
      var self = this;
      var propertyName = params.value[0].substring(0, params.value[0].indexOf("="));
      var propertyValue = params.value[0].substring(params.value[0].indexOf("=") + 1);
      
      var currentEvent = self.session.get('currentEvent');

      if (propertyValue == 'false')
      {
         if (propertyName == 'invitationsPDF')
         {
            currentEvent.emailInvitations = null;
         } else if (propertyName == 'invitationsHardcopyDirect'){
            currentEvent.hardcopyInvitations = null;
         }

         currentEvent.save(function(err, data) {
            if (err) {
               throw err;
            }

            self.respond(true, {format:'json'});
         });
      }
   }


  this.clearCurrentEvent = function (req, resp, params) {
    var self = this;
    DBUtils.clearCurrentObject(self,ModelName,params);
   };

  this.getCurrentEvent = function (req, resp, params){
    var self = this;
    DBUtils.getCurrentObject(self,ModelName);
  }


  ////////////////  Steps  ///////////////////
  this.eventStep1 = function (req, resp, params) {
    var self = this;

    var totalRepsProcessed = 0;
    var repsArray = [];

    var currentClient = self.session.get('currentClient');
    var currentProgram = self.session.get('currentProgram');
    var currentEvent = self.session.get('currentEvent');
    var eventRequestMode = false
    if (self.session.get('currentSection') == constants.sections.REP_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.REP_PORTAL_PROGRAM_DETAILS ||
      self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_PROGRAM_DETAILS)
    {
      eventRequestMode = true
    }
    
    var result={};
    result.eventRequestMode = eventRequestMode;
    console.log("event requestMODE "+eventRequestMode);
    result.selectPrompt = constants.SELECT_PROMPT();
    result.stepTitle = createTitleForEvent(self.session.get('currentEvent'),eventRequestMode);
    result.currentEvent = currentEvent;
    result.states= constants.STATES();
    result.timeZones= constants.TIME_ZONES();
    result.eventApprovalStatus= constants.EVENT_APPROVAL_STATUS();
    result.eventStatus= constants.EVENT_STATUS();
    result.jobNumber =  (currentEvent)? currentEvent.jobNumber : null;
    result.confirmedDate = (currentEvent)?currentEvent.confirmedDate: null;
    result.currentProgram = currentProgram;
    result.services  = null;
    result.reps = null;
console.log("event currentEvent "+currentEvent);
    if (currentEvent && currentEvent.hasOwnProperty('id') && eventRequestMode == false)
    {
      DBUtils.lockObject(self, currentEvent.id);
    }

//----

 if(currentProgram && currentProgram.programTypeId)
    {
        if(currentProgram.contracts)
        {
               //get all the contracts for client
               var contractCollection = geddy.globalDb.collection('contracts');

              var searchObject = {id: {$in: currentProgram.contracts}, status: 'Active'};// mongo search object

              contractCollection.find(searchObject).toArray(function(error, contracts){
                  if (error)
                     throw error

                  else 
                  {
                     var contactIds = [];

                     for (var i = contracts.length - 1; i >= 0; i--) {
                        var id = contracts[i].contactId;

                        contactIds.push(id);
                     };


                     if (contactIds.length > 0)
                     {
                        var contactCollection = geddy.globalDb.collection('contacts');
                        searchObject = {id: {$in: contactIds}};

                        contactCollection.find(searchObject).toArray(function (error, allContacts){
                           for (i = 0; i < allContacts.length; i++) {
                              var speaker = allContacts[i];

                                if (speaker)
                                {
                                  if (speaker.degrees && speaker.degrees.length > 0)
                                  {
                                    var re = /,/g;

                                    speaker.degrees = (speaker.degrees.toString()).replace(re, ", ");
                                  }

                                  //store the result
                                  speakersArray.push(speaker);
                                }
                           }

                           result.speakers = speakersArray;
                     
                        });
                     } else {
                        
                        
                     }
                  }
               });
      }
        else
        {
          //no contracts
         
        }
      // });
    }else
    {
      //no program type
      if(currentProgram && currentProgram.contracts)
      {
        for (i = 0; i < currentProgram.contracts.length; i++) 
        {
          geddy.model.Contract.first( currentProgram.contracts[i], function(err, contract) {
            //get speakers array
            geddy.model.Contact.first( contract.contactId, function(err, speaker) {
              if (err) {
                totalContractsProcessed++;
              } else {

                if (speaker.degrees.length > 0)
                {
                  var re = /,/g;

                  speaker.degrees = (speaker.degrees.toString()).replace(re, ", ");
                }

                //store the result
                speakersArray.push(speaker);
            
                totalContractsProcessed++;
                //check if all have been processed
                if (totalContractsProcessed == currentProgram.contracts.length)
                {
                  result.speakers = speakersArray;
                
                }
              }
            });
          });
        }
      }
      else
      {
        //no contracts
     
      }
    }

    if (eventRequestMode)
    {
      //rep portal
      result.eventTypes= constants.EVENT_TYPES();

      var repId = self.session.get("userLinkedId");

      geddy.model.Rep.first( repId, function(err, rep) {
        if (err) {
          throw err;
        } 
        
        result.currentRep = rep;
        //set currentRep on the session 
        self.session.set('currentRep',rep);

        var venueCollection = geddy.globalDb.collection('venues');
        result.venueCollection = venueCollection;
          var clientCollection = geddy.globalDb.collection('clients');

          //searching object 
          var searchObject = {"reps": repId};

          clientCollection.find(searchObject).toArray(function(err,clients){
            result.currentClient = clients[0];
            self.session.set('currentClient',result.currentClient);

            //this should be one client who has the programs ids 
            var programsArray=[];

            var programsProccesed = 0;
            var i;
            for (i = 0; i < clients[0].programs.length; i++) 
            {
              geddy.model.Program.first(clients[0].programs[i],function(err,program){
                if (err)
                {
                  throw err;
                }
                else
                {
                  programsArray.push(program);
                }

                programsProccesed ++;

                if ( programsProccesed == clients[0].programs.length )
                {

                  result.programs = programsArray;

                  //check if there is a programId in the current event to set it in the current program
                  if (currentEvent && currentEvent.programId)
                  {
                    geddy.model.Program.first(currentEvent.programId, function(err,program){
                      if (err) {
                      } else {
                        //set the current program to the session current program
                        self.session.set('currentProgram',program);
                      }

                        //get their manager also
                        if (rep.managerId)
                        {
                           geddy.model.Rep.first (rep.managerId, function (err, manager){
                              if (err){
                                 throw err;
                              }

                              result.currentManager = manager;

                              //set currentRep on the session 
                              self.session.set('currentManager',manager);
                               
                              //load the view 
                              self.respond( result, {format: 'html',template: 'app/views/events/eventStep1'});  
                           });
                        } else {
                            //set currentRep on the session 
                            result.currentManager = {id: -1};

                           self.session.set('currentManager', result.currentManager);

                           //load the view 
                           self.respond( result, {format: 'html',template: 'app/views/events/eventStep1'});  
                        }
                    });
                  }
                  else
                  {
                    //get their manager also
                     if (rep.managerId)
                     {
                        geddy.model.Rep.first (rep.managerId, function (err, manager){
                           if (err){
                              throw err;
                           }

                           result.currentManager = manager;

                           //set currentRep on the session 
                           self.session.set('currentManager',manager);
                            
                           //load the view 
                           self.respond( result, {format: 'html',template: 'app/views/events/eventStep1'});  
                        });
                     } else {
                        result.currentManager = {id: -1};

                        self.session.set('currentManager', result.currentManager);

                        //load the view 
                        self.respond( result, {format: 'html',template: 'app/views/events/eventStep1'});  
                     }
                  }
                }
              });
            }
          });
      });
    }
    else
    {
      var types = [];
      var constantTypes = constants.EVENT_TYPES();
      for (var j = 0; j < constantTypes.length; j++) {
          if (currentProgram.eventTypes.indexOf(constantTypes[j].type) != -1)
            types.push(constantTypes[j]);
       }; 

      result.eventTypes= types;

      result.currentClient = currentClient;

      //get all the CRG users
      geddy.model.User.all({or: [{roleId: constants.roles.SUPER_ADMIN.label}, {roleId: constants.roles.ADMIN.label}]},function (err, users){
        if (err) {
          throw err;
        }

        result.crgUsers = users;

        //step1 from CRG
        if (currentClient && currentClient.reps)
        {
          var i;
          for (i = 0; i < currentClient.reps.length; i++) 
          {
            //get rep based on the client
            geddy.model.Rep.first( currentClient.reps[i], function(err, rep) {
              if (err) {
                totalRepsProcessed++;
              } else {
                //store the result
                repsArray.push(rep);

                totalRepsProcessed++;
                //check if all have been processed
                if (totalRepsProcessed == currentClient.reps.length)
                {
                  
                  //sort reps
                  repsArray.sort(function(a,b){
                    var result = DBUtils.alphabetical(a.lastName, b.lastName);
                    return result;
                  });

                  //get services based on the programType of the program
                  if (currentProgram && currentProgram.programTypeId)
                  {
                    geddy.model.ProgramType.first(currentProgram.programTypeId,function(err,programType){
                      if (err) {

                      } else {
                        var services = null;
                        if (programType && programType.services)          
                        {
                          services = programType.services;
                        }

                        result.services = services;
                        result.reps = repsArray;

                        //if currentEvent has a rep set, let's set it as the currentRep
                        if (currentEvent && currentEvent.requestingRepId)
                        {
                           geddy.model.Rep.first( currentEvent.requestingRepId, function(err, rep) {
                            if (err) {
                              throw err;
                            } 
                            
                            result.currentRep = rep;
                            //set currentRep on the session 
                            self.session.set('currentRep',rep);

                            self.respond( result, {format: 'html',template: 'app/views/events/eventStep1'});  
                          });

                        } else {
                          self.respond( result, {format: 'html',template: 'app/views/events/eventStep1'});  
                        }
                      }
                    });


                  }
                  else
                  {
                    //no services 
                    result.reps = repsArray;

                    //if currentEvent has a rep set, let's set it as the currentRep
                    if (currentEvent && currentEvent.requestingRepId)
                    {
                       geddy.model.Rep.first( currentEvent.requestingRepId, function(err, rep) {
                        if (err) {
                          throw err;
                        } 
                        
                        result.currentRep = rep;
                        //set currentRep on the session 
                        self.session.set('currentRep',rep);

                        self.respond( result, {format: 'html',template: 'app/views/events/eventStep1'});  
                      });

                    } else {
                      self.respond( result, {format: 'html',template: 'app/views/events/eventStep1'});  
                    }
                  }
                } 
              }  
            });  
          }
        }
        else
        {
          //no reps 
          //get services based on the programType of the program
          if (currentProgram && currentProgram.programTypeId)
          {
            geddy.model.ProgramType.first(currentProgram.programTypeId,function(err,programType){
              if (err) {

              } else {
                var services = null;
                if (programType && programType.services)          
                {
                  services = programType.services;
                }

                result.services = services;
                self.respond( result, {format: 'html',template: 'app/views/events/eventStep1'});  
              }
            });
          }
          else
          {
            //no services 
            self.respond( result, {format: 'html',template: 'app/views/events/eventStep1'});  
          }
        }
      });
    }

  };


  this.getEventTypesForSelectedProgram = function(req, resp, params)
  {
      var self = this;
      //get program so we can get its list of event types
      geddy.model.Program.first(params.selectedProgramId, function (err, program){
         var types = [];
         var constantTypes = constants.EVENT_TYPES();
         for (var j = 0; j < constantTypes.length; j++) {
             if (program.eventTypes.indexOf(constantTypes[j].type) != -1)
               types.push(constantTypes[j]);
          }; 

         self.respond({eventTypes: types}, {
            format: 'json'
         });
      });
  }

  this.eventStep2 = function (req, resp, params) {
    var self = this;

    var currentProgram = self.session.get('currentProgram');
    var totalContractsProcessed = 0;
    var speakersArray = [];
    var i;

    var eventRequestMode = false;
    if (self.session.get('currentSection') == constants.sections.REP_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.REP_PORTAL_PROGRAM_DETAILS ||
      self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_PROGRAM_DETAILS)
    {
      eventRequestMode = true
    }

    var result={};
    result.eventRequestMode = eventRequestMode;
    result.selectPrompt = constants.SELECT_PROMPT();
    

    // if (eventRequestMode)
    // {
      result.speakerStatusFieldForce = constants.SPEAKER_STATUS();
    // } else {
      result.speakerStatus = constants.SPEAKER_STATUS_CRG();
    // }

    result.stepTitle = createTitleForEvent(self.session.get('currentEvent'));
    result.currentEvent = self.session.get('currentEvent');
    result.currentClient = self.session.get('currentClient');
    result.currentProgram = currentProgram;
    result.speakers = speakersArray;
    result.currentManager = self.session.get('currentManager');
    result.currentRep = self.session.get('currentRep');

    if(currentProgram && currentProgram.programTypeId)
    {
        if(currentProgram.contracts)
        {
               //get all the contracts for client
               var contractCollection = geddy.globalDb.collection('contracts');

              var searchObject = {id: {$in: currentProgram.contracts}, status: 'Active'};// mongo search object

              contractCollection.find(searchObject).toArray(function(error, contracts){
                  if (error)
                     throw error

                  else 
                  {
                     var contactIds = [];

                     for (var i = contracts.length - 1; i >= 0; i--) {
                        var id = contracts[i].contactId;

                        contactIds.push(id);
                     };


                     if (contactIds.length > 0)
                     {
                        var contactCollection = geddy.globalDb.collection('contacts');
                        searchObject = {id: {$in: contactIds}};

                        contactCollection.find(searchObject).toArray(function (error, allContacts){
                           for (i = 0; i < allContacts.length; i++) {
                              var speaker = allContacts[i];

                                if (speaker)
                                {
                                  if (speaker.degrees && speaker.degrees.length > 0)
                                  {
                                    var re = /,/g;

                                    speaker.degrees = (speaker.degrees.toString()).replace(re, ", ");
                                  }

                                  //store the result
                                  speakersArray.push(speaker);
                                }
                           }

                           result.speakers = speakersArray;
                           self.respond( result,{format: 'html',template: 'app/views/events/eventStep2'}); 
                        });
                     } else {
                        
                        self.respond( result,{format: 'html',template: 'app/views/events/eventStep2'}); 
                     }
                  }
               });
      }
        else
        {
          //no contracts
          self.respond( result,{format: 'html',template: 'app/views/events/eventStep2'});
        }
      // });
    }
    else
    {
      //no program type
      if(currentProgram && currentProgram.contracts)
      {
        for (i = 0; i < currentProgram.contracts.length; i++) 
        {
          geddy.model.Contract.first( currentProgram.contracts[i], function(err, contract) {
            //get speakers array
            geddy.model.Contact.first( contract.contactId, function(err, speaker) {
              if (err) {
                totalContractsProcessed++;
              } else {

                if (speaker.degrees.length > 0)
                {
                  var re = /,/g;

                  speaker.degrees = (speaker.degrees.toString()).replace(re, ", ");
                }

                //store the result
                speakersArray.push(speaker);
            
                totalContractsProcessed++;
                //check if all have been processed
                if (totalContractsProcessed == currentProgram.contracts.length)
                {
                  result.speakers = speakersArray;
                  self.respond( result,{format: 'html',template: 'app/views/events/eventStep2'});
                }
              }
            });
          });
        }
      }
      else
      {
        //no contracts
        self.respond( result,{format: 'html',template: 'app/views/events/eventStep2'});
      }
    }
  };

  this.eventStep3 = function (req, resp, params) {
    var self = this;

    var currentProgram = self.session.get('currentProgram');
    var currentEvent = self.session.get('currentEvent');

    var eventRequestMode = false;
    if (self.session.get('currentSection') == constants.sections.REP_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.REP_PORTAL_PROGRAM_DETAILS ||
      self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_PROGRAM_DETAILS)
    {
      eventRequestMode = true
    }

    var result={};
    result.eventRequestMode = eventRequestMode;
    result.currentProgram = currentProgram;
    result.currentEvent = currentEvent;
    result.selectPrompt = constants.SELECT_PROMPT();
    result.venueTypes = constants.VENUE_TYPES();
    result.venueAVStatus = constants.VENUE_AV_STATUS();
    result.states = constants.STATES();
    result.stepTitle = createTitleForEvent(self.session.get('currentEvent'));
    result.currentClient = self.session.get('currentClient');
    result.currentManager = self.session.get('currentManager');
    result.currentRep = self.session.get('currentRep');
    result.requireVenueInfo = currentProgram.requireVenueInfo;

    result.currentVenue = null;
    result.potentialVenue = null;
    result.currentCaterer = null;
    result.potentialCaterer = null;
    result.venues = [];
    result.caterers = [];

    if (eventRequestMode)
    {
      result.potentialVenue = currentEvent.potentialVenue? currentEvent.potentialVenue[0] : null;
      result.potentialCaterer = currentEvent.potentialCaterer? currentEvent.potentialCaterer[0] : null;

      self.respond( result,{format: 'html',template: 'app/views/events/eventStep3'});
    } else {

      //no caterer, use potential caterer info
      if (currentEvent.potentialCaterer)
      {
        result.potentialCaterer = currentEvent.potentialCaterer[0];
        if (result.potentialCaterer.venueName)
          result.potentialCaterer.venueName = result.potentialCaterer.venueName.replace(/'+/g, '_');
       }

      //no venue, use potential venue info
      if (currentEvent.potentialVenue)
      {
        result.potentialVenue = currentEvent.potentialVenue[0];
        if (result.potentialVenue.venueName)
          result.potentialVenue.venueName = result.potentialVenue.venueName.replace(/'+/g, '_');
      }

      if(currentProgram && currentProgram.programTypeId)
      {
        //console.log (" >>>>>> program ");
        // geddy.model.ProgramType.first( currentProgram.programTypeId, function(err, programType) {
          // if (err) {
          //   throw err;
          // }  
          // result.progType = programType.programTypeName;
          //console.log (" >>>>>>>currentEvent venueId "+ currentEvent.venueId);
          geddy.model.Venue.all({venueType: {ne: 'Caterer'}}, function(err, venues) {
               if (err) {
                 throw err;
               }

               result.venues = venues;

               //now get all the caterers
               geddy.model.Venue.all({venueType: 'Caterer'},function(err, caterers) {
                 if (err) {
                   throw err;
                 }
                 result.caterers = caterers;

                 if (currentEvent.venueId != null && currentEvent.venueId != 'null')
                 {
                   geddy.model.Venue.first(currentEvent.venueId, function(err, currentVenue) {
                     if (err) {
                       throw err;
                     }
                     result.currentVenue = currentVenue;

                     if (result.currentVenue && result.currentVenue.venueName)
                     {
                      result.currentVenue.venueName = result.currentVenue.venueName.replace(/'+/g, '_');
                    } else {
                      result.currentVenue.venueName = '';
                    }

                     if (currentEvent.catererId)
                     {
                       geddy.model.Venue.first(currentEvent.catererId, function(err, currentCaterer) {
                         if (err) {
                           throw err;
                         }
                         result.currentCaterer = currentCaterer;
                        result.currentCaterer.venueName = result.currentCaterer.venueName.replace(/'+/g, '_');

                         self.respond( result,{format: 'html',template: 'app/views/events/eventStep3'});
                       }); 
                     }
                     else
                     {
                       //console.log (" >>>>>>>NO caterer ");
                       //console.log('>>>>>>>> RESULT '+ JSON.stringify(result));
                       self.respond( result,{format: 'html',template: 'app/views/events/eventStep3'});
                     }
                   });
                 }
                 else
                 {
                   //console.log (" >>>>>>>NO venue ");
                   if (currentEvent.catererId)
                   {
                     geddy.model.Venue.first(currentEvent.catererId, function(err, currentCaterer) {
                       if (err) {
                         throw err;
                       }

                        result.currentCaterer = currentCaterer;

                        if (result.currentCaterer.venueName)
                        {
                          result.currentCaterer.venueName = result.currentCaterer.venueName.replace(/'+/g, '_');
                        } else {
                          result.currentCaterer.venueName = '';
                        }

                       self.respond( result,{format: 'html',template: 'app/views/events/eventStep3'});
                     });
                   }
                   else
                   {
                     //console.log (" >>>>>>>NO venue no caterer ");
                     self.respond( result,{format: 'html',template: 'app/views/events/eventStep3'});
                   }
                 }
               });
          }); 
      }
    }
  };


  this.eventStep4 = function (req, resp, params) {
    var self = this;
    var currentEvent = self.session.get('currentEvent');
    var eventRequestMode = false;
    if (self.session.get('currentSection') == constants.sections.REP_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.REP_PORTAL_PROGRAM_DETAILS ||
      self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_PROGRAM_DETAILS)
    {
      eventRequestMode = true
    }

    var result={};
    result.eventRequestMode = eventRequestMode;
    result.stepTitle = createTitleForEvent(self.session.get('currentEvent'));
    result.currentEvent = currentEvent;
    result.currentClient = self.session.get('currentClient');
    result.currentProgram = self.session.get('currentProgram');
    result.currentManager = self.session.get('currentManager');
    result.currentRep = self.session.get('currentRep');
    result.requestingRep = null;

    //check if the requesting rep exist
    if (currentEvent && currentEvent.requestingRepId)
    {
      geddy.model.Rep.first( currentEvent.requestingRepId, function(err, rep) {
        if (err) {
        } else {
        }
        result.requestingRep = rep;

        self.respond(result,{format: 'html',template: 'app/views/events/eventStep4'});
      });
    }
    else
    {
        self.respond(result,{format: 'html',template: 'app/views/events/eventStep4'});
    }
  };

  this.eventStep5 = function (req, resp, params) {
    var self = this;

    var currentRep = self.session.get('currentRep');
    var currentClient = self.session.get('currentClient');

    var eventRequestMode = false;
    if (self.session.get('currentSection') == constants.sections.REP_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.REP_PORTAL_PROGRAM_DETAILS ||
      self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_PROGRAM_DETAILS)
    {
      eventRequestMode = true
    }

    //pagination vars 
    var contactCollection = geddy.globalDb.collection('contacts');
    var pageLimit = geddy.config.pageLimit;

    //clean up spaces first
    var zips = '';
    if (currentRep.territoryZips)
    {
      currentRep.territoryZips = currentRep.territoryZips.replace(/ /g, '');
      zips = currentRep.territoryZips.split(',');
    }
    
    //var searchObject = {"addresses.postalCode": {$in: zips}, "contactType": 'kol'};
     var searchObject = {"addresses.postalCode": {$in: zips}, "contactType": 'attendee', 'clients.id': currentClient.id};
    var currentPage = 1;
    var recordCount=0;
    var totalPages=0;

    var sort = {
      lastName: 'asc',
      firstName : 'asc'
    }

    var result={};
    result.eventRequestMode = eventRequestMode;
    result.searchAudienceProperties = constants.SEARCH_EVENT_AUDIENCE_PROPERTIES();
    result.stepTitle = createTitleForEvent(self.session.get('currentEvent'));
    result.currentEvent = self.session.get('currentEvent');
    result.currentClient = currentClient;
    result.currentProgram = self.session.get('currentProgram');
    result.currentManager = self.session.get('currentManager');
    result.currentRep = currentRep;
    result.currentPage = currentPage;


    result.displayEmailColumn = false;
    result.displayHardCopyColumn = false;
    result.displayInvitationPoolColumn = false;

    if (result.currentEvent.invitationsPDF)
    {
      result.displayEmailColumn = true;
    }

    if (result.currentEvent.invitationsHardcopyDirect)
    {
      result.displayHardCopyColumn = true;
    }

    if (result.currentEvent.invitationsPDFRep)
    {
      result.displayInvitationPoolColumn = true;
    }

    if (result.displayEmailColumn || result.displayHardCopyColumn || result.displayInvitationPoolColumn)
    {
      contactCollection.find(searchObject).toArray(function(err,allContacts){
        //console.log ('allContacts '+ allContacts.length);
        if (allContacts != null)
        {
          recordCount = allContacts.length;
          totalPages = Math.ceil(recordCount/pageLimit);
        }

        contactCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,audience){
        //geddy.model.Contact.all(function (err, audience) {
          //console.log ('audience '+ audience.length);
          if (err)
          {
            throw err;
          }

          result.totalPages = totalPages;
          result.audience = audience;
          self.respond( result, {format: 'html',template: 'app/views/events/eventStep5'});
        });
      });
    } else {
      result.totalPages = 0;
      result.audience = [];

      self.respond( result, {format: 'html',template: 'app/views/events/eventStep5'});
    }
}



  this.eventStep6 = function (req, resp, params) {
    var self = this;
    var currentEvent = self.session.get('currentEvent');

    var eventRequestMode = false;
    if (self.session.get('currentSection') == constants.sections.REP_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.REP_PORTAL_PROGRAM_DETAILS ||
      self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_EVENT_DETAILS || self.session.get('currentSection') == constants.sections.MANAGER_PORTAL_PROGRAM_DETAILS)
    {
      eventRequestMode = true
    }
    var result={};
    result.eventRequestMode = eventRequestMode;
    result.currentEvent = currentEvent;
    result.stepTitle = createTitleForEvent(self.session.get('currentEvent'));
    result.currentManager = self.session.get('currentManager');
    result.currentProgram = self.session.get('currentProgram');
    result.currentRep = self.session.get('currentRep');
    result.speakerStatusArray = constants.SPEAKER_STATUS();
    result.venue = null;
    result.caterer = null;
    result.rep = null;

    result.displayEmailColumn = false;
    result.displayHardCopyColumn = false;
    result.displayInvitationPoolColumn = false;

    if (result.currentEvent.invitationsPDF)
    {
      result.displayEmailColumn = true;
    }

    if (result.currentEvent.invitationsHardcopyDirect)
    {
      result.displayHardCopyColumn = true;
    }

    if (result.currentEvent.invitationsPDFRep)
    {
      result.displayInvitationPoolColumn = true;
    }

    //get the venueid 
    if(currentEvent && currentEvent.venueId)
    {
      geddy.model.Venue.first(currentEvent.venueId, function(err, venuecurrentEvent) {
        //console.log("getting venue "+ currentEvent.venueId);
        result.venue = venuecurrentEvent;

        //get the catererid
        if(currentEvent.catererId)
        {
          geddy.model.Venue.first(currentEvent.catererId, function(err, caterercurrentEvent) {
            //console.log("getting caterer "+ currentEvent.catererId);
            result.caterer = caterercurrentEvent
            
            //get the requesting repId
            if(currentEvent.requestingRepId)
            {
              geddy.model.Rep.first(currentEvent.requestingRepId, function(err, repcurrentEvent) {
                //console.log("getting rep "+ currentEvent.requestingRepId);
                result.rep = repcurrentEvent;

                //console.log(">>>>>>>> result "+ JSON.stringify(result));
                self.respond( result, {format: 'html',template: 'app/views/events/eventStep6'});
              });
            }
            else
            {
              // no requesting repId
              //console.log("result "+ JSON.stringify(result));
              self.respond( result, {format: 'html',template: 'app/views/events/eventStep6'});
            }
          });
        }
        else
        {
          //no caterer
          if (currentEvent.potentialCaterer)
          {
            result.caterer = currentEvent.potentialCaterer[0];
          }

          //get the requesting repId
          if(currentEvent.requestingRepId)
          {
            geddy.model.Rep.first(currentEvent.requestingRepId, function(err, repcurrentEvent) {
              //console.log("getting rep "+ currentEvent.requestingRepId);
              result.rep = repcurrentEvent;
              //console.log("result "+ JSON.stringify(result));
              self.respond( result, {format: 'html',template: 'app/views/events/eventStep6'});
            });
          }
          else
          {
            // no requesting repId
            //console.log("result "+ JSON.stringify(result));
            self.respond( result, {format: 'html',template: 'app/views/events/eventStep6'});
          }
        }
      });  
    }
    else
    {
      //no venue
      if (currentEvent.potentialVenue)
      {
        result.venue = currentEvent.potentialVenue[0];
      }

      //get the catererid
      if(currentEvent && currentEvent.catererId)
      {
        geddy.model.Venue.first(currentEvent.catererId, function(err, caterercurrentEvent) {
          //console.log("getting caterer "+ currentEvent.catererId);
          result.caterer = caterercurrentEvent
          
          //get the requesting repId
          if(currentEvent.requestingRepId)
          {
            geddy.model.Rep.first(currentEvent.requestingRepId, function(err, repcurrentEvent) {
              console.log("getting rep "+ currentEvent.requestingRepId);
              result.rep = repcurrentEvent;
              //console.log("result "+ JSON.stringify(result));
              self.respond( result, {format: 'html',template: 'app/views/events/eventStep6'});
            });
          }
          else
          {
            // no requesting repId
            //console.log("result"+ JSON.stringify(result));
            self.respond( result, {format: 'html',template: 'app/views/events/eventStep6'});
          }
        });
      }
      else
      {
        //no caterer
         if (currentEvent.potentialCaterer)
         {
           result.caterer = currentEvent.potentialCaterer[0];
         }

        //get the requesting repId
        if(currentEvent && currentEvent.requestingRepId)
        {
          geddy.model.Rep.first(currentEvent.requestingRepId, function(err, repcurrentEvent) {
            //console.log("getting rep "+ currentEvent.requestingRepId);
            result.rep = repcurrentEvent;
            //console.log("result "+ JSON.stringify(result));
            self.respond( result, {format: 'html',template: 'app/views/events/eventStep6'});
          });
        }
        else
        {
          // no requesting repId
          //console.log("result "+ JSON.stringify(result));
          self.respond( result, {format: 'html',template: 'app/views/events/eventStep6'});
        }
      }
    }

  };


  // load PRF version of Event (this is the original approved event from manager)
  this.prf = function (req, resp, params) {
    var self = this;

    DBUtils.unlockObject(self);

    self.session.set('displayingPRF', true);
    self.session.set('currentSection',constants.sections.EVENT_PRF);

    self.respond( true,
      {format: 'json'});
  }


  this.finances = function (req, resp, params) {
    var self = this;

    DBUtils.unlockObject(self);

    self.session.set('currentSection', constants.sections.EVENT_FINANCES);

    var result = new Object();
    var currentEvent = self.session.get('currentEvent');
    result.currentEvent = currentEvent;
    result.confirmedSpeaker = '';
    
    //get confirmed speaker
    for (var i = 0; i < currentEvent.potentialSpeakers.length; i++) {
      if (currentEvent.potentialSpeakers[i].crgStatus == constants.SPEAKER_CONFIRMED)
      {
        result.confirmedSpeaker = currentEvent.potentialSpeakers[i].speakerName;
        break;
      }
    };

    self.respond( result,
      {format: 'html', template: 'app/views/events/finances'});
  }


   this.notes = function (req, resp, params) {
       var self = this;

       DBUtils.unlockObject(self);

       var result = {};

       result.currentClient = self.session.get('currentClient');
       result.currentProgram = self.session.get('currentProgram');
       result.currentEvent = self.session.get('currentEvent');
       self.session.set('currentSection',constants.sections.EVENT_NOTES);

       self.respond( result,
         {format: 'html', template: 'app/views/events/notes'});
   }

   this.setCurrentEventNoteIndex = function (req, resp, params)
   {
      var self = this;
      self.session.set('currentNoteIndex', params.noteIndex);

      self.respond(true, {format: 'json'});
   }

   this.saveNote = function (req, resp, params)
   {
      var self = this;
      var currentNoteIndex = self.session.get('currentNoteIndex');

      params.targetIndex = currentNoteIndex;

      DBUtils.saveObjectEmbeddedModel(self,ModelName,params);
   }

   this.eventNoteStep1 = function (req, resp, params) {
       var self = this;
       var result = {};

       var currentEvent = self.session.get('currentEvent');
       
      if (currentEvent.notes && currentEvent.notes.length > 0)
      {
        var noteIndex = self.session.get('currentNoteIndex');
        result.currentNote = currentEvent.notes[noteIndex];
      } else 
      {
        result.currentNote = null;
      }

       self.respond( result,
         {format: 'html', template: 'app/views/events/eventNoteStep1'});
   }


   this.deleteNotes = function (req, resp, params) {
      var self = this;

      DBUtils.deleteFromArrayByIndex(self, params.ids, self.session.get('currentEvent'), 'notes');
   }


   function createTitleForProgramRequest (currentEvent) {
      var headerTitle = "";
      if (currentEvent) {
         if (currentEvent.eventName) {
            headerTitle = currentEvent.eventName + ' - ';
         }
      } else {
         headerTitle = 'New Program Request - '
      }
      return headerTitle;
   }

   function createTitleForEvent (currentEvent,eventRequestMode) {
      var headerTitle = "";
      // if (currentEvent) {
      //    if (currentEvent.presentationTitle) {
      //       headerTitle = currentEvent.presentationTitle + ' - ';
      //    } else {
      //     if (eventRequestMode)
      //     {
      //       headerTitle = 'Event Request - '
      //     }
      //     else
      //     {
      //       headerTitle = 'Event - '
      //     }
      //    }
      // } else {
      //   if (eventRequestMode)
      //   {
      //     headerTitle = 'New Event Request - '
      //   }
      //   else
      //   {
      //     headerTitle = 'New Event - '
      //   }
      // }
      return headerTitle;
   }


   this.audienceSearch = function (req, resp, params)
   {
        var $thisRequest = this;
        var search = require('../helpers/search/search.js');

        var sort = {
          lastName: 'asc',
          firstName : 'asc'
        }

        if (!params['addresses.postalCode'])
        {
          // clean up spaces first
          var currentRep = $thisRequest.session.get('currentRep');

          currentRep.territoryZips = currentRep.territoryZips.replace(/ /g, '');
          var zips = currentRep.territoryZips.split(',');
          
          // var searchObject = {"addresses.postalCode": {$in: zips}, "contactType": 'attendee', 'clients.id': currentClient.id};
         // params["addresses.postalCode"] = zips;
        }

        params.contactType = 'attendee';
        var currentClient = $thisRequest.session.get('currentClient');
        params['clients.id'] = currentClient.id;

        search.AdvanceSearch($thisRequest, null, sort, 'contacts', params, processAudience, zips);
   }

   function processAudience (self, result)
   {
      var currentEvent = self.session.get('currentEvent');

      var displayEmailColumn = 'disabled=disabled';
      var displayHardCopyColumn = 'disabled=disabled';

      if (currentEvent.invitationsPDF == true)
      {
        displayEmailColumn = '';
      }

      if (currentEvent.invitationsHardcopyDirect == true)
      {
        displayHardCopyColumn = '';
      } 

      var i;
      for(i= 0; i < result.dataset.length; i++)
      {
         result.dataset[i].displayEmailColumn = displayEmailColumn;
         result.dataset[i].displayHardCopyColumn = displayHardCopyColumn;
      }
      
      //send results to view
      self.respond(result, { format: 'json' });
   }

};

exports.Events = Events;
