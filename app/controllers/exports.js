var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var Utils = require('../helpers/utils/utils.js');

var Exports = function () {
   this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

   this.before(function () { 
      var self = this;
      if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
      {
        geddy.globalDb.close();
         self.respond('false',{format: 'txt'});
      }
   });

   
   this.setCurrentExportObject = function(req, resp, params)
   {
         var self = this;

         //var currentObject = geddy.model[params.model].create();

         // geddy.model[params.model].first(function(err, data){
         //    if (err)
         //    {
         //       throw err;
         //    }

         //    var currentObject = data;

         //    self.session.set('currentExportObject', currentObject);
         //    self.session.set('currentExportType', params.exportType);

         geddy.model.Client.all({createdAt: {ne: null}}, {sort: {name: 'asc'}}, function (err, clients) {
            if (err) {
               throw err;
            }
            result = {};
            result.clients = clients;
               
            self.respond( result, {format: 'html',template: 'app/views/export/export'});
         });

   }

   this.getCurrentExportObject = function(req, resp, params)
   {
         var self = this;

         var result = {};
         result.obj = self.session.get('currentExportObject');
         result.discardProps = constants.DISCARD_PROPS().toString();

         self.respond(result, {format: 'json'});
   }

   this.clearCurrentExportObject = function(req, resp, params)
   {
         var self = this;

         self.session.set('currentExportObject', null);
         self.session.set('currentExportType', null);
         
         self.respond( true, {format: 'json'});
   }

   this.exportContacts = function(req, resp, params)
   {
         var self = this;

         // self.respond( true,{format: 'html',template: 'app/views/export/export'});
   }

   this.getAllObjectsForExport = function(req, resp, params){
      var self  = this;

      // geddy.model.Contact.all(function(err, contacts){
      //       if (err)
      //       {
      //          throw err;
      //       }

      //       self.respond({contacts: contacts}, {format: 'json'});
      //    });
   }
}

exports.Exports = Exports;
