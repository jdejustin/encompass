//STUTZENV3 whole file 
var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var Utils = require('../helpers/utils/utils.js');
var mime = require('mime');

var Home = function () {
	this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

	this.before(function () { 
      var self = this;
      if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
      {
       geddy.globalDb.close();
        self.respond('false',{format: 'txt'});
      }
      else
      {
        self.session.set('activeController', self.name);
      }
    });

	this.index = function (req, resp, params) {
		var self = this;

		self.session.set('currentSection', constants.sections.HOME);
        var result={};
        result.searchKOLProperties = constants.SEARCH_HOME_KOL();
		result.searchProgramProperties = constants.SEARCH_HOME_PROGRAM();
        result.searchClientProperties = constants.SEARCH_HOME_CLIENT();
        result.searchUserProperties = constants.SEARCH_HOME_USER();
		result.type=params.type;
		console.log("---------"+params);
		geddy.model.Client.all(function(err, clients){
			if (err)
			{
				throw err;
			} 
 
			var clientsArray = [];

			for (var i = 0; i < clients.length; i++) {
				var client = new Object();
				client.id = clients[i].id;
				client.name = clients[i].name;

				clientsArray.push(client);
			};

			result.clients = clientsArray;
			result.currentPage = 1;
			result.totalPages=0;
			self.respond( result,{type:'Home'});
            //self.respond( result, {format: 'html',template: 'app/views/home/index'});
		});
	};

};

exports.Home = Home;

