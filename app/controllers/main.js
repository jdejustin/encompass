/*
 * Geddy JavaScript Web development framework
 * Copyright 2112 Matthew Eernisse (mde@fleegix.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

var util = require('util');

var strategies = require('../helpers/passport/strategies')
  , authTypes = geddy.mixin(strategies, {local: {name: 'local account'}});;

var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var mime = require('mime');
var constants = require('../helpers/utils/constants.js');
var mkdirp = require('mkdirp');
var DBUtils = require('../helpers/utils/DBUtils.js');


var Main = function () {
	
	this.before(function () {
	    //mongo = require('mongodb-wrapper');
	    //geddy.globalDb = mongo.db(geddy.config.db.mongo.host, geddy.config.db.mongo.port, geddy.config.db.mongo.dbname); //var db = mongo.db('localhost', 27017, 'encompass');			
		//console.log("      >>>>>    this.before   " + util.inspect(geddy.globalDb));

	});
		
	//stutzen
	this.index = function (req, resp, params) {
		  var self = this;
		  //console.log ( "  >>>>>>>> MAIN INDEX userid "+ self.session.get('userId') );
		  // TEMP = "not exist";
		  self.session.set('currentSection', ''); //init currentSection to blank, since we're just starting app
		  
			  console.log('>>>>>>>>>>>> role: ' + self.session.get('userRoleId'));
			  var roleId = self.session.get('userRoleId');
			  if (roleId) {
				// console.log ( ">>>>>>>> ACTIVE CONTROLLER "+self.session.get('activeController'));
				var activeController = self.session.get('activeController');

				var userId = self.session.get('userId');
				if (userId)
				{
					//remove any locks that belong to user, in case they were left behind from a timed out session
					DBUtils.removeAllLocks(userId);
				}


				if(activeController)
				{
				  self.redirect({controller: activeController});
				} else {
					//send user to correct portal, based on their role
					switch (roleId)
					{
						case constants.roles.SUPER_ADMIN.label:
						case constants.roles.ADMIN.label:
				  			self.redirect({controller: 'Clients'});	
				  			//console.log('>>>>>>>>>>>>>>>>>>>>> Loading Clients controller ');
				  		break;

					  	case constants.roles.REP.label:
					  		//console.log('>>>>>>>>>>>>>>>>>>>>> Loading Rep Portal controller ');
							self.redirect({controller: 'Portalreps'});
					  		break;

					  	case constants.roles.MANAGER.label:
					  		//console.log('>>>>>>>>>>>>>>>>>>>>> Loading Manager controller ');
					  		self.redirect({controller: 'Portalmanagers'});
					  		break;

					  	case constants.roles.CLIENT.label:
					  		//console.log('>>>>>>>>>>>>>>>>>>>>> Loading Client controller ');
					  		self.redirect({controller: 'Portalclients'});
					  		break;

					  	case constants.roles.KOL.label:
					  		//console.log('>>>>>>>>>>>>>>>>>>>>> Loading Contact controller ');
					  		self.redirect({controller: 'Portalcontacts'});
					  		break;
					  	default:
					  		self.redirect('/login');
					  		break;
					}
				}

				var data = {
      					authType: null
      				};

				if (userId)
					data.authType = authTypes[self.session.get('authType')].name;
		          }
			  else
			  {
				  self.redirect('/login');
			  }

	  };

	  this.login = function (req, resp, params) {
	  	  var self = this;

		  console.log(">>>>>>>>> login params "+JSON.stringify(params));
		  //stutzen

		  //this works with the logo and companyNmae 
		  if (typeof params.companyName == 'undefined' || !params.companyName)
		  {
			  this.session.set("companyName", constants.COMPANIES['crg'].name);
			  this.session.set("companyEmail", constants.COMPANIES['crg'].email);
		  }
		  else
		  {
			  this.session.set("companyName", constants.COMPANIES[params.companyName].name);
			  this.session.set("companyEmail", constants.COMPANIES[params.companyName].email);
		  }
			
		  self.params.showMessage = false;
		console.log(JSON.stringify(constants.COMPANIES)+"1>>>>>>>>> login params "+JSON.stringify(params));
	  	  geddy.model.Setting.first(function(err, settings) {
		      
		      if (settings)
		      {
		      	self.params.showMessage = settings.showMessage;
		      	self.params.maintenanceMessage = settings.maintenanceMessage;
		      }

			  self.respond(self.params, {
				  format: 'html'
					  , template: 'app/views/main/login'
			  });
		  });

	  	  
	  };

	  this.noPrivileges = function (req, resp, params) {
		  this.respond(params, {
			  format: 'html'
				  , template: 'app/views/main/noPrivileges'
		  });
	  };

	  this.expired = function (req, resp, params) {
		  this.respond(params, {
			  format: 'html'
				  , template: 'app/views/main/expired'
		  });
	  };

	  this.logout = function (req, resp, params) {
		  var self = this;

		  DBUtils.unlockObject(self);

          //NOTE: don't clean the companyName, companyEmail
		  this.session.unset('userId');
		  this.session.unset('authType');
		  self.session.unset('activeController');
		  self.session.unset('userFirstName');
          self.session.unset('userLastName');
		  self.session.unset('userRoleId');
          self.session.unset('userLinkedId');
          self.session.unset('userPhoto');
          self.session.unset('userClientId');

          self.session.set('currentSection', '');

		  var data = {
				  user: null
				  , authType: null
				  , showMessage: false
		  };
		
//		  geddy.globalDb.close();

	  	  geddy.model.Setting.first(function(err, settings) {
		      
		      if (settings)
		      {
		      	data.showMessage = settings.showMessage;
		      	data.maintenanceMessage = settings.maintenanceMessage;
		      }

			  self.respond(data, {
				  format: 'html'
					  , template: 'app/views/main/login'
			  });
		  });
	  };

	  // Allows user to upload a picture to the server
	  this.uploadPhoto = function (req, resp, params) {
		  var form = new formidable.IncomingForm();
		  form.uploadDir = geddy.config.UploadTempFile;
		  var self = this;

		  form.on('end', function () {
		  });

		  form.parse(req, function (err, fields, files) {
			  var path = files[0].path;
			  var fileName = files[0].name;
			  var dstpath = geddy.config.StorageDir + 'userPhotos/' + fields.contactId + "/";

			  //Was the file uploaded correctly to server?
			  if (!err && fs.existsSync(path)) {

				  //Does the user's directory exist?
				  if (!fs.existsSync(dstpath))
				  {
					  mkdirp(dstpath, function (error) {
						  if (error) {
							  console.error(error);
						  } else {
							  fs.rename(path, dstpath + fileName, function (err) {
							  });


							  self.respond(
									  {
										  fileFullName: fields.contactId + "/" + fileName
									  }, {
										  format: 'json'
									  });
						  }
					  });
				  } else {
					  //Just rename and go!
					  fs.rename(path, dstpath + fileName, function (err) {
					  });


					  self.respond(
							  {
								  fileFullName: fields.contactId + "/" + fileName
							  }, {
								  format: 'json'
							  });
				  }
			  } else {
				  console.error("Photo was not uploaded correctly to server");
			  }
		  });
	  };

	  // Allows user to upload a file to the server
	  this.uploadFile = function (req, resp, params) {
		  //console.log(">>>>>>>>>>>> uploadFile " + JSON.stringify(params));
		  var self = this;
		  var form = new formidable.IncomingForm();
		  form.uploadDir = geddy.config.UploadTempFile

		  form.on('end', function () {
		  });

		  form.parse(req, function (err, fields, files) {
		  	  //console.log(">>>>>>>>>>>> fields " + JSON.stringify(fields));
		  	  //console.log(">>>>>>>>>>>> files " + JSON.stringify(files));
			  var path = files[0].path;
			  var fileName = files[0].name;
			  var dstpath = geddy.config.StorageDir +"uploadedFiles/" + fields.contactId + "/";

			  //Was the file uploaded correctly to server?
			  if (!err && fs.existsSync(path)) {

				  //Does the user's directory exist?
				  if (!fs.existsSync(dstpath))
				  {
					  mkdirp(dstpath, function (error) {
						  if (error) {
							  console.error(error);
						  } else {
		 		 			  fs.rename(path, dstpath + fileName, function (err) {
							  });


					  		  console.error("File uploaded -- File Exists!");


							  self.respond(
									  {
										  fileFullName: fields.contactId + "/" + fileName
									  }, {
										  format: 'json'
									  });
						  }
					  });
				  } else {
					  //Just rename and go!
					  fs.rename(path, dstpath + fileName, function (err) {
					  });

					  console.error("File uploaded -- Just rename and go!");

					  self.respond(
							  {
								  fileFullName: fields.contactId + "/" + fileName
							  }, {
								  format: 'json'
							  });
				  }
			  } else {
				  console.error("File was not uploaded correctly to server");
			  }
		  });
	  };


	  //updates the name of an uploaded file, which was saved without a contact ID previously
	  this.updateFileName = function (req, resp, params) {
		  //console.log(">>>>>>>>>>>> updateFileName " + JSON.stringify(params));
		  var self = this;
		  var path = "private" + params.fileFullPath;
		  var fileName = path.substring(path.lastIndexOf("/" ) + 1);
		  var dstpath = geddy.config.StorageDir +"uploadedFiles/" + params.contactId + "/";

		  //Does the user's directory exist?
		  if (!fs.existsSync(dstpath))
		  {
			  mkdirp(dstpath, function (error) {
				  if (error) {
					  console.error(error);
				  } else {
					  fs.rename(path, dstpath + fileName, function (err) {
						  if (err) throw err;

						  self.respond(true, {
							  format: 'json'
						  });
					  });
				  }
			  });
		  } else {
			  //Just rename and go!
			  fs.rename(path, dstpath + fileName, function (err) {
				  if (err) throw err;

				  self.respond(true, {
					  format: 'json'
				  });
			  });

		  }
	  };


	  // Allows user to upload a file to the server
	  this.uploadExternalAttachmentFile = function (req, resp, params) {
		  //console.log(">>>>>>>>>>>> uploadFile " + JSON.stringify(params));
		  var self = this;
		  var form = new formidable.IncomingForm();
		  form.uploadDir = geddy.config.UploadTempFile

		  form.on('end', function () {
		  });

		  form.parse(req, function (err, fields, files) {
		  	  //console.log(">>>>>>>>>>>> fields " + JSON.stringify(fields));
		  	  //console.log(">>>>>>>>>>>> files " + JSON.stringify(files));
			  var path = files[0].path;
			  var fileName = fields.fileId + '_attachment_' + files[0].name;
			  var dstpath = geddy.config.StorageDir + "externalAttachments/";

			  //Was the file uploaded correctly to server?
			  if (!err && fs.existsSync(path)) {

				  //Does the user's directory exist?
				  if (!fs.existsSync(dstpath))
				  {
					  mkdirp(dstpath, function (error) {
						  if (error) {
							  console.error(error);
						  } else {
							  fs.rename(path, dstpath + fileName, function (err) {
							  });


					  		  console.error("File uploaded -- File Exists!");


							  self.respond(
									  {
										  fileFullName: fileName
									  }, {
										  format: 'json'
									  });
						  }
					  });
				  } else {
					  //Just rename and go!
					  fs.rename(path, dstpath + fileName, function (err) {
					  });

					  console.error("File uploaded -- Just rename and go!");

					  self.respond(
							  {
								  fileFullName: fileName
							  }, {
								  format: 'json'
							  });
				  }
			  } else {
				  console.error("File was not uploaded correctly to server");
			  }
		  });
	  };

	  this.showError = function (req,resp,params)
	  {
	  	//console.log(" >>>>>>>>>SET FILE TO DISPLAY PARAMS<<<<<<<<<<<<"+JSON.stringify(params));
	  	
	  	displayFlash({ type: 'error', message: params.message });

	  	self.respond(true, {
			format: 'json'
	    });
	  }


	  this.setFileToDisplay = function (req, resp, params)
	  {
	  	var self = this;
	  //	console.log(" >>>>>>>>>SET FILE TO DISPLAY PARAMS<<<<<<<<<<<<"+JSON.stringify(params));

	   	self.session.set('filePathToDisplay', "private" + params.filePath);

	   	self.respond(true, {
			format: 'json'
	    });
	  }


	  this.displayFile = function (req, resp, params)
	  {
	  	var self = this;

	  	//is it a video file?
	  	var filePath = 'private' + params.data;
	  	var fileExt = filePath.substring(filePath.lastIndexOf('.') + 1);
	  	var fileName = filePath.substring(filePath.lastIndexOf('/') + 1, filePath.length);
	  	var displayName = params.displayName;

	  	if (fileExt == 'mp4' && params.os == 'apple')
	  	{
	  		//load up html file to handle video request
	  		self.respond({
	  			videoSource: '/displayVideo?path=' + filePath,
	  			displayName: params.displayName
	  		}, {format: 'html',template: 'app/views/videos/video'});
	  	} else {
	        fs.readFile(filePath, function (err, data) {
	            if (err) {
	                throw err;
	            }


	            fileName = fileName.replace(/\s/g,'_');
	            var mime_type = mime.lookup(fileName);

	            self.output(206, { 
	            	'Content-Disposition': 'attachment; filename=' + fileName,
	                'Content-Type': mime_type},
	                data);
	            return;       
	        });
		}
	  }


	  this.displayVideo = function (req, resp, params)
	  {
	  	var self = this,
        fs = require('fs'),
        path = require('path');
        var movie_mp4;           
        var privateDir = params.path;

        // fs.readFile(path.resolve(privateDir, params.filename), function (err, data) {
        fs.readFile(privateDir, function (err, data) {
            if (err) {
                throw err;
            }
            movie_mp4 = data;

            var total = movie_mp4.length;
            var range = req.headers.range;
            var positions = range.replace(/bytes=/, "").split("-");
            var start = parseInt(positions[0], 10);
            // if last byte position is not present then it is the last byte of the video file.
            var end = positions[1] ? parseInt(positions[1], 10) : total - 1;
            var chunksize = (end-start)+1;

            self.output(206, { "Content-Range": "bytes " + start + "-" + end + "/" + total,
                                 "Accept-Ranges": "bytes",
                                 "Content-Length": chunksize,
                                 "Content-Type":"video/mp4"},
                        movie_mp4.slice(start, end+1));
            return;       
        });
	  }


	  this.displayPhoto = function (req, resp, params)
	  {
	  	var self = this;
	  	var filePath = self.session.get('filePathToDisplay');
	  	var fileExt = filePath.substring(filePath.lastIndexOf('.'));

	    fs.readFile(filePath, 'base64', function (err, data) {
		  if (err) throw err;
		  
		  self.respond({
		  	file: data,
		  	fileEncoding: fileExt}, {
			  format: 'json'
		  });

		  self.session.set('filePathToDisplay', '');
		});
	  }
};
exports.Main = Main;
