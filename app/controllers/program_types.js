var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');

var ProgramTypes = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

   this.before(function () { 
    var self = this;
    if(typeof self.session.get('userRoleId')=='undefined')
    {
      self.redirect('/login');
    }
    self.session.set('activeController', self.name);
    });

  this.index = function (req, resp, params) {
    var self = this;

    self.session.set('currentSection', constants.sections.PROGRAM_TYPES_SECTION);

    DBUtils.unlockObject(self);

    geddy.model.ProgramType.all(function(err, programTypes) {
      if (err) {
        throw err;
      }
      self.respondWith(programTypes, {type:'ProgramType'});
    });
  };

  this.add = function (req, resp, params) {
	  var self = this;
		geddy.model.Service.all(function (err, data) {
			if (err)
			{
				throw err;
			}
			self.respond({params: params, services: data });
		});
	  //this.respond({params: params});
  };

  this.create = function (req, resp, params) {
    var self = this;
    
  if (params.services.constructor === String)
	{
		params.services = [params.services];
	}
    var servicesProcessed = 0;
    var programType = geddy.model.ProgramType.create(params);

    if (!programType.isValid()) {
      this.respondWith(programType);
    }
    else {
    	programType.services = [];
		for (var i=0;i<self.params.services.length;i++)
		{
			var service;
			geddy.model.Service.first(self.params.services[i],  function(err, data) {
				service = data;
				//console.log("service to SAVE " + JSON.stringify(service));
				programType.services.push(service);
				servicesProcessed++;
				if (servicesProcessed == self.params.services.length)
				{
					programType.save(function(err, data) {
						if (err) {
							throw err;
						}
						
						self.respondWith(programType, {status: err});
					});
				}
			});
		}
    }
  };

  this.show = function (req, resp, params) {
    var self = this;

    geddy.model.ProgramType.first(params.id, function(err, programType) {
      if (err) {
        throw err;
      }
      if (!programType) {
        throw new geddy.errors.NotFoundError();
      }
      else {
        self.respondWith(programType);
      }
    });
  };

  this.edit = function (req, resp, params) {
    var self = this;

    geddy.model.ProgramType.first(params.id, function(err, programType) {
      if (err) {
        throw err;
      }
      if (!programType) {
        throw new geddy.errors.BadRequestError();
      }
      else {
        //self.respondWith(programType);
        geddy.model.Service.all(function (err, data) {
			  if (err)
			  {
				  throw err;
			  }
			  
			  for (var i=0;i<data.length;i++)
			  {
				  data[i].checked = programType.hasService(data[i]);
			  }
			  
			  self.respond({params:params,programType:programType,services:data});
	    	  });
      }
    });
  };

  this.update = function (req, resp, params) {
    var self = this;

    geddy.model.ProgramType.first(params.id, function(err, programType) {
      if (err) {
        throw err;
      }
      programType.updateProperties(params);

      if (!programType.isValid()) {
        self.respondWith(programType);
      }
      else {
        programType.save(function(err, data) {
          if (err) {
            throw err;
          }
          self.respondWith(programType, {status: err});
        });
      }
    });
  };

  this.remove = function (req, resp, params) {
    var self = this;

    geddy.model.ProgramType.first(params.id, function(err, programType) {
      if (err) {
        throw err;
      }
      if (!programType) {
        throw new geddy.errors.BadRequestError();
      }
      else {
        geddy.model.ProgramType.remove(params.id, function(err) {
          if (err) {
            throw err;
          }
          self.respondWith(programType);
        });
      }
    });
  };

};

exports.ProgramTypes = ProgramTypes;
