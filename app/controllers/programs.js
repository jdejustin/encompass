var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var search = require('../helpers/search/search.js');
var ModelName= "Program";

var Programs = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

   this.before(function () { 
      var self = this;
      if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
      {
       geddy.globalDb.close();
        self.respond('false',{format: 'txt'});
      }
      else
      {
        self.session.set('activeController', self.name);
      }
    });

  this.index = function (req, resp, params) {
    var self = this;

    DBUtils.unlockObject(self);

    var nextEvents = [];

    self.session.set('currentProgram', null);
    var result={};

    var currentClient = self.session.get('currentClient');

    self.session.set('currentSection', constants.sections.CLIENT_PROGRAMS);
    
    result.programTypes  = null;
    result.currentClient = currentClient;
    result.searchProgramProperties = constants.SEARCH_PROGRAM_PROPERTIES();

    //pagination vars 
    var programCollection = geddy.globalDb.collection('programs');
    var pageLimit = geddy.config.pageLimit;
    var searchObject = {id: {$in: currentClient.programs}};
    var currentPage = 1;
    var recordCount=0;
    var totalPages=0;

    var sort = { programName: 'asc' };

    result.currentPage = currentPage;

    programCollection.find(searchObject).toArray(function(err,allPrograms){
        if (allPrograms != null)
        {
          recordCount = allPrograms.length;
          totalPages = Math.ceil(recordCount/pageLimit);
        } 

        programCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,programs){

          // go to each program to see the events 
          var nextEvent = '';

          if (programs)
          {
            //figure out next event for this program
            for (i = 0; i < programs.length; i++) {
              var program = programs[i];
              if (program.events && program.events.length > 0) 
              {
                var j;
                for (j = 0; j < program.events.length; j++) {
                  if (!nextEvent || nextEvent > program.events[j].eventDate)
                  {
                    nextEvent = program.events[j].eventDate;
                  }
                };
              }
              nextEvents.push(nextEvent);
            }
          } else {
            programs = [];
          }

          result.totalPages = totalPages;
          result.programs = programs;
          result.nextEvents = nextEvents;

          //finally get all the program types available
          geddy.model.ProgramType.all(function(err, types) {
            if (err) {
              throw err;
            }
            if (!types) {
              throw new geddy.errors.NotFoundError();
            }
            else {
              //console.log("types length "+types.length);
              result.programTypes = types;
              self.session.set('programTypes',types);

              self.respond(result,{type:'Program'});
           }
          });
          
        });
    });




/*
    geddy.model.ProgramType.all(function(err, types) {
      if (err) {
        throw err;
      }
      if (!types) {
        throw new geddy.errors.NotFoundError();
      }
      else {
        //console.log("types length "+types.length);
        result.programTypes = types;
        self.session.set('programTypes',types);

        //process any programs that the client has stored
        if (currentClient.programs && currentClient.programs.length > 0)
        {
          var totalIdsProcessed = 0;

          var i;
          for (i = 0; i < currentClient.programs.length; i++) {

            //get program
            geddy.model.Program.first( currentClient.programs[i], function(err, program) {
              if (err) {
                totalIdsProcessed++;
              } else {
                //store result
                programs.push(program);

                var nextEvent = '';

                //figure out next event for this program
                if (program.events && program.events.length > 0) 
                {
                  var j;
                  for (j = 0; j < program.events.length; j++) {
                    if (!nextEvent || nextEvent > program.events[j].eventDate)
                    {
                      nextEvent = program.events[j].eventDate;
                    }
                  };
                }

                nextEvents.push(nextEvent);

                totalIdsProcessed++;
                
                //check if we have processed all the client's programs
                if (totalIdsProcessed == currentClient.programs.length)
                {
                    //we're done!
                    result.programs = programs;
                    result.nextEvents = nextEvents;
                    self.respond( result,{type:'Program'});
                }
              }
            });
          }
        } else {
          result.programs = programs;
          result.nextEvents = nextEvents;
          self.respond( result,{type:'Program'});
        }
      }
    });*/
  };


  this.show = function (req, resp, params) {
    var self = this;

    var continueProcess = true;

    if (params.id == '1234')
    {
      if (self.session.get('currentProgram'))
      {
        params.id = self.session.get('currentProgram').id;
      } else {
        continueProcess = false;
        self.respond('null',{format: 'txt'});
      }
    }

    if (continueProcess)
    {

      DBUtils.unlockObject(self);

      //get contact... we need to get its reps first
      geddy.model.Program.first(params.id, function (err, program) {
        if (err)
        {
          throw err;
        }

        var otherParams = new Object();
        otherParams['programTypes'] = self.session.get('programTypes');

        DBUtils.showObject(self,ModelName,params,constants.sections.PROGRAM_DETAILS, otherParams);

      });
    }
    
  };

  
  this.save = function (req, resp, params) {
    var self = this;

    DBUtils.saveObject(self,ModelName,params);

    var currentClient = self.session.get("currentClient");
    var currentProgram = self.session.get("currentProgram");

    DBUtils.saveDataTo(self, 'Client', currentClient.id, 'programs', currentProgram.id);
  }


  this.saveEmbeddedModel = function (req, resp, params) {
    var self = this;
    DBUtils.saveObjectEmbeddedModel(self,ModelName,params);

    var currentClient = self.session.get("currentClient");
    var currentProgram = self.session.get("currentProgram");

    DBUtils.saveDataTo(self, 'Client', currentClient.id, 'programs', currentProgram.id);
  }

  this.deletePrograms = function (req, resp, params) {
    var self = this;
    DBUtils.deleteObject(self,ModelName,params);
  };


  this.deleteProgramFromClient = function (req, resp, params) {
    var self = this;
    DBUtils.deleteFromArrayByValue(self, params.ids, self.session.get('currentClient'), 'programs');
  }

  this.clearCurrentProgram = function (req, resp, params) {
    var self = this;
    DBUtils.clearCurrentObject(self,ModelName,params);
   };


  // Contact Step pages
  this.programStep1 = function (req, resp, params) {
    var self = this;
   
    var selectPrompt = constants.SELECT_PROMPT();

    var currentProgram = self.session.get('currentProgram');
    
    if (currentProgram && currentProgram.id)
    {
      DBUtils.lockObject(self, currentProgram.id);
    }

    //get all the CRG users
    geddy.model.User.all({or: [{roleId: constants.roles.SUPER_ADMIN.label}, {roleId: constants.roles.ADMIN.label}]},function (err, users){
      if (err) {
        throw err;
      }

      self.respond( {
          stepTitle: createTitle(self.session.get('currentProgram')),
          selectPrompt:selectPrompt,
          crgUsers: users,
          eventTypes: constants.EVENT_TYPES(),
          invitationDeliveries: constants.INVITATION_DELIVERY_TYPES(),
          programTypes: self.session.get('programTypes'), 
          currentClient: self.session.get('currentClient'),
          currentProgram: currentProgram},
         {format: 'html',template: 'app/views/programs/programStep1'}
      );
    });
  };


  ////////////////  Resources  ///////////////////
  this.resources = function (req, resp, params) {
    var self = this;

    DBUtils.unlockObject(self);

    self.session.set('currentSection', constants.sections.PROGRAM_RESOURCES);

    self.respond( {
      currentClient: self.session.get('currentClient'),
      currentProgram: self.session.get('currentProgram')},
      {format: 'html',template: 'app/views/programs/resources'});
  }

  this.deleteResources = function(req, resp, params)
  {
    var self = this;
    DBUtils.deleteUploadedFiles(self, params.ids, self.session.get('currentProgram'), 'resources');
    DBUtils.deleteFromArrayByIndex(self, params.ids, self.session.get('currentProgram'), 'resources');
  }


  ////////////////  Speakers  ///////////////////
  this.speakers = function (req, resp, params) {
    var self = this;

    DBUtils.unlockObject(self);

    var result ={};
    var currentClient = self.session.get('currentClient');
    result.currentClient = currentClient;
    var currentProgram = self.session.get('currentProgram');
    result.currentProgram = currentProgram;

    self.session.set('currentSection', constants.sections.PROGRAM_SPEAKERS);

    result.searchContactProperties = constants.SEARCH_KOL_PROPERTIES();

    //pagination vars 
    var repCollection = geddy.globalDb.collection('reps');
    var pageLimit = 300;//geddy.config.pageLimit;
    var currentPage = 1;
    var recordCount=0;
    var totalPages=0;


    var sort = {
              lastName: 'asc',
              firstName : 'asc'
      } 

    //Get all the client's contacts
    var contactCollection = geddy.globalDb.collection('contacts');
    var contractCollection = geddy.globalDb.collection('contracts');
    
    var searchObject = {"clients.id": currentClient.id, contactType: 'kol'};
    contactCollection.find(searchObject).toArray(function(err, data){
      if (err) {
        throw err;
      }
      if (data != null)
      {
        recordCount = data.length;
        totalPages = Math.ceil(recordCount/pageLimit);
      }

      contactCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,clientContactsResult){
         var clientContacts = clientContactsResult;
         result.contacts = clientContacts;
         result.totalPages = totalPages;

         // build list of contracts per contact, so we can display if we have a program contract or not
         self.session.set('progContracts', []);
         var progContracts = self.session.get('progContracts');
         var i;
         var j;
         //var totalContractsProcessed = 0;
         var contractsIdsArray = [];

         for (i = 0; i < clientContacts.length; i++) {
           if (clientContacts[i].contracts)
           {
             var contracts = clientContacts[i].contracts;
             for (j = 0; j < contracts.length; j++) {
               //get contract
               contractsIdsArray.push(clientContacts[i].contracts[j]);
               
               /*geddy.model.Contract.first(clientContacts[i].contracts[j], function(err, contract) {
                 if (err) {
                   totalContractsProcessed++;
                 } else {
                   totalContractsProcessed++;

                   progContracts.push(contract);

                   if (totalContractsProcessed == contracts.length)
                   {
                     result.currentPage = currentPage;
                     result.progContracts = progContracts,
                     self.respond( result, {format: 'html',template: 'app/views/programs/speakers'}
                     );
                   }
                 }
               });
   */
             }
           }
         }
         //console.log(" <<<<<<<<<<<<<<<<<<< contracts ids array" + JSON.stringify(contractsIdsArray));
         var contractSearchObject = {id: {$in: contractsIdsArray}, $and: [{linkedTo: {$ne: null}}, {linkedTo: currentProgram.id}]}; //linkedTo: {$and [$ne: null}, ]};
         contractCollection.find(contractSearchObject).toArray(function(err, contractsResult){
           progContracts = contractsResult; //set the value to the session because the progContracts is link to the prog contracts session variable
           result.progContracts = progContracts;
           //console.log(" <<<<<<<<<<<<<<<<<<< contracts " + JSON.stringify(progContracts));
           result.currentPage = currentPage;
           self.respond( result, {format: 'html',template: 'app/views/programs/speakers'});
         });
      });
    });
  }


  ////////////////  Reps  ///////////////////
  this.reps = function (req, resp, params) {
    var self = this;

    DBUtils.unlockObject(self);

    var result={};
    var currentClient = self.session.get('currentClient');

    self.session.set('currentSection', constants.sections.PROGRAM_REPS);
    
    result.searchRepProperties = constants.SEARCH_REP_PROPERTIES();
    result.currentProgram = self.session.get('currentProgram');


    var repCollection = geddy.globalDb.collection('reps');
    var pageLimit = geddy.config.pageLimit;
    var searchObject = {id: {$in: currentClient.reps}};
    var currentPage = 1;
    var recordCount=0;
    var totalPages=0;


    //Get all the client's reps
   /* var clientReps = [];
    var totalRepsProcessed = 0;
*/

    var sort = {
      lastName: 'asc',
      firstName : 'asc'
    }

    result.currentPage = currentPage;

    repCollection.find(searchObject).toArray(function(err,allReps){
        if (allReps != null)
        {
          recordCount = allReps.length;
          totalPages = Math.ceil(recordCount/pageLimit);
        }

        repCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,reps){

          result.totalPages = totalPages;
          result.currentClient = currentClient;
          result.reps = reps;

          self.respond(result,{format: 'html',template: 'app/views/programs/reps'});
        });
    });
  }


  function createTitle (currentProgram) {

    var headerTitle = "";

    if(currentProgram) {
      if(currentProgram.programName){
         headerTitle = currentProgram.programName + ' - ';
      }
    } else {   
      headerTitle = 'New Program - '
    }

    return headerTitle;
  }

  /////////////// Search  ////////////////////

  this.search = function (req, resp, params) {
    var $thisRequest = this;
    
    var currentClient = $thisRequest.session.get('currentClient');
    if(currentClient && currentClient.programs){
      var searchObject = {id: {$in: currentClient.programs}};
    }else{
      var searchObject = {};
    }

    var sort = { programName: 'asc' };

    search.AdvanceSearch($thisRequest, searchObject, sort, 'programs', params);
  }

  this.speakerSearch = function (req, resp, params) {
    var $thisRequest = this;

	var currentClient = $thisRequest.session.get('currentClient');
    var searchObject = {'clients.id': currentClient.id, "contactType": 'kol'};
    
    var sort = {
            lastName: 'asc',
            firstName : 'asc'
    }

    search.AdvanceSearch($thisRequest, searchObject, sort, 'contacts', params, processContracts);
  }


  function processContracts (self, result)
  {
    var i;
    var progContracts = self.session.get('progContracts');

    for(i= 0 ; i < result.dataset.length; i++)
    {
      //for each result, see if a contract exists
      //but default to not found
      result.dataset[i].contractAction = 'Upload';
      result.dataset[i].contractId = null;
      result.dataset[i].btnClassType = 'btn-info';

      var contactId = result.dataset[i].id;
      var j;
      for (j = 0; j < progContracts.length; j++) {
        if (progContracts[j].contactId == contactId)
        {
          //found it!
          result.dataset[i].contractAction = 'Update';
          result.dataset[i].contractId = progContracts[j].id;
          result.dataset[i].btnClassType = 'btn-primary';
        }
      };
    }

    //send results to view
    self.respond(result, { format: 'json' });
  }


};

exports.Programs = Programs;
