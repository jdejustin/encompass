var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var Utils = require('../helpers/utils/utils.js');
var mime = require('mime');

var Reports = function () {
	this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

	this.before(function () { 
      var self = this;
      if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
      {
       geddy.globalDb.close();
        self.respond('false',{format: 'txt'});
      }
      else
      {
        self.session.set('activeController', self.name);
      }
    });

	this.index = function (req, resp, params) {
		var self = this;

		self.session.set('currentSection', constants.sections.REPORTS);

		var result = {};
		result.reportTypes = constants.REPORT_TYPES();

		geddy.model.Client.all(function(err, clients){
			if (err)
			{
				throw err;
			} 
 
			var clientsArray = [];

			for (var i = 0; i < clients.length; i++) {
				var client = new Object();
				client.id = clients[i].id;
				client.name = clients[i].name;

				clientsArray.push(client);
			};

			result.clients = clientsArray;
	
			self.respond(result, {params: params});
		});
	};

	this.generateReport = function(req, resp, params)
	{
		var self  = this;

		var reportType = params.reportType;
		var clientId = params.clientId;

		//based on type of report, make the necessary queries to gather all the information needed

		switch(reportType)
		{
			case 'Speaker List':
				 generateSpeakerList(params.clientId, self);
				 break;

			case 'Sales Force List':
				 generateRepsList(params.clientId, self);
				 break;

			case 'Remaining Funds':
				 generateRemainingFundsReport(params.clientId, self);
				 break;

			case 'Data Region Summary':
					generateDataRegionSummary(params.clientId, self);
				 break;

			case 'Speaker Usage Summary':
					generateUsageSummary(params.clientId, self);
				 break;

			case 'Program Status':
				generateProgramStatusReport(params.clientId, self);
				 break;

			case 'Attendance Report':
				generateAttendenceReport(params.clientId, self);
				 break;

			case 'HCP Info Report':
				generateHCPInfoReport(params.clientId, self);
				 break;

			case 'Reconciliation Report':
				generateReconciliationReport(params.clientId, self);
				break;
		}
	}


	function generateTable(objectArray, fileName, self)
	{
	  	var table = '<html><body><table><thead><tr>';

	    //create header 
	    if (objectArray.length > 0)
	    {

	      var tempObject = objectArray[0];
	      var propertiesArray = Object.getOwnPropertyNames(tempObject);

	      for (i= 0; i < propertiesArray.length; i++) {
	         var headerName = humanNameProp(propertiesArray[i]);

	         table += "<th>"+headerName+"</th>";
	      }

	      table += '</tr>';

	      table += '<tbody>';

	      for (j= 0; j < objectArray.length; j++) {
	        var newElement = "<tr>";
	        for (k= 0; k < propertiesArray.length; k++) {
	         var value = objectArray[j][propertiesArray[k]];

	         if (value == undefined)
	         {
	            value = ''
	         }
	            newElement += "<td>"+ value +"</td>";
	        }

	        newElement += "</tr>";
	        table += newElement;
	      }
	    }

	    table += '</tbody></table></body></html>';

	    var mime_type = mime.lookup(fileName);

		 self.output(206, { 
        	'Content-Disposition': 'attachment;filename=' + fileName,
            'Content-Type': mime_type},
            table);
        return; 

  	}


  	function humanNameProp (stringElement)
   	{
      	stringElement = stringElement.replace(/([A-Z])/g," $1");
      	stringElement = stringElement.substring(0,1).toUpperCase() + stringElement.substring(1,stringElement.length); 
      	return stringElement;
   	}


	function generateSpeakerList (clientId, self)
	{
			var contactCollection = geddy.globalDb.collection('contacts');

			var searchObject = new Object({'clients.id': clientId, contactType:'kol'});
			var projection = new Object({_id:0, firstName: 1, middleName: 1, lastName: 1, suffix:1, degrees:1, otherDegree:1, affiliations:1, addresses:1, phoneNumbers:1, emailsAddresses: 1, medicalLicenses:1, npi:1, taxonomyCode:1, specialties:1 });

			var sort = {
				 lastName: 'asc',
				 firstName : 'asc'
			}

			contactCollection.find(searchObject, projection).sort(sort).toArray(function(err,contacts){
				 if (err)
				 {
						throw err;
				 }

				 var reportContacts = [];
				 var i;
				 for (i = 0; i < contacts.length; i++) {
						var contact = {};
						contact.firstName = contacts[i].firstName;
						contact.middleName = contacts[i].middleName;
						contact.lastName = contacts[i].lastName;
						contact.suffix = contacts[i].suffix;
						contact.degrees = contacts[i].degrees;

						if (contacts[i].otherDegree)
						{
							contact.degrees += ',' + contacts[i].otherDegree;
						}

						contact.institution = '';
						if (contacts[i].affiliations)
						{
							 contact.institution = contacts[i].affiliations[0].affiliationName;
						}

						contact.streetAddress = '';
						contact.streetAddress2 = '';
						contact.city = '';
						contact.state = '';
						contact.postalCode = '';
						contact.addressType = '';
						contact.country = '';

						var j;
						if (contacts[i].addresses)
						{
							 for (j = 0; i < contacts[i].addresses.length; i++) {
									if (contacts[i].addresses[j].preferred)
									{
										 contact.streetAddress = contacts[i].addresses[j].address1;
										 contact.streetAddress2 = contacts[i].addresses[j].address2;
										 contact.city = contacts[i].addresses[j].city;
										 contact.state = contacts[i].addresses[j].state;
										 contact.postalCode = contacts[i].addresses[j].postalCode;
										 contact.addressType = contacts[i].addresses[j].addressType;
										 contact.country = contacts[i].addresses[j].country;
										 break;
									}
							 };
						}

						contact.phoneNumber = '';
						contact.phoneType = '';

						if (contacts[i].phoneNumbers)
						{
							 for (j = 0; i < contacts[i].phoneNumbers.length; i++) {
									if (contacts[i].phoneNumbers[j].preferred)
									{
										 contact.phoneNumber = contacts[i].phoneNumbers[j].number;
										 contact.phoneType = contacts[i].phoneNumbers[j].phoneType;
										 break;
									}
							 };
						}

						contact.email = '';

						if (contacts[i].emailAddresses)
						{
							 for (j = 0; i < contacts[i].emailAddresses.length; i++) {
									if (contacts[i].emailAddresses[j].preferred)
									{
										 contact.email = contacts[i].emailAddresses[j].email;
										 break;
									}
							 };
						}

						contact.medicalLicenseNumber = '';
						contact.medicalLicenseState = '';

						if(contacts[i].medicalLicenses)
						{
							 for (j = 0; i < contacts[i].medicalLicenses.length; i++) {
										 contact.medicalLicenseNumber = contacts[i].medicalLicenses[j].number;
										 contact.medicalLicenseState = contacts[i].medicalLicenses[j].state;
										 break;
							 };
						}

						contact.npi = contacts[i].npi;
						contact.taxonomyCode = '';
						if (contacts[i].taxonomyCode)
						{
							contact.taxonomyCode = contacts[i].taxonomyCode;
						}
						contact.specialties = contacts[i].specialties;
						reportContacts.push(contact);
				 };

				 var finalTable = generateTable(reportContacts, "Speaker_List.xls", self);
			});
	 }

	

	function generateRepsList (clientId, self)
	{
			var result = {};

			geddy.model.Client.first(clientId, function(err, client){
				 if (err)
				 {
						throw err;
				 }

				 var repCollection = geddy.globalDb.collection('reps');
				 var searchObject = {id: {$in: client.reps}};
				 var projection = new Object({_id:0, firstName: 1, middleName: 1, lastName: 1, suffix:1, addresses:1, phoneNumbers:1, emailsAddresses: 1, region:1, team:1, role:1, zone:1, territory:1, status:1, clientRepId:1, hireDate:1});

				 var sort = {
					 lastName: 'asc',
					 firstName : 'asc'
				 }  

				 repCollection.find(searchObject, projection).sort(sort).toArray(function(err,reps){
						if (err)
						{
							 throw err;
						}

						var reportReps = [];
						var i;
						for (i = 0; i < reps.length; i++) {
							 var rep = {};
							 rep.firstName = reps[i].firstName;
							 rep.middleName = reps[i].middleName;
							 rep.lastName = reps[i].lastName;
							 rep.suffix = reps[i].suffix;

							 rep.streetAddress = '';
							 rep.streetAddress2 = '';
							 rep.city = '';
							 rep.state = '';
							 rep.postalCode = '';
							 rep.addressType = '';
							 rep.country = '';

							 var j;
							 if (reps[i].addresses)
							 {
									for (j = 0; i < reps[i].addresses.length; i++) {
										 if (reps[i].addresses[j].preferred)
										 {
												rep.streetAddress = reps[i].addresses[j].address1;
												rep.streetAddress2 = reps[i].addresses[j].address2;
												rep.city = reps[i].addresses[j].city;
												rep.state = reps[i].addresses[j].state;
												rep.postalCode = reps[i].addresses[j].postalCode;
												rep.addressType = reps[i].addresses[j].addressType;
												break;
										 }
									};
							 }

							 rep.phoneNumber = '';
							 rep.phoneType = '';

							 if (reps[i].phoneNumbers)
							 {
									for (j = 0; i < reps[i].phoneNumbers.length; i++) {
										 if (reps[i].phoneNumbers[j].preferred)
										 {
												rep.phoneNumber = reps[i].phoneNumbers[j].number;
												rep.phoneType = reps[i].phoneNumbers[j].phoneType;
												break;
										 }
									};
							 }

							 rep.email = '';

							 if (reps[i].emailAddresses)
							 {
									for (j = 0; i < reps[i].emailAddresses.length; i++) {
										 if (reps[i].emailAddresses[j].preferred)
										 {
												rep.email = reps[i].emailAddresses[j].email;
												break;
										 }
									};
							 }

							 rep.region = reps[i].region;
							 rep.team = reps[i].team;
							 rep.role = reps[i].role;
							 rep.zone = reps[i].zone;
							 rep.territory = reps[i].territory;
							 rep.status = reps[i].status;
							 rep.employeeId = reps[i].clientRepId;
							 rep.hireDate = reps[i].hireDate;

							 reportReps.push(rep);
						};

						var finalTable = generateTable(reportReps, "Speaker_Field_Force_List.xls", self);
				 });
			});
	 }


	 function generateProgramStatusReport (clientId, self)
	 {
			var result = {};

			//join clients, events, reps
			geddy.model.Client.first(clientId, function(err, client){
				 if (err)
				 {
						throw err;
				 }

				 var searchObject = {programId: {$in: client.programs}};
				 var projection = new Object({_id:0, approvalStatus: 1, jobNumber: 1, status: 1, eventType:1, addresses:1, potentialSpeakers:1, confirmedDate: 1, eventStartTime:1, requestingRepName:1});

				 var eventsCollection = geddy.globalDb.collection('events');

				 eventsCollection.find(searchObject).toArray(function(err, allEvents){
						if (err)
						{
							 throw err;
						}

						var reportEvents = [];
						var approvalStatus = constants.EVENT_APPROVAL_STATUS();
						var eventStatus = constants.EVENT_STATUS();
						var i;
						for (i = 0; i < allEvents.length; i++) {
							 var evnt = new Object();
							 evnt.salesRepresentative = allEvents[i].requestingRepName;
							 evnt.approvedOrDeniedStatus = approvalStatus[allEvents[i].approvalStatus].type;
							 evnt.jobNumber = allEvents[i].jobNumber;
							 evnt.programStatus = eventStatus[allEvents[i].status].type;
							 evnt.programType = allEvents[i].eventType;

							 evnt.confirmedSpeaker = '';
							 if (allEvents[i].potentialSpeakers)
							 {
									var j;
									for (var j = 0; j < allEvents[i].potentialSpeakers.length; j++) {

										 if (allEvents[i].potentialSpeakers[j].crgStatus == constants.SPEAKER_CONFIRMED)
										 {
												 evnt.confirmedSpeaker = allEvents[i].potentialSpeakers[j].speakerName;
												 evnt.confirmedSpeakerCityAndState = allEvents[i].potentialSpeakers[j].city + ', ' + allEvents[i].potentialSpeakers[j].state;
												 break;
										 }
									};
							 }
							 evnt.programDate = allEvents[i].confirmedDate;
							 evnt.programTime = allEvents[i].eventStartTime;

							 reportEvents.push(evnt);
						};

						var finalTable = generateTable(reportEvents, "Program_Status_Report.xls", self);
				 })
						
			});
	 }


	 function generateAttendenceReport (clientId, self)
	{
			var result = {};

			//event, contact, attendee, 

			geddy.model.Client.first(clientId, function(err, client){
				 if (err)
				 {
						throw err;
				 }

				 var searchObject = {programId: {$in: client.programs}};
				 var projection = new Object({_id:0, approvalStatus: 1, jobNumber: 1, status: 1, eventType:1, addresses:1, potentialSpeakers:1, confirmedDate: 1, eventStartTime:1, requestingRepName:1});

				 var eventsCollection = geddy.globalDb.collection('events');

				 eventsCollection.find(searchObject).toArray(function(err, allEvents){
						if (err)
						{
							 throw err;
						}

						var reportEvents = [];
						var approvalStatus = constants.EVENT_APPROVAL_STATUS();
						var eventStatus = constants.EVENT_STATUS();


						//get all venues
						searchObject = {id: {$in: client.venues}};
						var venuesCollection = geddy.globalDb.collection('venues');
						
						venuesCollection.find(searchObject).toArray(function(err, allVenues){
							 if (err)
							 {
									throw err;
							 }

							 var MJ = require("mongo-fast-join");
							 var MongoClient = require('mongodb').MongoClient;
							 var mongoJoin = new MJ();
							 var connectionString = 'mongodb://'+geddy.config.db.mongo.host+':'+geddy.config.db.mongo.port+'/'+geddy.config.db.mongo.dbname;


							 if (typeof(geddy.config.db.mongo.serverOptions)){
									if(geddy.config.db.mongo.serverOptions.ssl){
										 connectionString += "?ssl=true";
									}
							 }

							 //get all attendee ids from all the client's events
							 var j;
							 var attendeesIds = [];
							 for (j = allEvents.length - 1; j >= 0; j--) {
									attendeesIds = attendeesIds.concat(allEvents[j].attendees);
							 };

							 MongoClient.connect(connectionString, function(err, db) {
									if(err) throw err;
									var collection1 = db.collection('attendees');
									var collection2 = db.collection('contacts');
									
									mongoJoin.query(collection1,
											 {id: {$in : attendeesIds}}, //query statement
											 {}, //fields
											 {
													// limit: 10000//options
											 }
									 ).join({
											 joinCollection: collection2,
											 //respects the dot notation, multiple keys can be specified in this array
											 leftKeys: ["contactId"],
											 //This is the key of the document in the right hand document
											 rightKeys: ["_id"],
											 //This is the new subdocument that will be added to the result document
											 newKey: "contact"
									 }).exec(function (err, allAttendees) {    //Call exec to run the compiled query and catch any errors and results, in the callback
									 		db.close();

									 		var status_array = constants.ATTENDEE_STATUS_TYPE();

												var i;

												for (i = 0; i < allEvents.length; i++) {
													if (allEvents[i].jobNumber)
													{
														 var evnt = new Object();
														 evnt.jobNumber = allEvents[i].jobNumber;
														 // evnt.confirmedSpeakerName = '';
														 // evnt.confirmedSpeakerCityAndState = '';

														 // if (allEvents[i].potentialSpeakers && allEvents[i].potentialSpeakers.length > 0)
														 // {
															// 	var j;
															// 	for (var j = 0; j < allEvents[i].potentialSpeakers.length; j++) {

															// 		 if (allEvents[i].potentialSpeakers[j].crgStatus && allEvents[i].potentialSpeakers[j].crgStatus == constants.SPEAKER_CONFIRMED)
															// 		 {
															// 				 evnt.confirmedSpeakerName = allEvents[i].potentialSpeakers[j].speakerName;
															// 				 evnt.confirmedSpeakerCityAndState = allEvents[i].potentialSpeakers[j].city + ', ' + allEvents[i].potentialSpeakers[j].state;
															// 				 break;
															// 		 } else {
															// 		 	 evnt.confirmedSpeakerName = ''
															// 			 evnt.confirmedSpeakerCityAndState = '';
															// 		 }
															// 	};
														 // }

														// evnt.programStatus = (allEvents[i].crgStatus)? eventStatus[allEvents[i].crgStatus] : 'N/A';

														 //console.log('event status: ' + evnt.programStatus);

														 evnt.programDate = allEvents[i].confirmedDate;
														 evnt.programTime = allEvents[i].eventStartTime;

														 evnt.venueCity = '';
														 evnt.venueState = '';
														 
														 if (allEvents[i].venueId)
														 {
																var z;
																var venueId = allEvents[i].venueId;

																for (z = 0; z < allVenues.length; z++) {
																	 if (allVenues[z].id == venueId)
																	 {
																			if (allVenues[z].addresses && allVenues[z].addresses[0])
																			{
																				 evnt.venueCity = allVenues[z].addresses[0].city;
																			} else {
																				 evnt.venueCity = '';
																			}

																			 if (allVenues[z].addresses && allVenues[z].addresses[0])
																			{
																				 evnt.venueState = allVenues[z].addresses[0].state;
																			} else {
																				 evnt.venueState = '';
																			}

																			break;
																	 }
																};
														 }

														 evnt.salesRepresentative = (allEvents[i].requestingRepName)? allEvents[i].requestingRepName : '';
														 evnt.typeOfProgram = allEvents[i].eventType;

														 evnt.expectedNumberAttendees = allEvents[i].expectedNumberAttendees;

														 // var emailInvitations = (allEvents[i].emailInvitations)? allEvents[i].emailInvitations.length : 0;
														 // var hardcopyInvitations = (allEvents[i].hardcopyInvitations)? allEvents[i].hardcopyInvitations.length : 0;

														 // evnt.numberOfInvited = emailInvitations + hardcopyInvitations;
														 // evnt.estimatedAttendance = allEvents[i].expectedNumberAttendees;

														 if (allEvents[i].attendees && allEvents[i].attendees.length > 0)
														 {
															 var a;
															 var totalRegistered = 0;
															 for (a = 0; a < allEvents[i].attendees.length; a++) {
																	if (allEvents[i].attendees[a].status  == 6)
																	{
																		totalRegistered++;
																	}
																};

															// evnt.totalRegistered = totalRegistered;
															var startingRow = (reportEvents.length == 0)? 3: reportEvents.length + 3;
															var endingRow = startingRow + allEvents[i].attendees.length - 1;
																evnt.totalRegistered = '=ROWS(J'+ startingRow + ':J' + endingRow + ')'
														 } else {
															evnt.totalRegistered = 0;
														 }

														 evnt.attendeeStatus = '';
														 evnt.firstName = '';
														 evnt.lastName = '';
														 evnt.degrees = '';
														 evnt.affiliation = '';
														 evnt.address = '';
														 evnt.city = '';
														 evnt.state = '';
														 evnt.postalCode = '';
														 evnt.phone = '';
														 evnt.fax = '';
														 evnt.email = '';
														 evnt.npiNumber = '';
														 evnt.taxonomyCode = '';
														 evnt.licenseNumber = '';
														 evnt.licensingState = '';


														 reportEvents.push(evnt);


														 //build rows for attendees
														 if (allEvents[i].attendees && allEvents[i].attendees.length > 0)
														 {
															 for (var x = allEvents[i].attendees.length - 1; x >= 0; x--) {
																var extraRow = new Object();
																extraRow.jobNumber = evnt.jobNumber;
																extraRow.programDate = evnt.programDate;
														 		extraRow.programTime = evnt.programTime;
														 		extraRow.venueCity = evnt.venueCity;
														 		extraRow.venueState = evnt.venueState;
														 		extraRow.salesRepresentative = evnt.salesRepresentative;
														 		extraRow.typeOfProgram = evnt.typeOfProgram;
														 		extraRow.expectedNumberAttendees = evnt.expectedNumberAttendees;
														 		extraRow.totalRegistered = evnt.totalRegistered;

																extraRow.attendeeStatus = '';
																extraRow.firstName = '';
																extraRow.lastName = '';
																extraRow.degrees = '';
																extraRow.affiliation = '';
																extraRow.address = '';
																extraRow.city = '';
																extraRow.state = '';
																extraRow.postalCode = '';
																extraRow.phone = '';
															    extraRow.fax = '';
															    extraRow.email = '';
															    extraRow.npiNumber = '';
															    extraRow.taxonomyCode = '';
															    extraRow.licenseNumber = '';
															    extraRow.licensingState = '';

																var attendeeId = allEvents[i].attendees[x];

																for (var w = allAttendees.length - 1; w >= 0; w--) {
																	 if (attendeeId == allAttendees[w].id)
																	 {
																	 	if (allAttendees[w].contact)
																	 	{
																				var data = allAttendees[w].contact;

																				if (allAttendees[w].status)
																				{
																					for (var st = status_array.length - 1; st >= 0; st--) {
																						if (allAttendees[w].status ==  status_array[st].id)
																						{
																							extraRow.attendeeStatus = status_array[st].label;
																						}
																					};
																				}

																				extraRow.firstName +=  data.firstName + '<br/>';                             
																				extraRow.lastName +=  data.lastName + '<br/>';    

																				if (data.degrees && data.degrees.length > 0)
																				{
																					extraRow.degrees +=  data.degrees.toString() + '<br/>';
																					if (data.otherDegree)
																					{
																						extraRow.degrees += ',' + data.otherDegree;
																					}
																				} else {
																					extraRow.degrees += '' + '<br/>'; 
																				} 

																				if (data.affiliations && data.affiliations.length > 0)
																				{
																					 extraRow.affiliation = data.affiliations[0].affiliationName + '<br/>';
																				} else {
																					 extraRow.affiliation += '' + '<br/>'; 
																				}

																				if (data.addresses && data.addresses.length > 0)
																				{
																					extraRow.address += data.addresses[0].address1;
																					if (data.addresses[0].address2)
																					{
																						extraRow.address += data.addresses[0].address2 + '<br/>';
																					} else {
																						extraRow.address += '' + '<br/>'; 
																					}

																					if (data.addresses[0].city)
																					{
																						extraRow.city += data.addresses[0].city + '<br/>';
																					} else {
																						extraRow.city += '' + '<br/>'; 
																					}

																					if (data.addresses[0].state)
																					{
																						extraRow.state += data.addresses[0].state + '<br/>';
																					} else {
																						extraRow.state += '' + '<br/>'; 
																					}

																					if (data.addresses[0].postalCode)
																					{
																						extraRow.postalCode += data.addresses[0].postalCode + '<br/>';
																					} else {
																						extraRow.postalCode += '' + '<br/>'; 
																					}
																				}

																				if (data.phoneNumbers && data.phoneNumbers.length > 0)
																				{
																					for (var ph = data.phoneNumbers.length - 1; ph >= 0; ph--) {
																						if (data.phoneNumbers[ph].phoneType && data.phoneNumbers[ph].phoneType == 'Fax')
																						{
																							if (data.phoneNumbers[ph].number)
																							{
																								extraRow.fax += data.phoneNumbers[ph].number + '<br/>';
																							}
																							break;
																						} 
																					};

																					for (ph = data.phoneNumbers.length - 1; ph >= 0; ph--) {
																						if (data.phoneNumbers[ph].phoneType && data.phoneNumbers[ph].phoneType != 'Fax')
																						{
																							if (data.phoneNumbers[ph].number)
																							{
																								extraRow.phone += data.phoneNumbers[ph].number + '<br/>';
																							}
																							break;
																						} 
																					};
																				}

																				if (data.defaultEmail)
																				{
																					extraRow.email += data.defaultEmail + '<br/>';
																				} else {
																					if (data.emailAddresses && data.emailAddresses.length > 0)
																					{
																						extraRow.email += data.emailAddresses[0].email + '<br/>';
																					} else {
																						extraRow.email += '' + '<br/>'; 
																					}
																				}

																				if (data.npi)
																				{
																					extraRow.npiNumber += data.npi + '<br/>';
																				} else {
																					extraRow.npiNumber += '' + '<br/>'; 
																				}

																				if (data.taxonomyCode)
																				{
																					extraRow.taxonomyCode += data.taxonomyCode + '<br/>';
																				} else {
																					extraRow.taxonomyCode += '' + '<br/>'; 
																				}

																				if (data.medicalLicenses && data.medicalLicenses.length > 0)
																				{
																					if (data.medicalLicenses[0].number)
																					{
																						extraRow.licenseNumber += data.medicalLicenses[0].number + '<br/>';
																					}

																					if (data.medicalLicenses[0].state)
																					{
																						extraRow.licensingState += data.medicalLicenses[0].state + '<br/>';
																					}
																				} else {
																					extraRow.licenseNumber += '' + '<br/>'; 
																					extraRow.licensingState += '' + '<br/>'; 
																				}
																			}
																			
																			break;
																	 }
																};

																// extraRow.programDate = '';
																// extraRow.programTime = '';
																// extraRow.venueCity = '';
																// extraRow.venueState = '';

																reportEvents.push(extraRow);
															};
														}
													}
												};

												var finalTable = generateTable(reportEvents, "Attendance_Report.xls", self);
									});
							 });
						});
				 })
						
			});
	}


	function generateUsageSummary (clientId, self)
	{
			var result = {};

			geddy.model.Client.first(clientId, function(err, client){
				 if (err)
				 {
						throw err;
				 }

				 var contactCollection = geddy.globalDb.collection('contacts');

				 var searchObject = new Object({'clients.id': clientId, contactType:'kol'});
				 var projection = new Object({_id: 1, firstName: 1, lastName: 1, degrees:1, otherDegree:1, affiliations:1, addresses:1});

				 var sort = {
						lastName: 'asc',
						firstName : 'asc'
				 }

				 var totalContactsProcessed = 0;

				 contactCollection.find(searchObject, projection).sort(sort).toArray(function(err,contacts){
						if (err)
						{
							 throw err;
						}

						var contactIds = [];
						var i;
						for (i = 0; i < contacts.length; i++) {
							contactIds.push(contacts[i]._id);
						}

						//get all the events where these contacts are potential speakers and that belong to this client
						var searchEventsObject = {'potentialSpeakers.contactId': {$in: contactIds}, programId: {$in: client.programs}, status: 2};

						var eventsCollection = geddy.globalDb.collection('events');

						eventsCollection.find(searchEventsObject).toArray(function(err, completedEvents){
							 if (err)
							 {
									throw err;
							 }

							 var searchEventsObject2 = {'potentialSpeakers.contactId': {$in: contactIds}, programId: {$in: client.programs}, status: 5};

							 eventsCollection.find(searchEventsObject2).toArray(function(err, cancelledEvents){
									if (err)
									{
										 throw err;
									}

									var searchEventsObject3 = {'potentialSpeakers.contactId': {$in: contactIds}, programId: {$in: client.programs}, status: 3};

									eventsCollection.find(searchEventsObject3).toArray(function(err, reconciledEvents){
										 if (err)
										 {
												throw err;
										 }

										 var searchEventsObject4 = {'potentialSpeakers.contactId': {$in: contactIds}, programId: {$in: client.programs}, status: 0};

										 eventsCollection.find(searchEventsObject4).toArray(function(err, notConfirmedEvents){
												if (err)
												{
													 throw err;
												}

												var searchEventsObject5 = {'potentialSpeakers.contactId': {$in: contactIds}, programId: {$in: client.programs}, status: 1};

												eventsCollection.find(searchEventsObject5).toArray(function(err, confirmedEvents){
													 if (err)
													 {
															throw err;
													 }

													 var reportContacts = [];
													 for (i = 0; i < contacts.length; i++) {
															var contact = {};
															contact.lastName = contacts[i].lastName;
															contact.firstName = contacts[i].firstName;
															contact.degrees = contacts[i].degrees;

															if (contacts[i].otherDegree)
															{
																contact.degrees += ',' + contacts[i].otherDegree;
															}

															contact.institution = '';
															if (contacts[i].affiliations)
															{
																 contact.institution = contacts[i].affiliations[0].affiliationName;
															}

															contact.city = '';
															contact.state = '';

															var j;
															if (contacts[i].addresses)
															{
																 for (j = 0; j < contacts[i].addresses.length; j++) {
																		if (contacts[i].addresses[j].preferred == 'true')
																		{
																			 contact.city = contacts[i].addresses[j].city;
																			 contact.state = contacts[i].addresses[j].state;
																			 break;
																		}
																 };
															}

															//let's get the totals for each event status type
															var completedTotals = searchEvents(completedEvents, contacts[i]._id, true);
															contact.completed = completedTotals.counter;

															var scheduleTotals = searchEvents(confirmedEvents, contacts[i]._id, false);
															contact.scheduled = scheduleTotals.counter;

															var cancelledTotals = searchEvents(cancelledEvents, contacts[i]._id, false);
															contact.cancelled = cancelledTotals.counter;

															var notConfirmedTotals = searchEvents(notConfirmedEvents, contacts[i]._id, false);
															contact.notConfirmed = notConfirmedTotals.counter;

															var reconciledTotals = searchEvents(reconciledEvents, contacts[i]._id, true);
															contact.reconciled = reconciledTotals.counter;

															var totalHonorarium = completedTotals.honorarium + reconciledTotals.honorarium;
															contact.YearToDateHonorarium = Utils.formatCurrency(totalHonorarium);

															reportContacts.push(contact);
													 }

													 //we're done
													 var finalTable = generateTable(reportContacts, "Usage_Summary.xls", self);
												});
										 });
									});
							 });
						});
				 });
			});
	 }

		function generateDataRegionSummary (clientId, self)
		{
				 var result = {};
				 var report = [];

				 geddy.model.Client.first(clientId, function(err, client){
						if (err)
						{
							 throw err;
						}

						var searchEventsObject = {programId: {$in: client.programs}, status: 2};
						var projection = new Object({_id: 0, eventType:1, requestingRepId:1});
						var eventsCollection = geddy.globalDb.collection('events');

						eventsCollection.find(searchEventsObject, projection).toArray(function(err, allEvents){
							 if (err)
							 {
									throw err;
							 }

							var repsCollection = geddy.globalDb.collection('reps');
							var repsProjection = new Object({_id: 1, region:1});

							searchEventsObject = {id: {$in: client.reps}};
							repsCollection.find(searchEventsObject, repsProjection).toArray(function(err, allReps){
								if (err)
								{
									throw err;
								}

								var regions = [];
								for (var z = allReps.length - 1; z >= 0; z--) {
									var repRegion = allReps[z].region;
									if (repRegion)
									{
										if (regions.indexOf(repRegion) == -1)
										{
											regions.push(repRegion);

											var row = {};
											row.region = repRegion;

											row.completedInOfficePrograms = 0;
											row.completedOutOfOfficePrograms = 0;
											row.completedWebinarOnlyPrograms = 0;
											row.completedWebinarWithCateringPrograms = 0;
											row.totalCompleted = 0;

											 for (var x = allEvents.length - 1; x >= 0; x--) {
												if (allEvents[x].requestingRepId == allReps[z]._id)
												{
													switch(allEvents[x].eventType)
													{
														 case 'In Office':
																row.completedInOfficePrograms++;
																break;

														 case 'Out of Office':
																row.completedOutOfOfficePrograms++;
																break;

														 case 'Webinar Only':
																row.completedWebinarOnlyPrograms++;
																break;

														 case 'Webinar with Catering':
																row.completedWebinarWithCateringPrograms++;
																break;
													}

													row.totalCompleted++;
												}
											 };

											 report.push(row);
										}
									}
								}

								//we're done
								 var finalTable = generateTable(report, "Data_Region_Summary.xls", self);
							});
						});
				});
	 }


	 function generateRemainingFundsReport (clientId, self)
	{
		 var result = {};
		 var report = [];

		 geddy.model.Client.first(clientId, function(err, client){
				if (err)
				{
					 throw err;
				}

				var searchEventsObject = {programId: {$in: client.programs}, status: 2};
				// var projection = new Object({_id: 0, eventType:1});
				var eventsCollection = geddy.globalDb.collection('events');

				eventsCollection.find(searchEventsObject).toArray(function(err, allCompletedEvents){
					if (err)
					{
						throw err;
					}

					var repsCollection = geddy.globalDb.collection('reps');
					var repsProjection = new Object({_id: 1, region:1});

					searchEventsObject = {id: {$in: client.reps}};
					repsCollection.find(searchEventsObject, repsProjection).toArray(function(err, allReps){
						if (err)
						{
							throw err;
						}

						var regions = [];
						for (var z = allReps.length - 1; z >= 0; z--) {
							var repRegion = allReps[z].region;
							if (repRegion)
							{
								if (regions.indexOf(repRegion) == -1)
								{
									regions.push(repRegion);

									var row = {};
									row.region = repRegion;

									//total up this region's completed programs
									row.completedPrograms = 0;
									row.fees = 0;
									row.honorarium = 0;
									row.travel = 0;
									row.meals = 0;
									row.expenses = 0;
									row.yearToDateTotalCost = 0;

									for (var x = allCompletedEvents.length - 1; x >= 0; x--) {
										if (allCompletedEvents[x].requestingRepId == allReps[z]._id)
										{
											row.completedPrograms++;
											row.fees += Number(allCompletedEvents[x].invoices[0].feeService);
											row.honorarium += Number(allCompletedEvents[x].invoices[0].feeService);
											row.travel += Number(allCompletedEvents[x].invoices[0].airAmount);
											row.meals += Number(allCompletedEvents[x].invoices[0].mealsAmount);
											row.expenses += Number(allCompletedEvents[x].invoices[0].expenseAmount);
										}
									};

									report.push(row);
								}
							}
						};

						 //we're done
						 var finalTable = generateTable(report, "Remaining_Funds_Report.xls", self);

					});
				});
			});
	}


	 function searchEvents(eventArray, id, addHonorarium)
	 {
			var returnObject = {};
			var counter = 0;
			var honorarium = 0;
			var outerIndex = 0;
			for (outerIndex = eventArray.length - 1; outerIndex >= 0; outerIndex--) {
				 var innnerIndex = 0;
				 for (innnerIndex = eventArray[outerIndex].potentialSpeakers.length - 1; innnerIndex >= 0; innnerIndex--) {
						var contactId = eventArray[outerIndex].potentialSpeakers[innnerIndex].contactId;
						var crgStatus = eventArray[outerIndex].potentialSpeakers[innnerIndex].crgStatus;

						if (id == contactId && crgStatus == '1')
						{
							 counter++;

							 if (addHonorarium)
							 {
									honorarium += Number(eventArray[outerIndex].invoices[0].feeService);
							 }

							 break;
						}
				 };
			};

			returnObject.counter = counter;
			returnObject.honorarium = honorarium;

			return returnObject;
	 }


	function generateHCPInfoReport (clientId, self)
	{
			var result = {};

			var contactCollection = geddy.globalDb.collection('contacts');

			var searchObject = new Object({'clients.id': clientId, contactType:'kol'});
			var projection = new Object({_id:0, firstName: 1, lastName: 1, degrees:1, otherDegree:1, affiliations:1, addresses:1, emailsAddresses: 1, medicalLicenses:1, npi:1, specialties:1 });

			var sort = {
				 lastName: 'asc',
				 firstName : 'asc'
			}

			contactCollection.find(searchObject, projection).sort(sort).toArray(function(err,contacts){
				 if (err)
				 {
						throw err;
				 }

				 var reportContacts = [];
				 var i;
				 for (i = 0; i < contacts.length; i++) {
						var contact = {};
						contact.lastName = contacts[i].lastName;
						contact.firstName = contacts[i].firstName;
						
						contact.streetAddress = '';
						contact.streetAddress2 = '';
						contact.city = '';
						contact.state = '';
						contact.postalCode = '';
						contact.country = '';

						var j;
						if (contacts[i].addresses)
						{
							 for (j = 0; j < contacts[i].addresses.length; j++) {
									if (contacts[i].addresses[j].preferred)
									{
										 contact.streetAddress = contacts[i].addresses[j].address1;
										 contact.streetAddress2 = contacts[i].addresses[j].address2;
										 contact.city = contacts[i].addresses[j].city;
										 contact.state = contacts[i].addresses[j].state;
										 contact.postalCode = contacts[i].addresses[j].postalCode;
										 if (contacts[i].addresses[j].country == null)
										 {
										 	contact.country = 'USA';
										 } else {
										 	contact.country = contacts[i].addresses[j].country;
										 }
										 break;
									}
							 };
						}

						contact.recepientType = '';

						contact.degrees = contacts[i].degrees;

						if (contacts[i].otherDegree)
						{
							contact.degrees += ',' + contacts[i].otherDegree;
						}

						contact.specialties = contacts[i].specialties;

						contact.npi = contacts[i].npi;

						contact.medicalLicenseNumber = '';
						contact.medicalLicenseState = '';

						if(contacts[i].medicalLicenses)
						{
							 for (j = 0; j < contacts[i].medicalLicenses.length; i++) {
										 contact.medicalLicenseNumber = contacts[i].medicalLicenses[j].number;
										 contact.medicalLicenseState = contacts[i].medicalLicenses[j].state;
										 break;
							 };
						}

						reportContacts.push(contact);
				 };

				 var finalTable = generateTable(reportContacts, "HCP_info.xls", self);
			});
	 }


	 function generateReconciliationReport (clientId, self)
	{
			var result = {};

			var report = [];

		 geddy.model.Client.first(clientId, function(err, client){
				if (err)
				{
					 throw err;
				}

				var searchEventsObject = {programId: {$in: client.programs}, status: 2};
				var eventsCollection = geddy.globalDb.collection('events');

				eventsCollection.find(searchEventsObject).toArray(function(err, allCompletedEvents){
					if (err)
					{
						throw err;
					}

					var i;
					var len = allCompletedEvents.length;
					var total;

					//for (var i = len - 1; i >= 0; i--) {
						for (i = 0; i < len; i++) {
						console.log('item #: ' + i)
							var eventData = allCompletedEvents[i];
							// console.log('event id: ' + eventData.id);

							total = 0;

							var row = {};
							row.rowNumber = i;
							row.po = '';
							row.jobNumber = eventData.jobNumber;
							row.confirmedDate = eventData.confirmedDate;
							row.city = eventData.city;
							row.state = eventData.state;
							row.speakerName = eventData.confirmedSpeaker;
							row.crgFee = eventData.crgPercentageFee;

							//init rows in case there's no invoice data
							row.feeForService = '';
							row.venueTotal = '';
							row.venueRoomRental = '';
							row.venueAv = '';
							row.catererTotal = '';
							row.speakerExpenses = '';
							row.speakerExpensesAir = '';
							row.speakerExpensesGround = '';
							row.speakerExpensesHotel = '';
							row.speakerExpensesHotelAndAirportParking = '';
							row.speakerExpensesMeals = '';
							row.speakerExpensesMileage = '';
							row.speakerExpensesMisc = '';
							row.speakerExpensesTolls = '';
							row.speakerExpensesAmount = '';
							row.crgPaid = '';
							row.crgPaidAir = '';
							row.crgPaidGround = '';
							row.crgPaidHotel = '';
							row.crgPaidTravelManagementFee = '';
							row.crgPaidAV = '';


							if (eventData.invoices && eventData.invoices.length > 0)
							{
								row.feeForService = eventData.invoices[0].feeService;
								if (row.feeForService)
									total += Number(row.feeForService);

								row.venueTotal = eventData.invoices[0].venueTotalAmount;
								if (row.venueTotal)
									total += Number(row.venueTotal);

								row.venueRoomRental = eventData.invoices[0].venueRentalAmount;
								if (row.venueRoomRental)
									total += Number(row.venueRoomRental);
								
								row.venueAv = eventData.invoices[0].venueAVTotalAmount;
								if (row.venueAv)
									total += Number(row.venueAv);

								row.catererTotal = eventData.invoices[0].catererTotalAmount;
								if (row.catererTotal)
									total += Number(row.catererTotal);
								
								row.speakerExpensesAir = eventData.invoices[0].airAmount;
								if (row.speakerExpensesAir)
									total += Number(row.speakerExpensesAir);
								
								row.speakerExpensesGround = eventData.invoices[0].groundAmount;
								if (row.speakerExpensesGround)
									total += Number(row.speakerExpensesGround);
								
								row.speakerExpensesHotel = eventData.invoices[0].hotelAmount;
								if (row.speakerExpensesHotel)
									total += Number(row.speakerExpensesHotel);
								
								row.speakerExpensesHotelAndAirportParking = eventData.invoices[0].parkingAmount;
								if (row.speakerExpensesHotelAndAirportParking)
									total += Number(row.speakerExpensesHotelAndAirportParking);
								
								row.speakerExpensesMeals = eventData.invoices[0].mealsAmount;
								if (row.speakerExpensesMeals)
									total += Number(row.speakerExpensesMeals);
								
								row.speakerExpensesMileage = eventData.invoices[0].mileageAmount;
								if (row.speakerExpensesMileage)
									total += Number(row.speakerExpensesMileage);
								
								row.speakerExpensesMisc = eventData.invoices[0].miscAmount;
								if (row.speakerExpensesMisc)
									total += Number(row.speakerExpensesMisc);
								
								row.speakerExpensesTolls = eventData.invoices[0].tollsAmount;
								if (row.speakerExpensesTolls)
									total += Number(row.speakerExpensesTolls);
								
								row.speakerExpensesAmount = eventData.invoices[0].expenseAmount;
								if (row.speakerExpensesAmount)
									total += Number(row.speakerExpensesAmount);
								
								row.crgPaidAir = eventData.invoices[0].crgAirAmount;
								if (row.crgPaidAir)
									total += Number(row.crgPaidAir);
								
								row.crgPaidGround = eventData.invoices[0].crgGroundAmount;
								if (row.crgPaidGround)
									total += Number(row.crgPaidGround);
								
								row.crgPaidHotel = eventData.invoices[0].crgHotelAmount;
								if (row.crgPaidHotel)
									total += Number(row.crgPaidHotel);
								
								row.crgPaidTravelManagementFee = eventData.invoices[0].travelManagementFee;
								if (row.crgPaidTravelManagementFee)
									total += Number(row.crgPaidTravelManagementFee);
								
								row.crgPaidAV = eventData.invoices[0].avAmount;
								if (row.crgPaidAV)
									total += Number(row.crgPaidAV);

								row.production = '';
								row.miscellaneous = '';
								row.totalReconciliation = total;
							} 

							report.push(row);
					};

					//we're done
					var finalTable = generateTable(report, "Reconciliation_report.xls", self);

				});
			});
	}
};

exports.Reports = Reports;

