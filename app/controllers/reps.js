var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var search = require('../helpers/search/search.js');
var Utils = require('../helpers/utils/utils.js');
var ModelName= "Rep";
var formidable = require('formidable');

var Reps = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

   this.before(function () { 
      var self = this;
      if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
      {
       geddy.globalDb.close();
        self.respond('false',{format: 'txt'});
      }
      else
      {
        self.session.set('activeController', self.name);
      }
    });

  this.index = function (req, resp, params) {
    var self = this;

    DBUtils.unlockObject(self);

    //var roleId = self.session.get('userRoleId');
    
    self.session.set('currentSection', constants.sections.CLIENT_REPS);

    var result={};
    result.searchRepProperties = constants.SEARCH_REP_PROPERTIES();

    var start = +new Date();  // log start timestamp

    var currentClient = self.session.get('currentClient');
    
    //pagination vars 
    var repCollection = geddy.globalDb.collection('reps');
    var pageLimit = geddy.config.pageLimit;
    //var searchObject = {};
    var searchObject = {id: {$in: currentClient.reps}};
    var currentPage = 1;
    var recordCount=0;
    var totalPages=0;


    var sort = {
              lastName: 'asc',
              firstName : 'asc'
      }  

    result.currentPage = currentPage;

    repCollection.find(searchObject).toArray(function(err,allReps){
        if (allReps != null)
        {
          recordCount = allReps.length;
          totalPages = Math.ceil(recordCount/pageLimit);
        }

        repCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,reps){

          result.totalPages = totalPages;
          result.currentClient = currentClient;
          result.reps = reps;

          var end =  +new Date();  // log end timestamp
          var diff = end - start;
          //console.log('Processed info info duration: start: '+start +' end: '+ end +" diff: "+ diff);
          self.respond(result,{type:'Rep'});
        });
    });
  };


  this.show = function (req, resp, params) {
    var self = this;

    var continueProcess = true;

    if (params.id == '1234')
    {
      if (self.session.get('currentRep'))
      {
        params.id = self.session.get('currentRep').id;
      } else {
        continueProcess = false;
        self.respond('null',{format: 'txt'});
      }
    }

    if (continueProcess)
    {
      var roleId = self.session.get('userRoleId');

      DBUtils.unlockObject(self);
      
      geddy.model.Rep.all({role: {ne: null}},function (err, reps) {
        if (err)
        {
          throw err;
        }

        var resultObject = new Object();
        resultObject ['allReps'] = reps;

        resultObject ['displayEdit'] = true;

        DBUtils.showObject(self,ModelName,params,constants.sections.CLIENT_REPS, resultObject);
      });
    }
  };


  this.clearCurrentRep = function (req, resp, params) {
    var self = this;
    DBUtils.clearCurrentObject(self,ModelName,params);
   };



  this.save = function (req, resp, params) {
    var self = this;

    DBUtils.saveObject(self,ModelName,params);

    var currentClient = self.session.get("currentClient");
    var currentRep = self.session.get("currentRep");

    DBUtils.saveDataTo(self, 'Client', currentClient.id, 'reps', currentRep.id);

    //if setting status, we need to update user account also
    if (params.status && params.status != '')
    {
      geddy.model.User.first({email: currentRep.defaultEmail}, function (error, user) {
          if (!error)
          {
            if (user.state != null && user.state != constants.userState.PENDING_APPROVAL)
            {
              if (params.status == 'Deactive')
                user.state = constants.userState.Disabled;
              else 
                  user.state = constants.userState.Active;
            }

            user.save();
          }
      });
    }
  }


  this.saveEmbeddedModel = function (req, resp, params) {
    var self = this;

    DBUtils.saveObjectEmbeddedModel(self,ModelName,params);

    var currentClient = self.session.get("currentClient");
    var currentRep = self.session.get("currentRep");

    DBUtils.saveDataTo(self, 'Client', currentClient.id, 'reps', currentRep.id);
  }

  this.saveDefaultItem = function (req, resp, params){
    var self = this;
    var currentRep = self.session.get("currentRep");

    currentRep[params.targetProperty] = params.value;

    currentRep.save(function(err, data){
      self.respond(true, {
        format: 'json'
      });
    });
  }

  this.deleteEmbeddedModel = function (req, resp, params) {
    var self = this;
      DBUtils.deleteEmbeddedModel(self,ModelName,params);
  };


  this.createUserAccount = function (req, resp, params){
    var self = this;
    var currentRep = self.session.get("currentRep");

    //check to make sure rep has all the required elementes
    // if (params.targetProperty == 'defaultEmail' && currentRep.level)
    if (currentRep.firstName && currentRep.firstName.length > 0 && currentRep.lastName && currentRep.lastName.length > 0 && currentRep.defaultEmail && currentRep.defaultEmail.length > 0 &&
      currentRep.level) {
        //send rep data to be saved as a user... if needed
        //but first, let's check the level for the user so we can set their role correctly

        var role;

        switch(currentRep.level)
        {
          case 1:
          case 2:
            role = constants.roles.MANAGER.label;
            break;

          case 3: 
          case 4:
          case 5:
            role = constants.roles.REP.label;
            break;
        }

        DBUtils.saveAsUser(self, currentRep.firstName, currentRep.lastName, currentRep.defaultEmail, role, currentRep.id);
      }

      self.respond(true, {
          format: 'json'
        });
  }


  this.deleteReps = function (req, resp, params) {
    var self = this;
    DBUtils.deleteObject(self,ModelName,params);
  };


  this.deleteRepFromClient = function (req, resp, params) {
    var self = this;
    DBUtils.deleteFromArrayByValue(self, params.ids, self.session.get('currentClient'), 'reps');
  }


  // rep Step pages
  this.repStep1 = function (req, resp, params) {
    var self = this;
    var suffixes = constants.SUFFIXES();
    var countries = constants.COUNTRIES();
    var states = constants.STATES();
    var selectPrompt = constants.SELECT_PROMPT();
    var addressTypes = constants.ADDRESS_TYPES();
    var phoneTypes = constants.PHONE_NUMBER_TYPES();

    var currentRep = self.session.get("currentRep");

    var currentClient = self.session.get('currentClient');

    if (currentRep && currentRep.id)
    {
      DBUtils.lockObject(self, currentRep.id);
    }

    //let's put together the info for roles and levels
    var roles = [];
    var level1 = constants.SALES_FORCE_LABELS_LEVEL1();
    var level2 = constants.SALES_FORCE_LABELS_LEVEL2();
    var level3 = constants.SALES_FORCE_LABELS_LEVEL3();
    var level4 = constants.MEDICAL_AFFAIRS_LABELS();
    var level5 = constants.PAYOR_LABELS();

    var i;

    if (currentClient.salesForceLabelsLevel1)
    {
      for (i = 0; i < level1.length; i++) {
        if (currentClient.salesForceLabelsLevel1.indexOf(level1[i].label) != -1)
        {
          roles.push(level1[i]);
        }
      };
    }

    if (currentClient.salesForceLabelsLevel2)
    {
      for (i = 0; i < level2.length; i++) {
        if (currentClient.salesForceLabelsLevel2.indexOf(level2[i].label) != -1)
        {
          roles.push(level2[i]);
        }
      };
    }

    if (currentClient.salesForceLabelsLevel3)
    {
      for (i = 0; i < level3.length; i++) {
        if (currentClient.salesForceLabelsLevel3.indexOf(level3[i].label) != -1)
        {
          roles.push(level3[i]);
        }
      };
    }

    if (currentClient.medicalAffairs)
    {
      for (i = 0; i < level4.length; i++) {
        if (currentClient.medicalAffairs.indexOf(level4[i].label) != -1)
        {
          roles.push(level4[i]);
        }
      };
    }

    if (currentClient.payors)
    {
      for (i = 0; i < level5.length; i++) {
        if (currentClient.payors.indexOf(level5[i].label) != -1)
        {
          roles.push(level5[i]);
        }
      };
    }


    //get all the reps for current client... we'll need them for when a role is changed and we display next level up (for manager info)
    var allReps = [];
    geddy.model.Rep.all({role: {ne: null}, id: {$in: currentClient.reps}},{sort: {lastName: 'asc'}},function (err, reps) {
      if (err)
      {
        throw err;
      }

      allReps = reps;

      self.respond( {
          rep: currentRep,
          roles: roles,
          allReps: allReps,
          suffixes:suffixes,
          selectPrompt:selectPrompt,
          currentClient:self.session.get('currentClient'),
          countries:countries,
          states:states,
          addressTypes:addressTypes,
          phoneTypes:phoneTypes,
          stepTitle:createTitle(currentRep)},
          {format: 'html',template: 'app/views/reps/repStep1'});
    });
  }

   // rep Step pages
  this.repStep2 = function (req, resp, params) {
    //console.log('?>>>>>>>>>>>>>>REPO STEP2 ');
    self = this;
    //var countries = constants.COUNTRIES();
    var selectPrompt = constants.SELECT_PROMPT();
    var currentRep = self.session.get("currentRep");

    self.respond( {
      rep: currentRep,
      selectPrompt:selectPrompt,
      currentClient:self.session.get('currentClient'),
      stepTitle:createTitle(currentRep)},
      {format: 'html',template: 'app/views/reps/repStep2'});
  };

  this.getRepsByLevel = function (req, resp, params) {
    var self = this;
    var currentClient = self.session.get('currentClient');

    geddy.model.Rep.all({
      level: {lt: Number(params.level)},
      id: {$in: currentClient.reps}
     }, function (err, reps){
      if (err)
      {
        throw err;
      }

      self.respond( {
         reps: reps},
         {format: 'json'}
      );
    });

  }

/*  this.getPage = function (req, resp, params) {
    var $thisRequest = this;
    var search = require('../helpers/search/search.js');
    search.AdvanceSearch($thisRequest, null, null, 'reps', params);
  }
*/
  this.search = function (req, resp, params) {
    var $thisRequest = this;
    //var search = require('../helpers/search/search.js');
    var currentClient = $thisRequest.session.get('currentClient');
    var searchObject = {id: {$in: currentClient.reps}};

    var sort = {
              lastName: 'asc',
              firstName : 'asc'
    }

    search.AdvanceSearch($thisRequest, searchObject, sort, 'reps', params);
  }

  function createTitle (currentRep) {

     var headerTitle = "";

     if(currentRep) {
        if((currentRep.firstName) || (currentRep.lastName)){
           headerTitle = currentRep.firstName + ' ' + currentRep.lastName + ' - ';
        }
     } else {   
        headerTitle = 'New Field Force - '
     }

     return headerTitle;
  }






  this.showImport = function (req, resp, params) {
    var self = this;
    
    self.session.set('importResult', { diff: [], newReps: [], total: [] });
    
    self.respond(true,
        {format: 'html',
      template: 'app/views/reps/import'}
      );
  };  


  this.import = function (req, resp, params) {
    var self = this;

    var importResult = self.session.get('importResult');

    var form = new formidable.IncomingForm();
    form.uploadDir = geddy.config.UploadTempFile;
    var self = this;
    //console.log(" SALIDA ->" + "FILE");

    form.on('end', function () {
        //console.log('FILE DONE');
    });

    form.parse(req, function (err, fields, files) {
        //console.log(" SALIDA ->" + JSON.stringify(files["ExcelFile"]));
        var path = files.ExcelFile.path;
        //console.log(" Filename ->" + path);

        var XLSX = require('xlsx')
        var xlsx = XLSX.readFile(path);
        var sheet_name_list = xlsx.SheetNames;

        var sheet = 0;

        xlsx.SheetNames.forEach(function (y) {
            sheet++;
            var array1 = XLSX.utils.sheet_to_row_object_array(xlsx.Sheets[y]);

            array1.forEach(function (data) {
                importResult.total.push(data);
            });
        });

        //remove the last 17 items... this is just data for the template
        // importResult.total = importResult.total.slice(0, importResult.total.length - 18);

        //console.log("New rep db->" + JSON.stringify(importResult.total));
         var currentClient = self.session.get('currentClient');
         ProcessImport(importResult.total.shift(), self,currentClient);

    });

  };


  // Final task (same in all the examples)
  var FinalImport = function (self, currentClient) {
      console.log('Done');
      var importResult = self.session.get('importResult');

      for(i=0;i<importResult.newReps.length;i++){
        var idRep = importResult.newReps[i].id;

        if (!currentClient.reps)
        {
          currentClient.reps = [];
        }

        if (importResult.newReps[i].firstName && importResult.newReps[i].firstName != '')
        {
          if (currentClient.reps.indexOf(idRep) == -1)
            currentClient.reps.push(idRep);

          geddy.model.Rep.first(idRep, function (err, rep) {
              if (err) {
                  //throw err;
                  console.log(err);
              }else if(rep){
                var managerTerritoryNumber = rep.managerTerritoryNumber;
                // territory = Math.floor(territory / 100) * 100;
                //console.log(">>>>>>>>>>>>>> rep -> " + rep.id);
               // console.log(">>>>>>>>>>>>>> territory -> " + territory);
                if (managerTerritoryNumber != "")
                {
                   geddy.model.Rep.first({territory:managerTerritoryNumber}, function (err, manager) {
                       if (err) {
                           //throw err;
                           console.log(err);
                       }else if (manager){
                        //console.log(">>>>>>>>>>>>>> manager -> " + manager.id);
                         if (rep.id != manager.id)
                         {
                           rep.managerId = manager.id; 
                         }
                         rep.save(function (err, data2){
                            if (err)
                            {
                              console.log(err);
                            }
                          });
                       }
                   });
                }
              }
          });
        }

        currentClient.save(function (err, data1){
            if (err)
            {
              console.log(err);
            }
          });
      }
      
      self.respond(importResult, { format: 'json' });
  }

  var ProcessImport = function (currentRep, self,currentClient) {

      if (currentRep) {
        if (!currentRep["First Name"])
          return FinalImport(self, currentClient);
        

        var importResult = self.session.get('importResult');

          geddy.model.Rep.all({ firstName: currentRep["First Name"], lastName: currentRep["Last Name"] },
               function (err, rep) {
                   var newRep;

                   if (err) {
                       console.log(err);
                   }
                   else if (rep.length > 0) {
                      newRep = rep[0];

                       // var diff = { a: currentRep, b: [] };
                       // for (i = 0; i < rep.length; i++) {
                       //     console.log('Found Rep');
                       //     console.log(rep[i].firstName);
                       //     diff.b.push(rep[i]);
                       // }
                       // importResult.diff.push(diff);
                   } else  if (currentRep["First Name"] != undefined && currentRep["Last Name"] != undefined && currentRep["First Name"] != '' && currentRep["Last Name"] != ''){
                       newRep = geddy.model.Rep.create();
                   }

                    if (currentRep["First Name"] && currentRep["First Name"] != '')
                      newRep.firstName = currentRep["First Name"].trim();
                    
                    if (currentRep["Last Name"] && currentRep["Last Name"] != '')
                      newRep.lastName = currentRep["Last Name"].trim();
                    
                    if (currentRep["Middle Name"] && currentRep["Middle Name"] != '')
                      newRep.middleName = currentRep["Middle Name"].trim();
                    
                    if (currentRep["Suffix"] && currentRep["Suffix"] != '')
                      newRep.suffix = currentRep["Suffix"];

                    if ( currentRep["Team"] && currentRep["Team"] != '')
                      newRep.team = currentRep["Team"];

                    if (currentRep["Title"] && currentRep["Title"] != '')
                      newRep.title = currentRep["Title"];
                    
                    if (currentRep["Zone"] && currentRep["Zone"] != '')
                      newRep.zone = currentRep["Zone"]
                    
                    if (currentRep["Region"] && currentRep["Region"] != '')
                      newRep.region = currentRep["Region"].trim();
                    
                    if (currentRep["Territory Name"] && currentRep["Territory Name"] != '')
                      newRep.territoryName = currentRep["Territory Name"].trim();

                    if (currentRep["Territory Number"] && currentRep["Territory Number"] != '')
                      newRep.territory = currentRep["Territory Number"];
                    
                    if (currentRep["Manager Territory Number"] && currentRep["Manager Territory Number"] != '')
                      newRep.managerTerritoryNumber = currentRep['Manager Territory Number'];
                    
                    if (currentRep["Territory Zip Codes"] && currentRep["Territory Zip Codes"] != '')
                      newRep.territoryZips = currentRep["Territory Zip Codes"];
                    
                    if (currentRep["Territory Zip Codes 2"] && currentRep["Territory Zip Codes 2"] != '')
                      newRep.territoryZips += currentRep["Territory Zip Codes 2"];
                    
                    if (currentRep["Status"] && currentRep["Status"] != '')
                      newRep.status = currentRep["Status"];

                    if (currentRep["Employee ID"] && currentRep["Employee ID"] != '')
                      newRep.clientRepId = currentRep["Employee ID"];
                    
                    if (currentRep["Role"] && currentRep["Role"] != '')
                      newRep.role = currentRep["Role"];

                    if (currentRep["Hire Date"] && currentRep["Hire Date"] != '')
                      newRep.hireDate = currentRep["Hire Date"];
                    
                    if (currentRep["Email"] && currentRep["Email"] != '')
                    {
                      newRep.defaultEmail = currentRep["Email"];

                      var eMailAddress;
                      newRep.emailAddresses = new Array();
                      eMailAddress = geddy.model.ContactEmailAddress.create();
                      eMailAddress.email = newRep.defaultEmail;
                      eMailAddress.preferred = 'true';

                      newRep.emailAddresses.push(eMailAddress);
                    }
                    
                    if (currentRep["ME Number"] && currentRep["ME Number"] != '')
                      newRep.meNumber = currentRep["ME Number"];

                    /*Address*/
                    var newAddress;

                    if (!newRep.addresses || newRep.addresses.length == 0)
                    {
                      newRep.addresses = new Array();
                      newAddress = geddy.model.Address.create();
                    }
                    else 
                      newAddress = newRep.addresses[0];

                    if (currentRep["Address Type"] && currentRep["Address Type"] != '')
                    {
                      newAddress.addressType = currentRep["Address Type"];
                      newAddress.defaultAddress = currentRep["Address Type"];
                      newAddress.preferred = 'true';
                    }
                    
                    if (currentRep["Address 1"] && currentRep["Address 1"] != '')
                      newAddress.address1 = currentRep["Address 1"];
                    
                    if (currentRep["Address 2"] && currentRep["Address 2"] != '')
                      newAddress.address2 = currentRep["Address 2"];
                    
                    if (currentRep["City"] && currentRep["City"] != '')
                      newAddress.city = currentRep["City"];

                    if (currentRep["State/Province"] && currentRep["State/Province"] != '')
                      newAddress.state = currentRep["State/Province"];
                    
                    if (currentRep["Postal Code"] && currentRep["Postal Code"] != '')
                      newAddress.postalCode = currentRep["Postal Code"];
                    
                    if (currentRep["Country"] && currentRep["Country"] != '')
                      newAddress.country = currentRep["Country"];
                    
                    if (newRep.addresses.length == 0)
                      newRep.addresses.push(newAddress);

                    /*Phones*/
                    var newPhone1;
                    var newPhone2;
                    
                    if (!newRep.phoneNumbers || newRep.phoneNumbers.length == 0)
                    {
                        newRep.phoneNumbers = new Array();
                        newPhone1 = geddy.model.ContactPhoneNumber.create();
                        newPhone2 = geddy.model.ContactPhoneNumber.create();
                    } else {
                        newPhone1 = newRep.phoneNumbers[0];
                        newPhone2 = newRep.phoneNumbers[1];
                    }
                    
                    if (currentRep["Phone 1"] && currentRep["Phone 1"] != '') {
                      if (currentRep["Phone 1 Type"] && currentRep["Phone 1 Type"])
                        newPhone1.phoneType = currentRep["Phone 1 Type"];

                      if (currentRep["Phone 1"] && currentRep["Phone 1"])
                        newPhone1.number = Utils.formatPhoneNumber(currentRep["Phone 1"]);

                        newPhone1.preferred = 'true';

                        newRep.defaultPhoneNumber = newPhone1.number;
                    }

                    if (currentRep["Phone 2"] && currentRep["Phone 2"] != '') {
                      if (currentRep["Phone 2 Type"] && currentRep["Phone 2 Type"])
                        newPhone2.phoneType = currentRep["Phone 2 Type"];

                      if (currentRep["Phone 2"] && currentRep["Phone 2"])
                        newPhone2.number = Utils.formatPhoneNumber(currentRep["Phone 2"]);

                        newPhone2.preferred = 'false';
                    }

                    if (newRep.phoneNumbers.length == 0)
                    {
                     newRep.phoneNumbers.push(newPhone1);
                     newRep.phoneNumbers.push(newPhone2);
                    }


                    importResult.newReps.push(newRep);
                    newRep.level = DBUtils.GetRepLevelFromRole(newRep.role, currentClient);

                    //console.log("New rep Level->" + newRep.level);
                    //console.log("New rep db->" + JSON.stringify(newRep));

                    newRep.save(function (err, rep) {
                        if (err) {
                            console.log(JSON.stringify(err));
                            throw err;
                        }else{
                           //Create USer
                           var role;
                           switch(rep.level)
                           {
                             case 1:
                             case 2:
                               role = constants.roles.MANAGER.label;
                               break;

                             case 3: 
                             case 4:
                             case 5:
                               role = constants.roles.REP.label;
                               break;
                           }
                           DBUtils.saveAsUser(null, rep.firstName, rep.lastName, rep.defaultEmail, role, rep.id);
                        }
                    });
                       
                   return ProcessImport(importResult.total.shift(), self, currentClient);
               });

      }
      else {
          return FinalImport(self, currentClient);
      }
  };

  this.saveDiff = function (req, resp, params) {
      var $thisRequest = this;
      //console.log('Done');

      geddy.model.Rep.first(params.id, function (err, rep) {
          if (err) {
              //throw err;
              console.log(err);
          }
          if (!rep) {
              throw new geddy.errors.NotFoundError();
          }
          else {

              rep.suffix = params.Suffix;
              rep.team =  params.team;
              rep.title = params.title;
              rep.zone = params.zone;
              rep.region = params.region;
              rep.region = params.region;
              rep.territory = params.territory;
              rep.territoryName = params.territoryName;
              rep.territoryZips = params.territoryZips;
              rep.status = params.status;
              rep.clientRepId = params.clientRepId;
              rep.hireDate = params.hireDate;
              rep.role = params.role;
              rep.meNumber = params.meNumber;


              if (rep.emailAddresses && rep.emailAddresses[0]) {
                  rep.emailAddresses[0].email = params.Email;
                  rep.defaultEmail = rep.emailAddresses[0].email;
              }

              if (rep.phoneNumbers) {
                  for (var i = 0; i < rep.phoneNumbers.length; i++) {
                      rep.phoneNumbers[i].number = params.PhoneNumber[i];
                  }
              }

              if (rep.addresses) {
                  for (var i = 0; i < rep.addresses.length; i++) {
                      rep.addresses[i].addressType = params.AddressType[i];
                      rep.addresses[i].address1 = params.Address1[i];
                      rep.addresses[i].address2 = params.Address2[i];
                      rep.addresses[i].city = params.City[i];
                      rep.addresses[i].state = params.State[i];
                      rep.addresses[i].postalCode = params.Zip[i];
                      rep.addresses[i].country = params.Country[i];
                  }
              }

              rep.save(function (err, data) {
                  if (err) {
                      console.log(JSON.stringify(err));
                      throw err;
                  } else {
                      $thisRequest.respond(params, { format: 'json' });
                  }
              });

          }
      });
  };
};

exports.Reps = Reps;
