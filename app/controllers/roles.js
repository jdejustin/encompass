//var utils = require('../helpers/utils/utils');
var constants = require('../helpers/utils/constants.js');


var Roles = function () {
	this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

	this.index = function (req, resp, params) {
		var self = this;

		geddy.model.Role.all(function(err, roles) {
			if (err) {
				throw err;
			}
			self.respondWith(roles, {type:'Role'});
		});
	};

	this.add = function (req, resp, params) {
		var self = this;
		var aclNames = constants.ACL_NAME_LIST(); 
		var aclActions = constants.ACL_ACTION_LIST(); 
		self.respond({params: params, aclNames:aclNames, aclActions:aclActions });
	};

	this.create = function (req, resp, params) {
		var self = this;
		
		var aclNames = constants.ACL_NAME_LIST(); 
		var tempAcl = [];
		
		for (var i=0;i<aclNames.length;i++)
		{
			if(params.acl[aclNames[i].aclName])
			{
				var tempAclObject = new Object();
				tempAclObject['aclName'] = aclNames[i].aclName;
				tempAclObject['actions'] = params.acl[aclNames[i].aclName];
				tempAcl.push(tempAclObject);
			}
		}
		
		params.acl = tempAcl;
			
		var role = geddy.model.Role.create(params);

		if (!role.isValid()) {
			this.respondWith(role);
		}
		else {
			role.save(function(err, data) {
				if (err) {
					throw err;
				}
				self.respondWith(role, {status: err});
			});
		}
	};
	
	

	this.show = function (req, resp, params) {
		var self = this;
		geddy.model.Role.first(params.id, function(err, role) {
			if (err) {
				throw err;
			}
			if (!role) {
				throw new geddy.errors.NotFoundError();
			}
			else {
				role.getUsers(function (err, data) {
					//self.respondWith(role);
					//console.log('USERS BY ROLES'+geddy.array.humanize(data));
					self.respond({role:role,users:data});
		        });
			}
		});
	};

	this.edit = function (req, resp, params) {
		var self = this;

		var aclNames = constants.ACL_NAME_LIST(); 
		var aclActions = constants.ACL_ACTION_LIST();
		
		geddy.model.Role.first(params.id, function(err, role) {
			if (err) {
				throw err;
			}
			if (!role) {
				throw new geddy.errors.BadRequestError();
			}
			else {
			self.respond({params:params,role:role,aclNames:aclNames,aclActions:aclActions});
			}
		});
	};

	this.update = function (req, resp, params) {
		var self = this;

		var aclNames = constants.ACL_NAME_LIST(); 
		var tempAcl = [];
		
		for (var i=0;i<aclNames.length;i++)
		{
			if(params.acl[aclNames[i].aclName])
			{
				var tempAclObject = new Object();
				tempAclObject['aclName'] = aclNames[i].aclName;
				tempAclObject['actions'] = params.acl[aclNames[i].aclName];
				tempAcl.push(tempAclObject);
			}
		}
		params.acl = tempAcl;
		
		geddy.model.Role.first(params.id, function(err, role) {
			if (err) {
				throw err;
			}
			role.updateProperties(params);

			if (!role.isValid()) {
				self.respondWith(role);
			}
			else {
				role.save(function(err, data) {
					if (err) {
						throw err;
					}
					self.respondWith(role, {status: err});
				});
			}
		});
	};

	this.remove = function (req, resp, params) {
		var self = this;

		geddy.model.Role.first(params.id, function(err, role) {
			if (err) {
				throw err;
			}
			if (!role) {
				throw new geddy.errors.BadRequestError();
			}
			else {
				geddy.model.Role.remove(params.id, function(err) {
					if (err) {
						throw err;
					}
					self.respondWith(role);
				});
			}
		});
	};

};

exports.Roles = Roles;
