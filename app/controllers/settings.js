var DBUtils = require('../helpers/utils/DBUtils.js');
var ModelName= "Setting";

var Settings = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

  this.before(function () { 
    var self = this;
    if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
    {
      geddy.globalDb.close();

      self.respond('false', {format: 'txt'});
    }
    else
    {
      self.session.set('activeController', self.name);
    }
  });


  this.index = function (req, resp, params) {
    var self = this;

    var result= {};

    geddy.model.Setting.first(function(err, settings) {
      if (err) {
        throw err;
      }

      if (settings)
      {
         self.session.set("currentSettings", settings);
      } else {
         self.session.set("currentSettings", null);
      }

      result.settings = settings;
      self.respond(result, {type:'json'});
    });
  };

  this.save = function (req, resp, params) {
    var self = this;

    var settings = self.session.get("currentSettings");

    settings.updateProperties(params);

    settings.save(function(err, data) {
      self.respond(true, {type:'json'});
    });

  };
};

exports.Settings = Settings;
