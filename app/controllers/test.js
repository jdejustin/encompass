/*
 * Geddy JavaScript Web development framework
 * Copyright 2112 Matthew Eernisse (mde@fleegix.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
var strategies = require('../helpers/passport/strategies')
  , authTypes = geddy.mixin(strategies, {local: {name: 'local account'}});;

var constants = require('../helpers/utils/constants.js');


  var formidable = require('formidable');
  var fs = require('fs');



  var Test = function () {

  this.BuildCommunicationsObject = function(response,eventId,callback){
      var MJ = require("mongo-fast-join");
      var MongoClient = require('mongodb').MongoClient;
      var mongoJoin = new MJ();
      var connectionString = 'mongodb://'+geddy.config.db.mongo.host+':'+geddy.config.db.mongo.port+'/'+geddy.config.db.mongo.dbname;


    if (typeof(geddy.config.db.mongo.serverOptions)){
      if(geddy.config.db.mongo.serverOptions.ssl){
        connectionString += "?ssl=true";
      }
    }


    MongoClient.connect(connectionString, function(err, db) {
      if(err) throw err;

    var EventsCollection = db.collection('events');
    var ProgramCollection = db.collection('programs');

    mongoJoin.query(EventsCollection,
          {id:eventId}, //query statement
          {}, //fields
          {
              //limit: 10000//options
          }
      ).join({
          joinCollection: ProgramCollection,
          //respects the dot notation, multiple keys can be specified in this array
          leftKeys: ["programId"],
          //This is the key of the document in the right hand document
          rightKeys: ["id"],
          //This is the new subdocument that will be added to the result document
          newKey: "program"
      }).exec(function (err, items) {    //Call exec to run the compiled query and catch any errors and results, in the callback
        callback(response,items);
      });
    });
  },

      this.AddAttendiesInfo = function(response,communicationObject,arrayAttendies,result){
        console.log(">>>>arrayAttendies:"+JSON.stringify(arrayAttendies));
        console.log(">>>>arrayAttendies.length:"+JSON.stringify(arrayAttendies.length));

        if (arrayAttendies.length == 0){
          console.log(">>>>result:"+JSON.stringify(result));
          communicationObject.attendeesInfo = result;
          response.AddRepRequestingInfo(response,communicationObject);
        }else{
          var currentAttendee = arrayAttendies.shift();
          var MJ = require("mongo-fast-join");
          var MongoClient = require('mongodb').MongoClient;
          var mongoJoin = new MJ();
          var connectionString = 'mongodb://'+geddy.config.db.mongo.host+':'+geddy.config.db.mongo.port+'/'+geddy.config.db.mongo.dbname;

          if (typeof(geddy.config.db.mongo.serverOptions)){
            if(geddy.config.db.mongo.serverOptions.ssl){
              connectionString += "?ssl=true";
            }
          }
          //Get attendeeInfo with contactInfo at the same time
          MongoClient.connect(connectionString, function(err, db) {
            if(err) throw err;
          var AttendeeCollection = db.collection('attendees');
          var ContactCollection = db.collection('contacts');
          mongoJoin.query(AttendeeCollection,
                {id:currentAttendee}, //query statement
                {}, //fields
                {
                    //limit: 10000//options
                }
            ).join({
                joinCollection: ContactCollection,
                //respects the dot notation, multiple keys can be specified in this array
                leftKeys: ["contactId"],
                //This is the key of the document in the right hand document
                rightKeys: ["id"],
                //This is the new subdocument that will be added to the result document
                newKey: "contactInfo"
            }).exec(function (err, items) {    //Call exec to run the compiled query and catch any errors and results, in the callback
                if (items) {
                  result.push(items);
                }            
                db.close();
                return response.AddAttendiesInfo(response,communicationObject,arrayAttendies,result);
            });
          });
        /*
          geddy.model.Attendee.first({ id: currentAttendee }, function (err, data) {
              if (data) {
                result.push(data);
                return response.AddAttendiesInfo(response,communicationObject,arrayAttendies,result);
              }
            });
        */
        }
      }

      this.AddRepRequestingInfo = function(response,communicationObject){
          geddy.model.Rep.first({ id: communicationObject.requestingRepId }, function (err, data) {
              if (data) {
                communicationObject.RepRequestingInfo = data;
              }
              response.AddRSMInfo(response,communicationObject);
            });
      }

      this.AddRSMInfo = function(response,communicationObject){
          geddy.model.Rep.first({ id: communicationObject.RepRequestingInfo.managerId }, function (err, data) {
              if (data) {
                communicationObject.RSMInfo = data;
              }
              response.AddVenueInfo(response,communicationObject);
            });
      }

      this.AddVenueInfo = function(response,communicationObject){
          geddy.model.Venue.first({ id: communicationObject.venueId }, function (err, data) {
              if (data) {
                communicationObject.VenueInfo = data;
              }
              response.AddCatererInfo(response,communicationObject);
            });
      }

      this.AddCatererInfo = function(response,communicationObject){
          geddy.model.Venue.first({ id: communicationObject.catererId }, function (err, data) {
              if (data) {
                communicationObject.CatererInfo = data;
              }
              response.AddSpeakersInfo(response,communicationObject,communicationObject.potentialSpeakers,[]);
            });
      }

      this.AddSpeakersInfo = function(response,communicationObject,arraySpeakers,result){
        if ((!arraySpeakers) || arraySpeakers.length == 0){
          console.log(">>>>result:"+JSON.stringify(result));
          communicationObject.SpeakersInfo = result;
          communicationObject.ConfirmedSpeakerInfo = [];
          for(var i=0; result.length > i ;i++ ){
            if (result[i].status == 1){
              communicationObject.ConfirmedSpeakerInfo.push(result[i]);
            }        
          }
          communicationObject.ConfirmedSpeakerInfo
          response.AddCRGManagerInfo(response,communicationObject);
          //response.AddRepRequestingInfo(response,communicationObject);
        }else{
          console.log(">>>>arraySpeakers:"+JSON.stringify(arraySpeakers));
          console.log(">>>>arraySpeakers.length:"+JSON.stringify(arraySpeakers.length));
          var currentSpeaker = arraySpeakers.shift();
          console.log(">>>>>>currentSpeaker"+JSON.stringify(currentSpeaker));
          geddy.model.Contact.first({ id: currentSpeaker.contactId }, function (err, data) {
              var DBUtils = require('../helpers/utils/DBUtils.js');   
              var newSpeakerInfo = DBUtils.cloneObject(currentSpeaker);
              if (data) {
                newSpeakerInfo.contactInfo = data
              }
              result.push(newSpeakerInfo);
              return response.AddSpeakersInfo(response,communicationObject,arraySpeakers,result);
            });
        }
      }


      this.AddCRGManagerInfo = function(response,communicationObject){
          geddy.model.User.first({ id: communicationObject.program.crgProgramManager }, function (err, data) {
              if (data) {
                communicationObject.CRGManagerInfo = data;
              }
              response.respond(communicationObject, { format: 'json' });
            });
      }



      this.responseCommunicationEvents = function(response,events){
          var DBUtils = require('../helpers/utils/DBUtils.js');   
          var result = DBUtils.cloneObject(events);
          if (result.length > 0){
            result = result[0];
          }
          if(result.attendees != null){
            response.AddAttendiesInfo(response,result,result.attendees,[]);
          }
      }

      this.communication = function (req, resp, params) {
          var $thisRequest = this;
          var result = {};
          this.BuildCommunicationsObject($thisRequest,"71B492D8-C904-4590-AEE2-E0FAB85E841E",this.responseCommunicationEvents);
          //$thisRequest.respond({ result: result }, { format: 'json' });
      },






      this.email = function (req, resp, params) {
          var $thisRequest = this;
          var notificator = require('../helpers/notificator/notificator.js');

          var notification = {
              type: "UserConfirmation",
              to: ['jvillmo@gmail.com'],
              subject: "User confirmation",
              name: "Jorge Villalobos",
              country: "Costa Rica",
              filename: "body.html"
          };

          //var result = notificator.sendNotification(notification);

//          var result = notificator.sendCommunication(notification);






          $thisRequest.respond({ result: result }, { format: 'json' });
      },

      this.responseEvents = function(response,events){
          response.respond({dataset:events}, { format: 'json' });
      }


      this.reportTable = function(req, resp, param){
       self = this;
       var result = {};
      self.respond( result,{format: 'html',template: 'app/views/test/report'});

      }

this.changeStatus = function(req, resp, param){
   self = this;
   var result = {};
   var DBUtils = require('../helpers/utils/DBUtils.js');   
   DBUtils.changeStatus(self,"Event","19513EF6-766B-48D9-8FE9-58E42C4BAE27",2,"this is my comment","B4B4A26B-FA9E-4394-A668-E507B9C5E404",true);
}





      this.viewSnapShot =  function(req, resp, param){
        self = this;
//        var objectSerialized = "3gAuqWNyZWF0ZWRBdLgyMDE0LTA1LTIzVDIwOjAzOjA2Ljg1MlqpdXBkYXRlZEF0uDIwMTQtMDUtMjVUMTQ6NTM6MzUuNjQ1WqVicmFuZKhramRrc2FqZq5zdWJtaXRpbmdUaW1lcwCpcHJvZ3JhbUlk2gAkNTAxODYzRkItRUMxNS00QUFBLUE1MzMtRDE4NUM1OTkwMjg4sXByZXNlbnRhdGlvblRpdGxlpnRpdGxlIKlldmVudFR5cGWpSW4gT2ZmaWNlqWpvYk51bWJlcsCmc3RhdHVzArBjcmdQZXJjZW50YWdlRmVlwK9yZXF1ZXN0aW5nUmVwSWTaACRBQzQyMUMyOC1FQ0JCLTQ4MzktODlGNy1GNEI0MzUzQzVBQTKoc2VydmljZXPApGNpdHnApXN0YXRlwK5ldmVudFN0YXJ0VGltZcCodGltZXpvbmXArmV2ZW50RGF0ZVByZWYxwK5ldmVudERhdGVQcmVmMsCuZXZlbnREYXRlUHJlZjPArWNvbmZpcm1lZERhdGXAtGRhdGVMb2NhdGlvbkNvbW1lbnRzwLFwb3RlbnRpYWxTcGVha2Vyc8CndmVudWVJZMCtdmVudWVSb29tTmFtZcC0dmVudWVHdXJhbnRlZWROdW1iZXLAqWNhdGVyZXJJZMCtY2F0ZXJlclN0YXR1c8CxZXN0aW1hdGVkTWVhbENvc3TAt2V4cGVjdGVkTnVtYmVyQXR0ZW5kZWVzwK12ZW51ZUFWU3RhdHVzwK12ZW51ZUNvbW1lbnRzwK5pbnZpdGF0aW9uc1BERsC5aW52aXRhdGlvbnNIYXJkY29weURpcmVjdMC2aW52aXRhdGlvbnNIYXJkY29weVJlcMC2cmVwTWFpbGluZ0FkZHJlc3NFZGl0c8CzaW52aXRhdGlvbnNDb21tZW50c8CwZW1haWxJbnZpdGF0aW9uc8CzaGFyZGNvcHlJbnZpdGF0aW9uc8C7YXVkaWVuY2VSZWNydWl0bWVudENvbW1lbnRzwKlhdHRlbmRlZXPAsnByZWNvbmZpcm1lZE51bWJlcsCuY29tbXVuaWNhdGlvbnPApXByZklkwKpoaXN0b3JpY2FswKJpZNoAJDRFMDE3RDgyLTg0MzAtNDdFRC1CM0I4LURBNEVGNDc4MjYxMqR0eXBlpUV2ZW50";
        var objectSerialized = "3gAuqWNyZWF0ZWRBdLgyMDE0LTA1LTIzVDIwOjE5OjM2LjkwNFqpdXBkYXRlZEF0uDIwMTQtMDUtMjRUMDI6NTI6NTkuNjc1WqVicmFuZLdyZXF1ZXN0IGJyYW5kIGZyb20gamF2aa5zdWJtaXRpbmdUaW1lcwKpcHJvZ3JhbUlk2gAkNTAxODYzRkItRUMxNS00QUFBLUE1MzMtRDE4NUM1OTkwMjg4sXByZXNlbnRhdGlvblRpdGxls3JlcXVlc3QgZnJvbSBqYXZpIDKpZXZlbnRUeXBlqUluIE9mZmljZalqb2JOdW1iZXLApnN0YXR1cwCwY3JnUGVyY2VudGFnZUZlZcCvcmVxdWVzdGluZ1JlcElk2gAkQjRCNEEyNkItRkE5RS00Mzk0LUE2NjgtRTUwN0I5QzVFNDA0qHNlcnZpY2VzwKRjaXR5pWNpdHkgpXN0YXRlwK5ldmVudFN0YXJ0VGltZcCodGltZXpvbmXArmV2ZW50RGF0ZVByZWYxwK5ldmVudERhdGVQcmVmMsCuZXZlbnREYXRlUHJlZjPArWNvbmZpcm1lZERhdGXAtGRhdGVMb2NhdGlvbkNvbW1lbnRzwLFwb3RlbnRpYWxTcGVha2Vyc5GNpHR5cGWnU3BlYWtlcqZfc2F2ZWTCp19ldmVudHOAqWNyZWF0ZWRBdLgyMDE0LTA1LTI0VDAxOjQ4OjU4LjAwNVqpdXBkYXRlZEF0wKljb250YWN0SWTaACRBMDBFQ0YxNy05RERELTRGRTktOURDQy1COUU1M0UyREM4MzepZmlyc3ROYW1lpGJhcnSmc3RhdHVzwKRjaXR5wKVzdGF0ZcCqdHJhdmVsVHlwZcC0Z3JvdW5kVHJhbnNwb3J0YXRpb27AqGNvbW1lbnRzwKd2ZW51ZUlkwK12ZW51ZVJvb21OYW1lwLR2ZW51ZUd1cmFudGVlZE51bWJlcsCpY2F0ZXJlcklkwK1jYXRlcmVyU3RhdHVzwLFlc3RpbWF0ZWRNZWFsQ29zdMC3ZXhwZWN0ZWROdW1iZXJBdHRlbmRlZXPArXZlbnVlQVZTdGF0dXPArXZlbnVlQ29tbWVudHPArmludml0YXRpb25zUERGwLlpbnZpdGF0aW9uc0hhcmRjb3B5RGlyZWN0wLZpbnZpdGF0aW9uc0hhcmRjb3B5UmVwwLZyZXBNYWlsaW5nQWRkcmVzc0VkaXRzwLNpbnZpdGF0aW9uc0NvbW1lbnRzwLBlbWFpbEludml0YXRpb25zwLNoYXJkY29weUludml0YXRpb25zwLthdWRpZW5jZVJlY3J1aXRtZW50Q29tbWVudHPAqWF0dGVuZGVlc8CycHJlY29uZmlybWVkTnVtYmVywK5jb21tdW5pY2F0aW9uc8ClcHJmSWTAqmhpc3RvcmljYWzAomlk2gAkMTk1MTNFRjYtNzY2Qi00OEQ5LThGRTktNThFNDJDNEJBRTI3pHR5cGWlRXZlbnQ=";
        var result = {};
        var DBUtils = require('../helpers/utils/DBUtils.js');   
        result = DBUtils.DeserializeObject(objectSerialized);
        self.respond(result, { format: 'json' });
      }


      this.joinTest = function(req, resp, param){
          var $thisRequest = this;
          var MJ = require("mongo-fast-join"),
          mongoJoin = new MJ();
          var MongoClient = require('mongodb').MongoClient;



  MongoClient.connect('mongodb://192.168.2.146:27017/encompass', function(err, db) {
    if(err) throw err;

    var collection1 = db.collection('reps');
    var collection2 = db.collection('events');


mongoJoin
    .query(
      //say we have sales records and we store all the products for sale in a different collection
      collection1,
        {}, //query statement
        {}, //fields
        {
            limit: 10000//options
        }
    )
    .join({
        joinCollection: collection2,
        //respects the dot notation, multiple keys can be specified in this array
        leftKeys: ["_id"],
        //This is the key of the document in the right hand document
        rightKeys: ["requestingRepId"],
        //This is the new subdocument that will be added to the result document
        newKey: "events"
    })
    //Call exec to run the compiled query and catch any errors and results, in the callback
    .exec(function (err, items) {
//        console.log(items);
          var sift = require('sift');
          var result = new Array();
          for (var i =0 ; i<items.length;i++){
            //console.log('>>>>>>>>>>>>> items[i].events '+JSON.stringify(items[i].events));
            var sifted = sift({'jobNumber': '9999999'}, items[i].events);//count = 1
            if (typeof(sifted) == 'object'){
              //console.log('>>>>>>>>>>>>> sifted '+JSON.stringify(sifted));
             result = result.concat(sifted);
            }
          }
          $thisRequest.respond(result, { format: 'json' });
    });
  });          




      },

      this.search = function (req, resp, params) {
          var $thisRequest = this;

          var search = require('../helpers/search/search.js');
          search.AdvanceSearch($thisRequest, 'contacts');
          /*          console.log(JSON.stringify(params));
          contactCollection = geddy.globalDb.collection('contacts');

          var searchObject = new Object();
          var conditionsArray = [];
          var keys = [];
          var condition = new Object();

          //paramsList
          for (var key in params) {
          keys.push(key);
          }

          for (var i = 0; i < keys.length; i++) {
          if (keys[i].match(/(method|controller|action)/)) {
          continue;
          }
          var filters = "";
          if (Array.isArray(params[keys[i]])) {
          //To get OR filters
          filters = params[keys[i]].join("|");
          console.log("      >>>>>    IS ARRAY   ");
          } else filters = params[keys[i]];
          console.log("      >>>>>       filters->" + filters);
          var newRegex = new RegExp(".*(" + filters + ").*","i");
          condition[keys[i]] = newRegex;
          }
          conditionsArray.push(condition);
          searchObject['$or'] = conditionsArray;

          console.log("<><<<<<<<<<<<<<<<<<< Search OBJECT conditions Array " + JSON.stringify(searchObject));
          contactCollection.find(searchObject).toArray(function (err, contactList) {
          //console.log("CONTACT LIST >>>> "+JSON.stringify(contactList));
          var result = new Object();
          result.dataset = contactList;
          $thisRequest.respond(result, { format: 'json' });
			
          });*/


      },

      this.searchOld = function (req, resp, params) {
          var $thisRequest = this;
          //console.log(JSON.stringify(params));


          var keys = [];
          for (var key in params) {
              keys.push(key);
          }

          //console.log("      >>>>>       KEYS->" + JSON.stringify(keys));
          var query = {};
          var query2 = { 'degrees': { like: /MD/} };



          for (var i = 0; i < keys.length; i++) {

              if (keys[i].match(/(method|controller|action)/)) {
                  continue;
              }

              var filters = "";

              if (Array.isArray(params[keys[i]])) {
                  filters = params[keys[i]].join("|");
                  console.log("      >>>>>    IS ARRAY   ");
              } else filters = params[keys[i]];


              //console.log("      >>>>>       filters->" + filters);


              var newRegex = new RegExp(".*(" + filters + ").*");
              query[keys[i]] = { like: newRegex };



              //{ lastName: { like: /.*(irz|nh).*/} };
          }

          //console.log("      >>>>>       query->" + JSON.stringify(query));
          //console.log("      >>>>>      query2->" + JSON.stringify(query2));


          geddy.model.Contact.all(query, /*{ nocase: ["specialties"] },*/
                   function (err, contact) {
                       var result = { dataset: [] };
                       if (err) {
                           console.log(err);
                       }
                       else if (contact.length > 0) {
                           for (i = 0; i < contact.length; i++) {
                               //console.log('Found Contact');
                               //console.log(contact[i].lastName);
                               result.dataset.push(contact[i]);
                           }
                       } else { }
                       console.log("      >>>>>      query2->" + JSON.stringify(query2));
                       $thisRequest.respond(result, { format: 'json' });
                   });
      };





      this.save = function (req, resp, params) {
          var $thisRequest = this;
          //console.log('Done');

          geddy.model.Contact.first(params.id, function (err, contact) {
              if (err) {
                  //throw err;
                  console.log(err);
              }
              if (!contact) {
                  throw new geddy.errors.NotFoundError();
              }
              else {

                  contact.suffix = params.Suffix;
                  if (contact.degrees && contact.degrees[0]) {
                      contact.degrees[0] = params.Degrees;
                  }
                  contact.salutation = params.Salutation;

                  contact.npi = params.npi;
                  contact.dea = params.dea;
                  contact.tsaName = params.TsaName;
                  contact.tsaGender = params.TsaGender;
                  contact.tsaDOB = params.TsaDOB;
                  contact.dietaryRestrictions = params.dietaryRestrictions;
                  contact.specialNeeds = params.specialNeeds;
                  contact.notes = params.notes;



                  if (contact.degrees && contact.degrees[1]) {
                      contact.degrees[0] = params.Degrees;
                  }

                  if (contact.affiliations && contact.affiliations[0]) {
                      contact.affiliations[0].affiliationName = params.Affiliation[0];
                      contact.affiliations[0].title = params.AffiliationTitle[0];
                      contact.affiliations[0].city = params.AffiliationCity[0];
                  }

                  if (contact.affiliations && contact.affiliations[1]) {
                      contact.affiliations[1].affiliationName = params.Affiliation[1];
                      contact.affiliations[1].title = params.AffiliationTitle[1];
                      contact.affiliations[1].city = params.AffiliationCity[1];
                  }

                  if (contact.emailAddresses && contact.emailAddresses[0]) {
                      contact.emailAddresses[0].email = params.Email[0];
                      //contact.emailAddresses[1].prefered = params.AffiliationTitle[1];
                  }


                  if (contact.phoneNumbers) {
                      for (var i = 0; i < contact.phoneNumbers.length; i++) {
                          contact.phoneNumbers[i].number = params.PhoneNumber[i];
                      }
                  }

                  if (contact.addresses) {
                      for (var i = 0; i < contact.addresses.length; i++) {
                          contact.addresses[i].addressType = params.AddressType[i];
                          contact.addresses[i].address1 = params.Address1[i];
                          contact.addresses[i].address2 = params.Address2[i];
                          contact.addresses[i].city = params.City[i];
                          contact.addresses[i].state = params.State[i];
                          contact.addresses[i].postalCode = params.Zip[i];
                          contact.addresses[i].country = params.Country[i];
                      }
                  }

                  if (contact.officeStaffContact) {
                      contact.officeStaffContact.fullName = params.officeStaffContact;
                      contact.officeStaffContact.email = params.officeStaffContactEmail;
                      if (contact.officeStaffContact.phoneNumbers) {
                          contact.officeStaffContact.phoneNumbers[0].number = params.officeStaffContactPhone;
                      }
                  }


                  if (contact.emergencyContacts && contact.emergencyContacts[0]) {
                      contact.emergencyContacts[0].fullName = params.EmergencyContactName;
                      if (contact.emergencyContacts[0].phoneNumbers) {
                          contact.emergencyContacts[0].phoneNumbers[0].number = params.EmergencyContactPhones;
                      }
                  }


                  if (contact.specialties) {
                      if (contact.specialties[0]) {
                          params.Specialty[0];
                      } else contact.specialties.push(params.Specialty[0]);
                      if (contact.specialties[1]) {
                          params.Specialty[1];
                      } else contact.specialties.push(params.Specialty[1]);
                      if (contact.specialties[2]) {
                          params.Specialty[2];
                      } else contact.specialties.push(params.Specialty[2]);
                  }

                  if (contact.therapeuticInterests && contact.therapeuticInterests[0]) {
                      contact.therapeuticInterests[0] = params.therapeuticInterests;
                  }

                  if (contact.medicalLicenses && contact.medicalLicenses[0]) {
                      contact.medicalLicenses[0].number = params.medicalLicenses[0];
                      contact.medicalLicenses[0].state = params.medicalLicenseState[0];
                  }

                  if (contact.medicalLicenses && contact.medicalLicenses[1]) {
                      contact.medicalLicenses[1].number = params.medicalLicenses[1];
                      contact.medicalLicenses[1].state = params.medicalLicenseState[1];
                  }


                  contact.save(function (err, data) {
                      if (err) {
                          console.log(JSON.stringify(err));
                          throw err;
                      } else {
                          $thisRequest.respond(params, { format: 'json' });
                      }
                  });

              }
          });
      };



      var result = { diff: [], newContacts: [], total: [] };
      this.index = function (req, resp, params) {
          var $thisRequest = this;

          var form = new formidable.IncomingForm();
          form.uploadDir = "F:\\InteractiveLabs\\geddy\\myapp1\\uploads\\";
          var self = this;
          //console.log(" SALIDA ->" + "FILE");

          form.on('end', function () {
              //console.log('FILE DONE');
          });

          form.parse(req, function (err, fields, files) {
              //console.log(" SALIDA ->" + JSON.stringify(files["ExcelFile"]));
              var path = files.ExcelFile.path;
              //console.log(" Filename ->" + path);

              var XLSX = require('xlsx')
              var xlsx = XLSX.readFile(path);
              var sheet_name_list = xlsx.SheetNames;

              var sheet = 0;

              xlsx.SheetNames.forEach(function (y) {
                  sheet++;
                  var array1 = XLSX.utils.sheet_to_row_object_array(xlsx.Sheets[y]);

                  array1.forEach(function (data) {
                      result.total.push(data);
                  });
              });

              //console.log("New contact db->" + JSON.stringify(result.total));
              Recurcion(result.total.shift(), $thisRequest);

          });

      };


      // Final task (same in all the examples)
      var Final = function (self) {
          //console.log('Done');
          self.respond(result, { format: 'json' });
      }

      var Recurcion = function (currentContact, self) {
          if (currentContact) {
              geddy.model.Contact.all({ firstName: currentContact["First Name"], lastName: currentContact["Last Name"] },
                   function (err, contact) {
                       if (err) {
                           console.log(err);
                       }
                       else if (contact.length > 0) {
                           var diff = { a: currentContact, b: [] };
                           for (i = 0; i < contact.length; i++) {
                               //console.log('Found Contact');
                               //console.log(contact[i].firstName);
                               diff.b.push(contact[i]);
                           }
                           result.diff.push(diff);
                       } else {
                           //console.log('Contact Not Found create it');
                           var newContact = geddy.model.Contact.create();
                           var newContactType = geddy.model.ContactType.create();
                           newContactType.contactTypeName = "kol";
                           newContact.contactType = newContactType;
                           newContact.firstName = currentContact["First Name"];
                           newContact.lastName = currentContact["Last Name"];
                           newContact.middleName = currentContact["Middle"];
                           newContact.suffix = currentContact["Suffix"];
                           newContact.degrees = new Array();
                           newContact.degrees.push(currentContact["Degrees"]);

                           newContact.salutation = currentContact["Saluation"];

                           newContact.therapeuticInterests = new Array();
                           newContact.therapeuticInterests.push(currentContact["Therapeutic Area/Area(s) of Interest"]);

                           newContact.tsaName = currentContact["TSA: Name on Photo"];
                           newContact.tsaGender = currentContact["TSA: Gender"];
                           newContact.tsaDOB = currentContact["TSA: Date of Birth (DOB"];

                           newContact.dietaryRestrictions = currentContact["Dietary Restrictions"];
                           newContact.specialNeeds = currentContact["Special Needs"];


                           newContact.npi = currentContact["NPI # (10-digit)"];
                           newContact.dea = currentContact["DEA #"];

                           newContact.notes = currentContact["Notes"];


                           /*Address*/
                           var newAddress = geddy.model.Address.create();
                           newAddress.addressType = currentContact["Address Type"];
                           newAddress.address1 = currentContact["Address 1"];
                           newAddress.address2 = currentContact["Address 2"];
                           newAddress.city = currentContact["City"];
                           newAddress.state = currentContact["State (abbreviated)"];
                           newAddress.postalCode = currentContact["Zip"];
                           newAddress.country = currentContact["Country"];
                           newContact.addresses = new Array();
                           newContact.addresses.push(newAddress);

                           /*Phones*/
                           newContact.phoneNumbers = new Array();
                           var newPhone;

                           if (currentContact["Office Phone"]) {
                               newPhone = geddy.model.ContactPhoneNumber.create();
                               newPhone.phoneType = "Office";
                               newPhone.number = currentContact["Office Phone"].replace(/-.*$/, "");
                               newContact.phoneNumbers.push(newPhone);
                           }


                           if (currentContact["Fax"]) {
                               newPhone = geddy.model.ContactPhoneNumber.create();
                               newPhone.phoneType = "Fax";
                               newPhone.number = currentContact["Fax"].replace(/-.*$/, "");
                               newContact.phoneNumbers.push(newPhone);
                           }

                           if (currentContact["Home"]) {
                               newPhone = geddy.model.ContactPhoneNumber.create();
                               newPhone.phoneType = "Home";
                               newPhone.number = currentContact["Home"].replace(/-.*$/, "");
                               newContact.phoneNumbers.push(newPhone);
                           }


                           if (currentContact["Cell"]) {
                               newPhone = geddy.model.ContactPhoneNumber.create();
                               newPhone.phoneType = "Cell";
                               newPhone.number = currentContact["Cell"].replace(/-.*$/, "");
                               newContact.phoneNumbers.push(newPhone);
                           }

                           if (currentContact["Other Contact Relationship"]) {
                               newPhone = geddy.model.ContactPhoneNumber.create();
                               newPhone.phoneType = currentContact["Other Contact Relationship"];
                               newPhone.number = currentContact["Other Contact"].replace(/-.*$/, "");
                               newContact.phoneNumbers.push(newPhone);
                           }


                           newContact.specialties = new Array();
                           if (currentContact["Specialty 1"]) {
                               newContact.specialties.push(currentContact["Specialty 1"]);
                           }
                           if (currentContact["Specialty 2"]) {
                               newContact.specialties.push(currentContact["Specialty 2"]);
                           }
                           if (currentContact["Specialty 3"]) {
                               newContact.specialties.push(currentContact["Specialty 3"]);
                           }

                           newContact.medicalLicenses = new Array();
                           var medicalLicense;
                           if (currentContact["Medical License #1"]) {
                               medicalLicense = geddy.model.MedicalLicense.create();
                               medicalLicense.state = currentContact["Licensing State #1"];
                               medicalLicense.number = currentContact["Medical License #1"];
                               newContact.medicalLicenses.push(medicalLicense);
                           }

                           if (currentContact["Medical License #2"]) {
                               medicalLicense = geddy.model.MedicalLicense.create();
                               medicalLicense.state = currentContact["Licensing State #2"];
                               medicalLicense.number = currentContact["Medical License #2"];
                               newContact.medicalLicenses.push(medicalLicense);
                           }


                           newContact.emailAddresses = new Array();
                           var eMailAddress;
                           if (currentContact["Preferred Email Address"]) {
                               eMailAddress = geddy.model.ContactEmailAddress.create();
                               eMailAddress.email = currentContact["Preferred Email Address"];
                               eMailAddress.prefered = true;
                               newContact.emailAddresses.push(eMailAddress);
                           }

                           if (currentContact["Seconday Email Address"]) {
                               eMailAddress = geddy.model.ContactEmailAddress.create();
                               eMailAddress.email = currentContact["Seconday Email Address"];
                               eMailAddress.prefered = false;
                               newContact.emailAddresses.push(eMailAddress);
                           }


                           newContact.theraputicInterests = currentContact["Therapeutic Area/Area(s) of Interest"];


                           //newContact.officeStaffContact = new Array();
                           var officeStaffContact = geddy.model.ContactInfo.create();
                           officeStaffContact.fullName = currentContact["Office Staff Contact"];
                           officeStaffContact.phoneNumbers = new Array();
                           officeStaffContact.email = currentContact["Office Staff Email"];
                           newPhone = geddy.model.ContactPhoneNumber.create();
                           newPhone.number = currentContact["Office Staff Tel #"];
                           officeStaffContact.phoneNumbers.push(newPhone);
                           //newContact.officeStaffContact.push(officeStaffContact);
                           newContact.officeStaffContact = officeStaffContact;

                           newContact.emergencyContacts = new Array();
                           var emergencyContact = geddy.model.ContactInfo.create();
                           emergencyContact.fullName = currentContact["Emergency Contact Name"];
                           emergencyContact.phoneNumbers = new Array();
                           newPhone = geddy.model.ContactPhoneNumber.create();
                           newPhone.number = currentContact["Emergency Contact #"].replace(/-.*$/, "");
                           emergencyContact.phoneNumbers.push(newPhone);
                           newContact.emergencyContacts.push(emergencyContact);


                           newContact.affiliations = new Array();
                           var affiliation = geddy.model.Affiliation.create();
                           affiliation.affiliationName = currentContact["Affiliation"];
                           affiliation.title = currentContact["Title"];
                           affiliation.city = currentContact["City, State of Affiliation/Institution"];
                           newContact.affiliations.push(affiliation);


                           if (currentContact["Additional Affiliation"]) {
                               affiliation = geddy.model.Affiliation.create();
                               affiliation.affiliationName = currentContact["Additional Affiliation"];
                               affiliation.title = currentContact["Additional Title"];
                               affiliation.city = currentContact["City, State of Additional Affiliation/Institution"];
                               newContact.affiliations.push(affiliation);
                           }


                           if (currentContact["Frequent Flyer Airline"]) {
                               newContact.airlines = new Array();
                               var travelAir = geddy.model.TravelAir.create();
                               travelAir.airlineName = currentContact["Frequent Flyer Airline"];
                               travelAir.ffNumber = currentContact["Frequent Flyer #"];
                               newContact.airlines.push(travelAir);
                           }


                           result.newContacts.push(newContact);
                           //console.log("New contact db->" + JSON.stringify(newContact));
                           newContact.save(function (err, data) {
                               if (err) {
                                   console.log(JSON.stringify(err));
                                   throw err;
                               }
                           });
                       }


                       return Recurcion(result.total.shift(), self);
                   });

          }
          else {
              return Final(self);
          }
      }


  };
  

exports.Test = Test;


