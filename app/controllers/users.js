
var passport = require('../helpers/passport');
var cryptPass = passport.cryptPass;
var requireAuth = passport.requireAuth;
var path = require('path');
var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var search = require('../helpers/search/search.js');
var ModelName= "User";


var failureRedirect = geddy.config.passport.failureRedirect;

var Users = function () {

    this.before(checkExpiration, { 
                except: ['joinNow', 'resetPassword', 'forgotPassword', 'confirmUser', 'userPendingOfConfirmation','confirmUser','showActivation']
        });


    function checkExpiration ()
    {
        var self = this;
        if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
        {
            geddy.globalDb.close();
            self.respond('false',{format: 'txt'});
        }
        else
        {
            self.session.set('activeController', self.name);
        }
    };

    this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];
    
    
    this.joinNow =function(req, resp, params){
        // console.log(">>>>>>>>>>>>>>>>> params JOIN NOW  "+JSON.stringify(params));
        var self = this;
        var $thisRequest = this;
        //if (params.addUser){
            geddy.model.User.first({ email:params.email,firstName:params.firstName, lastName:params.lastName}, {nocase: true},
            function (err, user) {
                if (err) {
                    //$thisRequest.redirect(failureRedirect);
                }
                if (!user){
                    /*user = geddy.model.User.create(params);
                    var uuid = require('node-uuid');geddy.constants.userState
                    user.guid = uuid.v4();
                    if (user.isValid()) {
                        user.password = cryptPass(user.password);
                        user.state = constants.userState.PendingOfConfirmation;
                        user.save();
                    }else{
                    */
                        //console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> User does not exist");
                        self.flash.error('Account does not exist. Please make sure you entered the information correctly (This is case sensitive).');
                        self.respond({showActivation:true},{format: 'html',template: 'app/views/main/login'});
                    //}
                }else if (user && (!user.state || user.state == constants.userState.PendingOfConfirmation)) {
                    //get global email address
                    geddy.model.Setting.first(function(err, settings) {
                         if (err) {
                           throw err;
                         }

                        var uuid = require('node-uuid');
                        user.guid = uuid.v4();
                        var notificator = require('../helpers/notificator/notificator.js');
                        var notification = {
                            type: "UserConfirmation",
                            to: user.email,
                            from:  settings.globalEmailAddress,
                            subject: "User confirmation",
                            firstName: user.firstName,
                            lastName: user.lastName,
                            companyName: self.session.get('companyName'),
                            crgNumber:constants.CRG_NUMBER,
                            notificationEmail: settings.globalEmailAddress,//self.session.get('companyEmail'),
                            guid: user.guid
                        };
                        result = notificator.sendNotification(notification);
                        user.state = constants.userState.PendingOfConfirmation;
                        user.save();
                        //$thisRequest.redirect("/UserConfirmationProcess.html");
                        self.respond({},{format: 'html',template: 'app/views/users/userConfirmationProcess'});
                    });
                }else{
                    console.log("User already confirmed");
                    self.flash.error('This account has already been activated.  If you need to, please reset your password.');
                    $thisRequest.redirect(failureRedirect);
                }
            });
        //}
    };

    this.resetPassword = function (req, resp, params) {
        var self = this;
        var result = false;
        //console.log(">>>>>>>>>>>>>>>>>>>> RESET PASS PARAMS"+JSON.stringify(params));
        if (params.resetPassword){
            var userId = self.session.get('userId');
            var hasActiveSession = false; 
            var query = null;
            
            if(params.password != params.passwordConfirmation)
            {
                self.flash.error('Password and Password Confirmation do not match.');
                self.respond({},{format: 'html',template: 'app/views/users/resetPassword'});    
                return false;
            }

            if(!this.validPassword(params.password))
            {
                self.flash.error('The password does not meet the security rules.');
                self.respond({},{format: 'html',template: 'app/views/users/resetPassword'});    
                return false;
            }



            if (typeof (userId) != "undefined"){
               query = { id: userId };
               hasActiveSession = true;
            }else{
                var id = this.session.get('tempuserId');
                if (id){
                    query = { guid: id };
                }
            }
            //console.log(">>>>>>>>> QUERY" + JSON.stringify(query));
            if (query){
            geddy.model.User.first(query,
            function (err, user) {
                if (err) {
                    self.redirect(failureRedirect);
                    console.log(">>>>>>>>>>>ERROR:"+err);
                }
                if (user) {
                    console.log(JSON.stringify(user));
                    user.password = params.password;
                    if (user.isValid()) {
                        user.password = cryptPass(user.password);
                    }
                    user.save();
                }
                if (hasActiveSession){
                    self.redirect("/");
                }else {
                    self.redirect(failureRedirect);
                }
            });
            }
        }else{
            self.session.set('tempuserId', params.id);
            var id = this.session.get('tempuserId');
            //console.log("ID->"+id);
            //temporal
            self.respond({},{format: 'html',template: 'app/views/users/resetPassword'});
            //$thisRequest.redirect("/resetpassword.html");
        }
    };

    this.updatePassword=function(req,resp,params){
        var self = this;
        var result = false;
        if (params.email) {
            geddy.model.User.first({ email: params.email },
            function (err, user) {
                if (err) {
                    self.redirect(failureRedirect);
                }
                 if (user) {
                    user.password = params.password;
                    if (user.isValid()) {
                        user.password = cryptPass(user.password);
                    }
                    user.save(function (err, data) {
                        if (err) {
                                throw err;
                            }
                            if (!object) {
                                throw new geddy.errors.BadRequestError();
                            }
                            else {
                                console.log("-------Updating password------------------");
                            }
                    });
                 }
                 self.respond({},{format: 'html',template: 'app/views/users/forgotPassword'});
            });
                
        }
        
    }

    this.forgotPassword = function (req, resp, params) {
        var self = this;
        var result = false;
        if (params.email) {
            geddy.model.User.first({ email: params.email },
            function (err, user) {
                if (err) {
                    self.redirect(failureRedirect);
                }
                if (user) {
                    //get global email address
                    geddy.model.Setting.first(function(err, settings) {
                         if (err) {
                           throw err;
                         }

                        var uuid = require('node-uuid');
                        user.guid = uuid.v4();
                        user.save();
                        var notificator = require('../helpers/notificator/notificator.js');
                        var notification = {
                            type: "ForgotPassword",
                            to: user.email,
                            from:  settings.globalEmailAddress,
                            subject: "Forgot password",
                            firstName: user.firstName,
                            lastName: user.lastName,
                            companyName: self.session.get('companyName'),
                            crgNumber:constants.CRG_NUMBER,
                            notificationEmail: settings.globalEmailAddress,
                            guid:user.guid
                        };
                        result = notificator.sendNotification(notification);
                    });
                }
                //$thisRequest.redirect("/forgotPassword.html");
                self.respond({},{format: 'html',template: 'app/views/users/forgotPassword'});
            });
        }
    };


    this.submitProfileUpdate = function (req, resp, params) {
        var self = this;
        var result = false;

        //get global email address
        geddy.model.Setting.first(function(err, settings) {
             if (err) {
               throw err;
             }

            var notificator = require('../helpers/notificator/notificator.js');
            var notification = {
                type: "ProfileUpdate",
                to: settings.globalEmailAddress,
                from:  params.email, 
                subject: "Profile Update Request",
                userName: params.userName,
                updates: params.updates,
            };

            //result = notificator.sendNotification(notification);

            self.respond(true,{format: 'json'});
        });
    ;}


    this.confirmUser = function (req, resp, params) {
        var self = this;
        var result = false;
        var $thisRequest = this;
        var result = false;
        if (params.setPassword){
           var id = this.session.get('tempuserId');

            if(!this.validPassword(params.password))
            {
                self.flash.error('The password does not meet the security rules.');
                self.respond({},{format: 'html',template: 'app/views/users/confirmUser'});    
                return false;
            }


           if(params.password == params.passwordConfirmation)
           {
               if (id){
                    geddy.model.User.first({ guid: id },
                    function (err, user) {
                        if (err) {
                            self.redirect(failureRedirect);
                        }
                        if (user) {
                            user.password = params.password;
                            if (user.isValid()) {
                                user.password = cryptPass(user.password);
                            }
                            user.state = constants.userState.Active;
                            user.save();
                        }
                        self.redirect(failureRedirect);
                    });
                }
            }
            else
            {
                self.flash.error('Password and Password Confirmation do not match.');
                self.respond({},{format: 'html',template: 'app/views/users/confirmUser'});    
            }
        }else{
            self.session.set('tempuserId', params.id);
            var id = this.session.get('tempuserId');
            //temporal
            self.respond({},{format: 'html',template: 'app/views/users/confirmUser'});
        }
    };

    this.userPendingOfConfirmation = function (req, resp, params) {
    	var $thisRequest = this;
    	var result = false;
    	var userId = this.session.get('tempuserId');
    	if (userId) {
    		geddy.model.User.first({ id: userId },
    				function (err, user) {
    			if (err) {
    				$thisRequest.redirect(failureRedirect);
    			}
    			//console.log (" >>>>> companyName"+self.session.get('companyName'));
    			//console.log (" >>>>> companyName"+self.session.get('crgNumber'));
    			if (user) {

                    //get global email address
                    geddy.model.Setting.first(function(err, settings) {
                         if (err) {
                           throw err;
                         }

        				var notificator = require('../helpers/notificator/notificator.js');
        				var notification = {
        						type: "UserConfirmation",
        						to: user.email,
                                from: settings.globalEmailAddress,
        						subject: "User confirmation",
        						firstName: user.firstName,
        						lastName: user.lastName,
        						companyName: session.get("companyName"),
        						crgNumber:geddy.crgNumber,
        						notificationEmail: settings.globalEmailAddress,//session.get("companyName")+geddy.mailDomain,
        						guid: user.guid
        				};
        				result = notificator.sendNotification(notification);
                    });
    			}

    			$thisRequest.redirect("/users/joinNow");
    		});
    	}
    };
    
    this.showActivation = function (req, resp, params) {
    	 this.respond({showActivation:true},{format: 'html',template: 'app/views/main/login'});
    };



   
    this.index = function (req, resp, params) {

        // var temp = geddy.viewHelpers.urlFor({ relPath: true });

        var self = this;
        var userProcessed = 0;

        self.session.set('currentSection', constants.sections.USERS_SECTION);

        DBUtils.unlockObject(self);
        
        //TODO: uncomment check for sessionUser so we can block access to users section to only super admins
        //first check to see if we're allowed to be accesing this page
        //if (self.session.get('userId') && (self.session.get('userRoleId') == geddy.constants.roles.SUPER_ADMIN.label) )
        //{

            geddy.model.User.all(function (err, users) {
                var i;
                // if (users.length == 0) {
                    self.respond({ params: params, users: users });
                // }
                // for (i = 0; i < users.length; i++) {
                    //get role name for each user 
                    // geddy.model.Role.first({ id: users[i].roleId }, function (err, role) {
                    //     if (!role) {
                    //         users[userProcessed++].roleId = "n/a";
                    //     }
                    //     else {
                    //         users[userProcessed++].roleId = role.roleId;
                    //     }
                    //     if (userProcessed == users.length) {
                    //         self.respond({ params: params, users: users });
                    //     }
                    // });
                // }
            });
        //} else {
         //   self.redirect('/login');
       // }
    };

    this.add = function (req, resp, params) {
        var self = this;
        var rolesArray = [];
        for (var role in constants.roles)
        {
            if (constants.roles[role].label == "Super Admin" || constants.roles[role].label == "Admin" || constants.roles[role].label == "Client")
            {
                rolesArray.push(constants.roles[role]);
            }
        }

        geddy.model.Client.all(function (err, clients){
            if (err)
            {
                throw err;
            }

            self.respond({ params: params, roles: rolesArray, clients: clients, uploadedFile: params.uploaded_file });
        });
    };

    this.updateEmailsBulk = function (req, resp, params)
    {
         var self = this;
         
        geddy.model.User.all (function (err, users) {
            for (var i = users.length - 1; i >= 0; i--) {
                users[i].email = users[i].email.replace('bdsi-us','bdsi');

                users[i].save();
            }

            self.respond(true, {
                  format: 'json'
            });
        });
    };

    this.create = function (req, resp, params) {
        var self = this;

        //clean up params
        params.firstName = params.firstName.trim();
       // params.middleName = params.middleName.trim();
        params.lastName = params.lastName.trim();
        params.email = params.email.trim();

        var user = geddy.model.User.create(params);

        //console.log(">>>>>>>>>>>>>>>>>>>>>params " + JSON.stringify(params));
        //geddy.file.cpR('','directory');
        // Non-blocking uniqueness checks are hard
        geddy.model.User.first({ email: user.email }, function (err, data) {
            if (data) {
                params.errors = {
                    email: 'This email is already in use.'
                };
                self.transfer('add');
            }
            else {
                // if (user.isValid()) {
                //     user.password = cryptPass(user.password);
                // }
                user.save(function (err, data) {
                    if (err) {
                        params.errors = err;
                        self.transfer('add');
                    }
                    else {
                        //	uploadFile(self,req);
                        self.redirect({ controller: self.name });
                    }
                });
            }
        });
    };

    this.show = function (req, resp, params) {
        console.log(1212);
        var self = this;
        geddy.model.User.first(params.id, function (err, user) {
            if (!user) {
                var err = new Error();
                err.statusCode = 400;
                self.error(err);
            } else {
                //self.session.set('currentUser', user);
                //null certain fields before sending them back to show... we don't need to be moving that data over the network
                user.password = null;
                user.guid = null;
                user.linkedId = null;

                self.respond({ params: params, user: user.toObj() });
            }
        });
    };

    this.edit = function (req, resp, params) {
        var self = this;

        geddy.model.User.first(params.id, function (err, user) {
            if (!user) {
                var err = new Error();
                err.statusCode = 400;
                self.error(err);
            } else {

                var rolesArray = [];
            /*
                switch(user.roleId)
                {
                    case constants.roles.SUPER_ADMIN.label:
                    case constants.roles.ADMIN.label:
                        rolesArray.push(constants.roles.SUPER_ADMIN);
                        rolesArray.push(constants.roles.ADMIN);
                        break;

                    case constants.roles.CLIENT.label:
                        rolesArray.push(constants.roles.CLIENT);
                        break;

                    case constants.roles.REP.label:
                        rolesArray.push(constants.roles.REP);
                        break;

                    case constants.roles.MANAGER.label:
                        rolesArray.push(constants.roles.MANAGER);
                        break;

                    case constants.roles.KOL.label:
                        rolesArray.push(constants.roles.KOL);
                        break;
                }
                */
                rolesArray.push(constants.roles.SUPER_ADMIN);
                rolesArray.push(constants.roles.ADMIN);
                rolesArray.push(constants.roles.CLIENT);
                rolesArray.push(constants.roles.REP);
                rolesArray.push(constants.roles.MANAGER);
                 rolesArray.push(constants.roles.KOL);


                geddy.model.Client.all(function (err, clients){
                    if (err)
                    {
                        throw err;
                    }

                    self.respond({params: params, user: user, roles:rolesArray, clients: clients,});
                });
            }
        });
    };

    this.update = function (req, resp, params) {
        var self = this;
        // var newEmail = params.email;

        geddy.model.User.first(params.id, function (err, user) {
            //make sure we have the photo path correctly here

            console.log(JSON.stringify(params));
            user.updateAttributes(params);

            user.save(function (err, data) {
                if (err) {
                    params.errors = err;
                    self.transfer('edit');
                } else {
                    self.redirect({ controller: self.name });
                }
            });
        });
    };

    this.destroy = function (req, resp, params) {
        var self = this;

        geddy.model.User.remove(params.id, function (err) {
            if (err) {
                params.errors = err;
                self.transfer('edit');
            } else {
                self.redirect({ controller: self.name });
            }
        });
    };


    this.deleteUsers = function (req, resp, params) {
        var self = this;
         DBUtils.deleteObject(self,ModelName,params);    

        var i,ids,idCounter;
        ids=params.ids;
        idCounter=0;
        console.log("assaas");
		for (i = 0; i < ids.length; ++i)
		{
			idCounter++;
			 console.log("id : " + ids[i]);
			geddy.model[ModelName].first(ids[i], function(err, object) {
				console.log(object);
                if (err) {
					throw err;
				}
				if (!object) {
					throw new geddy.errors.BadRequestError();
				}
				else {
                    console.log(111+object.linkedId);
                   var searchObject = {"managerId": object.linkedId};// mongo search object
                   var repCollection = geddy.globalDb.collection('reps');
                   console.log(2);
                   geddy.model.Rep.all(searchObject, {sort: {}}, function (err, allReps) {
                         if (err) {
                            throw err;
                        }
                        if (!object) {
                            throw new geddy.errors.BadRequestError();
                        }
                        console.log(allReps.length);
                        var j;
                        //console.log(allReps);
                            for(j=0;j<allReps.length;j++){
                                console.log("id ------------------------------------ j: " + j);
                                allReps[j].updateAttributes({managerId:null});
                                allReps[j].save(function (err, data) {
                                    if (err) {
                                            throw err;
                                        }
                                        if (!object) {
                                            throw new geddy.errors.BadRequestError();
                                        }
                                        else {
                                            console.log("-------Updating dependency------------------");
                                        }
                                });      
                            }

                        });
				}
			});
        }
}


    this.savePhoto = function(req, resp, params) {
        var self = this;
        var user;

        geddy.model.User.first(params.userId, function (err, userData) {
            if (err)
            {
                throw err;
            } else {

                userData.photo = params.fileFullName;

                 userData.save(function (err, data) {
                    if (err) {
                        throw err;
                    } else {
                        self.respond(true, {
                          format: 'json'
                        });
                    }
                });
            }
        });
    };


    this.validPassword = function(password){
        var result = false;
        var regex = new RegExp("^((?=.*\\d)(?=.*[A-Z])(?=.*[:;\"'<>,.`~!@#$%^&*()_=\\-\\+\\{\\}\\[\\]\\|\\?\\/])(.{6,}))$");
        //console.log(">>>>>>>>>>> VALIATE: " + password);
        result = regex.test(password);
        return result;
    };


  this.search = function (req, resp, params) {
    var $thisRequest = this;
    
    var searchObject = {};
    var sort = { programName: 'asc' };

    search.AdvanceSearch($thisRequest, searchObject, sort, 'users', params);
  }
};


exports.Users = Users;
