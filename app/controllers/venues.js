var constants = require('../helpers/utils/constants.js');
var DBUtils = require('../helpers/utils/DBUtils.js');
var ModelName= "Venue";

var Venues = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

   this.before(function () { 
      var self = this;
      if(typeof self.session.get('userRoleId')=='undefined' || !self.session.get('userRoleId'))
      {
       geddy.globalDb.close();
        self.respond('false',{format: 'txt'});
      }
      else
      {
        self.session.set('activeController', self.name);
      }
    });

  this.index = function (req, resp, params) {
    var self = this;

    DBUtils.unlockObject(self);

    var result={};
    var allVenues = [];
 
    var currentClient = self.session.get('currentClient');

    self.session.set('currentSection', constants.sections.CLIENT_VENUES);

    result.searchVenueProperties = constants.SEARCH_VENUE_PROPERTIES();

    var sort = {
              venueName: 'asc'
      }

    var venueCollection = geddy.globalDb.collection('venues');
    var pageLimit = geddy.config.pageLimit;
    //var searchObject = {};
    var searchObject = {id: {$in: currentClient.venues}};
    var currentPage = 1;
    var recordCount=0;
    var totalPages=0;

    result.currentPage = currentPage;

    venueCollection.find().toArray(function(err,allVenues){
        if (allVenues != null)
        {
          self.session.set('allVenues', allVenues);
          result.allVenues = allVenues;
        } else {
          result.allVenues = [];
          self.session.set('allVenues', []);
        }

        venueCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,clientVenues){
          if (clientVenues)
          {
            recordCount = clientVenues.length;
            totalPages = Math.ceil(recordCount/pageLimit);

            venueCollection.find(searchObject,{limit: pageLimit, skip: 0 }).sort(sort).toArray(function(err,venues){
              if (venues)
              {
                result.venues = venues;
              } else {
                result.venues = [];
              }

              result.totalPages = totalPages;
              result.currentClient = currentClient;
              self.respond(result);
            });
          } else {
            result.venues = [];
            result.totalPages = totalPages;
            result.currentClient = currentClient;
            self.respond(result);
          }
        });
    });
  }

  
  this.show = function (req, resp, params) {
    var self = this;
    var continueProcess = true;
    DBUtils.unlockObject(self);

    var currentVenue = self.session.get('currentVenue');

    if (params.id == '1234')
    {
      if (currentVenue)
      {
        params.id = currentVenue.id;
      } else {
        continueProcess = false;
        self.respond('null',{format: 'txt'});
      }
    }

    if (continueProcess)
    {
      var resultObject = new Object();
      resultObject ['hours'] = getHoursOfOperation(currentVenue);
      resultObject ['parking'] = getParkingCosts(currentVenue);
      resultObject ['roomsMaxCapacities'] = getRoomsMaxCapacity(currentVenue);
      resultObject ['roomsAVCosts'] = getRoomsAVCosts(currentVenue);
      resultObject ['daysOfWeek'] = constants.DAYS_OF_WEEK();
      resultObject ['parkingOptions'] = constants.PARKING_OPTIONS();
      resultObject ['roomCapacityOptions'] = constants.VENUE_ROOM_CAPACITY_OPTIONS();
      resultObject ['roomAVCostOptions'] = constants.VENUE_ROOM_AVCOSTS_OPTIONS();
      resultObject ['currentClient'] = self.session.get('currentClient');
      resultObject ['selectPrompt'] = constants.SELECT_PROMPT();

      DBUtils.showObject(self,ModelName,params,constants.sections.CLIENT_VENUES, resultObject);
    }
  };


  this.save = function (req, resp, params) {
    var self = this;

    DBUtils.saveObject(self,ModelName,params);

    var currentClient = self.session.get("currentClient");
    console.log (" currentclient name " + currentClient.name);
    var currentVenue = self.session.get("currentVenue");
    console.log (" currentvenue name " + currentVenue.venueName);

    DBUtils.saveDataTo(self, 'Client', currentClient.id, 'venues', currentVenue.id);
  }



  this.unsetCurrentVenue = function (req, resp, params) {
    var self = this;
    console.log(">>>> setting current venue to null ");
    self.session.set("currentVenue",null);
    self.respond(true, {
          format: 'json'
    });
  }

  this.setCurrentVenueById = function (req, resp, params) {
    var self = this;
    if (params.venueId == null || params.venueId == '')
    {
      self.session.set("currentVenue",null);
      console.log(">>>> setting current venue to "+ null);
      self.respond(true, {
            format: 'json'
      });
    }
    else
    {
      geddy.model.Venue.first(params.venueId, function(err, venue) {
        if (err) {
        }
        else
        {
          if(venue)
          {
            self.session.set("currentVenue",venue);
          }
          console.log(">>>> setting current venue to "+ venue.venueName);
        }
        self.respond(true, {
              format: 'json'
        });
      });
    }
  }


  this.saveEmbeddedModel = function (req, resp, params) {
    var self = this;
    DBUtils.saveObjectEmbeddedModel(self,ModelName,params);

    var currentClient = self.session.get("currentClient");
    var currentVenue = self.session.get("currentVenue");

    DBUtils.saveDataTo(self, 'Client', currentClient.id, 'venues', currentVenue.id);
  }


  this.deleteEmbeddedModel = function (req, resp, params) {
    var self = this;
      DBUtils.deleteEmbeddedModel(self,ModelName,params);
  };


  this.deleteVenues = function (req, resp, params) {
    var self = this;
    DBUtils.deleteObject(self,ModelName,params);
  };

  this.deleteVenueFromOthers = function (req, resp, params) {
    var self = this;
    //delete the venueId from clients
    DBUtils.deleteFromArrayByValue(self, params.ids, self.session.get('currentClient'), 'venues');


    //delete the venueId from events
    var i;
    for (i = 0; i < params.ids.length; i++) 
    {
      //console.log("Deleting from event looking for "+ params.ids[i])
      geddy.model.Event.all({venueId: params.ids[i]}, function(err, events) {
        if (err) {
          throw err;
        }
        var j;
        for (j = 0; j < events.length; j++) 
        {
          //console.log ("EVENT TO DELETE venueid found "+events[j].brand);
          events[j].venueId = null;
          events[j].save(function(err, data) {
            if (err) {
              throw err;
            }
          });    
        }
      });
    }
  }

  this.clearCurrentVenue = function (req, resp, params) {
    var self = this;
    DBUtils.clearCurrentObject(self,ModelName,params);
   };


  this.addVenueToClient = function (req, resp, params) {
    var self = this;
    var currentClient = self.session.get("currentClient");

    DBUtils.saveDataTo(self, 'Client', currentClient.id, 'venues', params.id);

    self.respond(true, {format:'json'});
  };


  function getHoursOfOperation(currentVenue)
  {
    //get hours in array format so we can use to display on the view
    var hours = [];
    if (currentVenue && currentVenue.hours)
    {
      hours = currentVenue.hours.split(',');
    }

    return hours;
  }

  function getParkingCosts(currentVenue)
  {
    //get parking in array format so we can use to display on the view
    var parking = [];
    if (currentVenue && currentVenue.parking)
    {
      parking = currentVenue.parking.split(',');
    }

    return parking;
  }

  function getRoomsMaxCapacity(currentVenue) 
  {
    var roomsMaxCapacities = [];
    if (currentVenue && currentVenue.rooms)
    {
      for (var i = 0; i < currentVenue.rooms.length; i++) {
        if (currentVenue.rooms[i].maxCapacity){
          roomsMaxCapacities.push(currentVenue.rooms[i].maxCapacity.split(','));  
        } else {
          roomsMaxCapacities.push([]);
        }
        
      };
    } 
    return roomsMaxCapacities;
  }

  function getRoomsAVCosts(currentVenue)
  {
    var roomsAVCosts = [];
    if (currentVenue && currentVenue.rooms)
    {
      for (var i = 0; i < currentVenue.rooms.length; i++) {
        if (currentVenue.rooms[i].avCosts)
        {
          roomsAVCosts.push(currentVenue.rooms[i].avCosts.split(','));
        } else {
          roomsAVCosts.push([]);
        }
      };
    }

    return roomsAVCosts;
  }

  function getRoomsMaxCapacityByRoom(currentRoom) 
  {
    var roomsMaxCapacities = [];
    if (currentRoom)
    {
      if (currentRoom.maxCapacity){
        roomsMaxCapacities = currentRoom.maxCapacity.split(',');  
      } else {
        roomsMaxCapacities.push([]);
      }
    } 
    //console.log(" >>> ROM MAX CAPACITIES "+ JSON.stringify(roomsMaxCapacities));
    return roomsMaxCapacities;
  }

  function getRoomsAVCostsByRoom(currentRoom)
  {
    var roomsAVCosts = [];
    if (currentRoom)
    {
      if (currentRoom.avCosts)
      {
        roomsAVCosts = currentRoom.avCosts.split(',');
      } else {
        roomsAVCosts.push([]);
      }
    }
    console.log(" >>> ROM AV COSTS "+ JSON.stringify(roomsAVCosts));
    return roomsAVCosts;
  }

  // Venue Step pages
  this.venueStep1 = function (req, resp, params) {
    var self = this;

    var venueTypes = constants.VENUE_TYPES();
    var selectPrompt = constants.SELECT_PROMPT();
    var states = constants.STATES();
    var countries = constants.COUNTRIES();
    var phoneTypes = constants.VENUE_PHONE_NUMBER_TYPES();
    var foodTypes = constants.FOOD_TYPES();

    var currentVenue = self.session.get('currentVenue');

    if (currentVenue && currentVenue.id)
      {
        DBUtils.lockObject(self, currentVenue.id);
      }

    self.respond( {
      stepTitle: createTitle(self),
      selectPrompt: selectPrompt,
      venueTypes: venueTypes,
      phoneTypes: phoneTypes,
      foodTypes: foodTypes,
      countries: countries,
      states: states,
      currentClient: self.session.get('currentClient'),
      currentVenue: currentVenue},
      {format: 'html',template: 'app/views/venues/venueStep1'}
    );
  };

  this.venueStep2 = function (req, resp, params) {
    var self = this;

    var currentVenue = self.session.get('currentVenue');
    var daysOfWeek = constants.DAYS_OF_WEEK();
    var parkingOptions = constants.PARKING_OPTIONS();

    //get hours in array format so we can use to display on the view
    var hours = getHoursOfOperation(currentVenue);

    //get parking in array format so we can use to display on the view
    var parking = getParkingCosts(currentVenue);

    self.respond( {
      stepTitle: createTitle(self),
      daysOfWeek: daysOfWeek,
      parkingOptions: parkingOptions,
      hours: hours,
      parking: parking,
      currentClient: self.session.get('currentClient'),
      currentVenue: currentVenue},
      {format: 'html',template: 'app/views/venues/venueStep2'}
    );
  };

  this.venueStep3 = function (req, resp, params) {
    var self = this;

    var currentVenue = self.session.get('currentVenue');
    /*var selectPrompt = constants.SELECT_PROMPT();
    var roomTypes = constants.VENUE_ROOM_TYPES();
    var roomCapacityOptions = constants.VENUE_ROOM_CAPACITY_OPTIONS();
    var roomAVCostOptions = constants.VENUE_ROOM_AVCOSTS_OPTIONS();

    var roomMaxCapacities = getRoomsMaxCapacity(currentVenue);
    
    var roomAVCosts = getRoomsAVCosts(currentVenue);
    */


    self.respond( {
      stepTitle: createTitle(self),
      /*selectPrompt: selectPrompt,
      roomTypes: roomTypes,
      roomCapacityOptions: roomCapacityOptions,
      roomAVCostOptions: roomAVCostOptions,
      roomMaxCapacities: roomMaxCapacities,
      roomAVCosts: roomAVCosts,*/
      currentClient: self.session.get('currentClient'),
      currentVenue: currentVenue},
      {format: 'html',template: 'app/views/venues/venueStep3'}
    );
  }

  this.venueRoomStep = function (req, resp, params) {
    var self = this;
    var currentRoom = null;

    var currentVenue = self.session.get('currentVenue');

    if (params.index)
    {
      currentRoom = currentVenue.rooms[params.index];
    }

    var result = {};
    result.selectPrompt = constants.SELECT_PROMPT();
    result.roomTypes = constants.VENUE_ROOM_TYPES();
    result.roomCapacityOptions = constants.VENUE_ROOM_CAPACITY_OPTIONS();
    result.roomAVCostOptions = constants.VENUE_ROOM_AVCOSTS_OPTIONS();
    result.roomMaxCapacities = getRoomsMaxCapacityByRoom(currentRoom);
    result.roomAVCosts = getRoomsAVCostsByRoom(currentRoom);
    result.stepTitle = createTitle(self);
    result.currentRoom = currentRoom;

    
    self.respond( result,{format: 'html',template: 'app/views/venues/venueRoomStep'}
    );
  }


  this.venueStep4 = function (req, resp, params) {
    var self = this;

    var selectPrompt = constants.SELECT_PROMPT();
    var phoneTypes = constants.VENUE_PHONE_NUMBER_TYPES();

    self.respond( {
      stepTitle: createTitle(self),
      selectPrompt: selectPrompt,
      phoneTypes: phoneTypes,
      currentClient: self.session.get('currentClient'),
      currentVenue: self.session.get('currentVenue')},
      {format: 'html',template: 'app/views/venues/venueStep4'}
    );
  };

  this.search = function (req, resp, params) {
    var $thisRequest = this;
    var search = require('../helpers/search/search.js');
    var currentClient = $thisRequest.session.get('currentClient');
    var searchObject = {id: {$in: currentClient.venues}};
    var sort = {
      venueName: 'asc'
    }
    
    search.AdvanceSearch($thisRequest, searchObject, sort, 'venues', params);
  }

  function createTitle (self) {
   var headerTitle = "";
   var currentVenue = self.session.get("currentVenue");
   if(currentVenue) {
      if(currentVenue.venueName){
         headerTitle = currentVenue.venueName + ' - ';
      }
   } else {   
      headerTitle = 'New Venue - '
   }

   return headerTitle;
}
};

exports.Venues = Venues;