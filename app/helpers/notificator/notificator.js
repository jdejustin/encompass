var Notificator = new (function () {

    this.sendNotification = function sendNotification(args) {
        var api_key = 'key-0ec2fda357887c7aec58d91bc6f0f239';
        var domain = 'encompassmanager.com';
        var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

        var emailTemplate = this.getEmailTemplate(args.type);
        if (emailTemplate) {
            var regex = /\{\$(.*)\}/;
            var match = regex.exec(emailTemplate);
            var keys = [];

       //     args.baseURL = geddy.config.baseURL + (geddy.config.port != 80 ? (":" + geddy.config.port) : "");
        args.baseURL = 'encompassmanager.com';
            for (var k in args) keys.push(k);
            
            for (var i = 0; i < keys.length; i++) {
                var param = keys[i];
                var regexParams = new RegExp("\\{\\$"+param+"\\}","g");
                emailTemplate = emailTemplate.replace(regexParams, args[param]);
            }

            var mailOptions = {
                from: args.from,
                to: args.to,
                subject: geddy.config.EmailSubjectPrefix + args.subject,
                html: emailTemplate
            };

            console.log('Sending email to MailGun');
            
            mailgun.messages().send(mailOptions, function (error, body) {
              console.log(body);
            });

        } else {
            return false;
        }
    };

    this.getEmailTemplate = function getEmailTemplate(type) {
        var fs = require('fs');
        var path = require('path');
        var filePath = path.join(geddy.config.EmailTemplatesFolder + type + '.html');
        var data = fs.readFileSync(filePath, { encoding: 'utf-8' });
        return data;
    }; 
});

module.exports = Notificator;
