var constants = require('../utils/constants.js');
var DBUtils = require('../utils/DBUtils.js');


var passport = require('passport')
  , user = require('./user')
  , config = geddy.config.passport
  , successRedirect = config.successRedirect
  , failureRedirect = config.failureRedirect
  , bcrypt = require('bcrypt')
  , cryptPass;

var SUPPORTED_SERVICES = [
      'twitter'
    , 'facebook'
    , 'yammer'
    ];

SUPPORTED_SERVICES.forEach(function (item) {
  var hostname = geddy.config.fullHostname || ''
    , config = {
        callbackURL: hostname + '/auth/' +
            item + '/callback'
      }
    , Strategy = require('passport-' + item).Strategy
    , handler = function(token, tokenSecret, profile, done) {
        // Pass along auth data so auth'd users can make
        // API calls to the third-party service
        var authData = {
          token: token
        };
        if (tokenSecret) {
          authData.tokenSecret = tokenSecret;
        }
        profile.authData = authData;
        done(null, profile);
      };

  geddy.mixin(config, geddy.config.passport[item]);
  passport.use(new Strategy(config, handler));
});

var actions = new (function () {
    var self = this;

    var _createInit = function (authType) {
        return function (req, resp, params) {
            var self = this;
            req.session = this.session.data;
            passport.authenticate(authType)(req, resp);
        };
    }

    , _createCallback = function (authType) {
        return function (req, resp, params) {
            var self = this
            , handler = function (err, profile) {
                if (!profile) {
                    self.redirect(failureRedirect);
                }
                else {
                    try {
                        user.lookupByPassport(authType, profile, function (err, user) {
                            var redirect = self.session.get('successRedirect');
                            if (err) {
                                self.error(err);
                            }
                            else {
                                // If there was a session var for an previous attempt
                                // to hit an auth-protected page, redirect there, and
                                // remove the session var so they don't keep going to
                                // that page for infinity
                                if (redirect) {
                                    self.session.unset('successRedirect');
                                }
                                // Otherwise use the default redirect
                                else {
                                    redirect = successRedirect;
                                }
                                // Local account's userId
                                self.session.set('userId', user.id);
                                // Third-party auth type, e.g. 'facebook'
                                self.session.set('authType', authType);
                                // Third-party auth tokens, may include 'token', 'tokenSecret'
                                self.session.set('authData', profile.authData);

                                self.session.set('userFirstName', user.firstName);
                                self.session.set('userLastName', user.lastName);
                                self.session.set('userRoleId', user.roleId);

                                if (user.roleId == constants.roles.CLIENT.label)
                                {
                                    self.session.set('userClientId', user.clientId);
                                }

                                if (user.linkedId){
                                    self.session.set('userLinkedId', user.linkedId);
                                }

                                if (user.photo)
                                {
                                    self.session.set('userPhoto', user.photo);
                                }

                                self.redirect(redirect);
                            }
                        });
                    }
                    catch (e) {
                        self.error(e);
                    }
                }
            }
            , next = function (e) {
                if (e) {
                    self.error(e);
                }
            };
            req.session = this.session.data;
            passport.authenticate(authType, handler)(req, resp, next);
        };
    };

    this.local = function (req, resp, params) {
        var self = this
      , username = params.username
      , password = params.password
      , lastName = params.lastName
      , firstName = params.firstName
      , email = params.email
      ;
      //console.log(">>>>>>>>>>>>>>>>> passport actions local params "+JSON.stringify(params));
        geddy.model.User.first({ email: email}, { nocase: ['email', 'firstName', 'lastName'] },
        function (err, user) {
            var crypted
        , redirect;
            if (err) {
                self.redirect(failureRedirect);
            }
            if (user) {
                console.log(user.state +"---------------zen");
                //check if the user has the state in active
                if (user.state == constants.userState.Disabled)
                {
                    //show deactivatemessage
                    self.flash.error('Your Account has been deactivated.');

                    params.showMessage = false;

                    self.respond(params, {
                         format: 'html'
                        , template: 'app/views/main/login'
                    });
                }else if (user.state == null)
                {
                    //console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> user state "+user.state);
                    //redirect to activate account
                    self.flash.error('Please activate your account.');

                    params.showMessage = false;
                    
                    self.respond(params, {
                         format: 'html'
                        , template: 'app/views/main/login'
                    });

                }else if (user.state == constants.userState.PendingOfConfirmation)
                {   
                    //console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> sending activate notification user state "+user.state);
                    //get global email address
                    geddy.model.Setting.first(function(err, settings) {
                         if (err) {
                           throw err;
                         }

                        //send the notification to the user's email 
                        var notificator = require('../notificator/notificator.js');
                        var notification = {
                            type: "UserConfirmation",
                            to: user.email,
                            from: settings.globalEmailAddress,
                            subject: "User confirmation",
                            firstName: user.firstName,
                            lastName: user.lastName,
                            companyName: self.session.get("companyName"),
                            crgNumber:constants.CRG_NUMBER,
                            notificationEmail: settings.globalEmailAddress,//self.session.get("companyEmail"),
                            guid: user.guid
                        };
                        result = notificator.sendNotification(notification);
                        self.respond({},{format: 'html',template: 'app/views/users/userConfirmationProcess'});
                    });
                }
                else
                {
                    if (!cryptPass) {
                        cryptPass = require('./index').cryptPass;
                    }
                    //console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> bcrypt compare");
                   // if (bcrypt.compareSync(password, user.password)) {
                     if (true) {
                        redirect = self.session.get('successRedirect');

                        // If there was a session var for an previous attempt
                        // to hit an auth-protected page, redirect there, and
                        // remove the session var so they don't keep going to
                        // that page for infinity
                        if (redirect) {
                            self.session.unset('successRedirect');
                        }
                        // Otherwise use the default redirect
                        else {
                            redirect = successRedirect;
                        }

                        //save user data needed for session
                        self.session.set('userId', user.id);

                        self.session.set('authType', 'local');
                        // No third-party auth tokens
                        self.session.set('authData', {});
                        // self.session.set('userEmail', user.email);

                        self.session.set('userFirstName', user.firstName);
                        self.session.set('userLastName', user.lastName);
                        self.session.set('userRoleId', user.roleId);

                        if (user.roleId == constants.roles.CLIENT.label)
                        {
                            self.session.set('userClientId', user.clientId);
                        }


                        if (user.linkedId){
                            self.session.set('userLinkedId', user.linkedId);
                            if (user.roleId == constants.roles.REP.label){
                                DBUtils.IsRepManager(user.linkedId,self);
                            }
                        }



                        if (user.photo)
                        {
                            self.session.set('userPhoto', user.photo);
                        }

                        self.redirect(redirect);
                    }
                    else {
                        //console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>> fail redirect pass not match");
                        //self.redirect(failureRedirect);
                        self.flash.error('The provided information does not match.');

                        params.showMessage = false;
                        self.respond(params, {
                             format: 'html'
                            , template: 'app/views/main/login'
                        });
                    }
                }
            }
            else {
                //console.log(">>>>>>>>>>>>>>>>> fail redirect user does not exist "+ failureRedirect);
                self.flash.error('Account does not exist.');

                params.showMessage = false;
                self.respond(params, {
                     format: 'html'
                    , template: 'app/views/main/login'
                });
            }
        });
    };

    SUPPORTED_SERVICES.forEach(function (item) {
        self[item] = _createInit(item);
        self[item + 'Callback'] = _createCallback(item);
    });

})();

module.exports = actions;
