var page = require('webpage').create();
var system = require('system')

if (system.args.length === 1) {
  console.log('Usage: test1.js <some URL>');
  phantom.exit();
}

var htmlfilename=system.args[1];
var pdffilename=htmlfilename.replace(".html",".pdf");

page.open(htmlfilename, function() {
page.paperSize ={
  format: 'A4',
  orientation: 'portrait',
  border: '1cm'
};

  page.render(pdffilename);
  phantom.exit();
});
