var HTML2PDF = new (function () {

    this.convertMultipleFiles = function convertSync(filesToConvert,callback,args) {
    	var convertedFiles = [];
		var htmlfilename = "";

    	for(var i=0; i < filesToConvert.length; i++){
			htmlfilename = filesToConvert[i];
			convertedFiles.push(htmlfilename.replace(/\.html$/,".pdf"));
    		this.processFile(htmlfilename);
    	}
		this.checkIfDone(convertedFiles,callback,args);
    }


    this.checkIfDone = function(convertedFiles,callback,args){
		var fs = require('fs');
		var result = true;
		var filePath = "";
    	for(var i=0; i < convertedFiles.length; i++){
			filePath = convertedFiles[i];
			console.log(" >>>> checkIfDone "+ filePath );
			result = result && fs.existsSync(filePath);
		}

		if (!result){
			setTimeout(this.checkIfDone, 5000,convertedFiles,callback,args); 
		}else{
			callback(convertedFiles,args);
		}
    }

    this.convert = function convert(htmlfilename) {
    	this.processFile(htmlfilename);
    };

    this.processFile = function processFile(htmlfilename){
		var path = require('path')
		var childProcess = require('child_process')
		var phantomjs = require('phantomjs')
		var binPath = phantomjs.path

		var childArgs = [
		  path.join(__dirname, 'convert.js'), htmlfilename
		]


		console.log(">>>>>>>html2pdf>>>>>>>>"+JSON.stringify(childArgs));
		console.log(">>>>>>>html2pdf>>>>>>>>"+JSON.stringify(childArgs));

		childProcess.execFile(binPath, childArgs, function(err, stdout, stderr) {
		  // handle results
		  if (err){
		  	console.log(err);
		  }
		  if (stdout){
		  	console.log(stdout);
		  }
		  if(stderr){
		  	console.log(stderr);
		  }
		});
    }
});

module.exports = HTML2PDF; 







