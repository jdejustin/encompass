﻿var Search = new (function () {
    this.AdvanceSearch = function ($thisRequest, searchObject, sort, objectName, params, callBack, zips) {

        objectCollection = geddy.globalDb.collection(objectName);

        //set the params for pagination
        var pageLimit = geddy.config.pageLimit;
        var currentPage = params.currentPage;
        var skip = currentPage == 1 ? 0 : ((currentPage-1) * pageLimit);
        var recordCount = 0;
        //console.log("Object Name: "+objectName);
        console.log("Params: "+JSON.stringify(params));
        
        if (searchObject == null)
        {
            searchObject = new Object();
        }

        var conditionsArray = [];
        var keys = [];
        var condition = new Object();

        //paramsList
        for (var key in params) {
            keys.push(key);
        }

        for (var i = 0; i < keys.length; i++) {
            if (keys[i].match(/(method|controller|action|currentPage|selectedClientId)/)) {
                continue;
            }
            var filters = "";
            if (Array.isArray(params[keys[i]])) {
                //To get OR filters
                filters = params[keys[i]].join("|");
                console.log("      >>>>>    IS ARRAY   ");
            } else filters = params[keys[i]];
            console.log("      >>>>>       filters->" + filters);
            var newRegex = new RegExp(".*(" + filters + ").*", "i");
            console.log (' >>>>>>>>>>>>>>>>>  REG EXP ' + newRegex);
            condition[keys[i]] = newRegex;
        }

        conditionsArray.push(condition);
        searchObject['$or'] = conditionsArray;
        console.log("<><<<<<<<<<<<<<<<<<< Search OBJECT Keys " + JSON.stringify(keys));
        console.log("<><<<<<<<<<<<<<<<<<< Search OBJECT conditions " + JSON.stringify(conditionsArray));
        console.log("<><<<<<<<<<<<<<<<<<< Search OBJECT conditions Array " + JSON.stringify(searchObject));

        var result = new Object();
        result.currentPage = currentPage;
        result.recordCount = 0;
        result.totalPages = 0;

        if (zips)
        {
            searchObject['addresses.postalCode'] = {$in: zips};
        }

        //objectCollection.find(searchObject).toArray(function (err, elementList) {
        //objectCollection.count({},function (err, count) {
            objectCollection.find(searchObject).count(function (err, count) {
            console.log("ELEMENT LIST >>>> "+JSON.stringify(count));
            if (count != null)
            {
                //result.recordCount = elementList.length;
                result.recordCount = count;
                result.totalPages = Math.ceil(result.recordCount/pageLimit);
            }

            console.log("current page: "+currentPage);
            console.log("recordCOunt: "+result.recordCount);
            console.log("pageLimit: "+pageLimit);
            console.log("skip: "+skip);

            objectCollection.find(searchObject,{limit: pageLimit, skip: skip}).sort(sort).toArray(function(err,list){
                result.dataset = list;

                if (callBack)
                {
                    callBack($thisRequest, result);
                } else 
                {
                    $thisRequest.respond(result, { format: 'json' });
                }
            });

            // result.dataset = elementList.splice(skip, pageLimit);
        });
    }
});

module.exports = Search; 