var constants = require('./constants.js');
var fs = require('fs');


var DBUtils = new (function () {

	this.showObject = function (self,ObjectName,params,section, otherParams, callBack ) {
		var dbutils = this;
		var currentObjectTag = "current"+ObjectName;
		self.session.set('currentSection',section);

		//console.log(">>>>>>>>>>>>>>>>>>>>>>>> Show OBJECT params"+ JSON.stringify(params));
		//var selectPrompt = constants.SELECT_PROMPT();
		geddy.model[ObjectName].first(params.id, function(err, object) {
			if (err) {
				throw err;
			}
			if (!object) {
				throw new geddy.errors.NotFoundError();
			}
			else {
				//console.log(' session setting '+currentObjectTag);
				self.session.set(currentObjectTag,object);

				var result;

				if (otherParams)
				{
					result = otherParams;
				} else {
					result = {};
				}

				//find object in collection, if there, then no, it's not editable
				geddy.model.Lock.first({objectId:object.id, editorId: {ne: self.session.get('userId')}}, function(err, lock){
					if (err)
					{
						throw err;
					}

					if (lock)
					{
						// //check lock, in case owner left it locked and their session has timed out
						// var timeDiff = new Date().getTime() - lock.editTime.getTime();
					 //   	timeDiff = (timeDiff/1000/60) << 0;

						// if (timeDiff >  0.5)//constants.LOCK_MAX_TIME)
						// {
						// 	dbutils.unlockObject(self, lock.id);
						// 	result.editing = false;
						// 	result.editor = null;
						// } else {
							result.editing = true;
							result.editor = lock.editorFirstName + ' ' + lock.editorLastName;
						// }
					} else {
						result.editing = false;
						result.editor = null;
					}


					result[currentObjectTag] = object;

					if (callBack)
					{
						callBack();
					} else {
						self.respond(result);
					}
				});
			}
		});
	};

	this.saveObject = function(self,ObjectName,params,otherParams, callBack){
	    var currentObject;

	    if (params){
	    	currentObject = updateObject(self,ObjectName,params);
	    } else {
	    	currentObject = self.session.get( "current"+ObjectName);
	    }

	    //save object
		currentObject.save(function(err, data) {
			if (err) {
				throw err;
			}

			var result;

			if (otherParams)
			{
				result = otherParams;
			} else {
				result = {};
			}

			var returnTag = ObjectName.toLowerCase()+"Id";
			result[returnTag] = data.id;

			if (callBack)
			{
				callBack();
			} else {
				self.respond(result,{format:'json'});
			}
			
		});
	};

	this.saveObjectWithoutRespond = function(self,ObjectName,params,callback){
		console.log(" >>>>>>>>>saveObjectWithoutRespond "+ ObjectName +" PARAMS<<<<<<<<<<<<"+JSON.stringify(params));
	    var currentObject = updateObject(self,ObjectName,params);
	    //save object
	    //vea  voy a probar var tempSelf = self;
	    //si no hago la linea anterior 
		currentObject.save(function(err, data) {
			if (err) {
				throw err;
			}
			var result = {};
			var returnTag = ObjectName.toLowerCase()+"Id";
			result[returnTag] = data.id;
			// console.log ('>>>>>>>>>>>>>>>>>finish save saveObjectWithoutRespond: ');
			self.session.set('savedId', data.id);
			callback(result, self, params);
		});
	};



	function updateObject(self,ObjectName,params)
	{
		var currentObjectTag = "current"+ObjectName;
		var currentObject = null;
		console.log(" >>>>>>>>>SAVE "+ ObjectName +" PARAMS<<<<<<<<<<<<"+JSON.stringify(params));
		if (!self.session.get(currentObjectTag)) 
		{
			//console.log('creating new model');
	    	//create object
	    	currentObject = geddy.model[ObjectName].create(params);
	    	self.session.set(currentObjectTag,currentObject);
	    }
	    else
    	{
    		//console.log('updating existing model');
	    	currentObject = self.session.get(currentObjectTag);

	    	if (params)
	    	{
	    		currentObject.updateProperties(params);
	    	}
	    }
	    
	    return currentObject;
	}


	function saveObjectSubModel (self,ObjectName,params,propertyName,propertyValue)
	{
		//console.log(" >>>>>>>>>SAVE OBJECT SUBMODEL PARAMS<<<<<<<<<<<<"+JSON.stringify(params));
		//console.log(" >>>>>>>>>SAVE OBJECT SUBMODEL OBJECT NAME<<<<<<<<<<<<"+ObjectName);
		//console.log(" >>>>>>>>>SAVE OBJECT SUBMODEL PROPERTYNAME<<<<<<<<<<<<"+propertyName);
		////console.log(" >>>>>>>>>SAVE OBJECT SUBMODEL PROPERTYVALUE<<<<<<<<<<<<"+propertyValue);
		var currentObjectTag = "current"+ObjectName;
		var currentObject = self.session.get(currentObjectTag);

		if(!currentObject[params.targetProperty][params.targetIndex])
		{
			currentObject[params.targetProperty].push(geddy.model[params.modelName].create());
		}
		
		//does subTargetProperty exist?
		if (!currentObject[params.targetProperty][params.targetIndex][params.subTargetProperty])
		{
			currentObject[params.targetProperty][params.targetIndex][params.subTargetProperty] = [];
		}

		var tmpSubModel;
		if( parseInt(params.subTargetIndex) > currentObject[params.targetProperty][params.targetIndex][params.subTargetProperty].length-1)
		{
			tmpSubModel = geddy.model[params.subModelName].create();
		}
		else
		{
			tmpSubModel = currentObject[params.targetProperty][params.targetIndex][params.subTargetProperty][params.subTargetIndex];
		}

		tmpSubModel[propertyName] = propertyValue;
		currentObject[params.targetProperty][params.targetIndex][params.subTargetProperty][params.subTargetIndex] = tmpSubModel;
	};

	this.saveObjectEmbeddedModel = function(self,ObjectName,params,callback){

		var currentObjectTag = "current"+ObjectName;
		var currentObject = self.session.get(currentObjectTag);

		//console.log(" >>>>>>>>> current Object "+JSON.stringify(currentObject));
		//console.log(" >>>>>>>>>SAVE EMBEDDED "+ObjectName+" PARAMS<<<<<<<<<<<<"+JSON.stringify(params));
		if (!currentObject) 
		{
			//create model first
			currentObject = geddy.model[ObjectName].create();
			self.session.set(currentObjectTag,currentObject);
		}

		//does targetProperty exist?
		if (!currentObject[params.targetProperty])
		{
			currentObject[params.targetProperty] = [];
		}

		//does embedded Model exist already? if so, we just need to update its property, otherwise, create a new one
		var tmpModel;

		var i;
		for (i = 0; i < params.value.length; ++i)
		{
			var propertyName = params.value[i].substring(0, params.value[i].indexOf("="));
			var propertyValue = params.value[i].substring(params.value[i].indexOf("=") + 1);
			propertyValue = propertyValue.trim();

			console.log ('property '+ propertyName +":"+propertyValue);

			//if saving default items, clear all of them first
			if (propertyName == 'preferred' && propertyValue == 'true')
		    {
		      var i;
		      for (i = 0; i < currentObject[params.targetProperty].length; i++) {
		        	currentObject[params.targetProperty][i].preferred = 'false';
		      };
		    }

			if (propertyValue != constants.SELECT_PROMPT())
			{
				if( parseInt(params.targetIndex) > currentObject[params.targetProperty].length-1)
				{
					//create a new embedded Model
					if(params.subModelName)
					{
						saveObjectSubModel(self, ObjectName, params,propertyName,propertyValue);
					}
					else
					{
						if (params.modelName =="")
						{
							//saving a string, only if it's not a 'preferred' property we're dealing with
							if (propertyName != 'preferred')
							{
								currentObject[params.targetProperty].push(propertyValue);
							}
						}
						else
						{
							tmpModel = geddy.model[params.modelName].create();
							tmpModel[propertyName] = propertyValue;
							currentObject[params.targetProperty].push(tmpModel);
						}
					}
				} else {
					if(params.subModelName)
					{
						saveObjectSubModel(self, ObjectName, params,propertyName,propertyValue);
					}
					else
					{
						if (params.modelName == "")
						{
							//saving a string, only if it's not a 'preferred' property we're dealing with
							if (propertyName != 'preferred')
							{
								currentObject[params.targetProperty][params.targetIndex] = propertyValue;
							}
						}
						else
						{
							tmpModel = currentObject[params.targetProperty][params.targetIndex];
							// get embedded doc from array, update its props and save back
							tmpModel[propertyName] = propertyValue;
							currentObject[params.targetProperty][params.targetIndex] = tmpModel;
						}
					}
				}
			}
		}

		//save object
		currentObject.save(function(err, data) {
			if (err) {
				throw err;
			}
			var returnTag = ObjectName.toLowerCase()+"Id";
			var result = {};
			result[returnTag] =data.id;
			if (!callback)
			{
				self.respond(result,{format:'json'});
			}
			else
			{
				callback();				
			}
		});
	};	

	this.saveDataTo = function (self, ObjectName, filter, propertyName, data){
		//console.log (">>>>>>> save data to:"+ ObjectName+ " prop:"+ propertyName+" data:"+data )
		var currentObjectTag = "current"+ObjectName;
		geddy.model[ObjectName].first(filter, function(err, object) {
          if (err) {
            throw err;
          }

          if (!object) {
            throw new geddy.errors.BadRequestError();
          }
          else {
            if (!object[propertyName])
            {
              object[propertyName] = [];
            } 

            if (object[propertyName].indexOf(data) == -1) {
              object[propertyName].push(data);
              
              object.save(function(err, data) {
              	self.session.set(currentObjectTag,object);
              });
            } 
          }
        });
	}


	this.saveAsUser = function(self, firstName, lastName, email, role, id, photo)
	{
		// console.log("firstName: " + firstName);
		// console.log("lastName: " + lastName);
		// console.log("email: " + email);
		// console.log('id: ' + id);
		// console.log('role: ' + role);

		//first check if there's already a user account for this person
		geddy.model.User.first({linkedId: id }, function (err, user) {
            if (user) {
            	//we're updating an existing account
            	user.email = email;
            	user.roleId = role;
            	user.save(function (err, data) {
                    if (err) {
                       throw err;
                    }
                });
            } else {
            	//creating a new user
            	//first check to see if we have all the needed data
				if (firstName && lastName && email)
				{
					var params = new Object();
					params.firstName = firstName;
					params.lastName = lastName;
					params.email = email;
					params.roleId = role;
					params.linkedId = id;

					if (photo)
					{
						var propertyValue = photo.substring(photo.indexOf("=") + 1);
	            		params.photo = propertyValue.substring(propertyValue.indexOf('userPhotos/') + 11);
	            	}

					var newUser = geddy.model.User.create(params);

			        geddy.model.User.first({ email: params.email }, function (err, data) {
			            if (data) {
			                params.errors = {
			                    email: 'This email is already in use.'
			                };
			                console.log('This email is already in use.');
			               //if(tupeof self != 'undefided'){
			               //    self.transfer('add');
			               //}
			            }
			            else {
			                newUser.save(function (err, data) {
			                    if (err) {
			                       throw err;
			                    }
			                });
			            }
			        });
				}
            }
	    });
	}


	this.savePhotoToUser = function(id, photo)
	{
		geddy.model.User.first({ linkedId: id }, function (err, user) {
            if (err) {
               throw err;
            }

            if (user)
            {

	            var propertyValue = photo[1].substring(photo[1].indexOf("=") + 1);
	            user.photo = propertyValue.substring(propertyValue.indexOf('userPhotos/') + 11);

	            user.save(function (err, data){
	            	if (err) {
		               throw err;
		            }
	            });
	        }
        });
	}


	this.deletePhotoFromUser = function(id)
	{
		geddy.model.User.first({ linkedId: id }, function (err, user) {
            if (err) {
               throw err;
            }

            if (user)
            {
	            user.photo = null;

	            user.save(function (err, data){
	            	if (err) {
		               throw err;
		            }
	            });
	        }
        });
	}



	this.deleteObject = function (self,ObjectName,params) {
		var ids = params.ids;
		var idCounter = 0;
		// console.log(" >>>>>>>>>DELETING "+ObjectName+" PARAMS<<<<<<<<<<<<"+JSON.stringify(params));

		var i;
		for (i = 0; i < ids.length; ++i)
		{
			idCounter++;
			// console.log("id : " + ids[i]);
			geddy.model[ObjectName].first(ids[i], function(err, object) {
				if (err) {
					throw err;
				}
				if (!object) {
					throw new geddy.errors.BadRequestError();
				}
				else {
					geddy.model[ObjectName].remove(object.id, function(err) {
						if (err) {
							throw err;
						}

						if (idCounter == ids.length)
						{
							self.respond(true, {
								format: 'json'
							});
						}
					});
				}
			});
		}
	};

	this.deleteEmbeddedModel = function (self,ObjectName,params) {
		var currentObjectTag = "current"+ObjectName;
		var currentObject = self.session.get(currentObjectTag);
		

		var currentObject = self.session.get(currentObjectTag);
		
		// check if currentObject and array exists, if so, remove element
		//console.log(" >>>>>>>>>DELETING "+ObjectName+" PARAMS<<<<<<<<<<<<"+JSON.stringify(params));
		if (currentObject && currentObject[params.targetProperty]) {
			//console.log(" >>>>>>>>>DELETING AFFILIATION PARAMS<<<<<<<<<<<<"+JSON.stringify(currentObject));
			if (parseInt(params.targetIndex) <= currentObject[params.targetProperty].length - 1) 
			{
				if(params.subTargetProperty)
				{
					//erase the subelement
					//erase the element	
					currentObject[params.targetProperty][params.targetIndex][params.subTargetProperty].splice(params.subTargetIndex, 1);
				}
				else
				{
					//erase the element	
					currentObject[params.targetProperty].splice(params.targetIndex, 1);
				}
			}
			
			//console.log(" >>>>>>>>>DELETING AFFILIATION PARAMS<<<<<<<<<<<<"+JSON.stringify(currentObject));
			//save contact
			currentObject.save(function(err, data) {
				if (err) {
					throw err;
				}
				self.respond(true, {
					format: 'json'
				});
			});
		}
	};

	this.deleteFromArrayByIndex = function(self, indices, objectName, property)
	{
		//console.log("delete from array by index. ObjectName: "+ objectName+" property: "+ property+" indices: "+JSON.stringify(indices));
		var idCounter = 0;
		var arrayCopy = [];

		var j;

		for (var i = indices.length - 1; i >=0; i--) {
			objectName[property].splice(indices[i], 1);
		};

		//save client
		objectName.save(function(err, data) {
			if (err) {
				throw err;
			}
			self.respond(true, {
				format: 'json'
			});
		});
	}

	this.deleteFromArrayByValue = function(self, ids, ObjectName, property)
	{
		//console.log("delete from array by value. indices: "+JSON.stringify(ids));
		var idCounter = 0;

		var indices = [];
		var i;
		var j;

		//build list of indices first
		for (i = 0; i < ids.length; i++) {
			console.log('looking for ' + ids[i]);
			var index = ObjectName[property].indexOf(ids[i]);
			console.log("found: " + index);
			ObjectName[property].splice(index, 1);
		}

		//save client
		ObjectName.save(function(err, data) {
			if (err) {
				throw err;
			}
			self.respond(true, {
				format: 'json'
			});
		});
	}

	//Deletes multipile files (if any) using an array of UploadedFile objects
	this.deleteUploadedFiles = function(self, ids, ObjectName, property)
	{
		//delete physical file first
		var i;
		for (i= 0; i < ids.length; i++) {
			var filePath = "private" + ObjectName[property][ids[i]].pathToFile;
			if (fs.existsSync(filePath))
			{
				fs.unlink(filePath, function (err) {
		  			if (err) throw err;
				});
			}
		};
	}

	//deletes 1 file, path to file is passed as param
	this.deleteUploadedFile = function(self, filePath)
	{
		//delete physical file first
		var filePath = "private" + filePath;
		if (fs.existsSync(filePath))
		{
			fs.unlink(filePath, function (err) {
	  			if (err) throw err;
			});
		}
	}


	this.clearCurrentObject = function (self,ObjectName,params) {
		
		var currentObjectTag = "current" + ObjectName;
		console.log (" #################### clearin " + currentObjectTag );
		self.session.set(currentObjectTag, null);

		self.respond(true, {
					format: 'json'
		});
	};	

	this.getCurrentObject = function (self, ObjectName) {
		var currentObjectTag = "current" + ObjectName;

		var currentObject = self.session.get(currentObjectTag);

		//console.log(">>>>>>>>>> DB UTILS  getCurrenObject"+ currentObject);
		self.respond({currentObject: currentObject}, {
					format: 'json'
		}); 
	}

//////////////////////////////////////////////////////////
	this.cloneObject = function (obj) {
	    // Handle the 3 simple types, and null or undefined
	    if (null == obj || "object" != typeof obj) return obj;

	    // Handle Date
	    if (obj instanceof Date) {
	        var copy = new Date();
	        copy.setTime(obj.getTime());
	        return copy;
	    }

	    // Handle Array
	    if (obj instanceof Array) {
	        var copy = [];
	        for (var i = 0, len = obj.length; i < len; i++) {
	            copy[i] = this.cloneObject(obj[i]);
	        }
	        return copy;
	    }

	    // Handle Object
	    if (obj instanceof Object) {
	        var copy = {};
	        for (var attr in obj) {
	            if (obj.hasOwnProperty(attr)) copy[attr] = this.cloneObject(obj[attr]);
	        }
	        return copy;
	    }

	    throw new Error("Unable to copy obj! Its type isn't supported.");
	}	

	this.SerializeObject = function(object){
	 	var objectToSerialize = this.cloneObject(object);
	 	delete objectToSerialize.historical;
	 	var msgpack = require('msgpack');
	    var serialized = msgpack.pack(objectToSerialize);
		var encoded = new Buffer(serialized).toString('base64');
		return encoded;
	}


	this.DeserializeObject = function(encoded){
	 	var msgpack = require('msgpack');
		var decoded = new Buffer(encoded, 'base64');
	    var result = msgpack.unpack(decoded);
		return result;
	}


//////////////////////////////

	this.changeStatus = function (objectName, objectId, newApprovalStatus, newStatus, comment, modifierId, createObjectSnapshot)
	{
		var self = this;
		
		//console.log(">>>>>>>>>>>>>>>>>>> objectName:"+objectName);
		//get the object
		geddy.model[objectName].first(objectId, function(err, object) {
          if (err) {
            throw err;
          }

          var newHistorical = geddy.model.Historical.create();
          newHistorical.date = new Date();
          newHistorical.previousState = object.approvalStatus;
          newHistorical.currentState = newApprovalStatus;
          newHistorical.comment = comment;
          newHistorical.modifier = modifierId;
          
          if(createObjectSnapshot){
              newHistorical.objectSnapshot = self.SerializeObject(object);
      	  }


          if ((typeof(object.historical) =='undefined') || !object.historical){
          		object.historical = new Array();
          }
		  object.historical.push(newHistorical);


          object.approvalStatus = newApprovalStatus;

          if (newStatus)
          {
          	object.status = newStatus;
          }

          object.save(function(err, data) {
			if (err) {
				throw err;
			}

			
			//console.log(">>>>>>>>>>>>>>>>>>> FinalObject "+JSON.stringify(object));
		});
      });
	}



////////////////////

	this.alphabetical = function (a, b)
	{
	     var A = "";
	     try{
	     	A = a.toLowerCase();
	     }catch(ex){

	     }

	     var B = "";
	     try{
	    	B=b.toLowerCase();
	 	 }catch(ex){

	 	 }


	     if (A < B){
	        return -1;
	     }else if (A > B){
	       return  1;
	     }else{
	       return 0;
	     }
	}

	this.numericCompare = function (a, b)
	{
		return b - a;
	}


	this.firstBy = (function() {
	    /* mixin for the `thenBy` property */
	    function extend(f) {
	        f.thenBy = tb;
	        return f;
	    }
	    /* adds a secondary compare function to the target function (`this` context)
	which is applied in case the first one returns 0 (equal)
	returns a new compare function, which has a `thenBy` method as well */
	    function tb(y) {
	        var x = this;
	        return extend(function(a, b) {
	            return x(a,b) || y(a,b);
	        });
	    }
	    return extend;
	})();


////////////////REP
	this.IsRepManager = function(repId,self){
		geddy.model.Rep.all({"managerId":repId},function(err,reps){
				self.session.set("IsRepManager",(reps.length > 0));
		});
	}


	this.GetMyProgramRequestEvents = function(repId,callback,self){
		geddy.model.Event.all({"requestingRepId":repId},function(err,events){
			callback(self,events);
		});
	}


	this.GetRepManagerEvents = function(managerId,callback,self){
      var MJ = require("mongo-fast-join");
      var MongoClient = require('mongodb').MongoClient;
      var mongoJoin = new MJ();
	  var connectionString = 'mongodb://'+geddy.config.db.mongo.host+':'+geddy.config.db.mongo.port+'/'+geddy.config.db.mongo.dbname;


	  if (typeof(geddy.config.db.mongo.serverOptions)){
	  	if(geddy.config.db.mongo.serverOptions.ssl){
	  		connectionString += "?ssl=true";
	  	}
	  }


	  	MongoClient.connect(connectionString, function(err, db) {
		    if(err) throw err;
			var collection1 = db.collection('reps');
		    var collection2 = db.collection('events');
			mongoJoin.query(collection1,
		        {managerId:managerId}, //query statement
		        {}, //fields
		        {
		            limit: 10000//options
		        }
		    ).join({
		        joinCollection: collection2,
		        //respects the dot notation, multiple keys can be specified in this array
		        leftKeys: ["_id"],
		        //This is the key of the document in the right hand document
		        rightKeys: ["requestingRepId"],
		        //This is the new subdocument that will be added to the result document
		        newKey: "events"
		    }).exec(function (err, items) {    //Call exec to run the compiled query and catch any errors and results, in the callback
				db.close();
				
				var sift = require('sift');
				var result = new Array();
				for (var i =0 ; i<items.length;i++){
					
					var compareArray = [];
					if ((typeof(items[i].events)!= 'undefined') && (items[i].events)){
						if(!Array.isArray(items[i].events)){
							compareArray.push(items[i].events);
						}else{
							compareArray = items[i].events;
						}
					}

					var sifted = sift({approvalStatus:{$ne:constants.NOT_SUBMITTED}}, compareArray);

					if (typeof(sifted) == 'object'){

					// sifted[0].confirmedDate = sifted[0].confirmedDate.toLocaleDateString();
					 result = result.concat(sifted);
					}
				}

				//now get all the manager events (if any)
				geddy.model.Event.all({requestingRepId: managerId}, function(err, events){
					if (err)
					{
						throw err;
					}

					result = result.concat(events);
					callback(self,result);
				});

			});
    	});			
	}

	this.GetContactEvents = function (contactId, callback, self)
	{
		var eventCollection  = self.session.get('eventCollection');

		//searching object 
		var searchObject = {"potentialSpeakers.contactId": contactId, 'potentialSpeakers.crgStatus': constants.SPEAKER_CONFIRMED.toString(), approvalStatus: constants.APPROVED};

		if (typeof eventCollection == "undefined" || !eventCollection)
		{
			self.session.set("eventCollection",geddy.globalDb.collection('events'));
			eventCollection  = self.session.get('eventCollection');
		}

		eventCollection.find(searchObject).toArray(function(err, result){
			//parse array and send only the ones that this speaker is confirmed
			var i;
			for (i = result.length - 1; i >= 0; i--) {
				var notFound = true;

				for (var j = 0; j < result[i].potentialSpeakers.length; j++) {
				 	if (result[i].potentialSpeakers[j].contactId == contactId && result[i].potentialSpeakers[j].crgStatus == constants.SPEAKER_CONFIRMED.toString())
				 	{
				 		notFound = false;
				 		break;
				 	}
				};

				//if we didn't find the speaker with a status of confirmed, then remove this event from the list
				if (notFound)
				{
					result.splice(i, 1);
				}
			};
			callback(self,result);
		});
	}


	this.GetClientEvents = function (clientId, callback, self)
	{
		var eventCollection  = self.session.get('eventCollection');

		geddy.model.Client.first(clientId, function (error, client){
			//searching object 
			var searchObject = {"programId": {$in: client.programs}, approvalStatus: {$in: [constants.APPROVED, constants.DENIED, constants.PENDING_APPROVAL]}};

			if (typeof eventCollection == "undefined" || !eventCollection)
			{
				self.session.set("eventCollection",geddy.globalDb.collection('events'));
				eventCollection  = self.session.get('eventCollection');
			}

			eventCollection.find(searchObject).toArray(function(err, result){
				callback(self,result);

				searchObject = null;
			});
		});
	}


	this.GetRepLevelFromRole = function(role, currentClient){
		var sift = require('sift');
		var sifted = null;

		sifted = sift({$eq:role}, currentClient.salesForceLabelsLevel1); 
		//console.log(">>>>>>>>>>>>>>>>>>>>>>sifted:" + JSON.stringify(sifted));
		//console.log(">>>>>>>>>>>>>>>>>>>>>>typeof sifted:" + typeof(sifted));
		if (sifted.length != 0){
			sifted = sift({label:{$eq:role}}, constants.SALES_FORCE_LABELS_LEVEL1()); 
			//console.log(">>>>>>>>>>>>>>>>>>>>>>sifted:" + JSON.stringify(sifted));
			//console.log(">>>>>>>>>>>>>>>>>>>>>>typeof sifted:" + typeof(sifted));
			if (sifted.length != 0){
				return sifted[0].level;
			}
		}

		sifted = sift({$eq:role}, currentClient.salesForceLabelsLevel2);
		if (sifted.length != 0){
			sifted = sift({label:{$eq:role}}, constants.SALES_FORCE_LABELS_LEVEL2()); 
			//console.log(">>>>>>>>>>>>>>>>>>>>>>sifted:" + JSON.stringify(sifted));
			//console.log(">>>>>>>>>>>>>>>>>>>>>>typeof sifted:" + typeof(sifted));
			if (sifted.length != 0){
				return sifted[0].level;
			}
		}

		sifted = sift({$eq:role}, currentClient.salesForceLabelsLevel3);  
		if (sifted.length != 0){
			sifted = sift({label:{$eq:role}}, constants.SALES_FORCE_LABELS_LEVEL3()); 
			//console.log(">>>>>>>>>>>>>>>>>>>>>>sifted:" + JSON.stringify(sifted));
			//console.log(">>>>>>>>>>>>>>>>>>>>>>typeof sifted:" + typeof(sifted));
			if (sifted.length != 0){
				return sifted[0].level;
			}
		}

		sifted = sift({$eq:role}, currentClient.medicalAffairs);  
		if (sifted.length != 0){
			sifted = sift({label:{$eq:role}}, constants.MEDICAL_AFFAIRS_LABELS()); 
			//console.log(">>>>>>>>>>>>>>>>>>>>>>sifted:" + JSON.stringify(sifted));
			//console.log(">>>>>>>>>>>>>>>>>>>>>>typeof sifted:" + typeof(sifted));
			if (sifted.length != 0){
				return sifted[0].level;
			}
		}
		
		sifted = sift({$eq:role}, currentClient.payors);
		if (sifted.length != 0){
			sifted = sift({label:{$eq:role}}, constants.PAYOR_LABELS()); 
			//console.log(">>>>>>>>>>>>>>>>>>>>>>sifted:" + JSON.stringify(sifted));
			//console.log(">>>>>>>>>>>>>>>>>>>>>>typeof sifted:" + typeof(sifted));
			if (sifted.length != 0){
				return sifted[0].level;
			}
		}

		return null;

	}	



	this.GetEventAttendees = function(attendeeIds, callback, self, result){
      var MJ = require("mongo-fast-join");
      var MongoClient = require('mongodb').MongoClient;
      var mongoJoin = new MJ();
	  var connectionString = 'mongodb://'+geddy.config.db.mongo.host+':'+geddy.config.db.mongo.port+'/'+geddy.config.db.mongo.dbname;
	  result.attendees = null;
	  console.log(12312+"---------------");
	  if (typeof(geddy.config.db.mongo.serverOptions)){
	  	if(geddy.config.db.mongo.serverOptions.ssl){
	  		connectionString += "?ssl=true";
	  	}
	  }


	  MongoClient.connect(connectionString, function(err, db) {
	    if(err) throw err;
		var collection1 = db.collection('attendees');
	    var collection2 = db.collection('contacts');
		mongoJoin.query(collection1,
	        {id: {$in: attendeeIds}}, //query statement
	        {}, //fields
	        {
	            limit: 10000//options
	        }
	    ).join({
	        joinCollection: collection2,
	        //respects the dot notation, multiple keys can be specified in this array
	        leftKeys: ["contactId"],
	        //This is the key of the document in the right hand document
	        rightKeys: ["_id"],
	        //This is the new subdocument that will be added to the result document
	        newKey: "contact"
	    }).exec(function (err, items) {    //Call exec to run the compiled query and catch any errors and results, in the callback
			db.close();
			result.attendees = items;

			//figured out how many registered in total
			result.totalAttendeesInvited = 0;
			result.totalAttendeesAttended = 0;
			result.totalAttendeesWalkIn = 0;
			result.totalAttendeesPending = 0;
			result.totalAttendeesRegistered = 0;
			result.totalAttendeesCanceled = 0;
			result.totalAttendeesNoShow = 0;
			result.totalAttendeesDeclined = 0;

			var i;
			for (i = items.length - 1; i >= 0; i--) {
				switch(items[i].status)
				{
					case 1:
						result.totalAttendeesInvited++;
						break;

					case 3:
						result.totalAttendeesAttended++;
						break;

					case 4:
						result.totalAttendeesWalkIn++;
						break;

					case 5:
						result.totalAttendeesPending++;
						break;

					case 6:
						result.totalAttendeesRegistered++;
						break;

					case 7:
						result.totalAttendeesCanceled++;
						break;

					case 8:
						result.totalAttendeesNoShow++;
						break;

					case 9:
						result.totalAttendeesDeclined++;
						break;
				}
			};

			callback(self,result);
		    });
    	});			
	}


	this.lockObject = function (self, objectId){
		var lock = geddy.model.Lock.create();
		lock.editorId = self.session.get('userId');
		lock.editorFirstName = self.session.get('userFirstName');
		lock.editorLastName = self.session.get('userLastName');
		lock.objectId = objectId;
		lock.editTime = new Date();

		lock.save(function(err, data){
			if (err) {
            	throw err;
          	}

          	self.session.set('currentLockId', data.id);
		});
	};

	this.unlockObject = function(self)
	{
		var editorId = self.session.get('userId');

		// var MongoClient = require('mongodb').MongoClient;
  // 		var connectionString = 'mongodb://'+geddy.config.db.mongo.host+':'+geddy.config.db.mongo.port+'/'+geddy.config.db.mongo.dbname;

  // 		if (typeof(geddy.config.db.mongo.serverOptions)){
		// 	if(geddy.config.db.mongo.serverOptions.ssl){
		// 		connectionString += "?ssl=true";
		// 	}
		// }

  // 		MongoClient.connect(connectionString,  geddy.config.db.mongo.serverOptions, function(err, db) {
  //   		if(err) throw err;
			//remove all the locks found for this editor
			var locks = geddy.globalDb.collection('locks');

			locks.remove({editorId: editorId}, function (err, result){
				if (err)
				{
					throw err;
				}

				self.session.set('currentLockId', null);

				locks = null;

			});
		// });
	}


	this.removeAllLocks = function (editorId)
	{
		if (editorId)
		{
			var MongoClient = require('mongodb').MongoClient;
	  		var connectionString = 'mongodb://'+geddy.config.db.mongo.host+':'+geddy.config.db.mongo.port+'/'+geddy.config.db.mongo.dbname;

	  		if (typeof(geddy.config.db.mongo.serverOptions)){
				if(geddy.config.db.mongo.serverOptions.ssl){
					connectionString += "?ssl=true";
				}
			}

	  		MongoClient.connect(connectionString, function(err, db) {
	    		if(err) throw err;
				//remove all the locks found for this editor
				db.close();
				
				var locks = db.collection('locks');

				locks.remove({editorId: editorId}, function (err, result){
					if (err)
					{
						throw err;
					}

					locks = null;

				});
			});
		}
	}
});

module.exports = DBUtils; 
