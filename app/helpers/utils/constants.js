/**
\ * New node file
 */

//this names should be the same names of the controllers, it should be case sensitive
//if something of this change the role must be saved again
var ACL_NAME_ARRAY = [ {aclName: 'Contacts'},
                        {aclName: 'client'},
                        {aclName: 'user'},
                        {aclName: 'roles'} ];

//this names should be mapped in the contacts controller in the function mapAction
var ACL_ACTION_ARRAY = [ {aclAction: 'view'},
                        {aclAction: 'list'},
                        {aclAction: 'delete'},
                        {aclAction: 'edit'} 
                        ];

var SALUTATIONS_ARRAY = [ {salutationId: 1,    text: 'Dr'}, 
		               {salutationId: 2,    text: 'Prof'},
		               {salutationId: 2,    text: 'Mr'},
		               {salutationId: 3,    text: 'Mrs'},
		               {salutationId: 4,    text: 'Ms'}  ];

var SUFFIXES_ARRAY = [ {sufixId: 2,    text: 'Jr'},
	                    {sufixId: 3,    text: 'Sr'},
	                    {sufixId: 4,    text: 'III'}  ];

var COMPLIANCE_ARRAY = [ {complianceId: 1,    text: 'Porzio.'},
                       {complianceId: 2,    text: 'Porzio AggSpend System'},
                       {complianceId: 3,    text: 'Polaris'},
                       {complianceId: 4,    text: 'ASIC Vendor Portal'},
                       {complianceId: 5,    text: 'CIS'},
                       {complianceId: 6,    text: 'Cegedim AS360 System'},
                       {complianceId: 7,    text: 'Shine-Porzio Aggregate Spend System'}  ];

var DELIVERIES_METHODS_ARRAY = [ {deliveryId: 1,    text: 'Email to Client'},
                         {deliveryId: 2,    text: 'Email to Compliance Vendor'},
                         {deliveryId: 3,    text: 'CRG Upload'}  ];


var ROOM_TYPES_ARRAY = [ {label: 'Single', id: 0}, 
	                    {label: 'Double', id: 1},
	                    {label: 'Triple', id: 2}];


var COUNTRIES_ARRAY = [
    {countryCode:'AF', name: 'AFGHANISTAN'},
	{countryCode:'AX', name: 'ALAND ISLANDS'},
	{countryCode:'AL', name: 'ALBANIA'},
	{countryCode:'DZ', name: 'ALGERIA'},
	{countryCode:'AS', name: 'AMERICAN SAMOA'},
	{countryCode:'AD', name: 'ANDORRA'},
	{countryCode:'AO', name: 'ANGOLA'},
	{countryCode:'AI', name: 'ANGUILLA'},
	{countryCode:'AQ', name: 'ANTARCTICA'},
	{countryCode:'AG', name: 'ANTIGUA AND BARBUDA'},
	{countryCode:'AR', name: 'ARGENTINA'},
	{countryCode:'AM', name: 'ARMENIA'},
	{countryCode:'AW', name: 'ARUBA'},
	{countryCode:'AU', name: 'AUSTRALIA'},
	{countryCode:'AT', name: 'AUSTRIA'},
	{countryCode:'AZ', name: 'AZERBAIJAN'},
	{countryCode:'BS', name: 'BAHAMAS'},
	{countryCode:'BH', name: 'BAHRAIN'},
	{countryCode:'BD', name: 'BANGLADESH'},
	{countryCode:'BB', name: 'BARBADOS'},
	{countryCode:'BY', name: 'BELARUS'},
	{countryCode:'BE', name: 'BELGIUM'},
	{countryCode:'BZ', name: 'BELIZE'},
	{countryCode:'BJ', name: 'BENIN'},
	{countryCode:'BM', name: 'BERMUDA'},
	{countryCode:'BT', name: 'BHUTAN'},
	{countryCode:'BO', name: 'BOLIVIA PLURINATIONAL STATE OF'},
	{countryCode:'BQ', name: 'BONAIRE SINT EUSTATIUS AND SABA'},
	{countryCode:'BA', name: 'BOSNIA AND HERZEGOVINA'},
	{countryCode:'BW', name: 'BOTSWANA'},
	{countryCode:'BV', name: 'BOUVET ISLAND'},
	{countryCode:'BR', name: 'BRAZIL'},
	{countryCode:'IO', name: 'BRITISH INDIAN OCEAN TERRITORY'},
	{countryCode:'BN', name: 'BRUNEI DARUSSALAM'},
	{countryCode:'BG', name: 'BULGARIA'},
	{countryCode:'BF', name: 'BURKINA FASO'},
	{countryCode:'BI', name: 'BURUNDI'},
	{countryCode:'KH', name: 'CAMBODIA'},
	{countryCode:'CM', name: 'CAMEROON'},
	{countryCode:'CA', name: 'CANADA'},
	{countryCode:'CV', name: 'CAPE VERDE'},
	{countryCode:'KY', name: 'CAYMAN ISLANDS'},
	{countryCode:'CF', name: 'CENTRAL AFRICAN REPUBLIC'},
	{countryCode:'TD', name: 'CHAD'},
	{countryCode:'CL', name: 'CHILE'},
	{countryCode:'CN', name: 'CHINA'},
	{countryCode:'CX', name: 'CHRISTMAS ISLAND'},
	{countryCode:'CC', name: 'COCOS (KEELING) ISLANDS'},
	{countryCode:'CO', name: 'COLOMBIA'},
	{countryCode:'KM', name: 'COMOROS'},
	{countryCode:'CG', name: 'CONGO'},
	{countryCode:'CD', name: 'CONGO THE DEMOCRATIC REPUBLIC OF THE'},
	{countryCode:'CK', name: 'COOK ISLANDS'},
	{countryCode:'CR', name: 'COSTA RICA'},
	{countryCode:'CI', name: 'COTE DIVOIRE'},
	{countryCode:'HR', name: 'CROATIA'},
	{countryCode:'CU', name: 'CUBA'},
	{countryCode:'CW', name: 'CURACAO'},
	{countryCode:'CY', name: 'CYPRUS'},
	{countryCode:'CZ', name: 'CZECH REPUBLIC'},
	{countryCode:'DK', name: 'DENMARK'},
	{countryCode:'DJ', name: 'DJIBOUTI'},
	{countryCode:'DM', name: 'DOMINICA'},
	{countryCode:'DO', name: 'DOMINICAN REPUBLIC'},
	{countryCode:'EC', name: 'ECUADOR'},
	{countryCode:'EG', name: 'EGYPT'},
	{countryCode:'SV', name: 'EL SALVADOR'},
	{countryCode:'GQ', name: 'EQUATORIAL GUINEA'},
	{countryCode:'ER', name: 'ERITREA'},
	{countryCode:'EE', name: 'ESTONIA'},
	{countryCode:'ET', name: 'ETHIOPIA'},
	{countryCode:'FK', name: 'FALKLAND ISLANDS (MALVINAS)'},
	{countryCode:'FO', name: 'FAROE ISLANDS'},
	{countryCode:'FJ', name: 'FIJI'},
	{countryCode:'FI', name: 'FINLAND'},
	{countryCode:'FR', name: 'FRANCE'},
	{countryCode:'GF', name: 'FRENCH GUIANA'},
	{countryCode:'PF', name: 'FRENCH POLYNESIA'},
	{countryCode:'TF', name: 'FRENCH SOUTHERN TERRITORIES'},
	{countryCode:'GA', name: 'GABON'},
	{countryCode:'GM', name: 'GAMBIA'},
	{countryCode:'GE', name: 'GEORGIA'},
	{countryCode:'DE', name: 'GERMANY'},
	{countryCode:'GH', name: 'GHANA'},
	{countryCode:'GI', name: 'GIBRALTAR'},
	{countryCode:'GR', name: 'GREECE'},
	{countryCode:'GL', name: 'GREENLAND'},
	{countryCode:'GD', name: 'GRENADA'},
	{countryCode:'GP', name: 'GUADELOUPE'},
	{countryCode:'GU', name: 'GUAM'},
	{countryCode:'GT', name: 'GUATEMALA'},
	{countryCode:'GG', name: 'GUERNSEY'},
	{countryCode:'GN', name: 'GUINEA'},
	{countryCode:'GW', name: 'GUINEA-BISSAU'},
	{countryCode:'GY', name: 'GUYANA'},
	{countryCode:'HT', name: 'HAITI'},
	{countryCode:'HM', name: 'HEARD ISLAND AND MCDONALD ISLANDS'},
	{countryCode:'VA', name: 'HOLY SEE (VATICAN CITY STATE)'},
	{countryCode:'HN', name: 'HONDURAS'},
	{countryCode:'HK', name: 'HONG KONG'},
	{countryCode:'HU', name: 'HUNGARY'},
	{countryCode:'IS', name: 'ICELAND'},
	{countryCode:'IN', name: 'INDIA'},
	{countryCode:'ID', name: 'INDONESIA'},
	{countryCode:'IR', name: 'IRAN ISLAMIC REPUBLIC OF'},
	{countryCode:'IQ', name: 'IRAQ'},
	{countryCode:'IE', name: 'IRELAND'},
	{countryCode:'IM', name: 'ISLE OF MAN'},
	{countryCode:'IL', name: 'ISRAEL'},
	{countryCode:'IT', name: 'ITALY'},
	{countryCode:'JM', name: 'JAMAICA'},
	{countryCode:'JP', name: 'JAPAN'},
	{countryCode:'JE', name: 'JERSEY'},
	{countryCode:'JO', name: 'JORDAN'},
	{countryCode:'KZ', name: 'KAZAKHSTAN'},
	{countryCode:'KE', name: 'KENYA'},
	{countryCode:'KI', name: 'KIRIBATI'},
	{countryCode:'KP', name: 'KOREA DEMOCRATIC PEOPLES REPUBLIC OF'},
	{countryCode:'KR', name: 'KOREA REPUBLIC OF'},
	{countryCode:'KW', name: 'KUWAIT'},
	{countryCode:'KG', name: 'KYRGYZSTAN'},
	{countryCode:'LA', name: 'LAO PEOPLES DEMOCRATIC REPUBLIC'},
	{countryCode:'LV', name: 'LATVIA'},
	{countryCode:'LB', name: 'LEBANON'},
	{countryCode:'LS', name: 'LESOTHO'},
	{countryCode:'LR', name: 'LIBERIA'},
	{countryCode:'LY', name: 'LIBYA'},
	{countryCode:'LI', name: 'LIECHTENSTEIN'},
	{countryCode:'LT', name: 'LITHUANIA'},
	{countryCode:'LU', name: 'LUXEMBOURG'},
	{countryCode:'MO', name: 'MACAO'},
	{countryCode:'MK', name: 'MACEDONIA THE FORMER YUGOSLAV REPUBLIC OF'},
	{countryCode:'MG', name: 'MADAGASCAR'},
	{countryCode:'MW', name: 'MALAWI'},
	{countryCode:'MY', name: 'MALAYSIA'},
	{countryCode:'MV', name: 'MALDIVES'},
	{countryCode:'ML', name: 'MALI'},
	{countryCode:'MT', name: 'MALTA'},
	{countryCode:'MH', name: 'MARSHALL ISLANDS'},
	{countryCode:'MQ', name: 'MARTINIQUE'},
	{countryCode:'MR', name: 'MAURITANIA'},
	{countryCode:'MU', name: 'MAURITIUS'},
	{countryCode:'YT', name: 'MAYOTTE'},
	{countryCode:'MX', name: 'MEXICO'},
	{countryCode:'FM', name: 'MICRONESIA FEDERATED STATES OF'},
	{countryCode:'MD', name: 'MOLDOVA REPUBLIC OF'},
	{countryCode:'MC', name: 'MONACO'},
	{countryCode:'MN', name: 'MONGOLIA'},
	{countryCode:'ME', name: 'MONTENEGRO'},
	{countryCode:'MS', name: 'MONTSERRAT'},
	{countryCode:'MA', name: 'MOROCCO'},
	{countryCode:'MZ', name: 'MOZAMBIQUE'},
	{countryCode:'MM', name: 'MYANMAR'},
	{countryCode:'NA', name: 'NAMIBIA'},
	{countryCode:'NR', name: 'NAURU'},
	{countryCode:'NP', name: 'NEPAL'},
	{countryCode:'NL', name: 'NETHERLANDS'},
	{countryCode:'NC', name: 'NEW CALEDONIA'},
	{countryCode:'NZ', name: 'NEW ZEALAND'},
	{countryCode:'NI', name: 'NICARAGUA'},
	{countryCode:'NE', name: 'NIGER'},
	{countryCode:'NG', name: 'NIGERIA'},
	{countryCode:'NU', name: 'NIUE'},
	{countryCode:'NF', name: 'NORFOLK ISLAND'},
	{countryCode:'MP', name: 'NORTHERN MARIANA ISLANDS'},
	{countryCode:'NO', name: 'NORWAY'},
	{countryCode:'OM', name: 'OMAN'},
	{countryCode:'PK', name: 'PAKISTAN'},
	{countryCode:'PW', name: 'PALAU'},
	{countryCode:'PS', name: 'PALESTINE STATE OF'},
	{countryCode:'PA', name: 'PANAMA'},
	{countryCode:'PG', name: 'PAPUA NEW GUINEA'},
	{countryCode:'PY', name: 'PARAGUAY'},
	{countryCode:'PE', name: 'PERU'},
	{countryCode:'PH', name: 'PHILIPPINES'},
	{countryCode:'PN', name: 'PITCAIRN'},
	{countryCode:'PL', name: 'POLAND'},
	{countryCode:'PT', name: 'PORTUGAL'},
	{countryCode:'PR', name: 'PUERTO RICO'},
	{countryCode:'QA', name: 'QATAR'},
	{countryCode:'RE', name: 'REUNION'},
	{countryCode:'RO', name: 'ROMANIA'},
	{countryCode:'RU', name: 'RUSSIAN FEDERATION'},
	{countryCode:'RW', name: 'RWANDA'},
	{countryCode:'BL', name: 'SAINT BARTHELEMY'},
	{countryCode:'SH', name: 'SAINT HELENA ASCENSION AND TRISTAN DA CUNHA'},
	{countryCode:'KN', name: 'SAINT KITTS AND NEVIS'},
	{countryCode:'LC', name: 'SAINT LUCIA'},
	{countryCode:'MF', name: 'SAINT MARTIN (FRENCH PART)'},
	{countryCode:'PM', name: 'SAINT PIERRE AND MIQUELON'},
	{countryCode:'VC', name: 'SAINT VINCENT AND THE GRENADINES'},
	{countryCode:'WS', name: 'SAMOA'},
	{countryCode:'SM', name: 'SAN MARINO'},
	{countryCode:'ST', name: 'SAO TOME AND PRINCIPE'},
	{countryCode:'SA', name: 'SAUDI ARABIA'},
	{countryCode:'SN', name: 'SENEGAL'},
	{countryCode:'RS', name: 'SERBIA'},
	{countryCode:'SC', name: 'SEYCHELLES'},
	{countryCode:'SL', name: 'SIERRA LEONE'},
	{countryCode:'SG', name: 'SINGAPORE'},
	{countryCode:'SX', name: 'SINT MAARTEN (DUTCH PART)'},
	{countryCode:'SK', name: 'SLOVAKIA'},
	{countryCode:'SI', name: 'SLOVENIA'},
	{countryCode:'SB', name: 'SOLOMON ISLANDS'},
	{countryCode:'SO', name: 'SOMALIA'},
	{countryCode:'ZA', name: 'SOUTH AFRICA'},
	{countryCode:'GS', name: 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS'},
	{countryCode:'SS', name: 'SOUTH SUDAN'},
	{countryCode:'ES', name: 'SPAIN'},
	{countryCode:'LK', name: 'SRI LANKA'},
	{countryCode:'SD', name: 'SUDAN'},
	{countryCode:'SR', name: 'SURINAME'},
	{countryCode:'SJ', name: 'SVALBARD AND JAN MAYEN'},
	{countryCode:'SZ', name: 'SWAZILAND'},
	{countryCode:'SE', name: 'SWEDEN'},
	{countryCode:'CH', name: 'SWITZERLAND'},
	{countryCode:'SY', name: 'SYRIAN ARAB REPUBLIC'},
	{countryCode:'TW', name: 'TAIWAN PROVINCE OF CHINA'},
	{countryCode:'TJ', name: 'TAJIKISTAN'},
	{countryCode:'TZ', name: 'TANZANIA UNITED REPUBLIC OF'},
	{countryCode:'TH', name: 'THAILAND'},
	{countryCode:'TL', name: 'TIMOR-LESTE'},
	{countryCode:'TG', name: 'TOGO'},
	{countryCode:'TK', name: 'TOKELAU'},
	{countryCode:'TO', name: 'TONGA'},
	{countryCode:'TT', name: 'TRINIDAD AND TOBAGO'},
	{countryCode:'TN', name: 'TUNISIA'},
	{countryCode:'TR', name: 'TURKEY'},
	{countryCode:'TM', name: 'TURKMENISTAN'},
	{countryCode:'TC', name: 'TURKS AND CAICOS ISLANDS'},
	{countryCode:'TV', name: 'TUVALU'},
	{countryCode:'UG', name: 'UGANDA'},
	{countryCode:'UA', name: 'UKRAINE'},
	{countryCode:'AE', name: 'UNITED ARAB EMIRATES'},
	{countryCode:'GB', name: 'UNITED KINGDOM'},
	{countryCode:'US', name: 'UNITED STATES'},
	{countryCode:'UM', name: 'UNITED STATES MINOR OUTLYING ISLANDS'},
	{countryCode:'UY', name: 'URUGUAY'},
	{countryCode:'UZ', name: 'UZBEKISTAN'},
	{countryCode:'VU', name: 'VANUATU'},
	{countryCode:'VE', name: 'VENEZUELA BOLIVARIAN REPUBLIC OF'},
	{countryCode:'VN', name: 'VIET NAM'},
	{countryCode:'VG', name: 'VIRGIN ISLANDS BRITISH'},
	{countryCode:'VI', name: 'VIRGIN ISLANDS U.S.'},
	{countryCode:'WF', name: 'WALLIS AND FUTUNA'},
	{countryCode:'EH', name: 'WESTERN SAHARA'},
	{countryCode:'YE', name: 'YEMEN'},
	{countryCode:'ZM', name: 'ZAMBIA'}];

var STATES_ARRAY = [
	{stateCode: 'AL', name:'Alabama'},
	{stateCode: 'AK', name:'Alaska'},
	{stateCode: 'AZ', name:'Arizona'},
	{stateCode: 'AR', name:'Arkansas'},
	{stateCode: 'CA', name:'California'},
	{stateCode: 'CO', name:'Colorado'},
	{stateCode: 'CT', name:'Connecticut'},
	{stateCode: 'DE', name:'Delaware'},
	{stateCode: 'DC', name:'District of Columbia'},
	{stateCode: 'FL', name:'Florida'},
	{stateCode: 'GA', name:'Georgia'},
	{stateCode: 'GU', name:'Guam'},
	{stateCode: 'HI', name:'Hawaii'},
	{stateCode: 'ID', name:'Idaho'},						
	{stateCode: 'IL', name:'Illinois'},
	{stateCode: 'IN', name:'Indiana'},
	{stateCode: 'IA', name:'Iowa'},						
	{stateCode: 'KS', name:'Kansas'},
	{stateCode: 'KY', name:'Kentucky'},
	{stateCode: 'LA', name:'Louisiana'},
	{stateCode: 'ME', name:'Maine'},
	{stateCode: 'MD', name:'Maryland'},
	{stateCode: 'MA', name:'Massachusetts'},
	{stateCode: 'MI', name:'Michigan'},
	{stateCode: 'MN', name:'Minnesota'},
	{stateCode: 'MS', name:'Mississippi'},
	{stateCode: 'MO', name:'Missouri'},
	{stateCode: 'MT', name:'Montana'},
	{stateCode: 'NE', name:'Nebraska'},
	{stateCode: 'NV', name:'Nevada'},
	{stateCode: 'NH', name:'New Hampshire'},
	{stateCode: 'NJ', name:'New Jersey'},
	{stateCode: 'NM', name:'New Mexico'},
	{stateCode: 'NY', name:'New York'},
	{stateCode: 'NC', name:'North Carolina'},
	{stateCode: 'ND', name:'North Dakota'},
	{stateCode: 'OH', name:'Ohio'},
	{stateCode: 'OK', name:'Oklahoma'},
	{stateCode: 'OR', name:'Oregon'},
	{stateCode: 'PA', name:'Pennsylvania'},
	{stateCode: 'PR', name:'Puerto Rico'},
	{stateCode: 'RI', name:'Rhode Island'},
	{stateCode: 'SC', name:'South Carolina'},
	{stateCode: 'SD', name:'South Dakota'},
	{stateCode: 'TN', name:'Tennessee'},
	{stateCode: 'TX', name:'Texas'},
	{stateCode: 'UT', name:'Utah'},
	{stateCode: 'VT', name:'Vermont'},
	{stateCode: 'VI', name:'Virgin Islands'},
	{stateCode: 'VA', name:'Virginia'},
	{stateCode: 'WA', name:'Washington'},
	{stateCode: 'WV', name:'West Virginia'},
	{stateCode: 'WI', name:'Wisconsin'},
	{stateCode: 'WY', name:'Wyoming'}];

var PHONE_NUMBER_TYPES_ARRAY = [
	{type:'Office', value:0},
	{type:'Fax', value:1},
	{type:'Home', value:2},
	{type:'Cell', value:3}];

var ADDRESS_TYPES_ARRAY = [
	{type:'Home', value:0},
	{type:'Primary Office', value:1},
	{type:'Secondary Office', value:2},
	{type:'Other', value:3}];

var FOOD_TYPES_ARRAY = [
	{label:'American', value:0},
	{label:'Asian', value:1},
	{label:'Cafe/Deli', value:2},
	{label:'Continental', value:3},
	{label:'French', value:4},
	{label:'Greek', value:5},
	{label:'Italian', value:6},
	{label:'Latin', value:7},
	{label:'Seafood', value:8},
	{label:'Steakhouse', value:9},
	{label:'N/A', value:10},
	{label:'Other', value:11}];


var CLIENT_ADDRESS_TYPES_ARRAY = [
	{type:'Main Office', value:0},
	{type:'Shipping', value:1}];

var VENUE_TYPES_ARRAY = [
	{type:'Hotel', value:0},
	{type:'Restaurant', value:1},
	{type:'Caterer', value:2},
	{type:'Conference Center', value:3},
	{type:'Convention Center', value:4},
	{type:'Healthcare Office/Clinic', value:5},
	{type:'Hospital', value:6},
	{type:'University', value:7}
	];

var VENUE_AV_STATUS_ARRAY = [
	{type:'CRG to arrange', value:0},
	{type:'No AV required', value:1}
	];

var VENUE_ROOM_TYPES_ARRAY = [
	{label:'Private', value:0},
	{label:'Semi-Private', value:1}];

var VENUE_ROOM_CAPACITY_OPTIONS_ARRAY = [
	{label:'Crescent Rounds'},
	{label:'Board Room'},
	{label:'ClassRoom'},
	{label:'Full Rounds'},
	{label:'U-Shaped'},
	{label:'Other'},
];	

var VENUE_ROOM_AVCOSTS_OPTIONS_ARRAY = [
	{label:'Flatscreen TV'},
	{label:'Projector Screen'},
	{label:'Audio Speakers'},
	{label:'LCD Projector'},
	{label:'Outside Rental'},
];	

var VENUE_PHONE_NUMBER_TYPES_ARRAY = [
	{type:'Office', value:0},
	{type:'Fax', value:1},
	{type:'Cell', value:2}];

var MODELS_WITH_CLIENTS_LIST_ARRAY = [
	{name:'Contact'},
	{name:'ContactEmailAddress'},
	{name:'ContactPhoneNumber'},
	{name:'Address'},
	{name:'UploadedFile'},
	{name:'W8'},
	{name:'W9'}];

var SEARCH_KOL_PROPERTIES_ARRAY = [
   {label:'First Name', value:'firstName'},
   {label:'Middle Name', value:'middleName'},
   {label:'Last Name', value:'lastName'},
   {label:'Tsa Name', value:'tsaName'},
   {label:'Quickbooks Name', value:'quickbooksName'},
   {label:'NPI', value:'npi'}, 
   {label:'DEA', value:'dea'},
   {label:'Degrees', value:'degrees'},
   {label:'Specialties', value:'specialties'},
   {label:'Therapeutic Interests', value:'therapeuticInterests'},
   {label:'Affiliations Name', value:'affiliations.affiliationName'},
   {label:'Affiliations Title', value:'affiliations.title'},
   {label:'Affiliations City', value:'affiliations.city'},
   {label:'Contact Type', value:'contactType'},
   {label:'City', value:'addresses.city'},
   {label:'State', value:'addresses.state'},
   {label:'Postal Code', value:'addresses.postalCode'},
   {label:'Country', value:'addresses.country'}
];


var SEARCH_ATTENDEE_PROPERTIES_ARRAY = [
   {label:'First Name', value:'firstName'},
   {label:'Middle Name', value:'middleName'},
   {label:'Last Name', value:'lastName'},
   {label:'Type', value:'tsaName'},
   {label:'Phone Number', value:'quickbooksName'},
   {label:'NPI', value:'npi'}, 
   {label:'Degrees', value:'degrees'},
   {label:'Specialties', value:'specialties'},
   {label:'Therapeutic Interests', value:'therapeuticInterests'},
   {label:'Affiliations Name', value:'affiliations.affiliationName'},
   {label:'Affiliations Title', value:'affiliations.title'},
   {label:'Affiliations City', value:'affiliations.city'},
   {label:'City', value:'addresses.city'},
   {label:'State', value:'addresses.state'},
   {label:'Postal Code', value:'addresses.postalCode'},
   {label:'Country', value:'addresses.country'},
   {label:'status', value:'status'}
];

var SEARCH_EVENT_AUDIENCE_PROPERTIES_ARRAY = [
   {label:'First Name', value:'firstName'},
   {label:'Middle Name', value:'middleName'},
   {label:'Last Name', value:'lastName'},
   {label:'NPI', value:'npi'}, 
   {label:'Degrees', value:'degrees'},
   {label:'Product', value:'specialties'},
   {label:'Therapeutic Interests', value:'therapeuticInterests'},
   {label:'Affiliations Name', value:'affiliations.affiliationName'},
   {label:'Affiliations Title', value:'affiliations.title'},
   {label:'Affiliations City', value:'affiliations.city'},
   {label:'City', value:'addresses.city'},
   {label:'State', value:'addresses.state'},
   {label:'Postal Code', value:'addresses.postalCode'},
   {label:'Country', value:'addresses.country'}
];

var SEARCH_CLIENT_PROPERTIES_ARRAY = [
	{label:'Client Name', value:'name'},
	{label:'CRG Prefix Code', value:'crgPrefixCode'},
	{label:'CRG Bussiness Name', value:'crgBusinessDevelopmentName'},
	{label:'Product', value:'products.productName'},
	{label:'Therapeutic Areas', value:'therapeuticAreas'},
	{label:'Street', value:'addresses.address1'},
	{label:'City', value:'addresses.city'},
	{label:'State', value:'addresses.state'},
	{label:'Postal Code', value:'addresses.postalCode'},
	{label:'Compliance System', value:'complianceSystem'},
	{label:'Delivery Method', value:'deliveryMethod'}
];

var SEARCH_REP_PROPERTIES_ARRAY = [
	{label:'First Name', value:'firstName'},
	{label:'Last Name', value:'lastName'},
	{label:'Team', value:'team'},
	{label:'Role', value:'role'},
	{label:'Level', value:'level'},
	{label:'Zone', value:'zone'},
	{label:'Region', value:'region'},
	{label:'Territory', value:'territory'},
	{label:'status', value:'status'},
	{label:'Street', value:'addresses.address1'},
	{label:'City', value:'addresses.city'},
	{label:'State', value:'addresses.state'},
	{label:'Postal Code', value:'addresses.postalCode'}

];

var SEARCH_PROGRAM_PROPERTIES_ARRAY = [
	{label:'Name', value:'programName'},
	{label:'CRG Job Number', value:'crgJobNumber'},
	{label:'PO Number', value:'poNumber'},
	{label:'Business Owner Name', value:'businessOwnerName'}
];

var SEARCH_VENUE_PROPERTIES_ARRAY = [
	{label:'Name', value:'venueName'},
	{label:'Chain', value:'chain'},
	{label:'Street', value:'addresses.address1'},
	{label:'City', value:'addresses.city'},
	{label:'State', value:'addresses.state'},
	{label:'Postal Code', value:'addresses.postalCode'},
	{label:'Main Email', value:'mainEmail'},
	{label:'Food Type', value:'foodType'},
	{label:'Rating', value:'rating'}
];

var SEARCH_EVENT_PROPERTIES_ARRAY = [
	{label:'Brand', value:'brand'},
	{label:'Presentation Title', value:'presentationTitle'},
	{label:'Event Type', value:'eventType'},
	{label:'Field Force', value:'requestingRepName'},
	{label:'Job Number', value:'jobNumber'},
	{label:'Approval Status', value:'approvalStatus'},
	{label:'Status', value:'status'},
	{label:'services', value:'services.serviceName'},
	{label:'City', value:'city'},
	{label:'State', value:'state'},
	{label:'Speaker', value:'potentialSpeakers.speakerName'},
];

var SEARCH_REP_PORTAL_PROPERTIES_ARRAY = [
	{label:'Brand', value:'brand'},
	{label:'Presentation Title', value:'presentationTitle'},
	{label:'Event Type', value:'eventType'},
	{label:'Approval Status', value:'approvalStatus'},
	{label:'Status', value:'status'},
	{label:'City', value:'city'},
	{label:'State', value:'state'},
	{label:'Speaker', value:'potentialSpeakers.speakerName'},
];

var SEARCH_HOME_KOL_ARRAY = [
	{label:'First Name', value:'firstName'},
   {label:'Last Name', value:'lastName'},
   {label:'Degrees', value:'degrees'},
   {label:'Specialties', value:'specialties'},
   {label:'Affiliations Name', value:'affiliations.affiliationName'},
   {label:'City', value:'addresses.city'},
   {label:'State', value:'addresses.state'},
   {label:'Country', value:'addresses.country'}
];

var SEARCH_HOME_PROGRAM_ARRAY = [
	{label:'Program Name', value:'programName'},
	{label:'CRG Job Number', value:'crgJobNumber'},
	{label:'PO Number', value:'poNumber'},
	{label:'Brand', value:'poNumber'},
	{label:'Program Format', value:'poNumber'},
	{label:'Business Owner Name', value:'businessOwnerName'}
];

var SEARCH_HOME_CLIENT_ARRAY = [
	{label:'Client Name', value:'name'},
	{label:'CRG Prefix Code', value:'crgPrefixCode'},
	{label:'CRG Bussiness Development Name', value:'crgBusinessDevelopmentName'},
	{label:'Product', value:'products.productName'},
	{label:'Therapeutic Areas', value:'therapeuticAreas'},
	{label:'City', value:'addresses.city'},
	{label:'State', value:'addresses.state'}
];

var SEARCH_HOME_USER_ARRAY = [
	{label:'Email', value:'email'},
	{label:'First Name', value:'firstName'},
   {label:'Last Name', value:'lastName'},
	{label:'Role', value:'roleId'}
];


var SALES_FORCE_LABELS_LEVEL1_ARRAY = [
	{label:'Regional Business Manager (RBM)', level: 1},
	{label:'Area Director (AD)', level: 1},
	{label:'Area Vice President (AVP)', level: 1},
	{label:'Regional Sales Manager (RSM)', level: 1},
	{label:'District Manager (DM)', level: 1},
	{label:'Regional Sales Director (RSD)', level: 1},
	{label:'Veterans Services Manager (VSM)', level: 1},
	{label:'Managed Markets (MM)', level: 1}
	];

var SALES_FORCE_LABELS_LEVEL2_ARRAY = [
	{label:'Territory Business Manager (TBM)', level: 2},
	{label:'Regional Business Manager (RBM)', level: 2},
	{label:'Regional Sales Manager (RSM)', level: 2},
	{label:'Specialty Sales Consultant (SSC)', level: 3},
	{label:'Clinical Specialist (CS)', level: 3},
	{label:'Sales Representative (SR)', level: 3},
	{label:'Veterans Services Representative (VSR)', level: 3}
	];                            

var SALES_FORCE_LABELS_LEVEL3_ARRAY = [
	{label:'Long Term Care Sales Rep (LNAM)', level: 3},
	{label:'Retail Sales Rep (NAM)', level: 3},
	{label:'Sales Representative (SR)', level: 3},
	];

var MEDICAL_AFFAIRS_LABELS_ARRAY = [
	{label:'Regional Medical Manager (RMM)', level: 4},
	{label:'Regional Reimbursement Manager (RRM)', level: 4},
	{label:'Medical Science Liaison (MSL)', level: 4}
	];

var PAYOR_ARRAY = [
	{label:'National Account Manager (NAM)', level: 5}
	];

var ATTENDEE_STATUS_TYPE_ARRAY = [
	{label:'Confirmed', id: 0},
	{label:'Invited', id: 1},
	{label:'Not Invited', id: 2},
	{label:'Attended',  id: 3},
	{label:'Walk-in', id: 4},
	{label:'Registered - Pending Validation', id: 5},
	{label:'Registered', id: 6},
	{label:'Cancelled', id: 7},
	{label:'No-Show', id: 8},
	{label:'Declined', id: 9}
	// {label:'OPT-Out', id: 10}
	];


var ATTENDEE_TYPE_ARRAY = [
	{label:'Chair', id: 0},
	{label:'Presenter', id: 1},
	{label:'Faculty', id: 2},
	{label:'Attendee', id: 3},
	{label:'CRG', id: 4},
	{label:'Client', id: 5},
	{label:'Vendor', id: 6},
	{label:'Guest', id: 7}
	];

var SPEAKER_STATUS_ARRAY = [
	{label: 'Invited and confirmed', value: 0},
	{label: 'Invited but not yet confirmed', value: 1},
	{label: 'Needs to be invited and confirmed', value: 2},
]


var SPEAKER_STATUS_CRG_ARRAY = [
	{label: 'Invited', value: 0},
	{label: 'Confirmed', value: 1},
	{label: 'Cancelled', value: 2},
	{label: 'Declined', value: 3},
]


var CONTRACT_STATUS_TYPES_ARRAY = [
	{label:'Active', value:0},
	{label:'Deactive', value:1}];

var DAYS_OF_WEEK_ARRAY = [
	{label:'Monday'},
	{label:'Tuesday'},
	{label:'Wednesday'},
	{label:'Thursday'},
	{label:'Friday'},
	{label:'Saturday'},
	{label:'Sunday'}
];

var PARKING_OPTIONS_ARRAY = [
	{label:'Self Parking (Lot-on property)'},
	{label:'Self Parking (Lot-off property)'},
	{label:'Self Parking (Street)'},
	{label:'Valet'}
];

var TIME_ZONES_ARRAY = [
	{label:'Eastern'},
	{label:'Central'},
	{label:'Mountain'},
	{label:'Mountain (no DST)'},
	{label:'Pacific'},
	{label:'Alaska'},
	{label:'Hawaii'},
	{label:'Hawaii (no DST)'}
];

var EVENT_TYPES_ARRAY = [
	{type:"In Office",value:0},
	{type:"Out of Office",value:1},
	{type:"Webinar Only",value:2},
	{type:"Webinar with Catering",value:3},
];


var INVITATION_DELIVERY_TYPES_ARRAY = [
	{type:"PDF Version Via E-mail Direct to HCPs (max 50)",value:0},
	{type:"PDF Version Via E-mail to Sales Representative",value:1},
	{type:"Hard Copy Version Via Direct Mail to HCPs (max 50)",value:2},
	{type:"Hard Copy Version to Sales Representative (qty 25)",value:3},
];


var EVENT_APPROVAL_STATUS_ARRAY = [
	{type:"Not submitted",value:0},
	{type:"Pending Approval",value:1},
	{type:"Approved",value:2},
	{type:"Denied",value:3},
];

var EVENT_STATUS_ARRAY = [
	{type:"Not Confirmed",value:0},
	{type:"Confirmed",value:1},
	{type:"Completed",value:2},
	{type:"Reconciled",value:3},
	{type:"Rescheduled",value:4},
	{type:"Cancelled",value:5},
];

var DELIVERABLE_TYPES_ARRAY = [
	{type:"Program Request Submission",value:0},
	{type:"Program Request Approval",value:1},
	{type:"Program Request Denial",value:2},
	{type:"Program Request Approval/Denial",value:3},
	{type:"Attendee Registration",value:4}
];
	//{type:"General Communication",value:3}

var DELIVERABLE_TO_RECEPIENTS_ARRAY = [
	{label:"Attendees",value:0},
	{label:"Speakers",value:1},
	{label:"Field Force",value:2},
	{label:"RSM",value:3},
	{label:"Client",value:4}
];

var DELIVERABLE_CC_RECEPIENTS_ARRAY = [
	{label:"Field Force",value:0},
	{label:"RSM",value:1},
	{label:"Client",value:2},
	{label:"CRG Program Manager",value:3},
	{label:"Speaker's Office Contact",value:4}
];

var MONTHS = [
			{label:"January", data:0},
			{label:"February", data:1},
			{label:"March", data:2},
			{label:"April", data:3},
			{label:"May", data:4},
			{label:"June", data:5},
			{label:"July", data:6},
			{label:"August", data:7},
			{label:"September", data:8},
			{label:"October", data:9},
			{label:"November", data:10},
			{label:"December", data:11}
		];
		
var DAYSOFTHEWEEK = [
			{label:"Sunday", data:0},
			{label:"Monday", data:1},
			{label:"Tuesday", data:2},
			{label:"Wednesday", data:3},
			{label:"Thursday", data:4},
			{label:"Friday", data:5},
			{label:"Saturday", data:6}
		];


var REPORT_TYPES_ARRAY = [
	'Speaker List',
	'Sales Force List',
	// 'Remaining Funds',
	'Data Region Summary',
	'Speaker Usage Summary',
	'Program Status',
	'Attendance Report',
	//'HCP Info Report',
	'Reconciliation Report'];


var Constants =  new (function (){
    //this.GeneralDeliveryType = 3;

    this.userState = {
   		Disabled:-1,
    	PendingOfConfirmation:0,
    	Active:1,
    	NeedsChangePassword:2
    };

    this.NOT_SUBMITTED = 0;
    this.PENDING_APPROVAL = 1;
    this.APPROVED = 2;
    this.DENIED = 3;

    this.PROGRAM_REQUEST_SUBMISSION = "Program Request Submission";//0
    this.PROGRAM_REQUEST_APPROVAL = "Program Request Approval";//1;{type:"",value:1},
    this.PROGRAM_REQUEST_DENIAL = "Program Request Denial";//2;
    this.PROGRAM_REQUEST_APPROVAL_DENIAL = "Program Request Approval/Denial";//3
    this.EVENT_REGISTRATION = "Attendee Registration";//4;


    this.NOT_CONFIRMED = 0;
    this.SPEAKER_CONFIRMED = 1;

    this.WEBCAST_PROGRAM_TYPE ='webinar';

	this.roles = {
			SUPER_ADMIN:{label:'Super Admin', destinationSection:'clients'},
			ADMIN:{label:'Admin', destinationSection:'clients'},
			CLIENT:{label:'Client', destinationSection:'clientProgramDetails'},
			REP:{label:'Field Force', destinationSection:'repProgramDetails'},
			MANAGER:{label:'Manager', destinationSection:'srmProgramDetails'},
			KOL:{label:'KOL', destinationSection:'kolProgramDetails'},
	};
	
	this.COMPANIES = {
			bdsi:{name:'BDSI', email:'BDSISB@encompassmanager.com'},
			crg:{name:'CRG', email:'support@encompassmanager.com'},
			bpl:{name:'BPL',email:'BPL@encompassmanager.com'},
			leo:{name:'LEO',email:'leo@encompassmanager.com'},
	};

	this.CRG_NUMBER = "(866) 320-5679";

	this.sections = {
		//Tab ids
		CLIENT_INDEX:'clientsIndex',
		CLIENT_DETAILS:'clientDetails',
		CLIENT_PROGRAMS:'clientPrograms',
		CLIENT_VENUES:'clientVenues',
		CLIENT_GUIDELINES:'clientGuidelines',
		CLIENT_CONTRACTS:'clientContracts',
		CLIENT_REPS:'clientReps',
		CLIENT_RESOURCES:'clientResources',
		PROGRAM_DETAILS:'programDetails',
		PROGRAM_EVENTS:'programEvents',
		PROGRAM_SPEAKERS:'programSpeakers',
		PROGRAM_COMMUNICATIONS:'programCommunications',
		PROGRAM_REPS:'programReps',
		PROGRAM_RESOURCES:'programResources',
		EVENT_PRF:'eventPRF',
		EVENT_DETAILS:'eventDetails',
		EVENT_ATTENDEES:'eventAttendees',
		EVENT_COMMUNICATIONS:'eventCommunications',
		EVENT_FINANCES:'eventFinances',
		EVENT_NOTES:'eventNotes',
		REP_PORTAL_PROGRAM_DETAILS:'repPortalProgramDetails',
		REP_PORTAL_DETAILS:'repPortalDetails',
		REP_PORTAL_RESOURCES:'repPortalResources',
		REP_PORTAL_EVENT_DETAILS:'repPortalEventDetails',
		
		MANAGER_PORTAL_PROGRAM_DETAILS:'managerPortalProgramDetails',
		MANAGER_PORTAL_DETAILS:'managerPortalDetails',
		MANAGER_PORTAL_RESOURCES:'managerPortalResources',
		MANAGER_PORTAL_EVENT_DETAILS:'managerPortalEventDetails',
		
		CONTACT_PORTAL_PROGRAM_DETAILS:'contactPortalProgramDetails',
		CONTACT_PORTAL_DETAILS:'contactPortalDetails',
		CONTACT_PORTAL_RESOURCES:'contactPortalResources',
		CONTACT_PORTAL_EVENT_DETAILS:'contactPortalEventDetails',
		
		CLIENT_PORTAL_PROGRAM_DETAILS:'clientPortalProgramDetails',
		CLIENT_PORTAL_DETAILS:'clientPortalDetails',
		CLIENT_PORTAL_RESOURCES:'clientPortalResources',
		CLIENT_PORTAL_EVENT_DETAILS:'clientPortalEventDetails',
		
		USERS_SECTION : "usersSection",
		SERVICES_SECTION : "servicesSection",
		PROGRAM_TYPES_SECTION : "programTypesSection",
		REPORTS : "reports",
		HOME : "home"  //STUTZENV3
	};

	this.LOCK_MAX_TIME = 15; // MINUTES


	this.showingAlert = function showingAlert(message) {
	   console.log('>>>>>>>>>>>>>>>>>>>>>' + message);
	};

	this.ROLES = function ROLES() {
		return ROLES_ARRAY;
	};

	this.SALUTATIONS = function SALUTATIONS() {
	   return SALUTATIONS_ARRAY;
	};

	this.SUFFIXES = function SUFFIXES() {
	   return SUFFIXES_ARRAY;
	};

	this.COMPLIANCES = function COMPLIANCES() {
	   return COMPLIANCE_ARRAY;
	};

	this.DELIVERIES = function DELIVERIES() {
	   return DELIVERIES_METHODS_ARRAY;
	};

	this.ROOM_TYPES = function ROOM_TYPES() {
	   return ROOM_TYPES_ARRAY;
	};

	this.COUNTRIES = function COUNTRIES() {
	   return COUNTRIES_ARRAY;
	};

	this.STATES = function STATES() {
	   return STATES_ARRAY;
	};

	this.PHONE_NUMBER_TYPES = function PHONE_NUMBER_TYPES() {
	   return PHONE_NUMBER_TYPES_ARRAY;
	};

	this.CONTRACT_STATUS_TYPES = function CONTRACT_STATUS_TYPES() {
	   return CONTRACT_STATUS_TYPES_ARRAY;
	};

	this.ADDRESS_TYPES = function ADDRESS_TYPES() {
	   return ADDRESS_TYPES_ARRAY;
	};

	this.FOOD_TYPES = function FOOD_TYPES() {
	   return FOOD_TYPES_ARRAY;
	};

	this.CLIENT_ADDRESS_TYPES = function CLIENT_ADDRESS_TYPES() {
	   return CLIENT_ADDRESS_TYPES_ARRAY;
	};

	this.VENUE_TYPES = function VENUE_TYPES() {
	   return VENUE_TYPES_ARRAY;
	};

	this.VENUE_AV_STATUS = function VENUE_AV_STATUS() {
	   return VENUE_AV_STATUS_ARRAY;
	};

	this.VENUE_ROOM_TYPES = function VENUE_ROOM_TYPES() {
	   return VENUE_ROOM_TYPES_ARRAY;
	};

	this.VENUE_ROOM_CAPACITY_OPTIONS = function VENUE_ROOM_CAPACITY_OPTIONS() {
	   return VENUE_ROOM_CAPACITY_OPTIONS_ARRAY;
	};

	this.VENUE_ROOM_AVCOSTS_OPTIONS = function VENUE_ROOM_AVCOSTS_OPTIONS() {
	   return VENUE_ROOM_AVCOSTS_OPTIONS_ARRAY;
	};

	this.VENUE_PHONE_NUMBER_TYPES = function VENUE_PHONE_NUMBER_TYPES() {
	   return VENUE_PHONE_NUMBER_TYPES_ARRAY;
	};

	this.SEARCH_KOL_PROPERTIES = function SEARCH_KOL_PROPERTIES() {
	   return SEARCH_KOL_PROPERTIES_ARRAY;
	};

	this.SEARCH_EVENT_AUDIENCE_PROPERTIES = function SEARCH_EVENT_AUDIENCE_PROPERTIES() {
	   return SEARCH_EVENT_AUDIENCE_PROPERTIES_ARRAY;
	};

	this.SEARCH_CLIENT_PROPERTIES = function SEARCH_CLIENT_PROPERTIES() {
	   return SEARCH_CLIENT_PROPERTIES_ARRAY;
	};

	this.SEARCH_PROGRAM_PROPERTIES = function SEARCH_PROGRAM_PROPERTIES() {
	   return SEARCH_PROGRAM_PROPERTIES_ARRAY;
	};

	this.SEARCH_VENUE_PROPERTIES = function SEARCH_VENUE_PROPERTIES() {
	   return SEARCH_VENUE_PROPERTIES_ARRAY;
	};

	this.SEARCH_REP_PROPERTIES = function SEARCH_REP_PROPERTIES() {
	   return SEARCH_REP_PROPERTIES_ARRAY;
	};

	this.SEARCH_EVENT_PROPERTIES = function SEARCH_EVENT_PROPERTIES() {
	   return SEARCH_EVENT_PROPERTIES_ARRAY;
	};

	this.SEARCH_REP_PORTAL_PROPERTIES = function SEARCH_REP_PORTAL_PROPERTIES() {
	   return SEARCH_REP_PORTAL_PROPERTIES_ARRAY;
	};

	this.SEARCH_ATTENDEE_PROPERTIES = function SEARCH_ATTENDEE_PROPERTIES() {
	   return SEARCH_ATTENDEE_PROPERTIES_ARRAY;
	};

	this.SELECT_PROMPT = function SELECT_PROMPT() {
	   return "Please Select One";
	};

	this.SEARCH_HOME_KOL = function SEARCH_HOME_KOL() {
	   return SEARCH_HOME_KOL_ARRAY;
	};
	
	this.SEARCH_HOME_PROGRAM = function SEARCH_HOME_PROGRAM() {
	   return SEARCH_HOME_PROGRAM_ARRAY;
	};

	this.SEARCH_HOME_CLIENT = function SEARCH_HOME_CLIENT() {
	   return SEARCH_HOME_CLIENT_ARRAY;
	};

	this.SEARCH_HOME_USER = function SEARCH_HOME_USER() {
	   return SEARCH_HOME_USER_ARRAY;
	};

	this.MODELS_WITH_CLIENTS_LIST = function MODELS_WITH_CLIENTS_LIST() {
	   return MODELS_WITH_CLIENTS_LIST_ARRAY;
	};

	this.ACL_NAME_LIST = function ACL_NAME_LIST() {
	   return ACL_NAME_ARRAY;
	};

	this.ACL_ACTION_LIST = function ACL_ACTION_LIST() {
	   return ACL_ACTION_ARRAY;
	};

	this.SALES_FORCE_LABELS_LEVEL1 = function SALES_FORCE_LABELS_LEVEL1()
	{
		return SALES_FORCE_LABELS_LEVEL1_ARRAY;
	};

	this.SALES_FORCE_LABELS_LEVEL2 = function SALES_FORCE_LABELS_LEVEL2()
	{
		return SALES_FORCE_LABELS_LEVEL2_ARRAY;
	};

	this.SALES_FORCE_LABELS_LEVEL3 = function SALES_FORCE_LABELS_LEVEL3()
	{
		return SALES_FORCE_LABELS_LEVEL3_ARRAY;
	};

	this.MEDICAL_AFFAIRS_LABELS = function MEDICAL_AFFAIRS_LABELS()
	{
		return MEDICAL_AFFAIRS_LABELS_ARRAY;
	};

	this.PAYOR_LABELS = function PAYOR_LABELS()
	{
		return PAYOR_ARRAY;
	};

	this.ATTENDEE_STATUS_TYPE = function ATTENDEE_STATUS_TYPE()
	{
		return ATTENDEE_STATUS_TYPE_ARRAY;
	};	

	this.ATTENDEE_TYPE = function ATTENDEE_TYPE()
	{
		return ATTENDEE_TYPE_ARRAY;
	};	

	this.SPEAKER_STATUS = function SPEAKER_STATUS()
	{
		return SPEAKER_STATUS_ARRAY;
	};	

	this.SPEAKER_STATUS_CRG = function SPEAKER_STATUS_CRG()
	{
		return SPEAKER_STATUS_CRG_ARRAY;
	};	

	this.DAYS_OF_WEEK = function DAYS_OF_WEEK()
	{
		return DAYS_OF_WEEK_ARRAY;
	};	

	this.PARKING_OPTIONS = function PARKING_OPTIONS()
	{
		return PARKING_OPTIONS_ARRAY;
	};

	this.TIME_ZONES = function TIME_ZONES()
	{
		return TIME_ZONES_ARRAY;
	};

	this.EVENT_TYPES = function EVENT_TYPES()
	{
		return EVENT_TYPES_ARRAY;
	};	

	this.INVITATION_DELIVERY_TYPES = function INVITATION_DELIVERY_TYPES()
	{
		return INVITATION_DELIVERY_TYPES_ARRAY;
	};	

	this.EVENT_STATUS = function EVENT_STATUS()
	{
		return EVENT_STATUS_ARRAY;
	};	

	this.EVENT_APPROVAL_STATUS = function EVENT_APPROVAL_STATUS()
	{
		return EVENT_APPROVAL_STATUS_ARRAY;
	};

	this.DELIVERABLE_TYPES = function DELIVERABLE_TYPES()
	{
		return DELIVERABLE_TYPES_ARRAY;
	};

	this.DELIVERABLE_TO_RECEPIENTS = function DELIVERABLE_TO_RECEPIENTS()
	{
		return DELIVERABLE_TO_RECEPIENTS_ARRAY;
	};	

	this.DELIVERABLE_CC_RECEPIENTS = function DELIVERABLE_CC_RECEPIENTS()
	{
		return DELIVERABLE_CC_RECEPIENTS_ARRAY;
	};	
	
	this.DISCARD_PROPS = function DISCARD_PROPS()
	{
		return DISCARD_PROPS_ARRAY;
	};	


	this.REPORT_TYPES = function REPORT_TYPES()
	{
		return REPORT_TYPES_ARRAY;
	};	

	this.GET_DAY_NAME = function GET_DAY_NAME(_intDayNumber)
	{
		if (_intDayNumber >= 0 && _intDayNumber < 7)
		{
			return DAYSOFTHEWEEK[_intDayNumber].label;
		} else {
			return "";
		}
	}
		
		
	this.GET_MONTH_NAME = function GET_MONTH_NAME(_intMonthNumber)
	{
		if (_intMonthNumber >= 0 && _intMonthNumber < 12)
		{
			return MONTHS[_intMonthNumber].label;
		} else {
			return "";
		}
	}
});

module.exports = Constants; 