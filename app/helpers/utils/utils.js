
// New node file

var Utils =  new (function (){


	this.formatPhoneNumber = function (phone)
	{
		var strippedNumber = phone;
	    strippedNumber = strippedNumber.replace(/[^0-9]/g, '')

	    var newNumber = '+1 (' + strippedNumber.slice(0, 3) + ') ' + strippedNumber.slice(3, 6) + '-' + strippedNumber.slice(6);

	    return newNumber; 
	};

	this.formatCurrency = function(total) {
	    var neg = false;

	    if(total < 0) {
	        neg = true;
	        total = Math.abs(total);
	    }
	    
	    return (neg ? "-$" : '$') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
	};
});

module.exports = Utils;