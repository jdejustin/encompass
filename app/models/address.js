var Address = function () {

  this.defineProperties({
    addressType: {type: 'string'},
    international: {type: 'boolean'},
    address1: {type: 'string'},
    address2: {type: 'string'},
    city: {type: 'string'},
    state: {type: 'string'},
    postalCode: {type: 'string'},
    country: {type: 'string'},
    clients: {type: 'object'},
    preferred: {type: 'boolean'}
  });
};

exports.Address = Address;

