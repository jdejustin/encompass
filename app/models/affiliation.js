var Affiliation = function () {

  this.defineProperties({
    affiliationName: {type:'string'},
    title: {type: 'string'},
    city: {type: 'string'},
    state: {type: 'string'},
  });

};


Affiliation = geddy.model.register('Affiliation', Affiliation);
