var Attendee = function () {

  this.defineProperties({
    contactId: {type: 'string'},
    speaker: {type: 'boolean'},
    attendeeType: {type: 'string'},
    status: {type: 'number'},
    eventId: {type: 'string'},
    attendingDinner: {type: 'string'},
    dietaryRestrictions: {type: 'string'},
    specialNeeds: {type: 'string'},
    hotelArrivalDate: {type: 'date'},
    hotelDepartureDate: {type: 'date'},
    hotelConfirmationNumber: {type: 'number'},
    hotelBilling: {type: 'number'},
    roomType: {type:'string'},
    hotelNotes: {type: 'string'},
    travelItineraryReceived: {type: 'string'},
    travelArrivalDate: {type: 'date'},
    travelArrivalTime: {type: 'string'},
    arrivalCode: {type: 'number'},
    arrivalLocation: {type: 'string'},
    carArrivalConfirmationNumber: {type: 'number'},
    carArrivalLocation: {type: 'string'},
    passengersNumber: {type: 'number'},
    travelDepartureDate: {type: 'date'},
    travelDepartureTime: {type: 'string'},
    travelDepartureCode: {type: 'string'},
    travelDepartureLocation: {type: 'string'},
    carDepartureLocation: {type: 'string'},
    carDepartureConfirmationNumber: {type: 'number'},
    pickupTime: {type: 'string'},
    recordLocator: {type: 'string'},
    comments: {type: 'text'},
    airfareCost: {type: 'string'},
    travelDocuments: {type: 'object'},  //array of uploadedDocument
    registrationDate: {type: 'date'},
    pin: {type:'string'}
  });

};

Attendee = geddy.model.register('Attendee', Attendee);
