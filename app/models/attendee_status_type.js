var AttendeeStatusType = function () {

  this.defineProperties({
    attendeeStatusTypeName: {type: 'string'}
  });

  this.hasMany('Attendees');
  
};

AttendeeStatusType = geddy.model.register('AttendeeStatusType', AttendeeStatusType);
