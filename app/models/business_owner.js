var BusinessOwner = function () {

  this.defineProperties({
    businessOwnerName: {type: 'string'},
    description: {type: 'text'}
  });

};

exports.BusinessOwner = BusinessOwner;

