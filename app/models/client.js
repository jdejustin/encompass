var Client = function () {

  this.defineProperties({
    name: {type: 'string'},
    crgPrefixCode: {type: 'string'},
    crgBusinessDevelopmentName: {type: 'string'},
    products: {type: 'object'},  //array of ClientProduct
    therapeuticAreas: {type: 'text'},
    complianceSystem: {type: 'string'},
    deliveryMethod: {type: 'string'},
    addresses: {type: 'object'},//array of Address ;; model file name: address
    clientContacts : {type: 'object'},  //array of ClientContact
    financeContact: {type: 'object'},  //contactInfo
    salesForceLabelsLevel1: {type: 'object'},
    salesForceLabelsLevel2: {type: 'object'},
    salesForceLabelsLevel3: {type: 'object'},
    medicalAffairs: {type: 'object'},
    payors: {type: 'object'},
    programs: {type: 'object'}, // array of Program ids
    venues: {type: 'object'},  // array of Venue ids
    reps: {type: 'object'}, //array of Rep 
    contracts: {type: 'object'}, //array of contract ids
    guidelines: {type: 'object'}, //array of UploadedFile
    kolResources: {type: 'object'}, // array of UploadedFile
    repResources: {type: 'object'}, // array of UploadedFile
    rsmResources: {type: 'object'}, // array of UploadedFile
    clientResources: {type: 'object'}, // array of UploadedFile
    brands: {type:'object'}, //array of ClientBrand
    presentationTitles: {type:'object'}, //array of ClientPresentationTitle
    editing: {type: "boolean"},
    email: {type:'string'}
  });
};

Client = geddy.model.register('Client', Client);
