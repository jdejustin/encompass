var ClientContact = function () {

  this.defineProperties({
    title: {type: 'string', required: true},
    firstName: {type: 'string'},
    middleName: {type: 'string'},
    lastName: {type: 'string'},
    suffix: {type: 'string'},
    degrees: {type: 'object'}, //array of strings
    otherDegree: {type: 'string'}, //array of strings
    department: {type: 'string'}, 
    addresses: {type: 'object'},//array of Address ;; model file name: address
    phoneNumbers: {type: 'object'}, //array of ContactPhoneNumber
    emailAddresses: {type: 'object'}, //array of ContactEmailAddress
    officeAdminContact: {type: 'object'}, //ContactInfo
    officeAdminContactPhoneNumbers: {type: 'object'}, //array of ContactPhoneNumber
    officeAdminContactAddresses: {type: 'object'} //array of Address
  });
};

exports.ClientContact = ClientContact;

