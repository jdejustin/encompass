var Communication = function () {

  this.defineProperties({
    communicationType: {type: 'string'},
    dateSent: {type: 'date'},
    mailTemplate: {type: 'string'},  // template id
    attachmentTemplates: {type: 'object'}, //array of template ids
    recepients: {type: 'object'} //array of ids
  });

};

exports.Communication = Communication;

