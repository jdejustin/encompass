var CommunicationDelivery = function () {

  this.defineProperties({
    sendAction: {type: 'string'},
    //deliveryType: {type: 'string'},
    toRecepients: {type: 'object'}, //array of ids
    ccRecepients: {type: 'object'}, //array of ids
    attendeesRecepients: {type: 'object'}, //array of ids
    speakersRecepients: {type: 'object'}, //array of ids
    additionalEmails: {type:'object'}, //array of strings
    mailTemplate: {type: 'string'},  // template id
    templateTitle: {type: 'string'},
    attachmentTemplates: {type: 'object'}, //array of template ids
    externalAttachments: {type: 'object'}, //array of paths
    eventId: {type: 'string'},
    individualEmails: {type: 'boolean'},
    lastDateSent: {type: 'date'}
  });

};

exports.CommunicationDelivery = CommunicationDelivery;

