var CommunicationProgramDelivery = function () {

  this.defineProperties({
    deliveryType: {type: 'string'},
    mailTemplate: {type: 'string'},  // template id
    attachmentTemplates: {type: 'object'}, //array of template ids
    programId: {type: 'string'},
    lastDateSent: {type: 'date'},
  });

};

exports.CommunicationProgramDelivery = CommunicationProgramDelivery;

