var CommunicationTemplate = function () {

  this.defineProperties({
    templateTitle: {type:'string'},
    programId:{type:'string'},
    subject: {type: 'string'}
  });

};

exports.CommunicationTemplate = CommunicationTemplate;
