var Contact = function () {

  this.defineProperties({
    contactType: {type: 'string'}, //possible values: kol, attendee
    defaultPhotoIndex: {type: 'number'},
    clients: {type: 'object'}, //array of ids
    firstName: {type: 'string'},
    middleName: {type: 'string'},
    lastName: {type: 'string'},
    suffix: {type: 'string'},
    salutation: {type: 'string'},
    degrees: {type: 'object'}, //array of strings
    otherDegree: {type: 'string'},
    affiliations: {type: 'object'}, //array of Affiliation
    emailAddresses: {type: 'object'},//array of ContactEmailAddress :: model file name: contact_email_address
    defaultEmail: {type: 'string'},
    phoneNumbers: {type: 'object'},//array of ContactPhoneNumber :: model file name: contact_phone_number
    defaultPhoneNumber: {type: 'string'},
    addresses: {type: 'object'},//array of Address ;; model file name: address
    defaultAddress: {type: 'string'},
    officeStaffContact: {type: 'object'},//array of ContactInfo :: model file name: contact_info
    emergencyContacts: {type: 'object'}, //array of ContactInfo
    otherContacts: {type: 'object'}, //array of ContactInfo
    specialties: {type: 'object'}, //array of strings
    therapeuticInterests: {type: 'object'}, //array of strings
    medicalLicenses: {type: 'object'}, //array of MedicalLicense
    otherMedicalLicenses: {type: 'object'}, //array of OtherMedicalLicense
    npi: {type: 'string'},
    dea: {type: 'string'},
    taxonomyCode: {type: 'string'},
    tsaName: {type: 'string'},
    tsaGender: {type: 'string'},
    tsaDOB: {type: 'string'},
    passports: {type: 'object'}, // array of ContactPassport
    airlines : {type: 'object'},  //array of TravelAir
    hotels :{type: 'object'},  // array of TravelHotel
    dietaryRestrictions: {type: 'text'},
    specialNeeds: {type: 'text'},
    taxForms: {type: 'object'}, //array of W9 and W8
    cvs: {type: 'object'}, //array of UploadedFile
    bios: {type: 'object'}, //array of UploadedFile
    factSheets: {type: 'object'}, //array of UploadedFile
    contracts: {type: 'object'}, // contract ids
    photos: {type: 'object'}, //array of UploadedFile
    speakerType: {type: 'object'},
    trainingEvents: {type: 'object'}, //array of TrainingEvent
    appropoTrainingStatus: {type: 'boolean'},
    uniqueSpeakerIdentifier: {type: 'number'},
    reps: {type: 'object'}, // array of ids
    notes:{type: 'text'}, //this must be visible only to admins or higher level
    quickbooksName:{type: 'string'},
    meNumber: {type:'string'},
    optOut: {type: 'boolean'}
  });
};


Contact = geddy.model.register('Contact', Contact);
