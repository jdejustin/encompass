var ContactEmailAddress = function () {

  this.defineProperties({
    email: {type: 'string'},
    preferred: {type: 'boolean'},
    clients: {type: 'object'}
  });

};


exports.ContactEmailAddress = ContactEmailAddress;

