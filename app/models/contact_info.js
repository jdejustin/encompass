var ContactInfo = function () {

  this.defineProperties({
    fullName: {type: 'string'},
    title: {type: 'string'},
    email: {type: 'string'}, 
    phoneNumbers: {type: 'object'},// model file name contactPhoneNumber
    relationship: {type: 'string'},
    addresses: {type: 'object'}, //array of Address
    department: {type: 'string'}, 
  });

};

exports.ContactInfo = ContactInfo;

