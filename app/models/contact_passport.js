var ContactPassport = function () {

  this.defineProperties({
    number: {type: 'number'},
    country: {type: 'string'},
    expirationDate: {type: 'string'}
  });

};

exports.ContactPassport = ContactPassport;

