var ContactPhoneNumber = function () {

  this.defineProperties({
    number: {type: 'number'},
    phoneType: {type: 'string'},
    clients: {type: 'object'},
    preferred: {type: 'boolean'}
  });


};

exports.ContactPhoneNumber = ContactPhoneNumber;

