var ContactType = function () {

  this.defineProperties({
    contactTypeName: {type: 'string', required: true}
  });

};

ContactType = geddy.model.register('ContactType', ContactType);
