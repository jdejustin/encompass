var Contract = function () {

  this.defineProperties({
    contractName: {type: 'string'},
    contractFile: {type: 'object'},
    creationDate: {type: 'date'},
    expirationDate: {type: 'date'},
    status: {type: 'string'},
    linkedTo: {type: 'string'}, //id of program linked to or 'MainContract'
    contactId: {type: 'string'},
  });

};

Contract = geddy.model.register('Contract', Contract);
