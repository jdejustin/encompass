var Edit = function () {

  this.defineProperties({
    userId: {type: 'string'},
    userFirstName: {type: 'string'},
    userLastName: {type: 'string'},
    objectId: {type: 'string'}
  });
};

exports.Edit = Edit;

