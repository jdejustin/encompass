var Event = function () {

  this.defineProperties({
    brand: {type: 'string'},
    submitingTimes: {type: 'number'},
    programId: {type: 'string'},
    presentationTitle: {type: 'string'},
    eventType: {type: 'string'},
    jobNumber: {type: 'string'},
    approvalStatus: {type: 'number'},  //4 states: Not Submitted, Pending Approval, Approved, Denied
    status: {type: 'number'}, //
    crgPercentageFee: {type: 'number'},
    crgProgramManager: {type: 'string'},
    requestingRepId: {type: 'string'},
    requestingRepName: {type: 'string'},
    services: {type: 'object'}, //array of serviceLevel
    city: {type: 'string'},
    state: {type: 'string'},
    eventStartTime: {type: 'string'},
    timezone: {type: 'string'},
    eventDatePref1: {type: 'string'},
    eventDatePref2: {type: 'string'},
    eventDatePref3: {type: 'string'},
    confirmedDate: {type: 'string'},
    dateLocationComments: {type: 'text'},
    potentialSpeakers: {type: 'object'},  //array of Speaker
    confirmedSpeaker: {type: 'string'},
    potentialVenue: {type: 'object'}, //Venue
    venueId: {type: 'string'},
    venueStatus: {type: 'string'},
    venueCRGStatus: {type: 'string'},
    venueRoomName: {type: 'string'},
    venueGuranteedNumber: {type: 'number'},
    potentialCaterer: {type: 'object'}, //Venue
    catererId: {type:'string'},
    catererStatus: {type: 'string'},
    catererCRGStatus: {type: 'string'},
    estimatedMealCost: {type: 'string'},
    expectedNumberAttendees: {type: 'string'},
    venueAVStatus: {type: 'string'},
    venueComments: {type: 'text'},
    invitationsPDF: {type: 'boolean'},
    invitationsHardcopyDirect: {type: 'boolean'},
    invitationsHardcopyRep: {type: 'boolean'},
    invitationsPDFRep: {type: 'boolean'},
    repMailingAddressEdits: {type: 'text'},
    invitationsComments: {type: 'text'},
    emailInvitations: {type: 'object'},  //array of elements
    hardcopyInvitations: {type: 'object'}, //array of elements
    emailPDFInvitations: {type: 'object'}, //array of elements
    audienceRecruitmentComments: {type: 'text'}, 
    attendees: {type: 'object'},  //array of Attendee
    preconfirmedNumber: {type: 'number'},
    communications: {type: 'object'}, //array of UploadedFile
    prfId: {type: 'string'},  //id of EventRequest
    historical:{type:'object'},// array of historical
    notes: {type:'object'}, //array of EventNote
    sendRegistration: {type: 'boolean'},
    invoices: {type: 'object'} //array of invoices
  });
};

exports.Event = Event;

