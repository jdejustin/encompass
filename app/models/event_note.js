var EventNote = function () {

  this.defineProperties({
    accountExecutive: {type: 'string'},
    category: {type: 'string'},
    details: {type: 'text'}
  });


};

exports.EventNote = EventNote;

