var Invoice = function () {

  this.defineProperties({
    speakerId: {type: 'string'},
    feeService: {type: 'number'},
    feeCheck: {type: 'number'},
    feeDatePaid: {type: 'date'},
    expenseAmount: {type: 'number'},
    expenseCheck: {type: 'number'},
    expenseDatePaid: {type: 'date'},
    airAmount: {type: 'number'},
    groundAmount: {type: 'number'},
    hotelAmount: {type: 'number'},
    parkingAmount: {type: 'number'},
    mealsAmount: {type: 'number'},
    mileageAmount: {type: 'number'},
    miscAmount: {type: 'number'},
    tollsAmount: {type: 'number'},
    crgAirAmount: {type: 'number'},
    crgGroundAmount: {type: 'number'},
    crgHotelAmount: {type: 'number'},
    travelManagementFee: {type: 'number'},
    venueTotalAmount: {type: 'number'},
    venueRentalAmount: {type: 'number'},
    catererTotalAmount: {type: 'number'},
    venueAVTotalAmount: {type: 'number'},
    avAmount: {type: 'number'}
  });

};

exports.Invoice = Invoice;

