var Lock = function () {

  this.defineProperties({
    editorId: {type: 'string'},
    editorFirstName: {type: 'string'},
    editorLastName: {type: 'string'},
    objectId: {type: 'string'},
    editTime: {type: 'date'},
  });
};

exports.Lock = Lock;

