var NotificationTemplate = function () {

  this.defineProperties({
  	clientId: {type: 'string'},
  	eventId: {type: 'string'},
    notificationType: {type: 'string'},
    description: {type: 'string'},
  });

};

exports.NotificationTemplate = NotificationTemplate;

