var OtherMedicalLicense = function () {

  this.defineProperties({
    otherMedicalLicenseType: {type: 'string'},
    number: {type: 'number'}
  });

};


exports.OtherMedicalLicense = OtherMedicalLicense;

