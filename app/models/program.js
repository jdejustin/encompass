var Program = function () {

  this.defineProperties({
    programName: {type: 'string'},
    programTypeId: {type: 'string'},
    programTypeName: {type: 'string'},
    poNumber: {type: 'number'},
    crgJobNumber: {type: 'string'},
    businessOwnerName: {type: 'string'},
    businessOwnerDescription: {type: 'text'},
    programNotes: {type: 'text'},
    events: {type: 'object'}, //array of Event
    taxForms: {type: 'object'}, //array of UploadedFile
    crgProgramManager: {type: 'string'},  // crg admin id in charge of program
    programClientEmail: {type: 'string'},  // client email to send automatic communications
    communications: {type: 'object'}, //array of UploadedFile
    resources: {type: 'object'}, //array of UploadedFile
    contracts: {type: 'object'}, //array of Contract
    reps: {type: 'string'}, //array of rep ids
    eventTypes: {type: 'string'},
    invitationDeliveries: {type: 'string'},
    requireVenueInfo: {type: 'string'}
  });
};

Program = geddy.model.register('Program', Program);
