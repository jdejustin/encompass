var ProgramType = function () {

  this.defineProperties({
    programTypeName: {type: 'string', required: true},
    services: {type: 'object'}
  });

  this.hasService = function (service)
  {
	var returnValue = false;
	for (var i=0;i<this.services.length;i++)
	{
		if (service.id == this.services[i].id)
		{
			returnValue = true;
			break;
		}
	}
	return returnValue;
  };
};



exports.ProgramType = ProgramType;

