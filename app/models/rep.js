var Rep = function () {

  this.defineProperties({
    title: {type: 'string'},
    firstName: {type: 'string'},
    middleName: {type: 'string'},
    //clients: {type: 'object'}, //array of clients ids
    lastName: {type: 'string'},
    suffix: {type: 'string'},
    team: {type: 'string'},
    role: {type: 'text'},
    level: {type: 'number'},
    zone: {type: 'number'},
    prtId: {type: 'number'},
    region:{type: 'string'},    
    territory: {type: 'string'},
    territoryName: {type: 'string'},
    territoryZips: {type: 'text'},
    status: {type: 'string'},
    clientRepId: {type: 'string'},
    addresses: {type: 'object'},
    defaultAddress: {type: 'string'},
    phoneNumbers: {type: 'object'},
    defaultPhoneNumber: {type: 'string'},
    emailAddresses: {type: 'object'},
    defaultEmail: {type: 'string'},
    icEligibilityDate: {type: 'date'},
    hireDate: {type: 'date'},
    termDate: {type: 'date'},
    loaStart: {type: 'date'},
    loaEnd: {type: 'date'},
    jdeId: {type: 'string'},
    managerId: {type: 'string'},
    managerTerritoryNumber: {type: 'string'},
    meNumber: {type:'string'}
  });
};

Rep = geddy.model.register('Rep', Rep);
