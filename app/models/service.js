var Service = function () {

  this.defineProperties({
    serviceName: {type: 'string'},
    percentage: {type: 'number'},
    numberHours: {type: 'string'}
  });
  
};

Service = geddy.model.register('Service', Service);
