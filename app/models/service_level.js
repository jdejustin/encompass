var ServiceLevel = function () {

  this.defineProperties({
    service: {type: 'object'},
    percentage: {type: 'int'},
    hours: {type: 'int'}
  });

};

exports.ServiceLevel = ServiceLevel;

