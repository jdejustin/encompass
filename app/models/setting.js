var Setting = function () {

  this.defineProperties({
    globalEmailAddress: {type: 'string'},
    maintenanceMessage: {type:'text'},
    showMessage: {type:'boolean'}
  });

};

Setting = geddy.model.register('Setting', Setting);
