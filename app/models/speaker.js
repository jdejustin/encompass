var Speaker = function () {

  this.defineProperties({
    contactId: {type: 'string'},
    speakerName: {type: 'string'},
    crgStatus: {type: 'number'},
    status: {type: 'number'},
    city: {type: 'string'},
    state: {type: 'string'},
    travelType: {type: 'string'},
    groundTransportation: {type: 'string'},
    comments: {type: 'text'}
  });

};

exports.Speaker = Speaker;

