var TravelPassport = function () {

  this.defineProperties({
    number: {type: 'string'},
    country: {type: 'string'},
    expirationDate: {type: 'date'}
  });

};

exports.TravelPassport = TravelPassport;

