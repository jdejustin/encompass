var UploadedFile = function () {

  this.defineProperties({
    name: {type: 'string'},
    pathToFile: {type: 'string'},
    clients: {type: 'object'},
    notes: {type:'text'},
    link: {type:'text'}
  });

};

exports.UploadedFile = UploadedFile;

