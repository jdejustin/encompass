var User = function () {
    this.defineProperties({
        // username: { type: 'string', required: true },
        password: { type: 'string'},
        firstName: { type: 'string', required: true },
        middleName: { type: 'string'},
        lastName: { type: 'string', required: true },
        email: { type: 'string', required: true },
        ccEmail: { type: 'string' },
        signature: { type: 'text' },
        photo: { type: 'string' },
        state: { type: 'int' },
        guid: { type: 'string' },
        linkedId: {type: 'string'}, //a rep or contact id... if this user was created via Rep or KOL creation
        clientId: {type: 'string'} //used if the user is of type Client
    });

    // this.validatesLength('username', { min: 3 });
    // this.validatesFormat('email', /[]);
    this.hasMany('Passports');

    /* this */
    // this.validateAction = function (controller, action) {
    //     // Do some stuff
    //     var returnValue = false;
    //     // console.log('>>>>>> VALIDATE THIS CONTROLLER '+ controller + " ACTION "+action);
    //     role = this.session.get('sessionUserRole');
    //     if (role) {
    //         for (i = 0; i < role.acl.length; i++) {
    //             // console.log('CONTROLLER '+ controller + " role acl Name "+role.acl[i].aclName);
    //             if (role.acl[i].aclName == controller) {
    //                 for (j = 0; j < role.acl[i].actions.length; j++) {
    //                     console.log('role action  ' + role.acl[i].actions[j] + " action " + action);
    //                     if (role.acl[i].actions[j] == action) {
    //                         returnValue = true;
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     return returnValue;
    // };
};


User = geddy.model.register('User', User);


