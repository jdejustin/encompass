var Venue = function () {

  this.defineProperties({
    venueName: {type: 'string'},
    addresses: {type: 'object'}, //array of Address
    phoneNumbers: {type: 'object'}, //array of ContactPhoneNumber
    mainEmail: {type: 'string'},
    contact: {type: 'object'}, //one ContactInfo
    website: {type: 'string'},
    venueType: {type: 'string'},
    foodType: {type: 'string'},
    venueNotes: {type: 'text'},
    avNotes: {type: 'text'},
    chain: {type: 'string'},
    brand: {type: 'string'},
    rooms: {type: 'object'}, //array of VenueRoom
    venueAV: {type: 'string'}, 
    parking: {type: 'string'}, 
    hours: {type: 'string'},
    rating: {type: 'string'},
    pharmaMenuAvailable: {type: 'string'},
    stateSalesTax: {type: 'number'},
    serviceGratuity: {type: 'number'},
    roomRate: {type:'number'}
  });

};

Venue = geddy.model.register('Venue', Venue);
