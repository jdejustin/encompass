var VenueRoom = function () {

  this.defineProperties({
    roomName: {type: 'string'},
    roomFee: {type: 'number'},
    location: {type: 'string'},
    roomType: {type: 'string'},
    size: {type: 'string'},
    dimensions: {type: 'string'},
    maxCapacity: {type: 'string'}, 
    headcountMin: {type: 'number'},
    foodBeverageMin: {type: 'number'},
    inRoomAV: {type: 'string'},
    avCosts: {type: 'string'} 
  });

};

exports.VenueRoom = VenueRoom;

