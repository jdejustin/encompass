var W8 = function () {

  this.defineProperties({
    name: {type: 'string'},
    incCountry: {type: 'string'},
    uploadedFile: {type: 'object'},
    ownerType: {type:'string'},
    address1: {type: 'string'},
    address2: {type: 'string'},
    city: {type: 'string'},
    state: {type: 'string'},
    postalCode: {type: 'string'},
    country: {type: 'string'},
    mailingAddress1: {type: 'string'},
    mailingAddress2: {type: 'string'},
    mailingCity: {type: 'string'},
    mailingState: {type: 'string'},
    mailingPostalCode: {type: 'string'},
    mailingCountry: {type: 'string'},
    ssn: {type: 'string'},
    ein: {type: 'string'},
    foreignId: {type: 'string'},
    referenceNumbers: {type: 'string'},
    signature: {type: 'date'},
    clients: {type: 'object'}
  });
};

exports.W8 = W8;

