var W9 = function () {

  this.defineProperties({
    name: {type: 'string'},
    uploadedFile: {type: 'object'},
    businessName: {type: 'string'},
    taxClass: {type: 'string'},
    llcType: {type: 'string'},
    address1: {type: 'string'},
    address2: {type: 'string'},
    city: {type: 'string'},
    state: {type: 'string'},
    postalCode: {type: 'string'},
    ssn: {type: 'string'},
    ein: {type: 'string'},
    exemptPayeeCode: {type: 'string'},
    exemptFatcaCode: {type: 'string'},
    listAccountNumbers: {type: 'string'},
    signature: {type: 'date'},
    clients: {type: 'object'}
  });
};

exports.W9 = W9;

