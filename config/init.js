var init = function(cb) {
  // Add uncaught-exception handler in prod-like environments
  if (geddy.config.environment != 'development') {
    process.addListener('uncaughtException', function (err) {
      var msg = err.message;
      if (err.stack) {
        msg += '\n' + err.stack;
      }
      if (!msg) {
        msg = JSON.stringify(err);
      }
      geddy.log.error(msg);
    });
  }



////////////


var constants = require('../app/helpers/utils/constants.js');
mongo = require('mongodb-wrapper');
geddy.globalDb = mongo.db(geddy.config.db.mongo.host, geddy.config.db.mongo.port, geddy.config.db.mongo.dbname); //var db = mongo.db('localhost', 27017, 'encompass');      
geddy.constants = constants;
cb();



};

exports.init = init;