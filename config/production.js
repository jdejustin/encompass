/*
 * Geddy JavaScript Web development framework
 * Copyright 2112 Matthew Eernisse (mde@fleegix.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

var config = {
  detailedErrors: true
, debug: true
, hostname: "127.0.0.1"
, baseURL: "encompassmanager.com"
, port: 3000
/*, ssl: {
	key: '/etc/cert/encompassmanager.com.key',
	cert: '/etc/cert/encompassmanager.crt'
 }*/
, EmailTemplatesFolder: "private/EmailTemplates/"
, EmailSentFrom: "BDSISB@encompassmanager.com <bdsisb@encompassmanager.com>"
, EmailSubjectPrefix: "Encompass - "
, UploadTempFile: "private/uploads/"
, StorageDir: "private/storage/"
, CommunicationsFolder: "private/Communications/"
, pageLimit: 50
, model: {
    defaultAdapter: 'mongo'
  }
, db: {
  mongo: {
    //host: '90.103.12.166',
    //port: 443,
    /*host: '10.80.80.62',
    port: 27017,*/
    host: '127.0.0.1',
    port: 27017,
    dbname: 'encompass',
    serverOptions: {
      auto_reconnect: true,
      poolSize: 5,
      ssl: false,
      socketOptions: {keepAlive: 1, connectTimeoutMS: 30000 }
    }
  }
}
, sessions: {
    store: 'memory'
  , key: 'sid'
  , expiry: 3600 // 1 hour
  }
  , SMTP: {
	host: "smtp.mailgun.org", // hostname
      	secureConnection: true, // DO NOT use SSL
      	port: 465, // SSL TLS
      	auth: {
          user: "bdsisb@encompassmanager.com",
	pass: "q7&oM302",
//          pass: "759946f6dcb771761a3bd98c3cdeb2fb",
      }
  }
};

module.exports = config;
