/*
 * Geddy JavaScript Web development framework
 * Copyright 2112 Matthew Eernisse (mde@fleegix.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/


var router = new geddy.RegExpRouter();


// Basic routes
// router.match('/moving/pictures/:id', 'GET').to('Moving.pictures');
//
// router.match('/farewells/:farewelltype/kings/:kingid', 'GET').to('Farewells.kings');
//
// Can also match specific HTTP methods only
// router.get('/xandadu').to('Xanadu.specialHandler');
// router.del('/xandadu/:id').to('Xanadu.killItWithFire');
//
// Resource-based routes
// router.resource('hemispheres');
//
// Nested Resource-based routes
// router.resource('hemispheres', function(){
//   this.resource('countries');
//   this.get('/print(.:format)').to('Hemispheres.print');
// });

router.get('/test').to('Test.index');
router.post('/test').to('Test.index');
router.post('/test/save').to('Test.save');
router.get('/test/email').to('Test.email');
router.get('/test/search').to('Test.search');
router.get('/test/join').to('Test.joinTest');
router.get('/test/changeStatus').to('Test.changeStatus');
router.get('/test/viewSnapShot').to('Test.viewSnapShot');
router.get('/test/communication').to('Test.communication');
router.get('/test/reportTable').to('Test.reportTable');


router.match('/SalesRep').to('SalesRepPortal.index');


//Global routes
router.get('/').to('Main.index');
//router.get('/login').to('Main.maintenance');
router.get('/login').to('Main.login');
router.match('/login/:companyName', 'GET').to('Main.login');
router.match('/showError').to('Main.showError');

/*router.get('/loginActivation').to('Main.loginActivation');
router.match('/loginActivation/:companyName', 'GET').to('Main.loginActivation');*/
router.get('/noPrivileges').to('Main.noPrivileges');
router.get('/logout').to('Main.logout');
router.get('/logout/:companyName').to('Main.logout');
router.get('/expired').to('Main.expired');
router.match('/uploadPhoto').to('Main.uploadPhoto');
router.match('/uploadFile').to('Main.uploadFile');
router.match('/uploadExternalAttachmentFile').to('Main.uploadExternalAttachmentFile');
router.match('/updateFileName').to('Main.updateFileName');
router.match('/setFileToDisplay').to('Main.setFileToDisplay');
router.match('/displayFile').to('Main.displayFile');
router.match('/displayPhoto').to('Main.displayPhoto');
router.match('/displayVideo').to('Main.displayVideo');


//User routes
router.get('/users/userPendingOfConfirmation').to('Users.userPendingOfConfirmation');
router.get('/users/confirmUser').to('Users.confirmUser');
router.post('/users/confirmUser').to('Users.confirmUser');
router.post('/users/forgotPassword').to('Users.forgotPassword');
router.get('/users/resetPassword').to('Users.resetPassword');
router.post('/users/resetPassword').to('Users.resetPassword');
router.post('/users/joinNow').to('Users.joinNow');
router.post('/users/submitProfileUpdate').to('Users.submitProfileUpdate');
router.match('/users/showActivation').to('Users.showActivation');
router.match('/users/deleteUsers').to('Users.deleteUsers');
router.match('/users/savePhoto').to('Users.savePhoto');
router.match('/users/updateEmailsBulk').to('Users.updateEmailsBulk');
router.post('/users/updatePassword').to('Users.updatePassword');
router.match('/users/search').to('Users.search');

//Clients routes
router.match('/clients/clearCurrentClientId').to('Clients.clearCurrentClientId');
router.match('/clients/clientStep1').to('Clients.clientStep1');
router.match('/clients/clientStep2').to('Clients.clientStep2');
router.match('/clients/clientStep3').to('Clients.clientStep3');
router.match('/clients/contactClientStep').to('Clients.contactClientStep');
router.match('/clients/save').to('Clients.save');
router.match('/clients/deleteClients').to('Clients.deleteClients');
router.match('/clients/goIndex').to('Clients.goIndex');
router.match('/clients/saveEmbeddedModel').to('Clients.saveEmbeddedModel');
router.match('/clients/deleteEmbeddedModel').to('Clients.deleteEmbeddedModel');
router.match('/clients/deleteContactClients').to('Clients.deleteContactClients');
router.match('/clients/search').to('Clients.search');
router.match('/clients/guidelines').to('Clients.guidelines');
router.match('/clients/deleteGuidelines').to('Clients.deleteGuidelines');
router.match('/clients/resources').to('Clients.resources');
router.match('/clients/deleteKolResources').to('Clients.deleteKolResources');
router.match('/clients/deleteRepResources').to('Clients.deleteRepResources');
router.match('/clients/deleteRSMResources').to('Clients.deleteRSMResources');
router.match('/clients/cleanUp').to('Clients.cleanUp');
// router.match('/stories').to('Stories.index');
// router.match('/stories/loadStory').to('Stories.loadStory');


//Clients routes
router.match('/clients2s/clearCurrentClientId').to('Clients2.clearCurrentClientId');
router.match('/clients2s/clientStep1').to('Clients2.clientStep1');
router.match('/clients2s/clientStep2').to('Clients2.clientStep2');
router.match('/clients2s/clientStep3').to('Clients2.clientStep3');
router.match('/clients2s/save').to('Clients2.save');
router.match('/clients2s/deleteClients').to('Clients2.deleteClients');
router.match('/clients2s/goIndex').to('Clients2.goIndex');
router.match('/clients2s/saveEmbeddedModel').to('Clients2.saveEmbeddedModel');
router.match('/clients2s/deleteEmbeddedModel').to('Clients2.deleteEmbeddedModel');
router.match('/clients2s/search').to('Clients2.search');
router.match('/clients2s/guidelines').to('Clients2.guidelines');
router.match('/clients2s/deleteGuidelines').to('Clients2.deleteGuidelines');
router.match('/clients2s/resources').to('Clients2.resources');

/*router.match('/clients2').to('Clients2.index');*/

//Contacts routes
router.match('/contacts/contactStep1').to('Contacts.contactStep1');
router.match('/contacts/contactStep2').to('Contacts.contactStep2');
router.match('/contacts/contactStep3').to('Contacts.contactStep3');
router.match('/contacts/contactStep4').to('Contacts.contactStep4');
router.match('/contacts/contactStep5').to('Contacts.contactstep5');
router.match('/contacts/clearCurrentContactId').to('Contacts.clearCurrentContactId');
router.match('/contacts/edit').to('Contacts.edit');
router.match('/contacts/save').to('Contacts.save');
router.match('/contacts/search').to('Contacts.search');
router.match('/contacts/searchAll').to('Contacts.searchAll');
router.match('/contacts/searchContactsByClientId').to('Contacts.searchContactsByClientId');
router.match('/contacts/saveEmbeddedModel').to('Contacts.saveEmbeddedModel');
router.match('/contacts/deleteEmbeddedModel').to('Contacts.deleteEmbeddedModel');
router.match('/contacts/deleteContacts').to('Contacts.deleteContacts');
router.match('/contacts/deleteCVs').to('Contacts.deleteCVs');
router.match('/contacts/deleteBios').to('Contacts.deleteBios');
router.match('/contacts/deleteFactSheets').to('Contacts.deleteFactSheets');
router.match('/contacts/deleteW8W9s').to('Contacts.deleteW8W9s');
router.match('/contacts/deletePhotos').to('Contacts.deletePhotos');
router.match('/contacts/setCurrentClient').to('Contacts.setCurrentClient');
router.match('/contacts/linkToNewClient').to('Contacts.linkToNewClient');
router.match('/contacts/removeClientId').to('Contacts.removeClientId');
router.match('/contacts/import').to('Contacts.showImport');
router.match('/contacts/processFile').to('Contacts.import');
router.match('/contacts/setCurrentKOLTypeForImport').to('Contacts.setCurrentKOLTypeForImport');
router.match('/contacts/saveDiff').to('Contacts.saveDiff');
router.match('/contacts/saveDefaultItem').to('Contacts.saveDefaultItem');


//Export 
router.match('/exports/setCurrentExportObject').to('Exports.setCurrentExportObject');
router.match('/exports/getCurrentExportObject').to('Exports.getCurrentExportObject');
router.match('/exports/clearCurrentExportObject').to('Exports.clearCurrentExportObject');
router.match('/exports/exportContacts').to('Exports.exportContacts');
router.match('/exports/getAllObjectsForExport').to('Exports.getAllObjectsForExport');


//Reports
router.match('/reports').to('Reports.index');
router.match('/reports/generateReport').to('Reports.generateReport');

//Home //STUTZENV3
router.match('/home').to('Home.index');



// Program routes
router.match('/programs/clearCurrentProgram').to('Programs.clearCurrentProgram');
router.match('/programs/programStep1').to('Programs.programStep1');
router.match('/programs/save').to('Programs.save');
router.match('/programs/search').to('Programs.search');
router.match('/programs/speakerSearch').to('Programs.speakerSearch');
router.match('/programs/saveEmbeddedModel').to('Programs.saveEmbeddedModel');
router.match('/programs/deleteEmbeddedModel').to('Programs.deleteEmbeddedModel');
router.match('/programs/deletePrograms').to('Programs.deletePrograms');
router.match('/programs/deleteProgramFromClient').to('Programs.deleteProgramFromClient');
router.match('/programs/resources').to('Programs.resources');
router.match('/programs/deleteResources').to('Programs.deleteResources');
router.match('/programs/speakers').to('Programs.speakers');
router.match('/programs/reps').to('Programs.reps');


// Communication routes
router.match('/communications/communicationDelivery').to('Communications.communicationDelivery');
router.match('/communications/communicationTemplate').to('Communications.communicationTemplate');
router.match('/communications/editDeliveryCommunication').to('Communications.editDeliveryCommunication');
router.match('/communications/editDeliveryCommunication/:deliveryId').to('Communications.editDeliveryCommunication');
router.match('/communications/editTemplateCommunication').to('Communications.editTemplateCommunication');
router.match('/communications/editTemplateCommunication/:templateId').to('Communications.editTemplateCommunication');
router.match('/communications/clearCurrentTemplate').to('Communications.clearCurrentTemplate');
router.match('/communications/clearCurrentProgramDelivery').to('Communications.clearCurrentProgramDelivery');
router.match('/communications/saveTemplate').to('Communications.saveTemplate');
router.match('/communications/saveDelivery').to('Communications.saveDelivery');
router.match('/communications/deleteTemplates').to('Communications.deleteTemplates');
router.match('/communications/deleteDeliveries').to('Communications.deleteDeliveries');
router.match('/communications/deleteProgramDeliveries').to('Communications.deleteProgramDeliveries');
router.match('/communications/sendTestSample').to('Communications.sendTestSample');
router.match('/communications/downloadDelivery/:deliveryId').to('Communications.downloadDelivery');
router.match('/communications/sendDelivery').to('Communications.sendDelivery');
router.match('/communications/editProgramDeliveryCommunication').to('Communications.editProgramDeliveryCommunication');
router.match('/communications/editProgramDeliveryCommunication/:programDeliveryId').to('Communications.editProgramDeliveryCommunication');
router.match('/communications/saveProgramDelivery').to('Communications.saveProgramDelivery');
router.match('/communications/editDeliveryFiles/:deliveryId').to('Communications.editDeliveryFiles');
router.match('/communications/getFileContent').to('Communications.getFileContent');
router.match('/communications/saveDeliveryFile').to('Communications.saveDeliveryFile');
router.match('/communications/clearCurrentDeliveryFiles').to('Communications.clearCurrentDeliveryFiles');
router.match('/communications/sendAutomaticEventRequestNotification').to('Communications.sendAutomaticEventRequestNotification');
router.match('/communications/saveProgramDeliveryTemplates').to('Communications.saveProgramDeliveryTemplates');
router.match('/communications/deleteExternalAttachments').to('Communications.deleteExternalAttachments');
router.get('/communications/TestCommunicationObject').to('Communications.TestCommunicationObject');
router.get('/communications/TestAutimaticCommunicationObject').to('Communications.TestAutimaticCommunicationObject');



// Contract routes
router.match('/contracts/clearCurrentContract').to('Contracts.clearCurrentContract');
router.match('/contracts/contractStep1').to('Contracts.contractStep1');
router.match('/contracts/contractStep2').to('Contracts.contractStep2');
router.match('/contracts/showContract').to('Contracts.showContract');
router.match('/contracts/save').to('Contracts.save');
router.match('/contracts/index').to('Contracts.index');
router.match('/contracts/search').to('Contracts.search');
router.match('/contracts/saveEmbeddedModel').to('Contracts.saveEmbeddedModel');
router.match('/contracts/deleteEmbeddedModel').to('Contracts.deleteEmbeddedModel');
router.match('/contracts/deleteContracts').to('Contracts.deleteContracts');
router.match('/contracts/deleteContractFromClientOrProgram').to('Contracts.deleteContractFromClientOrProgram');
router.match('/contracts/programContractStep1').to('Contracts.programContractStep1');


// Reps routes
router.match('/reps/clearCurrentRep').to('Reps.clearCurrentRep');
router.match('/reps/repStep1').to('Reps.repStep1');
router.match('/reps/repStep2').to('Reps.repStep2');
router.match('/reps/save').to('Reps.save');
router.match('/reps/saveEmbeddedModel').to('Reps.saveEmbeddedModel');
router.match('/reps/deleteReps').to('Reps.deleteReps');
router.match('/reps/deleteRepFromClient').to('Reps.deleteRepFromClient');
router.match('/reps/getRepsByLevel').to('Reps.getRepsByLevel');
router.match('/reps/saveDefaultItem').to('Reps.saveDefaultItem');
router.match('/reps/createUserAccount').to('Reps.createUserAccount');
router.match('/reps/import').to('Reps.showImport');
router.match('/reps/processFile').to('Reps.import');
router.match('/reps/saveDiff').to('Reps.saveDiff');
router.match('/reps/search').to('Reps.search');
router.match('/reps/deleteEmbeddedModel').to('Reps.deleteEmbeddedModel');
/*
router.match('/reps/showRep').to('Reps.showRep');
router.match('/reps/index').to('Reps.index');
*/

// Venue routes
router.match('/venues/clearCurrentVenue').to('Venues.clearCurrentVenue');
router.match('/venues/venueStep1').to('Venues.venueStep1');
router.match('/venues/venueStep2').to('Venues.venueStep2');
router.match('/venues/venueStep3').to('Venues.venueStep3');
router.match('/venues/venueRoomStep').to('Venues.venueRoomStep');
router.match('/venues/venueStep4').to('Venues.venueStep4');
router.match('/venues/save').to('Venues.save');
router.match('/venues/search').to('Venues.search');
router.match('/venues/saveEmbeddedModel').to('Venues.saveEmbeddedModel');
router.match('/venues/deleteEmbeddedModel').to('Venues.deleteEmbeddedModel');
router.match('/venues/deleteVenues').to('Venues.deleteVenues');
router.match('/venues/deleteVenueFromOthers').to('Venues.deleteVenueFromOthers');
router.match('/venues/addVenueToClient').to('Venues.addVenueToClient');
router.match('/venues/unsetCurrentVenue').to('Venues.unsetCurrentVenue');
router.match('/venues/setCurrentVenueById').to('Venues.setCurrentVenueById');

//Event routes
router.match('/events/clearCurrentEvent').to('Events.clearCurrentEvent');
router.match('/events/getCurrentEvent').to('Events.getCurrentEvent');
router.match('/events/eventStep1').to('Events.eventStep1');
router.match('/events/eventStep2').to('Events.eventStep2');
router.match('/events/eventStep3').to('Events.eventStep3');
router.match('/events/eventStep4').to('Events.eventStep4');
router.match('/events/eventStep5').to('Events.eventStep5');
router.match('/events/eventStep6').to('Events.eventStep6');
router.match('/events/submitStatus').to('Events.submitStatus');
router.match('/events/saveStatusEvent').to('Events.saveStatusEvent');
router.match('/events/save').to('Events.save');
router.match('/events/search').to('Events.search');
router.match('/events/saveEmbeddedModel').to('Events.saveEmbeddedModel');
router.match('/events/deleteEmbeddedModel').to('Events.deleteEmbeddedModel');
router.match('/events/deleteEvents').to('Events.deleteEvents');
router.match('/events/deleteEventFromProgram').to('Events.deleteEventFromProgram');
router.match('/events/addEventToProgram').to('Events.addEventToProgram');
router.match('/events/communications').to('Events.communications');
router.match('/events/deleteCommunications').to('Events.deleteCommunications');
router.match('/events/saveInvitationListContact').to('Events.saveInvitationListContact');
router.match('/events/removeInvitationListContact').to('Events.removeInvitationListContact');
router.match('/events/removeAllInvitations').to('Events.removeAllInvitations');
router.match('/events/prf').to('Events.prf');
router.match('/events/finances').to('Events.finances');
router.match('/events/audienceSearch').to('Events.audienceSearch');
router.match('/events/notes').to('Events.notes');
router.match('/events/eventNoteStep1').to('Events.eventNoteStep1');
router.match('/events/setCurrentEventNoteIndex').to('Events.setCurrentEventNoteIndex');
router.match('/events/saveNote').to('Events.saveNote');
router.match('/events/deleteNotes').to('Events.deleteNotes');


//Attendee routes
router.match('/attendees/clearCurrentAttendee').to('Attendees.clearCurrentAttendee');
router.match('/attendees/deleteAttendees').to('Attendees.deleteAttendees');
router.match('/attendees/addById').to('Attendees.addById');
router.match('/attendees/deleteAttendeeFromEvent').to('Attendees.deleteAttendeeFromEvent');
router.match('/attendees/attendeeStep1').to('Attendees.attendeeStep1');
router.match('/attendees/attendeeStep2').to('Attendees.attendeeStep2');
router.match('/attendees/attendeeStep3').to('Attendees.attendeeStep3');
router.match('/attendees/attendeeStep4').to('Attendees.attendeeStep4');
router.match('/attendees/attendeeStep5').to('Attendees.attendeeStep5');
router.match('/attendees/save').to('Attendees.save');
router.match('/attendees/search').to('Attendees.search');
router.match('/attendees/searchByNPI').to('Attendees.searchByNPI');
router.match('/attendees/searchById').to('Attendees.searchById');
router.match('/attendees/saveEmbeddedModel').to('Attendees.saveEmbeddedModel');
router.match('/attendees/deleteTravelDocuments').to('Attendees.deleteTravelDocuments');
router.match('/attendees/createNewKOL').to('Attendees.createNewKOL');


///////////////  PORTALS  /////////////////////

router.match('/portalreps').to('Portalreps.index');
router.match('/portalreps/getMyProgramRequest').to('Portalreps.getMyProgramRequest');
router.match('/repProgramDetails').to('Portalreps.repProgramDetails');
router.match('/repDetails').to('Portalreps.repDetails');
router.match('/repResources').to('Portalreps.repResources');
router.match('/newProgramRequest').to('Portalreps.newProgramRequest');
router.get('/portalreps/getMyProgramRequest').to('Portalreps.getMyProgramRequest');
router.match('/portalreps/search').to('Portalreps.search');


router.match('/portalmanagers').to('Portalmanagers.index');
router.match('/portalmanagers/getMyProgramRequest').to('Portalmanagers.getMyProgramRequest');
router.match('/managerProgramDetails').to('Portalmanagers.managerProgramDetails');
router.match('/managerDetails').to('Portalmanagers.managerDetails');
router.match('/managerResources').to('Portalmanagers.managerResources');

router.match('/portalcontacts').to('Portalcontacts.index');
router.match('/contactProgramDetails').to('Portalcontacts.contactProgramDetails');
router.match('/contactDetails').to('Portalcontacts.contactDetails');
router.match('/contactResources').to('Portalcontacts.contactResources');
router.match('/getMyEvents').to('Portalcontacts.getMyEvents');

router.match('/portalclients').to('Portalclients.index');
router.match('/portalclients/getClientEvents').to('Portalclients.getClientEvents');
router.match('/clientProgramDetails').to('Portalclients.clientProgramDetails');
router.match('/clientDetails').to('Portalclients.clientDetails');
router.match('/clientResources').to('Portalclients.clientResources');



//////////////////  EVENT REGISTRATION  /////////////////
router.match('/eventregistration').to('EventRegistration.index');
router.match('/eventRegistration').to('EventRegistration.index');
router.match('/eventRegistration/submitRegistrationCode').to('EventRegistration.submitRegistrationCode');
router.match('/eventRegistration/registrationForm').to('EventRegistration.registrationForm');
router.match('/eventRegistration/submitAttendeeInfo').to('EventRegistration.submitAttendeeInfo');
router.match('/eventRegistration/thankyou').to('EventRegistration.thankyou');

// Passport routes
router.post('/auth/local').to('Auth.local');
router.get('/auth/twitter').to('Auth.twitter');
router.get('/auth/twitter/callback').to('Auth.twitterCallback');
router.get('/auth/facebook').to('Auth.facebook');
router.get('/auth/facebook/callback').to('Auth.facebookCallback');
router.get('/auth/yammer').to('Auth.yammer');
router.get('/auth/yammer/callback').to('Auth.yammerCallback');

//Settings
router.match('/settings/save').to('Settings.save');

// Resource routes
router.resource('users');
router.resource('roles');
router.resource('activities');
router.resource('contact_types');
router.resource('program_types');
router.resource('services');
router.resource('attendee_status_types');
router.resource('attendee_types');
router.resource('room_types');
router.resource('attendees');
router.resource('contacts');
router.resource('clients2');
router.resource('clients');
router.resource('programs');
router.resource('events');
router.resource('venues');
router.resource('contracts');
router.resource('reps');
router.resource('settings');

router.match('/:companyName', 'GET').to('Main.login');

exports.router = router;

