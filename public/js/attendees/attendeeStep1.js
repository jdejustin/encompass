var affiliationIndex;

function initAttendeeStep1(newAttendeeFlag)
{
	$('html, body').animate({ scrollTop: 0 }, 'slow');

	initGlobal();

    $(".js-hotel-arrival").datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '-100:+0',
      onSelect: onHotelArrivalDateSelected
    });

	$(".js-hotel-departure").datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '-100:+0',
      onSelect: onHotelDepartureDateSelected
    });

    $(".js-arrival").datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '-100:+0',
      onSelect: onAttendeeArrivalDateSelected
    });

	$(".js-departure").datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '-100:+0',
      onSelect: onAttendeeDepartureDateSelected
    });

    $(".js-attendee-property").focusout(function (){saveAttendeeProperty(this);});
    $(".js-attendee-status-property").focusout(function (){saveAttendeeStatusProperty(this);});
    $(".js-attending-dinner").change(function (){saveAttendingDinnerOption(this);});
    $('.js-monetary-value-hotel').autoNumeric('init', {aSign:'$'});
	$('.js-monetary-value-hotel').focusout(function (){saveAttendeeHotelBilling(this);});
	$(".js-itinerary-received").change(function (){saveAttendeeItineraryReceived(this);});
    $('.js-timePicker').timepicker();
	$('.js-timePicker').on('changeTime', function (){saveAttendeeTimeSelected(this)});
	$('.js-number-only').autoNumeric('init', {aPad: false, aSep: '', vMin: 0, vMax: 9999999999});
	$('.js-record-locator').autoNumeric('init', {aPad: false, aSep: '', vMin: 000000, vMax: 999999});
	$('.js-monetary-value').focusout(function (){saveAttendeeCost(this);});


	$('.deleteSelectedBtn').click(function(){
      showDeleteTravelDocuments(this);
    });

	//init function to call when search is completed
	initClickOnTableRowTravelDocuments();

	onRowClickFunction = initClickOnTableRowTravelDocuments;

	$('#File').on('change', onFileSelected);

	//init the dialog to hide from HTML the first time 
	$( "#dialog-confirm-delete" ).dialog({autoOpen:false}); 
	$( "#uploadFile-form" ).dialog({autoOpen: false});


	/* focus out of the elements */
	$(".contactProperty").focusout(function (){saveProperty(this);});
			
	//add the focus out for each element of the affiliations
	$('.AffiliationElement').focusout(function (){saveToContact(this, 'affiliations', 'Affiliation');});//targetProperty: "affiliations", modelName: "Affiliation"
	$(".AffiliationElement").bind('input',function (){changeElement(this);});
	
	affiliationIndex = $("#AffiliationContainer").children('.FormRowBackground').length-1;

	//var linkedClients = $('#linkedClients');
	//var clientList = $('#clientList');
	

	$(".DegreesGroup").change(function() {saveDegrees();});

	$("#OtherDegreeCheckbox").change(function() {
		otherDegreeCheckBoxChange();
	});


	$( "#dialog-clientLink" ).dialog({autoOpen: false});

    if ($('#linkedClients').attr('mode') == 'false')
    {
    	var successCallback = function ()
    	{
	          var clientName = $('option:selected', clientList).text();
    	      var linkedClients = $('#linkedClients');
    	      linkedClients.append(clientName);
    	};	

    	var closeCallback = function() {
       		if (!linkedToClient)
       		{
   		 		goContactsList();
   		 	}
   		 	//reset flag, just in case
   		 	linkedToClient = false;
          	$( this ).dialog( "close" );
       	};

    	var url = '/contacts/setCurrentClient';

    	initClientLinkDialog(url,successCallback,true);
    } else if ($('#linkedClients').attr('mode') == 'true') {
    	$("#clientList").change(function() {switchData();});
    }

    $(".js-optOutRadio").change(function() {saveOptOutStatus(this);});

  	//disable all fields until we execute a search and find one if we're dealing with a Create New Attendee action
   	checkForNewAttendee(newAttendeeFlag);
}


function saveOptOutStatus(target)
{
	var selection = $(target).val();

	var data = new Object();

	if (selection == "Yes")
	{
		data.optOut = true;
	} else {
		data.optOut = false;
	}

	saveModel(data);
}


function saveAttendeeStatusProperty(target){
	var value = $(target).val();

	var data = {status: value};

	jQuery.ajax({
		url:'/attendees/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			if (data.notificationData)
			{
				jQuery.ajax({
					url:'/communications/sendAutomaticEventRequestNotification',
					datatype:'json',
					type: 'POST',
					async: true,
					data: data.notificationData,
					success:function(data){
		 	    	 	console.log('done.');
					}
	      		});
			}
		}
	});
}

function searchByNPI()
{
	$('#npiSearchError').hide();
	// $('#npiSearchSuccess').hide();
	$('#badNPIError').hide();
	$('#saveAsKOL').hide();

	var data = {npiNumber: $('#npiNumber').val()};

	//make sure npi number is 10 digits long
	if (data.npiNumber.length < 10)
	{
		$('#badNPIError').show();
	} else {

		//make AJAX call to get Contact from npi number
		jQuery.ajax({
			url:'/attendees/searchByNPI',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
			success:function(data){
				if (data.foundContact)
				{
					// $('#npiSearchSuccess').show();
					// checkForNewAttendee(false);
					//reload listing since now we have a currentContact on session
					goAttendeeStep1();
				} else {
					$('#npiSearchError').show();
					$('#addManually').show();
				}
			}
		});
	}
}


function saveAsKOL()
{
	var data = 'contactType=kol';

	jQuery.ajax({
		url:'/contacts/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("saved as KOL");
		}
	});
}

function saveDegrees()
{
	//console.log("Saving degrees");
	var degrees = $('input:checkbox:checked.DegreesGroup').map(function () {
		  return this.value;
		}).get();
	var data = {
			degrees:degrees
		};
	
	saveModel(data);
}


/*this shows and hide the other degree input depending on the other degree checkbox */
function otherDegreeCheckBoxChange()
{
	var tempOtherDegree = $("#OtherDegree").clone();
	$("#OtherDegree").val("");
	$("#OtherDegree").toggleClass("hidden");
	if($("#OtherDegreeCheckbox").prop('checked') == false)
	{
		var data = {
				otherDegree: "",
			};
		saveModel(data);
	}
}


function addAffiliation(target)
{
	affiliationIndex++;
	
	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteAffiliation(this)'>X</button>";

	updatePage(target,deleteButton,affiliationIndex);
	
	// focus out elements to the components 
	$('.AffiliationElement').focusout(function (){saveToContact(this, 'affiliations', 'Affiliation');});//targetProperty: "affiliations", modelName: "Affiliation"
	$(".AffiliationElement").bind('input',function (){changeElement(this);});
}


function deleteAffiliation(target)
{
	affiliationIndex--;
	deleteElement(target,"affiliations",affiliationIndex);
}

function onHotelArrivalDateSelected(dateText, target)
{
	var data = {hotelArrivalDate: dateText};
	saveAttendeeModel(data);
}


function onHotelDepartureDateSelected(dateText, target)
{
	var data = {hotelDepartureDate: dateText};
	saveAttendeeModel(data);
}

function saveAttendingDinnerOption(target)
{
	var data;

	if ($(target).prop('checked') == true)
	{
		data = {attendingDinner: 'true'};
	} else {
		data = {attendingDinner: 'false'};
	}
	
	saveAttendeeModel(data);
}


function saveAttendeeHotelBilling(target)
{
	var attendeeCost = '';

	$(target).parent().parent().find('.js-monetary-value-hotel').each(function() {
		//get rate
		var rAttendeeCost = $(this).autoNumeric('get');
		
		attendeeCost +=  ((rAttendeeCost != '')? rAttendeeCost : 0) + ',';

	});

	//strip last comma from string
	attendeeCost = attendeeCost.slice(0, attendeeCost.length - 1);

	var data = $(target).prop('name') + '=' + attendeeCost;
    
	saveAttendeeModel(data);
}

function initClickOnTableRowTravelDocuments()
{
	$('.mainTable tbody td').not('.checkBoxTd').click(function () {
      displayFile($(this).parent().find('.checkBoxTd').attr('data-path'));
    });

	 //add select class to checkbox
	$('.mainTable tbody td').find('[type=checkbox]').addClass('selectElement');

	//set the behaviour of the select all element
	setSelectionBehaviour();  
}

function createNewTravelDocument(id)
{
	$('#uploadNotes').show();

    fileUploadSuccessFunction = onTravelDocumentFileUploadSuccess;

    uploadTargetId = id;

	var title = 'Upload New Document';
	initUploadFileDialog(title, uploadFileFunction, uploadFileDialogCloseFunction);
}


function onTravelDocumentFileUploadSuccess(returnedData)
{
	var filePath = '/storage/uploadedFiles/' + returnedData.fileFullName;
	var data;
	var totalItems = $('.mainTable tbody tr').length;
	data = {
			value: ["name=" + uploadData.displayName, "pathToFile=" + filePath, "notes=" + uploadData.notes, "link=" + uploadData.link],
			// targetIndex: targetIndextargetIndex, 
			targetIndex: totalItems, 
			targetProperty: 'travelDocuments',
			modelName: 'UploadedFile',
	};

	jQuery.ajax({
		url:'/attendees/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			goAttendeeStep2();
		}
	});
}


function showDeleteTravelDocuments(element)
{
  var title = 'Delete Selected Travel Document(s)?';
  var message = 'Selected Travel Document(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/attendees/deleteTravelDocuments';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element);

}


function onAttendeeArrivalDateSelected(dateText, target)
{
	var data = {travelArrivalDate: dateText};
	saveAttendeeModel(data);
}


function onAttendeeDepartureDateSelected(dateText, target)
{
	var data = {travelDepartureDate: dateText};
	saveAttendeeModel(data);
}

function saveAttendeeItineraryReceived(target)
{
	var data;

	if ($(target).prop('checked') == true)
	{
		data = {travelItineraryReceived: 'true'};
	} else {
		data = {travelItineraryReceived: 'false'};
	}
	
	saveAttendeeModel(data);
}

function saveAttendeeTimeSelected(target)
{
	var validation = $(target).validationEngine('validate');
	if (!validation)
	{
		//start time
		var sTime = $(target).val();
		var startTime =  (isNaN(sTime)? sTime : 0);

		var serializedData =  $(target).attr('name') + '=' + startTime;
		saveAttendeeModel(serializedData);
	}
}

function saveAttendeeCost(target)
{
	var attendeeCost = '';

	$(target).parent().parent().find('.js-monetary-value').each(function() {
		//get rate
		var rAttendeeCost = $(this).autoNumeric('get');
		
		attendeeCost +=  ((rAttendeeCost != '')? rAttendeeCost : 0) + ',';

	});

	//strip last comma from string
	attendeeCost = attendeeCost.slice(0, attendeeCost.length - 1);

	var data = $(target).prop('name') + '=' + attendeeCost;
    
	saveAttendeeModel(data);
}

function addAttendeeManually()
{
	$('#saveAsKOL').show();
	checkForNewAttendee(false);

	var data = {contactType: 'attendee'};

	jQuery.ajax({
		url:'/attendees/createNewKOL',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			//create new attendee
			var attendeeData = {contactId: data.contactId};
			
			saveAttendeeModel(attendeeData);
			//save kol id to attendee
		}
	});
}


function checkForNewAttendee(flag)
{
	$('.contactProperty').prop('disabled', flag);
	$('.AffiliationElement').prop('disabled', flag);
	$('.js-attendee-property').prop('disabled', flag);
	$('.js-attending-dinner').prop('disabled', flag);
	$('.js-monetary-value-hotel').prop('disabled', flag);
	$('.js-itinerary-received').prop('disabled', flag);
	$('.js-timePicker').prop('disabled', flag);
	$('.js-record-locator').prop('disabled', flag);
	$('.js-monetary-value').prop('disabled', flag);
	$('.js-hotel-arrival').prop('disabled', flag);
	$('.js-hotel-departure').prop('disabled', flag);
	$('.js-arrival').prop('disabled', flag);
	$('.js-departure').prop('disabled', flag);
	$('input.DegreesGroup').prop('disabled', flag);
	$('#OtherDegreeCheckbox').prop('disabled', flag);

	if (!flag)
	{
		$('#searchKOL').addClass('hidden');
		$('#eventKOLINfo').removeClass('hidden');
	}
}