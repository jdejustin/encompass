function initAttendeeStep2()
{
	//https://airportcodes.org

	$('html, body').animate({ scrollTop: 0 }, 'slow');

	initGlobal();

	$(".js-arrival").datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '-100:+0',
      onSelect: onAttendeeArrivalDateSelected
    });

	$(".js-departure").datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '-100:+0',
      onSelect: onAttendeeDepartureDateSelected
    });

	$(".js-attendee-property").focusout(function (){saveAttendeeProperty(this);});
    $(".js-itinerary-received").change(function (){saveAttendeeItineraryReceived(this);});

    $('.js-timePicker').timepicker();
	$('.js-timePicker').on('changeTime', function (){saveAttendeeTimeSelected(this)});

	$('.deleteSelectedBtn').click(function(){
      showDeleteTravelDocuments(this);
    });

	//init function to call when search is completed
	initClickOnTableRowTravelDocuments();

	onRowClickFunction = initClickOnTableRowTravelDocuments;

	$('#File').on('change', onFileSelected);

	//init the dialog to hide from HTML the first time 
	$( "#dialog-confirm-delete" ).dialog({autoOpen:false}); 
	$( "#uploadFile-form" ).dialog({autoOpen: false});

	$('.js-number-only').autoNumeric('init', {aPad: false, aSep: '', vMin: 0, vMax: 9999999999});
	$('.js-record-locator').autoNumeric('init', {aPad: false, aSep: '', vMin: 000000, vMax: 999999});
	$('.js-monetary-value').autoNumeric('init', {aSign:'$'});
	$('.js-monetary-value').focusout(function (){saveAttendeeCost(this);});
}

function initClickOnTableRowTravelDocuments()
{
	$('.mainTable tbody td').not('.checkBoxTd').click(function () {
      displayFile($(this).parent().find('.checkBoxTd').attr('data-path'));
    });

	 //add select class to checkbox
	$('.mainTable tbody td').find('[type=checkbox]').addClass('selectElement');

	//set the behaviour of the select all element
	setSelectionBehaviour();  
}

function createNewTravelDocument(id)
{
	$('#uploadNotes').show();

    fileUploadSuccessFunction = onTravelDocumentFileUploadSuccess;

    uploadTargetId = id;

	var title = 'Upload New Document';
	initUploadFileDialog(title, uploadFileFunction, uploadFileDialogCloseFunction);
}


function onTravelDocumentFileUploadSuccess(returnedData)
{
	var filePath = '/storage/uploadedFiles/' + returnedData.fileFullName;
	var data;
	var totalItems = $('.mainTable tbody tr').length;
	data = {
			value: ["name=" + uploadData.displayName, "pathToFile=" + filePath, "notes=" + uploadData.notes],
			// targetIndex: targetIndextargetIndex, 
			targetIndex: totalItems, 
			targetProperty: 'travelDocuments',
			modelName: 'UploadedFile',
	};

	jQuery.ajax({
		url:'/attendees/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			goAttendeeStep2();
		}
	});
}


function showDeleteTravelDocuments(element)
{
  var title = 'Delete Selected Travel Document(s)?';
  var message = 'Selected Travel Document(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/attendees/deleteTravelDocuments';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element);

}


function onAttendeeArrivalDateSelected(dateText, target)
{
	var data = {travelArrivalDate: dateText};
	saveAttendeeModel(data);
}


function onAttendeeDepartureDateSelected(dateText, target)
{
	var data = {travelDepartureDate: dateText};
	saveAttendeeModel(data);
}

function saveAttendeeItineraryReceived(target)
{
	var data;

	if ($(target).prop('checked') == true)
	{
		data = {travelItineraryReceived: 'true'};
	} else {
		data = {travelItineraryReceived: 'false'};
	}
	
	saveAttendeeModel(data);
}

function saveAttendeeTimeSelected(target)
{
	var validation = $(target).validationEngine('validate');
	if (!validation)
	{
		//start time
		var sTime = $(target).val();
		var startTime =  (isNaN(sTime)? sTime : 0);

		var serializedData =  $(target).attr('name') + '=' + startTime;
		saveAttendeeModel(serializedData);
	}
}

function saveAttendeeCost(target)
{
	var attendeeCost = '';

	$(target).parent().parent().find('.js-monetary-value').each(function() {
		//get rate
		var rAttendeeCost = $(this).autoNumeric('get');
		
		attendeeCost +=  ((rAttendeeCost != '')? rAttendeeCost : 0) + ',';

	});

	//strip last comma from string
	attendeeCost = attendeeCost.slice(0, attendeeCost.length - 1);

	var data = $(target).prop('name') + '=' + attendeeCost;
    
	saveAttendeeModel(data);
}