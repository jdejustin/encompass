function setCurrentAttendeeId(id)
{
	jQuery.ajax({
		url:'/attendees/setCurrentAttendee',
		datatype:'json',
		type: 'POST',
		async: true,
		data: id,
		success:function(data){
			console.log("current attendee id set");
		}
	});
}

function saveAttendeeProperty(target)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		//console.log("saving client");
		var serializedData = $(target).serialize();
		saveAttendeeModel(serializedData);
	}
}

function saveToAttendee(target, targetProperty, targetModel)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var data = {
			value: [$(target).attr("name") + "=" + $(target).val()],
			targetIndex: 0,
			targetProperty: targetProperty,
			modelName: targetModel
		};
	    
		saveAttendeeEmbeddedModel(data);
	}
}

function saveAttendeeModel(data)
{
	jQuery.ajax({
		url:'/attendees/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("saved");
		}
	});
}


function saveAttendeeEmbeddedModel(data)
{
	jQuery.ajax({
		url:'/attendees/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("Saved: " + data);
		}
	});
}


function validateAttendeeStep1(functionToCall)
{
	var validation = $('#attendeeStep1ValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}

// Navigation
function goAttendeeDetails()
{
	jQuery('#content').load('/attendees/1234 #AjaxContent',function (data){
		if(data == 'false')
		{
			window.location = "/expired";
		}
		 else if (data == 'null')
	   {
			goAttendeesList();
	   } else {
			initAttendeeShow();
	   }
	});
}


function goAttendeesList()
{
	jQuery('#content').load('/attendees #AjaxContent',function (data){
		if(data == 'false')
		{
			window.location = "/expired";
		}
		else
		{
   	 	initAttendeesIndex();
		}

	});
}

function goAttendeeStep1()
{
	jQuery("#content").load('/attendees/attendeeStep1 #AjaxContent', function (data){
	   if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initAttendeeStep1(false);
	   }

	});
}

function goAttendeeStep2()
{
	jQuery("#content").load('/attendees/attendeeStep2 #AjaxContent', function (data){
	   if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   }

			initContactStep2();
	});
}

function goAttendeeStep3()
{
	jQuery("#content").load('/attendees/attendeeStep3 #AjaxContent', function (data){
	   if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initContactStep3();
	   }
	});
}

function goAttendeeStep4()
{
	jQuery("#content").load('/attendees/attendeeStep4 #AjaxContent', function (data){
	   if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initContactStep4();
	   }
	});
}

function goAttendeeStep5()
{
	jQuery("#content").load('/attendees/attendeeStep5 #AjaxContent', function (data){
	   if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initContactStep5();
	   }
	});
}

//<!-- STUTZENV3 -->
    function openPrior() {
        jQuery('.show-select-options').slideToggle();
    }
