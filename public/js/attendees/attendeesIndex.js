function initAttendeesIndex() {

  $('html, body').animate({ scrollTop: 0 }, 'slow');

  initGlobal();
  
  $('#advancedSearch').click(function(){
    //display advanced search options
    $('#advancedSearchOptions').toggle();
  });


	$('.deleteSelectedBtn').click(function(){
    	showDeleteAttendees(this);
    });

$('#addpriorbtn').click(function(){
    	showPriorAttendees(this);
    });

initPriorAttendees();

     jQuery("#PriorSearchButton").click(function () {
        $('#CurrentPage2').val(1);//set to 1 for search for the first page
         jQuery("#PriorSearchForm").submit();
     });

  //init function to call when search is completed
  initClickOnTableRowAttendees();

  onRowClickFunction = initClickOnTableRowAttendees;

  $( "#dialog-confirm-delete" ).dialog({autoOpen: false});
};


function showPriorAttendees(element)
{
  //create dialog
  var title = 'Add Attendees(s)?';
  var message = 'Selected attendees will be added to this event, Are you sure?';
  var successUrl = '/attendees/addById';
  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,successUrl,element,goAttendeesList);
}


function initPriorAttendees()
{
$('#PriorSearchForm').ajaxForm({
         success: function () {
         },
         complete: function (xhr) {
             var data = jQuery.parseJSON(xhr.responseText);
             injectRows(data, ".mainTable2" );
             //set currentPage and recordCount
             updatePageNavigation(data);
         }
      });
}

function initClickOnTableRowAttendees()
{
  $('.mainTable tbody td').not('.checkBoxTd').click(function (){
    jQuery("#content").load('/attendees/' + $(this).parent().find('[type=checkbox]').val() + ' #AjaxContent', function (data){
      if(data == 'false')
       {
          window.location = "/expired";
       }
       else
       {
         initAttendeeShow(false);
       }
    });
  });

  //add select class to checkbox
  $('.mainTable tbody td').find('[type=checkbox]').addClass('selectElement');

  //set the behaviour of the select all element
  setSelectionBehaviour();  
}


function addPriorAttendees(element)
{
  var callBack = function (data)
  {
     //finally, remove programs from clients
     jQuery("#content").load('/attendees/addPriorAttendees #AjaxContent', {'ids': data}, function (data) {
       if(data == 'false')
       {
          window.location = "/expired";
       }
       else
       {
        goAttendeesList();
          
       }
     });
  }

  var title = 'Add Selected Attendees(s)?';
  var message = 'Selected Attendees(s) will be added to this event. Are you sure?';
  var deleteUrl = '/attendees/searchById';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element, callBack);
}

function showDeleteAttendees(element)
{
  var callBack = function (data)
  {
     //finally, remove programs from clients
     jQuery("#content").load('/attendees/deleteAttendeeFromEvent #AjaxContent', {'ids': data}, function (data) {
       if(data == 'false')
       {
          window.location = "/expired";
       }
       else
       {
        goAttendeesList();
          
       }
     });
  }

  var title = 'Delete Selected Attendees(s)?';
  var message = 'Selected Attendees(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/attendees/deleteAttendees';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element, callBack);
}


function createNewAttendee()
{
	jQuery("#content").load('/attendees/clearCurrentAttendee #AjaxContent', function (data){
     if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
        jQuery("#content").load('/attendees/attendeeStep1 #AjaxContent', function (){
      		initAttendeeStep1(true);
        });
     }
	});
}  