function initAttendeeShow() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');

	initGlobal();

	$('#editContactBtn').click(function(){
		jQuery("#content").load('/attendees/attendeeStep1 #AjaxContent', function (){
			initAttendeeStep1();
		});
	});

	//load photo, if any

    var path = $('#contactImg').attr('data-pathToFile');
    getUserPhoto(path.substring(20), 'contactImg');
}