function initClientResources() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
  	initGlobal();

  	//set the behaviour of the select all element
	setSelectionBehaviour();  

	$('#deleteKolResourceButton').click(function(){
		showDeleteClientKolResources(this);
	});
	
	$('#deleteRepResourceButton').click(function(){
		showDeleteClientRepResources(this);
	});
	
	$('#deleteRSMResourceButton').click(function(){
		showDeleteClientRSMResources(this);
	});

	//init function to call when search is completed
	initClickOnTableRowClientResources();

	$('#File').on('change', onFileSelected);

	$( "#dialog-confirm-delete" ).dialog({autoOpen: false});
	$( "#uploadFile-form" ).dialog({autoOpen: false});
}

function initClickOnTableRowClientResources()
{
    $('.mainTable tbody td').not('.checkBoxTd').click(function () {
    	//highlight selected row
		$(this).parent('tr').addClass("selected").siblings().removeClass("selected");
		
		var path = $(this).parent().find('.checkBoxTd').attr('data-path');
		var displayName = $(this).parent().find('.checkBoxTd').attr('data-displayName');

		if (path.indexOf('.key') == -1)
		{
   			displayFile(path, displayName);
   		} 
    });
}

function createNewClientResource(createButton,targetProperty,id)
{
	//container de resources
	var $resourceContainer = $(createButton).parents('.js-resource-container');
	var totalItems = $resourceContainer.find('.mainTable tbody tr').length;

	$('#uploadNotes').show();

	fileUploadSuccessFunction = function (returnedData){
		var filePath = '/storage/uploadedFiles/' + returnedData.fileFullName;
			var data;
			

			data = {
					value: ["name=" + uploadData.displayName, "pathToFile=" + filePath, "notes=" + uploadData.notes, "link=" + uploadData.link],
					targetIndex: totalItems, 
					targetProperty: targetProperty,
					modelName: 'UploadedFile',
			};

			jQuery.ajax({
				url:'/clients/saveEmbeddedModel',
				datatype:'json',
				type: 'POST',
				async: true,
				data: data,
				success:function(data){
					goToClientResources();
				}
			});
	}
  	uploadTargetId = id;

	var title = 'Upload New Document';
	//upload file funcion is a function in the global the default function 
	initUploadFileDialog(title, uploadFileFunction, uploadFileDialogCloseFunction);
}


function showDeleteClientKolResources(element)
{
  var title = 'Delete Selected Resource(s)?';
  var message = 'Selected Resource(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/clients/deleteKolResources';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element);

}

function showDeleteClientRepResources(element)
{
  var title = 'Delete Selected Resource(s)?';
  var message = 'Selected Resource(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/clients/deleteRepResources';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element);

}

function showDeleteClientRSMResources(element)
{
  var title = 'Delete Selected Resource(s)?';
  var message = 'Selected Resource(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/clients/deleteRSMResources';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element);

}