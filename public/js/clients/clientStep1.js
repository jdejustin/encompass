var brandIndex;
var presentationTitleIndex;
var productIndex;
var addressIndex;

function initClientStep1() {
	$('.js-number-only').autoNumeric('init', {aPad: false, aSep: '', lZero:'allow'});
	
	/* focus out of the elements */
	$(".ClientProperty").focusout(function (){saveClientProperty(this);});
	
	$('.AddressElement').focusout(function (){saveToClientLevel1(this, 'addresses', 'Address');});
	$(".AddressElement").bind('input',function (){changeClientField(this);});
	
	$('.productElement').focusout(function (){saveToClientLevel1(this, 'products', 'ClientProduct');});
	$(".productElement").bind('input',function (){changeClientField(this);});

	$('.brandElement').focusout(function (){saveToClientLevel1(this, 'brands', 'ClientBrand');});
	$(".brandtElement").bind('input',function (){changeClientField(this);});

	$('.presentationTitleElement').focusout(function (){saveToClientLevel1(this, 'presentationTitles', 'ClientPresentationTitle');});
	$(".presentationTitleElement").bind('input',function (){changeClientField(this);});
	
	$('.internationalCheckbox').change(function (){internationalClientStep1Change(this);});
	
	addressIndex = $("#AddressContainer").children('.FormRowBackground').length-1;
	productIndex = $("#ProductContainer").children('.FormRowBackground').length-1;
	brandIndex = $("#BrandContainer").children('.FormRowBackground').length-1;
	presentationTitleIndex = $("#PresentationTitleContainer").children('.FormRowBackground').length-1;
}


///////////  ADDRESS /////////////////
function internationalClientStep1Change(target)
{
	$(target).parents('.js-client-contact-row').find("[name='state']").toggleClass('hidden');	
	$(target).parents('.js-client-contact-row').find("[for='Country']").toggleClass('hidden');	
	$(target).parents('.js-client-contact-row').find(".country").toggleClass('hidden');

	var targetIndex = $(target).parents('.js-client-contact-row').find('.index').val();
	var data;
	
	if ($(target).prop("checked"))
	{
		//checked
		$(target).parents('.js-client-contact-row').find('.stateLabel').text("State/Province:");
		var data = {
				value: ["international=true"],
				targetIndex: targetIndex, targetProperty: "addresses",
				modelName: "Address"
			};
	}
	else
	{
		//uncheck
		$(target).parents('.js-client-contact-row').find('.stateLabel').text("State:");
		var data = {
				value: ["international=false","state="+$("[name='state']").val(),"country="+$("[name='country']").val()],
				targetIndex: targetIndex, targetProperty: "addresses",
				modelName: "Address"
			};
	}
	
	//enable the delete buttons
	var container = $(target).parents('.js-client-contact-row').parent();
	enableDeleteButtonXByContainer(container);
	enableAddButtonByContainer(container);
	
	saveClientEmbeddedModel(data);
}


function addProduct(target)
{
	productIndex++;

	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);

	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteProduct(this)'>X</button>";
	updateClientPage(target,deleteButton,productIndex);
	
	// add focus out to the components 
	$('.productElement').focusout(function (){saveToClientLevel1(this, 'products', 'ClientProduct');});
	//$(".productElement").bind('input',function (){changeElement(this);});
	//bind the last element by container
	bindClientTheLastRowChangeByAddButtonContainer(addButtonContainer);
}


function deleteProduct(target)
{
	productIndex--;
	deleteClientElementLevel1(target,"products",productIndex);
}



function addBrand(target)
{
	brandIndex++;

	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);

	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteBrand(this)'>X</button>";
	updateClientPage(target,deleteButton,brandIndex);
	
	// add focus out to the components 
	$('.brandElement').focusout(function (){saveToClientLevel1(this, 'brands', 'ClientBrand');});

	//bind the last element by container
	bindClientTheLastRowChangeByAddButtonContainer(addButtonContainer);
}


function deleteBrand(target)
{
	brandIndex--;
	deleteClientElementLevel1(target,"brands",brandIndex);
}


function addPresentationTitle(target)
{
	presentationTitleIndex++;

	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);

	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deletePresentationTitle(this)'>X</button>";
	updateClientPage(target,deleteButton,presentationTitleIndex);
	
	// add focus out to the components 
	$('.presentationTitleElement').focusout(function (){saveToClientLevel1(this, 'presentationTitles', 'ClientPresentationTitle');});

	//bind the last element by container
	bindClientTheLastRowChangeByAddButtonContainer(addButtonContainer);
}


function deletePresentationTitle(target)
{
	presentationTitleIndex--;
	deleteClientElementLevel1(target,"presentationTitles",presentationTitleIndex);
}


function addClientAddress(target)
{
	addressIndex++;

	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);
	
	//create the delete button
   var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteClientAddress(this)'>X</button>";

	var newElement = updateClientPage(target,deleteButton,addressIndex);

	//hide the country
	newElement.find(".country").addClass('hidden');
	newElement.find("[for='Country']").addClass('hidden');
	//set the state input
	newElement.find("input[name='state']").addClass('hidden');	
	newElement.find("select[name='state']").removeClass('hidden');	
	//set text for state
	newElement.find('.stateLabel').text("State:");
	
	
	// focus out elements to the components 
	$('.AddressElement').focusout(function (){saveToClientLevel1(this, 'addresses', 'Address');});//targetProperty: "addresses",	modelName: "Address"
	//$(".AddressElement").bind('input',function (){changeElement(this);});
	//bind the last element by container
	bindClientTheLastRowChangeByAddButtonContainer(addButtonContainer);
	$('.internationalCheckbox').change(function (){internationalClientStep1Change(this);});
}

function deleteClientAddress(target)
{
	addressIndex--;
	deleteClientElementLevel1(target,"addresses",addressIndex);
}


