var financeNumberIndex;
var clientContactIndex;
function initClientStep2() {

	//$('#clientContactDialog').dialog({autoOpen: false});

	initGlobal();
	
	financeNumberIndex = $("#FinanceContactNumberContainer").children('.FormRowBackground').length-1;
	
	$('.FinanceElement').focusout(function (){saveFinance(this);});
	//$(".FinanceElement").bind('input',function (){changeClientElement(this);});
	$('.FinanceNumberElement').focusout(function (){saveNumberFinance(this);});
	$(".FinanceNumberElement").bind('input',function (){changeClientElement(this);});

	$('.mainTable tbody td').not('.checkBoxTd').click(function(){
    	
    	//get the currentClientContactIndex
	    clientContactIndex = $(this).parent().find('[type=checkbox]').val();
	    
	    //open dialog 
	    goContactClientStep(clientContactIndex);
	  });

	/* focus out of the elements */
	setClientFocusElements();


	//set the behaviour of the select all element
  	setSelectionBehaviour();  

	$('.deleteSelectedBtn').click(function(){
      showDeleteClientContacts(this);
    });

    //init the dialog to hide from HTML the first time 
  	$( "#dialog-confirm-delete" ).dialog({autoOpen:false}); 

  	//start the validation engine 
	$('#ClientStep2ValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});
}


function setDegrees(clientContact)
{
	var $degreeContainer = $('.js-degrees-container');

	degreesArray = clientContact.degrees.split(",");
	
	if (degreesArray.length >1)
	{
		for (index = 0; index < degreesArray.length; ++index) 
		{
			$degreeContainer.find("[value='"+degreesArray[index]+"']").prop("checked",true);
		}
	}

	if (clientContact.otherDegree != '')
	{
		$degreeContainer.find('#OtherCheckbox').prop('checked', true);
		$degreeContainer.find('[name="otherDegree"]').removeClass("hidden");
		$degreeContainer.find('[name="otherDegree"]').prop("value",clientContact.otherDegree);
	}
}



function createNewContact()
{
	clientContactIndex = $(".mainTable tbody tr").length;
	console.log("client contact index"+clientContactIndex);
	goContactClientStep();//open the step without contact 
}

function showDeleteClientContacts(element)
{
  //create dialog
  var title = 'Delete Selected Contact\'s Client(s)?';
  var message = 'Selected Contact\'s Client(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/clients/deleteContactClients';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element);
}

function setClientFocusElements()
{
	$(".PhoneNumber").intlTelInput();
}


function saveToClientContact(subTarget,subTargetProperty,subTargetModel)
{
	
	if($(subTarget).val() && $(subTarget).val() != selectPrompt)
	{
		var subTargetIndex = $(subTarget).parents('.js-row').find('.js-index').val();
		
		var data = {
				value: [$(subTarget).attr("name") + "=" + $(subTarget).val()], 
				targetIndex: clientContactIndex, subTargetIndex: subTargetIndex, 
				targetProperty: "clientContacts", subTargetProperty: subTargetProperty,
				modelName: "ClientContact", subModelName: subTargetModel
		};
		
		//console.log ('saving to client contact'+JSON.stringify(data));
		saveClientEmbeddedModel(data);
	}
}


function internationalClientChange(target)
{
	$(target).parents('.js-row').find("[name='state']").toggleClass('hidden');	
	$(target).parents('.js-row').find("[for='Country']").toggleClass('hidden');	
	$(target).parents('.js-row').find(".js-country").toggleClass('hidden');

	
	var subTargetIndex = $(target).parents('.js-row').find('.js-index').val();
	var data;
	
	if ($(target).prop("checked"))
	{
		//checked
		$(target).parents('.js-row').find('.stateLabel').text("State/Province:");
		var data = {
				value: ["international=true"],
				targetIndex: clientContactIndex, targetProperty: "clientContacts",
				subTargetIndex: subTargetIndex, subTargetProperty: "addresses",
				modelName: "ClientContact",subModelName: "Address"
			};
	}
	else
	{
		//uncheck
		$(target).parents('.js-row').find('.stateLabel').text("State:");
		var data = {
				value: ["international=false","state="+$(target).parents('.js-row').find("[name='state']").val(),"country="+$(target).parents('.js-row').find("[name='country']").val()],
				targetIndex: clientContactIndex, targetProperty: "clientContacts",
				subTargetIndex: subTargetIndex, subTargetProperty: "addresses",
				modelName: "ClientContact",subModelName: "Address"
			};
	}
	
	
	//enable the delete buttons
	var container = $(target).parents('.js-row').parent();
	enableClientDeleteButtonXByContainer(container);
	enableClientAddButtonByContainer(container);
	

	console.log('international data'+ JSON.stringify(data));

	saveClientEmbeddedModel(data);
}


///////////  FINANCE CONTACT /////////////////
function addFinanceContactNumber(target)
{
	financeNumberIndex++;

	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);
	
	//create the delete button
	var deleteButton = '<button class="DeleteButtonX btn btn-danger" onclick="javascript:deleteFinance(this)">X</button>';

	 updateClientPage(target,deleteButton,financeNumberIndex);
	
	// focus out elements to the components 
	$('.FinanceNumberElement').focusout(function (){saveNumberFinance(this);});
	//$(".FinanceNumberElement").bind('input',function (){changeClientElement(this);});
	//bind the last element by container
	bindClientTheLastRowChangeByAddButtonContainer(addButtonContainer);
	
	$(".PhoneNumber").intlTelInput();
}

function saveFinance(target)
{
	var validation = $(target).validationEngine('validate');
	if (!validation)
	{
		var data = {
			value: [$(target).attr("name") + "=" + $(target).val()], targetIndex:"0", targetProperty: "financeContact",
			modelName: "ContactInfo"
		};
		saveClientEmbeddedModel(data);
	}
}

function saveNumberFinance(target)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var subTargetIndex = $(target).parents('.js-client-contact-row').find('.index').val();
		
		var data = {
				value: [$(target).attr("name") + "=" + $(target).val()], targetIndex:"0", subTargetIndex: subTargetIndex, 
				targetProperty: "financeContact", subTargetProperty: "phoneNumbers",
				modelName: "ContactInfo", subModelName: "ContactPhoneNumber"
		};
		
		//enable the delete buttons
		//var container = $(target).parent().parent().parent();
		var container = $(target).parents('.js-client-contact-row').parent();
		enableClientDeleteButtonXByContainer(container);
		enableClientAddButtonByContainer(container);
		//enable the client contact add button 

		saveClientEmbeddedModel(data);
	}
}

function deleteFinance(target)
{
	financeNumberIndex--;
	deleteSubClientElement(target,"financeContact",financeNumberIndex,"phoneNumbers");
}

function deleteContactClientElement(targetProperty,indexToDelete)
{
	data = {
		targetIndex:clientContactIndex,targetProperty:"clientContacts",
		subTargetIndex: indexToDelete, subTargetProperty: targetProperty,
	};
	
	jQuery.ajax({
		url:'/clients/deleteEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
	});
}


function validateClientStep2(functionToCall)
{
	var validation = $('#ClientStep2ValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}