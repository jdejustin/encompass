function initClientStep3() {
	$(".ClientProperty").focusout(function (){saveClientProperty(this);});

	$(".salesForceLabelsLevel1Group").change(function() {saveLabels("checkbox", "salesForceLabelsLevel1");});
	$(".salesForceLabelsLevel2Group").change(function() {saveLabels("checkbox", "salesForceLabelsLevel2");});
	$(".salesForceLabelsLevel3Group").change(function() {saveLabels("checkbox", "salesForceLabelsLevel3");});
	$(".medicalAffairsGroup").change(function() {saveLabels("checkbox", "medicalAffairs");});
	$(".payorsGroup").change(function() {saveLabels("checkbox", "payors");});

	//start the validation engine 
	$('#clientStep3ValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});
}

function saveLabels(type, value)
{
	var label = $('input:' + type + ':checked.' + value + 'Group').map(function () {
		  return this.value;
		}).get();

	var data = new Object();
	data[value] = label;

	saveClientModel(data);
}

function validateClientStep3(functionToCall)
{
	var validation = $('#clientStep3ValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}