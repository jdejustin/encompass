///////////////////////////////////////////////////////////
///////////  COMMON FUNCTIONS FOR CLIENTS /////////////////
///////////////////////////////////////////////////////////
function saveClientProperty(target)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		//console.log("saving client");
		var serializedData = $(target).serialize();
		saveClientModel(serializedData);
	}
}

function saveClientModel(data)
{
	jQuery.ajax({
		url:'/clients/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("saved");
		}
	});
}


function saveClientEmbeddedModel(data)
{
	console.log('save client embeded model');
	jQuery.ajax({
		url:'/clients/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("Saved: " + JSON.stringify(data))
		}
	});
}


function showClientDeleteButtonX(elementIndex,container,deleteButton)
{
	//if we are adding the second element we need to add the delete button to the first element that already exist
	if(elementIndex == 1 )
	{
		container.children('.FormRowBackground:nth-child(1)').children(':nth-child(2)').append(deleteButton);
	}
}

function enableClientDeleteButtonXByContainer(container)
{
	//enable all delete buttons after add the first delete button
	//TODO eliminate the class .DeleteButtonX
	$(container).find(".DeleteButtonX").each(function( index ) {
		$(this).prop('disabled',false);	
		$(this).removeClass('Disable');	
	});


	$(container).find(".js-delete-button").each(function( index ) {
		$(this).prop('disabled',false);	
		$(this).removeClass('Disable');	
	});
}

function disableClientDeleteButtonXByContainer(container)
{
   //disable all delete buttons after add the first delete button
	$(container).find(".DeleteButtonX").each(function( index ) {
		$(this).prop('disabled',true);	
		$(this).addClass('Disable');	
	});
}

function enableClientAddButtonByContainer(container)
{
	//enable button	
	$(container).find(".addButton").removeClass("Disable");
	$(container).find(".addButton").prop("disabled",false);
}


//bind the change to the last element by addButton 
function bindClientTheLastRowChangeByAddButtonContainer(addButtonContainer)
{
	console.log('bindTheLastRowChangeByCloneContainer');
	//unbind the old elements
	$(addButtonContainer).find(".js-element").each(function (){
		//$(this).css({"border":"0"});
		$(this).unbind('input');
	});
	
	//bind only for the last child of the rows 
	var $jsRowLastChild = $(addButtonContainer).find(".js-client-contact-row:last");
	$jsRowLastChild.find(".js-element").each(function(){
		//console.log('bind:'+this.name);
		//$(this).css({'border':'1px solid #FF0000'});
		$(this).bind('input',function(){changeClientField(this);});
	});
}


function changeClientField(actualField)
{

	//enable the add button
	$addButton = $(actualField).parents('.js-add-button-container').find('.js-add-button');
	$addButton.prop('disabled',false);
	$addButton.removeClass('Disable');

	//enable the delete buttons
	var container = $(actualField).parents('.js-add-button-container');
	enableClientDeleteButtonXByContainer(container);
	enableClientAddButtonByContainer(container);
   }

/*function changeClientElement(target)
{
	console.log('changeClientElement');
	if ($(target).val() != "")
	{
		enableClientAddButtonByContainer($(target).parents('.js-client-contact-row').parent());
	}
}*/

function disableClientAddButtonByContainer(container)
{
	//enable button
	$(container).find("> .addButton").addClass("Disable");
	$(container).find("> .addButton").prop("disabled",true);
}


function saveToClientLevel1(target, targetProperty, targetModel)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
	    var targetIndex = $(target).parents('.js-client-contact-row').find('.index').val(); 
	    
		var data = {
			value: [$(target).attr("name")+"="+$(target).val()],
			targetIndex: targetIndex,
			targetProperty: targetProperty,
			modelName: targetModel
		};
	    
		//enable the delete buttons
		var container = $(target).parents('.js-client-contact-row').parent();
		enableClientDeleteButtonXByContainer(container);
		enableClientAddButtonByContainer(container);
		
		saveClientEmbeddedModel(data);
	}
}

function saveToClientLevel2(target, targetIndex, targetProperty, targetModel)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
	    
		var data = {
			value: [$(target).attr("name")+"="+$(target).val()],
			targetIndex: targetIndex,
			targetProperty: targetProperty,
			modelName: targetModel
		};
	    
		//enable the delete buttons
		var container = $(target).parents('.js-client-contact-panel').parent();
		enableClientDeleteButtonXByContainer(container);
		//enableClientAddButtonByContainer(container);
		
		//console.log('save to client level2 '+JSON.stringify(data));
		saveClientEmbeddedModel(data);
	}
}

//this function update the page copying(cloning) the element the we need to add and also return the copied element in case that we need
//to update personalized things for example in address if we clone an address previously marked as international its add a country so 
//we need to hide country in this case
function updateClientPage(target,deleteButton,indexToUpdate)
{
	//container that we are working on
	var container = $(target).parent();
	
	//add the first delete button to the container
	showClientDeleteButtonX(indexToUpdate, container, deleteButton);
	
	//get the elementToclone
	var elementToClone = $(target).parent().children(":first-child").clone();
	
	//disable all delete buttons after add the first delete button
	disableClientDeleteButtonXByContainer(container);

	//old button
	var oldAddButton = $(target).parent().find(".addButton");
	
	var addButton = oldAddButton.clone();
	//remove the old button 
	oldAddButton.remove();

	//clean the inputs
	$(elementToClone).find("input[type=text]").each(function( index ) {
		$(this).prop('value','');	
	});
	
	//clean the selects
	$(elementToClone).find("select").each(function( index ) {
		$(this).find('option:first-child').prop('selected', true);
	});
	
	//clean the checkboxes
	$(elementToClone).find("input[type=checkbox]").each(function( index ) {
		$(this).removeAttr('checked');
	});

	//clean the intNumberInputs
	var telInputElement = $(elementToClone).find(".intl-tel-input");

	//if exists a telInputElement we need to eliminate this and copy the number input only 
	if (telInputElement.length > 0)
	{
		var phoneElement = $(elementToClone).find(".PhoneNumber");
		telInputElement.before(phoneElement);
		telInputElement.remove();
	}
	
	//add the new element
	$(container).append(elementToClone);

	//append the new button 
	container.append(addButton);
	//disable addButton
	disableClientAddButtonByContainer(container);
	
	//Update the index of the elements 
	container.find(".FormRowBackground").each(function( index ) {
		$(this).find('.index').attr('value',index);	
	});

	//Fix for dealing with datePicker widget on the page, if it exists
	container.find(".FormRowBackground").each(function( index ) {
		$(this).find('.ExpirationDatePicker').removeClass('hasDatepicker');	
	});
	
	return elementToClone;
}



//this function update the page copying(cloning) the element the we need to add and also return the copied element in case that we need
//to update personalized things for example in address if we clone an address previously marked as international its add a country so 
//we need to hide country in this case
function updateTwoLevelPage(target,deleteButton,indexToUpdate)
{
	//container that we are working on
	var container = $(target).parent();
	
	//add the first delete button to the container
	showClientDeleteButtonX(indexToUpdate, container, deleteButton);
	
	//get the elementToclone
   var elementToClone = $('.js-contact-client-template').clone();
   elementToClone.removeClass('js-contact-client-template');
   elementToClone.removeClass('hidden');

	//disable all delete buttons after add the first delete button
	disableClientDeleteButtonXByContainer(container);

	//old button
	var oldAddButton = $(target).parent().find("#AddClientContactButton");
	
	var addButton = oldAddButton.clone();
   addButton.addClass('Disable');
   addButton.prop('disabled',true);
	//remove the old button 
	oldAddButton.remove();
	
	//add the new element
	$(container).append(elementToClone);

	//append the new button 
	container.append(addButton);
	
	//Update the index of the elements 
	container.children(".js-client-contact-panel").each(function( index ) {
		$(this).find('.clientIndex').attr('value',index);	
	});

	return elementToClone;
}

function deleteClientElementLevel1(target,targetProperty,elementIndex)
{
	var indexToDelete = $(target).parents('.js-client-contact-row').find('.index').val();
	clientDeleteByIndex(target, targetProperty, elementIndex, null, indexToDelete);
}

/*function deleteClientElement(target,targetProperty,elementIndex)
{
	var indexToDelete = $(target).parents('.js-client-contact-panel').find('.clientIndex').val();
	clientDeleteByIndex(target, targetProperty, elementIndex, null, indexToDelete);
}*/

//used in the contactStep
function deleteSubClientElement(target,targetProperty,elementIndex,subTargetProperty)
{
	var indexToDelete = $(target).parents('js-client-contact-row').find('.index').val();
	clientDeleteByIndex(target, targetProperty, elementIndex, subTargetProperty, indexToDelete);
}

function clientDeleteByIndex(target,targetProperty,elementIndex,subTargetProperty,indexToDelete)
{
	var container = $(target).parent().parent().parent();
	var addButton = $(container).find('.addButton');
	//check if the affiliation was saved
	var attr = addButton.prop('disabled');
	// For some browsers, `attr` is undefined; for others,
	// `attr` is false.  Check for both.
	if (typeof attr !== 'undefined' && attr !== false) 
	{
		// add button is disable so the affiliation was not saved
		//enable button	
		addButton.removeClass("Disable");
		addButton.prop( "disabled", false );
		
		//enable the delete buttons
		enableClientDeleteButtonXByContainer(container);
	}
	else
	{
		var data;
		if (subTargetProperty)
		{
			data = {
					targetIndex: 0, targetProperty:targetProperty,subTargetIndex:indexToDelete,subTargetProperty: subTargetProperty,
			};
		}
		else
		{
			data = {
					targetIndex: indexToDelete, targetProperty: targetProperty,
			};
		}
	
		jQuery.ajax({
			url:'/clients/deleteEmbeddedModel',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
		});
	}
	
	//removing the elements
	$(target).parent().parent().remove();
	
	container.find(".FormRowBackground").each(function( index ) {
		$(this).find('.Index').attr('value',index);	
	});
	
	//just remove the delete only if we have 1 row
	if (elementIndex == 0 )
	{
		container.find(':first-child').find('.DeleteButtonX').first().remove();
	}
}


// Navigation
function goClientDetails()
{
	jQuery("#content").load('/clients/1234 #AjaxContent', function (data){
		if(data == 'false')
       {
         window.location = "/expired";
       }
        else if (data == 'null')
	   {
			goClientsList();
	   } else {
			initClientsShow();
	   }

	});
}


function goClientsList()
{
	jQuery("#content").load('/clients #AjaxContent', function (data){
		if(data == 'false')
       {
         window.location = "/expired";
       }
       else
       {
			initClientsIndex();
       }
	});
}

function goClientStep1()
{
	jQuery("#content").load('/clients/clientStep1 #AjaxContent', function (data){
		if(data == 'false')
       {
         window.location = "/expired";
       }
       else
       {
			initClientStep1();
       }
	});
}

function goClientStep2()
{
	jQuery("#content").load('/clients/clientStep2 #AjaxContent', function (data){
		if(data == 'false')
		{
			window.location = "/expired";
		}
		else
		{
			initClientStep2();
		}
	});
}

function goClientStep3()
{
	jQuery("#content").load('/clients/clientStep3 #AjaxContent', function (data){
		if(data == 'false')
		{
			window.location = "/expired";
		}
		else
		{
			initClientStep3();
		}
	});
}


function goContactClientStep(currentContactIndex)
{
	if (currentContactIndex)
	{
		jQuery("#content").load('/clients/contactClientStep?index='+currentContactIndex+' #AjaxContent', function (data){
		if(data == 'false')
		{
			window.location = "/expired";
		}
		else
		{
			initContactClientStep();
		}

		});
	}
	else
	{
		jQuery("#content").load('/clients/contactClientStep #AjaxContent', function (data){
		if(data == 'false')
		{
			window.location = "/expired";
		}
		else
		{
			initContactClientStep();
		}

		});
	}
}