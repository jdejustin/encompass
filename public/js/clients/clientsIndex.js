$(function (){
	goToClientsHome();
  clientIndexLoaded = true;
});


function initClientsIndex() {
  $('html, body').animate({ scrollTop: 0 }, 'slow');
  
  updateNavigation('clients');

  initGlobal();


  $('#advancedSearch').click(function(){
    //display advanced search options
    $('#advancedSearchOptions').toggle();
  });

	$('.deleteSelectedBtn').click(function(){
      showDeleteClients(this);
    });


  //init function to call when search is completed
  initClickOnTableRowClients();

  onRowClickFunction = initClickOnTableRowClients;

  //init the dialog to hide from HTML the first time 
  $( "#dialog-confirm-delete" ).dialog({autoOpen:false}); 



};

function initClickOnTableRowClients()
{
    $('.mainTable tbody td').not('.checkBoxTd').click(function () {
        jQuery("#content").load('/clients/' + $(this).parent().find('[type=checkbox]').val() + ' #AjaxContent', function (data) {
          if(data == 'false')
          {
            window.location = "/expired";
          }
          else
          {
            initClientsShow();
          }
        });

    });

     //add select class to checkbox
    $('.mainTable tbody td').find('[type=checkbox]').addClass('selectElement');
    
    //set the behaviour of the select all element
    setSelectionBehaviour();  
}

function showDeleteClients(element)
{
  //create dialog
  var title = 'Delete Selected Client(s)?';
  var message = 'Selected Client(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/clients/deleteClients';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element);
}




function createNewClient()
{
	jQuery("#content").load('/clients/clearCurrentClientId #AjaxContent', function (data){
    if(data == 'false')
    {
      window.location = "/expired";
    }
    else
    {
       jQuery("#content").load('/clients/clientStep1 #AjaxContent', function (){
        initClientStep1();
      });
    }
	});
}    