var contactEmailIndex;
var contactAdressIndex;
var contactNumberIndex;
var contactOfficeNumberIndex;
var contactAdminAdressIndex;
//the client contact index used here is defined in the clientStep2.js

function initContactClientStep() {

	initGlobal();
	
	contactEmailIndex = $("#ClientContactEmailContainer").children('.FormRowBackground').length-1;
	contactNumberIndex = $("#ClientContactNumberContainer").children('.FormRowBackground').length-1;
	contactAdressIndex = $("#ClientContactAddressContainer").children('.FormRowBackground').length-1;
	contactOfficeNumberIndex = $("#OfficeContactNumberContainer").children('.FormRowBackground').length-1;
	contactAdminAdressIndex = $("#AdminContactAddressContainer").children('.FormRowBackground').length-1;
	/* focus out of the elements */
	setContactClientFocusElements();

	
	$("#ClientContactForm").validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});
}


function setDegrees(clientContact)
{
	var $degreeContainer = $('.js-degrees-container');

	degreesArray = clientContact.degrees.split(",");
	
	if (degreesArray.length >1)
	{
		for (index = 0; index < degreesArray.length; ++index) 
		{
			$degreeContainer.find("[value='"+degreesArray[index]+"']").prop("checked",true);
		}
	}

	if (clientContact.otherDegree != '')
	{
		$degreeContainer.find('#OtherCheckbox').prop('checked', true);
		$degreeContainer.find('[name="otherDegree"]').removeClass("hidden");
		$degreeContainer.find('[name="otherDegree"]').prop("value",clientContact.otherDegree);
	}
}


function setContactClientFocusElements()
{
	$('.js-contact-property').focusout(function (){saveToClientLevel2(this,clientContactIndex,'clientContacts', 'ClientContact');});
	$(".js-contact-property").bind('input',function (){changeClientField(this);});

	$('.js-email-element').focusout(function (){saveClientContactEmail(this);});
	$(".js-email-element").bind('input',function (){changeClientField(this);});
		
	$('.js-phone-number-element').focusout(function (){saveClientContactNumber(this);});
	$(".js-phone-number-element").bind('input',function (){changeClientField(this);});
	
	$('.js-address-element').focusout(function (){saveClientContactAddress(this);});
	$(".js-address-element").bind('input',function (){changeClientField(this);});
	
	$('.js-admin-address-element').focusout(function (){saveAdminContactAddress(this);});
	$(".js-admin-address-element").bind('input',function (){changeClientField(this);});
	
	$('.js-admin-number-element').focusout(function (){saveOfficeContactNumber(this);});
	$(".js-admin-number-element").bind('input',function (){changeClientField(this);});
	
	$('.js-admin-element').focusout(function (){saveOfficeContact(this);});
	$(".js-admin-element").bind('input',function (){changeClientField(this);});
	
	$('.js-international').change(function (){internationalClientChange(this);});
	$('.js-international-admin').change(function (){internationalAdminChange(this);});
	$(".js-degrees").change(function() {saveDegrees(this);});
	$('[name="otherCheckbox"]').change(function() {otherCheckBoxChange(this);});
	
	$(".PhoneNumber").intlTelInput();
}

function saveDegrees(target)
{
	//console.log("Saving degrees");
	var degrees = $(target).parents('.js-degrees-container').find('input:checkbox:checked.js-degrees').map(function () {
		  return this.value;
		}).get();
	
	//console.log("DREGREES " + degrees);
	var data = {
			degrees:degrees
		};
	
	var data = {
		value: ["degrees=" + degrees], targetIndex: clientContactIndex, targetProperty: "clientContacts",
		modelName: "ClientContact"
	};

	saveClientEmbeddedModel(data);
}


/*this shows and hide the other degree input depending on the other degree checkbox */
function otherCheckBoxChange(target)
{
	var $otherDegree = $(target).parents('.js-degrees-container').find('[name="otherDegree"]');
	$otherDegree.val("");

	$otherDegree.toggleClass("hidden");
	

	if($(target).prop('checked') == false)
	{
		if( ! $otherDegree.hasClass("hidden"))
		{
			$otherDegree.addClass('hidden');	
		}
		//console.log('uncheck clean the other degree');
		//var targetIndex = $(target).parents('.js-client-contact-panel').find('.clientIndex').val(); 
		var data = {
			value: [$otherDegree.attr("name")+"="+$otherDegree.val()],
			//targetIndex: targetIndex,
			targetIndex: clientContactIndex,
			targetProperty: 'clientContacts',
			modelName: 'ClientContact'
		};
		saveClientEmbeddedModel(data);
	}
	else
	{
		if( $otherDegree.hasClass("hidden"))
		{
			$otherDegree.removeClass('hidden');
		}
	}
}



///////////  CLIENT CONTACT EMAIL/////////////////
function addClientContactEmail(target)
{
	
	contactEmailIndex++;
	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);
	
	//create the delete button
	var deleteButton = '<button class="DeleteButtonX btn btn-danger" onclick="javascript:deleteClientContactEmail(this)">X</button>';
	
	updateClientPage(target,deleteButton,contactEmailIndex);
	
	// focus out elements to the components
	$('.js-email-element').focusout(function (){saveClientContactEmail(this);});
	//bind the last element by container
	bindClientTheLastRowChangeByAddButtonContainer(addButtonContainer);
}


function saveClientContactEmail(target)
{
	//validate email first
	var validation = $(target).validationEngine('validate');
	if($(target).val() && ($(target).val() != selectPrompt) && (!validation))
	{
		var subTargetIndex = $(target).parents('.js-client-contact-row').find('.index').val();
		
		var data = {
				value: [$(target).attr("name") + "=" + $(target).val().trim()], 
				targetIndex: clientContactIndex, 
				subTargetIndex: subTargetIndex, 
				targetProperty: "clientContacts", subTargetProperty: "emailAddresses",
				modelName: "ClientContact", subModelName: "ContactEmailAddress"
		};
		saveClientEmbeddedModel(data);
		//enable the delete buttons
		//var container = $(target).parent().parent().parent();
		/*var container = $(target).parents('js-client-contact-row').parent();
		enableClientDeleteButtonXByContainer(container);
		enableClientAddButtonByContainer(container);
		//enable the client contact add button 
		enableAddButtonByContainerParentLevel();*/
	}
}

function deleteClientContactEmail(target)
{
	contactEmailIndex--;
	deleteSubClientElement(target,"clientContacts",contactEmailIndex,"emailAddresses");
}


///////////  CLIENT CONTACT ADRESS/////////////////
function addClientContactAddress(target)
{
	contactAdressIndex++;
	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);
	
	//create the delete button
	var deleteButton = '<button class="DeleteButtonX btn btn-danger" onclick="javascript:deleteClientContactAddress(this)">X</button>';
	
	var newElement = updateClientPage(target,deleteButton,contactAdressIndex);

	//hide the country
	newElement.find(".country").addClass('hidden');
	newElement.find("[for='Country']").addClass('hidden');
	//set the state input
	newElement.find("input[name='state']").addClass('hidden');	
	newElement.find("select[name='state']").removeClass('hidden');	
	//set text for state
	newElement.find('.stateLabel').text("State:");
	
	$('.js-international').change(function (){internationalClientChange(this);});
	
	// focus out elements to the components
	$('.js-address-element').focusout(function (){saveClientContactAddress(this);});
	//bind the last element by container
	bindClientTheLastRowChangeByAddButtonContainer(addButtonContainer);
}

function saveClientContactAddress(target)
{

	if($(target).val() && $(target).val() != selectPrompt)
	{
		var subTargetIndex;
		
		subTargetIndex = $(target).parents('.js-client-contact-row').find('.index').val();
		
		var data = {
				value: [$(target).attr("name") + "=" + $(target).val().trim()], targetIndex: clientContactIndex, 
				subTargetIndex: subTargetIndex, 
				targetProperty: "clientContacts", subTargetProperty: "addresses",
				modelName: "ClientContact", subModelName: "Address"
		};
		
		//enable the delete buttons
		//var container = $(target).parent().parent().parent();
		/*var container = $(target).parents('js-client-contact-row').parent();
		enableClientDeleteButtonXByContainer(container);
		enableClientAddButtonByContainer(container);
		//enable the client contact add button 
		enableAddButtonByContainerParentLevel();*/
		
		saveClientEmbeddedModel(data);
	}
}

function deleteClientContactAddress(target)
{
	contactAdressIndex--;
	deleteSubClientElement(target,"clientContacts",contactAdressIndex,"addresses");
}

function internationalClientChange(target)
{
	$(target).parents('.js-client-contact-row').find("[name='state']").toggleClass('hidden');	
	$(target).parents('.js-client-contact-row').find("[for='Country']").toggleClass('hidden');	
	$(target).parents('.js-client-contact-row').find(".js-country").toggleClass('hidden');

	
	var subTargetIndex = $(target).parents('.js-client-contact-row').find('.index').val();
	var data;
	
	if ($(target).prop("checked"))
	{
		//checked
		$(target).parents('.js-client-contact-row').find('.stateLabel').text("State/Province:");
		var data = {
				value: ["international=true"],
				targetIndex: clientContactIndex, targetProperty: "clientContacts",
				subTargetIndex: subTargetIndex, subTargetProperty: "addresses",
				modelName: "ClientContact",subModelName: "Address"
			};
	}
	else
	{
		//uncheck
		$(target).parents('.js-client-contact-row').find('.stateLabel').text("State:");
		var data = {
				value: ["international=false","state="+$(target).parents('.js-client-contact-row').find("[name='state']").val(),"country="+$(target).parents('.js-client-contact-row').find("[name='country']").val()],
				targetIndex: clientContactIndex, targetProperty: "clientContacts",
				subTargetIndex: subTargetIndex, subTargetProperty: "addresses",
				modelName: "ClientContact",subModelName: "Address"
			};
	}
	
	
	//enable the delete buttons
	var container = $(target).parents('.js-client-contact-row');
	enableClientDeleteButtonXByContainer(container);
	enableClientAddButtonByContainer(container);
	
	saveClientEmbeddedModel(data);
}


///////////  ADMIN CONTACT ADRESS/////////////////
function addAdminContactAddress(target)
{
	contactAdminAdressIndex++;
	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);

	
	//create the delete button
	var deleteButton = '<button class="DeleteButtonX btn btn-danger" onclick="javascript:deleteAdminContactAddress(this)">X</button>';
	
	updateClientPage(target,deleteButton,contactAdminAdressIndex);
	
	$('.js-international-admin').change(function (){internationalAdminChange(this);});
	
	// focus out elements to the components
	$('.js-admin-address-element').focusout(function (){saveClientContactAddress(this);});
	//bind the last element by container
	bindClientTheLastRowChangeByAddButtonContainer(addButtonContainer);
}

function saveAdminContactAddress(target)
{
	
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var subTargetIndex = $(target).parents('.js-client-contact-row').find('.index').val();
		
		var data = {
				value: [$(target).attr("name") + "=" + $(target).val().trim()], 
				targetIndex: clientContactIndex, 
				subTargetIndex: subTargetIndex, 
				targetProperty: "clientContacts", subTargetProperty: "officeAdminContactAddresses",
				modelName: "ClientContact", subModelName: "Address"
		};
		
		//enable the delete buttons
		/*var container = $(target).parents('js-client-contact-row').parent();
		enableClientDeleteButtonXByContainer(container);
		enableClientAddButtonByContainer(container);
		//enable the client contact add button 
		enableAddButtonByContainerParentLevel();
		*/
		
		saveClientEmbeddedModel(data);
	}
}

function deleteAdminContactAddress(target)
{
	contactAdminAdressIndex--
	deleteSubClientElement(target,"clientContacts",contactAdminAdressIndex,"officeAdminContactAddresses");
}

function internationalAdminChange(target)
{
	$(target).parents('.js-client-contact-row').find("[name='state']").toggleClass('hidden');	
	$(target).parents('.js-client-contact-row').find("[for='Country']").toggleClass('hidden');	
	$(target).parents('.js-client-contact-row').find(".js-country").toggleClass('hidden');
	
	
	var subTargetIndex = $(target).parents('.js-client-contact-row').find('.index').val();
	var data;
	
	if ($(target).prop("checked"))
	{
		//checked
		$(target).parents('.js-client-contact-row').find('.stateLabel').text("State/Province:");
		var data = {
				value: ["international=true"],
				targetIndex: clientContactIndex, targetProperty: "clientContacts",
				subTargetIndex: subTargetIndex, subTargetProperty: "officeAdminContactAddresses",
				modelName: "ClientContact",subModelName: "Address"
		};
	}
	else
	{
		//uncheck
		$(target).parents('.js-client-contact-row').find('.stateLabel').text("State:");
		var data = {
				value: ["international=false","state="+ $(target).parents('.js-client-contact-row').find("[name='state']").val(),"country="+ $(target).parents('.js-client-contact-row').find("[name='country']").val()],
				targetIndex: targetIndex, targetProperty: "clientContacts",
				subTargetIndex: subTargetIndex, subTargetProperty: "officeAdminContactAddresses",
				modelName: "ClientContact",subModelName: "Address"
		};
	}
	
	//enable the delete buttons
	var container = $(target).parents('.js-client-contact-row');
	enableClientDeleteButtonXByContainer(container);
	enableClientAddButtonByContainer(container);
	
	saveClientEmbeddedModel(data);
}




///////////  CLIENT CONTACT NUMBERS/////////////////
function addClientContactNumber(target)
{
	contactNumberIndex++;
	
	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);

	//create the delete button
	var deleteButton = '<button class="DeleteButtonX btn btn-danger" onclick="javascript:deleteClientContactNumber(this)">X</button>';

	 updateClientPage(target,deleteButton,contactNumberIndex);
	
	// focus out elements to the components
	$('.js-phone-number-element').focusout(function (){saveClientContactNumber(this);});
	
	$(".PhoneNumber").intlTelInput();

	//bind the last element by container
	bindClientTheLastRowChangeByAddButtonContainer(addButtonContainer);
}

function saveClientContactNumber(target)
{
	console.log('saveClientContactNumer');
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var subTargetIndex = $(target).parents('.js-client-contact-row').find('.index').val();
		
		var data = {
				value: [$(target).attr("name") + "=" + $(target).val()], 
				targetIndex: clientContactIndex, 
				subTargetIndex: subTargetIndex, 
				targetProperty: "clientContacts", subTargetProperty: "phoneNumbers",
				modelName: "ClientContact", subModelName: "ContactPhoneNumber"
		};
		
		//enable the delete buttons
		/*var container = $(target).parents('.js-client-contact-row').parent();
		enableClientDeleteButtonXByContainer(container);
		enableClientAddButtonByContainer(container);
		//enable the client contact add button 
		enableAddButtonByContainerParentLevel();
		*/
		
		saveClientEmbeddedModel(data);
	}
}

function deleteClientContactNumber(target)
{
	contactNumberIndex--;
	deleteSubClientElement(target,"clientContacts",contactNumberIndex,"phoneNumbers");
}



///////////  OFFICE CONTACT /////////////////
function saveOfficeContact(target)
{
	//validate email first
	var validation = $(target).validationEngine('validate');
	if(!validation)
	{
		var data = {
			value: [$(target).attr("name") + "=" + $(target).val()], 
			targetIndex: clientContactIndex, targetProperty: "clientContacts",
			modelName: "ClientContact", subTargetIndex:"0", subTargetProperty: "officeAdminContact", subModelName:"ContactInfo"
		};
		saveClientEmbeddedModel(data);
	}

}

/*
function deleteOfficeNumber(target)
{
	var index = $(target).parents('.js-client-contact-panel').find('.clientIndex').val();
	clientContactOfficeIndex[index]--;
	deleteSubClientElement(target,"clientContacts",clientContactOfficeIndex,"officeAdminContactPhoneNumbers");
}

*/


///////////  CLIENT OFFICE NUMBERS/////////////////
function addOfficeContactNumber(target)
{
	contactOfficeNumberIndex++;
	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);

	//create the delete button
	var deleteButton = '<button class="DeleteButtonX btn btn-danger" onclick="javascript:deleteClientContactOfficeNumber(this)">X</button>'

	 updateClientPage(target,deleteButton,contactOfficeNumberIndex);
	
	// focus out elements to the components
	$('.js-admin-number-element').focusout(function (){saveOfficeContactNumber(this);});
	
	$(".PhoneNumber").intlTelInput();

	bindClientTheLastRowChangeByAddButtonContainer(addButtonContainer);
}

function saveOfficeContactNumber(target)
{
	console.log('saveOfficeContactNumer');
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var subTargetIndex = $(target).parents('.js-client-contact-row').find('.index').val();
		
		var data = {
				value: [$(target).attr("name") + "=" + $(target).val()], 
				targetIndex: clientContactIndex, subTargetIndex: subTargetIndex, 
				targetProperty: "clientContacts", subTargetProperty: "officeAdminContactPhoneNumbers",
				modelName: "ClientContact", subModelName: "ContactPhoneNumber"
		};
		
		//enable the delete buttons
		/*var container = $(target).parents('.js-client-contact-row').parent();
		//var container = $(target).parent().parent().parent();
		enableClientDeleteButtonXByContainer(container);
		enableClientAddButtonByContainer(container);
		//enable the client contact add button 
		enableAddButtonByContainerParentLevel();
		*/
		saveClientEmbeddedModel(data);
	}
}

function deleteClientContactOfficeNumber(target)
{
	contactOfficeNumberIndex--;
	deleteSubClientElement(target,"clientContacts",contactOfficeNumberIndex,"officeAdminContactPhoneNumbers");
}





function deleteContactClientElement(targetProperty,indexToDelete)
{
	data = {
		targetIndex:clientContactIndex,targetProperty:"clientContacts",
		subTargetIndex: indexToDelete, subTargetProperty: targetProperty,
	};
	
	jQuery.ajax({
		url:'/clients/deleteEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
	});
}

function validateClientStep()
{
	var clientContactFormValidation = $("#ClientContactForm").validationEngine('validate');
	//console.log("clientContainerValidate "+ JSON.stringify(ClientContactFormValidation));
	if(clientContactFormValidation)
	{
		goClientStep2();
	}
}