function initGuidelinesIndex()
{
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	initGlobal();

	//set the behaviour of the select all element
  	setSelectionBehaviour();  

	$('.deleteSelectedBtn').click(function(){
    	showDeleteGuidelines(this);
    });

    //init function to call when search is completed
    initClickOnTableRowGuidelines();

	$('#File').on('change', onFileSelected);

 	$( "#dialog-confirm-delete" ).dialog({autoOpen: false});
 	$( "#uploadFile-form" ).dialog({autoOpen: false});
}


function initClickOnTableRowGuidelines()
{
    $('.mainTable tbody td').not('.checkBoxTd').click(function () {
      displayFile($(this).parent().find('.checkBoxTd').attr('data-path'));
    });
}

function createNewGuideline(id)
{
	$('#uploadNotes').show();

    fileUploadSuccessFunction = onGuidelineFileUploadSuccess;

    uploadTargetId = id;

	var title = 'Upload New Document';
	initUploadFileDialog(title, uploadFileFunction, uploadFileDialogCloseFunction);
}


function onGuidelineFileUploadSuccess(returnedData)
{
	var filePath = '/storage/uploadedFiles/' + returnedData.fileFullName;
	var data;
	var totalItems = $('.mainTable tbody tr').length;
	data = {
			value: ["name=" + uploadData.displayName, "pathToFile=" + filePath, "notes=" + uploadData.notes, "link=" + uploadData.link],
			// targetIndex: targetIndextargetIndex, 
			targetIndex: totalItems, 
			targetProperty: 'guidelines',
			modelName: 'UploadedFile',
	};

	jQuery.ajax({
		url:'/clients/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			onGoToClientGuidelinesClick();
		}
	});
}


function showDeleteGuidelines(element)
{
  var title = 'Delete Selected Guideline(s)?';
  var message = 'Selected Guideline(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/clients/deleteGuidelines';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element);

}

initGuidelinesIndex();
var guidelinesLoaded = true;
