var savedDeliveryFile = true;

function initCommunication() {
}

function initTinyMce()
{
   tinymce.init({
      convert_urls:true,
      relative_urls:false,
      remove_script_host:false,
      selector: "#FileContents",
      height : 300,
      plugins: [
         "advlist link lists preview",
         "searchreplace visualblocks fullscreen",
         "table  paste"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic underline | link | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fieldForce | event | speaker | venue | attendee | finance",
      setup: function(editor) {
         editor.addButton('fieldForce', {
            type: 'menubutton',
            text: "Field Force Tags",
            icon: false,
            menu: [
               {text: 'RSM Full Name', onclick: function() {editor.insertContent('{$RSM_FullName}');}},
               {text: 'Field Force Full Name', onclick: function() {editor.insertContent('{$Rep_FullName}');}},
               {text: 'Field Force Cell Number', onclick: function() {editor.insertContent('{$Rep_PhoneNumber}');}},
               {text: 'Field Force Email', onclick: function() {editor.insertContent('{$Rep_Email}');}},
               {text: 'Portal URL', onclick: function() {editor.insertContent('{$Rep_Email}');}}
               ]
         });

         editor.addButton('event', {
            type: 'menubutton',
            text: "Event Tags",
            icon: false,
            menu: [
               {text: 'CRG Program Manager Full Name', onclick: function() {editor.insertContent('{$CRG_ProgramManager}');}},
               {text: 'CRG Event Manager Full Name', onclick: function() {editor.insertContent('{$CRG_EventManager}');}},
               {text: 'Event Confirmed Date', onclick: function() {editor.insertContent('{$Event_Confirmed_Date}');}},
               {text: 'Job Number', onclick: function() {editor.insertContent('{$Event_JobNumber}');}},
               {text: 'Brand', onclick: function() {editor.insertContent('{$Event_Brand}');}},
               {text: 'Presentation Title', onclick: function() {editor.insertContent('{$Event_PresentationTitle}');}},
               {text: 'Program Type', onclick: function() {editor.insertContent('{$Event_ProgramType}');}},
               {text: 'Event City', onclick: function() {editor.insertContent('{$Event_City}');}},
               {text: 'Event State', onclick: function() {editor.insertContent('{$Event_State}');}},
               {text: 'Event Date Pref1', onclick: function() {editor.insertContent('{$Event_DatePref1}');}},
               {text: 'Event Date Pref2', onclick: function() {editor.insertContent('{$Event_DatePref2}');}},
               {text: 'Event Date Pref3', onclick: function() {editor.insertContent('{$Event_DatePref3}');}},
               {text: 'Event Start Time', onclick: function() {editor.insertContent('{$Event_StartTime}');}},
               {text: 'Event Time Zone', onclick: function() {editor.insertContent('{$Event_TimeZone}');}},
               {text: 'Estimated Meal Cost', onclick: function() {editor.insertContent('{$Event_Estimated_MealCost}');}},
               {text: 'Estimated Attendance', onclick: function() {editor.insertContent('{$Event_Estimated_Attendance}');}},
               {text: 'Invitations Distribution Selections', onclick: function() {editor.insertContent('{$Event_Invitations_Distribution_Selections}');}},
               {text: 'Audience Recruitment Email Count', onclick: function() {editor.insertContent('{$Event_Audience_Recruitment_Email_Count}');}},
               {text: 'Audience Recruitment Hard Copy Count', onclick: function() {editor.insertContent('{$Event_Audience_Recruitment_HardCopy_Count}');}},
            ]
         });

         editor.addButton('speaker', {
            type: 'menubutton',
            text: "Speaker Tags",
            icon: false,
            menu: [
               {text: 'Speaker First Name', onclick: function() {editor.insertContent('{$Speaker_FirstName}');}},
               {text: 'Speaker Last Name', onclick: function() {editor.insertContent('{$Speaker_LastName}');}},
               {text: 'Speaker Full Name', onclick: function() {editor.insertContent('{$Speaker_FullName}');}},
               {text: 'Speaker Status', onclick: function() {editor.insertContent('{$Speaker_Status}');}},
               {text: 'Speaker City/State', onclick: function() {editor.insertContent('{$Speaker_City_State}');}},
               {text: 'Speaker City', onclick: function() {editor.insertContent('{$Speaker_City}');}},
               {text: 'Speaker State', onclick: function() {editor.insertContent('{$Speaker_State}');}},
               {text: 'Speaker Travel Type', onclick: function() {editor.insertContent('{$Speaker_Travel_Type}');}},
               {text: 'Speaker Ground Transportation', onclick: function() {editor.insertContent('{$Speaker_Ground_Transportation}');}},

               {text: 'Speaker Pref1 First Name', onclick: function() {editor.insertContent('{$Event_Pref1_Speaker_FirstName}');}},
               {text: 'Speaker Pref1 Last Name', onclick: function() {editor.insertContent('{$Event_Pref1_Speaker_LastName}');}},
               {text: 'Speaker Pref1 Full Name', onclick: function() {editor.insertContent('{$Event_Pref1_Speaker_FullName}');}},
               {text: 'Speaker Pref1 Status', onclick: function() {editor.insertContent('{$Event_Pref1_Speaker_Status}');}},
               {text: 'Speaker Pref1 City/State', onclick: function() {editor.insertContent('{$Event_Pref1_Speaker_City_State}');}},
               {text: 'Speaker Pref1 City', onclick: function() {editor.insertContent('{$Event_Pref1_Speaker_City}');}},
               {text: 'Speaker Pref1 State', onclick: function() {editor.insertContent('{$Event_Pref1_Speaker_State}');}},
               {text: 'Speaker Pref1 Travel Type', onclick: function() {editor.insertContent('{$Event_Pref1_Speaker_Travel_Type}');}},
               {text: 'Speaker Pref1 Ground Transportation', onclick: function() {editor.insertContent('{$Event_Pref1_Speaker_Ground_Transportation}');}},

               {text: 'Speaker Pref2 First Name', onclick: function() {editor.insertContent('{$Event_Pref2_Speaker_FirstName}');}},
               {text: 'Speaker Pref2 Last Name', onclick: function() {editor.insertContent('{$Event_Pref2_Speaker_LastName}');}},
               {text: 'Speaker Pref2 Full Name', onclick: function() {editor.insertContent('{$Event_Pref2_Speaker_FullName}');}},
               {text: 'Speaker Pref2 Status', onclick: function() {editor.insertContent('{$Event_Pref2_Speaker_Status}');}},
               {text: 'Speaker Pref2 City/State', onclick: function() {editor.insertContent('{$Event_Pref2_Speaker_City_State}');}},
               {text: 'Speaker Pref2 City', onclick: function() {editor.insertContent('{$Event_Pref2_Speaker_City}');}},
               {text: 'Speaker Pref2 State', onclick: function() {editor.insertContent('{$Event_Pref2_Speaker_State}');}},
               {text: 'Speaker Pref2 Travel Type', onclick: function() {editor.insertContent('{$Event_Pref2_Speaker_Travel_Type}');}},
               {text: 'Speaker Pref2 Ground Transportation', onclick: function() {editor.insertContent('{$Event_Pref2_Speaker_Ground_Transportation}');}},

               {text: 'Speaker Pref3 First Name', onclick: function() {editor.insertContent('{$Event_Pref3_Speaker_FirstName}');}},
               {text: 'Speaker Pref3 Last Name', onclick: function() {editor.insertContent('{$Event_Pref3_Speaker_LastName}');}},
               {text: 'Speaker Pref3 Full Name', onclick: function() {editor.insertContent('{$Event_Pref3_Speaker_FullName}');}},
               {text: 'Speaker Pref3 Status', onclick: function() {editor.insertContent('{$Event_Pref3_Speaker_Status}');}},
               {text: 'Speaker Pref3 City/State', onclick: function() {editor.insertContent('{$Event_Pref3_Speaker_City_State}');}},
               {text: 'Speaker Pref3 City', onclick: function() {editor.insertContent('{$Event_Pref3_Speaker_City}');}},
               {text: 'Speaker Pref3 State', onclick: function() {editor.insertContent('{$Event_Pref3_Speaker_State}');}},
               {text: 'Speaker Pref3 Travel Type', onclick: function() {editor.insertContent('{$Event_Pref3_Speaker_Travel_Type}');}},
               {text: 'Speaker Pref3 Ground Transportation', onclick: function() {editor.insertContent('{$Event_Pref3_Speaker_Ground_Transportation}');}},

               {text: 'Confirmed Speaker First Name', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_FirstName}');}},
               {text: 'Confirmed Speaker Last Name', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_LastName}');}},
               {text: 'Confirmed Speaker Full Name', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_FullName}');}},
               {text: 'Confirmed Speaker Degrees', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_Degrees}');}},
               {text: 'Confirmed Speaker Cell Number', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_CellNumber}');}},
               {text: 'Confirmed Speaker Default Email', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_DefaultEmail}');}},
               {text: 'Confirmed Speaker Other Emails', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_OtherEmails}');}},
               {text: 'Confirmed Speaker Address', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_Address}');}},
               {text: 'Confirmed Speaker Address2', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_Address2}');}},
               {text: 'Confirmed Speaker City', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_City}');}},
               {text: 'Confirmed Speaker State', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_State}');}},
               {text: 'Confirmed Speaker Postal Code', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_PostalCode}');}},
               {text: 'Confirmed Speaker W9 Address', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_W9Address}');}},
               {text: 'Confirmed Speaker W9 Address2', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_W9Address}');}},
               {text: 'Confirmed Speaker W9 City', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_W9City}');}},
               {text: 'Confirmed Speaker W9 State', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_W9State}');}},
               {text: 'Confirmed Speaker W9 Postal Code', onclick: function() {editor.insertContent('{$Event_Confirmed_Speaker_W9PostalCode}');}}
            ]
         });

         editor.addButton('venue', {
            type: 'menubutton',
            text: "Venue Tags",
            icon: false,
            menu: [
               {text: 'Venue Type', onclick: function() {editor.insertContent('{$Event_Venue_Type}');}},
               {text: 'Venue Name', onclick: function() {editor.insertContent('{$Event_Venue_Name}');}},
               {text: 'Venue Address 1', onclick: function() {editor.insertContent('{$Event_Venue_Address1}');}},
               {text: 'Venue Address 2', onclick: function() {editor.insertContent('{$Event_Venue_Address2}');}},
               {text: 'Venue City', onclick: function() {editor.insertContent('{$Event_Venue_City}');}},
               {text: 'Venue State', onclick: function() {editor.insertContent('{$Event_Venue_State}');}},
               {text: 'Venue Zip', onclick: function() {editor.insertContent('{$Event_Venue_Zip}');}},
               {text: 'Venue Phone Number', onclick: function() {editor.insertContent('{$Event_Venue_PhoneNumber}');}},
               {text: 'Venue Contact Name', onclick: function() {editor.insertContent('{$Event_Venue_Contact_Name}');}},
               {text: 'Venue Contact Phone', onclick: function() {editor.insertContent('{$Event_Venue_Contact_Phone}');}},
               {text: 'Venue Contact Email', onclick: function() {editor.insertContent('{$Event_Venue_Contact_Email}');}},
               {text: 'Caterer Name', onclick: function() {editor.insertContent('{$Event_Caterer_Name}');}}
            ]
         });

         editor.addButton('attendee', {
            type: 'menubutton',
            text: "Attendee Tags",
            icon: false,
            menu: [
               {text: 'Full Attendee List', onclick: function() {editor.insertContent('{$Event_Attendee_List}');}},
               {text: 'Full Attendee List - Registered', onclick: function() {editor.insertContent('{$Event_Attendee_List_Registered}');}},
               {text: 'Attendee Salutation', onclick: function() {editor.insertContent('{$Attendee_Salutation}');}},
               {text: 'Attendee First Name', onclick: function() {editor.insertContent('{$Attendee_FirstName}');}},
               {text: 'Attendee Last Name', onclick: function() {editor.insertContent('{$Attendee_LastName}');}},
               {text: 'Attendee Suffix', onclick: function() {editor.insertContent('{$Attendee_Suffix}');}},
               {text: 'Attendee Degrees', onclick: function() {editor.insertContent('{$Attendee_Degrees}');}},
               {text: 'Attendee Affiliation Name', onclick: function() {editor.insertContent('{$Attendee_Affiliation_Name}');}},
               {text: 'Attendee Affiliation City', onclick: function() {editor.insertContent('{$Attendee_Affiliation_City}');}},
               {text: 'Attendee Affiliation State', onclick: function() {editor.insertContent('{$Attendee_Affiliation_State}');}},
               {text: 'Attendee Registration Date', onclick: function() {editor.insertContent('{$Attendee_Registration_Date}');}},
               {text: 'Attendee Status', onclick: function() {editor.insertContent('{$Attendee_Status}');}},
               {text: 'Attendee Pin #', onclick: function() {editor.insertContent('{$Attendee_Pin}');}}
            ]
         });

         editor.addButton('finance', {
            type: 'menubutton',
            text: "Finance Tags",
            icon: false,
            menu: [
               {text: 'Fee For Service Amount', onclick: function() {editor.insertContent('{$Event_Fee_Amount}');}},
               {text: 'Fee For Service Check #', onclick: function() {editor.insertContent('{$Event_Check_Number}');}},
               {text: 'Fee For Service Date Paid', onclick: function() {editor.insertContent('{$Event_Date_Paid}');}},
               {text: 'Expenses Amount', onclick: function() {editor.insertContent('{$Event_Expenses_Amount}');}},
               {text: 'Expenses Check #', onclick: function() {editor.insertContent('{$Event_Expenses_Check_Number}');}},
               {text: 'Expenses Date Paid', onclick: function() {editor.insertContent('{$Event_Expenses_Date_Paid}');}}
            ]
         });
      }
   });
}


function initTinyMceEditingDeliverables()
{
   tinymce.init({
      convert_urls:true,
      relative_urls:false,
      remove_script_host:false,
      selector: "#FileEditingDeliverableContents",
      height : 300,
      plugins: [
         "advlist link lists preview",
         "searchreplace visualblocks fullscreen",
         "table  paste"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic underline | link | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fieldForce | event | speaker | venue | attendee | finance",
      setup: function(editor) {
           editor.on('change', function(e) {
               //console.log('change event', e);
               savedDeliveryFile = false;
           });
       }
   });



}
