function initCommunicationsDelivery() {
   
   initGlobal();

   //$('html, body').animate({ scrollTop: 0 }, 'slow');

   //set the behaviour of the select all element
   setSelectionBehaviour();  

   $('.deleteSelectedBtn').click(function(){
   	showDeleteDeliveries(this);
   });


   $('#File').on('change', onFileSelected);

   //init the dialog to hide from HTML the first time 
   $( "#dialog-test-send" ).dialog({autoOpen:false}); 
   $( "#dialog-confirm-delete" ).dialog({autoOpen: false});
   //$( "#uploadFile-form" ).dialog({autoOpen: false});

   initClickOnTableRowDeliveries();

   onRowClickFunction = initClickOnTableRowDeliveries;
   

   //create new communication button 
   $("#CreateCommunicationButton").click( function (){
      jQuery('#content').load('/communications/editDeliveryCommunication #AjaxContent',function (data){
        if(data == 'false')
        {
          window.location = "/expired";
        }
        else
        {
           initGlobal();

          //set the behaviour of the select all element
          //setSelectionBehaviour();  

           //bind the last mail 
          bindTheLastRowMailChangeByCloneContainer($('#EmailContainer'));

          initEditDeliveryListeners();

           $('#ExternalFile').on('change', addExternalAttachment);

           $('#deleteExternalAttachmentsBtn').click(function(){
              deleteExternalAttachment(this);
            });

        }
      });
   });
}




function bindTheLastRowMailChangeByCloneContainer($cloneContainer)
{
  //unbind the old elements
  $cloneContainer.find(".js-element").each(function (){
    console.log('unbind:'+this.name +" val " + $(this).val() );
    //$(this).unbind('input',function (){changeEmailField(this);});
    $(this).unbind('input');
    //$(this).css({'border':'1px solid #FF0000'});
  });
  //bind only for the last child of the rows 
  var $jsRowLastChild = $cloneContainer.find(".js-row:last");
  $jsRowLastChild.find(".js-element").each(function(){
    console.log('bind:'+this.name +" val " + $(this).val() );
    $(this).bind('input',function(){changeEmailField(this);});
    //$(this).css({'border':'1px solid #00FF00'});
  });

  //enable the delete button 
  $jsRowLastChild.find('.DeleteButtonX').prop("disabled",false);
  $jsRowLastChild.find('.DeleteButtonX').removeClass("Disable");
}


function changeEmailField(serviceField)
{
  console.log ("changing");
  //enable the add button
  $addButton = $(serviceField).parents('.js-add-button-container').find('.js-add-button');
  $addButton.prop('disabled',false);
  $addButton.removeClass('Disable');
}



function initClickOnTableRowDeliveries()
{
   console.log("init click in table deliveries")
   $('.mainTable tbody .js-edit-delivery-link').click(function(){
      jQuery("#content").load('/communications/editDeliveryCommunication/' + $(this).parent().find('[type=checkbox]').val() + ' #AjaxContent', function (data){
         if(data == 'false')
         {
            window.location = "/expired";
         }
         else
         {
           //to style the templates' table
           initGlobal();

           //set the behaviour of the select all element
           setSelectionBehaviour();  

           //turn all attachments to selected
           $(".selectElement").prop("checked", "checked");
           $('#deleteExternalAttachmentsBtn').prop('disabled', false);

           //bind the last mail 
           bindTheLastRowMailChangeByCloneContainer($('#EmailContainer'));

           initEditDeliveryListeners();

           $('#ExternalFile').on('change', addExternalAttachment);

           $('#deleteExternalAttachmentsBtn').click(function(){
              deleteExternalAttachment(this);
            });

         }
      });
   });

   //add select class to checkbox
   $('.mainTable tbody td').find('[type=checkbox]').addClass('selectElement');

   //set the behaviour of the select all element
   setSelectionBehaviour(); 
}

function initEditDeliveryListeners(){
  //cancel button 
   $("#CancelButton").click( function (){
      goToDeliveryCommunications();
   });

  $("#individualCheckbox").change(function() {
    if ($('#individualCheckbox').prop('checked'))
    {
      //individual is check so the buttons should be radios 
      $("#radioBoxRecipients").removeClass("hidden");
      $("#checkBoxRecipients").addClass("hidden");
      setValuesFromChecksToRadios();
    }
    else
    {
      //individual is no check so the buttons should be checkboxes 
      $("#radioBoxRecipients").addClass("hidden");
      $("#checkBoxRecipients").removeClass("hidden");
      setValuesFromRadiosToChecks();
    }
  });
  
  $('[type=radio]').change(function (){
    checkToRadioValues();
  })

  $('[type=checkbox][value="Attendees"]').change(function (){
    if ($('[type=checkbox][value="Attendees"]').prop('checked'))
    {
      //show attendees' table 
      $('.js-attendees-table').removeClass('hidden');
      //select all attendees 
      $('.js-attendees-table td .js-email-checkbox').each(function(){
        $(this).attr("checked",true);
      });
      $('.js-attendees-table-label').removeClass('hidden');
    }
    else
    {
      //hide attendees' table 
      $('.js-attendees-table').addClass('hidden');
      $('.js-attendees-table-label').addClass('hidden');
    }
  })

  $('[type=checkbox][value="Speakers"]').change(function (){
    if ($('[type=checkbox][value="Speakers"]').prop('checked'))
    {
      //show speakers' table 
      $('.js-speakers-table').removeClass('hidden');
      //select all speakers 
      $('.js-speakers-table td .js-speaker-checkbox').each(function(){
        $(this).attr("checked",true);
      });
      $('.js-speakers-table-label').removeClass('hidden');

    }
    else
    {
      //hide speakers' table 
      $('.js-speakers-table').addClass('hidden');
      $('.js-speakers-table-label').addClass('hidden');
    }
  })
}


function checkToRadioValues ()
{
  if ($('[type=radio][value="Attendees"]').prop('checked'))
  {
    //show attendees' table 
    $('.js-attendees-table').removeClass('hidden');
    //select all attendees 
    $('.js-attendees-table td .js-email-checkbox').each(function(){
      $(this).attr("checked",true);
    });
    $('.js-attendees-table-label').removeClass('hidden');
  }
  else
  {
    //hide attendees' table 
    $('.js-attendees-table').addClass('hidden');
    $('.js-attendees-table-label').addClass('hidden');
  }

  if ($('[type=radio][value="Speakers"]').prop('checked'))
  {
    //show speakers' table 
    $('.js-speakers-table').removeClass('hidden');
    //select all speakers 
    $('.js-speakers-table td .js-speaker-checkbox').each(function(){
      $(this).attr("checked",true);
    });
    $('.js-speakers-table-label').removeClass('hidden');
  }
  else
  {
    //hide speakers' table 
    $('.js-speakers-table').addClass('hidden');
    $('.js-speakers-table-label').addClass('hidden');
  }
}

function setValuesFromChecksToRadios()
{
  $('#checkBoxRecipients [type=checkbox]').each(function(index){
    $('[type=radio][value="'+$(this).val()+'"]').prop('checked',$(this).prop('checked'));
    $(this).prop('checked','');
  })

  //check if attendee radio is checked to know if the table should be shown 
  checkToRadioValues();
}

function setValuesFromRadiosToChecks()
{
  $('#radioBoxRecipients [type=radio]').each(function(index){
    $('[type=checkbox][value="'+$(this).val()+'"]').prop('checked',$(this).prop('checked'));
    $(this).prop('checked','');
  })
}

function saveDelivery()
{
   //console.log('file content '+ $('#FileContents').html());
   var serializedData = $('#DeliveryForm').serialize();

   console.log("serialize " + JSON.stringify(serializedData));
   if ($('#individualCheckbox').prop('checked'))
   {
      serializedData += '&individualEmails=true';
   }
   else
   {
      serializedData += '&individualEmails=false';
   }

   console.log ("form data "+ JSON.stringify(serializedData));

   jQuery.ajax({
      url:'/communications/saveDelivery',
      datatype:'json',
      type: 'POST',
      async: true,
      data: serializedData,
      success:function(data){
         console.log("saved");
            goToDeliveryCommunications();
      }
   });
}



function showDeleteDeliveries(element)
{
  var selectedTemplatesIds = [];
  
  //collect all checked ids first
  $('.selectElement:checked').each(function(){
    selectedTemplatesIds.push($(this).val());
  });

  var callBack = function (data)
  {
    goToDeliveryCommunications();
  }

  var title = 'Delete Selected Deliveries?';
  var message = 'Selected Deliveries will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/communications/deleteDeliveries';
  var data = {
      ids: selectedTemplatesIds
  };          

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title, message, deleteUrl, element, callBack);
}


///////////  EMAILS /////////////////
function addAdditionalTemplateEmail(target)
{
  //create the delete button
  var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='deleteAdditionalTemplateEmail(event,this)'>X</button>";
  addEmailElementToPage(target,deleteButton);

  //bind the last mail 
  bindTheLastRowMailChangeByCloneContainer($('#EmailContainer'));
  
}



function addEmailElementToPage(target,deleteButton)
{
  //container that we are working on
  var container = $(target).parent();
  
  //add the first delete button to the container
  if ($(container).children('.js-row').length == 1 )
  {
    container.find('.FormRowBackground').children(':last-child').append(deleteButton);
  }
  
  //get the elementToclone
  var elementToClone = $(target).parent().children(":first-child").clone();
  
  //disable all delete buttons after add the first delete button
  disableDeleteButtonXByContainer(container);

  //old button
  var oldAddButton = $(target).parent().find(".addButton");
  
  var addButton = oldAddButton.clone();
  //remove the old button 
  oldAddButton.remove();

  //clean the inputs
  $(elementToClone).find("input[type=text]").each(function( index ) {
    $(this).prop('value',''); 
  });

  //add the new element
  $(container).append(elementToClone);

  //append the new button 
  container.append(addButton);
 
  //disable addButton
  $(container).find(".addButton").addClass("Disable");
  $(container).find(".addButton").prop("disabled",true);
}



function deleteAdditionalTemplateEmail(event,element){
  event.preventDefault();
  //removing the element

  var $jsAddButtonContainer = $(element).parents('.js-add-button-container');
  $(element).parents('.js-row').remove();
  //enable the last delete
  var $jsRowLastChild = $jsAddButtonContainer.find(".js-row:last");
  $jsRowLastChild.find(".DeleteButtonX").addClass("Disable");
  $jsRowLastChild.find(".DeleteButtonX").prop("disabled",true);

  var jsRowCount = $jsAddButtonContainer.find(".js-row").length;

  if (jsRowCount == 1)
  {
    $jsAddButtonContainer.find(".js-row").find('.DeleteButtonX').remove();
  }

  bindTheLastRowMailChangeByCloneContainer($jsAddButtonContainer);
  //enable the add button 
  $jsAddButtonContainer.find(".addButton").removeClass("Disable");
  $jsAddButtonContainer.find(".addButton").prop("disabled",false);
}

function sendTestSample(deliveryId)
{
  var title = 'Send Sample Delivery';

  $( "#dialog-test-send" ).dialog({
      resizable: true,
      height:240,
      width:350,
      title:title,
      modal: true,
      buttons: {
        "Send": function() {
            var data = $("#test-send-form").serialize();
            data += "&deliveryId="+deliveryId;
            jQuery.ajax({
              url:"/communications/sendTestSample",
              datatype:'json',
              type: 'POST',
              async: false,
              data: data,
              success: function (data){
                $mainTable = $('.mainTable');
                /*
                $mainTable.find('tbody tr').each(function(){
                  for (var index in selectedIds) {
                      if ($(this).find('.selectElement').val() == selectedIds[index])
                      {
                          $(this).remove();
                      }
                  }
                });
                  */

                // //this indicates that the table is updated for the sorter by columns
                $mainTable.trigger("update");
              }
            });

            $( this ).dialog( "close" );
        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      }
    });
    $('#dialog-test-send').dialog("open");
}


function editDeliveryFiles(deliveryId)
{ 
  var data = {deliveryId:deliveryId};
  jQuery("#content").load('/communications/editDeliveryFiles/' + deliveryId + ' #AjaxContent', function (data){
    if(data == 'false')
    {
      window.location = "/expired";
    }
    else
    {
      initEditDeliveryFiles();
    }
  });
}




function downloadDelivery(deliveryId)
{ 
  var data = {deliveryId:deliveryId};
  window.location = '/communications/downloadDelivery/'+deliveryId;
  /*jQuery.ajax({
    url:"/communications/downloadDelivery/",
    datatype:'json',
    type: 'POST',
    async: false,
    data: data,
    success: function (data){
      // //this indicates that the table is updated for the sorter by columns
    }
  });*/
}


function sendDelivery(deliveryId)
{
  var data = {deliveryId:deliveryId};
  jQuery.ajax({
    url:"/communications/sendDelivery/",
    datatype:'json',
    type: 'POST',
    async: false,
    data: data,
    success: function (data){
      $('#emailSentMessage').show().delay(3000).fadeOut(400, goToDeliveryCommunications);
    }
  });

}