function initCommunicationsTemplate() {
   initGlobal();

   //$('html, body').animate({ scrollTop: 0 }, 'slow');

   //set the behaviour of the select all element
   setSelectionBehaviour();  

   $('#DeleteTemplate').click(function(){
      showDeleteTemplates(this);
   });

   $('#DeleteDelivery').click(function(){
      showDeleteProgramDeliveries(this);
   });


   $('#File').on('change', onFileSelected);

   $( "#dialog-confirm-delete" ).dialog({autoOpen: false});
   $( "#uploadFile-form" ).dialog({autoOpen: false});

   initClickOnTableRowTemplates();
   initClickOnTableRowProgramDeliveries();

   //onRowClickFunction = initClickOnTableRowTemplates;

   //create new communication button 
  $("#CreateCommunicationButton").click( function (){
      jQuery("#content").load('/communications/clearCurrentTemplate #AjaxContent', function (data){
         if(data == 'false')
         {
            window.location = "/expired";
         }
         else
         {
           jQuery('#content').load('/communications/editTemplateCommunication #AjaxContent',function(){
                
              $('.js-keyup-char').keyup(function () {
                  var charReg = /^\s*[a-zA-Z0-9_\-\s]+\s*$/;
                  $('#titleWarning').hide();
                  var inputVal = $(this).val();

                  if (!charReg.test(inputVal)) {
                      $("#titleWarning").show();
                  } else {
                      $("#titleWarning").hide();
                  }

              });

              initTinyMce();
              //cancel button 
              $("#CancelButton").click( function (){
                jQuery("#content").load('/communications/clearCurrentTemplate #AjaxContent', function (data){
                 if(data == 'false')
                 {
                    window.location = "/expired";
                 }
                 else
                 {
                    goToTemplateCommunications();
                 }
                });
              });
           });
         }
      });
   });

  $("#CreateProgramDeliveryButton").click( function (){
    jQuery('#content').load('/communications/editProgramDeliveryCommunication #AjaxContent',function (data){
      if(data == 'false')
      {
        window.location = "/expired";
      }
      else
      {
        initGlobal();
        //cancel button 
        $("#CancelButton").click( function (){
          goToTemplateCommunications();
        });
      }
    });
  });
}


function initClickOnTableRowTemplates()
{
   console.log("init click in table templates")
   $('#TemplateTable tbody td').not('.checkBoxTd').click(function(){

      jQuery("#content").load('/communications/editTemplateCommunication/' + $(this).parent().find('[type=checkbox]').val() + ' #AjaxContent', function (data){
       if(data == 'false')
       {
          window.location = "/expired";
       }
       else
       {
        $('.js-keyup-char').keyup(function () {
            var charReg = /^\s*[a-zA-Z0-9_\-\s]+\s*$/;
            $('#titleWarning').hide();
            var inputVal = $(this).val();

            if (!charReg.test(inputVal)) {
                $("#titleWarning").show();
            } else {
                $("#titleWarning").hide();
            }

        });


         initTinyMce();
         //cancel button 
         $("#CancelButton").click( function (){
            jQuery("#content").load('/communications/clearCurrentTemplate #AjaxContent', function (data){
               if(data == 'false')
               {
                  window.location = "/expired";
               }
               else
               {
                 goToTemplateCommunications();
               }
            });
         });
       }
      });
  });

  //add select class to checkbox
  $('#TemplateTable tbody td').find('[type=checkbox]').addClass('selectElement');

  //set the behaviour of the select all element
  setSelectionBehaviour(); 
}

function initClickOnTableRowProgramDeliveries()
{
   console.log("init click in table program deliveries")
   $('#DeliveryTable tbody td').not('.checkBoxTd').click(function(){
      jQuery("#content").load('/communications/editProgramDeliveryCommunication/' + $(this).parent().find('[type=checkbox]').val() + ' #AjaxContent', function (data){
         if(data == 'false')
         {
            window.location = "/expired";
         }
         else
         {
           initGlobal();
           //cancel button 
           $("#CancelButton").click( function (){
              jQuery("#content").load('/communications/clearCurrentProgramDelivery #AjaxContent', function (data){
                 if(data == 'false')
                 {
                    window.location = "/expired";
                 }
                 else
                 {
                   goToTemplateCommunications();
                 }
              });
           });
         }
      });
  });

  //add select class to checkbox
  $('#Delivery tbody td').find('[type=checkbox]').addClass('selectElement');

  //set the behaviour of the select all element
  setSelectionBehaviour(); 
}


function saveTemplate()
{
   var ed = tinyMCE.get('FileContents').getContent();
   $('#FileContents').html(ed);

   //console.log('file content '+ $('#FileContents').html());
   var serializedData = $('#TemplateForm').serialize();

   //console.log ("form data "+ serializedData);
   jQuery.ajax({
      url:'/communications/saveTemplate',
      datatype:'json',
      type: 'POST',
      async: true,
      data: serializedData,
      success:function(data){
         console.log("saved");
            goToTemplateCommunications();
      }
   });
}


function showDeleteTemplates(element)
{
  var selectedTemplatesIds = [];
  
  //collect all checked ids first
  $('.selectElement:checked').each(function(){
    selectedTemplatesIds.push($(this).val());
  });

  var callBack = function (data)
  {
    goToTemplateCommunications();
  }

  var title = 'Delete Selected Templates(s)?';
  var message = 'Selected Templates(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/communications/deleteTemplates';
  var data = {
      ids: selectedTemplatesIds
  };          

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title, message, deleteUrl, element, callBack);
}


function showDeleteProgramDeliveries(element)
{
  var selectedProgramDeliveriesIds = [];
  
  //collect all checked ids first
  $('.selectElement:checked').each(function(){
    selectedProgramDeliveriesIds.push($(this).val());
  });

  var callBack = function (data)
  {
    goToTemplateCommunications();
  }

  var title = 'Delete Selected Program Deliveries)?';
  var message = 'Selected Program Deliveries will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/communications/deleteProgramDeliveries';
  var data = {
      ids: selectedProgramDeliveriesIds
  };          

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title, message, deleteUrl, element, callBack);
}


function saveProgramDelivery()
{
   //console.log('file content '+ $('#FileContents').html());
   var serializedData = $('#ProgramDeliveryForm').serialize();
   //console.log ("form data "+ JSON.stringify(serializedData));
   jQuery.ajax({
      url:'/communications/saveProgramDelivery',
      datatype:'json',
      type: 'POST',
      async: true,
      data: serializedData,
      success:function(data){
         console.log("saved");
           //if there are templates selected, save them now
           if ($('.mainTable tbody tr').length > 0)
           {
               saveAttachementTemplates();
           } else {
               goToTemplateCommunications();
          }
      }
   });
}


function saveAttachementTemplates()
{
   var selectionContainer = $('.mainTable');

   var selectedIds = [];

    //collect all checked ids first
    selectionContainer.find('.selectElement:checked').each(function(){
        selectedIds.push($(this).val());
    });

      var data = {attachmentTemplates: selectedIds};

      jQuery.ajax({
      url:'/communications/saveProgramDeliveryTemplates',
      datatype:'json',
      type: 'POST',
      async: true,
      data: data,
      success:function(data){
         goToTemplateCommunications();
         }
      });
}