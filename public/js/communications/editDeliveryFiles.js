function initEditDeliveryFiles() {
   initTinyMceEditingDeliverables();
   setNavigationButtons();
   $( "#dialog-confirm-save" ).dialog({autoOpen: false});
}



function setNavigationButtons()
{
   var index = $('#CurrentIndex').val();
   var totalFiles = $('#TotalFiles').val();

   if (index == 1 )
   {
      $('#PreviousButton').addClass('Disable')
      $('#PreviousButton').prop('disabled',true);
   }
   else
   {
      $('#PreviousButton').removeClass('Disable')
      $('#PreviousButton').prop('disabled',false);
   }

   if (index == totalFiles )
   {
      $('#NextButton').addClass('Disable')
      $('#NextButton').prop('disabled',true);
   }
   else
   {
      $('#NextButton').removeClass('Disable')
      $('#NextButton').prop('disabled',false);
   }
}

function checkGetFileByIndex(target,newIndex)
{
   if (savedDeliveryFile)
   {
      getFileByIndex(target,newIndex);  
   }else
   {
      //show dialog
      showSaveConfirmationDialog(getFileByIndex,target,newIndex);
   }
}

function getFileByIndex(target,newIndex)
{
   if (!($(target).hasClass('disabled')))
   {
      var newFileIndex = parseInt($('#CurrentIndex').attr('value'))+newIndex;
      $("#CurrentIndex").val(newFileIndex);
      $("#CurrentIndexLabel").html(newFileIndex);
      
      //get the file content by index
      var serializedData = {contentIndex:newFileIndex};

      jQuery.ajax({
         url:'/communications/getFileContent',
         datatype:'json',
         type: 'POST',
         async: true,
         data: serializedData,
         success:function(data){
            tinymce.activeEditor.setContent(data);
         }
      });
      setNavigationButtons();
   }
}


function checkGoBackFromEditFiles()
{
   if (savedDeliveryFile)
   {
      goBackFromEditFiles();  
   }else
   {
      //show dialog
      showSaveConfirmationDialog(goBackFromEditFiles,null,null);
   }
}

function goBackFromEditFiles(){
   jQuery.ajax({
         url:'/communications/clearCurrentDeliveryFiles',
         datatype:'json',
         type: 'POST',
         async: true,
         success:function(data){
            goToDeliveryCommunications();
         }
      });
}


function saveFile()
{
   var content = tinymce.activeEditor.getContent();
   var index = parseInt($('#CurrentIndex').val());
   var data = {fileContent:content,fileIndex:index};
   jQuery.ajax({
      url:'/communications/saveDeliveryFile',
      datatype:'json',
      type: 'POST',
      async: true,
      data: data,
      success:function(data){
         console.log('saved');
         savedDeliveryFile = true;
      }
   });
}



function showSaveConfirmationDialog(callback,target,newIndex){
   $( "#dialog-confirm-save" ).dialog({
      resizable: false,
      height:240,
      width:350,
      title:'Save Confirmation',
      open: function() {
          $(this).html('Do you want to save changes ?');
        },
      modal: true,
      buttons: {
        "Yes": function() {
            var content = tinymce.activeEditor.getContent();
            var index = parseInt($('#CurrentIndex').val());
            var data = {fileContent:content,fileIndex:index};
            jQuery.ajax({
              url:'/communications/saveDeliveryFile',
              datatype:'json',
              type: 'POST',
              async: false,
              data: data,
              success: function (data){
                savedDeliveryFile = true;
                if (target)
                {
                  callback(target,newIndex);
                }
                else
                {
                  callback();
                }
              }
            });

            $(this).dialog( "close" );
        },
        No: function() {
          savedDeliveryFile = true;
          if (target)
          {
            callback(target,newIndex);
          }else
          {
            callback();
          }
          $( this ).dialog( "close" );
        }
      }
    });
    $('#dialog-confirm-save').dialog("open");
}


function addExternalAttachment(event)
{
   files = event.target.files;

   data = new FormData();
   
  $.each(files, function(key, value)
  {
    data.append(key, value);
  });

  data.append('fileId', new Date().getTime());

  //display selected file's name in dialog
  var fileName = $('#ExternalFile').val();

  //for IE.. clean up file name
  //fileName = fileName.slice(fileName.lastIndexOf('\\') + 1);

  console.log('sending attachment to server: ' + JSON.stringify(data));

  jQuery.ajax({
        url:'/uploadExternalAttachmentFile',
        datatype:'json',
        type: 'POST',
        async: false,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        data: data,
        success: onExternalFileUploadSuccess
      });
}



function onExternalFileUploadSuccess(returnedData)
{
  console.log('external attachment uploaded... got this back from server: ' + returnedData.fileFullName);
  //saveUploadedFileToDB('/storage/uploadedFiles/' + returnedData.fileFullName);
  //add file info to table
  var filePath = 'private/storage/externalAttachments/' + returnedData.fileFullName;
  var fileName = filePath.slice(filePath.indexOf('_attachment_') + 12);

  var mainTable = $('#externalAttachmentsTable');
  var row = '<tr><td><input type="checkbox" class="selectElement" name="externalAttachments[]" value="' + filePath + '" checked/></td><td>' + fileName + '</td></tr>';
  mainTable.find("tbody").append(row);

  mainTable.trigger("update");
}



function deleteExternalAttachment(element)
{
    var title = 'Delete Selected Attachment(s)?';
    var message = 'Selected attachment(s) will be permanently deleted and cannot be recovered. Are you sure?';
    var deleteUrl = '/communications/deleteExternalAttachments';

    var callBack = function(selectedIds)
    {
      //update table
      $('#externalAttachmentsTable').find('tbody tr').each(function(){
        for (var index in selectedIds) {
            if ($(this).find('.selectElement').val() == selectedIds[index])
            {
                $(this).remove();
            }
        }

        //reselect everything now
        $(".selectElement").prop("checked", "checked");
      });

      // //this indicates that the table is updated for the sorter by columns
      $mainTable.trigger("update");
    }

    var selectedIds = [];  
    var $selectionContainer = $(element).parents('.js-selection-container');
    $selectionContainer.find('.selectElement:checked').each(function(){
        selectedIds.push($(this).val());
    });

    if (selectedIds.length > 0)
    {
      //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
      initDeleteDialog(title,message,deleteUrl,element, callBack);
   }
}



