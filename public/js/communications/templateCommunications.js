function initProgramCommunications() {
  	initGlobal();

  	$('html, body').animate({ scrollTop: 0 }, 'slow');
  	
  	//set the behaviour of the select all element
	setSelectionBehaviour();  

	$('.deleteSelectedBtn').click(function(){
		showDeleteCommunications(this);
	});

	//init function to call when search is completed
	initClickOnTableRowProgramCommunications();

	$('#File').on('change', onFileSelected);

	$( "#dialog-confirm-delete" ).dialog({autoOpen: false});
	$( "#uploadFile-form" ).dialog({autoOpen: false});
}

function initClickOnTableRowProgramCommunications()
{
    $('.mainTable tbody td').not('.checkBoxTd').click(function () {
      displayFile($(this).parent().find('[type=checkbox]').val());
    });
}

function createNewCommunication(id)
{
	/*
	$('#uploadNotes').show();

	fileUploadSuccessFunction = onCommunicationFileUploadSuccess;
  	uploadTargetId = id;
	var title = 'Upload New Document';
	initUploadFileDialog(title, uploadFileFunction, uploadFileDialogCloseFunction);*/

}


function onCommunicationFileUploadSuccess(returnedData)
{
	var filePath = '/storage/uploadedFiles/' + returnedData.fileFullName;
	var data;
	var totalItems = $('.mainTable tbody tr').length;

	data = {
			value: ["name=" + uploadData.displayName, "pathToFile=" + filePath, "notes=" + uploadData.notes],
			targetIndex: totalItems, 
			targetProperty: 'communications',
			modelName: 'UploadedFile',
	};

	jQuery.ajax({
		url:'/programs/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			goToTemplateCommunications();
		}
	});
}


function showDeleteCommunications()
{
  var selectedCommunicationsIds = [];
  
  //collect all checked ids first
  $('.selectElement:checked').each(function(){
    selectedCommunicationsIds.push($(this).val());
  });

  var callBack = function (data)
  {
    goToTemplateCommunications();
  }

  var title = 'Delete Selected Communications(s)?';
  var message = 'Selected Communications(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/programs/deleteCommunications';
  var data = {
      ids: selectedCommunicationsIds
  };          

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,data,deleteUrl,updateTableFunc, callBack);

}