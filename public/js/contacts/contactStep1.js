var affiliationIndex;

function initContactStep1() {

	$('html, body').animate({ scrollTop: 0 }, 'slow');

	/* focus out of the elements */
	$(".contactProperty").focusout(function (){saveProperty(this);});
			
	//add the focus out for each element of the affiliations
	$('.AffiliationElement').focusout(function (){saveToContact(this, 'affiliations', 'Affiliation');});//targetProperty: "affiliations", modelName: "Affiliation"
	$(".AffiliationElement").bind('input',function (){changeElement(this);});
	
	affiliationIndex = $("#AffiliationContainer").children('.FormRowBackground').length-1;

	//var linkedClients = $('#linkedClients');
	//var clientList = $('#clientList');
	

	$(".DegreesGroup").change(function() {saveDegrees();});

	$("#OtherDegreeCheckbox").change(function() {
		otherDegreeCheckBoxChange();
	});


	$( "#dialog-clientLink" ).dialog({autoOpen: false});

    if ($('#linkedClients').attr('mode') == 'false')
    {
    	var successCallback = function ()
    	{
	          var clientName = $('option:selected', clientList).text();
    	      var linkedClients = $('#linkedClients');
    	      linkedClients.append(clientName);
    	};	

    	var closeCallback = function() {
       		if (!linkedToClient)
       		{
   		 		goContactsList();
   		 	}
   		 	//reset flag, just in case
   		 	linkedToClient = false;
          	$( this ).dialog( "close" );
       	};

    	var url = '/contacts/setCurrentClient';

    	initClientLinkDialog(url,successCallback,true);
    } else if ($('#linkedClients').attr('mode') == 'true') {
    	$("#clientList").change(function() {switchData();});
    }

    $(".js-optOutRadio").change(function() {saveOptOutStatus(this);});
}


function saveOptOutStatus(target)
{
	var selection = $(target).val();

	var data = new Object();

	if (selection == "Yes")
	{
		data.optOut = true;
	} else {
		data.optOut = false;
	}

	saveModel(data);
}


function saveDegrees()
{
	//console.log("Saving degrees");
	var degrees = $('input:checkbox:checked.DegreesGroup').map(function () {
		  return this.value;
		}).get();
	var data = {
			degrees:degrees
		};
	
	if(degrees.length == 0)
	{
		degrees.push('');
	}
	saveModel(data);
}


/*this shows and hide the other degree input depending on the other degree checkbox */
function otherDegreeCheckBoxChange()
{
	var tempOtherDegree = $("#OtherDegree").clone();
	$("#OtherDegree").val("");
	$("#OtherDegree").toggleClass("hidden");
	if($("#OtherDegreeCheckbox").prop('checked') == false)
	{
		var data = {
				otherDegree: "",
			};
		saveModel(data);
	}
}


function addAffiliation(target)
{
	affiliationIndex++;
	
	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteAffiliation(this)'>X</button>";

	updatePage(target,deleteButton,affiliationIndex);
	
	// focus out elements to the components 
	$('.AffiliationElement').focusout(function (){saveToContact(this, 'affiliations', 'Affiliation');});//targetProperty: "affiliations", modelName: "Affiliation"
	$(".AffiliationElement").bind('input',function (){changeElement(this);});
}


function deleteAffiliation(target)
{
	affiliationIndex--;
	deleteElement(target,"affiliations",affiliationIndex);
}