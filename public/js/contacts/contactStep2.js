var emailIndex;
var contactNumberIndex;
var addressIndex;
var staffNumberIndex;


function initContactStep2 (){

	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	/* focus out of the elements */
	$('.MailElement').focusout(function (){saveContactEmail(this);});
	$(".MailElement").bind('input',function (){changeElement(this);});
	
	$('.PhoneElement').focusout(function (){saveContactNumber(this);});
	$(".PhoneElement").bind('input',function (){changePhoneNumberElement(this);});

	$('.AddressElement').focusout(function (){saveContactAddress(this)});
	$(".AddressElement").bind('input',function (){changeElement(this);});

	$('.StaffElement').focusout(function (){saveStaff(this);});
	$(".StaffElement").bind('input',function (){changeElement(this);});

	$('.StaffNumberElement').focusout(function (){saveNumberStaff(this);});
	$(".StaffNumberElement").bind('input',function (){changePhoneNumberElement(this);});
	
	$('.internationalCheckbox').change(function (){internationalChange(this);});
	
	$(".PhoneNumber").intlTelInput();

	$('.js-default-email').change(function(){saveContactEmail($(this).parents('.FormRowBackground').find('.MailElement'))});
	$('.js-default-phoneNumber').change(function(){saveContactNumber($(this).parents('.FormRowBackground').find('.PhoneElement'))});
	$('.js-default-address').change(function(){saveContactAddress($(this).parents('.FormRowBackground').find('.js-street-address'))});
	
	//index
	emailIndex = $("#EmailContainer").children('.FormRowBackground').length-1;
	contactNumberIndex = $("#ContactNumberContainer").children('.FormRowBackground').length-1;
	addressIndex = $("#AddressContainer").children('.FormRowBackground').length-1;
	staffNumberIndex = $("#OfficeContactNumberContainer").children('.FormRowBackground').length-1;

	$("#clientList").change(function() {switchData();});

	//start the validation engine 
	$('#ContactStep2ValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});
}


function saveContactEmail(target)
{
	var validation = $(target).validationEngine('validate');
	if ($(target).val() && !validation)
	{
		var defaultFlag = $(target).parents('.FormRowBackground').find('.js-default-email').is(':checked');

		//validate email first
		saveToContact(target, 'emailAddresses', 'ContactEmailAddress', defaultFlag);

		//Check to see if this target is set to default
		if (defaultFlag)
		{
			saveContactDefaultItem(target, 'defaultEmail');
		}
	}
}

function saveContactAddress(target)
{
	if($(target).val())
	{
		var defaultFlag = $(target).parents('.FormRowBackground').find('.js-default-address').is(':checked');

		saveToContact(target, 'addresses', 'Address', defaultFlag);

		//Check to see if this target is set to default
		if (defaultFlag)
		{
			saveContactDefaultItem(target, 'defaultAddress');
		}
	}
}


///////////  EMAILS /////////////////
function addEmail(target)
{
	emailIndex++;
	
	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteEmail(this)'>X</button>";
	updatePage(target,deleteButton,emailIndex);
	
	// add focus out to the components 
	$('.MailElement').focusout(function (){saveContactEmail(this)});
	$(".MailElement").bind('input',function (){changeElement(this);});
}


function deleteEmail(target)
{
	emailIndex--;
	deleteElement(target,"emailAddresses",emailIndex);
}


///////////  CONTACT NUMBER /////////////////
function addContactNumber(target)
{
	contactNumberIndex++;
	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteContactNumber(this)'>X</button>";
	
	var clonedElement = updatePage(target,deleteButton,contactNumberIndex);
	
	// add focus out to the components 
	$('.PhoneElement').focusout(function (){saveContactNumber(this);});
	$(".PhoneElement").bind('input',function (){changePhoneNumberElement(this);});

	$(".PhoneNumber").intlTelInput();
}
    

function saveContactNumber(target)
{
	if($(target).val() && $(target).val()!=selectPrompt)
	{
		var defaultFlag = $(target).parents('.FormRowBackground').find('.js-default-phoneNumber').is(':checked');

		var targetIndex;
		
		// if ($(target).attr('name') == 'phoneType')
		// {
		// 	targetIndex = $(target).parents('.FormRowBackground').find('.Index').val();
		// } else {
	    	targetIndex = $(target).parents('.FormRowBackground').find('.Index').val();
	    // }
	    
		var data = {
				value: [$(target).attr("name")+"="+$(target).val(), 'preferred=' + defaultFlag],
				targetIndex: targetIndex,
				targetProperty: "phoneNumbers",
				modelName: "ContactPhoneNumber"
			};
	
		//enable the delete buttons
		var container = $(target).parent().parent().parent();
		enableDeleteButtonXByContainer(container);
		enableAddButtonByContainer(container);
		
		saveEmbeddedModel(data);

		if ($(target).attr('name') != 'phoneType')
		{
			if (defaultFlag)
			{
				saveContactDefaultItem(target, 'defaultPhoneNumber');
			}
		}
	}
}


function deleteContactNumber(target)
{
	contactNumberIndex--;
	deleteElement(target,"phoneNumbers",contactNumberIndex);
}



///////////  ADDRESS /////////////////

function internationalChange(target)
{
	$(target).parent().parent().find("[name='state']").toggleClass('hidden');	
	$(target).parent().parent().find("[for='Country']").toggleClass('hidden');	
	$(target).parent().parent().find(".country").toggleClass('hidden');

	var defaultFlag = $(target).parents('.FormRowBackground').find('.js-default-address').is(':checked');
	
	var targetIndex = $(target).parent().parent().find('.Index').val();
	var data;
	
	if ($(target).prop("checked"))
	{
		//checked
		$(target).parent().parent().find('.stateLabel').text("State/Province:");
		var data = {
				value: ["international=true", 'preferred=' + defaultFlag],
				targetIndex: targetIndex, targetProperty: "addresses",
				modelName: "Address"
			};
	}
	else
	{
		//uncheck
		$(target).parent().parent().find('.stateLabel').text("State:");
		var data = {
				value: ["international=false","state="+$("[name='state']").val(),"country="+$("[name='country']").val(), 'preferred=' + defaultFlag],
				targetIndex: targetIndex, targetProperty: "addresses",
				modelName: "Address"
			};
	}
	
	//enable the delete buttons
	var container = $(target).parent().parent().parent();
	enableDeleteButtonXByContainer(container);
	enableAddButtonByContainer(container);
	
	saveEmbeddedModel(data);

	if (defaultFlag)
	{
		saveContactDefaultItem(target, 'defaultAddress');
	}
}


function addAddress(target)
{
	addressIndex++;
	
	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteAddress(this)'>X</button>";

	var newElement = updatePage(target,deleteButton,addressIndex);

	//hide the country
	newElement.find(".country").addClass('hidden');
	newElement.find("[for='Country']").addClass('hidden');
	//set the state input
	newElement.find("input[name='state']").addClass('hidden');	
	newElement.find("select[name='state']").removeClass('hidden');	
	//set text for state
	newElement.find('.stateLabel').text("State:");
	
	
	// focus out elements to the components 
	$('.AddressElement').focusout(function (){saveContactAddress(this);});//targetProperty: "addresses",	modelName: "Address"
	$(".AddressElement").bind('input',function (){changeElement(this);});
	$('.internationalCheckbox').change(function (){internationalChange(this);});

	$('.js-default-address').change(function(){saveContactAddress($(this).parents('.FormRowBackground').find('.js-address-type'))});
}


function deleteAddress(target)
{
	addressIndex--;
	deleteElement(target,"addresses",addressIndex);
}




///////////  STAFF CONTACT /////////////////
function addKOLOfficeContactNumber(target)
{
	staffNumberIndex++;
	
	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteStaff(this)'>X</button>";

	 updatePage(target,deleteButton,staffNumberIndex);
	
	// focus out elements to the components 
	$('.StaffNumberElement').focusout(function (){saveNumberStaff(this);});
	$(".StaffNumberElement").bind('input',function (){changePhoneNumberElement(this);});
	
	$(".PhoneNumber").intlTelInput();
}

function saveStaff(target)
{
	var validation = $(target).validationEngine('validate');
	if($(target).val() && $(target).val()!=selectPrompt && !validation)
	{
		var targetIndex = $(target).parent().parent().find('.Index').val();
		
		var data = {
			value: [$(target).attr("name") + "=" + $(target).val()], targetIndex: targetIndex, targetProperty: "officeStaffContact",
			modelName: "ContactInfo"
		};

		saveEmbeddedModel(data);
	}
}

function saveNumberStaff(target)
{
	if($(target).val() && $(target).val()!=selectPrompt)
	{
		var subTargetIndex;
		
		if ($(target).attr('name') == 'phoneType')
		{
			subTargetIndex = $(target).parent().parent().find('.Index').val();
		} else {
	    	subTargetIndex = $(target).parent().parent().parent().find('.Index').val();
	    }
	
		var data = {
				value: [$(target).attr("name") + "=" + $(target).val()], targetIndex:"0", subTargetIndex: subTargetIndex, 
				targetProperty: "officeStaffContact", subTargetProperty: "phoneNumbers",
				modelName: "ContactInfo", subModelName: "ContactPhoneNumber"
		};
		
		//enable the delete buttons
		var container = $(target).parent().parent().parent();
		enableDeleteButtonXByContainer(container);
		enableAddButtonByContainer(container);
		
		saveEmbeddedModel(data);
	}
}

function deleteStaff(target)
{
	staffNumberIndex--;
	deleteElement(target,"officeStaffContact",staffNumberIndex,"phoneNumbers");
}


function validateStep2(functionToCall)
{
	var validation = $('#ContactStep2ValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}