var emergencyContactIndex;
var specialtyIndex;
var interestIndex;
var usLicenseIndex;
var otherLicenseIndex;

function initContactStep3() {
	

	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	/* focus out of the elements */
	$('.EmergencyContactElement').focusout(function (){saveToContact(this, 'emergencyContacts', 'ContactInfo');});//targetProperty: "emergencyContacts",modelName: "ContactInfo"
	$(".EmergencyContactElement").bind('input',function (){changeElement(this);});

	$('.EmergencyContactNumberElement').focusout(function (){saveEmergencyNumberContact(this);});
	$(".EmergencyContactNumberElement").bind('input',function (){changePhoneNumberElement(this);});
	
	$('.SpeacialtyElement').focusout(function (){saveToContact(this, 'specialties', '');});	//targetProperty: "specialties",	modelName: ""


	$('.SpeacialtyElement').bind('input',function (){changeElement(this);});

	$('.InterestElement').focusout(function (){saveToContact(this, 'therapeuticInterests', '');});//targetProperty: "therapeuticInterests",modelName: ""
	$(".InterestElement").bind('input',function (){changeElement(this);});
	
	$('.USLicenseElement').focusout(function (){saveToContact(this, 'medicalLicenses', 'MedicalLicense');});//targetProperty: "medicalLicenses",	modelName: "MedicalLicense"
	$(".USLicenseElement").bind('input',function (){changeElement(this);});
	
	$('.OtherLicenseElement').focusout(function (){saveToContact(this, 'otherMedicalLicenses', 'OtherMedicalLicense');});//targetProperty: "otherMedicalLicenses",modelName: "OtherMedicalLicense"
	$(".OtherLicenseElement").bind('input',function (){changeElement(this);});
	
	$(".contactProperty").focusout(function (){saveProperty(this);});

	$(".PhoneNumber").intlTelInput();

	$('.js-number-only').autoNumeric('init', {aPad: false, aSep: '', vMin: 0000000000, vMax: 9999999999});

	$('.js-npi').focusout(function(){saveNPI(this)});
	
	//indexes of the section
	emergencyContactIndex = $("#EmergencyContactContainer").children('.FormRowBackground').length-1;
	specialtyIndex = $("#SpecialtyContainer").children('.FormRowBackground').length-1;
	interestIndex = $("#InterestContainer").children('.FormRowBackground').length-1;
	usLicenseIndex = $("#USLicensesContainer").children('.FormRowBackground').length-1;
	otherLicenseIndex = $("#OtherLicensesContainer").children('.FormRowBackground').length-1;

	$("#clientList").change(function() {switchData();});

	//start the validation engine 
	$('#ContactStep3ValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});
}


///////////  EMERGENCY CONTACT /////////////////

function addEmergencyContact(target)
{
	emergencyContactIndex++;
	
	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteEmergencyContact(this)'>X</button>"

	updatePage(target,deleteButton,emergencyContactIndex);
	// focus out elements to the components 
	$('.EmergencyContactElement').focusout(function (){saveToContact(this, 'emergencyContacts', 'ContactInfo');});//targetProperty: "emergencyContacts",modelName: "ContactInfo"
	$(".EmergencyContactElement").bind('input',function (){changeElement(this);});
	
	$('.EmergencyContactNumberElement').focusout(function (){saveEmergencyNumberContact(this);});
	$(".EmergencyContactNumberElement").bind('input',function (){changePhoneNumberElement(this);});
	
	$(".PhoneNumber").intlTelInput();
}

function saveEmergencyNumberContact(target)
{
	if($(target).val() && $(target).val()!=selectPrompt)
	{
		var targetIndex = $(target).parent().parent().parent().find('.Index').val();
		
		var data = {
				value: [$(target).attr("name") + "=" + $(target).val()], 
				targetIndex: targetIndex, 
				subTargetIndex:"0",
				targetProperty: "emergencyContacts", subTargetProperty:"phoneNumbers",
				modelName: "ContactInfo", subModelName:"ContactPhoneNumber"
		};
		
		//enable the delete buttons
		var container = $(target).parent().parent().parent();
		enableDeleteButtonXByContainer(container);
		enableAddButtonByContainer(container);
		
		saveEmbeddedModel(data);
	}
}

function deleteEmergencyContact(target)
{
	emergencyContactIndex--;
	deleteElement(target,"emergencyContacts",emergencyContactIndex);
}


///////////  SPECIALTY /////////////////


function addSpecialty(target)
{
	specialtyIndex++;
	
	//create the delete button
   var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteSpecialty(this)'>X</button>"

	updatePage(target,deleteButton,specialtyIndex);
	
	// add focus out to the components 
	$('.SpeacialtyElement').focusout(function (){saveToContact(this, 'specialties', '');});	//targetProperty: "specialties",	modelName: ""
	$(".SpeacialtyElement").bind('input',function (){changeElement(this);});
}


function deleteSpecialty(target)
{
	specialtyIndex--;
	deleteElement(target,"specialties",specialtyIndex);
}


///////////  INTEREST /////////////////


function changeInterest(target)
{
	if ($(target).val() != "")
	{
		//enable button
		$("#AddInterestButton").prop( "disabled", false );
		$('#AddInterestButton').removeClass("Disable");
	}
}

function addInterest(target)
{
	interestIndex++;
	
	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteInterest(this)'>X</button>";
	
	updatePage(target,deleteButton,interestIndex);
	
	// add focus out to the components 
	$('.InterestElement').focusout(function (){saveToContact(this, 'therapeuticInterests', '');});//targetProperty: "therapeuticInterests",modelName: ""
	$(".InterestElement").bind('input',function (){changeInterest(this);});
}


function deleteInterest(target)
{
	interestIndex--;
	deleteElement(target,"therapeuticInterests",interestIndex);
}



///////////  US LICENSE  /////////////////


function addUSLicense(target)
{
	usLicenseIndex++;
	
	//create the delete button
   var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteUSLicense(this)'>X</button>";
	
	updatePage(target,deleteButton,usLicenseIndex);
	
	// add focus out to the components 
	$('.USLicenseElement').focusout(function (){saveToContact(this, 'medicalLicenses', 'MedicalLicense');});//targetProperty: "medicalLicenses",	modelName: "MedicalLicense"
	$(".USLicenseElement").bind('input',function (){changeElement(this);});
}


function deleteUSLicense(target)
{
	usLicenseIndex--;
	deleteElement(target,"medicalLicenses",usLicenseIndex);
}


///////////  OTHER LICENSE  /////////////////


function addOtherLicense(target)
{
	otherLicenseIndex++;
	
	//create the delete button
   var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteOtherLicense(this)'>X</button>";
	
	updatePage(target,deleteButton,otherLicenseIndex);
	// add focus out to the components 
	$('.OtherLicenseElement').focusout(function (){saveToContact(this, 'otherMedicalLicenses', 'OtherMedicalLicense');});//targetProperty: "otherMedicalLicenses",modelName: "OtherMedicalLicense"
	$(".OtherLicenseElement").bind('input',function (){changeElement(this);});
}


function deleteOtherLicense(target)
{
	otherLicenseIndex--;
	deleteElement(target,"otherMedicalLicenses",otherLicenseIndex);
}

///////////  NPI Number  /////////////////
//required min number: 10 digits
function saveNPI(target)
{
	var val = $(target).val();

	if (val.length < 10)
	{
		$('#npiError').show().delay(5000).fadeOut(400);
	} else {
		//save npi number
		saveProperty(target);
	}
}

function validateStep3(functionToCall)
{
	var validation = $('#ContactStep3ValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}
