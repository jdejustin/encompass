var airlinesIndex;
var hotelsIndex;
var passportsIndex;
var repsIndex;

function initContactStep4(){

	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
    $("#tsaDOB").datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '-100:+0',
      onSelect: onDOBSelected
    });

   	$(".js-expiration-datepicker").datepicker({
      changeMonth: true,
      changeYear: true,
      onSelect: onPassportExpirationSelected
    });


    $('input[name="tsaGender"]:radio').change(updateTSAGender);

    /*defining focus out elements */
	$(".TextProperty").focusout(function (){saveProperty(this);});
			
	//add the focus out for each element that require array saving
	$('.AirlineElement').focusout(function (){saveToContact(this, 'airlines', 'TravelAir');});
	$('.AirlineElement').bind('input',function (){changeElement(this);});

	$('.HotelElement').focusout(function (){saveToContact(this, 'hotels', 'TravelHotel');});
	$('.HotelElement').bind('input',function (){changeElement(this);});
	
	$('.PassportElement').focusout(function (){saveToContact(this, 'passports', 'ContactPassport');});
	$('.PassportElement').bind('input',function (){changeElement(this);});

	$('.RepElement').focusout(function (){saveToContact(this, 'reps', '');}); //save the array of strings (ids)
	$('.RepElement').bind('change',function (){changeElement(this);});
	
	airlinesIndex = $("#TravelAirContainer").children('.FormRowBackground').length-1;
	hotelsIndex = $("#TravelHotelContainer").children('.FormRowBackground').length-1;
   passportsIndex = $("#TravelHotelContainer").children('.FormRowBackground').length-1;
	repsIndex = $("#RepContainer").children('.FormRowBackground').length-1;

	$("#clientList").change(function() {switchData();});
}


function onDOBSelected(dateText, target)
{
	var data = {tsaDOB: dateText};
	saveModel(data);
}

function updateTSAGender(event)
{
	saveProperty($(event.target));
}

function addAirline(target)
{
	airlinesIndex++;
	
	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteAirline(this)'>X</button>";

    updatePage(target, deleteButton, airlinesIndex);
	
	// focus out elements to the components 
	$('.AirlineElement').focusout(function (){saveToContact(this, 'airlines', 'TravelAir');});
	$(".AirlineElement").bind('input',function (){changeElement(this);});
}


function deleteAirline(target)
{
	airlinesIndex--;
	deleteElement(target, "airlines", airlinesIndex);
}


function addHotel(target)
{
	hotelsIndex++;
	
	//create the delete button
   var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteHotel(this)'>X</button>";


    updatePage(target, deleteButton, hotelsIndex);
	
	// focus out elements to the components 
	$('.HotelElement').focusout(function (){saveToContact(this, 'hotels', 'TravelHotel');});
	$(".HotelElement").bind('input',function (){changeElement(this);});
}


function deleteHotel(target)
{
	hotelsIndex--;
	deleteElement(target, "hotels", hotelsIndex);
}

function addPassport(target)
{
	passportsIndex++;
	
	//create the delete button
   var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deletePassport(this)'>X</button>";


   var elementToClone = updatePage(target, deleteButton, passportsIndex);
	
	// focus out elements to the components 
	$('.PassportElement').focusout(function (){saveToContact(this, 'passports', 'ContactPassport');});
	$('.PassportElement').bind('input',function (){changeElement(this);});


   $(elementToClone).find(".js-expiration-datepicker").each(function( index ) {
      $(this).datepicker({
         changeMonth: true,
         changeYear: true,
         onSelect: onPassportExpirationSelected
      });
   });
}


function deletePassport(target)
{
	passportsIndex--;
	deleteElement(target, "passports", passportsIndex);
}


function onPassportExpirationSelected(dateText, target)
{
	saveToContact(this, 'passports', 'ContactPassport');
}




function addRep(target)
{
	repsIndex++;
	
	//create the delete button
   var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteRep(this)'>X</button>";


    updatePage(target, deleteButton, repsIndex);
	
	// focus out elements to the components 
	$('.RepElement').focusout(function (){saveToContact(this, 'reps', '');});
	$('.RepElement:last').bind('input',function (){changeElement(this);});

}


function deleteRep(target)
{
	repsIndex--;
	deleteElement(target, "reps", repsIndex);
}





