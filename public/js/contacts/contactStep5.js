// Variable to store your files and current target to load info once file is uploaded
var files;
var currentTarget;
var currentTargetProperty;
var uploadType;

//object to hold form data, so we can save it to the DB after file upload
var form1Data;
var form2Data;

var fileDisplayNameInput;
var selectedFile;

//For W8/W9
var fileDisplayNameInputW9;
var selectedFileW9;
var formType;
var formName;
var businessName;
var incCountry;
var otherCheckBox;
var OtherTaxClassification;
var LLCType;
var taxClassValue;
var address1;
var address2;
var city;
var state;
var stateW8;
var postalCode;
var country;

var mailingAddress1;
var mailingAddress2;
var mailingCity;
var mailingState;
var mailingPostalCode;
var mailingCountry;

var ssn;
var ein;
var foreignId;
var listAccountNumbers;
var listAccountNumbersW8;
var signature;

var allTextFields;
var allSelectFields;
var allRadioGroups;

function initContactStep5() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	initGlobal();

	$('#advancedSearch').click(function(){
	
	//display advanced search options
	$('#advancedSearchOptions').toggle();
	});

	//set the behaviour of the select all element
	setSelectionBehaviour();  

	$('#deleteCVsButton').click(function(){
		showDeleteCVs(this);
	});

	$('#deleteBiosButton').click(function(){
		showDeleteBios(this);
	});

	$('#deleteFactSheetsButton').click(function(){
		showDeleteFactSheets(this);
	});

	$('#deleteW8W9Button').click(function(){
		showDeleteW8W9(this);
	});

	$('#deletePhotoButton').click(function(){
		showDeletePhoto(this);
	});


	//init function to call when search is completed
	initClickOnTableRowDocuments();

	onRowClickFunction = initClickOnTableRowDocuments;

	$( "#dialog-confirm-delete" ).dialog({autoOpen: false});

	$("#signature" ).datepicker();
	$('#ssn').keypress(function(e) {
            var verified = (e.which == 8 || e.which == undefined || e.which == 0) ? null : String.fromCharCode(e.which).match(/[^0-9-]/);
            if (verified) {e.preventDefault();}
    });

    $('#ein').keypress(function(e) {
            var verified = (e.which == 8 || e.which == undefined || e.which == 0) ? null : String.fromCharCode(e.which).match(/[^0-9-]/);
            if (verified) {e.preventDefault();}
    });
	 

	 // Add events
	$('#File').on('change', onFileSelected);
	$('#File2').on('change', onFileSelected);
	$('#formType').on('change', switchForm);
	$("#OtherCheckbox").change(function() {otherCheckBoxChange();});
	$('input[name="defaultPhoto"]:radio').change(setDefaultPhoto);
	$("#clientList").change(function() {switchData();});

	$( "#uploadFile-form" ).dialog({autoOpen: false});

	//make pointers to fields in data form
	//For File or Photo
	fileDisplayNameInput = $('#displayName');
	selectedFile = $('#selectedFile');

    //For W8/W9
	fileDisplayNameInputW9 = $('#displayNameW9');
	selectedFileW9 = $('#selectedFileW9');
    formType = $('#formType');
	formName = $('#name');
	businessName = $('#businessName');
	incCountry = $('#incCountry');
	otherCheckBox = $('otherCheckBox');
	OtherTaxClassification = $('OtherInput');
	LLCType = $('#LLCType');
	taxClassValue;
	address1 = $('#address1');
	address2 = $('#address2');
	city = $('#city');
	state = $('#state');
	stateW8 = $('#stateW8');
	postalCode = $('#postalCode');
	country = $('#country');

	mailingAddress1 = $('#address1');
	mailingAddress2 = $('#address2');
	mailingCity = $('#city');
	mailingState = $('#state');
	mailingPostalCode = $('#postalCode');
	mailingCountry = $('#mailingCountry');

	ssn = $('#ssn');
	ein = $('#ein');
	foreignId = $('#foreignId');
	listAccountNumbers = $('#listAccountNumbers');
	listAccountNumbersW8 = $('#listAccountNumbersW8');
	signature = $('#signature');

	// allTextFields = [fileDisplayNameInput, fileDisplayNameInputW9, selectedFile, selectedFileW9, formName, businessName, OtherTaxClassification, address1, address2, city, postalCode, 
	// ein, listAccountNumbers, mailingAddress1, mailingAddress2, mailingCity, mailingPostalCode, listAccountNumbersW8, signature];

 //    allSelectFields = [ LLCType, state, stateW8, mailingState, country, mailingCountry, incCountry];

	// allRadioGroups = [otherCheckBox];
}



//Delete functions 
function showDeleteCVs(element)
{
    var title = 'Delete Selected CV(s)?';
    var message = 'Selected CV(s) will be permanently deleted and cannot be recovered. Are you sure?';
    var deleteUrl = '/contacts/deleteCVs';
    //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
    initDeleteDialog(title,message,deleteUrl,element);
}

function showDeleteBios(element)
{
    var title = 'Delete Selected Bio(s)?';
    var message = 'Selected Bio(s) will be permanently deleted and cannot be recovered. Are you sure?';
    var deleteUrl = '/contacts/deleteBios';
    //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
    initDeleteDialog(title,message,deleteUrl,element);
}

function showDeleteFactSheets(element)
{
    var title = 'Delete Selected Fact Sheet(s)?';
    var message = 'Selected Fact Sheet(s) will be permanently deleted and cannot be recovered. Are you sure?';
    var deleteUrl = '/contacts/deleteFactSheets';
    //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
    initDeleteDialog(title,message,deleteUrl,element);
}

function showDeleteW8W9(element)
{
    var title = 'Delete Selected W8/W9(s)?';
    var message = 'Selected W8/W9(s) will be permanently deleted and cannot be recovered. Are you sure?';
    var deleteUrl = '/contacts/deleteW8W9s';
    //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
    initDeleteDialog(title,message,deleteUrl,element);
}

function showDeletePhoto(element)
{
    var title = 'Delete Selected Photo(s)?';
    var message = 'Selected Photo(s) will be permanently deleted and cannot be recovered. Are you sure?';
    var deleteUrl = '/contacts/deletePhotos';
    //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
    initDeleteDialog(title,message,deleteUrl,element);
}

function initClickOnTableRowDocuments()
{
  $('.mainTable tbody td').not('.checkBoxTd').click(function(){
  	displayFile($(this).parent().find('.checkBoxTd').attr('data-path'));
  });
}


function checkSSAndEINumbers(o1, o2, message, min, max) {
	//clean up first
	//check if both are blank first
	if ( o1.length == 0 && o2.length == 0 ) 
	{
		updateTips('Please provide a Social Security Number or an Employer Identification Number');
		return false;
	} else {
		o1 = o1.replace(/[-]/g, '');
		o2 = o2.replace(/[-]/g, '');

	
		if ( o1.length > 0){
			if ( o1.length > max || o1.length < min ) {
        		updateTips( 'Please provide a valid Social Security Number' );
        		return false;
      		} else {
        		return true;
	      	}
		} else {
			if ( o2.length > max || o2.length < min ) {
        		updateTips( 'Please provide a valid Employer Identification Number' );
        		return false;
      		} else {
        		return true;
	      	}
		}
	}
}

function checkSelection(o, message){
	console.log(o.val());
	if (o.val() == selectPrompt)
	{
		updateTips( message );
		return false;
	} else {
		return true;
	}
}

function checkRadioSelection(group, message){
	if ($('input[name=' + group + ']:checked').length == 0)
	{
		updateTips( message );
		return false;
	} else {
		if (group == "taxClass")
		{
    		taxClassValue = $('input[name=taxClass]:checked').val();

    		//if LLC is selected, then we need to make sure they select a type
    		if (taxClassValue == "LLC")
    		{
    			if (LLCType.val() == selectPrompt)
    			{
    				updateTips( 'Please select a type for the Limited Liability Company' );
    				return false;
    			}
    		}

    		if (taxClassValue == "Other")
    		{
    			if (OtherTaxClassification.length == 0)
    			{
    				updateTips('Please fill out a value for the Federal Tax Classification');
    				return false;
    			} else {
    				taxClassValue = OtherTaxClassification.val();
    			}
    		}
		}

		return true;
	}
}


function switchForm()
{
	if ($('#formType').val() == 'W8')
	{
		$('.w8').show();
		$('.w9').hide();
		uploadType = 'w8';
	} else {
		$('.w8').hide();
		$('.w9').show();
		uploadType = 'w9';
	}
}


function otherCheckBoxChange()
{
	$("#OtherTaxClassification").val("");
	$("#OtherTaxClassification").toggleClass("hidden");
	if($("#OtherCheckbox").prop('checked') == false)
	{
	//	saveDegrees();
	}
}

//Called by upload file buttons
function prepareUploadFile(target, id, type, tProperty)
{
	var proceed = true;

	if (type == 'photo')
	{
		var totalPhotos = $('#photoTable tbody tr').length;

		if (totalPhotos == 1)
		{
			proceed = false;

			alert('Only 1 photo per user is allowed');
		}
	}

	if (proceed)
	{
		if (type == "file" || type == "photo")
		{
			$('#form1').show();
			$('#form2').hide();
		} else {
			$('#form1').hide();
			$('#form2').show();
		}

		if (type == 'w9')
		{
			$('.w8').hide();
			$('.w9').show();
			formType.prop('selectedIndex',0);
		}

		uploadType = type;

		currentTarget = target;
		currentTargetProperty = tProperty;

		var title = 'Upload New Document';

		//set the upload targetid. This is the currentContact
		uploadTargetId = id;

	  	initUploadFileDialog(title, uploadFunction, closeContactStep5DialogFunction);
	}
}

function uploadFunction()
{
	var bValid = true;
 
  	if (uploadType == 'w9')
  	{
 		bValid = bValid && checkLength( selectedFileW9.val(), "Please select a file to upload.", 1, 150 );
  		bValid = bValid && checkLength( fileDisplayNameInputW9.val(), "Please fill out the display name field.", 1, 150 );

  		bValid = bValid && checkRadioSelection('taxClass', "Please select a Federal Tax Classification.", 1, 150 );
  		
  		bValid = bValid && checkLength( formName.val(), "Please input a name.", 1, 150 );
  		bValid = bValid && checkLength( address1.val(), "Please input a street address.", 1, 150 );
  		bValid = bValid && checkLength( city.val(), "Please input a city name.", 1, 150 );
  		bValid = bValid && checkSelection(state, "Please select a state.");
  		bValid = bValid && checkLength( postalCode.val(), "Please input a postal code.", 1, 150 );

  		bValid = bValid && checkSSAndEINumbers(ssn.val(), ein.val(), "Please provide a Social Security Number or an Employer Identification Number", 9, 9);
		bValid = bValid && checkLength( signature.val(), "Please provide a signature date.", 10, 10 );          		
  	} else if (uploadType == 'w8'){
  		bValid = bValid && checkLength( selectedFileW9.val(), "Please select a file to upload.", 1, 150 );
  		bValid = bValid && checkLength( fileDisplayNameInputW9.val(), "Please fill out the display name field field.", 1, 150 );

  		bValid = bValid && checkLength( formName.val(), "Please input a name.", 1, 150 );
  		bValid = bValid && checkSelection( incCountry, "Please select a country of incorporation.");

  		bValid = bValid && checkRadioSelection('ownerType', "Please select a Type of Beneficial Owner.", 1, 150 );
  		
  		bValid = bValid && checkLength( address1.val(), "Please input a street address.", 1, 150 );
  		bValid = bValid && checkLength( city.val(), "Please input a city name.", 1, 150 );
  		bValid = bValid && checkLength( stateW8.val(), "Please input a state or province name.", 1, 150 );
  		bValid = bValid && checkLength( postalCode.val(), "Please input a postal code.", 1, 150 );
  		bValid = bValid && checkSelection( country, "Please select a country.");

  		bValid = bValid && checkSSAndEINumbers(ssn.val(), ein.val(), "Please provide a Social Security Number or an Employer Identification Number", 9, 9);
		bValid = bValid && checkLength( signature.val(), "Please provide a signature date.", 10, 10 );   
  	} else {
  		bValid = bValid && checkLength( selectedFile.val(), "Please select a file to upload.", 1, 150 );
  		bValid = bValid && checkLength( fileDisplayNameInput.val(), "Please fill out the display name field field.", 1, 150 );
  	}

  	if ( bValid ) {
    	//storing form data, so we can save it to the database, once file is uploaded to server
    	if (uploadType == "file"  || uploadType == "photo")
    	{
    		 form1Data = {
    			displayName: fileDisplayNameInput.val()
    		}
    	} else {
    		if (uploadType == 'w9')
    		{
    			form2Data = {
					//get common data first
        			displayName: fileDisplayNameInputW9.val(),
        			formName: formName.val(),
        			businessName: businessName.val(),
        			taxClass: taxClassValue,
        			llcType: LLCType.val(),
        			address1: address1.val(),
        			address2: address2.val(),
        			city: city.val(),
        			state: state.val(),
        			postalCode: postalCode.val(),
        			ssn: ssn.val(),
        			ein: ein.val(),
        			listAccountNumbers: listAccountNumbers.val(),
        			signature: signature.val()
        		}

      		} else if (uploadType == 'w8')
      		{
      			form2Data = {
					//get common data first
        			displayName: fileDisplayNameInputW9.val(),
        			formName: formName.val(),
        			incCountry: incCountry.val(),
        			ownerType: $('input[name=ownerType]:checked').val(),
        			address1: address1.val(),
        			address2: address2.val(),
        			city: city.val(),
        			state: state.val(),
        			postalCode: postalCode.val(),
        			country: country.val(),
        			mailingAddress1: mailingAddress1.val(),
        			mailingAddress2: mailingAddress2.val(),
        			mailingCity: mailingCity.val(),
        			mailingState: mailingState.val(),
        			mailingPostalCode: mailingPostalCode.val(),
        			mailingCountry: mailingCountry.val(),

        			ssn: ssn.val(),
        			ein: ein.val(),
        			foreignId: foreignId.val(),
        			listAccountNumbers: listAccountNumbers.val(),
        			signature: signature.val()
        		}
      		}	
    	}

    	console.log("uploadFunction data "+JSON.stringify(data));
    	//upload photo or file
    	if (uploadType == "file" || uploadType == "w9")
    	{
			jQuery.ajax({
				url:'/uploadFile',
				datatype:'json',
				type: 'POST',
				async: false,
				processData: false, // Don't process the files
	       	 	contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				data: data,
				success: onFileUploadSuccess
			});
		} else if (uploadType == "photo")
		{
			jQuery.ajax({
				url:'/uploadPhoto',
				datatype:'json',
				type: 'POST',
				async: false,
				processData: false, // Don't process the files
	       	 	contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				data: data,
				success: onPhotoUploadSuccess
				});
		} 

		$( this ).dialog( "close" );
	}
}

function closeContactStep5DialogFunction () {
	fileDisplayNameInput.val('');
	fileDisplayNameInputW9.val('');
	selectedFile.val('');
	selectedFileW9.val('');
	formName.val('');
	businessName.val('');
	OtherTaxClassification.val('');
	address1.val('');
	address2, city.val('');
	postalCode.val('');
	ssn.val('');
	ein.val('');
	listAccountNumbers.val('');
	mailingAddress1.val('');
	mailingAddress2.val('');
	mailingCity.val('');
	mailingPostalCode.val('');
	listAccountNumbersW8.val('');
	signature.val('');

    LLCType.prop('selectedIndex',0);
    state.prop('selectedIndex',0);
    stateW8.prop('selectedIndex',0);
    mailingState.prop('selectedIndex',0);
    country.prop('selectedIndex',0);
    mailingCountry.prop('selectedIndex',0);
    incCountry.prop('selectedIndex',0);
    formType.prop('selectedIndex',0);

	$('input[name=otherCheckBox]:checked').prop('checked', false);
	$('input[name=taxClass]:checked').prop('checked', false);
	$('input[name=ownerType]:checked').prop('checked', false);

	uploadType = null;

	$( this ).dialog( "close" );
};



function onFileUploadSuccess(returnedData)
{
	console.log("onFileUploadSuccess returned data "+ JSON.stringify(returnedData));

	//save this new file info into the contact's record
	saveUploadedFileToDB('/storage/uploadedFiles/' + returnedData.fileFullName);
}

function onPhotoUploadSuccess(returnedData)
{

	//save this new file info into the contact's record
	saveUploadedFileToDB('/storage/userPhotos/' + returnedData.fileFullName);
}

function saveUploadedFileToDB(filePath)
{
	var targetIndex = $(currentTarget).parent().parent().parent().find('.mainTable tbody tr').length;
	var data;

	if (uploadType == "file" || uploadType == "photo")
	{
		data = {
				value: ["name=" + form1Data.displayName, "pathToFile=" + filePath],
				targetIndex: targetIndex, 
				targetProperty: currentTargetProperty,
				modelName: "UploadedFile"
		};

		jQuery.ajax({
			url:'/contacts/saveEmbeddedModel',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
			success:function(data){
				updateFileName(data.contactId, filePath);
			}
		});
	} else {  //we're dealing with taxforms...
		data = {
				value: ["name=" + form2Data.displayName, "pathToFile=" + filePath],
				targetIndex: targetIndex, 
				subTargetIndex: 0, 
				targetProperty: 'taxForms',
				subTargetProperty: "uploadedFile",
				modelName: (uploadType == "w9") ? 'W9' : 'W8',
				subModelName: "UploadedFile"
		};

		jQuery.ajax({
			url:'/contacts/saveEmbeddedModel',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
			success:function(data){
				saveTaxFormDataToDB();
			}
		});
	}
}


function updateFileName(id, filePath) 
{
	console.log("updateFileName id "+id);
	//check if when we saved the file to server, we had to use 0 as id... if yes, then let's go and rename the file to match the
	//contact's id now
	if (uploadTargetId == '0')
	{
		var data = {
			contactId : id,
			fileFullPath : filePath
		};

		jQuery.ajax({
			url:'/updateFileName',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data, 
			success:function(data){
				console.log("file renamed");

				//if we've just uploaded a W9 or W8, we need to save the forms data to the DB as well
				saveTaxFormDataToDB();
			}
		});
	} else {
		//reload step to get updated data
		goContactStep5();
	}
}

function saveTaxFormDataToDB()
{
	var data;
	var targetIndex = $(currentTarget).parent().parent().parent().find('.mainTable tbody tr').length;

	if (uploadType == "w9")
	{
		data = {
			value: ["name=" + form2Data.formName, "businessName=" + form2Data.businessName,
					"taxClass=" + form2Data.taxClass, "llcType=" + form2Data.llcType, 
					"address1=" + form2Data.address1,  "address2=" + form2Data.address2,
	        		"city=" + form2Data.city, "state=" + form2Data.state, "postalCode=" + form2Data.postalCode, 
	        		"ssn=" + form2Data.ssn, "ein=" + form2Data.ein, "listAccountNumbers=" + form2Data.listAccountNumbers,
	        		"signature=" + form2Data.signature, "clientId=000"
			],
			targetIndex: targetIndex, 
			targetProperty: currentTargetProperty,
			modelName: "W9"
		};
	} else {
		data = {
			value: ["name=" + form2Data.formName, "incCountry=" + form2Data.incCountry, "ownerType=" + form2Data.ownerType, 
				"address1=" + form2Data.address1, "address2=" + form2Data.address2,
	        	"city=" + form2Data.city, "state=" + form2Data.state, "postalCode=" + form2Data.postalCode, 
	        	"country=" + form2Data.country, 
	        	"mailingAddress1=" + form2Data.mailingAddress1, "mailingAddress2=" + form2Data.mailingAddress2,
	        	"mailingCity=" + form2Data.mailingCity, "mailingState=" + form2Data.mailingState, 
	        	"mailingPostalCode=" + form2Data.mailingPostalCode, 
	        	"mailingCountry=" + form2Data.mailingCountry, 
	        	"ssn=" + form2Data.ssn, "ein=" + form2Data.ein, "foreignId=" + form2Data.foreignId,
	        	"listAccountNumbers=" + form2Data.listAccountNumbers,
	        	"signature=" + form2Data.signature, "clientId=000"
			], //TODO:  get client ID
			targetIndex: targetIndex, 
			targetProperty: currentTargetProperty,
			modelName: "W8"
		};
	}

	jQuery.ajax({
			url:'/contacts/saveEmbeddedModel',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
			success:function(data){
				//reload step to get updated data
				goContactStep5();
			}
	});
}

function deleteFile(indexToDelete, targetProperty)
{
	var data = {
		targetIndex: indexToDelete, 
		targetProperty: targetProperty,
	};

	//erase the affiliation	
	jQuery.ajax({
		url:'/contacts/deleteEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success: function(data){
			//reload step to get updated data
			goContactStep5();
		}

	});
}

function setDefaultPhoto(event)
{
	var data = {
		defaultPhotoIndex: $(event.currentTarget).parent().index()
	};

	saveModel(data);
}