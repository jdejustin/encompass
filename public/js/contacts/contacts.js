///////////////////////////////////////////////////////////
///////////  COMMON FUNCTIONS FOR CONTACTS ////////////////
///////////////////////////////////////////////////////////
var selectPrompt = '';

function setPrompt(prompt)
{
	selectPrompt = prompt;
}

function saveProperty(target)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var serializedData = $(target).serialize();
		saveModel(serializedData);
	}
}

function saveModel(data)
{
	jQuery.ajax({
		url:'/contacts/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("saved");
		}
	});
}


function saveEmbeddedModel(data)
{
	jQuery.ajax({
		url:'/contacts/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("Saved: " + data);
		}
	});
}


function saveContactDefaultItem(target, targetProperty)
{
	var data = {
		value: $(target).val(),
		targetProperty: targetProperty,
	};

	jQuery.ajax({
		url:'/contacts/saveDefaultItem',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("saved default");
		}
	});
}

function showFirstDeleteButtonX(elementIndex,container,deleteButton)
{
	//if we are adding the second element we need to add the delete button to the first element that already exist
	if(elementIndex == 1 )
	{
		//container.find('.FormRowBackground').children(':nth-child(2)').append(deleteButton);
		container.find('.FormRowBackground').find('div:first').append(deleteButton);
	}
}

function enableDeleteButtonXByContainer(container)
{
	//enable all delete buttons after add the first delete button
	$(container).find(".DeleteButtonX").each(function( index ) {
		$(this).prop('disabled',false);	
		$(this).removeClass('Disable');	
	});
}

function disableDeleteButtonXByContainer(container)
{
   //disable all delete buttons after add the first delete button
	$(container).find(".DeleteButtonX").each(function( index ) {
		$(this).prop('disabled',true);	
		$(this).addClass('Disable');	
	});
}

function enableAddButtonByContainer(container)
{
	//enable button	
	$(container).find(".addButton").removeClass("Disable");
	$(container).find(".addButton").prop("disabled",false);
}

function changeElement(target)
{
	if ($(target).val() != "")
	{
		enableAddButtonByContainer($(target).parent().parent().parent());
	}
}

function changePhoneNumberElement(target)
{
	if ($(target).val() != "")
	{
		enableAddButtonByContainer($(target).parent().parent().parent().parent());
	}
}



function disableAddButtonByContainer(container)
{
	//enable button	
	$(container).find(".addButton").addClass("Disable");
	$(container).find(".addButton").prop("disabled",true);
}


function saveToContact(target, targetProperty, targetModel, defaultFlag)
{
	var validation = $(target).validationEngine('validate');

	if (!defaultFlag)
	{
		defaultFlag = false;
	}


	if($(target).val() && $(target).val() != selectPrompt && !validation)
	{
	    var targetIndex = $(target).parent().parent().find('.Index').val(); 
	    
		var data = {
			value: [$(target).attr("name") + "=" + $(target).val(), 'preferred=' + defaultFlag],
			targetIndex: targetIndex,
			targetProperty: targetProperty,
			modelName: targetModel
		};
	    
		//enable the delete buttons
		var container = $(target).parent().parent().parent();
		enableDeleteButtonXByContainer(container);
		enableAddButtonByContainer(container);
		
		saveEmbeddedModel(data);
	}
}

//this function updates the page copying(cloning) the element the we need to add and also return the copied element in case that we need
//to update personalized things for example in address if we clone an address previously marked as international its add a country so 
//we need to hide country in this case
function updatePage(target,deleteButton,indexToUpdate)
{
	//container that we are working on
	var container = $(target).parent();
	
	//add the first delete button to the container
	showFirstDeleteButtonX(indexToUpdate, container, deleteButton);
	
	//get the elementToclone
	var elementToClone = $(target).parent().children(":first-child").clone();
	
	//disable all delete buttons after add the first delete button
	disableDeleteButtonXByContainer(container);

	//old button
	var oldAddButton = $(target).parent().find(".addButton");
	
	var addButton = oldAddButton.clone();
	//remove the old button 
	oldAddButton.remove();

	//clean the inputs
	$(elementToClone).find("input[type=text]").each(function( index ) {
		$(this).prop('value','');	
	});

	//clean the selects
	$(elementToClone).find("select").each(function( index ) {
		$(this).find('option:first-child').prop('selected', true);
	});
	
	//clean the checkboxes
	$(elementToClone).find("input[type=checkbox]").each(function( index ) {
		$(this).removeAttr('checked');
	});

	//remove any tags from source that do not match current selected client
	elementToClone.find(".ItemDisplay").each(function( index ) {
		if ($(this).find('.ClientTag').attr('id') != $('#clientList').val())
		{
			$(this).remove();
		}
	});

	//clean the intNumberInputs
	var telInputElement = $(elementToClone).find(".intl-tel-input");

	//if exists a telInputElement we need to eliminate this and copy the number input only 
	if (telInputElement.length > 0)
	{
		var phoneElement = $(elementToClone).find(".PhoneNumber");
		telInputElement.before(phoneElement);
		telInputElement.remove();
	}
	
	//Fix for dealing with datePicker widget on the page, if it exists
	elementToClone.find(".js-expiration-datepicker").each(function( index ) {
		if( $(this).hasClass('hasDatepicker'))
		{
			console.log("destroying datepicker 0");
			//$(this).datepicker("destroy");
			console.log("destroying datepicker 1");
			$(this).removeClass("hasDatepicker");
			$(this).removeAttr("id");
			console.log("destroying datepicker 2");
		}
	});
	
	//add the new element
	$(container).append(elementToClone);

	//append the new button 
	container.append(addButton);
	//disable addButton
	disableAddButtonByContainer(container);
	
	//Update the index of the elements 
	container.find(".FormRowBackground").each(function( index ) {
		$(this).find('.Index').attr('value',index);	
	});

	return elementToClone;
}

function deleteElement(target,targetProperty,elementIndex,subTargetProperty)
{
	var container = $(target).parent().parent().parent();
	var indexToDelete = $(target).parent().parent().find('.Index').val(); 
	var addButton = $(container).find('.addButton');

	//check if the affiliation was saved
	var attr = addButton.prop('disabled');
	// For some browsers, `attr` is undefined; for others,
	// `attr` is false.  Check for both.
	if (typeof attr !== 'undefined' && attr !== false) 
	{
		// add button is disable so the affiliation was not saved
		//enable button	
		addButton.removeClass("Disable");
		addButton.prop( "disabled", false );
		
		//enable the delete buttons
		enableDeleteButtonXByContainer(container);
	}
	else
	{
		var data;
		if (subTargetProperty)
		{
			data = {
					targetIndex: 0, targetProperty:targetProperty,subTargetIndex:indexToDelete,subTargetProperty: subTargetProperty,
			};
		}
		else
		{
			data = {
					targetIndex: indexToDelete, targetProperty: targetProperty,
			};
		}
	
		jQuery.ajax({
			url:'/contacts/deleteEmbeddedModel',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
		});
	}
	
	//removing the elements
	$(target).parent().parent().remove();
	
	container.find(".FormRowBackground").each(function( index ) {
		$(this).find('.Index').attr('value',index);	
	});
	
	//just remove the delete only if we have 1 row
	if (elementIndex == 0 )
	{
		container.find(':first-child').find('.DeleteButtonX').remove();
	}
}

function switchData()
{
	//get selected client ID
	console.log("client name: " + $('#clientList').val());
	var data = {
    	id: $('#clientList').val(),
    	//clientName: $('#clientList').text()
    };

	jQuery.ajax({
		url:'/contacts/setCurrentClient',
		datatype:'json',
		type: 'POST',
		async: false,
		data: data,
		success: function(data) {
			//reload content
			reloadContent(data.currentStep);
		}
	});
}

function removeClientId(clientId, targetProperty)
{
	//get selected client ID
	var data = {
    	id: clientId,
    	targetProperty: targetProperty
    };

	jQuery.ajax({
		url:'/contacts/removeClientId',
		datatype:'json',
		type: 'POST',
		async: false,
		data: data,
		success: function(data) {
			//reload content
			reloadContent(data.currentStep);
		}
	});
} 




function reloadContent(currentStep)
{
	console.log(currentStep);
	this['goContactStep' + currentStep]();
	// var loadingString = '/contacts/contactStep'+stepNumber+' #AjaxContent';
 //    var functionName = 'initContactStep'+stepNumber;
	// jQuery('#content').load(loadingString,function(){
	// 	//this[functionName]();
	// });
}


function goContactDetails()
{
    jQuery('#content').load('/contacts/1234 #AjaxContent',function (data){
  	   if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	    else if (data == 'null')
	   {
			goContactsList();
	   } else {
			initShowContact();
	   }
	});
}


function goContactsList()
{
    jQuery('#content').load('/contacts #AjaxContent',function (data){
  	   if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
    		initContactIndex();
	   }
	});
}


function goContactStep1()
{
	jQuery('#content').load('/contacts/contactStep1 #AjaxContent',function (data){
	   if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	    	initContactStep1();
	   }
	});
}

function goContactStep2()
{
	jQuery('#content').load('/contacts/contactStep2 #AjaxContent',function (data){
	   if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initContactStep2();
	   }
	});
}

function goContactStep3()
{
	jQuery('#content').load('/contacts/contactStep3 #AjaxContent',function (data){
  	   if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
    		initContactStep3();
	   }
	});
}

function goContactStep4()
{
	jQuery('#content').load('/contacts/contactStep4 #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initContactStep4();
	   }
	});
}

function goContactStep5()
{
	jQuery('#content').load('/contacts/contactStep5 #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initContactStep5();
	   }
	});
}
