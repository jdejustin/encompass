$(function() {
  goToContactsHome();
  contactIndexLoaded = true;
	//initContactIndex();
});

function initContactIndex(){
  $('html, body').animate({ scrollTop: 0 }, 'slow');

  updateNavigation('contacts');
	
  initGlobal();

	$("[name='client']").change(function(){
		searchContactsByClientId();
	});

    $('.deleteSelectedBtn').click(function(){
        showDeleteContacts(this);
    });
    
    //init function to call when search is completed
	initClickOnTableRow();

	onRowClickFunction = initClickOnTableRow;

    //init the dialog to hide from HTML the first time 
    $( "#dialog-confirm-delete" ).dialog({autoOpen:false});
}

function initClickOnTableRow()
{
	$('.mainTable tbody td').not('.checkBoxTd').click(function(){
		jQuery("#content").load('/contacts/' + $(this).parent().find('[type=checkbox]').val() + ' #AjaxContent',function (data){
         if(data == 'false')
         {
            window.location = "/expired";
         }
         else
         {
			   initShowContact();
         }
		});
	});

    //add select class to checkbox
    $('.mainTable tbody td').find('[type=checkbox]').addClass('selectElement');
    
	//set the behaviour of the select all element
    setSelectionBehaviour();
}

function showDeleteContacts(element)
{
    var title = 'Delete Selected KOL(s)?';
    var message = 'Selected KOL(s) will be permanently deleted and cannot be recovered. Are you sure?';
    var deleteUrl = '/contacts/deleteContacts';

    //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
    initDeleteDialog(title,message,deleteUrl,element);
}


function createNewContact()
{
	jQuery("#content").load('/contacts/clearCurrentContactId #AjaxContent', function (data) {
      if(data == 'false')
      {
         window.location = "/expired";
      }
      else
      {
   		jQuery("#content").load('/contacts/contactStep1 #AjaxContent',function(){
   			initContactStep1();
   		});
      }
	});
}


function exportContacts()
{
  jQuery("#content").load('/exports/setCurrentExportObject #AjaxContent', {exportType: 'allContacts', model:'Contact'}, function (data) {
      if(data == 'false')
      {
         window.location = "/expired";
      } else {
        initExport();
      }
  });
}


function importContacts()
{
    jQuery('#content').load('/contacts/import #AjaxContent',function (data){
      if(data == 'false')
      {
         window.location = "/expired";
      }
      else
      {
    	 initContactsImport();
      }
    });
}