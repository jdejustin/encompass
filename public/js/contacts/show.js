
function initShowContact() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');

	initGlobal();

	$('#editContactBtn').click(function(){
		jQuery("#content").load('/contacts/edit #AjaxContent', function (data) {
         if(data == 'false')
         {
            window.location = "/expired";
         }
         else
         {
   			jQuery("#content").load('/contacts/contactStep1 #AjaxContent', function(){
   				initContactStep1();
   			});
         }
		});
	});


	$('#exportContactBtn').click(function(){
		console.log("Export this Contact!!");
	});

	$('#linkToClientBtn').click(function(){

		var successCallback = function ()
    	{
    		goContactStep1();
    	};	

    	var url = '/contacts/linkToNewClient';

      initClientLinkDialog(url,successCallback,false);
	});

	$( "#dialog-clientLink" ).dialog({autoOpen: false});

	//init function to call when search is completed
    initClickOnTableRowUserShowTables();

    //load photo, if any

    var path = $('#contactImg').attr('data-pathToFile');
    getUserPhoto(path.substring(20), 'contactImg');
}

function initClickOnTableRowUserShowTables()
{
    $('.mainTable tbody td').click(function () {
    	var path = $(this).parent().find('.js-filePath').attr('data-path');
    	if (path)
    	{
      		displayFile(path);
    	} 
    });
}