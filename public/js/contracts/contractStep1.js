function initContractStep1() {
	initGlobal();

	$('html, body').animate({ scrollTop: 0 }, 'slow');

	$('.mainTable tbody td').not('.checkBoxTd').click(function(){

		//highlight selected row
		$(this).parent('tr').addClass("selected").siblings().removeClass("selected");

		var serializedData = 'contactId=' + $(this).prop('id');

      	jQuery.ajax({
			url:'/contracts/save',
			datatype:'json',
			type: 'POST',
			async: false,
			data: serializedData,
			success: updateSelectedContactDisplay
		});
	});
}

function updateSelectedContactDisplay(data) {
	$('#selectedContactDisplay').text('Selected KOL:  ' + data.contactName);

	var displayData = 'contractName=' + data.contactName;
	saveContractModel(displayData);

	// saveContractModel('contractType=MainContract');
}

function launchContract(path)
{
	displayFile(path);
}