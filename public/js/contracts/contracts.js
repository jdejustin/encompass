///////////////////////////////////////////////////////////
///////////  COMMON FUNCTIONS FOR CONTRACTS ////////////////
///////////////////////////////////////////////////////////
function saveContractProperty(target)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var serializedData = $(target).serialize();
		saveContractModel(serializedData);
	}
}

function saveContractModel(data)
{
	jQuery.ajax({
		url:'/contracts/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("saved");
		}
	});
}


function goContractsList()
{
    jQuery('#content').load('/contracts #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initContractsIndex();
	   }
	});
}


function goContractStep1()
{
	jQuery('#content').load('/contracts/contractStep1 #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
		   initContractStep1();
	   }
	});
}

function goContractStep2()
{
	jQuery('#content').load('/contracts/contractStep2 #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initContractStep2();
	   }
	});
}
