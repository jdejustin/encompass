function initContractsIndex() {
  initGlobal();

  $('html, body').animate({ scrollTop: 0 }, 'slow');

  $('#advancedSearch').click(function(){
    //display advanced search options
    $('#advancedSearchOptions').toggle();
  });


 //  //set the behaviour of the select all element
  setSelectionBehaviour();


	$('.deleteSelectedBtn').click(function(){
      showDeleteContracts(this);
    });

  //init function to call when search is completed
  initClickOnTableRowContracts();

  onRowClickFunction = initClickOnTableRowContracts;
 
  $( "#dialog-confirm-delete" ).dialog({autoOpen: false});
};


function initClickOnTableRowContracts()
{
  $('.mainTable tbody td').not('.checkBoxTd').click(function(){
    jQuery("#content").load('/contracts/' + $(this).parent().find('[type=checkbox]').val() + ' #AjaxContent', function (data){
      if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
        initContractsShow();
     }
        
    });
  });
}

function showDeleteContracts(element)
{
  var callBack = function (data)
  {
     //finally, remove programs from clients
     jQuery("#content").load('/contracts/deleteContracts #AjaxContent', {'ids': data}, function (data) {
        if(data == 'false')
         {
            window.location = "/expired";
         }
         else
         {
            goContractsList();
         }
     });
  }

  var title = 'Delete Selected Contract(s)?';
  var message = 'Selected Contract(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/contracts/deleteContractFromClientOrProgram';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element, callBack);
}


function createNewContract(value)
{
  // if (value != 0)
  // {
  	jQuery("#content").load('/contracts/clearCurrentContract #AjaxContent', function (data){
      if(data == 'false')
       {
          window.location = "/expired";
       }
       else
       {
          jQuery("#content").load('/contracts/contractStep1 #AjaxContent', function(){
              initContractStep1();
  	       });
        }
  	});
  // }
}  