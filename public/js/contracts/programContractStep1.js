function initProgramContractStep1() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	$(".ContractProperty").focusout(function (){saveContractProperty(this);});

	fileDisplayNameInput = $('#displayName');
	selectedFile = $('#selectedFile');
	uploadNotes = $('#notes');

	$('#File').on('change', onFileSelected);

	allTextFields = $( [] ).add( fileDisplayNameInput ).add(selectedFile),
    tips = $( ".validateTips" );

	$("#creationDate" ).datepicker({
		onClose: function(dateText,datePickerInstance) {
        var oldValue = $(this).data('oldValue') || "";
	        if (dateText !== oldValue) {
	            $(this).data('oldValue', dateText);
	            saveContractProperty(this);
	           // saveSelectedDate($(this).prop('id'), $(this).val());
	        }
	    }
	});

	$("#expirationDate" ).datepicker({
		onClose: function(dateText,datePickerInstance) {
	        var oldValue = $(this).data('oldValue') || "";
	        if (dateText !== oldValue) {
	            $(this).data('oldValue', dateText);
	            saveContractProperty(this);
	           // saveSelectedDate($(this).prop('id'), $(this).val());
	        }
	    }
	});

	function saveSelectedDate(property, value)
	{
 		var data = property + '=' + value;
		saveContractModel(data);
	}

    $( "#uploadFile-form" ).dialog({autoOpen: false});
}


//Called by upload file buttons
function prepareUploadProgramContractFile(target, id)
{
	uploadTargetId = id;

	currentTarget = $(target).parent().parent().find('.documentContainer');

	fileUploadSuccessFunction = onContractFileUploadSuccess;

	var title = 'Upload New Document';
	initUploadFileDialog(title, uploadFileFunction, uploadFileDialogCloseFunction);
}


function onContractFileUploadSuccess(returnedData)
{
	currentTarget.empty();

	var div = $('<div>');

	var deleteBtn = $('<input>', {
		id: 'deleteBtn',
		title: 'Delete Document',
		type: 'button'
	});

	deleteBtn.on('click', onDeleteContractBtn);


	// var editBtn = $('<input>', {
	// 	id: 'editBtn',
	// 	title: 'Edit Document',
	// 	type: 'button'
	// });

	// editBtn.click(function(){

	// });

	
	var a = $('<a>', {
			text: uploadData.displayName,
			href: '/storage/uploadedFiles/' + returnedData.fileFullName,
			target: '_blank',
			id: uploadData.displayName
			});
	var br = $('<p/>');

	deleteBtn.appendTo(div);
	// editBtn.appendTo(div);
	a.appendTo(div);
	br.appendTo(div);
	div.appendTo(currentTarget);

	//save this new file info into the contact's record
	saveUploadedContractFileToDB('/storage/uploadedFiles/' + returnedData.fileFullName);
}

function onDeleteContractBtn(target)
{
	removeFromDB($(target).parent().index(), $(target).parent().parent().attr('id'));
	
	$(target).parent().remove();
}

function removeFromDB(indexToDelete, listName)
{
	var data = {
		targetIndex: indexToDelete, 
		targetProperty: listName.substring(0, listName.indexOf("L")),
	};

	//erase the contract	
	jQuery.ajax({
		url:'/contracts/deleteEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
	});
}

function saveUploadedContractFileToDB(filePath)
{
	// var targetIndex = currentTarget.find( "a" ).length - 1;
	var targetProperty;
	var data;

	var progType = '';

	data = {
			value: ["name=" + uploadData.displayName, "pathToFile=" + filePath],
			// targetIndex: targetIndextargetIndex, 
			targetIndex: 0, 
			targetProperty: 'contractFile',
			modelName: 'UploadedFile',
	};

	jQuery.ajax({
		url:'/contracts/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			var displayData = 'contractName=' + uploadData.displayName;
			saveContractModel(displayData);

			// saveContractModel('contractType=' + progType);
		}
	});
}