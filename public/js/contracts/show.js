function initContractsShow(){
	$('html, body').animate({ scrollTop: 0 }, 'slow');
}

function editContract() {
	jQuery("#content").load('/contracts/contractStep1 #AjaxContent', function (data){
      if(data == 'false')
      {
         window.location = "/expired";
      }
      else
      {
         initContractStep1();
      }
	});
}