(function () {
var Address = function () {

  this.defineProperties({
    addressType: {type: 'string'},
    international: {type: 'boolean'},
    address1: {type: 'string'},
    address2: {type: 'string'},
    city: {type: 'string'},
    state: {type: 'string'},
    postalCode: {type: 'string'},
    country: {type: 'string'},
    clients: {type: 'object'}
  });
};

exports.Address = Address;

}());

(function () {
var Affiliation = function () {

  this.defineProperties({
    affiliationName: {type:'string'},
    title: {type: 'string'},
    city: {type: 'string'},
    postalCode: {type: 'string'},
    country: {type: 'string'}
  });

};


Affiliation = geddy.model.register('Affiliation', Affiliation);
}());

(function () {
var Attendee = function () {

  this.defineProperties({
    contactId: {type: 'string'},
    speaker: {type: 'boolean'},
    attendingDinner: {type: 'boolean'},
    dietaryRestrictions: {type: 'string'},
    specialNeeds: {type: 'string'},
    hotelArrivalDate: {type: 'date'},
    hotelDepartureDate: {type: 'date'},
    hotelConfirmationNumber: {type: 'number'},
    hotelBilling: {type: 'number'},
    roomTypeId: {type:'number'},
    hotelNotes: {type: 'string'},
    travelItineraryReceived: {type: 'boolean'},
    travelArrivalDateTime: {type: 'datetime'},
    arrivalInfo: {type: 'number'},
    arrivalLocation: {type: 'string'},
    carArrivalConfirmationNumber: {type: 'number'},
    carArrivalLocation: {type: 'string'},
    passengersNumber: {type: 'number'},
    travelDepartureDate: {type: 'date'},
    travelDepartureInfo: {type: 'string'},
    travelDepartureTime: {type: 'time'},
    carDepartureLocation: {type: 'string'},
    carDepartureConfirmationNumber: {type: 'number'},
    pickupTime: {type: 'time'},
    recordLocator: {type: 'number'},
    comments: {type: 'text'},
    airfareCost: {type: 'number'},
    travelDocument: {type: 'object'},  //uploadedDocument
  });

};

Attendee = geddy.model.register('Attendee', Attendee);
}());

(function () {
var AttendeeStatusType = function () {

  this.defineProperties({
    attendeeStatusTypeName: {type: 'string'}
  });

  this.hasMany('Attendees');
  
};

AttendeeStatusType = geddy.model.register('AttendeeStatusType', AttendeeStatusType);
}());

(function () {
var AttendeeType = function () {

  this.defineProperties({
    attendeeTypeName: {type: 'string'}
  });

  this.hasMany('Attendees');
  

};



AttendeeType = geddy.model.register('AttendeeType', AttendeeType);
}());

(function () {
var BusinessOwner = function () {

  this.defineProperties({
    businessOwnerName: {type: 'string'},
    description: {type: 'text'}
  });

};

exports.BusinessOwner = BusinessOwner;

}());

(function () {
var Chain = function () {

  this.defineProperties({
    chainName: {type: 'string'}
  });

};

exports.Chain = Chain;

}());

(function () {
var Client = function () {

  this.defineProperties({
    name: {type: 'string'},
    crgPrefixCode: {type: 'string'},
    crgBusinessDevelopmentName: {type: 'string'},
    products: {type: 'object'},  //array of ClientProduct
    therapeuticAreas: {type: 'text'},
    complianceSystem: {type: 'string'},
    deliveryMethod: {type: 'string'},
    addresses: {type: 'object'},//array of Address ;; model file name: address
    clientContacts : {type: 'object'},  //array of ClientContact
    financeContact: {type: 'object'},  //contactInfo
    salesForceLabelsLevel1: {type: 'object'},
    salesForceLabelsLevel2: {type: 'object'},
    salesForceLabelsLevel3: {type: 'object'},
    medicalAffairs: {type: 'object'},
    payors: {type: 'object'},
    programs: {type: 'object'}, // array of Program ids
    venues: {type: 'object'},  // array of Venue ids
    reps: {type: 'object'}, //array of Rep 
    contracts: {type: 'object'}, //array of contract ids
    guidelines: {type: 'object'}, //array of UploadedFile
    kolResources: {type: 'object'}, // array of UploadedFile
    repResources: {type: 'object'}, // array of UploadedFile
    rsmResources: {type: 'object'}, // array of UploadedFile
    clientResources: {type: 'object'} // array of UploadedFile
  });
};

Client = geddy.model.register('Client', Client);
}());

(function () {
var ClientContact = function () {

  this.defineProperties({
    title: {type: 'string', required: true},
    firstName: {type: 'string'},
    middleName: {type: 'string'},
    lastName: {type: 'string'},
    suffix: {type: 'string'},
    degrees: {type: 'object'}, //array of strings
    department: {type: 'string'}, 
    addresses: {type: 'object'},//array of Address ;; model file name: address
    phoneNumbers: {type: 'object'}, //array of ContactPhoneNumber
    emailAddresses: {type: 'object'}, //array of ContactEmailAddress
    officeAdminContact: {type: 'object'}, //ContactInfo
    officeAdminContactPhoneNumbers: {type: 'object'}, //array of ContactPhoneNumber
    officeAdminContactAddresses: {type: 'object'} //array of Address
  });
};

exports.ClientContact = ClientContact;

}());

(function () {
var ClientProduct = function () {

  this.defineProperties({
    productName: {type: 'string'}
  });
};

exports.ClientProduct = ClientProduct;

}());

(function () {
var Communication = function () {

  this.defineProperties({
    communicationType: {type: 'string'},
    recepientName: {type: 'string'},
    recepientEmail: {type: 'string'},
    dateSent: {type: 'date'}
  });

};

exports.Communication = Communication;

}());

(function () {
var Contact = function () {

  this.defineProperties({
    contactType: {type: 'object'}, //string, possible values: kol, attendee
    defaultPhotoIndex: {type: 'number'},
    clients: {type: 'object'}, //array of ids
    firstName: {type: 'string'},
    middleName: {type: 'string'},
    lastName: {type: 'string'},
    suffix: {type: 'string'},
    salutation: {type: 'string'},
    degrees: {type: 'object'}, //array of strings
    otherDegree: {type: 'string'},
    affiliations: {type: 'object'}, //array of Affiliation
    emailAddresses: {type: 'object'},//array of ContactEmailAddress :: model file name: contact_email_address
    defaultEmail: {type: 'string'},
    phoneNumbers: {type: 'object'},//array of ContactPhoneNumber :: model file name: contact_phone_number
    defaultPhoneNumber: {type: 'string'},
    addresses: {type: 'object'},//array of Address ;; model file name: address
    defaultAddress: {type: 'string'},
    officeStaffContact: {type: 'object'},//array of ContactInfo :: model file name: contact_info
    emergencyContacts: {type: 'object'}, //array of ContactInfo
    otherContacts: {type: 'object'}, //array of ContactInfo
    specialties: {type: 'object'}, //array of strings
    therapeuticInterests: {type: 'object'}, //array of strings
    medicalLicenses: {type: 'object'}, //array of MedicalLicense
    otherMedicalLicenses: {type: 'object'}, //array of OtherMedicalLicense
    npi: {type: 'string'},
    dea: {type: 'string'},
    tsaName: {type: 'string'},
    tsaGender: {type: 'string'},
    tsaDOB: {type: 'string'},
    passports: {type: 'object'}, // array of ContactPassport
    airlines : {type: 'object'},  //array of TravelAir
    hotels :{type: 'object'},  // array of TravelHotel
    dietaryRestrictions: {type: 'text'},
    specialNeeds: {type: 'text'},
    taxForms: {type: 'object'}, //array of W9 and W8
    cvs: {type: 'object'}, //array of UploadedFile
    bios: {type: 'object'}, //array of UploadedFile
    factSheets: {type: 'object'}, //array of UploadedFile
    contracts: {type: 'object'}, // contract ids
    photos: {type: 'object'}, //array of UploadedFile
    speakerType: {type: 'object'},
    trainingEvents: {type: 'object'}, //array of TrainingEvent
    appropoTrainingStatus: {type: 'boolean'},
    uniqueSpeakerIdentifier: {type: 'number'},
    reps: {type: 'object'}, // array of ids
    notes:{type: 'text'}, //this must be visible only to admins or higher level
    quickbooksName:{type: 'string'}
  });
};


Contact = geddy.model.register('Contact', Contact);
}());

(function () {
var ContactEmailAddress = function () {

  this.defineProperties({
    email: {type: 'string'},
    prefered: {type: 'boolean'},
    clients: {type: 'object'}
  });

};


exports.ContactEmailAddress = ContactEmailAddress;

}());

(function () {
var ContactInfo = function () {

  this.defineProperties({
    fullName: {type: 'string'},
    title: {type: 'string'},
    email: {type: 'string'}, 
    phoneNumbers: {type: 'object'},// model file name contactPhoneNumber
    relationship: {type: 'string'},
    addresses: {type: 'object'}, //array of Address
    department: {type: 'string'}, 
  });

};

exports.ContactInfo = ContactInfo;

}());

(function () {
var ContactPassport = function () {

  this.defineProperties({
    number: {type: 'number'},
    country: {type: 'string'},
    expirationDate: {type: 'string'}
  });

};

exports.ContactPassport = ContactPassport;

}());

(function () {
var ContactPhoneNumber = function () {

  this.defineProperties({
    number: {type: 'number'},
    phoneType: {type: 'string'},
    clients: {type: 'object'}
  });


};

exports.ContactPhoneNumber = ContactPhoneNumber;

}());

(function () {
var ContactType = function () {

  this.defineProperties({
    contactTypeName: {type: 'string', required: true}
  });

};

ContactType = geddy.model.register('ContactType', ContactType);
}());

(function () {
var Contract = function () {

  this.defineProperties({
    contractName: {type: 'string'},
    contractFile: {type: 'object'},
    creationDate: {type: 'date'},
    expirationDate: {type: 'date'},
    status: {type: 'string'},
    linkedTo: {type: 'string'}, //id of program linked to or 'MainContract'
    contactId: {type: 'string'},
  });

};

Contract = geddy.model.register('Contract', Contract);
}());

(function () {
var Event = function () {

  this.defineProperties({
    brand: {type: 'string'},
    submitingTimes: {type: 'number'},
    programId: {type: 'string'},
    presentationTitle: {type: 'string'},
    eventType: {type: 'string'},
    jobNumber: {type: 'number'},
    status: {type: 'number'},
    crgPercentageFee: {type: 'number'},
    requestingRepId: {type: 'string'},
    services: {type: 'object'}, //array of serviceLevel
    city: {type: 'string'},
    state: {type: 'string'},
    eventStartTime: {type: 'string'},
    timezone: {type: 'string'},
    eventDatePref1: {type: 'string'},
    eventDatePref2: {type: 'string'},
    eventDatePref3: {type: 'string'},
    confirmedDate: {type: 'string'},
    dateLocationComments: {type: 'text'},
    potentialSpeakers: {type: 'object'},  //array of Speaker
    venueId: {type: 'string'},
    venueRoomName: {type: 'string'},
    venueGuranteedNumber: {type: 'number'},
    catererId: {type:'string'},
    catererStatus: {type: 'string'},
    estimatedMealCost: {type: 'number'},
    expectedNumberAttendees: {type: 'number'},
    venueAVStatus: {type: 'string'},
    venueComments: {type: 'text'},
    invitationsPDF: {type: 'boolean'},
    invitationsHardcopyDirect: {type: 'boolean'},
    invitationsHardcopyRep: {type: 'boolean'},
    repMailingAddressEdits: {type: 'text'},
    invitationsComments: {type: 'text'},
    emailInvitations: {type: 'object'},  //array of elements
    hardcopyInvitations: {type: 'object'}, //array of elements
    audienceRecruitmentComments: {type: 'text'}, 
    attendees: {type: 'object'},  //array of Attendee
    preconfirmedNumber: {type: 'number'},
    communications: {type: 'object'}, //array of UploadedFile
    prfId: {type: 'string'},  //id of EventRequest
    historical:{type:'object'}// array of historical
  });
};

exports.Event = Event;

}());

(function () {
var Expense = function () {

  this.defineProperties({
    date: {type: 'date'},
    description: {type: 'text'},
    total: {type: 'number'},
    notes: {type: 'text'}
  });

  /*
  this.property('login', 'string', {required: true});
  this.property('password', 'string', {required: true});
  this.property('lastName', 'string');
  this.property('firstName', 'string');

  this.validatesPresent('login');
  this.validatesFormat('login', /[a-z]+/, {message: 'Subdivisions!'});
  this.validatesLength('login', {min: 3});
  // Use with the name of the other parameter to compare with
  this.validatesConfirmed('password', 'confirmPassword');
  // Use with any function that returns a Boolean
  this.validatesWithFunction('password', function (s) {
      return s.length > 0;
  });

  // Can define methods for instances like this
  this.someMethod = function () {
    // Do some stuff
  };
  */

};

/*
// Can also define them on the prototype
Expense.prototype.someOtherMethod = function () {
  // Do some other stuff
};
// Can also define static methods and properties
Expense.someStaticMethod = function () {
  // Do some other stuff
};
Expense.someStaticProperty = 'YYZ';
*/

exports.Expense = Expense;

}());

(function () {
var Historical = function () {

  this.defineProperties({
    date: {type: 'date'},
    previousState: {type: 'int'},
    currentState: {type: 'int'},
    comment: {type: 'text'},
    modifier: {type: 'string'},
    objectSnapshot:{type:'string'}
  });
};

exports.Historical = Historical;

}());

(function () {
var Invoice = function () {

  this.defineProperties({
    expenses: {type: 'object'},
    dateCheckSent: {type: 'date'},
    checkNumber: {type: 'number'},
    dateAcknowledgementSent: {type: 'date'},
    notes: {type: 'text'}
  });

  /*
  this.property('login', 'string', {required: true});
  this.property('password', 'string', {required: true});
  this.property('lastName', 'string');
  this.property('firstName', 'string');

  this.validatesPresent('login');
  this.validatesFormat('login', /[a-z]+/, {message: 'Subdivisions!'});
  this.validatesLength('login', {min: 3});
  // Use with the name of the other parameter to compare with
  this.validatesConfirmed('password', 'confirmPassword');
  // Use with any function that returns a Boolean
  this.validatesWithFunction('password', function (s) {
      return s.length > 0;
  });

  // Can define methods for instances like this
  this.someMethod = function () {
    // Do some stuff
  };
  */

};

/*
// Can also define them on the prototype
Invoice.prototype.someOtherMethod = function () {
  // Do some other stuff
};
// Can also define static methods and properties
Invoice.someStaticMethod = function () {
  // Do some other stuff
};
Invoice.someStaticProperty = 'YYZ';
*/

exports.Invoice = Invoice;

}());

(function () {
var MedicalLicense = function () {

  this.defineProperties({
    state: {type: 'string'},
    number: {type: 'number'}
  });

};


exports.MedicalLicense = MedicalLicense;

}());

(function () {
var OtherMedicalLicense = function () {

  this.defineProperties({
    otherMedicalLicenseType: {type: 'string'},
    number: {type: 'number'}
  });

};


exports.OtherMedicalLicense = OtherMedicalLicense;

}());

(function () {
var Passport = function () {
  this.defineProperties({
    authType: {type: 'string'},
    key: {type: 'string'}
  });

  this.belongsTo('User');
};

Passport = geddy.model.register('Passport', Passport);

}());

(function () {
var Program = function () {

  this.defineProperties({
    programName: {type: 'string'},
    programTypeId: {type: 'string'},
    poNumber: {type: 'number'},
    crgJobNumber: {type: 'number'},
    businessOwnerName: {type: 'string'},
    businessOwnerDescription: {type: 'text'},
    programNotes: {type: 'text'},
    events: {type: 'object'}, //array of Event
    taxForms: {type: 'object'}, //array of UploadedFile
    crgProgramManager: {type: 'string'},  // crg admin id in charge of program
    communications: {type: 'object'}, //array of UploadedFile
    resources: {type: 'object'}, //array of UploadedFile
    contracts: {type: 'object'}, //array of Contract
    reps: {type: 'string'} //array of rep ids
  });
};

Program = geddy.model.register('Program', Program);
}());

(function () {
var ProgramType = function () {

  this.defineProperties({
    programTypeName: {type: 'string', required: true},
    services: {type: 'object'}
  });
};

exports.ProgramType = ProgramType;

}());

(function () {
var Rep = function () {

  this.defineProperties({
    title: {type: 'string'},
    firstName: {type: 'string'},
    middleName: {type: 'string'},
    //clients: {type: 'object'}, //array of clients ids
    lastName: {type: 'string'},
    suffix: {type: 'string'},
    team: {type: 'number'},
    role: {type: 'text'},
    level: {type: 'string'},
    zone: {type: 'number'},
    prtId: {type: 'number'},
    territory: {type: 'number'},
    territoryName: {type: 'string'},
    status: {type: 'string'},
    clientRepId: {type: 'string'},
    addresses: {type: 'object'},
    defaultAddress: {type: 'string'},
    phoneNumbers: {type: 'object'},
    defaultPhoneNumber: {type: 'string'},
    emailAddresses: {type: 'object'},
    defaultEmail: {type: 'string'},
    icEligibilityDate: {type: 'date'},
    hireDate: {type: 'date'},
    termDate: {type: 'date'},
    loaStart: {type: 'date'},
    loaEnd: {type: 'date'},
    jdeId: {type: 'string'},
    managerId: {type: 'string'}
  });
};

Rep = geddy.model.register('Rep', Rep);
}());

(function () {
var Role = function () {

  this.defineProperties({
    roleName: {type: 'string', required: true},
  	acl: { type: 'object'}
  	/*
  	[{aclName:"kol",actions:["add","edit","delete","view"]},
	{aclName:"client",actions:["add","edit","delete","view"]}]
  	 */
  });
  
  this.hasMany('Users'); 
};

//geddy.model.loadedAdapters.<ModelName>.client

  /*
  this.property('login', 'string', {required: true});
  this.property('password', 'string', {required: true});
  this.property('lastName', 'string');
  this.property('firstName', 'string');

  this.validatesPresent('login');
  this.validatesFormat('login', /[a-z]+/, {message: 'Subdivisions!'});
  this.validatesLength('login', {min: 3});
  // Use with the name of the other parameter to compare with
  this.validatesConfirmed('password', 'confirmPassword');
  // Use with any function that returns a Boolean
  this.validatesWithFunction('password', function (s) {
      return s.length > 0;
  });

  // Can define methods for instances like this
  this.someMethod = function () {
    // Do some stuff
  };
  */


/*
// Can also define them on the prototype
Role.prototype.someOtherMethod = function () {
  // Do some other stuff
};
// Can also define static methods and properties
Role.someStaticMethod = function () {
  // Do some other stuff
};
Role.someStaticProperty = 'YYZ';
*/

Role = geddy.model.register('Role', Role);
}());

(function () {
var Service = function () {

  this.defineProperties({
    serviceName: {type: 'string'},
    percentage: {type: 'number'},
    numberHours: {type: 'string'}
  });
  
};

Service = geddy.model.register('Service', Service);
}());

(function () {
var ServiceLevel = function () {

  this.defineProperties({
    service: {type: 'object'},
    percentage: {type: 'int'},
    hours: {type: 'int'}
  });

};

exports.ServiceLevel = ServiceLevel;

}());

(function () {
var Speaker = function () {

  this.defineProperties({
    contactId: {type: 'string'},
    firstName: {type: 'string'},
    status: {type: 'string'},
    city: {type: 'string'},
    state: {type: 'string'},
    travelType: {type: 'string'},
    groundTransportation: {type: 'string'},
    comments: {type: 'text'}
  });

};

exports.Speaker = Speaker;

}());

(function () {
var TravelAir = function () {

  this.defineProperties({
    airlineName: {type: 'string'},
    ffNumber: {type: 'string'}
  });

};

exports.TravelAir = TravelAir;

}());

(function () {
var TravelHotel = function () {

  this.defineProperties({
    hotelName: {type: 'string'},
    rewardsNumber: {type: 'string'}
  });

};


exports.TravelHotel = TravelHotel;

}());

(function () {
var TravelPassport = function () {

  this.defineProperties({
    number: {type: 'string'},
    country: {type: 'string'},
    expirationDate: {type: 'date'}
  });

};

exports.TravelPassport = TravelPassport;

}());

(function () {
var UploadedFile = function () {

  this.defineProperties({
    name: {type: 'string'},
    pathToFile: {type: 'string'},
    clients: {type: 'object'},
    notes: {type:'text'},
  });

};

exports.UploadedFile = UploadedFile;

}());

(function () {
var User = function () {
    this.defineProperties({
        // username: { type: 'string', required: true },
        password: { type: 'string'},
        firstName: { type: 'string', required: true },
        middleName: { type: 'string'},
        lastName: { type: 'string', required: true },
        email: { type: 'string', required: true },
        ccEmail: { type: 'string' },
        signature: { type: 'text' },
        photo: { type: 'string' },
        state: { type: 'int' },
        guid: { type: 'string' },
        linkedId: {type: 'string'} //a rep or contact id... if this user was created via Rep or KOL creation
    });

    // this.validatesLength('username', { min: 3 });
    this.validatesLength('firstName', { min: 3 });
    this.validatesLength('lastName', { min: 3 });
    // this.validatesFormat('email', /[]);
    this.hasMany('Passports');

    /* this */
    // this.validateAction = function (controller, action) {
    //     // Do some stuff
    //     var returnValue = false;
    //     // console.log('>>>>>> VALIDATE THIS CONTROLLER '+ controller + " ACTION "+action);
    //     role = this.session.get('sessionUserRole');
    //     if (role) {
    //         for (i = 0; i < role.acl.length; i++) {
    //             // console.log('CONTROLLER '+ controller + " role acl Name "+role.acl[i].aclName);
    //             if (role.acl[i].aclName == controller) {
    //                 for (j = 0; j < role.acl[i].actions.length; j++) {
    //                     console.log('role action  ' + role.acl[i].actions[j] + " action " + action);
    //                     if (role.acl[i].actions[j] == action) {
    //                         returnValue = true;
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     return returnValue;
    // };
};


User = geddy.model.register('User', User);


}());

(function () {
var Venue = function () {

  this.defineProperties({
    venueName: {type: 'string'},
    addresses: {type: 'object'}, //array of Address
    phoneNumbers: {type: 'object'}, //array of ContactPhoneNumber
    mainEmail: {type: 'string'},
    contact: {type: 'object'}, //one ContactInfo
    website: {type: 'string'},
    venueType: {type: 'string'},
    foodType: {type: 'string'},
    venueNotes: {type: 'text'},
    avNotes: {type: 'text'},
    chain: {type: 'string'},
    rooms: {type: 'object'}, //array of VenueRoom
    venueAV: {type: 'string'}, 
    parking: {type: 'string'}, 
    hours: {type: 'string'},
    rating: {type: 'string'},
    pharmaMenuAvailable: {type: 'string'},
    stateSalesTax: {type: 'number'},
    serviceGratuity: {type: 'number'},
    roomRate: {type:'number'}
  });

};

Venue = geddy.model.register('Venue', Venue);
}());

(function () {
var VenueRoom = function () {

  this.defineProperties({
    roomName: {type: 'string'},
    roomFee: {type: 'number'},
    location: {type: 'string'},
    roomType: {type: 'string'},
    size: {type: 'string'},
    dimensions: {type: 'string'},
    maxCapacity: {type: 'string'}, 
    headcountMin: {type: 'number'},
    foodBeverageMin: {type: 'number'},
    inRoomAV: {type: 'string'},
    avCosts: {type: 'string'} 
  });

};

exports.VenueRoom = VenueRoom;

}());

(function () {
var W8 = function () {

  this.defineProperties({
    name: {type: 'string'},
    incCountry: {type: 'string'},
    uploadedFile: {type: 'object'},
    ownerType: {type:'string'},
    address1: {type: 'string'},
    address2: {type: 'string'},
    city: {type: 'string'},
    state: {type: 'string'},
    postalCode: {type: 'string'},
    country: {type: 'string'},
    mailingAddress1: {type: 'string'},
    mailingAddress2: {type: 'string'},
    mailingCity: {type: 'string'},
    mailingState: {type: 'string'},
    mailingPostalCode: {type: 'string'},
    mailingCountry: {type: 'string'},
    ssn: {type: 'string'},
    ein: {type: 'string'},
    foreignId: {type: 'string'},
    referenceNumbers: {type: 'string'},
    signature: {type: 'date'},
    clients: {type: 'object'}
  });
};

exports.W8 = W8;

}());

(function () {
var W9 = function () {

  this.defineProperties({
    name: {type: 'string'},
    uploadedFile: {type: 'object'},
    businessName: {type: 'string'},
    taxClass: {type: 'string'},
    llcType: {type: 'string'},
    address1: {type: 'string'},
    address2: {type: 'string'},
    city: {type: 'string'},
    state: {type: 'string'},
    postalCode: {type: 'string'},
    ssn: {type: 'string'},
    ein: {type: 'string'},
    exemptPayeeCode: {type: 'string'},
    exemptFatcaCode: {type: 'string'},
    listAccountNumbers: {type: 'string'},
    signature: {type: 'date'},
    clients: {type: 'object'}
  });
};

exports.W9 = W9;

}());