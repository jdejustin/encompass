function initEventCommunications() {
  	initGlobal();

  	//set the behaviour of the select all element
	setSelectionBehaviour();  

	$('.deleteSelectedBtn').click(function(){
		showDeleteEventCommunications(this);
	});

	//init function to call when search is completed
	initClickOnTableRowEventCommunications();

	$('#File').on('change', onFileSelected);

	$( "#dialog-confirm-delete" ).dialog({autoOpen: false});
	$( "#uploadFile-form" ).dialog({autoOpen: false});
}

function initClickOnTableRowEventCommunications()
{
    $('.mainTable tbody td').not('.checkBoxTd').click(function () {
      displayFile($(this).parent().find('[type=checkbox]').val());
    });
}

function createNewEventCommunication(id)
{
	$('#uploadNotes').show();

	fileUploadSuccessFunction = onEventCommunicationFileUploadSuccess;

  	uploadTargetId = id;

	var title = 'Upload New Document';
	initUploadFileDialog(title, uploadFileFunction, uploadFileDialogCloseFunction);
}


function onEventCommunicationFileUploadSuccess(returnedData)
{
	var filePath = '/storage/uploadedFiles/' + returnedData.fileFullName;
	var data;
	var totalItems = $('.mainTable tbody tr').length;

	data = {
			value: ["name=" + uploadData.displayName, "pathToFile=" + filePath, "notes=" + uploadData.notes],
			targetIndex: totalItems, 
			targetProperty: 'communications',
			modelName: 'UploadedFile',
	};

	jQuery.ajax({
		url:'/events/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			goToDeliveryCommunications();
		}
	});
}


function showDeleteEventCommunications(element)
{
  // var callBack = function (data)
  // {
  //   goToDeliveryCommunications();
  // }

  var title = 'Delete Selected Communications(s)?';
  var message = 'Selected Communications(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/events/deleteCommunications';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element);

}