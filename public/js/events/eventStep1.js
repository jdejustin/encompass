function initEventStep1() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	//call global function to initialize data injection
	initDataInjection('/events/getCurrentEvent', injectDataToStep1CallBack, setListenersCallback);
}

function injectDataToStep1CallBack(currentEvent)
{
	//inject the data for the confirmed date
	if (currentEvent.confirmedDate)
	{
		var existConfirmedDate = false;
		if ( $(".js-eventDatePref1").val() ==currentEvent.confirmedDate)
		{
			//check the checkbox
			$(".js-eventDatePref1").next().prop('checked',true);
			existConfirmedDate = true;
		}
		else if ( $(".js-eventDatePref2").val() ==currentEvent.confirmedDate)
		{
		//check the checkbox
			$(".js-eventDatePref2").next().prop('checked',true);
			existConfirmedDate = true;
		}
		else if( $(".js-eventDatePref3").val() ==currentEvent.confirmedDate)
		{
			//check the checkbox
			$(".js-eventDatePref3").next().prop('checked',true);
			existConfirmedDate = true;
		}


		if (existConfirmedDate)
		{
			//disable the dates 
			$(".js-eventDatePref1").prop('disabled',true);

			$(".js-eventDatePref2").prop('disabled',true);

			$(".js-eventDatePref3").prop('disabled',true);
		}
	}

	if (currentEvent.sendRegistration && currentEvent.sendRegistration == true)
	{
		$('#YesSendRegistration').prop('checked', true);
	} else {
		$('#NoSendRegistration').prop('checked', true);
	}

	if (currentEvent.crgProgramManager)
	{
		$('#crgProgramManager').val(currentEvent.crgProgramManager);
	}

	setRepEmailPhoneNumber();

	setListenersCallback();
}

function setRepEmailPhoneNumber()
{ 
	var $selectedRepOption = $('[data-bind="requestingRepId"] option:selected');
	var $emailSelectedRepOption = $selectedRepOption.attr('data-repEmail');
	var $phoneSelectedRepOption = $selectedRepOption.attr('data-repPhone');
	if((typeof $emailSelectedRepOption != undefined) && ($emailSelectedRepOption!=""))
	{
		$('[data-bind="requestingRepEmail"]').html($emailSelectedRepOption);

		// save rep name to DB
		var data = 'requestingRepName=' + $selectedRepOption.text();
		saveEventModel(data);
	}
	else
	{
		$('[data-bind="requestingRepEmail"]').html("No info available");
	}

	if((typeof $phoneSelectedRepOption != undefined)&& ($phoneSelectedRepOption!=""))
	{
		$('[data-bind="requestingRepPhone"]').html($phoneSelectedRepOption);

		// save rep name to DB
		var data = 'requestingRepName=' + $selectedRepOption.text();
		saveEventModel(data);
	}
	else
	{
		$('[data-bind="requestingRepPhone"]').html("No info available");
	}
}

function setListenersCallback()
{
	$('.js-percentage-field').autoNumeric('init', {aPad: false, aSign: '%', pSign: 's', aSep: '', vMin: 0, vMax: 100});

	//console.log("settingListeners");
	//listener for normal properties
	$(".js-event-property").focusout(function (){saveEventProperty(this);});
	$(".js-event-program-property").focusout(function (){resetEventTypeOptions(this);});
	$(".js-percentage-property").focusout(function (){saveCRGPercentageProperty(this);});
	
	//listener for click on the checkbox 
	$('.js-date-pref-chkbox1').click(function(){
		datePrefCheckboxChecked(this);
	});

	$('.js-date-pref-chkbox2').click(function(){
		datePrefCheckboxChecked(this);
	});
	
	$('.js-date-pref-chkbox3').click(function(){
		datePrefCheckboxChecked(this);
	});

	$('.js-sendRegistrationRadio').click(function(){
		sendRegistrationChanged(this);
	});

	//listener for repChange 
	$('.js-rep-select').change(function(){setRepEmailPhoneNumber();});

	//start all the listeners
	initEventStep1FieldListeners();

	var maxDate = $(".js-eventDatePref2").val();
	$(".js-eventDatePref1").datepicker({
      changeMonth: true,
      changeYear: true,
      maxDate:maxDate,
      onSelect: onEventDatePref1Selected
    });

	if ($(".js-eventDatePref1").val()!="")
	{
		var minDate = $(".js-eventDatePref1").val();
		var maxDate = $(".js-eventDatePref3").val();
		$(".js-eventDatePref2").datepicker({
	      changeMonth: true,
	      changeYear: true,
	      minDate:minDate,
	      maxDate:maxDate,
	      onSelect: onEventDatePref2Selected
	    });		
	}

	if ($(".js-eventDatePref2").val()!="")
	{
		var minDate = $(".js-eventDatePref2").val();
		$(".js-eventDatePref3").datepicker({
	      changeMonth: true,
	      changeYear: true,
	      minDate:minDate,
	      onSelect: onEventDatePref3Selected
	    });		
	}


   $('.js-eventStartTime').timepicker();
	$('.js-eventStartTime').on('changeTime', function (){
		$("input[name='eventStartTime']").validationEngine('hide');
		//$("input[name='eventStartTime']").css({'border':'1px solid #FF0000'});
		saveEventStartTime(this);
	});

	var eventRequestMode = $('[data-eventRequestMode]').attr('data-eventRequestMode');

	if (eventRequestMode == "true")
	{
		//disable all elements if the program or eventType dont have value ONLY in event request
		checkProgramIdAndEventId();
		$('[name="programId"]').change(function(){checkProgramIdAndEventId();});
		$('[name="eventType"]').change(function(){checkProgramIdAndEventId();});
	} else {
		checkEventId();
		$('[name="eventType"]').change(function(){checkEventId();});
	}	

	//start the validation engine 
	$('#EventStep1ValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});
};


function resetEventTypeOptions(target)
{
	data = {
			selectedProgramId: $(target).val()
			};
	
	jQuery.ajax({
		url:'/events/getEventTypesForSelectedProgram',
		datatype:'json',
		type: 'POST',
		async: false,
		data: data,
		success:function(serverData){
	 		//clear selection box first
	 		$('[name="eventType"]').empty();
	 		$('[name="eventType"]').append('<option disabled selected>Please Select One</option>');
	 		
			var programEventTypes = serverData.eventTypes;
			var i;
			for (i = 0; i < programEventTypes.length; i++) {
			 	//now add new options based on program
		        $('[name="eventType"]').append('<option>' + programEventTypes[i].type + '</option>');
			};
		}
	});
}

function sendRegistrationChanged(target)
{
	var value = $(target).val() == 'Yes'? true: false;
	var data =  "sendRegistration=" + value;
	saveEventModel(data);
}



function datePrefCheckboxChecked(checkboxElement)
{
	if ($(checkboxElement).is(':checked')) {
		// the checkbox was checked 
		//console.log('checked');
		$('.js-date-pref-chkbox1').prop('checked', false);
		$('.js-date-pref-chkbox2').prop('checked', false);
		$('.js-date-pref-chkbox3').prop('checked', false);
		/*        $('.js-date-pref-chkbox').each(function (){
		//uncheck all the check elements 
		$(this).prop('checked', false);
		});*/
		//check only the check Element 
		$(checkboxElement).prop("checked",true);
		//save the date corresponding to the confirmed date
		var elementToSaveValue = $(checkboxElement).prev().val();

		//console.log( elementToSaveValue)
		var data =  "confirmedDate=" + elementToSaveValue;
		saveEventModel(data);
		
		//disable the dates 
		$(".js-eventDatePref1").prop('disabled',true);
		$(".js-eventDatePref1").addClass('Disable');

		$(".js-eventDatePref2").prop('disabled',true);
		$(".js-eventDatePref2").addClass('Disable');

		$(".js-eventDatePref3").prop('disabled',true);
		$(".js-eventDatePref3").addClass('Disable');

		/*$(".js-eventDatePref").each(function (){
			$(this).prop('disabled',true);
			$(this).addClass('Disable');
		});*/
    } 
    else
    {
    	console.log("checkbox not check enable everything");
    	$(".js-eventDatePref1").prop('disabled',false);
		$(".js-eventDatePref1").removeClass('Disable');

		$(".js-eventDatePref2").prop('disabled',false);
		$(".js-eventDatePref2").removeClass('Disable');

		$(".js-eventDatePref3").prop('disabled',false);
		$(".js-eventDatePref3").removeClass('Disable');
		/*
		$(".js-eventDatePref").each(function (){
			$(this).prop('disabled',false);
			$(this).removeClass('Disable');
		});*/
    }
}

function deleteElement(targetProperty,indexToDelete)
{
	data = {
			targetIndex: indexToDelete, targetProperty: targetProperty,
			};
	
	jQuery.ajax({
		url:'/events/deleteEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
	});
}

function initEventStep1FieldListeners()
{
	$(".js-event-service-element").focusout(function (){saveToEvent(this,"services","Service");});

	//get the clone container in this case for the service clone block 
	var $cloneContainer = $('[data-clone="services"]');
	bindTheLastRowChangeByCloneContainer($cloneContainer);
}

function initEventStep1FieldChangeListeners(addButtonElement)
{
	console.log("initEventStep1FieldChangeListeners");
	var $cloneContainer = $(addButtonElement).parents('.js-add-button-container').find('[data-clone]')
	bindTheLastRowChangeByCloneContainer($cloneContainer);
}


function bindTheLastRowChangeByCloneContainer($cloneContainer)
{
	//unbind the old elements
	$cloneContainer.find(".js-event-service-element").unbind('input',function (){changeServiceField(this);});
	//bind only for the last child of the rows 
	var $jsRowLastChild = $cloneContainer.find(".js-row:last-child");
	$jsRowLastChild.find(".js-event-service-element").each(function(){
		console.log('bind:'+this.name);
		$(this).bind('input',function(){changeServiceField(this);});
	});
}




function changeServiceField(serviceField)
{
	//enable the add button
	$addButton = $(serviceField).parents('.js-add-button-container').find('.js-add-button');
	$addButton.prop('disabled',false);
	$addButton.removeClass('Disable');
}

function changeEventType(eventTypeField)
{
	//enable the add button
	console.log(" event Type "+$(eventTypeField).prop('value'));
}



function onEventDatePref1Selected(dateText, target)
{
	$(".js-eventDatePref2").prop('disabled',false);

	var maxDate = $(".js-eventDatePref3" ).val();
	$(".js-eventDatePref2" ).datepicker( "destroy" );
	$(".js-eventDatePref2").datepicker({
      changeMonth: true,
      changeYear: true,
      minDate:dateText,
      maxDate:maxDate,
      onSelect: onEventDatePref2Selected
    });

	// hide the message
	$("input[name='eventDatePref1']").validationEngine('hide');
	//$("input[name='eventDatePref1']").css({'border':'1px solid #FF0000'});
	//console.log($(target).prop('input').attr("name"));
	var validation = $(target).validationEngine('validate');
	if (!validation)
	{
		var serializeData =  $(target).prop('input').attr("name")+'='+dateText;
		saveEventModel(serializeData);
	}
}

function onEventDatePref2Selected(dateText, target)
{
	$(".js-eventDatePref3").prop('disabled',false);

	$(".js-eventDatePref1" ).datepicker( "destroy" );
	$(".js-eventDatePref1").datepicker({
      changeMonth: true,
      changeYear: true,
      maxDate:dateText,
      onSelect: onEventDatePref1Selected
    });


	$(".js-eventDatePref3" ).datepicker( "destroy" );
	$(".js-eventDatePref3").datepicker({
      changeMonth: true,
      changeYear: true,
      minDate:dateText,
      onSelect: onEventDatePref3Selected
    });

	var validation = $(target).validationEngine('validate');
	if (!validation)
	{
		var serializeData =  $(target).prop('input').attr("name")+'='+dateText;
		saveEventModel(serializeData);
	}
}

function onEventDatePref3Selected(dateText, target)
{
	var minDate = 	$(".js-eventDatePref1").val();
	$(".js-eventDatePref2" ).datepicker( "destroy" );
	$(".js-eventDatePref2").datepicker({
      changeMonth: true,
      changeYear: true,
      minDate:minDate,
      maxDate:dateText,
      onSelect: onEventDatePref2Selected
    });

	// hide the message
	$("input[name='eventDatePref1']").validationEngine('hide');
	//$("input[name='eventDatePref1']").css({'border':'1px solid #FF0000'});
	//console.log($(target).prop('input').attr("name"));
	var validation = $(target).validationEngine('validate');
	if (!validation)
	{
		var serializeData =  $(target).prop('input').attr("name")+'='+dateText;
		saveEventModel(serializeData);
	}
}


function saveEventStartTime(target)
{
	var validation = $(target).validationEngine('validate');
	if (!validation)
	{
		//start time
		var sTime = $(target).val();
		var startTime =  (isNaN(sTime)? sTime : 0);

		var serializedData = 'eventStartTime=' + startTime;
		saveEventModel(serializedData);
	}
}


function saveCRGPercentageProperty(target)
{
	var serializedData = 'crgPercentageFee=' + $(target).autoNumeric('get');
	saveEventModel(serializedData);
}

function checkProgramIdAndEventId()
{
	var programValue = $('[name="programId"]').val();
	var eventTypeValue = $('[name="eventType"]').val();
	//console.log('programValue '+ programValue);	
	//console.log('eventTypeValue '+ eventTypeValue);	
	if  ( ( programValue != null && programValue != selectPrompt) && ( eventTypeValue != null && eventTypeValue != selectPrompt) )
	{
		//enable elements console.log("enable elements");
		$('[name="brand"]').prop('disabled',false);
		$('[name="presentationTitle"]').prop('disabled',false);
		$('[name="eventStartTime"]').prop('disabled',false);
		$('[name="timezone"]').prop('disabled',false);
		$('[name="eventDatePref1"]').prop('disabled',false);
		
		if ($('[name="eventDatePref1"]').val()!="")
		{
			$('[name="eventDatePref2"]').prop('disabled',false);
		}
		else
		{
			$('[name="eventDatePref2"]').prop('disabled',true);
		}
		
		if ($('[name="eventDatePref2"]').val()!="")
		{
			$('[name="eventDatePref3"]').prop('disabled',false);
		}
		else
		{
			$('[name="eventDatePref3"]').prop('disabled',true);
		}

		$('[name="dateLocationComments"]').prop('disabled',false);
		$('.js-next').prop('disabled',false);

		//check for webcast
		if(eventTypeValue.toLowerCase() == "webinar only")
		{
	  		$('[name="city"]').prop('disabled',true);
	  		$("#asteriskCity").addClass('hidden');
			$('[name="state"]').prop('disabled',true);
	  		$("#asteriskState").addClass('hidden');
		}
		else
		{
	  		$('[name="city"]').prop('disabled',false);
	  		$("#asteriskCity").removeClass('hidden');
			$('[name="state"]').prop('disabled',false);
	  		$("#asteriskState").removeClass('hidden');
		}
	}
	else
	{
		//disableElements console.log("disable elements");
		$('[name="brand"]').prop('disabled',true);
		$('[name="presentationTitle"]').prop('disabled',true);
		$('[name="city"]').prop('disabled',true);
		$('[name="state"]').prop('disabled',true);
		$('[name="eventStartTime"]').prop('disabled',true);
		$('[name="timezone"]').prop('disabled',true);
		$('[name="eventDatePref1"]').prop('disabled',true);
		$('[name="eventDatePref2"]').prop('disabled',true);
		$('[name="eventDatePref3"]').prop('disabled',true);
		$('[name="dateLocationComments"]').prop('disabled',true);
		$('.js-next').prop('disabled',true);
	}
}

function checkEventId()
{
	//check for event type for CRG admin event creation (programType is set already)
	var eventTypeValue = $('[name="eventType"]').val();
	if  ( eventTypeValue != null && eventTypeValue != selectPrompt )
	{
		$('[name="brand"]').prop('disabled',false);
		$('[name="presentationTitle"]').prop('disabled',false);
		$('[name="jobNumber"]').prop('disabled',false);
		$('[name="status"]').prop('disabled',false);
		$('[name="crgPercentageFee"]').prop('disabled',false);
		$('[name="requestingRepId"]').prop('disabled',false);
		$('[name="serviceName"]').prop('disabled',false);
		$('[name="percentage"]').prop('disabled',false);
		$('[name="numberHours"]').prop('disabled',false);
		$('[name="eventStartTime"]').prop('disabled',false);
		$('[name="timezone"]').prop('disabled',false);
		$('[name="eventDatePref1"]').prop('disabled',false);
		if ($('[name="eventDatePref1"]').val()!="")
		{
			$('[name="eventDatePref2"]').prop('disabled',false);
		}
		else
		{
			$('[name="eventDatePref2"]').prop('disabled',true);
		}
		
		if ($('[name="eventDatePref2"]').val()!="")
		{
			$('[name="eventDatePref3"]').prop('disabled',false);
		}
		else
		{
			$('[name="eventDatePref3"]').prop('disabled',true);
		}

		$('[name="sendRegistrationRadios"]').prop('disabled',false);

		$('[name="dateLocationComments"]').prop('disabled',false);
		$('.js-next').prop('disabled',false);
		//check for webcast
		if(eventTypeValue.toLowerCase() == "webinar only")
		{
	  		$('[name="city"]').prop('disabled',true);
	  		$("#asteriskCity").addClass('hidden');
			$('[name="state"]').prop('disabled',true);
	  		$("#asteriskState").addClass('hidden');
		}
		else
		{
	  		$('[name="city"]').prop('disabled',false);
	  		$("#asteriskCity").removeClass('hidden');
			$('[name="state"]').prop('disabled',false);
	  		$("#asteriskState").removeClass('hidden');
		}
	}
	else
	{
		//disableElements console.log("disable elements");
		$('[name="brand"]').prop('disabled',true);
		$('[name="presentationTitle"]').prop('disabled',true);
		$('[name="jobNumber"]').prop('disabled',true);
		$('[name="status"]').prop('disabled',true);
		$('[name="crgPercentageFee"]').prop('disabled',true);
		$('[name="requestingRepId"]').prop('disabled',true);
		$('[name="serviceName"]').prop('disabled',true);
		$('[name="percentage"]').prop('disabled',true);
		$('[name="numberHours"]').prop('disabled',true);
		$('[name="city"]').prop('disabled',true);
		$('[name="state"]').prop('disabled',true);
		$('[name="eventStartTime"]').prop('disabled',true);
		$('[name="timezone"]').prop('disabled',true);
		$('[name="eventDatePref1"]').prop('disabled',true);
		$('[name="eventDatePref2"]').prop('disabled',true);
		$('[name="eventDatePref3"]').prop('disabled',true);
		$('[name="sendRegistrationRadios"]').prop('disabled',true);
		$('[name="dateLocationComments"]').prop('disabled',true);
		$('.js-next').prop('disabled',true);
	}
}


function validateEventStep1Event(functionToCall)
{
	var validation = $('#EventStep1ValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}
