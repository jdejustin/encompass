function initEventStep2() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');

	initGlobal();
	//console.log('init event step2');
	//inject data to the listeners
	initDataInjection('/events/getCurrentEvent', injectDataToStep2CallBack, finalizeStep2Init);
}

function injectDataToStep2CallBack(currentEvent)
{
	//Do something here! to inject elements in diferent way than the data bind property
	var $elementsToSetValues = jQuery("[data-prefNumber]").parents('.js-speaker-container');

	var speakersArray = currentEvent.potentialSpeakers;

	if (speakersArray && speakersArray.length > 0)
	{
		//remove required message
		$('#speakerRequiredMessage').hide();
	}

 	$(speakersArray).each(function (index) {
		var htmlIndexToSearch = (index+1).toString();
		var speaker = this;

		console.log('htmlIndexToSearch:' + htmlIndexToSearch);

		var $speakerContainer = $('[data-prefNumber="'+htmlIndexToSearch+'"]').parents('.js-speaker-container');

		var $dataBindElements = $speakerContainer.find("[data-bind]");

		$dataBindElements.each(function (){
			var $this = $(this);
			//get the name of the data-bind 
			var bindElementName = $this.attr("data-bind");
			var objectValue="";
			try
			{
			  //eval the 
			  objectValue = eval("speaker."+bindElementName);   
			}
			catch (err)
			{
			  objectValue="";
			}

			// console.log("bind element: " + bindElementName);
			// console.log("objectValue: " +  objectValue);

			setElementValue ($this, objectValue);
		});


		//set the value for speaker name 
		// var elementValue = '';
		// var $nameOptionsElements = $speakerContainer.find('[name="contactId"] option');
		// try{
		// 	//eval the 
		// 	elementValue = eval("speaker.contactId");   
		// }
		// catch (err){
		// 	elementValue="";
		// }
		// $nameOptionsElements.each(function(){
		// 	if ($(this).val() == elementValue)
		// 	{
		// 		$(this).prop('selected', true)
		// 		return false; //exit each
		// 	}
		// });


      	//set the value for travelType 
		var travelValue = '';
		try{
			//eval the 
			travelValue = eval("speaker.travelType");   
		}
		catch (err){
			travelValue="";
		}
		$speakerContainer.find('.js-travel-radio-button').each(function(){
			//console.log('checking travelType '+ $(this).val() + ' '+travelValue);
			if ($(this).val() == travelValue)
			{
				$(this).prop('checked', true)
				return false; //exit each
			}
		});	

      	// set the value for ground trasportations
		var transportValue = '';
		try{
			//eval the 
			transportValue = eval("speaker.groundTransportation");   
		}
		catch (err){
			transportValue="";
		}
		$speakerContainer.find('.js-transportation-radio-button').each(function(){
			if ($(this).val() == transportValue)
			{
				$(this).prop('checked', true)
				return false; //exit each
			}
		});



      	//set the selected speaker
		$speakerContainer.find('[data-bind="speakerCity"]').html(eval('speaker.city'));
		$speakerContainer.find('[data-bind="speakerState"]').html(eval('speaker.state'));

		var optionValues = [];

		$('#fieldForceSpeakerSelectedStatus option').each(function() {
		    optionValues.push($(this).text());
		});

		optionValues.shift();
		var statusLabel = optionValues[eval('speaker.status')];

		$speakerContainer.find('[data-bind="speakerStatus"]').html(statusLabel);

		//var index = targetIndexToSave + 1;
		$('#speakerRequiredMessage' + htmlIndexToSearch).removeClass('hidden');
		$('#speakerStatusRequired' + htmlIndexToSearch).removeClass('hidden');
  	});


	//check program to set the radio buttons 
	var programType = $('[data-event-type]').attr('data-event-type');
	if(programType && (programType.toLowerCase() == 'webinar only') || (programType.toLowerCase() == 'webinar with catering') ) 
	{
		//set NA 
		$('.js-na').each(function (){
			$(this).next().html('N/A because of '+$('[data-event-type]').attr('data-event-type'));
			$(this).prop('checked', true);
		});

		//disable Radio Buttons
		$('input:radio').each(function (){
			$(this).prop('disabled',true);
		});	

		$('.travel_type_required').addClass('hidden');
		$('.transportation_required').addClass('hidden');
	}

	finalizeStep2Init();
}

function finalizeStep2Init()
{
	console.log('setting listeners');
	//add listeners, etc
	//listener for normal properties
	$("#EventStep2ValidateForm .js-event-property").focusout(function (){
		saveSpeakerElement(this);
	});	

	// $(".js-event-double-property").focusout(function (){
	// 	saveSpeakerNameAndContact(this);
	// });	

	$("#EventStep2ValidateForm .js-travel-radio-button").click(function (){
		saveSpeakerElement(this);
	});

	$("#EventStep2ValidateForm .js-transportation-radio-button").click(function (){
		saveSpeakerElement(this);
	});

	$("#EventStep2ValidateForm .js-speaker-status").change(function (){
		saveSpeakerStatus(this);
	});

	//listener for speaker change 
	//init function to call when search is completed
	initClickOnTableRowEventProgramSpeakers();

	//start the validation engine 
	$('#EventStep2ValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});

}

function initClickOnTableRowEventProgramSpeakers()
{
    $('.mainTable tbody td').click(function () {
    	//highlight selected row
		$(this).parent('tr').addClass("selected").siblings().removeClass("selected");
      	setSpeakerDataOnView(this);
    });
}

function saveSpeakerElement(target)
{
	var validation = $(target).validationEngine('validate');
	if($(target).val() && $(target).val() != selectPrompt && !validation)
	{
		//get the index of the element to save
		var targetIndexToSave = $(target).parents('.js-speaker-container').find('[data-prefNumber]').attr('data-prefNumber');
		targetIndexToSave--; //bacause the save is base 0 array 

		var elementName = $(target).attr("data-bind");
		var elementValue = $(target).val();

		// console.log ('saving speaker # '+ targetIndexToSave+ " name "+ elementName + " value " + elementValue);

		var data = {
			value: [elementName + "=" + elementValue],
			targetIndex: targetIndexToSave,
			targetProperty: "potentialSpeakers",
			modelName: "Speaker"
		};
		    
		saveEventEmbeddedModel(data);

		if (targetIndexToSave > 0)
		{
			var index = targetIndexToSave + 1;
			$('#speakerRequiredMessage' + index).removeClass('hidden');
			$('#speakerStatusRequired' + index).removeClass('hidden');
		}
	}
}

function saveSpeakerStatus(target)
{
	var validation = $(target).validationEngine('validate');
	if($(target).val() && $(target).val() != selectPrompt && !validation)
	{
		//get the index of the element to save
		var targetIndexToSave = $(target).parents('.js-speaker-container').find('[data-prefNumber]').attr('data-prefNumber');
		targetIndexToSave--; //bacause the save is base 0 array 

		var elementName = $(target).attr('name');//"status";
		var elementValue = $(target).val();

		// console.log ('saving speaker # '+ targetIndexToSave+ " name "+ elementName + " value " + elementValue);

		var data = {
			value: [elementName + "=" + elementValue],
			targetIndex: targetIndexToSave,
			targetProperty: "potentialSpeakers",
			modelName: "Speaker"
		};
		    
		saveEventEmbeddedModel(data);

		if (targetIndexToSave > 0)
		{
			var index = targetIndexToSave + 1;
			$('#speakerRequiredMessage' + index).removeClass('hidden');
			$('#speakerStatusRequired' + index).removeClass('hidden');
		}
	}
}

function saveSpeakerData(target, contactId, contactName, contactCity, contactState)
{
	// var validation = $(target).validationEngine('validate');
	// if($(target).val() && $(target).val() != selectPrompt && !validation)
	// {
		//get the index of the element to save
		var targetIndexToSave = $(target).parents('.js-speaker-container').find('[data-prefNumber]').attr('data-prefNumber');
		targetIndexToSave--; //bacause the save is base 0 array 

		//var contactIdName = $(target).attr("name");
		// var contactId = $(target).val();
		// var contactName = $(target).find('option:selected').html();

		var data = {
			value: ['contactId=' + contactId,"speakerName=" + contactName, "city=" + contactCity, "state=" + contactState],
			targetIndex: targetIndexToSave,
			targetProperty: "potentialSpeakers",
			modelName: "Speaker"
		};
		    
		saveEventEmbeddedModel(data);

		if (targetIndexToSave > 0)
		{
			var index = targetIndexToSave + 1;
			$('#speakerRequiredMessage' + index).removeClass('hidden');
			$('#speakerStatusRequired' + index).removeClass('hidden');
			$('#speakerTravelRequired' + index).removeClass('hidden');
			$('#speakerTransportationRequired' + index).removeClass('hidden');
			$('#status' + index).addClass('validate[condRequired[speakerNameHidden' + index + ']]');
			$('#speakerNameHidden' + index).val(contactName);
		}
	// }
}


function setSpeakerDataOnView(selectElement)
{ 
	var contactId = $(selectElement).parent('tr').attr("data-bind");
	var contactFirstName = $(selectElement).parent().find('.js-firstName').html().trim();
	var contactLastName = $(selectElement).parent().find('.js-lastName').html().trim();
	var contactDegrees = $(selectElement).parent().find('.js-degrees').html().trim();
	// var contactAffiliations = $(selectElement).parent().find('.js-affiliations').html().trim();
	// var contactSpecialties = $(selectElement).parent().find('.js-specialties').html().trim();
	var contactCity = $(selectElement).parent().find('.js-city').html().trim();
	var contactState = $(selectElement).parent().find('.js-state').html().trim();


	var finalNameString = contactFirstName + ' ' + contactLastName;

	if (contactDegrees.length > 0)
	{
		var re = /,/g;

		finalNameString += ', ' + contactDegrees.replace(re, ", ");
	}

	$(selectElement).parents('.js-speaker-container').find('[data-bind="speakerName"]').html(finalNameString);
	//$('[data-bind="speakerAffiliation"]').html(contactAffiliations);
	//$('[data-bind="speakerSpecialty"]').html(contactSpecialties);
	$(selectElement).parents('.js-speaker-container').find('[data-bind="speakerCity"]').html(contactCity);
	$(selectElement).parents('.js-speaker-container').find('[data-bind="speakerState"]').html(contactState);

	saveSpeakerData(selectElement, contactId, finalNameString, contactCity, contactState);

	// var city = $selectedSpkOption.attr('data-spkCity');
	// var state = $selectedSpkOption.attr('data-spkState');
	// if((typeof $citySelectedSpkOption != undefined) && ($citySelectedSpkOption!=""))
	// {
	// 	$('[data-bind="speakerCity"]').html($citySelectedSpkOption);
	// }
	// else
	// {
	// 	$('[data-bind="speakerCity"]').html("No info available");
	// }

	// if ((typeof $stateSelectedSpkOption != undefined)&& ($stateSelectedSpkOption!=""))
	// {
	// 	$('[data-bind="speakerState"]').html($stateSelectedSpkOption);
	// }
	// else
	// {
	// 	$('[data-bind="speakerState"]').html("No info available");
	// }
}

function validateEventStep2(functionToCall)
{
	var validation = $('#EventStep2ValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}