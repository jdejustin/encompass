function initEventStep3() {
	//$('html, body').animate({ scrollTop: 0 }, 'slow');

	$('.js-number-only').autoNumeric('init', {aPad: false, aSep: ''});
	$('.js-monetary-value').autoNumeric('init', {aSign:'$'});

	$(".PhoneNumber").intlTelInput();

	initDataInjection('/events/getCurrentEvent', injectDataToStep3CallBack, finalizeStep3Init);
}

function injectDataToStep3CallBack(currentEvent)
{
	//Do something here!
	//check program type to hide some elements
	var requireVenueInfo = $("[data-requireVenueInfo]").attr('data-requireVenueInfo');
	
	if (requireVenueInfo != 'Yes Require' && (currentEvent.eventType.toLowerCase() == 'webinar only' || currentEvent.eventType.toLowerCase() == 'webinar with catering'))
	{
		//hide de venue section 
		$('.js-venue-component').addClass('hidden');
		//hide the caterer section 
		$('.js-caterer-component').addClass('hidden');
		//show the webcast message box
		$('.js-webcast-component').removeClass('hidden');
		//Disable the elements of the 
		$('.js-webcast-non-show-element').each(function (){
			//$(this).addClass('hidden');
			$(this).prop('disabled',true);
		});
	}

	//are we in request mode
	var eventRequestMode = $("[data-eventRequestMode]").attr('data-eventRequestMode');
	if (eventRequestMode == 'true')
	{
		//venue data injection 
		var potentialVenueObject=null;
		//inject data to the view for the actual venue 
		try
		{
			// var potentialVenue =$("[data-potentialVenue]").attr('data-potentialVenue');
			potentialVenueObject =  currentEvent.potentialVenue[0];//JSON.parse(potentialVenue);
			var potentialVenueStatus = $("[data-currentVenueStatus]").attr('data-currentVenueStatus');
		}
		catch (err)
		{}

		//check if currentVenueObject
		if(potentialVenueObject)
		{
			$("#repProvidedVenueData").show();
			$("#repProvidedVenueDataNew").show();

			setVenueValues(potentialVenueObject,false,settingAutoCompleteCurrentVenueProperty, potentialVenueStatus, "", true);//don't save
		}

		//caterer data injection 
		var potentialCatererObject=null;
		//inject data to the view for the actual caterer 
		try
		{
			// var potentialCaterer = $("[data-potentialCaterer]").attr('data-potentialCaterer');
			potentialCatererObject = currentEvent.potentialCaterer[0];//JSON.parse(potentialCaterer);
			var potentialCatererStatus = $("[data-currentCatererStatus]").attr('data-currentCatererStatus');
		}
		catch (err)
		{}

		
		//check if currentCatererObject
		if(potentialCatererObject)
		{
			$("#repProvidedCatererData").show();
			$("#repProvidedCatererDataNew").show();

			setCatererValues(potentialCatererObject,false,settingAutoCompleteCurrentCatererProperty, potentialCatererStatus, "", true); //don't save
		}
	} else {
		//venue data injection 
		var currentVenueObject=null;
		//inject data to the view for the actual venue 
		try
		{
			var currentVenue = $("[data-currentVenue]").attr('data-currentVenue');
			currentVenueObject = JSON.parse(currentVenue);
			var currentVenueStatus = $("[data-currentVenueCRGStatus]").attr('data-currentVenueCRGStatus');

			// var venueName = "";
			// var venueName = currentVenueObject.venueName;
			//$('#autoCompleteVenueName').val(venueName); 
		}
		catch (err)
		{}

		//check if currentVenueObject
		if(currentVenueObject)
		{
			setVenueValues(currentVenueObject,false,settingAutoCompleteCurrentVenueProperty, currentVenueStatus, "Official", false);//don't save
		}

		//caterer data injection 
		var currentCatererObject=null;
		//inject data to the view for the actual caterer 
		try
		{
			var currentCaterer = $("[data-currentCaterer]").attr('data-currentCaterer');
			currentCatererObject = JSON.parse(currentCaterer);
			var currentCatererStatus = $("[data-currentCatererCRGStatus]").attr('data-currentCatererCRGStatus');

			// var catererName = "";
			// var catererName = currentCatererObject.venueName;
			//$('#autoCompleteCatererName').val(catererName); 
		}
		catch (err)
		{}

		

		//check if currentCatererObject
		if(currentCatererObject)
		{
			setCatererValues(currentCatererObject,false,settingAutoCompleteCurrentCatererProperty, currentCatererStatus, "Official", false); //don't save
		}


		//Display rep provided data
		var potentialVenueObject=null;
		//inject data to the view for the actual venue 
		try
		{
			var potentialVenue = $("[data-potentialVenue]").attr('data-potentialVenue');
			potentialVenueObject = JSON.parse(potentialVenue);
			var potentialVenueStatus = $("[data-currentVenueStatus]").attr('data-currentVenueStatus');
		}
		catch (err)
		{}

		//check if currentVenueObject
		if(potentialVenueObject)
		{
			$("#repProvidedVenueData").show();
			$("#repProvidedVenueDataNew").show();

			setVenueValues(potentialVenueObject,false,settingAutoCompleteCurrentVenueProperty, potentialVenueStatus, "RepProvided", false);//don't save
		}

		//caterer data injection 
		var potentialCatererObject=null;
		//inject data to the view for the actual caterer 
		try
		{
			var potentialCaterer = $("[data-potentialCaterer]").attr('data-potentialCaterer');
			potentialCatererObject = JSON.parse(potentialCaterer);
			var potentialCatererStatus = $("[data-currentCatererStatus]").attr('data-currentCatererStatus');
		}
		catch (err)
		{}

		
		//check if currentCatererObject
		if(potentialCatererObject)
		{
			$("#repProvidedCatererData").show();
			$("#repProvidedCatererDataNew").show();

			setCatererValues(potentialCatererObject,false,settingAutoCompleteCurrentCatererProperty, potentialCatererStatus, "RepProvided", false); //don't save
		}
	}

	finalizeStep3Init();
}

function finalizeStep3Init()
{
	var eventRequestMode = $("[data-eventRequestMode]").attr('data-eventRequestMode');
	if (eventRequestMode != 'true')
	{
		//add listeners, etc
		$("#eventStep3ValidateForm .js-event-property").focusout(function (){
			saveEventProperty(this);
		});

		$(".js-event-venue-property").focusout(function (){
			var elementValue = $(this).val();
			var elementName = $(this).attr('name');

			saveVenueProperty(this, elementValue, elementName, setVenueId);
		});


		$(".js-event-venue-address-property").focusout(function (){
			var elementValue = $(this).val();
			var elementName = $(this).attr('name');

			saveVenueEmbedded(this, elementValue, elementName, 'addresses', 'Address', setVenueId);
		});

		//save venue number, it is the same that venue contact number
		$(".js-event-venue-phone-number").focusout(function (){
			var elementValue = $(this).val();
			var elementName = $(this).attr('name');

			saveVenueEmbedded(this, elementValue, elementName, 'phoneNumbers', 'ContactPhoneNumber', setVenueId);
		});

	   $(".js-event-venue-contact-property").focusout(function (){
	   		var elementValue = $(this).val();
			var elementName = $(this).attr('name');

			saveVenueEmbedded(this, elementValue, elementName, 'contact', 'ContactInfo', setVenueId);
	   });

	   $(".js-event-caterer-property").focusout(function (){
	     	var elementValue = $(this).val();
			var elementName = $(this).attr('name');

			saveVenueProperty(this, elementValue, elementName, setCatererId);
	   });

	   $(".js-event-caterer-contact-property").focusout(function (){
	      	var elementValue = $(this).val();
			var elementName = $(this).attr('name');

			saveVenueEmbedded(this, elementValue, elementName, 'contact', 'ContactInfo', setCatererId);
	   });
	} else {
	   //////////////////////////////////
	   $("#eventStep3ValidateForm .js-event-property").focusout(function (){
			saveEventProperty(this);
		});
	   
	   $(".js-event-potential-venue-property").focusout(function (){
			saveEventProperty(this);
		});

		$(".js-event-potential-venue-property").focusout(function (){
			var elementValue = $(this).val();
			var elementName = $(this).attr('name');
			savePotentialVenueProperty(this, elementValue, elementName, 'Venue', 'potentialVenue');
		});


		$(".js-event-potential-venue-address-property").focusout(function (){
			var elementValue = $(this).val();
			var elementName = $(this).attr('name');
			savePotentialVenueEmbedded(this, elementValue, elementName, 'potentialVenue', 'addresses' , 'Venue', 'Address');
		});

		//save venue number, it is the same that venue contact number
		$(".js-event-potential-venue-phone-number").focusout(function (){
			var elementValue = $(this).val();
			var elementName = $(this).attr('name');
			savePotentialVenueEmbedded(this, elementValue, elementName, 'potentialVenue', 'phoneNumbers' , 'Venue', 'ContactPhoneNumber');
		});

	   $(".js-event-potential-venue-contact-property").focusout(function (){
	   		var elementValue = $(this).val();
			var elementName = $(this).attr('name');
			savePotentialVenueEmbedded(this, elementValue, elementName, 'potentialVenue', 'contact' , 'Venue', 'ContactInfo');
	   });

	   $(".js-event-potential-caterer-property").focusout(function (){
	     	var elementValue = $(this).val();
			var elementName = $(this).attr('name');
			savePotentialVenueProperty(this, elementValue, elementName, 'Venue', 'potentialCaterer');
	   });

	   $(".js-event-potential-caterer-contact-property").focusout(function (){
	      	var elementValue = $(this).val();
			var elementName = $(this).attr('name');
			savePotentialVenueEmbedded(this, elementValue, elementName, 'potentialCaterer', 'contact' , 'Venue', 'ContactInfo');
	   });

	 //   //scroll selected venue into view
	 //   	var row = $('#venuesTable').find('.selected').position();
		// $('#venuesTableWrapper').scrollTop(row.top - 150);

		// var row = $('#caterersTable').find('.selected').position();
		// $('#caterersTableWrapper').scrollTop(row.top - 150);
	}


   $('#venuesTable tbody td').click(function(){

		//highlight selected row
		$(this).parent('tr').addClass("selected").siblings().removeClass("selected");

		var venueData = $(this).parent('tr').attr('data-venue');
		setVenueValues(JSON.parse(venueData),true,settingAutoCompleteCurrentVenueProperty, null, 'Official'); 

	});

	// $('#venueSelect').change(function (){
	// 	//console.log("venue val "+$(this).val());
	// 	setVenueValues(JSON.parse($(this).val()),true,settingAutoCompleteCurrentVenueProperty, null, 'Official'); 
	// });

	// $('#catererSelect').change(function (){
	// 	//console.log("venue val "+$(this).val());
	// 	setCatererValues(JSON.parse($(this).val()),true,settingAutoCompleteCurrentVenueProperty, null, 'Official'); 
	// });

	$('#caterersTable tbody td').click(function(){

		//highlight selected row
		$(this).parent('tr').addClass("selected").siblings().removeClass("selected");

		var catererData = $(this).parent('tr').attr('data-caterer');
		setCatererValues(JSON.parse(catererData),true,settingAutoCompleteCurrentVenueProperty, null, 'Official'); 

	});


	$( "#venueTabs" ).tabs({
		activate: function( event, ui ) {
         console.log('ACTIVATING VENUE TABS');
			var active = $( "#venueTabs" ).tabs( "option", "active" );
			if(active == 1) //select new venue tab
			{
				//check the currentVenueId of the tab 
				var existingEditingVenueId = $('[data-newEditingTabCurrentVenueId]').attr('data-newEditingTabCurrentVenueId');
				if (existingEditingVenueId == '')
				{
					console.log('unsetCurrentVenue');		
					unsetCurrentVenue();
				}
				else 
				{
					//save the existing venue in the editing tab
					saveVenueIdToEvent(existingEditingVenueId,settingEditionCurrentVenueProperty);
				}

				//set caterer tab to first tab, to avoid having someone mixing values for venue and caterer
				$( "#catererTabs" ).tabs({ active: 0 });
			}
			else//select auto complete venue tab
			{
				var existingAutocompleteVenueId = $('[data-currentVenue]').attr('data-currentVenue');
				console.log("existingAutocompleteVenueId "+ existingAutocompleteVenueId);
				if (existingAutocompleteVenueId == '' || existingAutocompleteVenueId == null)
				{
					//save the existing venue to null
					console.log("save existing venue null");
					saveVenueIdToEvent(null,settingAutoCompleteCurrentVenueProperty);	
				}
				else
				{
					//save the existingVenue in the selection tab
					console.log("save existing venue:"+existingAutocompleteVenueId);
					saveVenueIdToEvent(existingAutocompleteVenueId,settingAutoCompleteCurrentVenueProperty);	
				}
			}
		}
	});

	$( "#catererTabs" ).tabs({
		activate: function( event, ui ) {
         console.log('ACTIVATING CATERER TABS');
			var active = $( "#catererTabs" ).tabs( "option", "active" );
			if(active == 1) //select edition or new caterer tab
			{
				//check the currentVenueId of the tab 
				var existingEditingCatererId = $('[data-newEditingTabCurrentCatererId]').attr('data-newEditingTabCurrentCatererId');
				if (existingEditingCatererId == '')
				{
					console.log('unsetCurrentCaterer');		
					unsetCurrentVenue();
				}
				else 
				{
					//save the existing catererId in the editing tab
					saveCatererIdToEvent(existingEditingCatererId,settingEditionCurrentCatererProperty);
				}

				//set venue tab to first tab, to avoid having someone mixing values for venue and caterer
				$( "#venueTabs" ).tabs({ active: 0 });
			}
			else//select auto complete caterer tab
			{
				var existingAutocompleteCatererId = $('[data-currentCaterer]').attr('data-currentCaterer');
				console.log("existingAutocompleteCatererId "+ existingAutocompleteCatererId);
				if (existingAutocompleteCatererId == '' || existingAutocompleteCatererId == null)
				{
					//save the existing venue to null
					console.log("save existing caterer null");
					saveCatererIdToEvent(null,settingAutoCompleteCurrentCatererProperty);	
				}
				else
				{
					//save the existingVenue in the selection tab
					console.log("save existing venue:"+existingAutocompleteCatererId);
					saveCatererIdToEvent(existingAutocompleteCatererId,settingAutoCompleteCurrentCatererProperty);	
				}
			}
		}
	});

	//start the validation engine 
	$('#eventStep3ValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});

	initGlobal();

}

function setEventVenueCallback(dataSaved)
{
	//create the data to save the event.
	saveVenueIdToEvent(dataSaved.venueId,settingEditionCurrentVenueProperty)
}

function setEventCatererCallback(dataSaved)
{
	//create the data to save the event.
	saveCatererIdToEvent(dataSaved.venueId,settingEditionCurrentCatererProperty)
}


function saveVenueIdToEvent(venueIdToSave,settingTabVenuePropertyCallback)
{
	console.log("saving venueid to event " + venueIdToSave);
	var data = {
		venueId: venueIdToSave
	};

	//save the venue id to the event
	jQuery.ajax({
		url:'/events/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			//console.log("saved new currentEvent venueId "+ venueId);
			//setting the value to the  current event 
			if(settingTabVenuePropertyCallback)
			{
				settingTabVenuePropertyCallback(venueIdToSave);
			}
		}
	});
}

function saveCatererIdToEvent(venueIdToSave,settingTabCatererPropertyCallback)
{
	console.log("saving catererId to event " + venueIdToSave);
	var data = {
		catererId: venueIdToSave
	};

	//save the venue id to the event
	jQuery.ajax({
		url:'/events/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			//console.log("saved new currentEvent venueId "+ venueId);
			//setting the value to the  current event 
			if(settingTabCatererPropertyCallback)
			{
				settingTabCatererPropertyCallback(venueIdToSave);
			}
		}
	});
}


//this function set the current venue to the property for editing venue option
function settingEditionCurrentVenueProperty(venueIdSaved)
{
	console.log('setting the venueIdSaved: '+venueIdSaved +' to the new-EditingTabCurrenVenueId property');
	$('[data-newEditingTabCurrentVenueId]').attr('data-newEditingTabCurrentVenueId',venueIdSaved);
}

//this function set the current venue to the property for editing venue option
function settingAutoCompleteCurrentVenueProperty(venueIdSaved)
{
	console.log('setting the venueIdSaved: '+venueIdSaved +' to the new-currentVenue property');
	$('[data-currentVenue]').attr('data-currentVenue',venueIdSaved);
}

//this function set the current caterer to the property for editing caterer option
function settingEditionCurrentCatererProperty(venueIdSaved)
{
	console.log('setting the catererIdSaved: '+venueIdSaved +' to the newEditingTabCurrentCatererId property');
	$('[data-newEditingTabCurrentCatererId]').attr('data-newEditingTabCurrentCatererId',venueIdSaved);
}

//this function set the current venue to the property for editing venue option
function settingAutoCompleteCurrentCatererProperty(venueIdSaved)
{
	console.log('setting the currentCatererIdSaved: '+venueIdSaved +' to the current caterer property');
	$('[data-currentCaterer]').attr('data-currentCaterer',venueIdSaved);
}


function unsetCurrentVenue()
{
	//save the venue id to the event
	jQuery.ajax({
		url:'/venues/unsetCurrentVenue',
		datatype:'json',
		type: 'POST',
		async: true,
		success:function(data){
			console.log("unsetCurrentVenue result came back");
		}
	});
}


function saveVenueProperty(saveInputElement, elementValue, elementName, callBack)
{
	//console.log("preSaveVenueProperty");
	var validation = $(saveInputElement).validationEngine('validate');
	if(!validation)
	{
		var data = $(saveInputElement).serialize();
		    
		saveVenueModel(data, callBack);
	}
}

function setVenueId(data)
{
	saveVenueIdToEvent(data.venueId);
}

function setCatererId(data)
{
	saveCatererIdToEvent(data.venueId);
}

function saveVenueEmbedded(saveInputElement, elementValue, elementName, targetProperty, modelName, callBack)
{
	var validation = $(saveInputElement).validationEngine('validate');
	if (!validation)
	{
		var data = {
			value: [elementName + "=" + elementValue],
			targetIndex: 0,
			targetProperty: targetProperty,
			modelName: modelName
		};
		    
		saveVenueEmbeddedModel(data, callBack);
	}	
}


function saveVenueModel(data,setEventVenueCallback)
{
	jQuery.ajax({
		url:'/venues/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("save venue model "+ JSON.stringify(data));
			if (setEventVenueCallback)
			{
				setEventVenueCallback(data);
			}
		}
	});
}


function saveVenueEmbeddedModel(data, setEventVenueCallback)
{
	jQuery.ajax({
		url:'/venues/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("save venue embedded model "+ JSON.stringify(data));
			if (setEventVenueCallback)
			{
				setEventVenueCallback(data);
			}
		}
	});
}


function savePotentialVenueProperty(saveInputElement, elementValue, elementName, modelName, targetProperty)
{
	var validation = $(saveInputElement).validationEngine('validate');
	if(!validation)
	{
		var data = {
			value: [elementName + "=" + elementValue],
			targetIndex: 0,
			targetProperty: targetProperty,
			modelName: modelName
		};
		    
		saveEventEmbeddedModel(data);
	}
}

function savePotentialVenueEmbedded(saveInputElement, elementValue, elementName, targetProperty, subTargetProperty, modelName, subModelName)
{
	//console.log("preSaveToVenue");
	var validation = $(saveInputElement).validationEngine('validate');
	if (!validation)
	{
		var data = {
			value: [elementName + "=" + elementValue],
			targetIndex: 0,
			subTargetIndex:0,
			targetProperty: targetProperty,
			subTargetProperty: subTargetProperty,
			modelName: modelName,
			subModelName: subModelName
		};
		    
		saveEventEmbeddedModel(data);
	}	
}

function getVenueCatererProperty(venueProperty)
{
	var venueIdFromProperty = $('['+venueProperty+']').attr(venueProperty);
	if (venueIdFromProperty == "")
	{
		venueIdFromProperty = null;
	}
	var data = {
		venueId: venueIdFromProperty
	}
	console.log("searching venueProperty" +venueProperty + " value: "+venueIdFromProperty);
	return data;
}


/*function selectVenueFromAutoComplete(venuesObjectArray, selectedVenueName){
	// var selectedVenueName = $('#autoCompleteVenueName').val();
	//update the venue id in the event
	$(venuesObjectArray).each(function(){
		console.log (" venue name "+this.venueName +" ")
		if( $.trim(this.venueName) == $.trim(selectedVenueName))
		{
			setVenueValues(this,true,settingAutoCompleteCurrentVenueProperty);
			return false;
		}
	});
}

function selectCatererFromAutoComplete(venuesObjectArray, selectedCatererName){
	// var selectedCatererName = $('#autoCompleteCatererName').val();
	//update the venue id in the event
	$(venuesObjectArray).each(function(){
		console.log (" Caterer name "+this.venueName +" ")
		if( $.trim(this.venueName) == $.trim(selectedCatererName))
		{
			setCatererValues(this,true,settingAutoCompleteCurrentCatererProperty);
			return false;
		}
	});
}*/



function setVenueValues (venue, needSave,settingAutoCompleteCurrentVenuePropertyCallBack, venueStatus, version, eventRequestMode)
{
   //if we need to save 
   if (needSave)
   {
        //save the venueId to the event
  	   saveVenueIdToEvent(venue.id,settingAutoCompleteCurrentVenuePropertyCallBack)
   }
   else
   {
      //don't save but set the property
      settingAutoCompleteCurrentVenuePropertyCallBack(venue.id);
   }

   	var venueName = (tryingGettingValue(venue,"venueName")).replace(/_+/g, "'");//venue.venueName;
	var address1 = tryingGettingValue(venue,"addresses[0].address1");
	var address2 = tryingGettingValue(venue,"addresses[0].address2");
	var city = tryingGettingValue(venue,"addresses[0].city");
	var postalCode = tryingGettingValue(venue,"addresses[0].postalCode");
	var state = tryingGettingValue(venue,"addresses[0].state");
	var phoneNumber = tryingGettingValue(venue,"phoneNumbers[0].number");
	var firstName = tryingGettingValue(venue,"contact[0].firstName");
	var middleName = tryingGettingValue(venue,"contact[0].middleName");
	var lastName = tryingGettingValue(venue,"contact[0].lastName");
	var email = tryingGettingValue(venue,"contact[0].email");
	var type = tryingGettingValue(venue,"venueType");
	var contactPhoneNumber;

	if (version == "" || version == "RepProvided")
	{
		contactPhoneNumber = tryingGettingValue(venue,"contact[0].number");

	} else {
		contactPhoneNumber = tryingGettingValue(venue,"contact[0].phoneNumbers[0].number");
	}

	if (eventRequestMode == true)
	{
		$("#venueTypeComboBoxRequest option").filter(function() {
		    return $(this).text() == type; 
		}).prop('selected', true);

		$("#venueStateComboBoxRequest option").filter(function() {
		    return $(this).text() == state; 
		}).prop('selected', true);

		$("#venueStatusRequest option").filter(function() {
		    return $(this).text() == venueStatus; 
		}).prop('selected', true);

		$('#venueNameRequest').val(venueName);
		$('#venueStreetAddress1Request').val(address1); 
		$('#venueStreetAddress2Request').val(address2); 
		$('#venueCityRequest').val(city); 
		$('#venuePostalCodeRequest').val(postalCode);
		$('#venuePhoneNumberRequest').val(phoneNumber); 
		$('#venueContactFirstNameRequest').val(firstName);
		$('#venueContactLastNameRequest').val(lastName); 
		$('#venueContactEmailRequest').val(email); 
		$('#venueContactNumberRequest').val(contactPhoneNumber); 


	} else {

		if (version == "Official")
		{
			$("#venueTypeComboBox" + version + " option").filter(function() {
			    return $(this).text() == type; 
			}).prop('selected', true);
			
			$("#venueStateComboBox option").filter(function() {
			    return $(this).text() == state; 
			}).prop('selected', true);

			if (venueStatus || venueStatus != '')
			{
				$("#venueStatus" + version + " option").filter(function() {
				    return $(this).text() == venueStatus; 
				}).prop('selected', true);
			} 

			$('#venueType' + version).html(type);
			$('#venueName' + version).html(venueName);
			$('#venueStreetAddress1'+ version).html(address1); 
			$('#venueStreetAddress2'+ version).html(address2); 
			$('#venueCity'+ version).html(city); 

			$('#venuePostalCode'+ version).html(postalCode);
			$('#venuePhoneNumber'+ version).html(phoneNumber); 

			//If showing combobox, set its option,otherwise it's a label
			$("#venueState" + version).html(state);

			$('#venueContactFirstName'+ version).html(firstName); 
			$('#venueContactLastName'+ version).html(lastName); 
			$('#venueContactEmail'+ version).html(email); 
			$('#venueContactNumber'+ version).html(contactPhoneNumber); 

		} else {
			$('#venueTypeRepProvided').html(type);
			$('#venueNameRepProvided').html(venueName);
			$('#venueStreetAddress1RepProvided').html(address1); 
			$('#venueStreetAddress2RepProvided').html(address2); 
			$('#venueCityRepProvided').html(city); 
			$('#venuePostalCodeRepProvided').html(postalCode);
			$('#venuePhoneNumberRepProvided').html(phoneNumber); 
			$("#venueStateRepProvided").html(state);
			$('#venueContactFirstNameRepProvided').html(firstName);
			$('#venueContactLastNameRepProvided').html(lastName); 
			$('#venueContactEmailRepProvided').html(email); 
			$('#venueContactNumberRepProvided').html(contactPhoneNumber); 

			$('#venueTypeNew').html(type);
			$('#venueNameNew').html(venueName);
			$('#venueStreetAddress1New').html(address1); 
			$('#venueStreetAddress2New').html(address2); 
			$('#venueCityNew').html(city); 
			$('#venuePostalCodeNew').html(postalCode);
			$('#venuePhoneNumberNew').html(phoneNumber); 
			$("#venueStateNew").html(state);
			$('#venueContactFirstNameNew').html(firstName); 
			$('#venueContactLastNameNew').html(lastName); 
			$('#venueContactEmailNew').html(email); 
			$('#venueContactNumberNew').html(contactPhoneNumber); 
		}
	}
}

function tryingGettingValue(venue,property)
{
	var returnValue ="";
	try
	{
		returnValue = eval('venue.'+property);
	}
	catch (err)
	{
		returnValue = "";
	}
	return returnValue;
}

function setCatererValues (caterer,needSave,settingAutoCompleteCurrentCatererPropertyCallBack, catererStatus, version, eventRequestMode)
{
   //if we need to save 
   if (needSave)
   {
     //save the catererID to the event
     saveCatererIdToEvent(caterer.id,settingAutoCompleteCurrentCatererPropertyCallBack)
   }
   else
   {
      //don't save but set the property
      settingAutoCompleteCurrentCatererPropertyCallBack(caterer.id);
   }

   	var catererName = (caterer.venueName).replace(/_+/g, "'");
	var firstName = tryingGettingValue(caterer,"contact[0].firstName");
	var lastName = tryingGettingValue(caterer,"contact[0].lastName");
	var email= tryingGettingValue(caterer,"contact[0].email");
	var contactPhoneNumber;

	if (version == "" || version == "RepProvided")
	{
		contactPhoneNumber = tryingGettingValue(caterer,"contact[0].number");
	} else {
		contactPhoneNumber = tryingGettingValue(caterer,"contact[0].phoneNumbers[0].number");
	}

	if (eventRequestMode == true)
	{
		$('#catererNameRequest').val(catererName);
		$('#catererContactFirstNameRequest').val(firstName); 
		$('#catererContactLastNameRequest').val(lastName); 
		$('#catererContactNumberRequest').val(contactPhoneNumber); 
		$('#catererContactEmailRequest').val(email); 

		$("#catererStatus" + version + " option").filter(function() {
		    return $(this).text() == catererStatus; 
		}).prop('selected', true);

	} else {
		if (version == "Official")
		{
			if (catererStatus)
			{
				$("#catererStatus" + version + " option").filter(function() {
				    return $(this).text() == catererStatus; 
				}).prop('selected', true);
			}

			$('#catererName'+ version).html(catererName);
			$('#catererContactFirstName'+ version).html(firstName); 
			$('#catererContactLastName'+ version).html(lastName); 
			$('#catererContactEmail'+ version).html(email); 
			$('#catererContactNumber'+ version).html(contactPhoneNumber); 

		} else {
			$('#catererNameRepProvided').html(catererName);
			$('#catererContactFirstNameRepProvided').html(firstName); 
			$('#catererContactLastNameRepProvided').html(lastName); 
			$('#catererContactNumberRepProvided').html(contactPhoneNumber); 
			$('#catererContactEmailRepProvided').html(email); 


			$('#catererNameNew').html(catererName);
			$('#catererContactFirstNameNew').html(firstName); 
			$('#catererContactLastNameNew').html(lastName); 
			$('#catererContactNumberNew').html(contactPhoneNumber); 
			$('#catererContactEmailNew').html(email); 
		}
	}
}


function validateEventStep3(functionToCall)
{
	var validation = $('#eventStep3ValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}

