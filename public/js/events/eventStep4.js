function initEventStep4() {
	//$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	initDataInjection('/events/getCurrentEvent', injectDataToStep4CallBack, finalizeStep4Init);
}

function injectDataToStep4CallBack(currentEvent)
{
	//Do something here!
	finalizeStep4Init();
}

function finalizeStep4Init()
{
	//add listeners, etc
	$(".js-invites-event-property").change(function (){
		var value = false;
	    if($(this).is(':checked')){
	       value = true;
	    }
	    saveCheckEventProperty($(this),value);
	});

	$(".js-event-property").change(function (){saveEventProperty(this);});

	//start the validation engine 
	$('#EventStep4ValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});
}

function saveCheckEventProperty($target,value)
{
	var data = $target.attr("data-bind") + "=" +value;
	saveEventModel(data);

	var removeData = {value: [data]};

	//make sure we handle invitations array if turning option off
	jQuery.ajax({
		url:'/events/removeAllInvitations',
		datatype:'json',
		type: 'POST',
		async: true,
		data: removeData,
		success:function(data){
			console.log('Invitations removed');
		}
	});
}

function validateEventStep4(functionToCall)
{
	var validation = $('#EventStep4ValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}

