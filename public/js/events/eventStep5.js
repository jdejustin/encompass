function initEventStep5() {
	//$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	initDataInjection('/events/getCurrentEvent', injectDataToStep5CallBack, finalizeInitStep5);
}

function injectDataToStep5CallBack(currentEvent)
{
	//Do injection here!

	
	
	finalizeInitStep5();
}


function finalizeInitStep5()
{
	//add listeners, etc
	initGlobal();

	//disable delete buttons if no contacts in lists
	var totalRowEmail = $('#emailInvitations tr').length;
	var totalRowHardcopy = $('#hardcopyInvitations tr').length;
	var totalRowPDFEmail = $('#emailPDFInvitations tr').length;

	//set up delete buttons
	$('.js-deleteEmailInvitationsBtn').click(function(){deleteInvitations('emailInvitations')})
	$('.js-deleteHardcopyInvitationsBtn').click(function(){deleteInvitations('hardcopyInvitations')})
	$('.js-deleteEmailPDFInvitationsBtn').click(function(){deleteInvitations('emailPDFInvitations')})
	//Since header of table is row #1, then we would show the button only if rows > 1
	if(totalRowEmail == 1){
		$('.js-deleteEmailInvitationsBtn').hide();
	}

	if(totalRowHardcopy == 1){
		$('.js-deleteHardcopyInvitationsBtn').hide();
	}

	if(totalRowPDFEmail == 1){
			$('.js-deleteEmailPDFInvitationsBtn').hide();
		}

	$(".js-event-property").focusout(function (){
		saveEventProperty(this);
	});

	//init function to call when search is completed
  	initAudienceRecruitmentCheckboxListeners();

  	onRowClickFunction = initAudienceRecruitmentCheckboxListeners;
}

function initAudienceRecruitmentCheckboxListeners()
{
	// var columnTh = $("table th:contains('Email')");

	// columnTh.find('[type=checkbox]').addClass('js-email-checkbox');
	 //$('.mainTable tbody td:nth-child(0)').find('[type=checkbox]').addClass('js-email-checkbox');
	// $('.mainTable tbody td').find('[type=checkbox]').addClass('selectElement');

    //listeners for email and hard copy checkboxes
	$('.js-email-checkbox').click(function(){onInvitationChecked(this, 'emailInvitations')});
	$('.js-hardcopyCheckbox').click(function(){onInvitationChecked(this, 'hardcopyInvitations')});
	$('.js-pdf-checkbox').click(function(){onInvitationChecked(this, 'emailPDFInvitations')});
}

function onInvitationChecked(target, targetList)
{
	//get contact info and add it to the email table
	var contact = new Object();
	contact.id = $(target).val();
	contact.firstName = $(target).parents('tr').find('.js-firstName').html();
	contact.lastName = $(target).parents('tr').find('.js-lastName').html();

	//first check if contact is already in list, if it is, see if user actually uncheck the box, if yes, then we need to remove the contact.
	var isContactThere = contactInvited(contact.id, targetList);

	if ($(target).is(':checked'))
	{
		//first check to make sure we have not hit the limit of people that can be invited
		var totalRows = $('#' + targetList + ' tr').length - 1;
		if (totalRows == 50)
		{
			$('#limitWarning').show().delay(8000).fadeOut(400);

			//remove check also
			$(target).removeAttr("checked");
		} else {
			$('#limitWarning').hide();

			//if we didn't find the contact already in the list, then add them
			if (!isContactThere.found)
			{
				//add contact to table
				insertContact(contact, targetList);

				//update table's counter
				updateCounterAndSaveChange(targetList, 'addContact', contact);
			} 
		}
	} else {
		$('#limitWarning').hide();

		if (isContactThere.found)
		{
			//find contact on table and remove it
			removeContactFromList(targetList, isContactThere.index);
			
			//update table's counter
			updateCounterAndSaveChange(targetList, 'removeContact', contact);
		}
	}
}

function insertContact(contact, targetList)
{
	//insert contact into table
	var newRow = '<td><input type="checkbox" value="' + contact.id + '"/></td>';
	newRow += '<td>' + contact.firstName + " " + contact.lastName + '</td>';

	$('#' + targetList).find("tbody").append("<tr>" + newRow + "</tr>");
}


function updateCounterAndSaveChange(targetList, action, contact)
{	
	var totalRows = $('#' + targetList + ' tr').length - 1;
	$('#' + targetList).find('.js-counter').text(totalRows + '/50');

	// //save change to DB, if adding a new contact to list
	if (action == 'addContact')
	{
		saveInvitationListContact(targetList, contact);
	} else {
		removeInvitationListContact(targetList, contact);
	}

	if (totalRows == 0)
	{
		switch(targetList)
		{
			case 'emailInvitations':
				$('.js-deleteEmailInvitationsBtn').hide();
				break;

			case 'hardcopyInvitations':
				$('.js-deleteHardcopyInvitationsBtn').hide();
				break;

			case 'emailPDFInvitations':
				$('.js-deleteEmailPDFInvitationsBtn').hide();
				break;
		}
	} else {
		switch(targetList)
		{
			case 'emailInvitations':
				$('.js-deleteEmailInvitationsBtn').show();
				break;

			case 'hardcopyInvitations':
				$('.js-deleteHardcopyInvitationsBtn').show();
				break;

			case 'emailPDFInvitations':
				$('.js-deleteEmailPDFInvitationsBtn').show();
				break;
		}
	}
}


function removeContactFromList(targetList, index)
{
	$('#' + targetList + ' tr:eq(' + index + ')').remove();
}


function contactInvited(id, list)
{
	var indexFound = -1;

	//let's see if we can find the id on this list
	$('#' + list + ' tbody td input:checkbox').each(function(index) {
		if (id == $(this).val())
		{
			indexFound = index;
		}
	});

	if (indexFound >= 0)
	{
		return {found: true, index: indexFound + 1};
	} else {
		return {found: false, index: indexFound};
	}
}


function deleteInvitations(targetList)
{
	var selectedContacts = [];
	var totalContactsProcessed = 0;

	$('#' + targetList + ' tbody td input:checkbox').each(function(index) {
		if($(this).is(':checked'))
		{
				selectedContacts.push($(this).val());

				var contact = new Object();
				contact.id = $(this).val();

				var onRemoveInvitationCompleted = function(){
					totalContactsProcessed++;
					console.log('came back' + totalContactsProcessed);
					if (totalContactsProcessed == selectedContacts.length)
					{
						//we're done, let's reload the page
						goEventStep5();
					} 
				}

				removeInvitationListContact(targetList, contact, onRemoveInvitationCompleted);
		}
	});
}