function initEventStep6() {

	//$('html, body').animate({ scrollTop: 0 }, 'slow');

	$('#wait').hide();

	var buttonContainerText = 'Submit';
	var $verifyCheck = $('#verifyCheck')

	//exist the verify and it is in request mode
	if($verifyCheck)
	{
		//disable the submit button
		var $button = $("button:contains('"+buttonContainerText+"')");
		$button.prop('disabled', true);
		//uncheck the verify the submit button
		$verifyCheck.change(function (){
			var value = false;
		    if($(this).is(':checked')){
		       	//enable the button
				$button.prop('disabled', false);
		    }
		    else{
				//disable the button
				$button.prop('disabled', true);
		    }
		});
		
	}
}
