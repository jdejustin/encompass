function setCurrentEventId(id)
{
	jQuery.ajax({
		url:'/events/setCurrentEvent',
		datatype:'json',
		type: 'POST',
		async: true,
		data: id,
		success:function(data){
			console.log("current event id set");
		}
	});
}

function saveEventProperty(target)
{
	var validation = $(target).validationEngine('validate');
	if($(target).val() && $(target).val() != selectPrompt && !validation)
	{
		//console.log("saving client");
		var serializedData = $(target).serialize();
		saveEventModel(serializedData);
	}
}

function saveToEvent(target, targetProperty, targetModel)
{
	var targetIndexToSave = $(target).parents('.js-row').find('.js-index').val();
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var data = {
			value: [$(target).attr("name") + "=" + $(target).val()],
			targetIndex: targetIndexToSave,
			targetProperty: targetProperty,
			modelName: targetModel
		};
	    
		saveEventEmbeddedModel(data);
	}
}

function saveEventModel(data)
{
	jQuery.ajax({
		url:'/events/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("saved");
		}
	});
}


function saveEventEmbeddedModel(data)
{
	jQuery.ajax({
		url:'/events/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("Saved: " + data);
		}
	});
}

function saveInvitationListContact(targetList, contact)
{
	var data = new Object();
	data.targetList = targetList;
	data.contact = contact;

	jQuery.ajax({
		url:'/events/saveInvitationListContact',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("saved");
		}
	});
}

function removeInvitationListContact(targetList, contact, callBack)
{
	var data = new Object();
	data.targetList = targetList;
	data.contact = contact;

	jQuery.ajax({
		url:'/events/removeInvitationListContact',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("contact removed from list");
			if (callBack)
			{
				callBack();
			}
		}
	});
}


function goEventDetails()
{
    jQuery('#content').load('/events/1234 #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else	if (data == 'null')
	   {
			goEventsList();
	   } else {
			initEventsShow();
	   }
	});
}



function goEventsList()
{
    jQuery('#content').load('/events #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initEventsIndex();
	   }
	});
}


function goEventStep1()
{
	jQuery('#content').load('/events/eventStep1 #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initEventStep1();
	   }
	});
}

function goEventStep2()
{
	jQuery('#step2content').load('/events/eventStep2 #EventStep2ValidateForm',function (data){
	//jQuery('#content').load('/events/eventStep2 #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initEventStep2();
	   }
	});
}


function goEventStep2test(currentEvent)
{
	jQuery('#content').load('/events/eventStep2 #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initEventStep2(currentEvent);
	   }
	});
}

function goEventStep3()
{
	jQuery('#step3content').load('/events/eventStep3 #eventStep3ValidateForm',function (data){
	//jQuery('#content').load('/events/eventStep3 #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initEventStep3();
	   }
	});
}

function goEventStep4()
{
	jQuery('#step4content').load('/events/eventStep4 #eventStep4ValidateForm',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initEventStep4();
	   }
	});
}

function goEventStep5()
{
	jQuery('#step5content').load('/events/eventStep5 #eventStep5ValidateForm',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initEventStep5();
	   }
	});
}

function goEventStep6()
{
	jQuery('#step6content').load('/events/eventStep6 #eventStep6ValidateForm',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initEventStep6();
	   }
	});
}	

function submitRequest()
{
	$('#step6Section').hide();
	$('#wait').show();

	var data =  "";
	jQuery.ajax({
		url:'/events/submitStatus',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			//redirect to portal reps
			if (data.notificationData.managerRequesting)
			{
				// send automatic communication delivery type to manager
				jQuery.ajax({
					url:'/communications/sendAutomaticEventRequestNotification',
					datatype:'json',
					type: 'POST',
					async: true,
					data: data.notificationData,
					success:function(data){
						alert('Your program request has been received and is processing.');
		 	   			goToManagerProgramDetails();
					 // console.log("event approved or denied");
					}
	      		});
			} else {

				// send automatic communication delivery
				jQuery.ajax({
					url:'/communications/sendAutomaticEventRequestNotification',
					datatype:'json',
					type: 'POST',
					async: true,
					data: data.notificationData,
					success:function(result){
						//need to email the manager also
						data.notificationData.deliveryType = data.approval_denial_type;

					 	jQuery.ajax({
							url:'/communications/sendAutomaticEventRequestNotification',
							datatype:'json',
							type: 'POST',
							async: true,
							data: data.notificationData,
							success:function(data){
								alert('Your program request has been received and is processing.');
								goToRepProgramDetails();
							 // console.log("event approved or denied");
							}
			      		});
					 
					}
	      		});
			}
		}
	});
}	