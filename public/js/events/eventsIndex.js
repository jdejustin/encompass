function initEventsIndex() {

  $('html, body').animate({ scrollTop: 0 }, 'slow');

  initGlobal();
  
  $('#advancedSearch').click(function(){
    //display advanced search options
    $('#advancedSearchOptions').toggle();
  });


	$('.deleteSelectedBtn').click(function(){
    	showDeleteEvents(this);
    });

  //init function to call when search is completed
  initClickOnTableRowEvents();

  onRowClickFunction = initClickOnTableRowEvents;

  $( "#dialog-confirm-delete" ).dialog({autoOpen: false});
};

function initClickOnTableRowEvents()
{
  $('.mainTable tbody td').not('.checkBoxTd').click(function(){
    jQuery("#content").load('/events/' + $(this).parent().find('[type=checkbox]').val() + ' #AjaxContent', function (data){
     if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
        initEventsShow();
     }
    });
  });

  //add select class to checkbox
  $('.mainTable tbody td').find('[type=checkbox]').addClass('selectElement');

  //set the behaviour of the select all element
  setSelectionBehaviour();  
}

function showDeleteEvents(element)
{
  var callBack = function (data)
  {
     //finally, remove programs from clients
     jQuery("#content").load('/events/deleteEvents #AjaxContent', {'ids': data}, function (data) {
        if(data == 'false')
       {
          window.location = "/expired";
       }
       else
       {
          goEventsList();
       }
     });
  }

  var title = 'Delete Selected Event(s)?';
  var message = 'Selected Event(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/events/deleteEventFromProgram';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element, callBack);
}


function createNewEvent()
{
	jQuery("#content").load('/events/clearCurrentEvent #AjaxContent', function (data){
    if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
      	jQuery("#content").load('/events/eventStep1 #AjaxContent', function(){
      		initEventStep1();
        });
     }
	});
}  