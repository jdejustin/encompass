function initEventFinances() {
	
	$(".js-finance-property-number").focusout(function (){saveToEvent(this, 'invoices', 'Invoice');});
	$(".js-finance-property").focusout(function (){saveNumericToEvent(this, 'invoices', 'Invoice');});
	
	$('.js-number-only').autoNumeric('init', {aPad: false, aSep: ''});
	$('.js-monetary-only').autoNumeric('init', {aSign:'$'});


	$(".js-dateField").datepicker({
      changeMonth: true,
      changeYear: true,
      onSelect: onCheckPaidDateSelected
    });

    //start the validation engine 
	//$('#EventFinancesValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});
}


function onCheckPaidDateSelected(dateText, target)
{
	var serializeData =  $(target).prop('input').attr("name")+'='+dateText;
	
	var data = {
			value: [serializeData],
			targetIndex: 0,
			targetProperty: 'invoices',
			modelName: 'Invoice'
		};
	    
		jQuery.ajax({
			url:'/events/saveEmbeddedModel',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
			success:function(data){
				console.log("Saved: " + data);
			}
		});
}



function saveToEvent(target, targetProperty, targetModel)
{
	if($(target).val())
	{
	    var targetIndex = 0;//$(target).parent().parent().find('.Index').val(); 
	    
		var data = {
			value: [$(target).attr("name") + "=" + $(target).val()],
			targetIndex: targetIndex,
			targetProperty: targetProperty,
			modelName: targetModel
		};
	    
		jQuery.ajax({
			url:'/events/saveEmbeddedModel',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
			success:function(data){
				console.log("Saved: " + data);
			}
		});
	}
}

function saveNumericToEvent(target, targetProperty, targetModel)
{
	if($(target).val())
	{
	    var targetIndex = 0;//$(target).parent().parent().find('.Index').val(); 
	    
		var data = {
			value: [$(target).attr("name") + "=" + $(target).autoNumeric('get')],
			targetIndex: targetIndex,
			targetProperty: targetProperty,
			modelName: targetModel
		};
	    
		jQuery.ajax({
			url:'/events/saveEmbeddedModel',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
			success:function(data){
				console.log("Saved: " + data);
			}
		});
	}
}