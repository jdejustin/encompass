function initEventNoteStep1() {
	
	$(".js-note-property").focusout(function (){saveNoteToEvent(this);});
}


function saveNoteToEvent(target)
{
	var data = {
		value: [$(target).attr("name") + "=" + $(target).val()],
		targetProperty: "notes",
		modelName: "EventNote"
	};
	    

	jQuery.ajax({
		url:'/events/saveNote',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("saved");
		}
	});
}