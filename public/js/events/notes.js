function initEventNotes() {

  $('html, body').animate({ scrollTop: 0 }, 'slow');

  initGlobal();
  

	$('.deleteSelectedBtn').click(function(){
    	showDeleteEventNotes(this);
    });

   //init function to call when search is completed
   initClickOnTableRowEventNotes();

  //set the behaviour of the select all element
  setSelectionBehaviour(); 

  $( "#dialog-confirm-delete" ).dialog({autoOpen: false});
  $( "#eventNote-form" ).dialog({autoOpen: false});
};


function initClickOnTableRowEventNotes()
{
    $('.mainTable tbody td').not('.checkBoxTd').click(function () {
      
      var noteIndex = $(this).parent().find('[type=checkbox]').val();

      jQuery("#content").load('/events/setCurrentEventNoteIndex #AjaxContent', {noteIndex: noteIndex}, function (data){
        if(data == 'false')
         {
            window.location = "/expired";
         }
         else
         {
           jQuery("#content").load('/events/eventNoteStep1 #AjaxContent', function(){
              initEventNoteStep1();
           });
         }
      });
    });
}

function showDeleteEventNotes(element)
{

  var title = 'Delete Selected Note(s)?';
  var message = 'Selected Note(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/events/deleteNotes';

  //Prompt user if they're sure... if yes, then let's delete the selected contacts from DB
  initDeleteDialog(title,message,deleteUrl,element);
}


function createNewNote()
{
  var noteIndex =  $('.mainTable tbody tr').length;

   jQuery("#content").load('/events/setCurrentEventNoteIndex #AjaxContent', {noteIndex: noteIndex}, function (data){
      if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
        jQuery("#content").load('/events/eventNoteStep1 #AjaxContent', function(){
           initEventNoteStep1();
        });
     }
   });
}