function initEventsShow(){
	$('html, body').animate({ scrollTop: 0 }, 'slow');

	initGlobal();
}

function editEvent() {
	jQuery("#content").load('/events/eventStep1 #AjaxContent', function (data){
      if(data == 'false')
      {
         window.location = "/expired";
      }
      else
      {
		    initEventStep1();
      }
	});
}