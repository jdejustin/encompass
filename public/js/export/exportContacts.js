function initExport()
{
   initGlobal();

  //   $( "#sortable1, #sortable2" ).sortable({
  //    connectWith: ".connectedSortable"
  //   }).disableSelection();

  // jQuery.ajax({
  //   url: "/exports/getCurrentExportObject",
  //   datatype:'json',
  //   type: 'POST',
  //   async: true,
  //   success:function(data){
  //     generatePropertyCheckBoxes(data.obj, data.discardProps);
  //     // generateTable(data.contacts);
  //   }
  // });


  // call controller with correct action (this will set currentExportObject, with the correct info based on the action called)
  // respond to load view and call initExport
  // initExport calls to 'getCurrentExportObject'
  // on respond, build list of checkboxes based on properties.
  // add export button, and on click, generate the table and ask to download

}


// function generatePropertyCheckBoxes(data, discardProps)
// {
//    var propertiesArray = Object.getOwnPropertyNames(data);
   
//    $('.js-properties-area').append('<div class="form-inline row-space-xs">');

//    var i;
//    // var counter = 1;

//    for (i= 0; i < propertiesArray.length; i++) {
//       //if property is in discard list, then skip
//       if (discardProps.indexOf(propertiesArray[i]) == -1)
//       {
//         var propName = humanNameProp(propertiesArray[i]);

//         // if (counter == 1)
//         // {
//         //   $('.js-properties-area').append("<div class='form-group'>");
//         // }

//         // $('.js-properties-area').append("<input type='checkbox' value='" + propertiesArray[i] + "'/><label class='CheckboxLabel'>" + propName+"</label>");
//          $('#sortable1').append('<li class="ui-state-default">' + propName + '</li>');

//         // if (counter == 6)
//         // {
//         //   $('.js-properties-area').append("</div'>");
//         //   counter = 1;
//         // } else {
//         //   counter++;
//         // }
//       }
//    }
//    $('.js-properties-area').append('</div">');
// }


// function tableToExcel (table, name, filename) 
// {
//     var uri = 'data:application/vnd.ms-excel;base64,'
//     , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
//     , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
//     , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    
//      if (!table.nodeType) table = document.getElementById(table)
//      var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

//      document.getElementById("dlink").href = uri + base64(format(template, ctx));
//      document.getElementById("dlink").download = filename;
//      document.getElementById("dlink").click();
// }

  // function generateTable(objectArray)
  // {

  //   //create header 
  //   if (objectArray.length > 0)
  //   {

  //     var tempObject = objectArray[0];
  //     var propertiesArray = Object.getOwnPropertyNames(tempObject);
  //     /*
  //     getOwnPropertyNames 
  //     Feature   Firefox (Gecko)   Chrome  Internet Explorer   Opera   Safari
  //     Basic support   4 (2.0)        5        9                 12      5
  //     */
  //     for (i= 0; i < propertiesArray.length; i++) {
  //        var headerName = humanNameProp(propertiesArray[i]);

  //        $('.js-properties-header').append("<th>"+headerName+"</th>");
  //     }

  //     for (j= 0; j < objectArray.length; j++) {
  //       var newElement = "<tr>";
  //       for (k= 0; k < propertiesArray.length; k++) {
  //        var value = objectArray[j][propertiesArray[k]];

  //        if (!value)
  //        {
  //           value = ''
  //        }

  //        newElement += "<td>"+ value +"</td>";
  //       }
  //       newElement += "</tr>";
  //        $('.js-table-container').append(newElement);
  //     }

  //     tableToExcel('mainTable', 'export', 'export.xls');
      
  //   }

  // }

  // function humanNameProp (stringElement)
  //  {
  //     stringElement = stringElement.replace(/([A-Z])/g," $1");
  //     stringElement = stringElement.substring(0,1).toUpperCase() + stringElement.substring(1,stringElement.length); 
  //     return stringElement;
  //  }