var onRowClickFunction;
var uploadTargetId;
var fileUploadSuccessFunction;// this function will be executed after upload a file in case that we need to update ids or someting 
var fileUploadCloseFunction;
var fileDisplayNameInput;
var selectedFile;
var uploadNotes;
var uploadLink;
var uploadData;
var allTextFields;
var allSelectFields;
var tips;
var files;
var data;
var uploadType;
//var dataToClone;
var selectPrompt = '';


function setPrompt(prompt)
{
  selectPrompt = prompt;
}

////////////////////////////  INIT //////////////////////////////////
function initGlobal(){
  // table sorter 
   $.extend($.tablesorter.themes.bootstrap, {
       // these classes are added to the table. To see other table classes available,
       // look here: http://twitter.github.com/bootstrap/base-css.html#tables
       table      : 'table table-bordered',
       caption    : 'caption',
       header     : 'bootstrap-header', // give the header a gradient background
       footerRow  : '',
       footerCells: '',
       icons      : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
       sortNone   : 'bootstrap-icon-unsorted',
       sortAsc    : 'icon-chevron-up glyphicon glyphicon-chevron-up',     // includes classes for Bootstrap v2 & v3
       sortDesc   : 'icon-chevron-down glyphicon glyphicon-chevron-down', // includes classes for Bootstrap v2 & v3
       active     : '', // applied when column is sorted
       hover      : '', // use custom css here - bootstrap class may not override it
       filterRow  : '', // filter row class
       even       : '', // odd row zebra striping
       odd        : ''  // even row zebra striping
     });

     SetupSortableTable(".mainTable");

    $('#AdvSearchForm').ajaxForm({
         success: function () {
         },
         complete: function (xhr) {
             var data = jQuery.parseJSON(xhr.responseText);
             injectRows(data, ".mainTable" );
             //set currentPage and recordCount
             updatePageNavigation(data);
         }
      });
     
     jQuery("#advancedSearchButton").click(function () {
        $('#CurrentPage').val(1);//set to 1 for search for the first page
         jQuery("#AdvSearchForm").submit();
     });

     jQuery("#AddFilter").click(function () {
         addFilter();
     });
}


function getElementListByPage(target,pageNumber)
{
  if (!($(target).parent().hasClass('disabled')))
  {
    var newPage = parseInt($('#CurrentPage').attr('value'))+pageNumber;
    jQuery("#CurrentPage").val(newPage);
    jQuery("#AdvSearchForm").submit();
  }
}

function getElementListByPage2(target,pageNumber)
{
  if (!($(target).parent().hasClass('disabled')))
  {
    var newPage = parseInt($('#CurrentPage2').attr('value'))+pageNumber;
    jQuery("#CurrentPage2").val(newPage);
    jQuery("#PriorSearchForm").submit();
  }
}

function updatePageNavigation(data)
{
  //update the totalPages
  $('#TotalPages').attr('value',data.totalPages);
  $('#TotalPages').html(data.totalPages);

   //updateCurrentPage
  $('#LabelCurrentPage').html(data.currentPage);

  $('.js-page-label').html(data.currentPage + ' / ' + data.totalPages);


  if (data.totalPages == 1)
  { 
    $('.js-pager').addClass('hidden');
  }
  else
  {
    $('.js-pager').removeClass('hidden');
    // check for last and first page
    if (data.currentPage == 1)
    {
      //disable back button 
      //$('.js-page-back-button').attr('disabled',true);
      $('.js-page-back-button').addClass('disabled');
    }
    else
    {
      //$('.js-page-back-button').attr('disabled',false);
      $('.js-page-back-button').removeClass('disabled');

    }

    if (data.currentPage == data.totalPages)
    {
      //disable next button 
      //$('.js-page-next-button').attr('disabled',true);
      $('.js-page-next-button').addClass('disabled');
    }
    else
    {
      //$('.js-page-next-button').attr('disabled',false);
      $('.js-page-next-button').removeClass('disabled');
    }
  }
}


////////////////////////////  SEARCH //////////////////////////////////
function addFilter() {
    var $FiltersDropDownList = jQuery("#FiltersDropDownList");
  //  console.log("addFIlter dropDownlist val " + $FiltersDropDownList.val());
    if ($FiltersDropDownList.val() != "-1") {
        var $selectedFilter = $FiltersDropDownList.find("option:selected");

        var $filterName = $selectedFilter.val();
        var $filterDisplayText = $selectedFilter.text();


        var $newFilter = $("#FilterTemplate").clone();
        $newFilter.removeClass('hidden');
        jQuery("#FiltersZoneDiv").append($newFilter);
        $newFilter.show(400);

        var $newLabel = $newFilter.find(".filterLabel");
        var $newFilterInput = $newFilter.find(".filterInput");
        $newLabel.text($filterDisplayText);
        $newFilterInput.attr("name", $filterName);

        $FiltersDropDownList.val("-1");
        $selectedFilter.attr("disabled", "disabled");

        $newFilterInput.keypress(function(event){
            if (event.keyCode == 10 || event.keyCode == 13) 
            {
               event.preventDefault();
               jQuery("#AdvSearchForm").submit();
            }
        });

        $newFilter.find(".removeFilter").click(function () {
            var $parent = $(this).parent();
            var $FilterInput = $parent.find(".filterInput").eq(0);
            $FiltersDropDownList.find("option[value='" + $FilterInput.attr("name") + "']").removeAttr("disabled");
            $parent.hide(500, function () { $parent.remove(); jQuery("#AdvSearchForm").submit();});
        });

        $newFilter.find(".addInputFilter").click(function () {
            var $parent = $(this).parent();
            var $FilterInputDiv = $parent.find(".filterInputDiv").eq(0);
            var $newFilterInputDiv = $FilterInputDiv.clone();
            $newFilterInputDiv.find(".filterInput").val("");
            $parent.find(".addInputFilter").before($newFilterInputDiv);

            displayRemoveInputFilter($newFilter);
         });

         $newFilter.find('.filterInputDiv').find(".removeInputFilter").click(function () {
             var $parent = $(this).parent();
             $parent.remove();

             displayRemoveInputFilter($newFilter);
         });

         displayRemoveInputFilter($newFilter);
    } 
}

function displayRemoveInputFilter(targetFilter)
{
   //only show remove input filters if more then one field is displaying
   var totalFilters = targetFilter.find('.filterInputDiv').length;

   if (totalFilters > 1)
   {
      targetFilter.find('.filterInputDiv').find(".removeInputFilter").show();
   } else {
      targetFilter.find('.filterInputDiv').find(".removeInputFilter").hide();
   }
}

////////////////////////////  BINDING & DATA ELEMENTS //////////////////////////////////


function BindFields(data) {
    var $elementsToBind = jQuery("[data-bind]");
    $elementsToBind.each(function () {
        var $this = jQuery(this);
        var bindField = $this.attr("data-bind");
        var regex = /(\{(.*?)\})/g;
        match = regex.exec(bindField);
        if (match != null) {
            bindField = match[2];
            var newValue = ""

            try {
                newValue = eval("data." + bindField);
            } catch (e) {
                newValue = "-";
            }

            if (!newValue) {
                newValue = "-";
            }
            $this.html(newValue);
        }
    });
}




////////////////////////////  ROW CLONING & DELETING //////////////////////////////////
function deleteClonedRow(deleteButton,deleteCallBack)
{
  var $addButton = $(deleteButton).parents('.js-add-button-container').find('.js-add-button');
  //get the clone container for update the cloned indexes and get the targetProperty to remove index
  var $cloneContainer = $(deleteButton).parents('[data-clone]');
  //row to delete to get the index and after delete it
  var $rowToDelete = $(deleteButton).parents('.js-row');
  //target property to be deleted
  var targetProperty = $cloneContainer.attr('data-clone');
  //get the index to be removed
  var indexToDelete = $rowToDelete.find('.js-index').val();


  //check if the element was saved 
  var attr = $addButton.prop('disabled');
  // For some browsers, `attr` is undefined; for others,
  // `attr` is false.  Check for both.
  if (!(typeof attr !== 'undefined' && attr !== false) )
  {
    //delete the element in the database
    deleteCallBack(targetProperty,indexToDelete);
  }
  
  //remove index from database
  $rowToDelete.remove();

  //update indexes
  updateClonedIndexes($cloneContainer); 

  //enable the add Button
  $addButton.prop('disabled',false); 
  $addButton.removeClass('Disable');

  //enable all delete button
  $cloneContainer.find('.js-delete-button').each(function (){
    //enable delete buttons
    $(this).prop('disabled',false); 
    $(this).removeClass('Disable');
  });

  // hide the delete button if there are only one row
  if ( $cloneContainer.find('.js-delete-button').length == 1)
  {
    $cloneContainer.find('.js-delete-button').addClass("hidden");
  }
}

function cloneRow(addButtonElement,callBackSaveListeners,callBackChangeListeners)
{
  //element who contains the cloned elements
  var $dataCloneContainer = $(addButtonElement).parents('.js-add-button-container').find('[data-clone]');
  var cloneFieldName = $dataCloneContainer.attr('data-clone');
  
  //clone the row
  var $elementToClone = $('.'+cloneFieldName+'Template').clone();
  //remove template class to insert without this class
  $elementToClone.removeClass(cloneFieldName+"Template");
  //remove hidden class
  $elementToClone.removeClass('hidden');


  //disable all delete button
  $dataCloneContainer.find('.js-delete-button').each(function (){
    //show all buttons
    $(this).removeClass('hidden'); 
    //disable all buttons
    $(this).prop('disabled',true); 
    $(this).addClass('Disable');
  });

  //enable the last one
  $elementToClone.find('.js-delete-button').prop('disabled',false);
  $elementToClone.find('.js-delete-button').removeClass('Disable');


  //Disable the add button 
  $(addButtonElement).prop('disabled',true);
  $(addButtonElement).addClass('Disable');

  $dataCloneContainer.append($elementToClone);

  //updateClonedElementsIndexes this must be after append 
  updateClonedIndexes($dataCloneContainer);
  //the callback is a function to set the listeners of the fields to save in the focus out
  callBackSaveListeners();
  //add listeners of change in the $element to clone when add 
  callBackChangeListeners(addButtonElement);
}


////////////////////////////  INJECT DATA BINDING TO THE VIEWS //////////////////////////////////
function initDataInjection(targetUrl, callBack, setListenersCallBack)
{
  //console.log(">>>>>>>>>>>>>> getting element from: "+targetUrl);
  jQuery.ajax({
    url: targetUrl,
    datatype:'json',
    type: 'POST',
    async: true,
    success:function(data){
      //console.log(">>>>>>>>>>>>>> respond of: "+targetUrl+" data:"+JSON.stringify(data));
      /*if (data.currentObject)
      {*/
        injectDataToView(data.currentObject,callBack,setListenersCallBack);
      /*} else {
        console.log(">>>>>>>>>>>>>> executing final callback "+ finalCallBack);
        finalCallBack();
      }*/
    }
  });
}


//this function injects data to view and clone the necesary elements
//the callback is to set some things related with the current object 
function injectDataToView(dataToClone,callBack,setListenersCallBack)
{
  console.log(">>>>>>>>> INject Data to view dataClone  ");
  console.log(">>>>>>>>> DataToClone "+JSON.stringify(dataToClone));
  
  //if(typeof dataToClone != "undefined")
  //{
    //get all the elements that should be cloned
    var $elementsToClone = $("[data-clone]");  
    $elementsToClone.each(function () {
      var $cloneContainer = $(this);
      //target property of the element to be cloned 
      var cloneFieldId = $cloneContainer.attr("data-clone");
      console.log('cloneFiled '+ cloneFieldId);
      var data = null;
      try
      {
        //get element of the json dataToClone
        data = eval("dataToClone."+cloneFieldId);
      }
      catch (err)
      {
        //data = [];
      }

      if (data != null)
      {
        $(data).each(function (){
          var currentData = this;
          var $elementToClone = $("."+cloneFieldId+"Template").clone();
          $elementToClone.removeClass(cloneFieldId+"Template");
          $elementToClone.removeClass('hidden');
          
          var $dataBindElements = $elementToClone.find("[data-bindCloned]");  
          
          $dataBindElements.each(function (){
            var $this = $(this);
            //get the name of the data-bind 
            var bindElementName = $this.attr("data-bindCloned");
            var objectValue="";
            try
            {
              //eval the 
              objectValue = eval("currentData."+bindElementName);   
            }
            catch (err)
            {
              objectValue="";
            }

            setElementValue ($this, objectValue);
            
          });
        $cloneContainer.append($elementToClone);
        });  

        //if the data length is only one hide the delete button
        //console.log ("DataLength:"+$(data).length);
        if($(data).length == 1)
        {
          $cloneContainer.find('.js-delete-button').addClass('hidden');
        }

        //enable the addButton
        $cloneContainer.parents('.js-add-button-container').find(".js-add-button").removeClass("Disable");
        $cloneContainer.parents('.js-add-button-container').find(".js-add-button").prop("disabled",false);

        //update indexes
        updateClonedIndexes($cloneContainer);
      }  
      else
      {
        //show one element empty
        console.log("show one element empty");
        var cloneFieldId = $cloneContainer.attr("data-clone");
        var $elementToClone = $("."+cloneFieldId+"Template").clone();
        //remove template class to insert without this class
        $elementToClone.removeClass(cloneFieldId+"Template");
        //remove hidden class to insert without this class
        $elementToClone.removeClass('hidden');
        //hide the delete Button
        $elementToClone.find('.js-delete-button').addClass('hidden');
        //append the clone element
        $cloneContainer.append($elementToClone);
      }
       
    });


    //bind the normal properties (not embeded models) of the object 
    var $elementsToBindProperty = $("[data-bind]");  
    $elementsToBindProperty.each(function (){
      var $this = $(this);
      var dataBindPropertyFieldId = $this.attr("data-bind");
      var data = null;
      try
      {
        //get element of the json dataToClone
        data = eval("dataToClone."+dataBindPropertyFieldId);
      }
      catch (err)
      {
        //data = [];
      }
      //console.log($this.attr("data-bind")+': '+JSON.stringify(data));
      setElementValue ($this, data);
    });
  /*}
  else
  {
    console.log('something is wrong please check the dataToClone');
  }*/
  
  //the callback is to set some thing related with the current object  
  //if the current object does not exist or its a new element just call the finalCallback
  if(typeof dataToClone == "undefined" || dataToClone == null) 
  {
    if (setListenersCallBack)
    {
      setListenersCallBack();
    }
  }
  else
  {
    if (callBack)
    {
      callBack(dataToClone);
    }
  }
  //console.log('checking for callback:'+ callBack);
}




//this function injects data to view and clone the necesary elements
/*function injectDataToView2(dataToClone,callBack)
{
  //console.log(">>>>>>>>> INject Data to view dataClone  ");
  //console.log(">>>>>>>>> DataToClone "+JSON.stringify(dataToClone));
  if(typeof dataToClone != "undefined")
  {
    //get all the elements that should be cloned
    var $elementsToClone = jQuery("[data-clone]");  
    $elementsToClone.each(function () {
      var $cloneContainer = $(this);
      //target property of the element to be cloned 
      var cloneFieldId = $cloneContainer.attr("data-clone");
      console.log('cloneFiled '+ cloneFieldId);
      var data = null;
      try
      {
        //get element of the json dataToClone
        data = eval("dataToClone."+cloneFieldId);
      }
      catch (err)
      {
        data = [];
      }

      if (data != null)
      {
        $(data).each(function (){
          var currentData = this;
          var $elementToClone = $("."+cloneFieldId+"Template").clone();
          $elementToClone.removeClass(cloneFieldId+"Template");
          $elementToClone.removeClass('hidden');
          
          var $dataBindElements = $elementToClone.find("[data-bindCloned]");  
          
          $dataBindElements.each(function (){
            var $this = $(this);
            //get the name of the data-bind 
            var bindElementName = $this.attr("data-bindCloned");
            var objectValue="";
            try
            {
              //eval the 
              objectValue = eval("currentData."+bindElementName);   
            }
            catch (err)
            {
              objectValue="";
            }

            setElementValue ($this, objectValue);
            
          });
        $cloneContainer.append($elementToClone);
        });  

        //if the data length is only one hide the delete button
        //console.log ("DataLength:"+$(data).length);
        if($(data).length == 1)
        {
          $cloneContainer.find('.js-delete-button').addClass('hidden');
        }

        //enable the addButton
        $cloneContainer.parents('.js-add-button-container').find(".js-add-button").removeClass("Disable");
        $cloneContainer.parents('.js-add-button-container').find(".js-add-button").prop("disabled",false);

        //update indexes
        updateClonedIndexes($cloneContainer);
      }  
      else
      {
        //show one element empty
        console.log("show one element empty");
        var cloneFieldId = $cloneContainer.attr("data-clone");
        var $elementToClone = $("."+cloneFieldId+"Template").clone();
        $elementToClone.removeClass('hidden');
        //TODO hide the delete Button
        $cloneContainer.append($elementToClone);
      }
       
    });


    //bind the normal properties (not embeded models) of the object 
    var $elementsToBindProperty = jQuery("[data-bind]");  
    $elementsToBindProperty.each(function (){
      var $this = $(this);
      var dataBindPropertyFieldId = $this.attr("data-bind");
      var data = null;
      try
      {
        //get element of the json dataToClone
        data = eval("dataToClone."+dataBindPropertyFieldId);
      }
      catch (err)
      {
        data = [];
      }
      //console.log($this.attr("data-bind")+': '+JSON.stringify(data));
      setElementValue ($this, data);
    });
  }
  else
  {
    console.log('something is wrong please check the dataToClone');
  }
  
  //console.log('checking for callback:'+ callBack);
  if (callBack)
  {
    callBack(dataToClone);
  }
}*/

//this function updates the indexes of the cloned elements
//receive the container of the cloned elements (data-clone container)
function updateClonedIndexes($container)
{
  //Update the index of the elements 
  $container.find(".js-row").each(function( index ) {
    $(this).find('.js-index').attr('value',index); 
  });
}

function setElementValue($element,value)
{
 // console.log('element:'+$element.html() +" value:"+value);
  switch($element.prop("tagName"))
    {
      case("INPUT"):
        switch($element.prop("type"))
        {
          case("text"):
            $element.val(value);
          break;
          case("checkbox"):
            if (value)
            {
              $element.prop('checked',true);
            }
          break;
          case("radioButton"):
          break;
          default:
        }
      break;
      case("SELECT"):
        $element.find('option').each(function (){
          var $this = $(this);
          //console.log('option:'+$this.text() +"< vs value:"+value);
         // console.log('option:'+$this.html() +"< vs value:"+value);
         // console.log('option:'+$this.val() +"< vs value:"+value);
          if ($this.html() == value || ($this.val() == value))
          {
            $this.prop('selected', 'selected');
          }
        });
      break;
      case("CHECKBOX"):
        if (value)
        {
          $element.prop("checked",true);
        }
      break;
      case("SPAN"):
        $element.html(value);
      break;
      case("LABEL"):
      case("TEXTAREA"):
      case("DIV"):
        $element.html(value);
      break;
      default:
    }
}


//this function injects rows to the tables
function injectRows(data, tableSelector) {
   var $table = jQuery(tableSelector);
   $table.find("tbody").html("");
   var RowTemplate = [];
   $table.find("thead th").each(function () {
       var $currentCol = jQuery(this);
       RowTemplate.push($currentCol.attr("data-bind"));
   });

   jQuery(data.dataset).each(function () {
           
       var currentDataRow = this;
       var newRow = "";
       jQuery(RowTemplate).each(function () {
           var colTemplate = this;

           var newCol = colTemplate;
           var regex = /(\{(.*?)\})/g;

           var match = regex.exec(colTemplate);

           while (match != null) {
               var tdClass = '';
               if (match[2] == 'id') {
                   tdClass = 'class="checkBoxTd"';
               }

               if (match[2].endsWith(".*")) {
                   match[2] = match[2].replace(".*", "");
                   //newCol = newCol.replace(match[0],currentDataRow[match[2]].join(", "));
                   try {
                       newCol = newCol.replace(match[0], eval("currentDataRow." + match[2]).join(", "));
                   } catch (e) {
                       newCol = "";
                   }
               }
               else if (match[2].contains(".*")) {

                   var FirstPart = match[2].substring(0, match[2].indexOf(".*"));
                   var SecondPart = match[2].replace(FirstPart, "").replace(".*.", "");
                   var tempDataRow = {};
                   try {
                       tempDataRow = eval("currentDataRow." + FirstPart);
                   } catch (e) {
                       tempDataRow = {};
                   }
                   var tmpCol = "";

                   jQuery(tempDataRow).each(function () {
                       var tempDataRowItem = this;
                       try{
                        tmpCol += ", " + eval("tempDataRowItem." + SecondPart);
                       }catch(e){
                        newCol = "";
                       }
                   });
                   newCol = newCol.replace(match[0], tmpCol.substring(2));
               }
               else {
                   try{
                   newCol = newCol.replace(match[0], eval("currentDataRow." + match[2]));
                   }
                   catch(e){
                    newCol = "";
                   }
               }
               match = regex.exec(colTemplate);
           }

           if (!newCol || newCol=='null' || newCol == 'undefined'){
            newCol = "";
           }


           newRow += "<td " + tdClass + ">" + newCol + "</td>";
       });
       $table.find("tbody").append("<tr>" + newRow + "</tr>");
   });

  $table.trigger('update');

  if (typeof(onRowClickFunction) != 'undefined'){
    onRowClickFunction();
  }
}

/*
function updateTableWithData(data)
{
  $('.mainTable tbody tr').each(function(){
    $(this).remove();
  });
  
  var newElement = '';
   jQuery(data).each(function (index){
    var lastName = (!data[index].lastName)? "" : data[index].lastName;
    var firstName = (!data[index].firstName)? "" : data[index].firstName;
     newElement = '<tr>'+
      '<td class="checkBoxTd"><input type="checkbox" name="selectedContacts[]" class="SelectContact" value="'+data[index].id+'"/></td>'+
          '<td  id='+data[index].id+'>'+ lastName+'</td>'+
          '<td  id='+data[index].id+'>'+ firstName+'</td>'+
          '<td  id='+data[index].id+'>';
            
       if(data[index].specialties)
       {
         for (var j=0,  jj = data[index].specialties.length; j < jj; j++) 
         {
           newElement += data[index].specialties[j]+"</br>";
         }
       }
      newElement += '</td>'+ 
        '<td  id='+data[index].id+'>';
       
      if(data[index].affiliations)
       {
         for (var j=0,  jj = data[index].affiliations.length; j < jj; j++) 
         {
           newElement += data[index].affiliations[j].affiliationName+"</br>";
         }
       }
      
       newElement += '</td>'+
         '<td  id='+data[index].id+'>';
       
          
       if (data[index].addresses) {
         newElement += data[index].addresses[0].city+"</br>";
       }
          
         newElement += '</td>'+
          '<td  id='+data[index].id+'>';  
         if (data[index].addresses) {
           newElement += data[index].addresses[0].state+"</br>";
         }
         newElement += '</td>'+
          '<td  id='+data[index].id+'>';
         if (data[index].addresses) {
           if (data[index].addresses[0].country != "Please Select One") {
             newElement += data[index].addresses[0].country+"</br>";
           }
           else
         {
             newElement += " United States</br>";
         }
         }
         newElement += '</td>'+
          '</tr>';
     
     $('.mainTable tbody').append(newElement);
     //this indicates that the table is updated for the sorter by columns
     $(".mainTable").trigger("update"); 
  });
}
*/

function searchContactsByClientId()
{
  //if display has been set to a client,then we need to narrow the search to only those client's contacts
  var searchClientId = $("[name='client']").val();

  var data = "clientId="+searchClientId;
  jQuery.ajax({
    url:'/contacts/searchContactsByClientId',
    datatype:'json',
    type: 'GET',
    data:data,
    async:true,
    success:function(data){
      injectRows(data, '.mainTable');
       // var data = jQuery.parseJSON(xhr.responseText);
       // injectRows(data, ".mainTable" );
       // //set currentPage and recordCount
        updatePageNavigation(data);
    }
  });
}


//////////////////////////////////  BEHAVIOUR  SELCT ALL & SORTABLE  //////////////////////////////////
function setSelectionBehaviour()
{
    console.log ('set select Behaviour');
    // make sure all checkboxes and unchecked
    $('.selectElement').removeAttr("checked");
    $(".selectall").removeAttr("checked");

    //Handle select all checkbox
    $(".selectall").click(function () {
      //put all the elements of the table with the selection of the selectallElement
      var $selecteElement = $(this).parents('table').find('.selectElement');
      $selecteElement.prop('checked',$(this).prop('checked'));


      //delete button of the table section
      var $deleteElement = $(this).parents('.js-selection-container').find('.deleteSelectedBtn');
      if ( $(this).parents('.js-selection-container').find('.selectElement:checked').length > 0)
      {
        //enable the element
        $deleteElement.prop('disabled', false);
      }
      else
      {
        //disable the element
        $deleteElement.prop('disabled', true);
      }
    });
 
    // if all checkbox are selected, check the selectall checkbox
    // and viceversa
    $(".selectElement").click(function(){

      var $table = $(this).parents('table');

      if($table.find(".selectElement").length == $table.find(".selectElement:checked").length) {
          $table.find(".selectall").prop("checked", "checked");
      } else {
          $table.find(".selectall").removeAttr("checked");
      }

      //delete button of the table section
      var $deleteElement = $(this).parents('.js-selection-container').find('.deleteSelectedBtn');
      //check if there are elements check to enable the delete button 
      if ($table.find(".selectElement:checked").length > 0)
      {
        $deleteElement.prop('disabled', false);
      } else {
        $deleteElement.prop('disabled', true);
      }

    });

    //disable all the delete buttons 
    $('.deleteSelectedBtn').prop('disabled', true);
}


function SetupSortableTable(tableSelector) {
    // call the tablesorter plugin and apply the uitheme widget
    $(tableSelector).tablesorter({
        // this will apply the bootstrap theme if "uitheme" widget is included
        // the widgetOptions.uitheme is no longer required to be set
        theme: "bootstrap",
        widthFixed: true,
        headerTemplate: '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!
        // widget code contained in the jquery.tablesorter.widgets.js file
        // use the zebra stripe widget if you plan on hiding any rows (filter widget)
        widgets: ["uitheme", "zebra", 'cssStickyHeaders'],
        widgetOptions: {
            // using the default zebra striping class name, so it actually isn't included in the theme variable above
            // this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
            //zebra: ["even", "odd"],

            cssStickyHeaders_offset        : 0,
            cssStickyHeaders_addCaption    : true,
            cssStickyHeaders_attachTo      : '.table-sorter-wrapper',
            cssStickyHeaders_filteredToTop : true,
            cssStickyHeaders_zIndex        : 10,

            // reset filters button
            // filter_reset: ".reset",
            // scroller_height : 800,
            // scroller_barWidth : 17,
            // scroller_jumpToHeader: false,
            // scroller_idPrefix : 's_'
            // set the uitheme widget to use the bootstrap theme class names
            // this is no longer required, if theme is set
           // uitheme : "bootstrap"
        }
    });

  // $(tableSelector).tablesorterPager({container: $("#pager")}); 
}

////////////////////////////  DIALOGS  //////////////////////////////////

function initClientLinkDialog(url,successCallback,checkOnClose)
{
    console.log ("showing dialog ");
    var linkedToClient = false;

    var clientLinkWindow = $( "#dialog-clientLink" );

    clientLinkWindow.dialog({
      autoOpen: false,
      resizable: false,
      height:200,
      width: 350,
      modal: true,
      buttons: {
        "Link Now": function() {
          var clientList = $('#clientList');
          var data = {
              id: clientList.val()
            };  

          jQuery.ajax({
            url:url,
            datatype:'json',
            type: 'POST',
            async: false,
            data: data,
            success: successCallback           
          });

          linkedToClient = true;
          $( this ).dialog( "close" );
        },
        Cancel: function() {
            $( this ).dialog( "close" );
        }
      }, 
       close: function (){
          if (checkOnClose)
          {
            if (!linkedToClient)
            {
              goContactsList();
            }
            //reset flag, just in case
            linkedToClient = false;
            $( this ).dialog( "close" );
          }
       }
    });
    clientLinkWindow.dialog("open");
}

function initVenueLinkDialog(url,successCallback,checkOnClose)
{
    var venueLinkWindow = $( "#dialog-venueLink" );

    venueLinkWindow.dialog({
      autoOpen: false,
      resizable: false,
      height:150,
      width: 350,
      modal: true,
      buttons: {
        "Add Venue": function() {

          var venueList = $('#venueList');
          var data = {
              id: venueList.val()
            };

        jQuery.ajax({
          url:url,
          datatype:'json',
          type: 'POST',
          async: false,
          data: data,
          success: successCallback  
        });

        $( this ).dialog( "close" );

        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      }, 
       close: function (){
         checkOnClose;
       }
    });

    venueLinkWindow.dialog("open");

}


function initDeleteDialog(title, message, deleteUrl, deleteElement, callBack)
{
    var selectedIds = [];  
    //js-selection-container 
    var $selectionContainer = $(deleteElement).parents('.js-selection-container');
    //collect all checked ids first
    $selectionContainer.find('.selectElement:checked').each(function(){
        selectedIds.push($(this).val());
    });

    var data = {
        ids: selectedIds
    };

    //select all element
    var $selectAllElement = $selectionContainer.find('.selectall'); 

    var deleteWindow = $( "#dialog-confirm-delete" );

    deleteWindow.dialog({
      resizable: false,
      height:240,
      width:350,
      title:title,
      open: function() {
          $(this).html(message);
        },
      modal: true,
      buttons: {
        "Confirm": function() {
          $( this ).dialog( "close" );
          
            jQuery.ajax({
              url:deleteUrl,
              datatype:'json',
              type: 'POST',
              async: false,
              data: data,
              success: function (data){
                if (callBack)
                {
                  callBack(selectedIds);
                } else {
                  $mainTable = $selectionContainer.find('.mainTable');
                  $mainTable.find('tbody tr').each(function(){
                    for (var index in selectedIds) {
                        if ($(this).find('.selectElement').val() == selectedIds[index])
                        {
                            $(this).remove();
                        }
                    }
                  });

                  // //this indicates that the table is updated for the sorter by columns
                  $mainTable.trigger("update");
                }

                $selectAllElement.removeAttr("checked");  
                $(deleteElement).prop('disabled', true);
              }
            });

            
        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      }
    });
    deleteWindow.dialog("open");
}



/////////  Upload dialog /////////////
function initUploadFileDialog(title, uploadFunction, closeFunction)
{
  fileDisplayNameInput = $('#displayName');
  selectedFile = $('#selectedFile');
  uploadNotes = $('#notes');
  uploadLink = $('#link');

  jQuery("#selectFile").unbind("click");
  jQuery("#selectFile").click(function(){
    jQuery("#File").click();
  });

  allTextFields = $( [] ).add( fileDisplayNameInput ).add(selectedFile).add(uploadNotes).add(uploadLink);
  tips = $( ".validateTips" );

    var uploadWindow = $( "#uploadFile-form" );

   uploadWindow.dialog({
      autoOpen: false,
      resize: 'auto', // 866
      width: 'auto',  //567
      modal: true,
      title: title,
      buttons: {
        "Upload and Save": uploadFunction,
         Cancel: closeFunction
      },
    close: closeFunction
  });

  uploadWindow.dialog( "open" );
}


function updateTips( t ) {
  // tips
  //   .text( t )
  //   .addClass( "ui-state-highlight" );
  // setTimeout(function() {
  //   tips.removeClass( "ui-state-highlight" );
  //   tips.text('All form fields are required.');
  // }, 3000 );
  alert(t);
}

function checkLength( o, message, min, max ) {
  console.log('looking at: ' + o);
  if ( o.length > max || o.length < min ) {
    updateTips( message );
    return false;
  } else {
    return true;
  }
}


function onFileSelected(event)
{
  files = event.target.files;

  data = new FormData();
   
  $.each(files, function(key, value)
  {
    data.append(key, value);
  });

  data.append('contactId', uploadTargetId);

  var fileName;

  if (!uploadType || uploadType == 'file' || uploadType == 'photo')
  {
    //display selected file's name in dialog
    fileName = $('#File').val();

    //for IE.. clean up file name
    fileName = fileName.slice(fileName.lastIndexOf('\\') + 1);

    $('#selectedFile').val(fileName);
  } else {
     //display selected file's name in dialog
     fileName= $('#File2').val();

    //for IE.. clean up file name
    fileName = fileName.slice(fileName.lastIndexOf('\\') + 1);

    $('#selectedFileW9').val(fileName);
  }
}

function uploadFileFunction()
{
  var bValid = true;

  bValid = bValid && checkLength( selectedFile.val(), "Please select a file to upload.", 1, 150 );
  bValid = bValid && checkLength( fileDisplayNameInput.val(), "Please fill out the display name field.", 1, 150 );

    if ( bValid ) {
      //storing form data, so we can save it to the database, once file is uploaded to server
      uploadData = {
        displayName: fileDisplayNameInput.val(),
        notes: uploadNotes.val(),
        link: uploadLink.val()
      } 

      //upload file
    jQuery.ajax({
      url:'/uploadFile',
      datatype:'json',
      type: 'POST',
      async: false,
      processData: false, // Don't process the files
      contentType: false, // Set content type to false as jQuery will tell the server its a query string request
      data: data,
      success: fileUploadSuccessFunction
    });

    $( this ).dialog( "close" );
  }
}

function uploadFileDialogCloseFunction () {
  //reset every field
  if (allTextFields)
  {
    allTextFields.val( "" );
  }

  if (allSelectFields)
  {
    allSelectFields.prop('selectedIndex',0);
  }

  $( this ).dialog( "close" );
};


//////////////  Display Uploaded File //////////////////
function displayFile(filePath, displayName)
{
  var fileExt = filePath.substring(filePath.lastIndexOf('.') + 1);

  if (fileExt == 'mp4' && navigator.userAgent.match(/(iPod|iPhone|iPad)/))
  {
     jQuery('#content').load('/displayFile #AjaxContent', {data: filePath, displayName: displayName, os:'apple'},function (data){
       if(data == 'false')
        {
           window.location = "/expired";
        }
     });
   } else {
      window.open('/displayFile?data=' + filePath)
   }
}


//////////////  Display Uploaded Photo //////////////////
function displayPhoto(filePath, $photoContainer)
{
  jQuery.ajax({
      url:'/setFileToDisplay',
      datatype:'json',
      type: 'POST',
      async: false,
      data: { 'filePath': '/storage/userPhotos/' + filePath},
      success: function (){
        jQuery.ajax({
          url:'/displayPhoto',
          datatype:'json',
          type: 'POST',
          async: false,
          success: function (fileData, fileEncoding){
            $photoContainer.attr("src", 'data:image/' + fileEncoding + ';base64,' + fileData.file);
          }
        });
      }
    });
}

