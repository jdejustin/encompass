function UploadExcel() {
}


function fillSideA($thisDifffDiv, info) {
    jQuery($thisDifffDiv).find(".SideA .LastName").val(info.a["Last Name"]);
    jQuery($thisDifffDiv).find(".SideA .MiddleName").val(info.a["Middle"]);
    jQuery($thisDifffDiv).find(".SideA .FirstName").val(info.a["First Name"]);
    jQuery($thisDifffDiv).find(".SideA .Suffix").val(info.a["Suffix"]);
    jQuery($thisDifffDiv).find(".SideA .Degrees").val(info.a["Degrees"]);
    jQuery($thisDifffDiv).find(".SideA .Salutation").val(info.a["Saluation"]);
    jQuery($thisDifffDiv).find(".SideA .Email1").val(info.a["Preferred Email Address"]);
    jQuery($thisDifffDiv).find(".SideA .Email2").val(info.a["Seconday Email Address"]);
    jQuery($thisDifffDiv).find(".SideA .OfficePhone").val(info.a["Office Phone"].replace(/-.*$/, ""));
    jQuery($thisDifffDiv).find(".SideA .FaxPhone").val(info.a["Fax"].replace(/-.*$/, ""));
    jQuery($thisDifffDiv).find(".SideA .HomePhone").val(info.a["Home"].replace(/-.*$/, ""));
    jQuery($thisDifffDiv).find(".SideA .CellPhone").val(info.a["Cell"].replace(/-.*$/, ""));
    jQuery($thisDifffDiv).find(".SideA .OtherContactPhone").val(info.a["Other Contact"].replace(/-.*$/, ""));
    jQuery($thisDifffDiv).find(".SideA .Specialty1").val(info.a["Specialty 1"]);
    jQuery($thisDifffDiv).find(".SideA .Specialty2").val(info.a["Specialty 2"]);
    jQuery($thisDifffDiv).find(".SideA .Specialty3").val(info.a["Specialty 3"]);
    jQuery($thisDifffDiv).find(".SideA .therapeuticInterests").val(info.a["Therapeutic Area/Area(s) of Interest"]);
    jQuery($thisDifffDiv).find(".SideA .notes").val(info.a["Notes"]);
    jQuery($thisDifffDiv).find(".SideA .EmergencyContactName").val(info.a["Emergency Contact Name"]);
    jQuery($thisDifffDiv).find(".SideA .EmergencyContactPhone").val(info.a["Emergency Contact #"].replace(/-.*$/, ""));
    jQuery($thisDifffDiv).find(".SideA .Affiliation").val(info.a["Affiliation"]);
    jQuery($thisDifffDiv).find(".SideA .AffiliationTitle").val(info.a["Title"]);
    jQuery($thisDifffDiv).find(".SideA .AffiliationCity").val(info.a["City, State of Affiliation/Institution"]);
    jQuery($thisDifffDiv).find(".SideA .AditionalAffiliation").val(info.a["Additional Affiliation"]);
    jQuery($thisDifffDiv).find(".SideA .AditionalAffiliationTitle").val(info.a["Additional Title"]);
    jQuery($thisDifffDiv).find(".SideA .AditionalAffiliationCity").val(info.a["City, State of Additional Affiliation/Institution"]);
    jQuery($thisDifffDiv).find(".SideA .AddressType").val(info.a["Address Type"]);
    jQuery($thisDifffDiv).find(".SideA .Address1").val(info.a["Address 1"]);
    jQuery($thisDifffDiv).find(".SideA .Address2").val(info.a["Address 2"]);
    jQuery($thisDifffDiv).find(".SideA .City").val(info.a["City"]);
    jQuery($thisDifffDiv).find(".SideA .State").val(info.a["State (abbreviated)"]);
    jQuery($thisDifffDiv).find(".SideA .Zip").val(info.a["Zip"]);
    jQuery($thisDifffDiv).find(".SideA .country").val(info.a["Country"]);
    jQuery($thisDifffDiv).find(".SideA .TsaName").val(info.a["TSA: Name on Photo"]);
    jQuery($thisDifffDiv).find(".SideA .TsaGender").val(info.a["TSA: Gender"]);
    jQuery($thisDifffDiv).find(".SideA .TsaDOB").val(info.a["TSA: Date of Birth (DOB"]);
    jQuery($thisDifffDiv).find(".SideA .dietaryRestrictions").val(info.a["Dietary Restrictions"]);
    jQuery($thisDifffDiv).find(".SideA .specialNeeds").val(info.a["Special Needs"]);
    jQuery($thisDifffDiv).find(".SideA .FrequentFlyerAirline").val(info.a["Frequent Flyer Airline"]);
    jQuery($thisDifffDiv).find(".SideA .FrequentFlyerNum").val(info.a["Frequent Flyer #"]);
    
    jQuery($thisDifffDiv).find(".SideA .medicalLicenses1").val(info.a["Medical License #1"]);
    jQuery($thisDifffDiv).find(".SideA .medicalLicenseState1").val(info.a["Licensing State #1"]);
    
    jQuery($thisDifffDiv).find(".SideA .medicalLicenses2").val(info.a["Medical License #2"]);
    jQuery($thisDifffDiv).find(".SideA .medicalLicenseState2").val(info.a["Licensing State #2"]);
    
    jQuery($thisDifffDiv).find(".SideA .npi").val(info.a["NPI # (10-digit)"]);
    jQuery($thisDifffDiv).find(".SideA .dea").val(info.a["DEA #"]);
    jQuery($thisDifffDiv).find(".SideA .officeStaffContact").val(info.a["Office Staff Contact"]);
    jQuery($thisDifffDiv).find(".SideA .officeStaffContactEmail").val(info.a["Office Staff Email"]);
    jQuery($thisDifffDiv).find(".SideA .officeStaffContactPhone").val(info.a["Office Staff Tel #"]);
}




function CompareValues(ElementA, ElementB, MongoValue) {
    if (ElementA.val() != MongoValue) {
        ElementA.addClass("Diferent");
        ElementB.addClass("Diferent");
    }
}


function fillSideB($thisDifffDiv, info) {
    var $SideBExisting = jQuery("#DiffTemplate .SideB .Existing");
    var $SideBEMailAdress = jQuery("#DiffTemplate .SideB .EMailAdress");
    var $SideBPhoneNumbers = jQuery("#DiffTemplate .SideB .PhoneNumbers");
    var $SideBAffiliations = jQuery("#DiffTemplate .SideB .Affiliations");
    var $SideBAddress = jQuery("#DiffTemplate .SideB .Address");

    var $sideB = jQuery($thisDifffDiv).find(".SideB");
    var i = 0;
    jQuery(info.b).each(function () {
        var currentBData = this;
        var $existing;
        if (i == 0) {
            $existing = jQuery($sideB).find(".Existing");
        } else {
            $existing = $SideBExisting.clone();
            $sideB.append($existing);
        }

        $existing.find(".LastName").val(this["lastName"]);
        $existing.find(".MiddleName").val(this["middleName"]);
        $existing.find(".FirstName").val(this["firstName"]);

        $existing.find(".Suffix").val(this["suffix"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .Suffix"), $existing.find(".Suffix"), this["suffix"]);
        $existing.find(".Degrees").val(this["degrees"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .Degrees"), $existing.find(".Degrees"), this["degrees"]);
        $existing.find(".Salutation").val(this["salutation"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .Salutation"), $existing.find(".Salutation"), this["salutation"]);


        $existing.find(".therapeuticInterests").val(this["therapeuticInterests"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .therapeuticInterests"), $existing.find(".therapeuticInterests"), this["therapeuticInterests"]);
        $existing.find(".notes").val(this["notes"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .notes"), $existing.find(".notes"), this["notes"]);

        $existing.find(".TsaName").val(this["tsaName"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .TsaName"), $existing.find(".TsaName"), this["tsaName"]);

        $existing.find(".TsaGender").val(this["tsaGender"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .TsaGender"), $existing.find(".TsaGender"), this["tsaGender"]);

        $existing.find(".TsaDOB").val(this["tsaDOB"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .TsaDOB"), $existing.find(".TsaDOB"), this["tsaDOB"]);


        $existing.find(".dietaryRestrictions").val(this["dietaryRestrictions"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .dietaryRestrictions"), $existing.find(".dietaryRestrictions"), this["dietaryRestrictions"]);

        $existing.find(".specialNeeds").val(this["specialNeeds"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .specialNeeds"), $existing.find(".specialNeeds"), this["specialNeeds"]);


        if (this.airlines && this.airlines[0]) {
            $existing.find(".FrequentFlyerAirline").val(this.airlines[0]["airlineName"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .FrequentFlyerAirline"), $existing.find(".FrequentFlyerAirline"), this.airlines[0]["airlineName"]);

            $existing.find(".FrequentFlyerNum").val(this.airlines[0]["ffNumber"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .FrequentFlyerNum"), $existing.find(".FrequentFlyerNum"), this.airlines[0]["ffNumber"]);
        }



        if (this.medicalLicenses && this.medicalLicenses[0]) {
            $existing.find(".medicalLicenses1").val(this.medicalLicenses[0]["number"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .medicalLicenses1"), $existing.find(".medicalLicenses1"), this.medicalLicenses[0]["number"]);

            $existing.find(".medicalLicenseState1").val(this.medicalLicenses[0]["state"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .medicalLicenseState1"), $existing.find(".medicalLicenseState1"), this.medicalLicenses[0]["state"]);
        }
        if (this.medicalLicenses && this.medicalLicenses[1]) {
            $existing.find(".medicalLicenses2").val(this.medicalLicenses[1]["number"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .medicalLicenses2"), $existing.find(".medicalLicenses2"), this.medicalLicenses[1]["number"]);

            $existing.find(".medicalLicenseState2").val(this.medicalLicenses[1]["state"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .medicalLicenseState2"), $existing.find(".medicalLicenseState2"), this.medicalLicenses[1]["state"]);
        }


        $existing.find(".npi").val(this["npi"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .npi"), $existing.find(".npi"), this["npi"]);

        $existing.find(".dea").val(this["dea"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .dea"), $existing.find(".dea"), this["dea"]);

        var specialtiesCount = 0;
        $existing.find(".Specialty").each(function () {
            var $this = jQuery(this);
            if (currentBData.specialties) {
                $this.val(currentBData.specialties[specialtiesCount]);
                CompareValues(jQuery($thisDifffDiv).find(".SideA .Specialty" + (specialtiesCount + 1)), $this, currentBData.specialties[specialtiesCount]);
            }
            specialtiesCount++;
        });


        var emergencyContact = this.emergencyContacts[0];
        if (emergencyContact) {
            $existing.find(".EmergencyContactName").val(emergencyContact.fullName);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .EmergencyContactName"), $existing.find(".EmergencyContactName"), emergencyContact.fullName);

            var emergencyContactPhone = emergencyContact.phoneNumbers[0];
            if (emergencyContactPhone) {
                $existing.find(".EmergencyContactPhone").val(emergencyContactPhone.number);
                CompareValues(jQuery($thisDifffDiv).find(".SideA .EmergencyContactPhone"), $existing.find(".EmergencyContactPhone"), emergencyContact.number);
            }
        }


        $existing.find("input[name=id]").val(this["id"]);

        var emailsCount = 0;
        jQuery(this.emailAddresses).each(function () {
            var $newEmail;
            if (emailsCount == 0) {
                $newEmail = $existing.find(".EMailAdress");
            } else {
                $newEmail = $SideBEMailAdress.clone();
                $existing.find("#ExistingContact").append($newEmail);
            }
            $newEmail.find(".Email").val(this["email"]);
            if (this["prefered"]) {
                $newEmail.find(".PreferredEmail").attr("checked", "checked");
            }
            CompareValues(jQuery($thisDifffDiv).find(".SideA .Email" + (emailsCount + 1)), $newEmail.find(".Email"), this["email"]);

            emailsCount++;
        });

        var phoneCount = 0;
        jQuery(this.phoneNumbers).each(function () {
            var $newPhone;
            if (phoneCount == 0) {
                $newPhone = $existing.find(".PhoneNumbers");
            } else {
                $newPhone = $SideBPhoneNumbers.clone();
                $existing.find("#ExistingContact").append($newPhone);
            }
            $newPhone.find(".PhoneNumber").val(this["number"]);

            CompareValues(jQuery($thisDifffDiv).find(".SideA ." + this["phoneType"] + "Phone"), $newPhone.find(".PhoneNumber"), this["number"]);


            $newPhone.find(".idPhoneNumbers").val(this["id"]);
            $newPhone.find(".PhoneType").text(this["phoneType"]);
            phoneCount++;
        });



        if (this.officeStaffContact) {
            $existing.find(".officeStaffContact").val(this.officeStaffContact["fullName"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .officeStaffContact"), $existing.find(".officeStaffContact"), this["officeStaffContact"]);

            $existing.find(".officeStaffContactEmail").val(this.officeStaffContact["email"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .officeStaffContactEmail"), $existing.find(".officeStaffContactEmail"), this["officeStaffContactEmail"]);

            if (this.officeStaffContact.phoneNumbers && this.officeStaffContact.phoneNumbers[0]) {
                $existing.find(".officeStaffContactPhone").val(this.officeStaffContact.phoneNumbers[0]["number"]);
                CompareValues(jQuery($thisDifffDiv).find(".SideA .officeStaffContactPhone"), $existing.find(".officeStaffContactPhone"), this.officeStaffContact.phoneNumbers[0]["number"]);
            }
        }

        var affiliationCount = 0;
        jQuery(this.affiliations).each(function () {
            var $newaffiliation;
            if (affiliationCount == 0) {
                $newaffiliation = $existing.find(".Affiliations");
            } else {
                $newaffiliation = $SideBAffiliations.clone();
                $existing.find("#ExistingAffiliations").append($newaffiliation);
            }
            $newaffiliation.find(".Affiliation").val(this["affiliationName"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .Affiliation"), $newaffiliation.find(".Affiliation"), this["affiliationName"]);

            $newaffiliation.find(".AffiliationTitle").val(this["title"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .AffiliationTitle"), $newaffiliation.find(".AffiliationTitle"), this["title"]);

            $newaffiliation.find(".AffiliationCity").val(this["city"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .AffiliationCity"), $newaffiliation.find(".AffiliationCity"), this["city"]);

            affiliationCount++;
        });


        var addressCount = 0;
        jQuery(this.addresses).each(function () {
            var $newaddress;
            if (addressCount == 0) {
                $newaddress = $existing.find(".Address");
            } else {
                $newaddress = $SideBAddress.clone();
                $existing.find("#ExistingAddress").append($newaddress);
            }
            $newaddress.find(".AddressType").val(this["addressType"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .AddressType"), $newaddress.find(".AddressType"), this["addressType"]);

            $newaddress.find(".Address1").val(this["address1"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .Address1"), $newaddress.find(".Address1"), this["address1"]);

            $newaddress.find(".Address2").val(this["address2"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .Address2"), $newaddress.find(".Address2"), this["address2"]);

            $newaddress.find(".City").val(this["city"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .City"), $newaddress.find(".City"), this["city"]);

            $newaddress.find(".State").val(this["state"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .State"), $newaddress.find(".State"), this["state"]);

            $newaddress.find(".Zip").val(this["postalCode"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .Zip"), $newaddress.find(".Zip"), this["postalCode"]);

            $newaddress.find(".country").val(this["country"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .country"), $newaddress.find(".country"), this["country"]);
            addressCount++;
        });


    });

}

function UploadExcelOnComplete(json) {
    var data = jQuery.parseJSON(json);
    var $DiffTemplate = jQuery("#DiffTemplate");
    jQuery(".UploadContainer").hide();
    jQuery(".DiffMain").show();

    var i = 0;
    jQuery(data.diff).each(function () {
        var info = this;
        var divId = "diff_" + i; i++;
        var $thisDifffDiv = $DiffTemplate.clone();
        $thisDifffDiv.attr("id", divId);
        fillSideA($thisDifffDiv, info);
        fillSideB($thisDifffDiv, info);

        $thisDifffDiv.find(".Save").click(function () {
            var $activeForm = jQuery(".Active .diffForm");
            $activeForm.ajaxForm({
                beforeSend: function () {
                },
                success: function () {
                },
                complete: function (xhr) {
                    //alert(xhr.responseText);
                    alert("Contact saved");
                }
            });
            $activeForm.submit();
        });

        jQuery("#diff").append($thisDifffDiv);
    });

    jQuery(".SideATabs").tabs({
        activate: function (event, ui) {
            var tabIndex = ui.newTab.parent().find("li").index(ui.newTab);
            var $parent = ui.newTab.parents(".DiffContainer").eq(0);
            $parent.find(".SideBTabs .ui-tabs-anchor").eq(tabIndex).click();
        }
    });
    jQuery(".SideBTabs").tabs({

        activate: function (event, ui) {
            var tabIndex = ui.newTab.parent().find("li").index(ui.newTab);
            var $parent = ui.newTab.parents(".DiffContainer").eq(0);
            $parent.find(".SideATabs .ui-tabs-anchor").eq(tabIndex).click();
        }

    });

    jQuery("#NextDiff").click();
}


jQuery(document).ready(function () {
    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status');
    jQuery(".DiffMain").hide();
    jQuery(".UploadContainer").show();


    $('#UploadExcel').ajaxForm({
        beforeSend: function () {
            jQuery("#diff").html("");
            status.empty();
            var percentVal = '0%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        uploadProgress: function (event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        success: function () {
            var percentVal = '100%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        complete: function (xhr) {
            UploadExcelOnComplete(xhr.responseText);
        }
    });



    jQuery("#NextDiff").click(function () {
        var $active = jQuery("#diff .Active");

        if ($active.length == 0) {
            $active = jQuery("#diff .DiffContainer:first");
            $active.show();
            $active.toggleClass("Active");
        } else {
            $active.toggleClass("Active");
            $active.hide();

            var $nextDiff = $active.next();

            if ($nextDiff.length == 0) {
                $nextDiff = jQuery("#diff .DiffContainer:first");
            }

            $nextDiff.toggleClass("Active");
            $nextDiff.show();
        }
    });

    jQuery("#PrevDiff").click(function () {
        var $active = jQuery("#diff .Active");

        if ($active.length == 0) {
            $active = jQuery("#diff .DiffContainer:last");
            $active.show();
            $active.toggleClass("Active");
        } else {
            $active.toggleClass("Active");
            $active.hide();

            var $nextDiff = $active.prev();

            if ($nextDiff.length == 0) {
                $nextDiff = jQuery("#diff .DiffContainer:last");
            }

            $nextDiff.toggleClass("Active");
            $nextDiff.show();
        }
    });


});