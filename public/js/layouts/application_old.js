$(function() {
	$.ajaxSetup ({
    	// Disable caching of AJAX responses
	    cache: false
	});

	// window.lastUrl = '';

	// $(window).on('hashchange', function() {

	// 	//only execute if there's a hash tag on url
	//     var url='text/' + window.location.hash + '.html';

	//     console.log("url: " + url);

	//     if (url.indexOf('#') == -1)
	//     {
	//     	url = window.lastUrl;
	//     }

	//     if (window.lastUrl != url) {
	//         window.lastUrl = url;
	//         var firstChar = url.substr(url.indexOf('#') + 1, 1).toUpperCase();
	//         var finalUrl = 'goTo' + firstChar +  url.substring(url.indexOf('#') + 2, url.lastIndexOf('.'));

	//         this[finalUrl]();
	//     }
	// });

	// window.onbeforeunload = function() {
	// 	console.log ("bye!!!");
 //  	}
});

function getUserPhoto(filePath, targetId)
{
	if (filePath)
	{
		displayPhoto(filePath, $('#' + targetId));
	} else {
		$('#'+ targetId).attr('src', '/img/profile-no-photo.gif');
	}
}

function loadUserProfilePage(id, userRole, photoFilePath, targetId)
{
	if (userRole == 'Super Admin' || userRole == "Admin")
	{
		jQuery("#content").load('/users/' + id + ' #AjaxContent',function (data){
			if(data == 'false')
		   {
		      window.location = "/expired";
		   }
		   else
		   {
				getUserPhoto(photoFilePath, targetId);
		   }
		});
	}
}

//need to have this function here since we call it from the header in application.html and also from the edit button in the User show page
function editUser(id, photoPath) {
	jQuery("#content").load('/users/' + id + '/edit' + ' #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
		   initEditUser();
			getUserPhoto(photoPath, 'photoThumbImgForm');
	   }
	});
}


function updateNavigation(section)
{
	$('#clientsButton').removeClass('active');
	$('#contactsButton').removeClass('active');
	$('#reportsButton').removeClass('active');
	$('#servicesButton').removeClass('active');
	$('#programTypesButton').removeClass('active');
	$('#usersButton').removeClass('active');
	$('#settingsButton').removeClass('active');

	$('#' + section + 'Button').addClass('active');

}


/////////////////   CLIENTS  //////////////////

function goToClientsHome()
{
	$('html,body').css('cursor','wait');

	if (typeof(clientIndexLoaded) == 'undefined')
	{
		include('/js/global/global.js', {dom: true});
		include('/js/utils/utils.js', {dom: true});
		include('/js/clients/clients.js', {dom: true});
		include('/js/clients/clientsIndex.js', {dom: true});
		include('/js/clients/show.js', {dom: true});
		include('/js/clients/clientStep1.js', {dom: true});
		include('/js/clients/clientStep2.js', {dom: true});
		include('/js/clients/clientStep3.js', {dom: true});
		include('/js/clients/contactClientStep.js', {dom: true});
		include('/js/clients/clientResources.js', {dom: true});

		include('/js/programs/programs.js', {dom: true});
		include('/js/programs/programsIndex.js', {dom: true});
		include('/js/programs/show.js', {dom: true});
		include('/js/programs/programStep1.js', {dom: true});
		include('/js/programs/programResources.js', {dom: true});
		include('/js/programs/programSpeakers.js', {dom: true});
		include('/js/programs/programReps.js', {dom: true});

		include('/js/communications/communications.js', {dom: true});
		include('/js/communications/communicationsTemplates.js', {dom: true});
		include('/js/communications/communicationsDeliveries.js', {dom: true});
		include('/js/communications/editDeliveryFiles.js', {dom: true});

		include('/js/contracts/contracts.js', {dom: true});
		include('/js/contracts/contractsIndex.js', {dom: true});
		include('/js/contracts/show.js', {dom: true});
		include('/js/contracts/contractStep1.js', {dom: true});
		include('/js/contracts/contractStep2.js', {dom: true});
		include('/js/contracts/programContractStep1.js', {dom: true});
		
		include('/js/venues/venues.js', {dom: true});
		include('/js/venues/venuesIndex.js', {dom: true});
		include('/js/venues/show.js', {dom: true});
		include('/js/venues/venueStep1.js', {dom: true});
		include('/js/venues/venueStep2.js', {dom: true});
		include('/js/venues/venueStep3.js', {dom: true});
		include('/js/venues/venueStep4.js', {dom: true});
		include('/js/venues/venueRoomStep.js', {dom: true});
		
		include('/js/reps/reps.js', {dom: true});
		include('/js/reps/repsIndex.js', {dom: true});
		include('/js/reps/show.js', {dom: true});
		include('/js/reps/repStep1.js', {dom: true});
		include('/js/reps/repStep2.js', {dom: true});
		include('/js/reps/importRepDiff.js', {dom: true});
		
		include('/js/events/events.js', {dom: true});
		include('/js/events/eventsIndex.js', {dom: true});
		include('/js/events/show.js', {dom: true});
		include('/js/events/eventStep1.js', {dom: true});
		include('/js/events/eventStep2.js', {dom: true});
		include('/js/events/eventStep3.js', {dom: true});
		include('/js/events/eventStep4.js', {dom: true});
		include('/js/events/eventStep5.js', {dom: true});
		include('/js/events/eventStep6.js', {dom: true});
		include('/js/events/notes.js', {dom: true});
		include('/js/events/noteStep1.js', {dom: true});
		include('/js/events/finances.js', {dom: true});
		
		include('/js/attendees/attendees.js', {dom: true});
		include('/js/attendees/attendeesIndex.js', {dom: true});
		include('/js/attendees/show.js', {dom: true});
		include('/js/attendees/attendeeStep1.js', {dom: true});
		
		include('/js/contacts/contacts.js', {dom: true});
		include('/js/contacts/contactStep2.js', {dom: true});
		include('/js/contacts/contactStep3.js', {dom: true});
		include('/js/contacts/contactStep4.js', {dom: true});
		include('/js/contacts/contactStep5.js', {dom: true});
	}

		initClientsIndex();
		$('html,body').css('cursor','default');
     }

   function onGotoClientsClick()
	{
	jQuery('#content').load('/clients #AjaxContent', function (data){
		if(data == 'false')
		{
			window.location = "/expired";
		}
		else
		{
			initClientsIndex();

			$('html,body').css('cursor','default');
		}		

		});
	}

function goToContactsHome()
{
	$('html,body').css('cursor','wait');

	if (typeof(contactIndexLoaded) == 'undefined')
	{
		include('/js/contacts/contacts.js', {dom: true});
		include('/js/contacts/contactsIndex.js', {dom: true});
		include('/js/contacts/show.js', {dom: true});
		include('/js/contacts/contactStep1.js', {dom: true});
		include('/js/contacts/contactStep2.js', {dom: true});
		include('/js/contacts/contactStep3.js', {dom: true});
		include('/js/contacts/contactStep4.js', {dom: true});
		include('/js/contacts/contactStep5.js', {dom: true});
		include('/js/contacts/importContactsDiff.js', {dom: true});
		
		include('/js/export/exportContacts.js', {dom: true});
	}

	jQuery('#content').load('/contacts #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initContactIndex();
	   	$('html,body').css('cursor','default');
	   }
	});
}


function goToReports()
{
	$('html,body').css('cursor','wait');

	if (typeof(reportsIndexLoaded) == 'undefined')
	{
		include('/js/reports/reports.js', {dom: true});
	}

	jQuery('#content').load('/reports #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initReports();
	   	$('html,body').css('cursor','default');
	   }
	});
}

function goToSettings()
{
	$('html,body').css('cursor','wait');

	if (typeof(settingsIndexLoaded) == 'undefined')
	{
		include('/js/settings/settings.js', {dom: true});
	}

	jQuery('#content').load('/settings #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initSettings();
	   	$('html,body').css('cursor','default');
	   }
	});
}



function goToClientDetails(clientId)
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/clients/' + clientId + ' #AjaxContent',function (data){
		if(data == 'false')
		{
			window.location = "/expired";
		}
		else
		{
			initClientsShow();
			$('html,body').css('cursor','default');
		}
	});
}

function goToClientPrograms()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/programs #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	    	initProgramsIndex();
	    	$('html,body').css('cursor','default');
	   }
	});

	//cleanUp();
}

function cleanUp()
{
	jQuery.ajax({
		url:'/clients/cleanUp',
		datatype:'json',
		type: 'POST',
		async: false,
		data: {}
	});
}


function goToClientEvents()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/events #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
    		//initContactIndex();
    		$('html,body').css('cursor','default');
	   }
	});
}

function goToClientContracts()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/contracts/index #AjaxContent', {targetModel: 'clientContracts'}, function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	    	initContractsIndex();
	    	$('html,body').css('cursor','default');
	   }
	});
}

function goToClientVenues()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/venues #AjaxContent',function (data){
   		if(data == 'false')
	   	{
	      	window.location = "/expired";
	   	}
	   	else
	   	{
			initVenuesIndex();
			$('html,body').css('cursor','default');
	   	}
	});
}

function goToClientGuidelines()
{
	$('html,body').css('cursor','wait');

	if (typeof(guidelinesLoaded) == 'undefined')
	{
		include('/js/clients/guidelinesIndex.js', {dom: true});
	}

	jQuery('#content').load('/clients/guidelines #AjaxContent',function (data){
		if(data == 'false')
		{
			window.location = "/expired";
		}
		else
		{
    		initGuidelinesIndex();
    		$('html,body').css('cursor','default');
		}
	});
}

function goToClientFieldForce()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/reps #AjaxContent',function (data){
		if(data == 'false')
		{
			window.location = "/expired";
		}
		else
		{
	    	initRepsIndex();
	    	$('html,body').css('cursor','default');
		}
	});
}

function goToClientResources()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/clients/resources #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
		   initClientResources();
	    	$('html,body').css('cursor','default');
	   }
	});
}


/////////////////   PROGRAMS  //////////////////

function goToProgramDetails(programId)
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/programs/' + programId + ' #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
		   initProgramShow();
			$('html,body').css('cursor','default');
	   }
	});
}


function goToProgramEvents()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/events #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	    	initEventsIndex();
	    	$('html,body').css('cursor','default');
	   }
	});
}

function goToProgramSpeakers()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/programs/speakers #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	    	initProgramSpeakers();
	    	$('html,body').css('cursor','default');
	   }
	});
}



function goToProgramFieldForce()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/programs/reps #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
		   initProgramReps();
	    	$('html,body').css('cursor','default');
	   }
	});
}

function goToProgramResources()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/programs/resources #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
		   initProgramResources();
	    	$('html,body').css('cursor','default');
	   }
	});
}


/////////////////   COMMUNICATIONS  //////////////////

function goToTemplateCommunications()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/communications/communicationTemplate #AjaxContent',function (data){
      if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
    		initCommunicationsTemplate();
    		$('html,body').css('cursor','default');
	   }
	});
}

function goToDeliveryCommunications()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/communications/communicationDelivery #AjaxContent',function (data){
    	if(data == 'false')
		{
		   window.location = "/expired";
		}
		else
		{
	    	initCommunicationsDelivery();
	    	$('html,body').css('cursor','default');
		}
	});
}



/////////////////   EVENTS  //////////////////
function goToEventDetails(eventId)
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/events/' + eventId + ' #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initEventsShow();
			$('html,body').css('cursor','default');
	   }
	});
}

function goToEventPRF(eventId)
{
	$('html,body').css('cursor','wait');

	var data = new Object();

	jQuery.ajax({
		url:'/events/prf',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(){
			jQuery('#content').load('/events/' + eventId + ' #AjaxContent', function (data){
				if(data == 'false')
			   {
			      window.location = "/expired";
			   }
			   else
			   {
					$('html,body').css('cursor','default');
			   }
			});
		}
	});
}

function goToEventAttendees()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/attendees #AjaxContent',function(){
    	initAttendeesIndex();
    	$('html,body').css('cursor','default');
	});
}


function goToEventFinances()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/events/finances #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	    	initEventFinances();
	    	$('html,body').css('cursor','default');
	   }
	});
}

function goToEventNotes()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/events/notes #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	    	initEventNotes();
	    	$('html,body').css('cursor','default');
	   }
	});
}


/////////////////   REP PORTAL  //////////////////
function goToRepProgramDetails ()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/repProgramDetails #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
		   initRepProgramDetails();
			$('html,body').css('cursor','default');
	   }
	});
}

function goToRepDetails ()
{
	$('html,body').css('cursor','wait');

	jQuery("#content").load('/repDetails #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
		   initRepsShow();
			$('html,body').css('cursor','default');
	   }
	});
}

function goToRepResources ()
{
	$('html,body').css('cursor','wait');

	 jQuery('#content').load('/repResources #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	    	initRepResources();
	    	$('html,body').css('cursor','default');
	   }
	});
}


/////////////////   MANAGER PORTAL  //////////////////
function goToManagerProgramDetails ()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/managerProgramDetails #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initManagerProgramDetails();
			$('html,body').css('cursor','default');
	   }
	});
}

function goToManagerDetails ()
{
	$('html,body').css('cursor','wait');

	jQuery("#content").load('/managerDetails #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initRepsShow();
			$('html,body').css('cursor','default');
	   }
	});
}

function goToManagerResources ()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/managerResources #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	    	initManagerResources();
	    	$('html,body').css('cursor','default');
	   }
	});
}


/////////////////   CONTACT PORTAL  //////////////////
function goToContactProgramDetails ()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/contactProgramDetails #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
		   initContactProgramDetails();
			$('html,body').css('cursor','default');
	   }
	});
}

function goToContactDetails ()
{
	$('html,body').css('cursor','wait');

	jQuery("#content").load('/contactDetails #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initPortalContactDetails();
			$('html,body').css('cursor','default');
	   }
	});
}

function goToContactResources ()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/contactResources #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
		   initContactResources();
	    	$('html,body').css('cursor','default');
	   }
	});
}


/////////////////   CLIENT PORTAL  //////////////////
function goToClientProgramDetails ()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/clientProgramDetails #AjaxContent',function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
		   initClientProgramDetails();
			$('html,body').css('cursor','default');
	   }
	});
}

function goToClientDetailsPortal ()
{
	$('html,body').css('cursor','wait');

	jQuery("#content").load('/clientDetails #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initClientsShow();
			$('html,body').css('cursor','default');
	   }
	});
}

function goToClientResourcesPortal ()
{
	$('html,body').css('cursor','wait');

	jQuery('#content').load('/clientResources #AjaxContent',function (data){
    	if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	    	initClientResources();
	    	$('html,body').css('cursor','default');
	   }
	});
}


if (!String.prototype.endsWith) {
    String.prototype.endsWith = function (suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
}

if (!String.prototype.contains) {
    String.prototype.contains = function () {
        return String.prototype.indexOf.apply(this, arguments) !== -1;
    };
}
