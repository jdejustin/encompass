var activateAccountFlag = false;

jQuery(document).ready(function () {
	jQuery("#ForgotPassword").click(function (event) {
            event.preventDefault();
            forgotPassword();
        });
        jQuery("#ActivateAccount").click(function (event) {
        	event.preventDefault();
        	activateAccount();
        });
        jQuery("#BackToLogin").click(function (event) {
        	event.preventDefault();
        	backToLogin();
        });
        
      if (activateAccountFlag)
	  {
    	  activateAccount();
	  }

	  //hide any error messages 
	  $('#flash-container').fadeOut(12000);

	  //$('#AccessForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000})
      
});


function validateLogin()
{
	return true;//$('#AccessForm').validationEngine('validate');
}

function setActivateAccount()
{
	activateAccountFlag = true;
}

function activateAccount()
{
	$('#AccessForm').validationEngine('hide')
	toggleElements();
	
	$('#PageSubtitle').removeClass("hidden");
	$('#PageSubtitle2').removeClass("hidden");
	$('#PageTitle').text("Activate Account");
	$('#AccessForm').attr('action', '/users/joinNow');
}

function forgotPassword()
{
	//$('#AccessForm').validationEngine('hide')
	toggleElements();
	
	$('#PageTitle').text("Forgot Password");
	$('#AccessForm').attr('action', '/users/forgotPassword');
}

function backToLogin()
{
	//$('#AccessForm').validationEngine('hide')
	toggleElements();
	if (!($('#PageSubtitle').hasClass('hidden')))
	{
		$('#PageSubtitle').addClass("hidden");
		$('#PageSubtitle2').addClass("hidden");
	}
	$('#PageTitle').text("Login");
	$('#AccessForm').attr('action', '/auth/local');
}

function toggleElements()
{
	$('#FirstName').toggleClass('hidden');
	$('#LastName').toggleClass('hidden');
	$('#Password').toggleClass('hidden');
	
	$('#BackToLogin').toggleClass('hidden');
	$('#ForgotPassword').toggleClass('hidden');
	$('#ActivateAccount').toggleClass('hidden');

	//clean the inputs
	/*$('#FirstName').prop('value','');
	$('#LastName').prop('value','');
	$('#Password').prop('value','');
	$('#email').prop('value','');*/

}
