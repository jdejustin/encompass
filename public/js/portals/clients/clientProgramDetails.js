$(function (){
	initClientProgramDetails();

	//window.location.hash = "clientProgramDetails";
});

function initClientProgramDetails()
{
	initGlobal();

	$('html, body').animate({ scrollTop: 0 }, 'slow');

  
	onRowClickFunction = function(){
    	jQuery(".mainTable tbody tr").click(function(){
		var $this = jQuery(this);
		var eventId = $this.find(".SelectItem").val();
		  goToClientEventDetails(eventId);
    });
  };

   jQuery.ajax({
      url:"/portalclients/getClientEvents",
      datatype:'json',
      success:function(data){
         injectRows(data, ".mainTable" );
      }      
   });
}


function goToClientEventDetails(eventId)
{
  jQuery("#content").load('/events/'+ eventId +' #AjaxContent', function (data){
    if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
      initEventsShow();
  	}
  });
}


function cancelEventAction() {
  //return to the program details client
  goToClientProgramDetails();
}