function initClientResources()
{
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	initGlobal();

	initClickOnTableRowClientPortalResources();
}

function initClickOnTableRowClientPortalResources()
{
  	$('#kolTable tbody tr').click(function(){
  		var path = $(this).attr('data-path');
		var displayName = $(this).attr('data-displayName');
   		displayFile(path, displayName);
	});

	$('#repTable tbody tr').click(function(){
  		var path = $(this).attr('data-path');
		var displayName = $(this).attr('data-displayName');
   		displayFile(path, displayName);
	});

	$('#rsmTable tbody tr').click(function(){
  		var path = $(this).attr('data-path');
		var displayName = $(this).attr('data-displayName');
   		displayFile(path, displayName);
	});
}

