function initPortalContactDetails()
{
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	initGlobal();

	var path = $('#contactImg').attr('data-pathToFile');
    getUserPhoto(path.substring(20), 'contactImg');
}

function submitContactProfileChanges(contactName, contactEmail) 
{
	var updates = $('#edits').val();
	
	jQuery.ajax({
			url:'/users/submitProfileUpdate',
			data: {userName:contactName, email: contactEmail, updates: updates},
			type: 'POST',
			success:function(){
				$('html, body').animate({ scrollTop: 0 }, 'slow');
				$('#edits').val('');
				alert("Update(s) to profile has been submitted to CRG.");
			}
	});
}