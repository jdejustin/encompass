$(function (){
	initContactProgramDetails();

  //window.location.hash = "contactProgramDetails";
});

function initContactProgramDetails()
{
  $('html, body').animate({ scrollTop: 0 }, 'slow');
  
	initGlobal();

	onRowClickFunction = function(){
    	jQuery(".mainTable tbody tr").click(function(){
		var $this = jQuery(this);
		var eventId = $this.find(".SelectItem").val();
		goToContactEventDetails(eventId);
    });
  };

   jQuery.ajax({
      url:"/getMyEvents",
      datatype:'json',
      success:function(data){
         injectRows(data, ".mainTable" );
      }      
   });
}

function goToContactEventDetails(eventId)
{
  jQuery("#content").load('/events/'+ eventId +' #AjaxContent', function (data){
    if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
        //initalgo???
     }
  });
}