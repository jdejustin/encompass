function initContactResources()
{
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	initGlobal();

	initClickOnTableRowContactPortalResources();
}

function initClickOnTableRowContactPortalResources()
{
  	$('.mainTable tbody tr').click(function(){
  		var path = $(this).attr('data-path');
		var displayName = $(this).attr('data-displayName');
   		displayFile(path, displayName);
	});
}

