function submitManagerProfileChanges(managerName, managerEmail) 
{
	var updates = $('#edits').val();
	
	jQuery.ajax({
			url:'/users/submitProfileUpdate',
			data: {userName:managerName, email: managerEmail, updates: updates},
			type: 'POST',
			success:function(){
				$('html, body').animate({ scrollTop: 0 }, 'slow');
				$('#edits').val('');
				alert("Update(s) to profile has been submitted to CRG.");
			}
	});
}