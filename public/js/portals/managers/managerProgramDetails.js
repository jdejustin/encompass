$(function (){
  initManagerProgramDetails();

 // window.location.hash = "managerProgramDetails";
});

function initManagerProgramDetails()
{
  $('html, body').animate({ scrollTop: 0 }, 'slow');

	initGlobal();
  
	onRowClickFunction = function(){
    	jQuery(".mainTable tbody tr").click(function(){
		var $this = jQuery(this);
		var eventId = $this.find(".SelectItem").val();
		  goToManagerEventDetails(eventId);
    });
  };

   jQuery.ajax({
      url:"/portalmanagers/getMyProgramRequest",
      datatype:'json',
      success:function(data){
         injectRows(data, ".mainTable" );
      }      
   });
}


function createNewProgramRequest() {
  jQuery("#content").load('/events/clearCurrentEvent #AjaxContent', function (data){
    if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
        jQuery("#content").load('/events/eventStep1 #AjaxContent', function(){
          initEventStep1();
        });
     }
  });
}


function goToManagerEventDetails(eventId)
{
  jQuery("#content").load('/events/'+ eventId +' #AjaxContent', function (data){
    if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
      initEventsShow();
      
        //set listeners for the radio buttons
        $('input[name="resolutionGroup"]').click(function (){
          resolutionClicked();
        });

        $('input[name="rejectedSelection"]').click(function (){
          rejectedSelectionClicked();
        });


        //disable the rejectSelection
        $('input[name="rejectedSelection"]').prop('disabled',true);
        //disable the other text
        $(".js-other-comment").prop('disabled',true);

        //disable the other text
        $(".js-other-comment").keyup(function (){
            if($(this).val()!='')
            {
              //enable the submit 
              $('.js-submit').prop('disabled',false);
            }
            else
            {
              //disable the submit 
              $('.js-submit').prop('disabled',true);
            }
        });
          ('disabled',true);
        //disable the submit 
        $('.js-submit').prop('disabled',true);
     }
  });
}


function resolutionClicked()
{
  var resolution = $('input[name="resolutionGroup"]:checked').val();

  if (resolution == 'approved')
  {
    //enable the submit 
    $('.js-submit').prop('disabled',false);

    //disable the reject selection
    $('input[name="rejectedSelection"]').prop('disabled',true);
  }
  else
  {
    $('input[name="rejectedSelection"]').prop('disabled',false);
    
    //check if reject is checked
    var rejectComment = $('input[name="rejectedSelection"]:checked').val();
    if (typeof rejectComment == 'undefined')
    {
      //disable the submit 
      $('.js-submit').prop('disabled',true);
    }
    else if (rejectComment == 2 && ($(".js-other-comment").val() == ''))
    {
      //disable the submit 
      $('.js-submit').prop('disabled',true);
    }

  }
}


function rejectedSelectionClicked()
{
  var rejectComment = $('input[name="rejectedSelection"]:checked').val();
  //check if other is selected 
  if (rejectComment == 2)
  {//other is selected
    //enable the other text 
    $(".js-other-comment").prop('disabled',false);
    //check if other has a value 
    if ($(".js-other-comment").val()!= '')
    {
      //enable the submit 
      $('.js-submit').prop('disabled',false);
    }
    else
    {
      //disable the submit 
      $('.js-submit').prop('disabled',true);
    }
  }
  else
  {
    //disable the other text 
    $(".js-other-comment").prop('disabled',true);
    //enable the submit 
    $('.js-submit').prop('disabled',false);
  }
}

function cancelEventAction() {
  //return to the program details manager
  goToManagerProgramDetails();
}

function submitEventResolution() {

  var eventRequestResolution = new Object();


  var resolution = $('input[name="resolutionGroup"]:checked').val();

  if (resolution == 'approved')
  {
    eventRequestResolution.approvalStatus = 2;   //constants file... approvedStatus = 2
    eventRequestResolution.status = 0; // not confirmed
    eventRequestResolution.comment = $('input[name="resolutionGroup"]:checked').next().html().trim();
    //TODO enable submit 
  } else {
    //set the status 
    eventRequestResolution.approvalStatus = 3;   //constants file... deniedStatus = 3
    
    //check if other is selected 
    var rejectComment = $('input[name="rejectedSelection"]:checked').val();

    if (rejectComment == 2)
    {
        eventRequestResolution.comment = $('.js-other-comment').val();
    }
    else
    {
        eventRequestResolution.comment = $('input[name="rejectedSelection"]:checked').next().html().trim();
    }
  }


  jQuery.ajax({
    url:'/events/saveStatusEvent',
    datatype:'json',
    type: 'POST',
    async: true,
    data: eventRequestResolution,
    success:function(notificationData){
      console.log(JSON.stringify(notificationData));
      // send automatic communication delivery type to rep
      jQuery.ajax({
        url:'/communications/sendAutomaticEventRequestNotification',
        datatype:'json',
        type: 'POST',
        async: true,
        data: notificationData,
        success:function(data){
          goToManagerProgramDetails();
          console.log("event approved or denied");
        }
      });
    }
  });

}