function initManagerResources()
{
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	initGlobal();

	initClickOnTableRowManagerPortalResources();
}

function initClickOnTableRowManagerPortalResources()
{
  	$('.mainTable tbody tr').click(function(){
  		var path = $(this).attr('data-path');
		var displayName = $(this).attr('data-displayName');
   		displayFile(path, displayName);
	});
}