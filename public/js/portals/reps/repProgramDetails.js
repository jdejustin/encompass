$(function (){
  //window.location.hash = "repProgramDetails";
  
	initRepProgramDetails();
});

function initRepProgramDetails()
{
  $('html, body').animate({ scrollTop: 0 }, 'slow');
  
	initGlobal();

	onRowClickFunction = function(){
    	jQuery(".mainTable tbody tr").click(function(){
    		var $this = jQuery(this);
    		var eventId = $this.find(".SelectItem").val();
    		goToRepEventDetails(eventId);
      });
    };

   jQuery.ajax({
      url:"/portalreps/getMyProgramRequest",
      datatype:'json',
      success:function(data){
         injectRows(data, ".mainTable");
      }      
   });
}


function goToRepEventDetails(eventId)
{
  jQuery("#content").load('/events/'+ eventId +' #AjaxContent', function (data){
    if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
      initEventsShow();
     }
  });
}




function createNewProgramRequest() {
	jQuery("#content").load('/events/clearCurrentEvent #AjaxContent', function(data){
    if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
  		jQuery("#content").load('/events/eventStep1 #AjaxContent', function(data){
  			initEventStep1();
  		});
    }
	});
}