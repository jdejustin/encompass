
function initRepResources()
{
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	initGlobal();

	initClickOnTableRowRepPortalResources();
}

function initClickOnTableRowRepPortalResources()
{
  	$('.mainTable tbody tr').click(function(){
  		var path = $(this).attr('data-path');
		var displayName = $(this).attr('data-displayName');
   		displayFile(path, displayName);
	});
}