function initProgramReps() {
  	initGlobal();

  	$('html, body').animate({ scrollTop: 0 }, 'slow');

	//init function to call when search is completed
	initCheckboxProgramReps();

	onRowClickFunction = initCheckboxProgramReps;
}

function initCheckboxProgramReps()
{
	//add select class to checkbox
    $('.mainTable tbody td').find('[type=checkbox]').addClass('selectElement');

   	$('.selectElement').click(activateRep);
}

function activateRep()
{
	var ids = [];
	$('.selectElement:checked').each(function() {
		ids.push($(this).val());
	});

	var data = 'reps=' + ids.toString();
	saveProgramModel(data);

	// var targetProperty;
	// var data;

	// data = {
	// 		value: ["reps=" + uploadData.displayName],
	// 		// targetIndex: targetIndextargetIndex, 
	// 		targetIndex: 0, 
	// 		targetProperty: 'contractFile',
	// 		modelName: 'UploadedFile',
	// };

	// jQuery.ajax({
	// 	url:'/contracts/saveEmbeddedModel',
	// 	datatype:'json',
	// 	type: 'POST',
	// 	async: true,
	// 	data: data,
	// 	success:function(data){
	// 		var displayData = 'contractName=' + uploadData.displayName;
	// 		saveContractModel(displayData);
	// 	}
	// });
}