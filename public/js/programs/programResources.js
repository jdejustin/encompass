function initProgramResources() {
  	initGlobal();

  	$('html, body').animate({ scrollTop: 0 }, 'slow');

  	//set the behaviour of the select all element
	setSelectionBehaviour();  

	$('.deleteSelectedBtn').click(function(){
		showDeleteProgramResources(this);
	});

	//init function to call when search is completed
	initClickOnTableRowProgramResources();

	$('#File').on('change', onFileSelected);

	$( "#dialog-confirm-delete" ).dialog({autoOpen: false});
	$( "#uploadFile-form" ).dialog({autoOpen: false});
}

function initClickOnTableRowProgramResources()
{
    $('.mainTable tbody td').not('.checkBoxTd').click(function () {
    	//highlight selected row
		$(this).parent('tr').addClass("selected").siblings().removeClass("selected");

		var path = $(this).parent().find('.checkBoxTd').attr('data-path');
		var displayName = $(this).parent().find('.checkBoxTd').attr('data-displayName');
   		displayFile(path, displayName);
    });
}

function createNewProgramResource(id)
{
	$('#uploadNotes').show();

	fileUploadSuccessFunction = onProgramResourceFileUploadSuccess;

  	uploadTargetId = id;

	var title = 'Upload New Document';
	initUploadFileDialog(title, uploadFileFunction, uploadFileDialogCloseFunction);
}


function onProgramResourceFileUploadSuccess(returnedData)
{
	var filePath = '/storage/uploadedFiles/' + returnedData.fileFullName;
	var data;
	var totalItems = $('.mainTable tbody tr').length;

	data = {
			value: ["name=" + uploadData.displayName, "pathToFile=" + filePath, "notes=" + uploadData.notes, "link=" + uploadData.link],
			targetIndex: totalItems, 
			targetProperty: 'resources',
			modelName: 'UploadedFile',
	};

	jQuery.ajax({
		url:'/programs/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			goToProgramResources();
		}
	});
}


function showDeleteProgramResources(element)
{
  var title = 'Delete Selected Resource(s)?';
  var message = 'Selected Resource(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/programs/deleteResources';

  var callBack = function (data)
  {
    goToProgramResources();
  }


  //Prompt user if they're sure... if yes, then let's delete the selected resource from DB
  initDeleteDialog(title,message,deleteUrl,element, callBack);

}