function initProgramStep1() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	$(".programProperty").focusout(function (){saveProgramProperty(this);});

	$('.programType').focusout(function (){saveProgramType(this);});

	$('.crgProgramManager').focusout(function (){saveCrgProgramManager(this);});

	$('.businessOwnerName').focusout(function (){saveProgramProperty(this)});
	$('.businessOwnerDescription').focusout(function (){saveProgramProperty(this)});

	$(".js_eventTypeGroup").change(function() {saveEventTypes();});
	$(".js_invitationDeliveriesGroup").change(function() {saveInvitationDeliveries();});
	$(".js_venueRequired").change(function() {saveVenueInfoRequired();});

	//start the validation engine 
	$('#ProgramStep1ValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});
};


function saveProgramType(target)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var serializedData = 'programTypeId=' + $(target).find('option:selected').attr('id');
		saveProgramModel(serializedData);
		var serializedData2 = 'programTypeName=' + $(target).val();
		saveProgramModel(serializedData2);
	}
}

function saveCrgProgramManager(target)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var serializedData = 'crgProgramManager=' + $(target).find('option:selected').attr('id');
		saveProgramModel(serializedData);
	}
}

function validateProgramStep1(functionToCall)
{
	var validation = $('#ProgramStep1ValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}


function saveEventTypes()
{
	var eventTypes = $('input:checkbox:checked.js_eventTypeGroup').map(function () {
		  return this.value;
		}).get();
	var data = {
			eventTypes:eventTypes
		};
	
	if(eventTypes.length == 0)
	{
		eventTypes.push('');
	}
	saveProgramModel(data);
}

function saveInvitationDeliveries()
{
	var invitationDeliveries = $('input:checkbox:checked.js_invitationDeliveriesGroup').map(function () {
		  return this.value;
		}).get();
	var data = {
			invitationDeliveries:invitationDeliveries
		};
	
	if(invitationDeliveries.length == 0)
	{
		invitationDeliveries.push('');
	}
	saveProgramModel(data);
}

function saveVenueInfoRequired()
{
	var requireVenueInfo = $('input:checkbox:checked.js_venueRequired').map(function () {
		  return this.value;
		}).get();
	var data = {
			requireVenueInfo:requireVenueInfo
		};
	
	if(requireVenueInfo.length == 0)
	{
		requireVenueInfo.push('');
	}

	saveProgramModel(data);
}