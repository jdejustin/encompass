function setCurrentProgramId(id)
{
	jQuery.ajax({
		url:'/programs/setCurrentProgram',
		datatype:'json',
		type: 'POST',
		async: true,
		data: id,
		success:function(data){
			console.log("current program id set");
		}
	});
}

function saveProgramProperty(target)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		//console.log("saving client");
		var serializedData = $(target).serialize();
		saveProgramModel(serializedData);
	}
}

function saveToProgram(target, targetProperty, targetModel)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var data = {
			value: [$(target).attr("name") + "=" + $(target).val()],
			targetIndex: 0,
			targetProperty: targetProperty,
			modelName: targetModel
		};
	    
		saveProgramEmbeddedModel(data);
	}
}

function saveProgramModel(data)
{
	jQuery.ajax({
		url:'/programs/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("saved");
		}
	});
}


function saveProgramEmbeddedModel(data)
{
	jQuery.ajax({
		url:'/programs/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("Saved: " + data);
		}
	});
}


function goProgramDetails()
{
	jQuery("#content").load('/programs/1234 #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else	if (data == 'null')
	   {
			goProgramsList();
	   } else {
			initProgramShow();
	   }
	});
} 


function goProgramsList()
{
	jQuery("#content").load('/programs #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initProgramsIndex();
	   }
	});
} 