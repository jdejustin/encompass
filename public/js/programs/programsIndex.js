function initProgramsIndex() {

  $('html, body').animate({ scrollTop: 0 }, 'slow');
  
  initGlobal();
  
  $('#advancedSearch').click(function(){
    //display advanced search options
    $('#advancedSearchOptions').toggle();
  });


	$('.deleteSelectedBtn').click(function(){
    	showDeletePrograms(this);
    });

  //init function to call when search is completed
  initClickOnTableRowPrograms();

  onRowClickFunction = initClickOnTableRowPrograms;

  $( "#dialog-confirm-delete" ).dialog({autoOpen: false});
};

function initClickOnTableRowPrograms()
{
   $('.mainTable tbody td').not('.checkBoxTd').click(function(){
      $('html,body').css('cursor','wait');

      jQuery("#content").load('/programs/' + $(this).parent().find('[type=checkbox]').val() + ' #AjaxContent', function (data){
         if(data == 'false')
         {
            window.location = "/expired";
         }
         else
         {
           initProgramShow();
           $('html,body').css('cursor','default');
         }
      });
   });

   //add select class to checkbox
   $('.mainTable tbody td').find('[type=checkbox]').addClass('selectElement');

   //set the behaviour of the select all element
   setSelectionBehaviour(); 
}

function showDeletePrograms(element)
{
  var title = 'Delete Selected Program(s)?';
  var message = 'Selected Program(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/programs/deletePrograms';

  var callBack = function (data)
  {
     //finally, remove programs from clients
     jQuery("#content").load('/programs/deleteProgramFromClient #AjaxContent', {'ids': data}, function (data) {
        if(data == 'false')
         {
            window.location = "/expired";
         }
         else
         {
            goProgramsList();
         }
     });
  }
  
  //Prompt user if they're sure... if yes, then let's delete the selected programs from DB
  initDeleteDialog(title,message,deleteUrl,element, callBack);

}


function createNewProgram()
{
	jQuery("#content").load('/programs/clearCurrentProgram #AjaxContent', function (data){
    if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
      	jQuery("#content").load('/programs/programStep1 #AjaxContent', function(){
      		initProgramStep1();
        });
     }
	});
}  