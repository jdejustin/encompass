$(function (){
	$('#submitEventRegistrationCodeBtn').click(function(){
		$('#error').hide();

		var code = $('#registrationCode').val();

		var data = {code: code};

		jQuery.ajax({
			url:'/eventRegistration/submitRegistrationCode',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
			success:function(data){
				loadRegistrationForm(data.eventId);
			},
			error: function() {
				$('#registrationCode').val('');
				$('#error').show();
			}
		});
	});

	checkScreenWidth();
});


function checkScreenWidth()
{
	if (window.innerWidth < 1000)
	{
		$('.js-instructions').html('Please enter the code provided<br>on the communication you received.');
		$('.js-instructions').css('padding', 0);
	}
}


function loadRegistrationForm(eventId)
{
	var mobile = false;

	if (window.innerWidth < 1000)
	{
    	mobile = true;
    } 
	
	jQuery("#content").load('/eventRegistration/registrationForm #AjaxContent', {'mobile': mobile}, function () {
			initRegistrationForm();
	});
}