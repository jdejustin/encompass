var attendeeInfo;

function initRegistrationForm()
{
	attendeeInfo = new Object();

	//call global function to initialize data injection
	injectDataToView(null, null, setRegistrationListenersCallback);
}

function setRegistrationListenersCallback()
{
	//start the validation engine 
	$('#registrationValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 3000});

	$('.js-number-only').autoNumeric('init', {aPad: false, aSep: '', vMin: 0000000000, vMax: 9999999999});

	$('.js-registration-data').focusout(function (){saveAttendeeInfo(this);});
	$('.js-registration-npi-data').focusout(function (){saveAttendeeNPI(this);});
	$('.js-registration-license-data').focusout(function (){saveAttendeeInfo(this);});
	$('.js-registration-phone-data').focusout(function (){saveAttendeePhoneInfo(this);});
	$('.js-registration-fax-data').focusout(function (){saveAttendeeFaxInfo(this);});
	$('.js-registration-degrees-data').change(function() {saveDegreesInfo();});
	$('.js-registration-degrees-other-checkbox-data').change(function() {saveDegreesOtherInfo();});
	$('.js-registration-degrees-other-data').focusout(function() {saveDegreesOtherInfo();});
}

function saveAttendeeInfo(target)
{
	var value = $(target).val();
	var propertyName = $(target).attr('name');

	attendeeInfo[propertyName] = value;
}

function saveAttendeeNPI(target)
{
	var npi = $('#NPI').val();
	if (npi.length < 10)
	{
		$('#npiError').show();
	} else {
		$('#npiError').hide();
		saveAttendeeInfo(target);
	}
}


function saveAttendeePhoneInfo()
{
	var value = $('#phone1').val() + '-' + $('#phone2').val() + '-' + $('#phone3').val(); 

	attendeeInfo.phone = value;
}


function saveAttendeeFaxInfo()
{
	var value = $('#fax1').val() + '-' + $('#fax2').val() + '-' + $('#fax3').val(); 

	attendeeInfo.fax = value;
}


function saveDegreesInfo()
{
	var degrees = $('input:checkbox:checked.js-registration-degrees-data').map(function () {
		  return this.value;
		}).get();
	
	attendeeInfo.degrees = degrees;

	//check selected degrees, if MD, DO or NP, then make NPI, License # and License state required, otherwise don't
	if (degrees.indexOf('MD') != -1 || degrees.indexOf('DO') != -1 || degrees.indexOf('NP') != -1)
	{
		$('#NPI').addClass('validate[required]');
		$('#NPI').parent().find('span').show();

		$('.js-registration-license-data').addClass('validate[required]');
		$('.js-registration-license-data').parent().find('span').show();
	} else {
		$('#NPI').removeClass('validate[required]');
		$('#NPI').parent().find('span').hide();

		$('.js-registration-license-data').removeClass('validate[required]');
		$('.js-registration-license-data').parent().find('span').hide();


	}
}


function saveDegreesOtherInfo()
{
	saveDegreesInfo();

	attendeeInfo.otherDegrees = $('.js-registration-degrees-other-data').val();
}


function submitEventRegistration()
{
	 $( "#registrationValidateForm" ).submit();
	// jQuery.ajax({
	// 	url:'/eventRegistration/submitAttendeeInfo',
	// 	datatype:'json',
	// 	type: 'POST',
	// 	async: true,
	// 	data: attendeeInfo,
	// 	success:function(notificationData){
	// 		if (notificationData)
	// 		{
	// 			// send automatic registration communication, only if event.sendEventRequestNotification is set to true
	// 			jQuery.ajax({
	// 				url:'/communications/sendAutomaticEventRequestNotification',
	// 				datatype:'json',
	// 				type: 'POST',
	// 				async: true,
	// 				data: notificationData,
	// 				success:function(data){
	// 	 	    	 	console.log('Thank you!');
	// 				}
	//       		});
	// 		}
	// 	}
	// });
}

function validateEventRegistrationForm(functionToCall)
{
	var validation = $('#registrationValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}


// function initRegistrationLicenseListeners()
// {
// 	$('.js-registration-license-data').focusout(function (){saveAttendeeInfo(this);});

// 	//get the clone container 
// 	var $cloneContainer = $('[data-clone="licenses"]');
// 	bindTheLastRowChangeByCloneContainerRegistration($cloneContainer);
// }

// function initRegistrationLicenseAddListener(addButtonElement)
// {
// 	var $cloneContainer = $(addButtonElement).parents('.js-add-button-container').find('[data-clone]')
// 	bindTheLastRowChangeByCloneContainerRegistration($cloneContainer);
// }


// function bindTheLastRowChangeByCloneContainerRegistration($cloneContainer)
// {
// 	//unbind the old elements
// 	$cloneContainer.find(".js-registration-license-data").unbind('input',function (){changeRegistrationLicenseField(this);});
	
// 	//bind only for the last child of the rows 
// 	var $jsRowLastChild = $cloneContainer.find(".js-row:last");
// 	$jsRowLastChild.find(".js-registration-license-data").each(function(){
// 		$(this).bind('input',function(){changeRegistrationLicenseField(this);});
// 	});
// }


// function changeRegistrationLicenseField(licenseField)
// {
// 	//enable the add button
// 	$addButton = $(licenseField).parents('.js-add-button-container').find('.js-add-button');
// 	$addButton.prop('disabled',false);
// 	$addButton.removeClass('Disable');
// }
