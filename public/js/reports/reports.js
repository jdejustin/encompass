function initReports()
{
  $('html, body').animate({ scrollTop: 0 }, 'slow');

   reportsIndexLoaded = true;

   updateNavigation('reports');

   // initiGlobal();

  loadNewRP();

}

function generateReport()
{
  var reportType = $('#reportType').val();
  var clientId = $('#clientOptions').val();

  var data = new Object();
  data.reportType = $('#reportType').val();
  data.clientId = $('#clientOptions').val();

  //clear table before generating a report
  $('.js-table-container').html('');
  $('.js-properties-header').html('');

  window.open('/reports/generateReport?reportType=' + reportType +'&clientId=' + clientId);

  // jQuery.ajax({
  //   url: "/reports/generateReport",
  //   datatype:'json',
  //   type: 'POST',
  //   async: true,
  //   data: data,
  //   success:function(data){
  //     generateTable(data.dataset, data.reportName, data.fileName);
  //   }
  // });
}


function tableToExcel (table, name, filename) 
{
     if (!table.nodeType) table = document.getElementById(table)

     document.getElementById("dlink").href = '<html><body>' + table + '</body></html>';
     document.getElementById("dlink").download = filename;
     document.getElementById("dlink").click();
}

  function generateTable(objectArray, reportName, fileName)
  {

    //create header 
    if (objectArray.length > 0)
    {

      var tempObject = objectArray[0];
      var propertiesArray = Object.getOwnPropertyNames(tempObject);
      /*
      getOwnPropertyNames 
      Feature   Firefox (Gecko)   Chrome  Internet Explorer   Opera   Safari
      Basic support   4 (2.0)        5        9                 12      5
      */
      for (i= 0; i < propertiesArray.length; i++) {
         var headerName = humanNameProp(propertiesArray[i]);

         $('.js-properties-header').append("<th>"+headerName+"</th>");
      }

      for (j= 0; j < objectArray.length; j++) {
        var newElement = "<tr>";
        for (k= 0; k < propertiesArray.length; k++) {
         var value = objectArray[j][propertiesArray[k]];

         if (value == undefined)
         {
            value = ''
         }

            newElement += "<td>"+ value +"</td>";
        }
        newElement += "</tr>";
         $('.js-table-container').append(newElement);
      }

      tableToExcel('mainTable', reportName, fileName + '.xls');
      
    }

  }

  function humanNameProp (stringElement)
   {
      stringElement = stringElement.replace(/([A-Z])/g," $1");
      stringElement = stringElement.substring(0,1).toUpperCase() + stringElement.substring(1,stringElement.length); 
      return stringElement;
   }

   var tabCount = 1;

//STUTZENV3
function loadNewRP(){


        jQuery.getJSON("/js/reports/reportfilter.json", function(data) {
            var items = [];
            console.log('filter data ');
            console.log(data);
            for (var reportIndex = 0; reportIndex < data.length; reportIndex++)
            {
                var report = data[reportIndex];
                var reportAttribute = report.filterattribute;
                var filterSection = '';
                var filterValueFiledStr = '';
                for (var reportAttributeIndex = 0; reportAttributeIndex < reportAttribute.length; reportAttributeIndex++)
                {
                    var reportAttributeItem = reportAttribute[reportAttributeIndex];
                    var panel = "<div class='panel-heading-panel'><h3 class='panel-title'><input type='checkbox'  class='tabwrapper filter-field-section-all' fieldsection='{{fieldsection-key}}' />{{title}}</h3></div><div class='panel-body'><div class='row'>{{filter}}</div></div></div>";

                    panel = panel.replace("{{title}}", reportAttributeItem.display);
                    panel = panel.replace("{{fieldsection-key}}", reportAttributeItem.key);

                    var sectionfilterFiled = '';
                    for (var filterIndex = 0; filterIndex < reportAttributeItem.sub.length; filterIndex++)
                    {
                        var field = reportAttributeItem.sub[filterIndex];
                        var filterField = '<div class="col-sm-12 show-d" >';

                        filterField += '<input type="checkbox" class="tabwrapper filter-field show-d sec-'+reportAttributeItem.key+'" field="' + field.key + '" fieldsection="'+reportAttributeItem.key+'" >'; //fieldsection="'+reportAttributeItem.key+'"
                        filterField += '<label>' + field.display + '</label>';
                        filterField += '</div>';

                        sectionfilterFiled += filterField;

                        var filterValueField = '<div class="col-sm-2-5 ' + field.key + '" fieldsection="'+reportAttributeItem.key+'" >';

                        filterValueField += '<input type="text" class="form-control " fieldvalue="' + field.key + '" placeholder="' + field.display + '" >'
                        filterValueField += '</div>';

                        filterValueFiledStr += filterValueField;
                    }

                    panel = panel.replace("{{filter}}", sectionfilterFiled);

                    filterSection += panel;

                }

                filterSection += '<div class="row "><div class="col-sm-12 "><button class="add-filter btn btn-warning pull-right ">Save <span class="glyphicon glyphicon-plus-sign"></span></button></div></div>';

                jQuery(".template-containers ." + report.key + " .filter-options").html(filterSection);
                jQuery(".template-containers ." + report.key + " .panel-body .form-filter-field .row").html(filterValueFiledStr);

                //                console.log(".template-containers ." + report.key + " .col-sm-3");
                //                console.log("filterSection");
                //                console.log(filterSection);
            }

            jQuery('.filter-field').click(function(event) {

                var tabnameAttr = jQuery(event.target).attr('tab');
                var fieldnameAttr = jQuery(event.target).attr('field');

                console.log("tabnameAttr = " + tabnameAttr);
                console.log("fieldnameAttr = " + fieldnameAttr);

                if (jQuery(this).is(":checked")) {
                    jQuery("[tab='" + tabnameAttr + "']" + " ." + fieldnameAttr).show();
                } else {
                    jQuery("[tab='" + tabnameAttr + "']" + " ." + fieldnameAttr).hide();
                }

            });

            jQuery('.filter-field-section-all').click(function(event) {

                var tabnameAttr = jQuery(event.target).attr('tab');
                var fieldnameAttr = jQuery(event.target).attr('field');
                var sectionnameAttr = jQuery(event.target).attr('fieldsection');

                console.log("tabnameAttr = " + tabnameAttr);
                console.log("fieldnameAttr = " + fieldnameAttr);
                console.log("sectionnameAttr = " + sectionnameAttr);

                if (jQuery(this).is(":checked")) {

                    jQuery("[tab='" + tabnameAttr + "'] "+"input:checkbox.filter-field."+"sec-"+sectionnameAttr).prop('checked', true);
                    jQuery("[tab='" + tabnameAttr + "']" + " .form-filter-field [fieldsection='"+ sectionnameAttr+"']").show();

                } else {
                    jQuery("[tab='" + tabnameAttr + "'] "+"input:checkbox.filter-field."+"sec-"+sectionnameAttr).prop('checked', false);
                    jQuery("[tab='" + tabnameAttr + "']" + " .form-filter-field [fieldsection='"+ sectionnameAttr+"']").hide();
                }



            });


        });


        jQuery('.dash ').click(function() {

            jQuery('.clone-containers div').removeClass('show-d ');
            jQuery('.nav-tabs .func').removeClass('active ');
            jQuery('.nav-tabs .dash').addClass('active ');
            jQuery('.row.func').removeClass('show-d ');
            jQuery('.row.dashboard ').addClass('show-d ');


        });

        jQuery('.tabnav .text').click(function(event) {

            var tabnameAttr = jQuery(event.target).attr('tab');
            if (tabnameAttr == undefined)
                return;

            jQuery('.clone-containers div').removeClass('show-d ');
            jQuery(".clone-containers [tab='" + tabnameAttr + "']").addClass('show-d ');
            jQuery('.row.dashboard ').removeClass('show-d ');

            jQuery('.nav-tabs li').removeClass('active');
            jQuery(".nav-tabs [tab='" + tabnameAttr + "']").addClass('active');

        });

        jQuery('.tabnav .img').click(function(event) {

            var tabnameAttr = jQuery(event.target).attr('tab');
            //     console.log('tabnumerAttr ='+tabnameAttr) ;
            if (tabnameAttr == undefined)
                return;

            //   console.log('tabnumerAttr ='+tabnameAttr) ;

            jQuery(".clone-containers [tab='" + tabnameAttr + "']").remove();
            jQuery(".nav-tabs [tab='" + tabnameAttr + "']").remove();

            jQuery('.clone-containers div').removeClass('show-d ');
            jQuery('.nav-tabs .func').removeClass('active ');
            jQuery('.nav-tabs .dash').addClass('active ');
            jQuery('.row.func').removeClass('show-d ');
            jQuery('.row.dashboard ').addClass('show-d ');



        });


        jQuery('.dashboard .speakers ').click(function() {

            jQuery('.nav-tabs .func').removeClass('active ');

            jQuery('.row.func').removeClass('show-d ');

            var wrapContainerCount = tabCount++;
            var titleBar = document.createElement('li');
            titleBar.setAttribute('tab', 'tab' + (wrapContainerCount));
            titleBar.setAttribute('class', 'tabwrapper func tabnav');
            jQuery(".nav-tabs").append(titleBar);
            jQuery(".nav-clone .speakers").clone(true, true).appendTo(".nav-tabs [tab='tab" + wrapContainerCount + "']");
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));


            var tabBody = document.createElement('div');
            tabBody.setAttribute('tab', 'tab' + (wrapContainerCount));
            tabBody.setAttribute('class', 'tabwrapper func');
            jQuery(".clone-containers").append(tabBody);
            jQuery(".template-containers .speakers").clone(true, true).appendTo(".clone-containers [tab='tab" + wrapContainerCount + "']");
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));

            jQuery('.nav-tabs li').removeClass('active');
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "']").addClass('active');

            jQuery('.clone-containers div').removeClass('show-d ');
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "']").addClass('show-d ');

        });





        jQuery('.dashboard .sales ').click(function() {
            jQuery('.nav-tabs .func').removeClass('active ');

            jQuery('.row.func').removeClass('show-d ');

            var wrapContainerCount = tabCount++;
            var titleBar = document.createElement('li');
            titleBar.setAttribute('tab', 'tab' + (wrapContainerCount));
            titleBar.setAttribute('class', 'tabwrapper func tabnav');
            jQuery(".nav-tabs").append(titleBar);
            jQuery(".nav-clone .sales").clone(true, true).appendTo(".nav-tabs [tab='tab" + wrapContainerCount + "']");
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));


            var tabBody = document.createElement('div');
            tabBody.setAttribute('tab', 'tab' + (wrapContainerCount));
            tabBody.setAttribute('class', 'tabwrapper func');
            jQuery(".clone-containers").append(tabBody);
            jQuery(".template-containers .sales").clone(true, true).appendTo(".clone-containers [tab='tab" + wrapContainerCount + "']");
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));

            jQuery('.nav-tabs li').removeClass('active');
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "']").addClass('active');

            jQuery('.clone-containers div').removeClass('show-d ');
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "']").addClass('show-d ');
        });

        jQuery('.dashboard  .data ').click(function() {
            jQuery('.nav-tabs .func').removeClass('active ');

            jQuery('.row.func').removeClass('show-d ');

            var wrapContainerCount = tabCount++;
            var titleBar = document.createElement('li');
            titleBar.setAttribute('tab', 'tab' + (wrapContainerCount));
            titleBar.setAttribute('class', 'tabwrapper func tabnav');
            jQuery(".nav-tabs").append(titleBar);
            jQuery(".nav-clone .data").clone(true, true).appendTo(".nav-tabs [tab='tab" + wrapContainerCount + "']");
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));


            var tabBody = document.createElement('div');
            tabBody.setAttribute('tab', 'tab' + (wrapContainerCount));
            tabBody.setAttribute('class', 'tabwrapper func');
            jQuery(".clone-containers").append(tabBody);
            jQuery(".template-containers .data").clone(true, true).appendTo(".clone-containers [tab='tab" + wrapContainerCount + "']");
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));

            jQuery('.nav-tabs li').removeClass('active');
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "']").addClass('active');

            jQuery('.clone-containers div').removeClass('show-d ');
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "']").addClass('show-d ');
        });

        jQuery('.dashboard .speaker-usage ').click(function() {
            jQuery('.nav-tabs .func').removeClass('active ');

            jQuery('.row.func').removeClass('show-d ');

            var wrapContainerCount = tabCount++;
            var titleBar = document.createElement('li');
            titleBar.setAttribute('tab', 'tab' + (wrapContainerCount));
            titleBar.setAttribute('class', 'tabwrapper func tabnav');
            jQuery(".nav-tabs").append(titleBar);
            jQuery(".nav-clone .speaker-usage").clone(true, true).appendTo(".nav-tabs [tab='tab" + wrapContainerCount + "']");
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));


            var tabBody = document.createElement('div');
            tabBody.setAttribute('tab', 'tab' + (wrapContainerCount));
            tabBody.setAttribute('class', 'tabwrapper func');
            jQuery(".clone-containers").append(tabBody);
            jQuery(".template-containers .speaker-usage").clone(true, true).appendTo(".clone-containers [tab='tab" + wrapContainerCount + "']");
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));

            jQuery('.nav-tabs li').removeClass('active');
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "']").addClass('active');

            jQuery('.clone-containers div').removeClass('show-d ');
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "']").addClass('show-d ');
        });

        jQuery('.dashboard .program').click(function() {
            jQuery('.nav-tabs .func').removeClass('active ');

            jQuery('.row.func').removeClass('show-d ');

            var wrapContainerCount = tabCount++;
            var titleBar = document.createElement('li');
            titleBar.setAttribute('tab', 'tab' + (wrapContainerCount));
            titleBar.setAttribute('class', 'tabwrapper func tabnav');
            jQuery(".nav-tabs").append(titleBar);
            jQuery(".nav-clone .program").clone(true, true).appendTo(".nav-tabs [tab='tab" + wrapContainerCount + "']");
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));


            var tabBody = document.createElement('div');
            tabBody.setAttribute('tab', 'tab' + (wrapContainerCount));
            tabBody.setAttribute('class', 'tabwrapper func');
            jQuery(".clone-containers").append(tabBody);
            jQuery(".template-containers .program").clone(true, true).appendTo(".clone-containers [tab='tab" + wrapContainerCount + "']");
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));

            jQuery('.nav-tabs li').removeClass('active');
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "']").addClass('active');

            jQuery('.clone-containers div').removeClass('show-d ');
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "']").addClass('show-d ');
        });


        jQuery('.dashboard .atten').click(function() {
            jQuery('.nav-tabs .func').removeClass('active ');

            jQuery('.row.func').removeClass('show-d ');

            var wrapContainerCount = tabCount++;
            var titleBar = document.createElement('li');
            titleBar.setAttribute('tab', 'tab' + (wrapContainerCount));
            titleBar.setAttribute('class', 'tabwrapper func tabnav');
            jQuery(".nav-tabs").append(titleBar);
            jQuery(".nav-clone .atten").clone(true, true).appendTo(".nav-tabs [tab='tab" + wrapContainerCount + "']");
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));


            var tabBody = document.createElement('div');
            tabBody.setAttribute('tab', 'tab' + (wrapContainerCount));
            tabBody.setAttribute('class', 'tabwrapper func');
            jQuery(".clone-containers").append(tabBody);
            jQuery(".template-containers .atten").clone(true, true).appendTo(".clone-containers [tab='tab" + wrapContainerCount + "']");
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));

            jQuery('.nav-tabs li').removeClass('active');
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "']").addClass('active');

            jQuery('.clone-containers div').removeClass('show-d ');
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "']").addClass('show-d ');
        });

        jQuery('.dashboard .recon').click(function() {
            jQuery('.nav-tabs .func').removeClass('active ');

            jQuery('.row.func').removeClass('show-d ');

            var wrapContainerCount = tabCount++;
            var titleBar = document.createElement('li');
            titleBar.setAttribute('tab', 'tab' + (wrapContainerCount));
            titleBar.setAttribute('class', 'tabwrapper func tabnav');
            jQuery(".nav-tabs").append(titleBar);
            jQuery(".nav-clone .recon").clone(true, true).appendTo(".nav-tabs [tab='tab" + wrapContainerCount + "']");
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));


            var tabBody = document.createElement('div');
            tabBody.setAttribute('tab', 'tab' + (wrapContainerCount));
            tabBody.setAttribute('class', 'tabwrapper func');
            jQuery(".clone-containers").append(tabBody);
            jQuery(".template-containers .recon").clone(true, true).appendTo(".clone-containers [tab='tab" + wrapContainerCount + "']");
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "'] .tabwrapper").attr('tab', 'tab' + (wrapContainerCount));

            jQuery('.nav-tabs li').removeClass('active');
            jQuery(".nav-tabs [tab='tab" + wrapContainerCount + "']").addClass('active');

            jQuery('.clone-containers div').removeClass('show-d ');
            jQuery(".clone-containers [tab='tab" + wrapContainerCount + "']").addClass('show-d ');
        });

        jQuery('span.img').click(function() {
            jQuery(this).parent('.func ').off();
            jQuery(this).parent('.func ').removeClass('active');
            jQuery(this).parent('.func ').removeClass('show-d');
            jQuery('.nav-tabs .dash').addClass('active');
        });

        //
        //        jQuery('.filter-field').click(function(event) {
        //
        //            var tabnameAttr = jQuery(event.target).attr('tab');
        //            var fieldnameAttr = jQuery(event.target).attr('field');
        //
        //            console.log("tabnameAttr = " + tabnameAttr);
        //            console.log("fieldnameAttr = " + fieldnameAttr);
        //
        //
        //        });

        jQuery('.speak-name-filter-field').click(function(event) {

            var tabnameAttr = jQuery(event.target).attr('tab');
            if (tabnameAttr == undefined)
                return;
            console.log('tabnumerAttr =' + tabnameAttr);

            if (jQuery(this).is(":checked")) {
                jQuery("[tab='" + tabnameAttr + "']" + " .speak-name").show();
            } else {
                jQuery("[tab='" + tabnameAttr + "']" + " .speak-name").hide();
            }
        });
        jQuery('.speak-mail-filter-field').click(function() {

            var tabnameAttr = jQuery(event.target).attr('tab');
            if (tabnameAttr == undefined)
                return;

            if (jQuery(this).is(":checked")) {
                jQuery("[tab='" + tabnameAttr + "']" + " .speak-mail").show();
            } else {
                jQuery("[tab='" + tabnameAttr + "']" + " .speak-mail").hide();
            }

        });

        jQuery('.speak-city-filter-field').click(function() {
            var tabnameAttr = jQuery(event.target).attr('tab');
            if (tabnameAttr == undefined)
                return;

            if (jQuery(this).is(":checked")) {
                jQuery("[tab='" + tabnameAttr + "']" + " .speak-city").show();
            } else {
                jQuery("[tab='" + tabnameAttr + "']" + " .speak-city").hide();
            }


        });
        jQuery('.speak-npi-filter-field').click(function() {

            var tabnameAttr = jQuery(event.target).attr('tab');
            if (tabnameAttr == undefined)
                return;

            if (jQuery(this).is(":checked")) {
                jQuery("[tab='" + tabnameAttr + "']" + " .speak-npi").show();
            } else {
                jQuery("[tab='" + tabnameAttr + "']" + " .speak-npi").hide();
            }

        });
        jQuery('.speak-avail-filter-field').click(function() {

            var tabnameAttr = jQuery(event.target).attr('tab');
            if (tabnameAttr == undefined)
                return;
            console.log('tabnumerAttr =' + tabnameAttr);

            if (jQuery(this).is(":checked")) {
                jQuery("[tab='" + tabnameAttr + "']" + " .speak-avail").show();
            } else {
                jQuery("[tab='" + tabnameAttr + "']" + " .speak-avail").hide();
            }

        });

        ////////////
        jQuery('#sale-name').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".sale-name").show();
            } else {
                jQuery(".sale-name").hide();
            }
        });
        jQuery('#sale-mail').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".sale-mail").show();
            } else {
                jQuery(".sale-mail").hide();
            }
        });
        jQuery('#sale-city').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".sale-city").show();
            } else {
                jQuery(".sale-city").hide();
            }
        });
        jQuery('#sale-npi').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".sale-npi").show();
            } else {
                jQuery(".sale-npi").hide();
            }
        });
        jQuery('#sale-avail').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".sale-avail").show();
            } else {
                jQuery(".sale-avail").hide();
            }
        });
        ////////////
        jQuery('#data-name').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".data-name").show();
            } else {
                jQuery(".data-name").hide();
            }
        });
        jQuery('#data-mail').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".data-mail").show();
            } else {
                jQuery(".data-mail").hide();
            }
        });
        jQuery('#data-city').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".data-city").show();
            } else {
                jQuery(".data-city").hide();
            }
        });
        jQuery('#data-npi').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".data-npi").show();
            } else {
                jQuery(".data-npi").hide();
            }
        });
        jQuery('#data-avail').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".data-avail").show();
            } else {
                jQuery(".data-avail").hide();
            }
        });
        ////////////
        jQuery('#usage-name').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".usage-name").show();
            } else {
                jQuery(".usage-name").hide();
            }
        });
        jQuery('#usage-mail').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".usage-mail").show();
            } else {
                jQuery(".usage-mail").hide();
            }
        });
        jQuery('#usage-city').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".usage-city").show();
            } else {
                jQuery(".usage-city").hide();
            }
        });
        jQuery('#usage-npi').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".usage-npi").show();
            } else {
                jQuery(".usage-npi").hide();
            }
        });
        jQuery('#usage-avail').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".usage-avail").show();
            } else {
                jQuery(".usage-avail").hide();
            }
        });
        ////////////
        jQuery('#progrm-name').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".progrm-name").show();
            } else {
                jQuery(".progrm-name").hide();
            }
        });
        jQuery('#progrm-mail').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".progrm-mail").show();
            } else {
                jQuery(".progrm-mail").hide();
            }
        });
        jQuery('#progrm-city').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".progrm-city").show();
            } else {
                jQuery(".progrm-city").hide();
            }
        });
        jQuery('#progrm-npi').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".progrm-npi").show();
            } else {
                jQuery(".progrm-npi").hide();
            }
        });
        jQuery('#progrm-avail').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".progrm-avail").show();
            } else {
                jQuery(".progrm-avail").hide();
            }
        });
        ////////////
        jQuery('#atten-name').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".atten-name").show();
            } else {
                jQuery(".atten-name").hide();
            }
        });
        jQuery('#atten-mail').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".atten-mail").show();
            } else {
                jQuery(".atten-mail").hide();
            }
        });
        jQuery('#atten-city').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".atten-city").show();
            } else {
                jQuery(".atten-city").hide();
            }
        });
        jQuery('#atten-npi').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".atten-npi").show();
            } else {
                jQuery(".progrm-npi").hide();
            }
        });
        jQuery('#atten-avail').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".atten-avail").show();
            } else {
                jQuery(".atten-avail").hide();
            }
        });
        ////////////
        jQuery('#recon-name').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".recon-name").show();
            } else {
                jQuery(".recon-name").hide();
            }
        });
        jQuery('#recon-mail').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".recon-mail").show();
            } else {
                jQuery(".recon-mail").hide();
            }
        });
        jQuery('#recon-city').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".recon-city").show();
            } else {
                jQuery(".recon-city").hide();
            }
        });
        jQuery('#recon-npi').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".recon-npi").show();
            } else {
                jQuery(".recon-npi").hide();
            }
        });
        jQuery('#recon-avail').click(function() {
            if (jQuery(this).is(":checked")) {
                jQuery(".recon-avail").show();
            } else {
                jQuery(".recon-avail").hide();
            }
        });
        ////////////

}
