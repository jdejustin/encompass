function UploadExcel() {
}


function fillSideA($thisDifffDiv, info) {
    jQuery($thisDifffDiv).find(".SideA .LastName").val(info.a["Last Name"]);
    jQuery($thisDifffDiv).find(".SideA .MiddleName").val(info.a["Middle Name"]);
    jQuery($thisDifffDiv).find(".SideA .FirstName").val(info.a["First Name"]);
    jQuery($thisDifffDiv).find(".SideA .Suffix").val(info.a["Suffix"]);
    jQuery($thisDifffDiv).find(".SideA .Title").val(info.a["Title"]);
    jQuery($thisDifffDiv).find(".SideA .Email").val(info.a["Email (will follow FILast@bdsi.com)"]);

    jQuery($thisDifffDiv).find(".SideA .Team").val(info.a["Team"]);
    jQuery($thisDifffDiv).find(".SideA .Zone").val(info.a["Zone"]);
    jQuery($thisDifffDiv).find(".SideA .TerritoryName").val(info.a["Territory Name"]);
    jQuery($thisDifffDiv).find(".SideA .EmployeeID").val(info.a["Employee ID"]);
    jQuery($thisDifffDiv).find(".SideA .HireDate").val(info.a["Hire Date"]);

    jQuery($thisDifffDiv).find(".SideA .Phone1").val(info.a["Phone 1"].replace(/-.*$/, ""));
    jQuery($thisDifffDiv).find(".SideA .Phone1Label").val(info.a["Phone 1 Label"]);
    jQuery($thisDifffDiv).find(".SideA .Phone2").val(info.a["Phone 2"].replace(/-.*$/, ""));
    jQuery($thisDifffDiv).find(".SideA .Phone2Label").val(info.a["Phone 2 Label"]);

    jQuery($thisDifffDiv).find(".SideA .AddressType").val(info.a["Address Label"]);
    jQuery($thisDifffDiv).find(".SideA .Address1").val(info.a["Street Address"]);
    jQuery($thisDifffDiv).find(".SideA .Address2").val(info.a["Address Line 2"]);
    jQuery($thisDifffDiv).find(".SideA .City").val(info.a["City"]);
    jQuery($thisDifffDiv).find(".SideA .State").val(info.a["State/ Province"]);
    jQuery($thisDifffDiv).find(".SideA .Zip").val(info.a["Postal Code"]);
    jQuery($thisDifffDiv).find(".SideA .Country").val(info.a["Country"]);
}




function CompareValues(ElementA, ElementB, MongoValue) {
    if (ElementA.val() != MongoValue) {
        ElementA.addClass("Diferent");
        ElementB.addClass("Diferent");
    }
}


function fillSideB($thisDifffDiv, info) {
    var $SideBExisting = jQuery("#DiffTemplate .SideB .Existing");
    var $SideBEMailAdress = jQuery("#DiffTemplate .SideB .EMailAdress");
    var $SideBPhoneNumbers = jQuery("#DiffTemplate .SideB .PhoneNumbers");
    //var $SideBAffiliations = jQuery("#DiffTemplate .SideB .Affiliations");
    var $SideBAddress = jQuery("#DiffTemplate .SideB .Address");

    var $sideB = jQuery($thisDifffDiv).find(".SideB");
    var i = 0;
    jQuery(info.b).each(function () {
        var currentBData = this;
        var $existing;
        if (i == 0) {
            $existing = jQuery($sideB).find(".Existing");
        } else {
            $existing = $SideBExisting.clone();
            $sideB.append($existing);
        }

        $existing.find(".LastName").val(this["lastName"]);
        $existing.find(".MiddleName").val(this["middleName"]);
        $existing.find(".FirstName").val(this["firstName"]);

        $existing.find(".Suffix").val(this["suffix"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .Suffix"), $existing.find(".Suffix"), this["suffix"]);

        $existing.find(".Title").val(this["title"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .Title"), $existing.find(".Title"), this["title"]);


        $existing.find("input[name=id]").val(this["id"]);


        $existing.find(".Email").val(this["defaultEmail"]);
        CompareValues(jQuery($thisDifffDiv).find(".SideA .Email"), $existing.find(".Email"), this["defaultEmail"]);

        var phoneCount = 0;
        jQuery(this.phoneNumbers).each(function () {
            var $newPhone;
            if (phoneCount == 0) {
                $newPhone = $existing.find(".PhoneNumbers");
            } else {
                $newPhone = $SideBPhoneNumbers.clone();
                $existing.find("#ExistingContact").append($newPhone);
            }
            $newPhone.find(".PhoneNumber").val(this["number"]);

            CompareValues(jQuery($thisDifffDiv).find(".SideA ." + this["phoneType"] + "Phone"), $newPhone.find(".PhoneNumber"), this["number"]);


            $newPhone.find(".idPhoneNumbers").val(this["id"]);
            $newPhone.find(".PhoneType").text(this["phoneType"]);
            phoneCount++;
        });



        if (this.officeStaffContact) {
            $existing.find(".officeStaffContact").val(this.officeStaffContact["fullName"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .officeStaffContact"), $existing.find(".officeStaffContact"), this["officeStaffContact"]);

            $existing.find(".officeStaffContactEmail").val(this.officeStaffContact["email"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .officeStaffContactEmail"), $existing.find(".officeStaffContactEmail"), this["officeStaffContactEmail"]);

            if (this.officeStaffContact.phoneNumbers && this.officeStaffContact.phoneNumbers[0]) {
                $existing.find(".officeStaffContactPhone").val(this.officeStaffContact.phoneNumbers[0]["number"]);
                CompareValues(jQuery($thisDifffDiv).find(".SideA .officeStaffContactPhone"), $existing.find(".officeStaffContactPhone"), this.officeStaffContact.phoneNumbers[0]["number"]);
            }
        }

        var affiliationCount = 0;
        jQuery(this.affiliations).each(function () {
            var $newaffiliation;
            if (affiliationCount == 0) {
                $newaffiliation = $existing.find(".Affiliations");
            } else {
                $newaffiliation = $SideBAffiliations.clone();
                $existing.find("#ExistingAffiliations").append($newaffiliation);
            }
            $newaffiliation.find(".Affiliation").val(this["affiliationName"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .Affiliation"), $newaffiliation.find(".Affiliation"), this["affiliationName"]);

            $newaffiliation.find(".AffiliationTitle").val(this["title"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .AffiliationTitle"), $newaffiliation.find(".AffiliationTitle"), this["title"]);

            $newaffiliation.find(".AffiliationCity").val(this["city"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .AffiliationCity"), $newaffiliation.find(".AffiliationCity"), this["city"]);

            affiliationCount++;
        });


        var addressCount = 0;
        jQuery(this.addresses).each(function () {
            var $newaddress;
            if (addressCount == 0) {
                $newaddress = $existing.find(".Address");
            } else {
                $newaddress = $SideBAddress.clone();
                $existing.find("#ExistingAddress").append($newaddress);
            }
            $newaddress.find(".AddressType").val(this["addressType"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .AddressType"), $newaddress.find(".AddressType"), this["addressType"]);

            $newaddress.find(".Address1").val(this["address1"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .Address1"), $newaddress.find(".Address1"), this["address1"]);

            $newaddress.find(".Address2").val(this["address2"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .Address2"), $newaddress.find(".Address2"), this["address2"]);

            $newaddress.find(".City").val(this["city"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .City"), $newaddress.find(".City"), this["city"]);

            $newaddress.find(".State").val(this["state"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .State"), $newaddress.find(".State"), this["state"]);

            $newaddress.find(".Zip").val(this["postalCode"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .Zip"), $newaddress.find(".Zip"), this["postalCode"]);

            $newaddress.find(".country").val(this["country"]);
            CompareValues(jQuery($thisDifffDiv).find(".SideA .country"), $newaddress.find(".country"), this["country"]);
            addressCount++;
        });


    });

}

function UploadExcelOnComplete(json) {

    var data = jQuery.parseJSON(json);
    var $DiffTemplate = jQuery("#DiffTemplate");
    var $DiffContainer = jQuery("#diff");

    jQuery(".UploadContainer").hide();

    if (data.diff && data.diff.length > 0) {

        jQuery(".DiffMain").show();

        var i = 0;
        jQuery(data.diff).each(function () {
            var info = this;
            var divId = "diff_" + i; i++;
            var $thisDifffDiv = $DiffTemplate.clone();
            $thisDifffDiv.attr("id", divId);

            fillSideA($thisDifffDiv, info);

            fillSideB($thisDifffDiv, info);

            $thisDifffDiv.find(".Save").click(function () {
                var $activeForm = jQuery(".Active .diffForm");
                $activeForm.ajaxForm({
                    beforeSend: function () {
                    },
                    success: function () {
                    },
                    complete: function (xhr) {
                        //alert(xhr.responseText);
                        alert("Contact saved");
                    }
                });
                $activeForm.submit();
            });

            $DiffContainer.append($thisDifffDiv);
        });

        jQuery(".SideATabs").tabs({
            activate: function (event, ui) {
                var tabIndex = ui.newTab.parent().find("li").index(ui.newTab);
                var $parent = ui.newTab.parents(".DiffContainer").eq(0);
                $parent.find(".SideBTabs .ui-tabs-anchor").eq(tabIndex).click();
            }
        });
        jQuery(".SideBTabs").tabs({

            activate: function (event, ui) {
                var tabIndex = ui.newTab.parent().find("li").index(ui.newTab);
                var $parent = ui.newTab.parents(".DiffContainer").eq(0);
                $parent.find(".SideATabs .ui-tabs-anchor").eq(tabIndex).click();
            }

        });

        jQuery("#NextDiff").click();
    } else {
        jQuery(".SuccessContainer").show();
    }
}


function initRepsImport() {
    $('html, body').animate({ scrollTop: 0 }, 'slow');
    
    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status');
    jQuery(".DiffMain").hide();
    jQuery(".SuccessContainer").hide();
    jQuery(".UploadContainer").show();


    $('#UploadExcel').ajaxForm({
        beforeSend: function () {
            jQuery("#diff").html("");
            status.empty();
            var percentVal = '0%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        uploadProgress: function (event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        success: function () {
            var percentVal = '100%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        complete: function (xhr) {
            UploadExcelOnComplete(xhr.responseText);
        }
    });



    jQuery("#NextDiff").click(function () {
        var $active = jQuery("#diff .Active");

        if ($active.length == 0) {
            $active = jQuery("#diff .DiffContainer:first");
            $active.show();
            $active.toggleClass("Active");
        } else {
            $active.toggleClass("Active");
            $active.hide();

            var $nextDiff = $active.next();

            if ($nextDiff.length == 0) {
                $nextDiff = jQuery("#diff .DiffContainer:first");
            }

            $nextDiff.toggleClass("Active");
            $nextDiff.show();
        }
    });

    jQuery("#PrevDiff").click(function () {
        var $active = jQuery("#diff .Active");

        if ($active.length == 0) {
            $active = jQuery("#diff .DiffContainer:last");
            $active.show();
            $active.toggleClass("Active");
        } else {
            $active.toggleClass("Active");
            $active.hide();

            var $nextDiff = $active.prev();

            if ($nextDiff.length == 0) {
                $nextDiff = jQuery("#diff .DiffContainer:last");
            }

            $nextDiff.toggleClass("Active");
            $nextDiff.show();
        }
    });


}