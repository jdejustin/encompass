var emailIndex;
var repNumberIndex;
var addressIndex;

function initRepStep1(data) {
	$('html, body').animate({ scrollTop: 0 }, 'slow');

	console.log('initRepStep1');
	/* focus out of the elements */
	$(".js-repProperty").focusout(function (){saveRepProperty(this);});
	$(".js-role").change(function (){saveRepRoleAndSetManagerList(this);});
	$(".js-managers").change(function (){saveManagerId(this);});

	$('.js-default-address').change(function(){saveRepAddress($(this).parents('.FormRowBackground').find('.js-street-address'))});
	$('.js-default-email').change(function(){saveRepEmail($(this).parents('.FormRowBackground').find('.repMailElement'))});
	$('.js-default-phoneNumber').change(function(){saveRepNumber($(this).parents('.FormRowBackground').find('.PhoneElement'))});

	$('.repMailElement').focusout(function (){saveRepEmail(this)});
	$(".repMailElement").bind('input',function (){changeRepField(this);});

	$('.PhoneElement').focusout(function (){saveRepNumber(this);});
	$(".PhoneElement").bind('input',function (){changePhoneNumberElement(this);});
	
	$('.internationalRepCheckbox').change(function (){internationalRepChange(this);});
	$('.addressElement').focusout(function (){saveRepAddress(this)});
	$(".addressElement").bind('input',function (){changeRepField(this);});

	$(".PhoneNumber").intlTelInput();

	emailIndex = $("#EmailContainer").children('.FormRowBackground').length-1;
	repNumberIndex = $("#ContactNumberContainer").children('.FormRowBackground').length-1;
	addressIndex = $("#AddressContainer").children('.FormRowBackground').length-1;

	/*var test = {
		 "emailAddresses":[
		     {"email":"myEmail@gmail.com","prefered" : null},
		     {"email":"myEmail4@gmail.com","prefered" : null}
		],
		"address":[
		     {"city":"springfield","state":"georgia"},
		     {"city":"springfield","state":"georgia"}
		]
		};*/
	injectDataToView(data);		

	//start the validation engine 
	$('#RepStep1ValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});
}


function saveRepEmail(target)
{
	var validation = $(target).validationEngine('validate');
	if(!validation)
	{
		var defaultFlag = $(target).parents('.FormRowBackground').find('.js-default-email').is(':checked');

		saveToRep(target, 'emailAddresses', 'ContactEmailAddress', defaultFlag);

		//Check to see if this target is set to default
		if (defaultFlag)
		{
			saveRepDefaultItem(target, 'defaultEmail');
		}
	}
}


function saveRepAddress(target)
{
	var defaultFlag = $(target).parents('.FormRowBackground').find('.js-default-address').is(':checked');

	saveToRep(target, 'addresses', 'Address', defaultFlag);

	//Check to see if this target is set to default

	if (defaultFlag)
	{
		saveRepDefaultItem(target, 'defaultAddress');
	}
}


function saveRepRoleAndSetManagerList(target)
{
	var roleLevel = $('.js-role option:selected').attr('data-level');

	var serializedData = 'role' + '=' + $(target).val();
	saveRepModel(serializedData);

	var serializedData = 'level' + '=' + roleLevel;
	saveRepModel(serializedData);

	var data = {level: roleLevel};

	//based on role level, get manager's data for list
	jQuery.ajax({
		url:'/reps/getRepsByLevel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(items){
			//reload data into managers list
			$('.js-managers').empty();

			// var items = jQuery.parseJSON(data);
			$('.js-managers').append($('<option>', { 
			        value: '0',
			        text : 'N/A'
			    }));

			$.each(items.reps, function (i, item) {
			    $('.js-managers').append($('<option>', { 
			        value: item.id,
			        text : item.firstName + ' ' + item.lastName 
			    }));
			});
		}
	});

	//call create user account just in case, we filled out other stuff first
	createUserAccount();
}

function saveManagerId(target)
{
	var serializedData = 'managerId' + '=' + $(target).val();
	saveRepModel(serializedData);
}


///////////  EMAILS /////////////////
function addEmail(target)
{
	emailIndex++;

	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);
	
	//create the delete button
   	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteEmail(this)'>X</button>";
	updateRepPage(target,deleteButton,emailIndex);
	
	// add focus out to the components 
//	$('.MailElement').focusout(function (){saveEmail(this);});
	$('.repMailElement').focusout(function (){saveRepEmail(this);});//targetProperty: "emailAddresses",modelName: "ContactEmailAddress"
	//$(".repMailElement").bind('input',function (){changeElement(this);});

	//bind the last element by container
	bindRepTheLastRowChangeByAddButtonContainer(addButtonContainer);
}


function deleteEmail(target)
{
	emailIndex--;
	deleteRepElement(target,"emailAddresses",emailIndex);
}




///////////  CONTACT NUMBER /////////////////
function addRepNumber(target)
{
	repNumberIndex++;
	
	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);

	//create the delete button
   var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteRepNumber(this)'>X</button>";
	
	var clonedElement = updateRepPage(target,deleteButton,repNumberIndex);
	
	// add focus out to the components 
	$('.PhoneElement').focusout(function (){saveRepNumber(this);});
	//$(".PhoneElement").bind('input',function (){changePhoneNumberElement(this);});
	//bind the last element by container
	bindRepTheLastRowChangeByAddButtonContainer(addButtonContainer);

	$(".PhoneNumber").intlTelInput();
}
    

function changePhoneNumberElement(target)
{
	if ($(target).val() != "")
	{
		enableAddButtonByContainer($(target).parent().parent().parent().parent());
	}
}


function saveRepNumber(target)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var defaultFlag = $(target).parents('.FormRowBackground').find('.js-default-phoneNumber').is(':checked');

		var targetIndex = $(target).parents('.js-rep-row').find('.index').val();

		var data = {
				value: [$(target).attr("name")+"="+$(target).val(), 'preferred=' + defaultFlag],
				targetIndex: targetIndex,
				targetProperty: "phoneNumbers",
				modelName: "ContactPhoneNumber"
			};
	
		//enable the delete buttons
		var container = $(target).parent().parent().parent();
		enableDeleteButtonXByContainer(container);
		enableAddButtonByContainer(container);
		
		saveRepEmbeddedModel(data);

		if ($(target).attr('name') != 'phoneType')
		{
			//Check to see if this target is set to default
			if (defaultFlag)
			{
				saveRepDefaultItem(target, 'defaultPhoneNumber');
			}
			
		}
	}
}


function deleteRepNumber(target)
{
	repNumberIndex--;
	deleteRepElement(target,"phoneNumbers",repNumberIndex);
}


///////////  ADDRESS /////////////////

function internationalRepChange(target)
{
	$(target).parents('.js-rep-row').find("[name='state']").toggleClass('hidden');	
	$(target).parents('.js-rep-row').find("[for='Country']").toggleClass('hidden');	
	$(target).parents('.js-rep-row').find(".country").toggleClass('hidden');

	
	var defaultFlag = $(target).parents('.FormRowBackground').find('.js-default-address').is(':checked');

	var targetIndex = $(target).parents('.js-rep-row').find('.index').val();
	var data;
	
	if ($(target).prop("checked"))
	{
		//checked
		$(target).parents('.js-rep-row').find('.stateLabel').text("State/Province:");
		var data = {
				value: ["international=true", 'preferred=' + defaultFlag],
				targetIndex: targetIndex, targetProperty: "addresses",
				modelName: "Address"
			};
	}
	else
	{
		//uncheck
		$(target).parents('.js-rep-row').find('.stateLabel').text("State:");
		var data = {
				value: ["international=false","state="+$("[name='state']").val(),"country="+$("[name='country']").val(), 'preferred=' + defaultFlag],
				targetIndex: targetIndex, targetProperty: "addresses",
				modelName: "Address"
			};
	}
	
	//enable the delete buttons
	var container = $(target).parents('.js-rep-row');
	enableDeleteButtonXByContainer(container);
	enableAddButtonByContainer(container);
	
	saveRepEmbeddedModel(data);

	if (defaultFlag)
	{
		saveRepDefaultItem(target, 'defaultAddress');
	}
}


function addAddress(target)
{
	addressIndex++;
	
	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);

	//create the delete button
	var deleteButton = "<button class='DeleteButtonX btn btn-danger' onclick='javascript:deleteAddress(this)'>X</button>";

	var newElement = updateRepPage(target,deleteButton,addressIndex);

	//hide the country
	newElement.find(".country").addClass('hidden');
	newElement.find("[for='Country']").addClass('hidden');
	//set the state input
	newElement.find("input[name='state']").addClass('hidden');	
	newElement.find("select[name='state']").removeClass('hidden');	
	//set text for state
	newElement.find('.stateLabel').text("State:");
	
	
	// focus out elements to the components 
	$('.addressElement').focusout(function (){saveRepAddress(this);});//targetProperty: "addresses",	modelName: "Address"
	//$(".addressElement").bind('input',function (){changeElement(this);});
	//bind the last element by container
	bindRepTheLastRowChangeByAddButtonContainer(addButtonContainer);
	$('.internationalRepCheckbox').change(function (){internationalRepChange(this);});
}


function deleteAddress(target)
{
	addressIndex--;
	deleteRepElement(target,"addresses",addressIndex);
}

function validateRepStep1(functionToCall)
{
	var validation = $('#RepStep1ValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}



