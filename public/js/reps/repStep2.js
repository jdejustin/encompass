
function initRepStep2() {

	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	console.log('initRepStep2');
	/* focus out of the elements */
	$(".js-repProperty").focusout(function (){saveRepProperty(this);});

	$(".js-datePicker").datepicker({
      changeMonth: true,
      changeYear: true,
      onSelect: onDateSelected
    });

    $('.js-number-only').autoNumeric('init', {aPad: false, aSep: ''});
	
}

function onDateSelected(dateText, target)
{
	var propName = $(target).prop('input').attr('name');
	var serializedData = propName + '=' + dateText;
	saveRepModel(serializedData);
}

