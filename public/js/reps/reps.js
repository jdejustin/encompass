function saveRepProperty(target)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		//console.log("saving client");
		var serializedData = $(target).serialize();
		saveRepModel(serializedData);
	}
}


function saveToRep(target, targetProperty, targetModel, defaultFlag)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var targetIndex = $(target).parents('.js-rep-row').find('.index').val();

		if (!defaultFlag)
		{
			defaultFlag = false;
		}

		var data = {
			value: [$(target).attr("name") + "=" + $(target).val(), 'preferred=' + defaultFlag],
			targetIndex: targetIndex,
			targetProperty: targetProperty,
			modelName: targetModel
		};
	    
		//enable the delete buttons
		var container = $(target).parent().parent().parent();
		enableDeleteButtonXByContainer(container);
		enableAddButtonByContainer(container);

		saveRepEmbeddedModel(data);
	}
}

function saveRepDefaultItem(target, targetProperty)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var data = {
			value: $(target).val(),
			targetProperty: targetProperty,
		};

		jQuery.ajax({
			url:'/reps/saveDefaultItem',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
			success:function(data){
				console.log("saved default");
				createUserAccount();
			}
		});
	}
}

function deleteRepElement(target,targetProperty,elementIndex)
{
	var indexToDelete = $(target).parents('.js-rep-row').find('.index').val();
	repDeleteByIndex(target, targetProperty, elementIndex, null, indexToDelete);
}

function repDeleteByIndex(target,targetProperty,elementIndex,subTargetProperty,indexToDelete)
{
	var container = $(target).parent().parent().parent();
	var addButton = $(container).find('.addButton');
	//check if the affiliation was saved
	var attr = addButton.prop('disabled');
	// For some browsers, `attr` is undefined; for others,
	// `attr` is false.  Check for both.
	if (typeof attr !== 'undefined' && attr !== false) 
	{
		// add button is disable so the affiliation was not saved
		//enable button	
		addButton.removeClass("Disable");
		addButton.prop( "disabled", false );
		
		//enable the delete buttons
		enableClientDeleteButtonXByContainer(container);
	}
	else
	{
		var data;
		if (subTargetProperty)
		{
			data = {
					targetIndex: 0, targetProperty:targetProperty,subTargetIndex:indexToDelete,subTargetProperty: subTargetProperty,
			};
		}
		else
		{
			data = {
					targetIndex: indexToDelete, targetProperty: targetProperty,
			};
		}
	
		jQuery.ajax({
			url:'/reps/deleteEmbeddedModel',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
		});
	}
	
	//removing the elements
	$(target).parent().parent().remove();
	
	container.find(".FormRowBackground").each(function( index ) {
		$(this).find('.index').attr('value',index);	
	});
	
	//just remove the delete only if we have 1 row
	if (elementIndex == 0 )
	{
		container.find(':first-child').find('.DeleteButtonX').first().remove();
	}
}


//bind the change to the last element by addButton 
function bindRepTheLastRowChangeByAddButtonContainer(addButtonContainer)
{
	//unbind the old elements
	$(addButtonContainer).find(".js-element").each(function (){
		//$(this).css({"border":"0"});
		$(this).unbind('input');
	});
	
	//bind only for the last child of the rows 
	var $jsRowLastChild = $(addButtonContainer).find(".js-rep-row:last");
	$jsRowLastChild.find(".js-element").each(function(){
		//console.log('bind:'+this.name);
		//$(this).css({'border':'1px solid #FF0000'});
		$(this).bind('input',function(){changeRepField(this);});
	});
}

function changeRepField(actualField)
{
	//enable the add button
	$addButton = $(actualField).parents('.js-add-button-container').find('.js-add-button');
	$addButton.prop('disabled',false);
	$addButton.removeClass('Disable');

	//enable the delete buttons
	var container = $(actualField).parents('.js-add-button-container');
	enableClientDeleteButtonXByContainer(container);
	enableClientAddButtonByContainer(container);
}


//this function updates the page copying(cloning) the element the we need to add and also return the copied element in case that we need
//to update personalized things for example in address if we clone an address previously marked as international its add a country so 
//we need to hide country in this case
function updateRepPage(target,deleteButton,indexToUpdate)
{
	//container that we are working on
	var container = $(target).parent();
	
	//add the first delete button to the container
	showFirstDeleteButtonX(indexToUpdate, container, deleteButton);
	
	//get the elementToclone
	var elementToClone = $(target).parent().children(":first-child").clone();
	
	//disable all delete buttons after add the first delete button
	disableDeleteButtonXByContainer(container);

	//old button
	var oldAddButton = $(target).parent().find(".addButton");
	
	var addButton = oldAddButton.clone();
	//remove the old button 
	oldAddButton.remove();

	//clean the inputs
	$(elementToClone).find("input[type=text]").each(function( index ) {
		$(this).prop('value','');	
	});

	//clean the selects
	$(elementToClone).find("select").each(function( index ) {
		$(this).find('option:first-child').prop('selected', true);
	});
	
	//clean the checkboxes
	$(elementToClone).find("input[type=checkbox]").each(function( index ) {
		$(this).removeAttr('checked');
	});

	//clean the intNumberInputs
	var telInputElement = $(elementToClone).find(".intl-tel-input");

	//if exists a telInputElement we need to eliminate this and copy the number input only 
	if (telInputElement.length > 0)
	{
		var phoneElement = $(elementToClone).find(".PhoneNumber");
		telInputElement.before(phoneElement);
		telInputElement.remove();
	}
	
	
	//add the new element
	$(container).append(elementToClone);

	//append the new button 
	container.append(addButton);
	//disable addButton
	disableAddButtonByContainer(container);
	
	//Update the index of the elements 
	container.find(".FormRowBackground").each(function( index ) {
		$(this).find('.index').attr('value',index);	
	});

	//Fix for dealing with datePicker widget on the page, if it exists
	container.find(".FormRowBackground").each(function( index ) {
		$(this).find('.ExpirationDatePicker').removeClass('hasDatepicker');	
	});


	return elementToClone;
}



function createUserAccount()
{
	jQuery.ajax({
			url:'/reps/createUserAccount',
			datatype:'json',
			type: 'POST',
			async: false,
			data: null
		});
}

function saveRepModel(data)
{
	jQuery.ajax({
		url:'/reps/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("saved");
		}
	});
}

function saveRepEmbeddedModel(data)
{
	jQuery.ajax({
		url:'/reps/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("Saved: " + data);
		}
	});
}


function goRepDetails()
{
	jQuery("#content").load('/reps/1234 #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	    else if (data == 'null')
	   {
			goRepsList();
	   } else {
			initRepsShow();
	   }
	});
} 


function goRepsList()
{
	jQuery("#content").load('/reps #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initRepsIndex();
	   }
	});
} 

function goRepStep1()
{
	jQuery("#content").load('/reps/repStep1 #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initRepStep1();
	   }
	});
}

function goRepStep2()
{
	jQuery("#content").load('/reps/repStep2 #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
	   	initRepStep2();
	   }
	});
} 

