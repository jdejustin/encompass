
function initRepsIndex() {

  searchingMode = false;
  $('html, body').animate({ scrollTop: 0 }, 'slow');

  initGlobal();
  
  
	$('.deleteSelectedBtn').click(function(){
      showDeleteReps(this);
  }); 

  //init function to call when search is completed
  initClickOnTableRowReps();

  onRowClickFunction = initClickOnTableRowReps;

  //init the dialog to hide from HTML the first time 
  $( "#dialog-confirm-delete" ).dialog({autoOpen:false});
};

function initClickOnTableRowReps()
{
  $('.mainTable tbody td').not('.checkBoxTd').click(function(){
    console.log("repIndex: table click");
    jQuery("#content").load('/reps/' + $(this).parent().find('[type=checkbox]').val() + ' #AjaxContent', function (data){
      if(data == 'false')
      {
        window.location = "/expired";
      }
      else
      {
        initRepsShow();
      }
    });
  });

  //add select class to checkbox
  $('.mainTable tbody td').find('[type=checkbox]').addClass('selectElement');
  
  //set the behaviour of the select all element
  setSelectionBehaviour();  
}


function showDeleteReps (element)
{
  var title = 'Delete Selected Rep(s)?';
  var message = 'Selected Rep(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/reps/deleteRepFromClient';

  var callBack = function (data)
  {
     //finally, remove programs from clients
     jQuery("#content").load('/reps/deleteReps #AjaxContent', {'ids': data}, function (data) {
        if(data == 'false')
        {
          window.location = "/expired";
        }
        else
        {
          goRepsList();
        }
     });
  }

  //Prompt user if they're sure... if yes, then let's delete the selected reps from DB
  initDeleteDialog(title,message,deleteUrl,element, callBack);

}

function createNewRep()
{
  jQuery("#content").load('/reps/clearCurrentRep #AjaxContent', function (data){
    if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
        jQuery("#content").load('/reps/repStep1 #AjaxContent', function(){
          initRepStep1();
        });
     }
     
  });
}  

function importReps()
{
    jQuery('#content').load('/reps/import #AjaxContent',function (data){
      if(data == 'false')
     {
        window.location = "/expired";
     }
     else
     {
        initRepsImport();
     }
    });
}