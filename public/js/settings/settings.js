function initSettings()
{
  $('html, body').animate({ scrollTop: 0 }, 'slow');

   settingsIndexLoaded = true;

   updateNavigation('settings');

   $(".js-settingsProperty").focusout(function (){saveSettingsProperty(this);});

   $(".js-settingsViewLog").click(function () {
   	displayFile('/Communications/log/mailgun_log.html');
    });
}

function saveSettingsProperty(target)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var serializedData = $(target).serialize();
		saveSettingsModel(serializedData);
	}
}

function saveSettingsModel(data)
{
	jQuery.ajax({
		url:'/settings/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("saved");
		}
	});
}
