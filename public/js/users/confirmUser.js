
$(function (){
	initConfirmUser();
});

function initConfirmUser() {
	// Add listeners
	console.log("initConfirmedUser");
	$('#Password').focusout(function(){
		$('#ConfirmUserForm').validationEngine('hide');		
	});
	$('#PasswordConfirmation').focusout(function(){
		$('#ConfirmUserForm').validationEngine('hide');		
	});
	$('#ConfirmUserForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});	

}

function validateConfirmUser()
{
	return $('#ConfirmUserForm').validationEngine('validate');
}