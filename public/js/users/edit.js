var userId;

function initEditUser() {
	// Add listeners
	$('#Photo').on('change', uploadFile);
	$('#roleSelector').on('change', checkForClientRole);
}

function openFileOption(id)
{
	userId = id;

	document.getElementById("Photo").click();
};

// Grab the files and set them to our variable
function uploadFile(event)
{
	// Variable to store your files
	var files = event.target.files;

	var data = new FormData();
	 
	$.each(files, function(key, value)
	{
		data.append(key, value);
	});

	data.append('contactId', userId);
	data.append('displayName', 'userPhoto');

	jQuery.ajax({
			url:'/uploadPhoto',
			datatype:'json',
			type: 'POST',
			async: false,
			processData: false, // Don't process the files
	   	 	contentType: false, // Set content type to false as jQuery will tell the server its a query string request
			data: data,
			success:function(data){
				displayPhoto(data.fileFullName, $('#photoThumbImgForm'));

				data.userId =  userId;

				// //save to info to DB
				jQuery.ajax({
					url:'/users/savePhoto',
					datatype:'json',
					type: 'POST',
					async: true,
					data: data,
				});
			}
	});
}; 

function checkForClientRole()
{
	if ($('#roleSelector').val() == 'Client')
	{
		//display client list to user
		$('#clientsList').show();
	} else {
		$('#clientsList').hide();
	}
}

