$(function() {

	$('html, body').animate({ scrollTop: 0 }, 'slow');

	updateNavigation('users');

	initGlobal();

	//set the behaviour of the select all element
    setSelectionBehaviour();

    $('.deleteSelectedBtn').click(function(){
        showDeleteUsers(this);
    });
    
    //init function to call when search is completed
	initClickOnTableRow();

	onRowClickFunction = initClickOnTableRow;

    //init the dialog to hide from HTML the first time 
    $( "#dialog-confirm-delete" ).dialog({autoOpen:false});
	$("#passwordreset").toggle();
	 
  //   $('#photo').imgAreaSelect({
  //       // x1:0,
  //       // y1:0,
  //       // x2: 200,
  //       // y2: 300,
  //      	handless: false,
		// aspectRatio: '1:1',
		// onSelectEnd: previewPhoto 
  //   });

  //   function previewPhoto(img, selection)
  //   {
		// // if (!selection.width || !selection.height)
		// // return;
		
		// var scaleX = 100 / (selection.width || 1);
		// var scaleY = 100 / (selection.height || 1);
		
		// $('#preview').css({
		// 	width: Math.round(scaleX * 300) + 'px',
		// 	height: Math.round(scaleY * 300) + 'px',
		// 	marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
		// 	marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
		// });
  //   }
});


function createNewUser()
{
	jQuery("#content").load('/users/add #AjaxContent',function (data){
		if(data == 'false')
      {
         window.location = "/expired";
      }
      else
      {
         initEditUser();
   		getUserPhoto(null, 'photoThumbImgForm');
      }
	});
}


function updateEmailsBulk()
{
	jQuery.ajax({
			url:'/users/updateEmailsBulk',
			type: 'POST',
			success:function(){
				alert('All BDSI emails updated, please refresh the page by clicking on the "Users" section');
			}
	});
}

function showDeleteUsers(element)
{
    var title = 'Delete Selected User(s)?';
    var message = 'Selected User(s) will be permanently deleted and cannot be recovered. Are you sure?';
    var deleteUrl = '/users/deleteUsers';

    initDeleteDialog(title,message,deleteUrl,element);
}


function initClickOnTableRow()
{
	$('.mainTable tbody td').not('.checkBoxTd').click(function(){
		var photoPath = $(this).parent().find('[type=checkbox]').attr('data-photo');
		jQuery("#content").load('/users/' + $(this).parent().find('[type=checkbox]').val() + ' #AjaxContent',function (data){
			if(data == 'false')
         {
            window.location = "/expired";
         }
         else
         {
            getUserPhoto(photoPath, 'photoThumbImgShow');
         }
		});
	});
}

function resetUserPassword(email)
{
	jQuery.ajax({
			url:'/users/forgotPassword',
			data: {email:email},
			type: 'POST',
			success:function(){
				alert("Reset information has been sent to user.");
			}
	});
}

function showUserPassword(){
	$("#passwordreset").toggle("slow");
}

function updateUserPassword(email){
	 var password =document.getElementById('userResetPassword').value;
	 var data = {
		 			email:email,
					password:password
				};
	jQuery.ajax({
			url:'/users/updatePassword',
			data: data,
			type: 'POST',
			success:function(){
				alert("Password updated.");
				$("#passwordreset").hide("slow");
			}
	});	
}
