function validateEmail(emailaddressVal)
{
    var result = new Object();
	result.success = true;
	result.message = '';

    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var errorMessage = '<span class="error">Enter a valid email address.</span>';

    if(emailaddressVal == '' || !emailReg.test(emailaddressVal)) {
        result.success = false;
        result.message = errorMessage;
    } 

   	return result;
}