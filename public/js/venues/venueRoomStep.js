//var roomIndex;

function initVenueRoomStep() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	$(".venueRoomProperty").focusout(function (){
		saveToVenueMultipleProperty(this, roomIndex, 'rooms', 'VenueRoom');});

	$('.capacity').focusout(function (){saveMaxCapacity(this);});
	$('.maxCapacityGroup').click(function(){enableRoomCapacity(this)});

	$('.avCost').autoNumeric('init', {aSign:'$'});
	$('.avCost').focusout(function (){saveAVCost(this);});
	$('.avCostsGroup').click(function (){enableAVCost(this)});

	$('.inRoomAVGroup').click(function(){saveInRoomAV(this)});

	$('.currency').autoNumeric('init', {aSign:'$'});
	$('.currency').focusout(function (){saveRoomCurrency(this);});
}

function saveRoomCurrency(target)
{
	var data = {
		value: [$(target).attr("name") + "=" + $(target).autoNumeric('get')],
		targetIndex: roomIndex,
		targetProperty: 'rooms',
		modelName: 'VenueRoom'
	};
    
	saveVenueEmbeddedModel(data);
}

function enableRoomCapacity(target)
{
	if(target.checked)
	{
		$(target).parent().find('.capacity').removeAttr('disabled');
	} else {
		$(target).parent().find('.capacity').prop('value','');
		$(target).parent().find('.capacity').attr('disabled', true);
	}
	saveMaxCapacity(target);
}

function saveMaxCapacity(target)
{
	var maxCapacity = '';

	$(target).parent().parent().find('.capacity').each(function() {
		//get rate
		var rCapacity = $(this).val();

		maxCapacity += ((rCapacity == '')? 0 : rCapacity) + ',';

	});

	//strip last comma from string
	maxCapacity = maxCapacity.slice(0, maxCapacity.length - 1);

	var data = {
		value: ['maxCapacity=' + maxCapacity],
		targetIndex: roomIndex,
		targetProperty: 'rooms',
		modelName: 'VenueRoom'
	};
    
	saveVenueEmbeddedModel(data);
}

function enableAVCost(target)
{
	if(target.checked)
	{
		$(target).parent().find('.avCost').removeAttr('disabled');
	} else {
		$(target).parent().find('.avCost').prop('value', '');
		$(target).parent().find('.avCost').attr('disabled', true);
	}

	saveAVCost(target);
}

function saveAVCost(target)
{
	var avCost = '';

	$(target).parent().parent().find('.avCost').each(function() {
		//get rate
		var rAVCost = $(this).autoNumeric('get');
		
		avCost +=  ((rAVCost != '')? rAVCost : 0) + ',';

	});

	//strip last comma from string
	avCost = avCost.slice(0, avCost.length - 1);

	var data = {
		value: ['avCosts=' + avCost],
		targetIndex: roomIndex,
		targetProperty: 'rooms',
		modelName: 'VenueRoom'
	};
    
	saveVenueEmbeddedModel(data);
}

function saveInRoomAV(target)
{
	var inRoomAV = '';

	$(target).parent().find('.inRoomAVGroup').each(function(){
		inRoomAV += (($(this).is(':checked') )? $(this).val() : '') + ','; 

	});

	//strip last comma from string
	inRoomAV = inRoomAV.slice(0, inRoomAV.length - 1);

	var data = {
		value: ['inRoomAV=' + inRoomAV],
		targetIndex: roomIndex,
		targetProperty: 'rooms',
		modelName: 'VenueRoom'
	};
    
	saveVenueEmbeddedModel(data);
}

function createNewRoom()
{
	roomIndex = $(".mainTable tbody tr").length;
	console.log("room index"+roomIndex);
	goVenueRoomStep();//open the step without contact 
}
