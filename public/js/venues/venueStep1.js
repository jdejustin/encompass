var contactNumberIndex;
var addressIndex;

function initVenueStep1() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	console.log('init venue step1');
	$(".PhoneNumber").intlTelInput();

	$('.js-number-only').autoNumeric('init', {aPad: false, aSep: ''});

	$(".venueProperty").focusout(function (){saveVenueProperty(this);});

	$('.internationalVenueCheckbox').change(function (){venueInternationalChange(this);});

	$('.PhoneElement').focusout(function (){saveVenueContactNumber(this);});
	$(".PhoneElement").bind('input',function (){changeVenueField(this);});

	$('.AddressElement').focusout(function (){
		var targetIndex = $(this).parent().parent().find('.Index').val(); 
		saveToVenueMultipleProperty(this, targetIndex, 'addresses', 'Address');});
	$(".AddressElement").bind('input',function (){changeVenueField(this);});

	contactNumberIndex = $("#VenueContactNumberContainer").children('.FormRowBackground').length-1;
	addressIndex = $("#AddressContainer").children('.FormRowBackground').length-1;

		//start the validation engine 
	$('#VenueStep1Form').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});

}

///////////  ADDRESS /////////////////

function venueInternationalChange(target)
{
	console.log('venue international change');
	$(target).parent().parent().find("[name='state']").toggleClass('hidden');	
	$(target).parent().parent().find("[for='Country']").toggleClass('hidden');	
	$(target).parent().parent().find(".country").toggleClass('hidden');

	
	var targetIndex = $(target).parent().parent().find('.Index').val();
	var data;
	
	if ($(target).prop("checked"))
	{
		//checked
		$(target).parent().parent().find('.stateLabel').text("State/Province:");
		var data = {
				value: ["international=true"],
				targetIndex: targetIndex, 
				targetProperty: "addresses",
				modelName: "Address"
			};
	}
	else
	{
		//uncheck
		$(target).parent().parent().find('.stateLabel').text("State:");
		var data = {
				value: ["international=false","state="+$("[name='state']").val(),"country="+$("[name='country']").val()],
				targetIndex: targetIndex, 
				targetProperty: "addresses",
				modelName: "Address"
			};
	}
	
	//enable the delete buttons
	var container = $(target).parent().parent().parent();
	enableDeleteButtonXByContainer(container);
	enableAddButtonByContainer(container);
	
	saveVenueEmbeddedModel(data);
}


function addRepAddress(target)
{
	addressIndex++;

	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);
	
	//create the delete button
	var deleteButton = '<button class="DeleteButtonX btn btn-danger" onclick="javascript:deleteVenueAddress(this)">X</button>';

	var newElement = updateVenuePage(target,deleteButton,addressIndex);

	//hide the country
	newElement.find(".country").addClass('hidden');
	newElement.find("[for='Country']").addClass('hidden');
	//set the state input
	newElement.find("input[name='state']").addClass('hidden');	
	newElement.find("select[name='state']").removeClass('hidden');	
	//set text for state
	newElement.find('.stateLabel').text("State:");
	
	
	// focus out elements to the components 
	$('.AddressElement').focusout(function (){
		var targetIndex = $(this).parent().parent().find('.Index').val(); 
		saveToVenueMultipleProperty(this, targetIndex,  'addresses', 'Address');
	});

	//bind the last element by container
	bindVenueTheLastRowChangeByAddButtonContainer(addButtonContainer);
	$('.internationalVenueCheckbox').change(function (){venueInternationalChange(this);});
}


function deleteVenueAddress(target)
{
	addressIndex--;
	deleteVenueElement(target,"addresses",addressIndex);
}

///////////  CONTACT NUMBER /////////////////
function addVenueNumber(target)
{
	contactNumberIndex++;
	
	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);
	
	//create the delete button
	var deleteButton = '<button class="DeleteButtonX btn btn-danger" onclick="javascript:deleteVenueContactNumber(this)">X</button>';
	
	var clonedElement = updateVenuePage(target,deleteButton,contactNumberIndex);
	
	// add focus out to the components 
	$('.PhoneElement').focusout(function (){saveVenueContactNumber(this);});
	//$(".PhoneElement").bind('input',function (){changeElement(this);});

	//bind the last element by container
	bindVenueTheLastRowChangeByAddButtonContainer(addButtonContainer);

	$(".PhoneNumber").intlTelInput();
}
    


function saveVenueContactNumber(target)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var targetIndex;
		
		if ($(target).attr('name') == 'phoneType')
		{
			targetIndex = $(target).parent().parent().find('.Index').val();
		} else {
	    	targetIndex = $(target).parent().parent().parent().find('.Index').val();
	    }
	    
		var data = {
				value: [$(target).attr("name")+"="+$(target).val()],
				targetIndex: targetIndex,
				targetProperty: "phoneNumbers",
				modelName: "ContactPhoneNumber"
			};
	
		//enable the delete buttons
		var container = $(target).parent().parent().parent();
		enableDeleteButtonXByContainer(container);
		enableAddButtonByContainer(container);
		
		saveVenueEmbeddedModel(data);
	}
}


function deleteVenueContactNumber(target)
{
	contactNumberIndex--;
	deleteVenueElement(target,"phoneNumbers",contactNumberIndex);
}

function validateVenueStep1(functionToCall)
{
	var validation = $('#VenueStep1Form').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}

