function initVenueStep2() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	$('.startTime').timepicker();
	$('.startTime').on('changeTime', function (){saveHoursOfOperation()});

	$('.endTime').timepicker();
	$('.endTime').on('changeTime', function (){saveHoursOfOperation()});

	$('.currency').autoNumeric('init', {aSign:'$'});
	$('.currency').focusout(function (){saveSpecialNumberType(this);});

	$('.percentage').autoNumeric('init', {aSign:'%', pSign: 's', aPad: false});
	$('.percentage').focusout(function (){saveSpecialNumberType(this);});

	$('.parkingRate').autoNumeric('init', {aSign:'$'});
	$('.parkingRate').focusout(function (){saveParking();});

	$(".venueProperty").focusout(function (){saveVenueProperty(this);});

	$('.daysGroup').click(enableTimeFrame);
	$('.parkingGroup').click(enableParkingRate);
	$('.venueAVGroup').click(saveAVGroup);
}

function saveSpecialNumberType(target)
{
	var serializedData = $(target).attr('name') + '=' + $(target).autoNumeric('get');
	saveVenueModel(serializedData);
}

function enableTimeFrame()
{
	if(this.checked)
	{
		$(this).parent().find('.startTime').removeAttr('disabled');
		$(this).parent().find('.endTime').removeAttr('disabled');
	} else {
		$(this).parent().find('.startTime').attr('disabled', true);
		$(this).parent().find('.endTime').attr('disabled', true);
	}

	saveHoursOfOperation();
}

function saveHoursOfOperation()
{
	var hoursOfOperation = '';

	$('.startTime').each(function (){
		//start time
		var sTime = $(this).val();
		hoursOfOperation +=  (isNaN(sTime)? sTime : 0) + ',';

		//end time
		var eTime = $(this).parent().find('.endTime').val();
		hoursOfOperation +=  (isNaN(eTime)? eTime : 0) + ',';
	});

	//strip last comma from string
	hoursOfOperation = hoursOfOperation.slice(0, hoursOfOperation.length - 1);

	var serializedData = 'hours=' + hoursOfOperation;
	saveVenueModel(serializedData);
}

function enableParkingRate()
{
	if(this.checked)
	{
		$(this).parent().find('.parkingRate').removeAttr('disabled');
	} else {
		$(this).parent().find('.parkingRate').attr('disabled', true);
	}

	saveParking();
}

function saveParking()
{
	var parking = '';

	$('.parkingRate').each(function (){
		//get rate
		var pRate = $(this).autoNumeric('get');
		
		parking +=  ((pRate != '')? pRate : 0) + ',';
	});

	//strip last comma from string
	parking = parking.slice(0, parking.length - 1);

	var serializedData = 'parking=' + parking;
	saveVenueModel(serializedData);
}

function saveAVGroup()
{
	var avgroup = '';

	$('.venueAVGroup').each(function(){
		avgroup += (($(this).is(':checked') )? $(this).val() : '') + ','; 
	});

	//strip last comma from string
	avgroup = avgroup.slice(0, avgroup.length - 1);

	var serializedData = 'venueAV=' + avgroup;
	saveVenueModel(serializedData);
}