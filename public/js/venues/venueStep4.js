var primaryContactNumberIndex;

function initVenueStep4() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	
	$(".PhoneNumber").intlTelInput();

	$(".venuePrimaryContactProperty").focusout(function (){
		var targetIndex = $(this).parent().parent().find('.Index').val(); 
		saveToVenueMultipleProperty(this, targetIndex, 'contact', 'ContactInfo');});

	$('.VenuePrimaryContactNumberElement').focusout(function (){saveVenuePrimaryContactNumber(this);});
	$(".VenuePrimaryContactNumberElement").bind('input',function (){changeVenueField(this);});

	primaryContactNumberIndex = $("#VenuePrimaryContactNumberContainer").children('.FormRowBackground').length-1;

	//start the validation engine 
	$('#VenueStep4ValidateForm').validationEngine('attach',{autoHidePrompt : true, autoHideDelay: 5000});
}


///////////  CONTACT NUMBER /////////////////
function addVenuePrimaryContactNumber(target)
{
	primaryContactNumberIndex++;

	//addbuttonContainer this should be before the update page 
	var addButtonContainer = $(target).parents('.js-add-button-container').eq(0);

	//create the delete button
	var deleteButton = '<input class="DeleteButtonX Right" type="button" value="X" onclick="javascript:deleteVenuePrimaryContactNumber(this)"/>';
	
	var clonedElement = updateVenuePage(target,deleteButton,primaryContactNumberIndex);
	
	// add focus out to the components 
	$('.VenuePrimaryContactNumberElement').focusout(function (){saveVenuePrimaryContactNumber(this);});
	//$(".VenuePrimaryContactNumberElement").bind('input',function (){changeElement(this);});

	//bind the last element by container
	bindVenueTheLastRowChangeByAddButtonContainer(addButtonContainer);

	$(".PhoneNumber").intlTelInput();
}
    

function deleteVenuePrimaryContactNumber(target)
{
	primaryContactNumberIndex--;
	deleteVenueElement(target,"contact",primaryContactNumberIndex,"phoneNumbers");
}

function validateVenueStep4(functionToCall)
{
	var validation = $('#VenueStep4ValidateForm').validationEngine('validate');
	if(validation)
	{
		functionToCall();
	}
}