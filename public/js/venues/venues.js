function setCurrentVenueId(id)
{
	jQuery.ajax({
		url:'/venues/setCurrentVenue',
		datatype:'json',
		type: 'POST',
		async: true,
		data: id,
		success:function(data){
			console.log("current venue id set");
		}
	});
}

function saveVenueProperty(target,setEventVenueCallback)
{
	var validation = $(target).validationEngine('validate');
	if($(target).val() && $(target).val() != selectPrompt && !validation)
	{
		//console.log("saving client");
		var serializedData = $(target).serialize();
		saveVenueModel(serializedData,setEventVenueCallback);
	}
}

function saveToVenue(target, targetProperty, targetModel, setEventVenueCallback)
{
	if($(target).val() && $(target).val() != selectPrompt)
	{
		var data = {
			value: [$(target).attr("name") + "=" + $(target).val()],
			targetIndex: 0,
			targetProperty: targetProperty,
			modelName: targetModel
		};
	    
		saveVenueEmbeddedModel(data, setEventVenueCallback);
	}
}

function saveToVenueMultipleProperty(target, targetIndex, targetProperty, targetModel)
{
	var validation = $(target).validationEngine('validate');
	if($(target).val() && $(target).val() != selectPrompt && !validation)
	{
		var data = {
			value: [$(target).attr("name") + "=" + $(target).val()],
			targetIndex: targetIndex,
			targetProperty: targetProperty,
			modelName: targetModel
		};
	    
		//enable the delete buttons
		var container = $(target).parent().parent().parent();
		enableDeleteButtonXByContainer(container);
		enableAddButtonByContainer(container);
		
		saveVenueEmbeddedModel(data);
	}
}

function saveVenueModel(data,setEventVenueCallback)
{
	jQuery.ajax({
		url:'/venues/save',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("save venue model "+ JSON.stringify(data));
			if (setEventVenueCallback)
			{
				setEventVenueCallback(data);
			}
		}
	});
}


function saveVenueEmbeddedModel(data, setEventVenueCallback)
{
	jQuery.ajax({
		url:'/venues/saveEmbeddedModel',
		datatype:'json',
		type: 'POST',
		async: true,
		data: data,
		success:function(data){
			console.log("save venue embedded model "+ JSON.stringify(data));
			if (setEventVenueCallback)
			{
				setEventVenueCallback(data);
			}
		}
	});
}

function showFirstDeleteButtonX(elementIndex,container,deleteButton)
{
	//if we are adding the second element we need to add the delete button to the first element that already exist
	if(elementIndex == 1 )
	{
		container.find('.FormRowBackground').children(':nth-child(2)').append(deleteButton);
	}
}

function enableDeleteButtonXByContainer(container)
{
	//enable all delete buttons after add the first delete button
	$(container).find(".DeleteButtonX").each(function( index ) {
		$(this).prop('disabled',false);	
		$(this).removeClass('Disable');	
	});
}

function disableDeleteButtonXByContainer(container)
{
   //disable all delete buttons after add the first delete button
	$(container).find(".DeleteButtonX").each(function( index ) {
		$(this).prop('disabled',true);	
		$(this).addClass('Disable');	
	});
}

function enableAddButtonByContainer(container)
{
	//enable button	
	$(container).find(".addButton").removeClass("Disable");
	$(container).find(".addButton").prop("disabled",false);
}

function changeElement(target)
{
	if ($(target).val() != "")
	{
		enableAddButtonByContainer($(target).parent().parent().parent());
	}
}

function disableAddButtonByContainer(container)
{
	//enable button	
	$(container).find(".addButton").addClass("Disable");
	$(container).find(".addButton").prop("disabled",true);
}

//this function updates the page copying(cloning) the element the we need to add and also return the copied element in case that we need
//to update personalized things for example in address if we clone an address previously marked as international its add a country so 
//we need to hide country in this case
function updateVenuePage(target,deleteButton,indexToUpdate)
{
	//container that we are working on
	var container = $(target).parent();
	
	//add the first delete button to the container
	showFirstDeleteButtonX(indexToUpdate, container, deleteButton);
	
	//get the elementToclone
	var elementToClone = $(target).parent().children(":first-child").clone();
	
	//disable all delete buttons after add the first delete button
	disableDeleteButtonXByContainer(container);

	//old button
	var oldAddButton = $(target).parent().find(".addButton");
	
	var addButton = oldAddButton.clone();
	//remove the old button 
	oldAddButton.remove();

	//clean the inputs
	$(elementToClone).find("input[type=text]").each(function( index ) {
		$(this).prop('value','');	
	});

	//clean the selects
	$(elementToClone).find("select").each(function( index ) {
		$(this).find('option:first-child').prop('selected', true);
	});
	
	//clean the checkboxes
	$(elementToClone).find("input[type=checkbox]").each(function( index ) {
		$(this).removeAttr('checked');
	});

	//clean the intNumberInputs
	var telInputElement = $(elementToClone).find(".intl-tel-input");

	//if exists a telInputElement we need to eliminate this and copy the number input only 
	if (telInputElement.length > 0)
	{
		var phoneElement = $(elementToClone).find(".PhoneNumber");
		telInputElement.before(phoneElement);
		telInputElement.remove();
	}
	
	
	//add the new element
	$(container).append(elementToClone);

	//append the new button 
	container.append(addButton);
	//disable addButton
	disableAddButtonByContainer(container);
	
	//Update the index of the elements 
	container.find(".FormRowBackground").each(function( index ) {
		$(this).find('.Index').attr('value',index);	
	});

	//Fix for dealing with datePicker widget on the page, if it exists
	container.find(".FormRowBackground").each(function( index ) {
		$(this).find('.ExpirationDatePicker').removeClass('hasDatepicker');	
	});


	return elementToClone;
}

function deleteVenueElement(target,targetProperty,elementIndex,subTargetProperty)
{
	var container = $(target).parent().parent().parent();
	var indexToDelete = $(target).parent().parent().find('.Index').val(); 
	var addButton = $(container).find('.addButton');

	//check if the element was saved
	var attr = addButton.prop('disabled');
	// For some browsers, `attr` is undefined; for others,
	// `attr` is false.  Check for both.
	if (typeof attr !== 'undefined' && attr !== false) 
	{
		// add button is disable so the affiliation was not saved
		//enable button	
		addButton.removeClass("Disable");
		addButton.prop( "disabled", false );
		
		//enable the delete buttons
		enableDeleteButtonXByContainer(container);
	}
	else
	{
		var data;
		if (subTargetProperty)
		{
			data = {
					targetIndex: 0, targetProperty:targetProperty,subTargetIndex:indexToDelete,subTargetProperty: subTargetProperty,
			};
		}
		else
		{
			data = {
					targetIndex: indexToDelete, targetProperty: targetProperty,
			};
		}
	
		jQuery.ajax({
			url:'/venues/deleteEmbeddedModel',
			datatype:'json',
			type: 'POST',
			async: true,
			data: data,
		});
	}
	
	//removing the elements
	$(target).parent().parent().remove();
	
	container.find(".FormRowBackground").each(function( index ) {
		$(this).find('.Index').attr('value',index);	
	});
	
	//just remove the delete only if we have 1 row
	if (elementIndex == 0 )
	{
		container.find(':first-child').find('.DeleteButtonX').remove();
	}
}


//bind the change to the last element by addButton 
function bindVenueTheLastRowChangeByAddButtonContainer(addButtonContainer)
{
	//unbind the old elements
	$(addButtonContainer).find(".js-element").each(function (){
		//$(this).css({"border":"0"});
		$(this).unbind('input');
	});
	
	//bind only for the last child of the rows 
	var $jsRowLastChild = $(addButtonContainer).find(".js-venue-row:last");
	$jsRowLastChild.find(".js-element").each(function(){
		//console.log('bind:'+this.name);
		//$(this).css({'border':'1px solid #FF0000'});
		$(this).bind('input',function(){changeVenueField(this);});
	});
}

function changeVenueField(actualField)
{
	//enable the add button
	$addButton = $(actualField).parents('.js-add-button-container').find('.js-add-button');
	$addButton.prop('disabled',false);
	$addButton.removeClass('Disable');

	//enable the delete buttons
	var container = $(actualField).parents('.js-add-button-container');
	enableClientDeleteButtonXByContainer(container);
	enableClientAddButtonByContainer(container);
}



function saveVenuePrimaryContactNumber(target, setEventVenueCallback)
{
	if($(target).val()!=selectPrompt)
	{
		var subTargetIndex;
		if ($(target).attr('name') == 'phoneType')
		{
			subTargetIndex = $(target).parent().parent().find('.Index').val();
		} else {
	    	subTargetIndex = $(target).parent().parent().parent().find('.Index').val();
	    }

		if (typeof subTargetIndex == 'undefined')
		{
			// if index it s not found the saving is from add venue on the evenstep3 >  venueInformation or catererInformation
			subTargetIndex = 0;
		}
	    
		var data = {
				value: [$(target).attr("name")+"="+$(target).val()],
				targetIndex: 0,
				subTargetIndex: subTargetIndex,
				targetProperty: "contact",
				subTargetProperty:"phoneNumbers",
				modelName: "ContactInfo",
				subModelName:"ContactPhoneNumber"
			};
	
		//enable the delete buttons
		var container = $(target).parent().parent().parent();
		enableDeleteButtonXByContainer(container);
		enableAddButtonByContainer(container);
		
		saveVenueEmbeddedModel(data, setEventVenueCallback);
	}
}

function goVenueDetails()
{
	jQuery("#content").load('/venues/1234 #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else	if (data == 'null')
	   {
		goVenuesList();
	   } else {
			initVenuesShow();
	   }

	});
} 


function goVenuesList()
{
	jQuery("#content").load('/venues #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initVenuesIndex();
	   }

	});
} 

function goVenueStep1()
{
	jQuery("#content").load('/venues/venueStep1 #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initVenueStep1();
	   }

	});
} 

function goVenueStep2()
{
	jQuery("#content").load('/venues/venueStep2 #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initVenueStep2();
	   }

	});
}

function goVenueStep3()
{
	jQuery("#content").load('/venues/venueStep3 #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initVenueStep3();
	   }

	});
}

function goVenueStep4()
{
	jQuery("#content").load('/venues/venueStep4 #AjaxContent', function (data){
		if(data == 'false')
	   {
	      window.location = "/expired";
	   }
	   else
	   {
			initVenueStep4();
	   }


	});
}


function goVenueRoomStep(roomIndex)
{
	if (roomIndex)
	{
		jQuery("#content").load('/venues/venueRoomStep?index='+roomIndex+' #AjaxContent', function (data){
			if(data == 'false')
		   {
		      window.location = "/expired";
		   }
		   else
		   {
				initVenueRoomStep();
		   }

		});
	}
	else
	{
		jQuery("#content").load('/venues/venueRoomStep #AjaxContent', function (data){
			if(data == 'false')
		   {
		      window.location = "/expired";
		   }
		   else
		   {
				initVenueRoomStep();
		   }
		});
	}
}