function initVenuesIndex() {

  $('html, body').animate({ scrollTop: 0 }, 'slow');

  initGlobal();
  
  $('#advancedSearch').click(function(){
    //display advanced search options
    $('#advancedSearchOptions').toggle();
  });

	$('.deleteSelectedBtn').click(function(){
    	showDeleteVenues(this);
    });

  //init function to call when search is completed
  initClickOnTableRowVenues();

  onRowClickFunction = initClickOnTableRowVenues;

  $( "#dialog-venueLink" ).dialog({autoOpen: false});
  $( "#dialog-confirm-delete" ).dialog({autoOpen: false});
};

function initClickOnTableRowVenues()
{
  $('.mainTable tbody td').not('.checkBoxTd').click(function(){
    jQuery("#content").load('/venues/' + $(this).parent().find('[type=checkbox]').val() + ' #AjaxContent', function (data){
      if(data == 'false')
      {
        window.location = "/expired";
      }
      else
      {
        initVenuesShow();
      }
    });
  });

  //add select class to checkbox
  $('.mainTable tbody td').find('[type=checkbox]').addClass('selectElement');

  //set the behaviour of the select all element
  setSelectionBehaviour(); 
}


function showDeleteVenues(element)
{
  var title = 'Delete Selected Venue(s)?';
  var message = 'Selected Venue(s) will be permanently deleted and cannot be recovered. Are you sure?';
  var deleteUrl = '/venues/deleteVenues';

  var callBack = function (data)
  {
     //finally, remove venues from clients
     jQuery("#content").load('/venues/deleteVenueFromOthers #AjaxContent', {'ids': data}, function (data) {
        if(data == 'false')
        {
          window.location = "/expired";
        }
        else
        {
          goVenuesList();
        }
     });
  }
  
  //Prompt user if they're sure... if yes, then let's delete the selected venues from DB
  initDeleteDialog(title,message,deleteUrl,element, callBack);

}

function addVenueToClient()
{
  var successCallback = function ()
  {
    goVenuesList();
  };  

  var closeCallback = function() {
    goVenuesList();
  };

  var url = '/venues/addVenueToClient';

  initVenueLinkDialog(url,successCallback, closeCallback);
}

function createNewVenue()
{
	jQuery("#content").load('/venues/clearCurrentVenue #AjaxContent', function (data){
    if(data == 'false')
      {
        window.location = "/expired";
      }
      else
      {
      	jQuery("#content").load('/venues/venueStep1 #AjaxContent', function(){
      		initVenueStep1();
        });
      }
	});
}  